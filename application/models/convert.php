<?php
class Convert extends CI_model {
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function id($id,$tabel) {
        $query = $this->db->get_where('setting', array('id' => 1))->row();
        return $query;
    }
  
    function traps() {
		$this->db->select('');
        $this->db->from('traps_3');
        $query = $this->db->get()->result();
		xdebug($query);die;
        return $query;
    }
	
    function convert_id($id,$tabel) {
        $query = $this->db->query("select * from ".$tabel." where id = '".$id."'")->row();
        return $query;
    }
    
    function convert_name($name,$tabel) {
        $query = $this->db->query("select * from ".$tabel." where nicename = '".$name."'")->row();
        return $query;
    }
    
    function convert_parent($id) {
        $cat[] = $id;
        $this->db->select('');
        $this->db->where('id_parent', $id);
        $this->db->from('m_menu');
        $chk = $this->db->count_all_results();
        if($chk) {
            $this->db->select('');
            $this->db->where('id_parent', $id);
            $sub = $this->db->get('m_menu')->result();
            foreach ($sub as $c) {
                $cat[] = $c->id_menu;
                $subs = $this->convert_parent($c->id_menu);
                $cat = array_merge($cat, $subs);
            }
        }
        $cat = array_reverse(array_unique($cat));
        return $cat;
    }
    
    function child_data($id) {
        $this->db->select('distinct(account_id), account.*');
        $this->db->where('log.places_id', $id);
        $this->db->where('log.log_status', 1);
        $this->db->where('log.log_type', 1);
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->limit(20,0);
        $this->db->from('log');
        
        $cat = $this->db->get()->result();
        return $cat;
    }
    
    function family($id) {
        $this->db->select('');
        $this->db->where('id', $id);
        $this->db->where('places_parent', 0);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $data1 = $this->db->get()->result();
        
        $this->db->select('');
        $this->db->where('places_parent', $id);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $data2 = $this->db->get()->result();
        $data3 = array_merge($data1, $data2);
        
        foreach($data3 as $row) {
            $x[] = $row->id;
        }
        
        return $x;
    }
    
	function familyparent($id) {        
        $this->db->select('id');
        $this->db->where('places_parent', $id);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $data2 = $this->db->get()->result();
        
        foreach($data2 as $row) {
            $x[] = $row->id;
        }
        
        return $x;
	}
	
    function nicename($file, $id = 0) {
        $healthy = array(" ", ".",",","?","&","(",")","#","!","^","%","\'","@","}","{","]","[","|","+","","'","/","|",":",'"',"<",">",";","--","+");
        $yummy   = array("-", "",);
        $result	 = strtolower(str_replace($healthy, $yummy, trim($file)));
        $result = preg_replace('/[^;\sa-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ]+/u', '', $result);
        
        $this->db->select('');
        $this->db->where('account_username', $result);
        $this->db->where('id !=', $id);
        $this->db->where('account_status !=', 0);
        $this->db->from('account');
        $c = $this->db->count_all_results();
        
        if($c) {
            $this->db->select('');
            $this->db->where('id !=', $id);
            $this->db->where('account_status !=', 0);
            $this->db->like('account_username', $result, 'after');
            $this->db->from('account');
            $c = $this->db->count_all_results();
            
            if($c) {
                $this->db->select('');
                $this->db->where('id !=', $id);
                $this->db->where('account_status !=', 0);
                $this->db->like('account_username', $result, 'after');
                $this->db->order_by('id', 'desc');
                $this->db->limit(1, 0);
                $res = $this->db->get('account')->row();
                
                $slice = explode('-', $res->account_username);
                $cc = count($slice);
                if(is_numeric($slice[$cc-1])) {
                    $slice[$cc-1] = $slice[$cc-1] + 1;
                    $jadi = implode('-', $slice);
                } else {
                    $jadi = $result.'-1';
                }
            } else {
                $jadi = $result.'-1';
            }
        } else {
            $jadi = $result;
        }
        return $jadi;
    }
    
    function recent() {
        $this->db->select('');
        $this->db->where('log_status', 1);
        $this->db->where('places.places_type <', 3);
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->limit(7, 0);
        $this->db->from('log');
        $recent = $this->db->get()->result();
        return $recent;
    }
    
    function recent_admin() {
        $this->db->select('');
        $this->db->where('log_status', 1);
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->limit(7, 0);
        $this->db->from('log');
        $recent = $this->db->get()->result();
        return $recent;
    }
    
    function history($user, $type) {
        $this->db->select('');
        $this->db->where('account_id', $user);
        $this->db->where('log_type', $type);
        $this->db->where('log_status', 1);
        $this->db->from('log');
        $recent = $this->db->count_all_results();
        return $recent;
    }
    
    function action($data) {
        $family = $this->family($data);
        
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_type', 1);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['all'] = $this->db->count_all_results();
        
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_fb !=', 0);
        $this->db->where('log.log_type', 1);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['fb'] = $this->db->count_all_results();
        
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_tw !=', 0);
        $this->db->where('log.log_type', 1);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['tw'] = $this->db->count_all_results();
    /*    
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_fs !=', 0);
        $this->db->where('log.log_type', 1);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['fs'] = $this->db->count_all_results();
        
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_gw !=', 0);
        $this->db->where('log.log_type', 1);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['gw'] = $this->db->count_all_results(); */

		$this->db->select('distinct(wooz_log.account_id)');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_type', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        
		$result['user'] = $this->db->get()->num_rows();
		
        return $result;	
    }

    function actiontotal($data) {
        $family = $this->family($data);
        
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_fb !=', 0);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['all'] = $this->db->count_all_results();
        
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_fb !=', 0);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['fb'] = $this->db->count_all_results();
        
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_tw !=', 0);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['tw'] = $this->db->count_all_results();

 		$this->db->select('distinct(wooz_log.account_id)');
        $this->db->where_in('log.places_id', $family);
        $this->db->order_by('log.id', 'desc');
        $this->db->where('log.log_status', 1);
        $this->db->from('log');
        
		$result['user'] = $this->db->get()->num_rows();       
        
        return $result;	
    }

    function action1($data) {
        $family = $this->family($data);
        
        $this->db->select('');
        $this->db->where_in('photos.places_id', $family);
        $this->db->order_by('photos.id', 'desc');
        $this->db->from('photos');
        $result['all'] = $this->db->count_all_results();
        
        $this->db->select('');
        $this->db->where_in('photos.places_id', $family);
        $this->db->where('photos.pid !=', 0);
        $this->db->order_by('photos.id', 'desc');
        $this->db->from('photos');
        $result['fb'] = $this->db->count_all_results();
        
        $this->db->select('');
        $this->db->where_in('photos.places_id', $family);
        $this->db->where('photos.twit_id !=', 0);
        $this->db->order_by('photos.id', 'desc');
        $this->db->from('photos');
        $result['tw'] = $this->db->count_all_results();
     
        $this->db->select('');
        $this->db->where_in('log.places_id', $family);
        $this->db->where('log.log_fs !=', 0);
        $this->db->where('log.log_type', 1);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result['fs'] = $this->db->count_all_results();
      /*     
        $this->db->select('');
        $this->db->where_in('photos.places_id', $family);
        $this->db->where('log.log_gw !=', 0);
        $this->db->where('log.log_type', 1);
        $this->db->where('log.log_status', 1);
        $this->db->order_by('photos.id', 'desc');
        $this->db->from('log');
        $result['gw'] = $this->db->count_all_results();
     */   
        return $result;	
    }
	
	function photos($data) {
        $family = $this->family($data);

        $this->db->select('');
        $this->db->where_in('photos.places_id', $family);
        $this->db->order_by('photos.id', 'desc');
        $this->db->from('photos');
        $result['photo'] = $this->db->count_all_results();

		$this->db->select('distinct(wooz_photos.account_id)');
        $this->db->where_in('photos.places_id', $family);
        $this->db->order_by('photos.id', 'desc');
        $this->db->from('photos');
        
		$result['user'] = $this->db->get()->num_rows();

        return $result;	
    }
}

