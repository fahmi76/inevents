<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of auth_m
 *
 * @author ubaidz
 */
class photobooth_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
    }

    function insert($data, $table) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function insertlog($photo_id) {
        $query = $this->db->query("select * from wooz_photos where id = $photo_id");
        $input = $query->row();

        $db['account_id'] = $input->account_id;
        $db['places_id'] = $input->places_id;
        $db['log_fb'] = $input->pid;
        $db['log_tw'] = $input->twit_id;
        $db['log_type'] = 6;
        $db['log_date'] = date('Y-m-d');
        $db['log_time'] = date('h:i:s');
        $db['log_hash'] = $input->code_photo;
        $db['log_status'] = $input->status;
        $this->db->insert('log', $db);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('photos', $data);
        return $update;
    }

    function user($id) {
        $query = $this->db->query("select account_tw_token,account_tw_secret,account_fbid,account_token,account_displayname from wooz_account where id = $id");
        $hasil = $query->row();
        return $hasil;
    }

    function places($id) {
        $query = $this->db->query("select places_fb_caption,places_album,places_tw_caption from wooz_places where id = $id");
        $hasil = $query->row();
        return $hasil;
    }

    function userlanding($rfid, $places) {
        $query = $this->db->query("select account_id from wooz_landing where landing_rfid = $rfid and landing_register_form = $places order by id desc");
        $hasil = $query->row();
        return $hasil;
    }

    function usercek($id) {
        $query = $this->db->query("select id,account_rfid,account_displayname from wooz_account where id = $id");
        $hasil = $query->row();
        return $hasil;
    }

    function usercekrfid($rfid) {
        $query = $this->db->query("select id,account_rfid,account_displayname from wooz_account where account_rfid = $rfid");
        $hasil = $query->row();
        return $hasil;
    }

}

?>
