<?php

class photobooth_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        date_default_timezone_set('Asia/Jakarta');
    }

    function getplaceslanding($custom_page) {
        $result = array();
        $sql = "SELECT id,places_duedate,places_parent FROM wooz_places WHERE id = '$custom_page'";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
            $a = strtotime($data->places_duedate);
            $b = time();
            if($a > $b){
                $result = $data;
            }
        }
        return $result;
    }
	
    function insert($data, $table) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function insertlog($photo_id) {
        $query = $this->db->query("select * from wooz_photos where id = $photo_id");
        $input = $query->row();

        $db['account_id'] = $input->account_id;
        $db['places_id'] = $input->places_id;
        $db['log_fb'] = $input->pid;
        $db['log_tw'] = $input->twit_id;
        $db['log_type'] = 6;
        $db['log_date'] = date('Y-m-d');
        $db['log_time'] = date('h:i:s');
        $db['log_hash'] = $input->code_photo;
        $db['log_status'] = $input->status;
        $this->db->insert('log', $db);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $this->db->where('id', $id);
        $update = $this->db->update('photos', $data);
        return $update;
    }

    function user($id) {
        $query = $this->db->query("select account_tw_token,account_tw_secret,account_fbid,account_token,account_displayname from wooz_account_sosmed where id = $id");
        $hasil = $query->row();
        return $hasil;
    }

    function places($id) {
        $query = $this->db->query("select places_fb_caption,places_album,places_tw_caption from wooz_places where id = $id");
        $hasil = $query->row();
        return $hasil;
    }

    function userlanding($rfid, $places) {
        $query = $this->db->query("select account_id from wooz_landing where landing_rfid = $rfid and landing_register_form = $places order by id desc");
        $hasil = $query->row();
        return $hasil;
    }

    function usercek($id) {
        $query = $this->db->query("select id,account_rfid,account_displayname from wooz_account_sosmed where id = $id");
        $hasil = $query->row();
        return $hasil;
    }

    function usercekrfid($rfid) {
        $query = $this->db->query("select id,account_rfid,account_displayname from wooz_account_sosmed where account_rfid = $rfid");
        $hasil = $query->row();
        return $hasil;
    }
	
    function getphototag($name_file) {
        $result = array();
        $sql = "SELECT a.id,b.account_token,a.pid 
				FROM wooz_photos a inner join wooz_account_sosmed b on b.id = a.account_id 
				WHERE photo_upload =  '$name_file'";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
			$result = $data;
        }
        return $result;
    }
	
    function getphotoaccounttag($id) {
        $result = array();
        $sql = "SELECT account_fbid,account_token 
				FROM wooz_account_sosmed
				WHERE id =  '$id'";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
			$result = $data;
        }
        return $result;
    }

}

?>
