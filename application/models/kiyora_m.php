<?php

class kiyora_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
    }

    function getplaceslanding($custom_page) {
        $result = array();
        $sql = "SELECT id,places_landing,places_backgroud,places_logo,places_model,places_type_registration,"
                . "places_duedate,places_name,places_fbid,places_fb_page_id,places_twitter,places_tw_id,places_css,"
                . "places_avatar,places_cstatus_fb,places_desc "
                . "FROM wooz_places "
                . "WHERE places_landing = '$custom_page' order by id desc";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
            $a = strtotime($data->places_duedate);
            $b = time();
            if ($a > $b) {
                $result = $data;
            }
        }
        return $result;
    }

    function check_rfid() {
        $rfid = $this->input->post('rfid');
        $loc = $this->input->post('places');
        $sql = "SELECT id as account_id,account_username FROM wooz_account 
                where account_rfid = '" . $rfid . "'
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            #$this->checkin_point($hasil->account_id);
            return $hasil;
        } else {
            $sqls = "SELECT a.account_id,b.account_username FROM wooz_landing a "
                    . "left join wooz_account b on b.id = a.account_id "
                    . "where a.landing_rfid = '" . $rfid . "'";
            if ($loc == 1) {
                $sqls .= " and a.landing_register_form in (436)";
            } else {
                $sqls .= " and a.landing_register_form in (436)";
            }
            $sqls .= " order by a.id desc";
            $querys = $this->db->query($sqls);
            $hasils = $querys->row();
            if ($hasils) {
                #$this->checkin_point($hasils->account_id);
                return $hasils;
            } else {
                return false;
            }
        }
    }

    function get_data($id, $username) {
        $sql = "SELECT id,account_username,account_displayname,account_rfid from wooz_account 
                where id = " . $id . " and account_username = '" . $username . "'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            return $hasil;
        } else {
            return false;
        }
    }

    function cek_sampling($id, $loc) {
        if ($loc == 2) {
            $sql = "SELECT id from wooz_kiyora where account_id = " . $id . " and sampling_id = '2'";
        } else {
            $sql = "SELECT id from wooz_kiyora where account_id = " . $id . " and selling_id = '1'";
        }
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            return true;
        } else {
            return false;
        }
    }

    function update_sampling($id, $loc) {
        $input['account_id'] = $id;
        if ($loc == 2) {
            $input['sampling_id'] = 2;
        } else {
            $input['selling_id'] = 1;
        }
        $input['kode_sampling'] = 0;
        $input['lokasi'] = 1;
        $input['date_add'] = date('Y-m-d H:i:s');
        $this->db->insert('kiyora', $input);
        return true;
    }

    function sampling_save($id, $sampling) {
        $input['account_id'] = $id;
        $input['sampling_id'] = 2;
        $input['kode_sampling'] = $sampling;
        $input['lokasi'] = 1;
        $input['date_add'] = date('Y-m-d H:i:s');
        $this->db->insert('kiyora', $input);
        return true;
    }

    function selling_save($id, $buy) {
        $input['account_id'] = $id;
        $input['selling_id'] = 1;
        $input['selling_bottle'] = $buy;
        $input['lokasi'] = 1;
        $input['date_add'] = date('Y-m-d H:i:s');
        $this->db->insert('kiyora', $input);
        return true;
    }

    function redeem_save($id, $redeem) {
        $input['account_id'] = $id;
        $input['redeem_id'] = 3;
        $input['kode_redeem'] = $redeem;
        $input['lokasi'] = 1;
        $input['date_add'] = date('Y-m-d H:i:s');
        $this->db->insert('kiyora', $input);
        return true;
    }

    function cek_redeem($id) {
        $redeem = 0;
        $sql = "SELECT sum(selling_bottle) as total FROM `wooz_kiyora` WHERE account_id = " . $id . " and selling_id = 1";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil->total >= 2) {
            $redeem = 1;
            $sqlcek = "SELECT id FROM `wooz_kiyora` WHERE account_id = " . $id . " and redeem_id = 3";
            $querycek = $this->db->query($sqlcek);
            $hasilcek = $querycek->row();
            if ($hasilcek) {
                $redeem = 2;
            }
        }
        return $redeem;
    }

    function get_redeem($id) {
        $sql = "SELECT sum(selling_bottle) as total FROM `wooz_kiyora` WHERE account_id = " . $id . " and selling_id = 1";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil->total;
    }

    function cekuser($id, $nama) {
        $sql = "select a.id
                from wooz_account_kiyora as a  
		where a.account_email = '" . $nama . "'
                || a.account_displayname like '%" . $nama . "%' || account_phone = '" . $nama . "'";
        $query = $this->db->query($sql);
        $row = $query->row();
        if ($row) {
            return $row;
        } else {
            return false;
        }
    }

    function kiyorauser($account_id) {
        $sql = "SELECT a.account_displayname,a.account_email,a.account_phone,a.account_birthdate,a.account_profession,
                c.account_fbid,c.account_token,c.account_fb_name,c.account_twid,c.id,c.account_tw_username,
                c.account_tw_token,c.account_tw_secret,c.account_displayname as name, c.account_email as email,
                c.account_phone as phone,c.account_gender as gender,c.account_birthdate as birthdate,c.account_profession as pekerjaan
		FROM wooz_account_kiyora as a left join wooz_account as c on c.id = a.account_id
                WHERE a.id = " . $account_id . "";
        $query = $this->db->query($sql);
        $data = $query->row();
        
        if($data){
            return $data;
        }else{
            return false;
        }
    }
    
    function gettable($field,$data,$table){
        $data = $this->db->get_where($table, array($field => $data))->row();
        return $data;      
    }	
}
