<?php

class merchant_m extends CI_Model {

    function list_merchant() {
        $sql = "select id,account_displayname,account_email from wooz_merchant_shop order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function getuser($id){
        $sql = "select * from wooz_merchant_shop where id = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil; 
    }

    function nicename($file, $id = 0) {
        $healthy = array(" ", ".",",","?","&","(",")","#","!","^","%","\'","@","}","{","]","[","|","+","","'","/","|",":",'"',"<",">",";","--","+");
        $yummy   = array("-", "",);
        $result  = strtolower(str_replace($healthy, $yummy, trim($file)));
        $result = preg_replace('/[^;\sa-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ]+/u', '', $result);
        
        $this->db->select('');
        $this->db->where('account_username', $result);
        $this->db->where('id !=', $id);
        $this->db->where('account_status !=', 0);
        $this->db->from('account_pre');
        $c = $this->db->count_all_results();
        
        if($c) {
            $this->db->select('');
            $this->db->where('id !=', $id);
            $this->db->where('account_status !=', 0);
            $this->db->like('account_username', $result, 'after');
            $this->db->from('merchant_shop');
            $c = $this->db->count_all_results();
            
            if($c) {
                $this->db->select('');
                $this->db->where('id !=', $id);
                $this->db->where('account_status !=', 0);
                $this->db->like('account_username', $result, 'after');
                $this->db->order_by('id', 'desc');
                $this->db->limit(1, 0);
                $res = $this->db->get('merchant_shop')->row();
                
                $slice = explode('-', $res->account_username);
                $cc = count($slice);
                if(is_numeric($slice[$cc-1])) {
                    $slice[$cc-1] = $slice[$cc-1] + 1;
                    $jadi = implode('-', $slice);
                } else {
                    $jadi = $result.'-1';
                }
            } else {
                $jadi = $result.'-1';
            }
        } else {
            $jadi = $result;
        }
        return $jadi;
    }
}
