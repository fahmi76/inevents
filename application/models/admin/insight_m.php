<?php

class insight_m extends CI_Model {

    function list_checkin($gate,$check,$status) {       
        $sql = "select distinct(a.account_id), a.id,b.nama,c.account_rfid,c.account_displayname,c.account_avatar,d.log_stamps,d.log_check from wooz_seat_account a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
            . "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate1 = ".$gate." and d.log_gate = ".$gate." and d.log_status = ".$status." and d.log_stamps >= '2015-01-31 16:00:00' group by a.account_id order by a.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }

    function get_gate($id){
        $sql = "SELECT nama FROM `wooz_gate` where id = '".$id."' order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil->nama;
    }

    function list_check($gate,$check,$status){
        $sql = "SELECT distinct(account_id),account_displayname,log_stamps,log_check FROM `wooz_log_user_gate` a "
            ."inner join wooz_account b on b.id = a.account_id where log_gate = '".$gate."' and log_status = '".$status."' "
            ."and log_stamps >= '2015-01-31 16:00:00' group by account_id";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }

    function list_arrive() {       
        $sql = "SELECT distinct(account_id),account_displayname,log_stamps,log_check FROM `wooz_log_user_gate` a "
            ."inner join wooz_account b on b.id = a.account_id where log_stamps >= '2015-01-31 16:00:00' group by account_id";
       
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }
    
    function list_gate(){
        $sql = "SELECT id,nama FROM `wooz_gate` order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function log_event(){
        $sql = "SELECT places_id,count(distinct(account_id)) as total_account,count(id) as total "
                . "FROM `wooz_log_user` where places_id in (10,14)  group by places_id order by places_id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
	
    function logcheckinstaff($id){
        $sql = "SELECT count(distinct(account_id)) as total_account "
                . "FROM `wooz_log_user_gate` where log_gate = '".$id."' and log_status = 1 and log_check = 1 and log_stamps >= '2015-01-31 16:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
	
    function logcheckoutstaff($id){
        $sql = "SELECT count(distinct(account_id)) as total_account "
                . "FROM `wooz_log_user_gate` where log_gate = '".$id."' and log_status = 1 and log_check = 2 and log_stamps >= '2015-01-31 16:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function arrive_event(){
        $sql = "SELECT count(distinct(account_id)) as total_account FROM `wooz_log_user_gate` where log_stamps >= '2015-01-31 16:00:00' ";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
        
    }
    
    function hourregtotal($rowplaces,$rowdate,$table,$places){
        $this->db->select('DISTINCT(hour(date_add)) AS thedate,'.$places.', count(distinct(account_id)) as total');
        $this->db->where($places, $rowplaces);
        $this->db->where("HOUR(date_add)",$rowdate);
        $this->db->from($table);
        $this->db->group_by("thedate"); 
        $this->db->order_by("thedate", "asc");
        $total = $this->db->get()->row();
        return $total;
    }
	
    function check_hour($gate,$check,$status){
        $this->db->select('DISTINCT(hour(log_stamps)) AS thedate, count(distinct(account_id)) as total');
        $this->db->where("log_gate", $gate);
        $this->db->where("log_check", $check);
        $this->db->where("log_status", $status);
		$this->db->where("log_stamps >= '2015-01-31 16:00:00' ");
        $this->db->from('log_user_gate');
		$this->db->group_by("thedate"); 
		$this->db->order_by("thedate", "asc");
        $total = $this->db->get()->result();
        return $total;
    }
	
    function get_pre(){
        $sql = "SELECT count(id) as total "
                . "FROM `wooz_account_merchant`";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function regissosmed(){
        $sql = "SELECT count(id) as total FROM `wooz_account_merchant` where account_fbid is not null or account_twid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_fb(){
        $sql = "SELECT count(id) as total,sum(account_fb_friends) as friends "
                . "FROM `wooz_account_merchant` where account_fbid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_tw(){
        $sql = "SELECT count(id) as total,sum(account_tw_follow) as friends "
                . "FROM `wooz_account_merchant` where account_twid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
     

}
