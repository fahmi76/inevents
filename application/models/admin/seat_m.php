<?php

class seat_m extends CI_Model {

    function list_visitor() {
        $sql = "SELECT a.id,b.account_displayname, a.seat FROM `wooz_seat_account` a inner join wooz_account b on b.id = a.account_id";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function list_gate() {
        $sql = "select id,nama,data_status from wooz_gate order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_gate($id){
        $sql = "select * from wooz_gate where id = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil; 
    }


    function getuser($id){
        $sql = "select * from wooz_visitor where id = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;        
    }
    
    function list_rfid(){
        $sql = "select a.id,b.nama,a.rfid,c.account_displayname from wooz_visitor_rfid a inner join wooz_visitor b on b.id = a.visitor_id "
            . "left join wooz_account_merchant c on c.id = a.account_id order by a.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function get_rfid($id){
        $sql = "select * from wooz_visitor_rfid where id = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;    
    }
	
	function list_vendor(){
        $sql = "select a.id,b.nama,a.rfid,c.account_displayname,c.account_groups,c.account_avatar,d.log_stamps,d.log_check from wooz_visitor_rfid a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
            . "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate = 11 and d.log_status = 1 and d.log_stamps >= '2015-01-31 16:00:00' order by a.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;   
	}
	function list_backstage(){
        $sql = "select a.id,b.nama,a.rfid,c.account_displayname,c.account_groups,c.account_avatar,d.log_stamps,d.log_check from wooz_visitor_rfid a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
            . "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate = 10 and d.log_status = 1 and d.log_stamps >= '2015-01-31 16:00:00' order by a.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;   
	}
	function list_vendor_fail(){
        $sql = "select a.id,b.nama,a.rfid,c.account_displayname,c.account_groups,c.account_avatar,d.log_stamps,d.log_check from wooz_visitor_rfid a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
            . "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate = 11 and d.log_status = 2 and d.log_stamps >= '2015-01-31 16:00:00' order by a.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;   
	}
	function list_backstage_fail(){
        $sql = "select a.id,b.nama,a.rfid,c.account_displayname,c.account_groups,c.account_avatar,d.log_stamps,d.log_check from wooz_visitor_rfid a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
            . "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate = 10 and d.log_status = 2 and d.log_stamps >= '2015-01-31 16:00:00' order by a.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;   
	}
}
