<?php

class user_m extends CI_Model {

    function list_user() {
        $sql = "select a.id,a.account_displayname,a.account_avatar,d.nama,a.account_status,a.account_rfid,c.seat,d.nama
                from wooz_account a left join wooz_seat_account c on c.account_id = a.id 
                left join wooz_visitor d on d.id = c.visitor_id order by a.id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function getuser($id){
        $sql = "select a.id,a.account_displayname,a.account_avatar,d.nama,a.account_status,a.account_rfid,c.seat,c.visitor_id,d.nama
                from wooz_account a left join wooz_seat_account c on c.account_id = a.id 
                left join wooz_visitor d on d.id = c.visitor_id where a.id = ".$id." order by a.id asc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;   
    }

    function user_win($id){
        $sql = "select id from wooz_winner_account where account_id = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }
    
    function nicename($file, $id = 0) {
        $healthy = array(" ", ".",",","?","&","(",")","#","!","^","%","\'","@","}","{","]","[","|","+","","'","/","|",":",'"',"<",">",";","--","+");
        $yummy   = array("-", "",);
        $result	 = strtolower(str_replace($healthy, $yummy, trim($file)));
        $result = preg_replace('/[^;\sa-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ]+/u', '', $result);
        
        $this->db->select('');
        $this->db->where('account_username', $result);
        $this->db->where('id !=', $id);
        $this->db->where('account_status !=', 0);
        $this->db->from('account');
        $c = $this->db->count_all_results();
        
        if($c) {
            $this->db->select('');
            $this->db->where('id !=', $id);
            $this->db->where('account_status !=', 0);
            $this->db->like('account_username', $result, 'after');
            $this->db->from('account');
            $c = $this->db->count_all_results();
            
            if($c) {
                $this->db->select('');
                $this->db->where('id !=', $id);
                $this->db->where('account_status !=', 0);
                $this->db->like('account_username', $result, 'after');
                $this->db->order_by('id', 'desc');
                $this->db->limit(1, 0);
                $res = $this->db->get('account')->row();
                
                $slice = explode('-', $res->account_username);
                $cc = count($slice);
                if(is_numeric($slice[$cc-1])) {
                    $slice[$cc-1] = $slice[$cc-1] + 1;
                    $jadi = implode('-', $slice);
                } else {
                    $jadi = $result.'-1';
                }
            } else {
                $jadi = $result.'-1';
            }
        } else {
            $jadi = $result;
        }
        return $jadi;
    }

}
