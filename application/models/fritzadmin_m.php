<?php

class fritzadmin_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function cek_promo(){        
        $sql = "SELECT * FROM wooz_magnum_promo_available
                order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
	function get_total_regis(){   
        $sql = "SELECT count(distinct(account_id)) as total FROM `wooz_landing` 
				where landing_register_form = 453 and account_id not in (14334,12209) and CONVERT_TZ(landing_joindate,'+00:00','-06:00') >= '2014-05-21 00:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil->total;
	}
	
	function get_total_regis_byday(){   
        $sql = "SELECT distinct(DATE_FORMAT(CONVERT_TZ(landing_joindate,'+00:00','-06:00'),'%d %M %Y')) as dates, count(distinct(account_id)) as total 
				FROM `wooz_landing` where landing_register_form = 453 and account_id not in (14334,12209) and CONVERT_TZ(landing_joindate,'+00:00','-06:00') >= '2014-05-21 00:00:00' 
				group by dates";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
	}
	
	function get_total_regis_total(){   
        $sql = "SELECT distinct(a.account_id),CONVERT_TZ(a.landing_joindate,'+00:00','-06:00') as newdate, 
				b.account_displayname,b.account_email,b.account_birthdate,b.account_gender,b.account_profession,
				b.account_profession_other,b.account_sport,b.account_sport_other
				FROM wooz_landing a inner join wooz_account b on b.id = a.account_id
				WHERE a.landing_register_form = 453 and account_id not in (14334,12209) and CONVERT_TZ(a.landing_joindate,'+00:00','-06:00') >= '2014-05-21 00:00:00' 
				group by a.account_id";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
	}
	
	function get_total_regis_byday_search($dates){   
        $sql = "SELECT distinct(a.account_id),CONVERT_TZ(a.landing_joindate,'+00:00','-06:00') as newdate, 
				b.account_displayname,b.account_email,b.account_birthdate,b.account_gender,b.account_profession,
				b.account_profession_other,b.account_sport,b.account_sport_other
				FROM wooz_landing a inner join wooz_account b on b.id = a.account_id
				WHERE a.landing_register_form = 453 and account_id not in (14334,12209) and CONVERT_TZ(a.landing_joindate,'+00:00','-06:00') like '%".$dates."%' 
				group by a.account_id";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
	}
	    
}
