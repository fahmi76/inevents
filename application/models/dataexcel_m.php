<?php

class dataexcel_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function searchhistory($sdate, $edate) {
        $sql = "SELECT distinct(c.account_id),a.account_fbid,a.account_twid,a.account_joindate,a.account_displayname,a.account_email,
		       a.account_phone,a.account_birthdate,a.account_gender,b.nasabah_niaga,c.landing_joindate
                FROM wooz_account a left join wooz_niaga_data b on b.account_id = a.id left join wooz_landing c on c.account_id = a.id
                WHERE c.landing_register_form = 413 and";
        if ($sdate != 0 && $edate != 0) {
            $sql .= " c.landing_joindate >= '$sdate 00:00:00' and c.landing_joindate < '$edate 00:00:00' ";
        } elseif ($sdate != 0 || $edate != 0) {
            if ($sdate != 0) {
                $sql .= " c.landing_joindate >= '$sdate 00:00:00' ";
            } else {
                $sql .= " c.landing_joindate < '$edate 00:00:00' ";
            }
        } else {
            $sql .= "";
        }
        $sql .= "group by c.account_id order by c.landing_joindate asc";

        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    function searchhistorynonreg($sdate, $edate) {
        $sql = "SELECT a.account_fbid,a.account_twid,a.account_joindate,a.account_displayname,a.account_email,
		       a.account_phone,a.account_birthdate,a.account_gender,b.nasabah_niaga
                FROM wooz_account a left join wooz_niaga_data b on b.account_id = a.id WHERE account_rfid = 0  and";
        if ($sdate != 0 && $edate != 0) {
            $sql .= " account_joindate >= '$sdate 00:00:00' and account_joindate < '$edate 00:00:00' ";
        } elseif ($sdate != 0 || $edate != 0) {
            if ($sdate != 0) {
                $sql .= " account_joindate1 >= '$sdate 00:00:00' ";
            } else {
                $sql .= " account_joindate < '$edate 00:00:00' ";
            }
        } else {
            $sql .= "";
        }
        $sql .= "order by account_joindate asc";

        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    function searchhistoryregulang($sdate, $edate) {
        $sql = "SELECT a.account_fbid,a.account_twid,a.account_joindate,a.account_displayname,a.account_email,
		       a.account_phone,a.account_birthdate,a.account_gender,b.nasabah_niaga
                FROM wooz_account a left join wooz_niaga_data b on b.account_id = a.id WHERE account_rfid != 0 and";
        if ($sdate != 0 && $edate != 0) {
            $sql .= " account_joindate >= '$sdate 00:00:00' and account_joindate < '$edate 00:00:00' ";
        } elseif ($sdate != 0 || $edate != 0) {
            if ($sdate != 0) {
                $sql .= " account_joindate1 >= '$sdate 00:00:00' ";
            } else {
                $sql .= " account_joindate < '$edate 00:00:00' ";
            }
        } else {
            $sql .= "";
        }
        $sql .= "order by account_joindate asc";

        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function totalsearchhistory($sdate, $edate) {
        $sql = "SELECT *
                FROM wooz_account WHERE ";
        if ($sdate != 0 && $edate != 0) {
            $sql .= " account_joindate >= '$sdate 00:00:00' and account_joindate < '$edate 00:00:00' ";
        } elseif ($sdate != 0 || $edate != 0) {
            if ($sdate != 0) {
                $sql .= " account_joindate >= '$sdate 00:00:00' ";
            } else {
                $sql .= " account_joindate < '$edate 00:00:00' ";
            }
        } else {
            $sql .= "";
        }
        $sql .= "order by account_joindate asc";

        $query = $this->db->query($sql);
        $hasil = $query->num_rows();
        return $hasil;
    }
    
    function fieldname($sdate, $edate) {
        $sql = "SELECT a.account_fbid,a.account_twid,a.account_joindate,a.account_displayname,a.account_email,
		       a.account_phone,a.account_birthdate,a.account_gender,b.nasabah_niaga
                FROM wooz_account a left join wooz_niaga_data b on b.account_id = a.id WHERE ";
        if ($sdate != 0 && $edate != 0) {
            $sql .= " account_joindate >= '$sdate 00:00:00' and account_joindate < '$edate 00:00:00' ";
        } elseif ($sdate != 0 || $edate != 0) {
            if ($sdate != 0) {
                $sql .= " account_joindate1 >= '$sdate 00:00:00' ";
            } else {
                $sql .= " account_joindate < '$edate 00:00:00' ";
            }
        } else {
            $sql .= "";
        }
        $sql .= "order by account_joindate asc";

        $query = $this->db->query($sql);
        $hasil = $query->list_fields();
        return $hasil;
    }

    function searchloghistory($barang, $tgl) {
        $sql = "SELECT distinct(b.account_id),places_id,a.account_displayname,a.account_email,a.account_gender,
				c.places_name,b.log_date
                FROM wooz_account as a LEFT JOIN wooz_log as b ON b.account_id = a.id 
				LEFT JOIN wooz_places as c ON c.id = b.places_id ";
        if ($barang != 0 && $tgl != 0) {
            $sql .= "WHERE b.places_id = $barang and b.log_date like '%2013-09-$tgl%' and account_id not in (14334,3179,25508) ";
        } elseif ($barang != 0 || $tgl != 0) {
            if ($barang != 0) {
                $sql .= "WHERE b.places_id = $barang and account_id not in (14334,3179,25508) ";
            } else {
                $sql .= "WHERE b.log_date like '%2013-09-$tgl%' and b.places_id in (370,371,372,373) and account_id not in (14334,3179,25508) ";
            }
        } else {
            $sql .= "b.places_id in (370,371,372,373) and account_id not in (14334,3179,25508) ";
        }
        $sql .= "order by b.places_id asc";

        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function searchreghistory($tgl) {
        $sql = "SELECT distinct(b.account_id),a.account_displayname,a.account_email,a.account_gender,
				b.landing_joindate
                FROM wooz_account as a LEFT JOIN wooz_landing as b ON b.account_id = a.id ";
        if ($tgl != 0) {
            $sql .= "WHERE b.landing_joindate like '%2013-09-$tgl%' and b.landing_register_form = 369 and b.account_id not in (14334,3179,25508) ";
        } else {
            $sql .= "b.landing_register_form = 369 and account_id not in (14334,3179,25508) ";
        }
        $sql .= "GROUP BY account_id order by b.id asc";

        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function barang() {
        $sql = "SELECT *
                FROM wooz_redeem_barang 
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function statistikregis() {
        $sql = "SELECT DISTINCT(DATE_FORMAT(`landing_joindate`, '%d %M %Y')) AS thedate, 
            count(distinct(`account_id`)) as total FROM (`wooz_landing`) 
            WHERE `landing_joindate` BETWEEN '2013-09-19' AND '2013-09-29' and landing_register_form = 369
            GROUP BY `thedate` ORDER BY `thedate` asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function statistikuserredeem() {
        $sql = "SELECT DISTINCT(DATE_FORMAT(`date_add`, '%d %M %Y')) AS thedate, 
            sum(case when `redeem_id` = 1 then 1 else 0 end) gloves,
                    sum(case when `redeem_id` = 2 then 1 else 0 end) topi,
                    sum(case when `redeem_id` = 3 then 1 else 0 end) tshirt,
                    sum(case when `redeem_id` = 4 then 1 else 0 end) ballpoint,
                    count(distinct(account_id)) as total
           FROM (`wooz_redeem`) 
            WHERE `date_add` BETWEEN '2013-09-19' AND '2013-09-29' 
            GROUP BY `thedate` ORDER BY `thedate` asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function getmotor($id) {
        $sql = "select merk_motor from wooz_merk_motor
                where id = '" . $id . "'";
        $query = $this->db->query($sql);
        $row = $query->row();
        if ($row) {
            return $row->merk_motor;
        } else {
            $data = "kosong";
            return $data;
        }
    }

    function totalregis() {
        $sql1 = 'SELECT distinct(account_id) FROM `wooz_landing`
				where landing_register_form = 413 GROUP BY account_id';
        $query1 = $this->db->query($sql1);
        $data1 = $query1->result();
        return count($data1);
    }

    function totalregisulang() {
        $sql = 'SELECT count(id) as total FROM `wooz_account` WHERE account_rfid != 0 ';
        $query = $this->db->query($sql);
        $data = $query->row();
        return $data->total;
    }

    function totalnonregisulang() {
        $sql = 'SELECT count(id) as total FROM `wooz_account` WHERE account_rfid = 0 ';
        $query = $this->db->query($sql);
        $data = $query->row();
        return $data->total;
    }
	
    function userphoto(){        
        $sql = 'SELECT a.`account_id` ,b.account_fb_id,b.account_tw_id,b.account_joindate,b.account_displayname,
                b.account_email,b.account_address,b.account_kota,b.account_propinsi,b.account_sim,
                b.account_religion,b.account_phone,b.account_birthdate_places,b.account_birthdate,b.account_gender,
                b.account_golongan_darah,b.account_nama_club,b.account_kota_club,b.account_merk_motor,
                b.account_merk_motor_lain,b.account_tipe_motor,b.account_nomor_rangka,b.account_rfid, 
                count( a.`account_id` ) AS total
                FROM wooz_photos a left join wooz_account b on b.id = a.account_id
                GROUP BY a.`account_id`
                HAVING total >=1
                ORDER BY total DESC';
        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;        
    }

    function totalphoto(){        
        $sql = 'select count(id) as total from wooz_photos';
        $query = $this->db->query($sql);
        $data = $query->row();
        return $data->total;
    }

    function totaluserphoto(){        
        $sql = 'select count(distinct(account_id)) as total from wooz_photos ';
        $query = $this->db->query($sql);
        $data = $query->row();
        return $data->total;
    }

}

?>
