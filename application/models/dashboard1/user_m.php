<?php

class user_m extends CI_Model {

    function list_user() {
        $sql = "select a.id,a.account_displayname,a.account_avatar,a.account_status,b.account_displayname as admin "
                . "from wooz_account_pre a inner join wooz_admin b on b.id = a.admin_id order by a.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function getuser($id){
        $sql = "select * from wooz_account_pre where id = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
        
    }
    
    function nicename($file, $id = 0) {
        $healthy = array(" ", ".",",","?","&","(",")","#","!","^","%","\'","@","}","{","]","[","|","+","","'","/","|",":",'"',"<",">",";","--","+");
        $yummy   = array("-", "",);
        $result	 = strtolower(str_replace($healthy, $yummy, trim($file)));
        $result = preg_replace('/[^;\sa-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ]+/u', '', $result);
        
        $this->db->select('');
        $this->db->where('account_username', $result);
        $this->db->where('id !=', $id);
        $this->db->where('account_status !=', 0);
        $this->db->from('account_pre');
        $c = $this->db->count_all_results();
        
        if($c) {
            $this->db->select('');
            $this->db->where('id !=', $id);
            $this->db->where('account_status !=', 0);
            $this->db->like('account_username', $result, 'after');
            $this->db->from('account_pre');
            $c = $this->db->count_all_results();
            
            if($c) {
                $this->db->select('');
                $this->db->where('id !=', $id);
                $this->db->where('account_status !=', 0);
                $this->db->like('account_username', $result, 'after');
                $this->db->order_by('id', 'desc');
                $this->db->limit(1, 0);
                $res = $this->db->get('account_pre')->row();
                
                $slice = explode('-', $res->account_username);
                $cc = count($slice);
                if(is_numeric($slice[$cc-1])) {
                    $slice[$cc-1] = $slice[$cc-1] + 1;
                    $jadi = implode('-', $slice);
                } else {
                    $jadi = $result.'-1';
                }
            } else {
                $jadi = $result.'-1';
            }
        } else {
            $jadi = $result;
        }
        return $jadi;
    }

}
