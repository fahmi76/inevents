<?php

class insight_m extends CI_Model {

    function list_checkin() {        
        $sql = "SELECT a.account_id,b.account_displayname,b.account_avatar,b.account_fbid,"
                . "b.account_tw_username,b.account_email,count(account_id) as total "
                . "FROM `wooz_log_user` a inner join wooz_account_pre b on b.id = a.account_id "
                . "WHERE a.places_id in (13,11,12) group by a.account_id having total >= 3 "
                . "order by b.account_displayname";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function log_event(){
        $sql = "SELECT places_id,count(distinct(account_id)) as total_account,count(id) as total "
                . "FROM `wooz_log_user` where places_id in (10,14)  group by places_id order by places_id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
	
    function logcheckin(){
        $sql = "SELECT count(distinct(account_id)) as total_account,count(id) as total "
                . "FROM `wooz_log_user` where places_id in (11,12,13)";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
	
    function arrive_event_excel(){
        $sql = "SELECT distinct(a.account_id),b.account_displayname,b.account_avatar,b.account_fbid,"
                . "b.account_tw_username,b.account_email "
                . "FROM `wooz_log_user` a inner join wooz_account_pre b on b.id = a.account_id "
                . "group by a.account_id order by b.account_displayname";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
        
    }
    
    function arrive_event(){
        $sql = "SELECT count(distinct(account_id)) as total_account FROM `wooz_log_user`";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
        
    }
    
    function get_pre(){
        $sql = "SELECT count(id) as total "
                . "FROM `wooz_account_pre`";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function regissosmed(){
        $sql = "SELECT count(id) as total FROM `wooz_account_pre` where account_fbid is not null or account_twid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_fb(){
        $sql = "SELECT count(id) as total,sum(account_fb_friends) as friends "
                . "FROM `wooz_account_pre` where account_fbid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_tw(){
        $sql = "SELECT count(id) as total,sum(account_tw_follow) as friends "
                . "FROM `wooz_account_pre` where account_twid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
     

}
