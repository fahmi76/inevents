<?php

class guinness_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
    }

    function getplaceslanding($custom_page) {
        $result = array();
        $sql = "SELECT id,places_landing,places_backgroud,places_logo,places_model,places_type_registration,"
                . "places_duedate,places_name,places_fbid,places_twitter,places_tw_id,places_css,"
                . "places_avatar,places_cstatus_fb,places_desc "
                . "FROM wooz_places "
                . "WHERE places_landing = '$custom_page' order by id desc";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
            $a = strtotime($data->places_duedate);
            $b = time();
            if ($a > $b) {
                $result = $data;
            }
        }
        return $result;
    }

    function check_rfid() {
        $rfid = $this->input->post('rfid');
        $loc = $this->input->post('places');
        $sql = "SELECT id as account_id,account_username FROM wooz_account 
                where account_rfid = '" . $rfid . "'
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            #$this->checkin_point($hasil->account_id);
            return $hasil;
        } else {
            $sqls = "SELECT a.account_id,b.account_username FROM wooz_landing a "
                    . "left join wooz_account b on b.id = a.account_id "
                    . "where a.landing_rfid = '" . $rfid . "'";
            if ($loc == 1) {
                $sqls .= " and a.landing_register_form in (436)";
            } else {
                $sqls .= " and a.landing_register_form in (436)";
            }
            $sqls .= " order by a.id desc";
            $querys = $this->db->query($sqls);
            $hasils = $querys->row();
            if ($hasils) {
                #$this->checkin_point($hasils->account_id);
                return $hasils;
            } else {
                return false;
            }
        }
    }

    function get_data($id, $username) {
        $sql = "SELECT id,account_username,account_displayname,account_rfid from wooz_account 
                where id = " . $id . " and account_username = '" . $username . "'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            return $hasil;
        } else {
            return false;
        }
    }

    function check_birthday($id, $username) {
        $date = date('Y-m-d');
        $sql = "SELECT id FROM wooz_account WHERE 
                DATE_FORMAT(`account_birthdate`,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d')
                and id = " . $id . " and account_username = '" . $username . "'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            $sqlcektrans = "select id from wooz_guinness_point"
                    . " where account_id = " . $id . " and redeem_id = 3 and date_add like '%" . $date . "%'"
                    . "order by id desc";
            $querycektrans = $this->db->query($sqlcektrans);
            $hasilcektrans = $querycektrans->row();
            if ($hasilcektrans) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 0;
        }
    }

    function check_promo() {
        $sql = "SELECT * FROM wooz_magnum_promo_available";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        if ($hasil) {
            return $hasil;
        } else {
            return false;
        }
    }

    function transaksi($data, $loc) {
        $nominal = $this->input->post('transaksi');
        $kode = $this->input->post('kodetrans');
        $point = $nominal / 5000;
        $input['account_id'] = $data->id;
        $input['lokasi'] = $loc;
        $input['nominal_transaksi'] = $nominal;
        $input['kode_transaksi'] = $kode;
        $input['jumlah_point'] = $point;
        $cek = $this->cek_transaksi($kode);
        if ($cek) {
            $this->db->where('id', $cek);
            $this->db->update('magnum_transaksi', $input);
        } else {
            $this->db->insert('magnum_transaksi', $input);
            $cek = $this->db->insert_id();
        }
        $this->cek_point_transaksi($data->id, $input, $cek, 4);
        return true;
    }

    function cek_point_transaksi($account_id, $input, $cek, $transaksi) {
        $db['account_id'] = $account_id;
        $db['transaksi_id'] = $transaksi;
        $db['kode_transaksi'] = $cek;
        $db['lokasi'] = $input['lokasi'];
        $db['redeem_point'] = $input['jumlah_point'];

        $sqlcektrans = "select * from wooz_guinness_point"
                . " where account_id = " . $account_id . " and kode_transaksi =" . $cek . " "
                . "order by id desc";
        $querycektrans = $this->db->query($sqlcektrans);
        $hasilcektrans = $querycektrans->row();
        if ($hasilcektrans) {
            if ($hasilcektrans->total_point == $input['jumlah_point']) {
                $db['point_awal'] = $hasilcektrans->point_awal;
                $db['total_point'] = $hasilcektrans->total_point;
            } else {
                $db['point_awal'] = $hasilcektrans->point_awal;
                $db['total_point'] = $hasilcektrans->total_point - $hasilcektrans->redeem_point + $input['jumlah_point'];
            }
            $this->db->where('id', $hasilcektrans->id);
            $this->db->update('guinness_point', $db);
        } else {
            $sqlcekpoint = "select * from wooz_guinness_point where account_id = " . $account_id . " "
                    . "order by id desc";
            $querycekpoint = $this->db->query($sqlcekpoint);
            $hasilcekpoint = $querycekpoint->row();

            if ($hasilcekpoint) {
                $db['point_awal'] = $hasilcekpoint->total_point;
                $db['total_point'] = $hasilcekpoint->total_point + $input['jumlah_point'];
            } else {
                $db['point_awal'] = 0;
                $db['total_point'] = $input['jumlah_point'];
            }
            $this->db->insert('guinness_point', $db);
        }
    }

    function cek_transaksi($kode) {
        $sql = "SELECT id FROM wooz_magnum_transaksi WHERE kode_transaksi = '" . $kode . "'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            return $hasil->id;
        } else {
            return false;
        }
    }

    function get_promo($id, $redeem, $data, $loc) {
        $hasil = $this->get_promo_data($redeem, $id);
        $total_point = $this->get_point($data->id, $loc);
        #$redeem = $this->insert_redeem($data->id, $loc, $total_point, $hasil, $redeem);
        $db['account_id'] = $data->id;
        $db['redeem_id'] = $hasil->id;
        $db['kode_redeem'] = $hasil->id;
        $db['transaksi_id'] = 3;
        $db['kode_transaksi'] = 3;
        $db['date_add'] = date('Y-m-d H:i:s');
        if ($redeem == 1) {
            if ($total_point >= $hasil->point) {
                $db['redeem_point'] = $hasil->point;
                $point_redeem = $hasil->point;
            } else {
                return false;
            }
        } else {
            $db['redeem_point'] = 0;
            $point_redeem = 0;
        }
        $db['lokasi'] = $loc;

        $sqlcekpoint = "select * from wooz_guinness_point where account_id = " . $data->id . " "
                . "order by id desc";
        $querycekpoint = $this->db->query($sqlcekpoint);
        $hasilcekpoint = $querycekpoint->row();

        if ($hasilcekpoint) {
            $db['point_awal'] = $hasilcekpoint->total_point;
            $db['total_point'] = $hasilcekpoint->total_point - $point_redeem;
        } else {
            $db['point_awal'] = 0;
            $db['total_point'] = 0;
        }
        $this->db->insert('guinness_point', $db);

        $inputbarang['total_barang'] = $hasil->total - 1;
        $this->db->where('id', $id);
        $this->db->update('guinness_promo', $inputbarang);
        return true;
    }

    function get_point($account_id, $loc) {
        $sql = "SELECT total_point FROM wooz_guinness_point "
                . "WHERE account_id = '" . $account_id . "' order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            return $hasil->total_point;
        } else {
            return 0;
        }
    }

    function get_promo_data($redeem, $id) {
        switch ($redeem) {
            case 1:
                $sql = "SELECT id,nama_promo as promo, point_promo as point, total_barang as total "
                        . "FROM wooz_guinness_promo WHERE status = 1 and id = '" . $id . "' order by id desc";
                break;
            case 2:
                $sql = "SELECT id,nama_discount as promo, point_discount as point, total_barang as total "
                        . "FROM wooz_magnum_discount WHERE status = 1 and id = '" . $id . "' order by id desc";
                break;
            case 3:
                $sql = "SELECT id,nama_promo as promo, point_promo as point, total_barang as total "
                        . "FROM wooz_magnum_promo_birthday WHERE status = 1 and id = '" . $id . "' order by id desc";
                break;
            default:
                return false;
        }
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

    function get_promo_one($redeem, $data, $loc) {
        $sql = "SELECT id,nama_promo as promo, point_promo as point "
                . "FROM wooz_guinness_promo WHERE status = 1 and total_barang != 0 order by point_promo asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function checkin_point($id) {
        $date = date('Y-m-d');
        $sqlcekpoint = "select * from wooz_guinness_point where account_id = " . $id . " "
                . "and date_add like '%" . $date . "%' and transaksi_id = 2 order by id desc";
        $querycekpoint = $this->db->query($sqlcekpoint);
        $hasilcekpoint = $querycekpoint->row();
        if (!$hasilcekpoint) {
            $db['account_id'] = $id;
            $db['transaksi_id'] = 2;
            $db['kode_transaksi'] = 2;
            $db['lokasi'] = 1;
            $db['redeem_point'] = 0;
            $db['date_add'] = date('Y-m-d H:i:s');
            $sqlcekpoint = "select * from wooz_guinness_point where account_id = " . $id . " "
                    . "order by id desc";
            $querycekpoint = $this->db->query($sqlcekpoint);
            $hasilcekpoint = $querycekpoint->row();

            if ($hasilcekpoint) {
                $db['point_awal'] = $hasilcekpoint->total_point;
                $db['total_point'] = $hasilcekpoint->total_point + 10;
            } else {
                $db['point_awal'] = 0;
                $db['total_point'] = 10;
            }
            $this->db->insert('guinness_point', $db);
            return true;
        } else {
            return false;
        }
    }

    function point_purchase($id,$pointid) {
        $db['account_id'] = $id;
        $db['transaksi_id'] = 4;
        $db['kode_transaksi'] = 4;
        $db['lokasi'] = 1;
        $db['redeem_point'] = 0;
        $db['date_add'] = date('Y-m-d H:i:s');
        $sqlcekpoint = "select * from wooz_guinness_point where account_id = " . $id . " "
                . "order by id desc";
        $querycekpoint = $this->db->query($sqlcekpoint);
        $hasilcekpoint = $querycekpoint->row();
        if ($hasilcekpoint) {
            $db['point_awal'] = $hasilcekpoint->total_point;
            $db['total_point'] = $hasilcekpoint->total_point + $pointid;
        } else {
            $db['point_awal'] = 0;
            $db['total_point'] = $pointid;
        }
        $this->db->insert('guinness_point', $db);
    }

	function cek_log_status($id,$places){
        $sqlcekpoint = "select * from wooz_log where account_id = " . $id . " and log_date = '".date('Y-m-d')."' and places_id = '".$places."'"
                . "order by id desc";
        $querycekpoint = $this->db->query($sqlcekpoint);
        $hasilcekpoint = $querycekpoint->row();
		if($hasilcekpoint){
			return false;
		}else{
			return true;
		}
	
	}
	
}
