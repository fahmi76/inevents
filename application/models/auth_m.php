<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of auth_m
 *
 * @author ubaidz
 */
class auth_m extends CI_Model{
    
    function __construct(){
            parent::__construct();
            $this->load->helper('cookie');
    }
    
    function insert($data,$table){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    
    function delete($data,$table){
        $this->db->where('id', $data);
        $this->db->delete($table); 
        return true;
    }
    
    function getTable($data,$table){
        $data = $this->db->get_where($table, array('id' => $data, 'account_status' => 1))->row();
        return $data;  
    }
    
    function loginall($user_mail,$password){
        $this->db->where("(account_email = '$user_mail' OR account_username = '$user_mail') AND account_passwd = '$password' ");
        $count = $this->db->get('account')->row();  
        return $count;
    }
    
    function loginemail($user_mail){
        $this->db->where("(account_email = '$user_mail' OR account_username = '$user_mail') AND account_status = 1 ");
        $counts = $this->db->get('account')->row();
        return $counts;
    }
    
    function update($id,$data){
        $this->db->where('id', $id);
        $update = $this->db->update('account', $data);
        return $update;
    }
    
    function cekavailable($id,$data){
        $this->db->where($id, $data);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('account')->row();
        return $new;
    }
    
    function getwhere($id,$data,$table){
        $data = $this->db->get_where($table, array($id => $data))->row();
        return $data;  
    }
    
    function wherename($result){
        $this->db->select('');
        $this->db->where('account_username', $result);
        $this->db->from('account');
        $c = $this->db->count_all_results();
        return $c;
    }

    function likename($result){
        $this->db->select('');
        $this->db->like('account_username', $result, 'after');
        $this->db->from('account');
        $c = $this->db->count_all_results();
        return $c;
    }    
    
    function likerowname($result){
        $this->db->select('');
        $this->db->like('account_username', $result, 'after');
        $this->db->order_by('id', 'desc');
        $this->db->limit(1, 0);
        $res = $this->db->get('account')->row();
        return $res;
    }      
    
    function loginfound($user,$pass,$id){
        $login = $this->input->cookie('login');   
        $this->db->where("(account_email = '$user' OR account_username = '$user') AND account_passwd = '$pass' ");
        $data = $this->db->get('account')->row();
        if($data){
            $datas = array('benar');
            $datas[] = $data;
            return $datas;
        }else{
            $date = date('Y-m-d');
            $datetime = date('Y-m-d H:i:s');
            $this->db->like('date', $date);
            $this->db->where('account_id',$id);
            $this->db->where('log_cookie',1);
            $this->db->from('login_log');
            $count = $this->db->count_all_results();
            if($count % 3 == 0 && $count != 0){
                $datas = array('block');
                return $datas;
            }else{
                $data = array('lanjut');
                #$cookie = array('account_id' => $id, 'log_cookie' => 1, 'date' => $datetime );
                #$this->db->insert('login_log', $cookie);
                return $datas;
            }            
        }
    }
    
}

?>
