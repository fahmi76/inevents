<?php
class mobile_db extends CI_model {
	function __construct(){
		parent::__construct();
	}
	
	function list_pin($id){
		$this->db->select('');
		$this->db->where('log.account_id', $id);
		$this->db->where('log.log_status', 1);
		$this->db->where('log.log_type', 2);
		$this->db->join('account', 'account.id = log.account_id', 'left');
		$this->db->join('places', 'places.id = log.places_id', 'left');
		$this->db->join('badge', 'badge.id = log.badge_id', 'left');
		$this->db->order_by('log.id', 'desc');
		$this->db->limit(3,0);
		$this->db->from('log');
		$getpin = $this->db->get();
		
		return $getpin;
	}
	
	function list_pins($id){
		$this->db->select('');
		$this->db->where('log.account_id', $id);
		$this->db->where('log.log_status', 1);
		$this->db->where('log.log_type', 2);
		$this->db->join('account', 'account.id = log.account_id', 'left');
		$this->db->join('places', 'places.id = log.places_id', 'left');
		$this->db->join('badge', 'badge.id = log.badge_id', 'left');
		$this->db->order_by('log.id', 'desc');
		$this->db->from('log');
		$getpin = $this->db->get();
		
		return $getpin;
	}
	
	function list_spot($id){
		$this->db->select('');
		$this->db->where('log.account_id', $id);
		$this->db->where('log.log_status', 1);
		$this->db->where('log.log_type', 1);
		$this->db->join('places', 'places.id = log.places_id', 'left');
		$this->db->order_by('log.id', 'desc');
		$this->db->limit(3,0);
		$this->db->from('log');
		$getspot = $this->db->get();
		
		return $getspot;
	}
	
	function list_spots($id){
		$this->db->select('');
		$this->db->where('log.account_id', $id);
		$this->db->where('log.log_status', 1);
		$this->db->where('log.log_type', 1);
		$this->db->join('places', 'places.id = log.places_id', 'left');
		$this->db->order_by('log.id', 'desc');
		$this->db->from('log');
		$getspot = $this->db->get();
		
		return $getspot;
	}
	
	function list_recent($id){
		$this->db->select('');
		$this->db->where('log.account_id', $id);
		$this->db->where('log.log_status', 1);
		$this->db->where('log.log_type', 1);
		$this->db->join('places', 'places.id = log.places_id', 'left');
		$this->db->order_by('log.id', 'desc');
		$this->db->limit(7,0);
		$this->db->from('log');
		$getactivity = $this->db->get();
		
		return $getactivity;
	}
	
	function num_pin($id){
		$this->db->select('');
		$this->db->where('log.account_id', $id);
		$this->db->where('log.log_status', 1);
		$this->db->where('log.log_type', 2);
		$this->db->join('account', 'account.id = log.account_id', 'left');
		$this->db->join('places', 'places.id = log.places_id', 'left');
		$this->db->join('badge', 'badge.id = log.badge_id', 'left');
		$this->db->order_by('log.id', 'desc');
		$this->db->from('log');
		$getpin = $this->db->get();
		$num = $getpin->num_rows();
		
		return $num;
	}
	
	function num_spot($id){
		$this->db->select('');
		$this->db->where('log.account_id', $id);
		$this->db->where('log.log_status', 1);
		$this->db->where('log.log_type', 1);
		$this->db->join('places', 'places.id = log.places_id', 'left');
		$this->db->order_by('log.id', 'desc');
		$this->db->from('log');
		$getspot = $this->db->get();
		$num = $getspot->num_rows();
		
		return $num;
	}
}