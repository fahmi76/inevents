<?php

class registrasi_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta'); 
    }
    
    function getplaceslanding($custom_page) {
        $result = array();
        $sql = "SELECT id,places_parent,places_landing,places_backgroud,places_logo,places_model,places_type_registration,"
                . "places_duedate,places_name,places_fbid,places_fb_page_id,places_twitter,places_tw_id,places_css,"
                . "places_avatar,places_cstatus_fb,places_desc "
                . "FROM wooz_places "
                . "WHERE places_landing = '$custom_page' order by id desc";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
            $a = strtotime($data->places_duedate);
            $b = time();
            if($a > $b){
                $result = $data;
            }
        }
        return $result;
    }
    
    function cekuser($id,$nama){
        $sql = "select distinct(b.account_id)
                from wooz_account as a inner join wooz_landing b on b.account_id = a.id 
				where b.landing_register_form = 453 and (a.account_email = '".$nama."' || a.account_displayname like '%".$nama."%' || a.account_phone = '".$nama."')";
        $query = $this->db->query($sql);
        $row = $query->row();
        if($row){
            return $row;
        }else{
            return false;
        }
    }
    function kelompok($nama){
        $sql = "select a.id,a.account_username from wooz_account as a
                left join wooz_marathon_data as b on b.account_id = a.id
                where a.account_email = '".$nama."'
                || a.account_displayname = '".$nama."'
                || b.nama_grup = '".$nama."' and b.grup = 1";
        $query = $this->db->query($sql);
        $row = $query->row();
        if($row){
            return $row;
        }else{
            return false;
        }
    }
	
    function gettable($field,$data,$table){
        $data = $this->db->get_where($table, array($field => $data))->row();
        return $data;      
    }	
}
