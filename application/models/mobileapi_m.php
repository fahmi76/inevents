<?php

class mobileapi_m extends CI_Model {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    function listplaces() {
        $date = date('Y-m-d');
        $traps = array();
        $this->db->select('id,places_landing,places_name,places_duedate,places_nicename');
        $this->db->from('places');
        $this->db->where('places_parent', 0);
        $customs = $this->db->get()->result();
        foreach ($customs as $row) {
            if ($row->places_duedate >= $date) {
                $traps[] = array(
                    'id' => $row->id,
                    'url' => $row->places_landing,
                    'nice' => $row->places_nicename,
                    'name' => $row->places_name
                );
            }
        }
        return $traps;
    }

    function getplacesadmin($id, $name) {
        $getdata = array();
        $sql = "SELECT * FROM wooz_places where id = " . $id . " and places_nicename = '" . $name . "'";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
            $framedata[] = array(
                '1' => '/uploads/photoframe/headquarters_frame.png'
            );
            $getdata[] = array(
                'id' => $data->id,
                'url' => $data->places_landing,
                'name' => $data->places_name,
                'nice' => $data->places_nicename,
                'registrasi' => '1',
                'input' => '1',
                'background' => 'assets/landing/images/bg.jpg',
                'id_update_status' => '18',
                'id_photobooth' => '1',
                'frame_photo' => $framedata,
                'total_frame' => '1',
                'orientasi_photo' => 'landscape'
            );
        }
        return $getdata;
    }

    function cekplaces($id_places, $name_places){
        $hasilcek = false;
        $date = date('Y-m-d');
        $sql = "SELECT * FROM wooz_places where id = " . $id_places . " and places_nicename = '" . $name_places . "'";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if($data){
            if($data->places_duedate >= $date){
                $hasilcek = true;
            }
        }
        return $hasilcek;
    }
    
}
