<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of woozer_m
 *
 * @author ubaidz
 */
class woozer_m extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }
    
    function getwhere($id,$data,$table){
        $data = $this->db->get_where($table, array($id => $data, 'account_status' => 1))->row();
        return $data;  
    }
    
    function getplaces($id,$data,$table){
        $places = $this->db->get_where($table, array($id => $data, 'data_status' => 1))->row();
        return $places;  
    }
    
    function update($id,$data){
        $this->db->where('id', $id);
        $update = $this->db->update('account', $data);
        return $update;
    }
    
    function get_rsvp($user_id,$promo_id){
        $this->db->select('');
        $this->db->where('account_id', $user_id);
        $this->db->where('promo_id', $promo_id);
        $this->db->from('rsvp');
        $rsvp = $this->db->get()->row();
        return $rsvp;
    }
    
    function log_history($user_id){
        $this->db->select('');
        $this->db->where('log.account_id', $user_id);
        $this->db->where('log.data_status', 1);
        $this->db->where('log.log_type', 1);
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');		
        $result = $this->db->get()->result();
        return $result;
        
    }
    
    function log_pin_hash($user_id,$data){
        $this->db->select('');
        $this->db->where('log.account_id', $user_id);
        $this->db->where('log.log_hash', $data);
        $this->db->where('log.data_status', 1);
        $this->db->where('log.log_type', 2);
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result = $this->db->get()->row();
        return $result;
    }
    
    function log_pin_nohash($user_id){
        $this->db->select('');
        $this->db->where('log.account_id', $user_id);
        $this->db->where('log.data_status', 1);
        $this->db->where('log.log_type', 2);
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $result = $this->db->get()->result();
        return $result;
    }    
    
    function history($user_id){
        $this->db->select('log.date_add,log.log_hash,log.log_type,log.badge_id,account.account_username,places.places_nicename,places.places_name,places.places_icon,badge.badge_icon,badge.badge_name');
        $this->db->where('log.account_id', $user_id);
        $this->db->where('log.data_status', 1);
        $this->db->where('log.log_type', 1);
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->limit(7, 0);
        $this->db->from('log');
        $history = $this->db->get()->result();
        return $history;
    }
    
    function pin($user_id){
        $this->db->select('');
        $this->db->where('log.account_id', $user_id);
        $this->db->where('log.data_status', 1);
        $this->db->where('log.log_type', 2);
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->limit(7, 0);
        $this->db->from('log');
        $pin = $this->db->get()->result();
        return $pin;
    }
    
    function view_account($limit,$offset){
        $this->db->select('');
        $this->db->where('account_status !=', 0);
        $this->db->order_by("account_displayname", "asc");
        $this->db->limit($limit, $offset);
        $this->db->from('account');
        $views = $this->db->get()->result();
        return $views;
    }
    
    function total_account(){
        $this->db->select('');
        $this->db->where('account_status !=', 0);
        $this->db->from('account');
        $total = $this->db->count_all_results();
        return $total;
    }
    
    function view_places($limit=0,$offset=0){
        $this->db->select('');
        $this->db->where('places_parent', 0);
        $this->db->where('data_status !=', 0);
        $this->db->where('places_type <', 3);
        $this->db->order_by("places_name", "asc");
        $this->db->limit($limit, $offset);
        $this->db->from('places');
        $views = $this->db->get()->result();
        return $views;
    }
    
    function total_places(){
        $this->db->select('');
        $this->db->where('places_parent', 0);
        $this->db->where('data_status !=', 0);
        $this->db->where('places_type <', 3);
        $this->db->from('places');
        $total = $this->db->count_all_results();
        return $total;
    }    
    
    function user_history(){
        $this->db->select('distinct(account_id), account.*');
        $this->db->where('log.data_status', 1);
        $this->db->where('log.log_type', 1);
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $data = $this->db->get()->result();
        return $data;
    }
    
}

?>
