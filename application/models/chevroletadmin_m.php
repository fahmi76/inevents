<?php

class chevroletadmin_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function searchhistory($barang, $tgl) {
        $sql = "SELECT a.account_displayname,a.account_email,a.account_gender,
				c.nama_barang,b.date_add
                FROM wooz_account as a LEFT JOIN wooz_redeem as b ON b.account_id = a.id 
				LEFT JOIN wooz_redeem_barang as c ON c.id = b.redeem_id ";
        if ($barang != 0 && $tgl != 0) {
            $sql .= "WHERE b.redeem_id = $barang and b.date_add like '%2013-09-$tgl%' ";
        } elseif ($barang != 0 || $tgl != 0) {
            if ($barang != 0) {
                $sql .= "WHERE b.redeem_id = $barang ";
            } else {
                $sql .= "WHERE b.date_add like '%2013-09-$tgl%' ";
            }
        } else {
            $sql .= "";
        }
        $sql .= "order by b.id desc";
		
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
	
    function searchloghistory($barang, $tgl) {
        $sql = "SELECT distinct(b.account_id),places_id,a.account_displayname,a.account_email,a.account_gender,
				c.places_name,b.log_date
                FROM wooz_account as a LEFT JOIN wooz_log as b ON b.account_id = a.id 
				LEFT JOIN wooz_places as c ON c.id = b.places_id ";
        if ($barang != 0 && $tgl != 0) {
            $sql .= "WHERE b.places_id = $barang and b.log_date like '%2013-09-$tgl%' and account_id not in (14334,3179,25508) ";
        } elseif ($barang != 0 || $tgl != 0) {
            if ($barang != 0) {
                $sql .= "WHERE b.places_id = $barang and account_id not in (14334,3179,25508) ";
            } else {
                $sql .= "WHERE b.log_date like '%2013-09-$tgl%' and b.places_id in (370,371,372,373) and account_id not in (14334,3179,25508) ";
            }
        } else {
            $sql .= "b.places_id in (370,371,372,373) and account_id not in (14334,3179,25508) ";
        }
        $sql .= "order by b.places_id asc";
		
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }   
	
    function searchreghistory($tgl) {
        $sql = "SELECT distinct(b.account_id),a.account_displayname,a.account_email,a.account_gender,
				b.landing_joindate
                FROM wooz_account as a LEFT JOIN wooz_landing as b ON b.account_id = a.id ";
        if ($tgl != 0) {
            $sql .= "WHERE b.landing_joindate like '%2013-09-$tgl%' and b.landing_register_form = 369 and b.account_id not in (14334,3179,25508) ";           
        } else {
            $sql .= "b.landing_register_form = 369 and account_id not in (14334,3179,25508) ";
        }
        $sql .= "GROUP BY account_id order by b.id asc";
		
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    } 
	
    function barang(){
        $sql = "SELECT *
                FROM wooz_redeem_barang 
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
	
	function statistikregis(){
        $sql = "SELECT DISTINCT(DATE_FORMAT(`landing_joindate`, '%d %M %Y')) AS thedate, 
            count(distinct(`account_id`)) as total FROM (`wooz_landing`) 
            WHERE `landing_joindate` BETWEEN '2013-09-19' AND '2013-09-30' and landing_register_form = 369
            GROUP BY `thedate` ORDER BY `thedate` asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
	
	}
	
	function statistikuserredeem(){	
        $sql = "SELECT DISTINCT(DATE_FORMAT(`date_add`, '%d %M %Y')) AS thedate, 
            sum(case when `redeem_id` = 1 then 1 else 0 end) gloves,
                    sum(case when `redeem_id` = 2 then 1 else 0 end) topi,
                    sum(case when `redeem_id` = 3 then 1 else 0 end) tshirt,
                    sum(case when `redeem_id` = 4 then 1 else 0 end) ballpoint,
                    count(distinct(account_id)) as total
           FROM (`wooz_redeem`) 
            WHERE `date_add` BETWEEN '2013-09-19' AND '2013-09-30' 
            GROUP BY `thedate` ORDER BY `thedate` asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
	}

}

?>
