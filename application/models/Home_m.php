<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of home_m
 *
 * @author user
 */
class home_m extends CI_Model{
    
    function __construct(){
            parent::__construct();
            $this->load->helper('cookie');
		date_default_timezone_set('Asia/Jakarta'); 
    }
    
    function insert($table,$data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    
    function getTable($data,$table){
        $data = $this->db->get_where($table, array('id' => $data, 'account_status' => 1))->row();
        return $data;  
    }
    
    function getwhere($id,$data,$table){
        $data = $this->db->get_where($table, array($id => $data, 'data_status' => 1))->row();
        return $data;  
    }
    
    function getaccount($id,$data,$table){
        $data = $this->db->get_where($table, array($id => $data))->row();
        return $data;  
    }
    
    function getfb($data,$table){
        $data = $this->db->get_where($table, array('account_fbid' => $data))->row();
        return $data;  
    }
    
    function gettwitter($tw_id,$id){
        $sql = "SELECT id,account_avatar,account_displayname,account_profile,account_location,account_url "
                . "FROM wooz_account WHERE account_twid = '$tw_id' ";
        if($id != 0){
            $sql .= "and id = '$id' ";
        }
        $sql .= "order by id desc";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        return $data;  
    }

    function update_sosmed($id,$data){
        $this->db->where('id', $id);
        $update = $this->db->update('account_sosmed', $data);
        return $update;
    }
    
    function update($id,$data){
        $this->db->where('id', $id);
        $update = $this->db->update('account', $data);
        return $update;
    }
    
    function getbanner(){
        $this->db->select('');
        $this->db->from('banner');
        $this->db->where('banner_cat', 'event');
        $this->db->where('banner_status', 1);
        $data = $this->db->get()->row();
        return $data;
    }
    
    function sidebanner(){
        $this->db->select('');
        $this->db->from('banner');
        $this->db->where('banner_cat', 'others');
        $this->db->where('banner_status', 1);
        $this->db->order_by('id', 'DESC');
        $data = $this->db->get()->result();
        return $data;
    }
    
    function spotshome(){
        $this->db->select('');
        $this->db->where('places_parent', 0);
        $this->db->where('data_status', 1);
        $this->db->where('places_type <', 3);
        $this->db->order_by("id", "desc");
        $this->db->limit(3, 0);
        $this->db->from('places');
        $data = $this->db->get()->result();
        return $data;
    }
    
    function activehome(){
        $this->db->select('places.*, log.places_id');
        $this->db->select('count(wooz_log.id) as jumlah');
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->where('places.places_type <', 3);
        $this->db->where('log.data_status', 1);
        $this->db->where('log_type', 1);
        $this->db->order_by("jumlah", "desc");
        $this->db->group_by('places_id');
        $this->db->limit(3, 0);
        $this->db->from('log');
        $data = $this->db->get()->result();
        return $data;
    }
    
}

?>
