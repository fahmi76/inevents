<?php

class login_m extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('fungsi');
    }

    function validate() {
        $user_email = $this->input->post('email_login');
        $user_password = $this->fungsi->acak($this->input->post('password_login', true));
        $sql = "SELECT id,account_username,account_event FROM wooz_admin
                where account_email = '" . $user_email . "' and account_passwd = '" . $user_password . "' ";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

    function get_db_session_data() {
        $query = $this->db->select('user_data')->get('ci_sessions');
        $user = array(); /* array to store the user data we fetch */
        foreach ($query->result() as $row) {
            $udata = unserialize($row->user_data);
            /* put data in array using username as key */
            $user['user_name'] = $udata['user_name'];
            $user['is_logged_in'] = $udata['is_logged_in'];
        }
        return $user;
    }

}
