<?php

class insight_m extends CI_Model {

	function list_checkin($gate, $check, $status) {
		$sql = "select distinct(a.account_id), a.id,b.nama,c.account_rfid,c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,c.account_avatar,d.log_stamps as time_upload,d.log_date,d.log_time,d.log_check from wooz_seat_account a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
			. "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate1 = " . $gate . " and d.log_gate = " . $gate . " and d.log_status = " . $status . " and d.log_stamps >= '2015-01-31 16:00:00' group by a.account_id order by a.id desc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_tabel($id) {
		$sql = "select id,field_name from wooz_form_regis where places_id = '" . $id . "'";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function ambil_content_data($acc_id, $places_id, $content_id) {
		$sql = "select a.id,content,field_name,field_type from wooz_form_regis_detail a inner join wooz_form_regis b on b.id = a.form_regis_id
            where a.account_id = '" . $acc_id . "' and a.places_id = '" . $places_id . "' and form_regis_id = '" . $content_id . "' ";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function list_all_gate($id) {
		$sql = "select badge_id,log_fb_places,d.account_id,c.account_displayname,c.account_gw_token,log_gate,c.account_gw_refresh,c.account_gw_time,d.log_stamps as time_upload,d.log_check_out,d.log_date,d.log_time from wooz_account c "
			. "left join wooz_log_user_gate d on d.account_id = c.id "
			. "where d.places_id = '" . $id . "' and log_check in (1,2) and d.log_stamps >= '2015-01-31 16:00:00' "
			. "order by d.id desc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}
	function list_all_reregis() {
		$sql = "select c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,d.log_stamps as time_upload,d.log_check_out from wooz_account_sosmed c "
			. "left join wooz_log_user_gate d on d.account_id = c.id "
			. "where d.log_gate = 14 and log_status = 2 and log_check = 1 and d.log_stamps >= '2015-01-31 16:00:00' group by d.account_id order by d.id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_photos($id) {
		if ($id == 1) {
			$sql = "select distinct(d.account_id),c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,d.time_upload from wooz_account_sosmed c "
				. "left join wooz_photos d on d.account_id = c.id "
				. "where d.places_id = 7 group by d.account_id order by d.id desc";
		} else {
			$sql = "select c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,d.time_upload from wooz_account_sosmed c "
				. "left join wooz_photos d on d.account_id = c.id "
				. "where d.places_id = 7 order by d.id desc";
		}
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_tagging() {
		$sql = "select c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,d.time_upload from wooz_account_sosmed c "
			. "inner join wooz_tagging d on d.account_id = c.id "
			. "order by d.id desc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;

	}

	function count_list_all_gate($id) {
		$sql = "SELECT count(a.id) as total_account FROM `wooz_log_user_gate` a inner join wooz_account b on b.id = a.account_id where places_id = '" . $id . "' and log_check = 1 and log_stamps >= '2015-01-31 16:00:00' ";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;

	}

	function get_gate($id) {
		$sql = "SELECT nama FROM `wooz_gate` where id = '" . $id . "' order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil->nama;
	}

	function list_check($gate, $check, $status) {
		$sql = "SELECT badge_id,log_fb_places,account_id,account_displayname,account_gw_token,account_gw_refresh,account_gw_time,log_stamps as time_upload,log_check_out,log_date,log_time FROM `wooz_log_user_gate` a "
			. "inner join wooz_account b on b.id = a.account_id where log_gate = '" . $gate . "' "
			. "and (log_check = '1' or log_check = '5') ";
		if ($check == 2) {
			$sql .= " and log_check_out = 2 ";
		}
		// if ($gate != 38) {
		// $sql .= "and log_stamps >= '2015-01-31 16:00:00' group by log_date";
		// } else {
		$sql .= "and log_stamps >= '2015-01-31 16:00:00' group by account_id";
		// }
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function get_first_checkin($log_date, $account_id, $gate) {
		$checkin = '0000-00-00 00:00:00';
		$sql_checkin = "SELECT log_stamps from wooz_log_user_gate where log_gate = '" . $gate . "' and log_stamps like '%" . $log_date . "%' and account_id = '" . $account_id . "' order by id asc";
		$query = $this->db->query($sql_checkin);
		$hasil = $query->row();
		if ($hasil) {
			$checkin = $hasil->log_stamps;
		}
		return $checkin;
	}

	function get_last_checkout($log_date, $account_id, $gate) {
		$result = array(
			'checkout' => '0000-00-00 00:00:00',
			'duration' => '00:00:00',
		);
		$sql_checkin = "SELECT log_date,log_time from wooz_log_user_gate where log_gate = '" . $gate . "' and log_date like '%" . $log_date . "%' and account_id = '" . $account_id . "' order by id desc";
		$query = $this->db->query($sql_checkin);
		$hasil = $query->row();
		if ($hasil) {
			$checkout = $hasil->log_date . ' ' . $hasil->log_time;
		}
		$checkin = $this->get_first_checkin($log_date, $account_id, $gate);
		$start_time = strtotime($checkin);
		$end_time = strtotime($checkout);
		$difference = $end_time - $start_time;
		$seconds = $difference % 60; //seconds
		$difference = floor($difference / 60);
		$min = $difference % 60; // min
		$difference = floor($difference / 60);
		$hours = $difference; //hours
		$duration = "$hours : $min : $seconds";
		$result = array(
			'checkout' => $checkout,
			'duration' => $duration,
		);
		return $result;
	}

	function list_arrive($id) {
		$sql = "SELECT distinct(account_id),account_displayname,account_gw_token,account_gw_refresh,account_gw_time,landing_joindate as time_upload FROM `wooz_landing` a "
			. "inner join wooz_account_sosmed b on b.id = a.account_id where landing_register_form = '6'";
		if ($id == 1) {
			$sql .= " and landing_from_regis = 0";
		} else {
			$sql .= " and landing_from_regis = 2";
		}
		$sql .= " group by account_id";

		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_gate($id) {
		$sql = "SELECT id,nama FROM `wooz_gate` where places_id = '" . $id . "' order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_places($id) {
		$sql = "SELECT id,places_name as nama FROM `wooz_places` where places_parent = '" . $id . "' order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function log_event() {
		$sql = "SELECT places_id,count(distinct(account_id)) as total_account,count(id) as total "
			. "FROM `wooz_log_user` where places_id in (10,14)  group by places_id order by places_id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function logcheckinreregis($id) {
		$sql = "SELECT count(distinct(account_id)) as total_account "
			. "FROM `wooz_log_user_gate` where log_gate = '" . $id . "' and log_status = 2 and log_check = 1 and log_stamps >= '2015-01-31 16:00:00'";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function logcheckinstation($id) {
		$sql = "SELECT count(a.id) as total_account "
			. "FROM `wooz_log` a inner join wooz_account b on b.id = a.account_id where places_id = '" . $id . "' and log_stamps >= '2015-01-31 16:00:00'";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function logcheckinstaff($id) {
		// $sql = "SELECT count(a.id) as total_account "
		$sql = "SELECT count(distinct(a.account_id)) as total_account "
			. "FROM `wooz_log_user_gate` a inner join wooz_account b on b.id = a.account_id where log_gate = '" . $id . "' and log_check = 1 and log_stamps >= '2015-01-31 16:00:00'";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function logcheckoutstaff($id) {
		// $sql = "SELECT count(id) as total_account "
		$sql = "SELECT count(distinct(account_id)) as total_account "
			. "FROM `wooz_log_user_gate` where log_gate = '" . $id . "' and log_check = 1 and log_check_out = 2 and log_stamps >= '2015-01-31 16:00:00'";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function arrive_event($id) {
		$sql = "SELECT count(distinct(account_id)) as total_account FROM `wooz_landing` where landing_register_form = 6";
		if ($id == 1) {
			$sql .= " and landing_from_regis = 0";
		} else {
			$sql .= " and landing_from_regis = 2";
		}
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;

	}

	function count_user_photos() {
		$sql = "SELECT count(distinct(account_id)) as total_account FROM `wooz_photos` where  places_id = 7";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function count_photos() {
		$sql = "SELECT count(id) as total_account FROM `wooz_photos` where  places_id = 7";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function count_tag_photos() {
		$sql = "SELECT count(id) as total_account FROM `wooz_tagging`";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function hourregtotal($rowplaces, $rowdate, $table, $places) {
		$this->db->select('DISTINCT(hour(date_add)) AS thedate,' . $places . ', count(distinct(account_id)) as total');
		$this->db->where($places, $rowplaces);
		$this->db->where("HOUR(date_add)", $rowdate);
		$this->db->from($table);
		$this->db->group_by("thedate");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->row();
		return $total;
	}

	function check_hour($gate, $check, $status) {
		// $this->db->select('DISTINCT(hour(log_stamps)) AS thedate, count(a.id) as total');
		$this->db->select('DISTINCT(hour(log_stamps)) AS thedate, count(DISTINCT(account_id)) as total');
		$this->db->where("log_gate", $gate);
		//$this->db->where("log_check", $check);
		$this->db->where("log_status", $status);
		if ($check == 2) {
			$this->db->where("log_check_out", 2);
		}
		$this->db->where("log_stamps >= '2015-01-31 16:00:00' ");
		$this->db->from('log_user_gate a');
		$this->db->join('account b', 'b.id = a.account_id', 'inner');
		$this->db->group_by("thedate");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->result();
		return $total;
	}

	function get_pre() {
		$sql = "SELECT count(id) as total "
			. "FROM `wooz_account_merchant`";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function regissosmed() {
		$sql = "SELECT count(id) as total FROM `wooz_account_merchant` where account_fbid is not null or account_twid is not null";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function get_fb() {
		$sql = "SELECT count(id) as total,sum(account_fb_friends) as friends "
			. "FROM `wooz_account_merchant` where account_fbid is not null";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function get_tw() {
		$sql = "SELECT count(id) as total,sum(account_tw_follow) as friends "
			. "FROM `wooz_account_merchant` where account_twid is not null";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

}
