<?php

class user_m extends CI_Model {

	function list_user() {
		$sql = "select a.id,a.account_displayname,a.account_email,account_gender,account_birthdate,account_gw_token,
                account_gw_refresh,account_gw_time,a.account_status,a.account_rfid
                from wooz_account_sosmed a order by a.id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_event() {
		$date = date('Y-m-d');
		$sql = "select id,places_name from wooz_places where places_parent = 0 and id > 8 and places_duedate > '" . $date . "' order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;

	}

	function list_tabel($id) {
		$sql = "select id,field_name from wooz_form_regis where places_id = '" . $id . "' order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_tabel_xls($id) {
		$sql = "select id,field_name from wooz_form_regis where field_name = '" . $id . "'";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function list_user_event($id) {
		$places_id = $id;
		$sql = "select distinct(b.account_id),a.id,a.account_firstname,a.account_lastname,a.account_displayname,a.account_rfid
                from wooz_account a inner join wooz_landing b on b.account_id = a.id where b.landing_register_form = '" . $id . "' group by b.account_id order by a.id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function ambil_content_data($acc_id, $places_id, $content_id) {
		$sql = "select a.id,content,field_name,field_type from wooz_form_regis_detail a inner join wooz_form_regis b on b.id = a.form_regis_id
            where a.account_id = '" . $acc_id . "' and a.places_id = '" . $places_id . "' and form_regis_id = '" . $content_id . "' ";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function getuser($id, $places_id) {
		$sql = "select a.id,a.account_displayname,a.account_rfid
                from wooz_account a inner join wooz_landing b on b.account_id = a.id where b.landing_register_form = '" . $places_id . "' and a.id = '" . $id . "' order by a.id asc";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function user_win($id) {
		$sql = "select id from wooz_winner_account where account_id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function cek_content_data($acc_id, $places_id, $content_id) {
		$sql = "select id,content from wooz_form_regis_detail where account_id = '" . $acc_id . "' and places_id = '" . $places_id . "' and form_regis_id = '" . $content_id . "' ";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function cek_content_winner($acc_id, $places_id, $content_id) {
		$sql = "select id from wooz_winner_account where account_id = '" . $acc_id . "' and places_id = '" . $places_id . "' and winner_id = '" . $content_id . "' ";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function cek_winner($nama, $places_id) {
		$sql = 'select id from wooz_winner where nama = "' . $nama . '" and places_id = "' . $places_id . '"';
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function list_winner() {
		$sql = "select nama,number from wooz_winner where places_id = '11' ";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function cek_content_presenter($acc_id, $places_id, $content_id) {
		$sql = "select id from wooz_presenter_account where account_id = '" . $acc_id . "' and cat_winner_id = '" . $content_id . "' ";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function nicename($file, $id = 0) {
		$healthy = array(" ", ".", ",", "?", "&", "(", ")", "#", "!", "^", "%", "\'", "@", "}", "{", "]", "[", "|", "+", "", "'", "/", "|", ":", '"', "<", ">", ";", "--", "+");
		$yummy = array("-", "");
		$result = strtolower(str_replace($healthy, $yummy, trim($file)));
		$result = preg_replace('/[^;\sa-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ]+/u', '', $result);

		$this->db->select('');
		$this->db->where('account_username', $result);
		$this->db->where('id !=', $id);
		$this->db->where('account_status !=', 0);
		$this->db->from('account');
		$c = $this->db->count_all_results();

		if ($c) {
			$this->db->select('');
			$this->db->where('id !=', $id);
			$this->db->where('account_status !=', 0);
			$this->db->like('account_username', $result, 'after');
			$this->db->from('account');
			$c = $this->db->count_all_results();

			if ($c) {
				$this->db->select('');
				$this->db->where('id !=', $id);
				$this->db->where('account_status !=', 0);
				$this->db->like('account_username', $result, 'after');
				$this->db->order_by('id', 'desc');
				$this->db->limit(1, 0);
				$res = $this->db->get('account')->row();

				$slice = explode('-', $res->account_username);
				$cc = count($slice);
				if (is_numeric($slice[$cc - 1])) {
					$slice[$cc - 1] = $slice[$cc - 1] + 1;
					$jadi = implode('-', $slice);
				} else {
					$jadi = $result . '-1';
				}
			} else {
				$jadi = $result . '-1';
			}
		} else {
			$jadi = $result;
		}
		return $jadi;
	}

}
