<?php

class main_m extends CI_Model {
    

    var $table = 'log_user_gate a';
    var $column = array('badge_id','log_fb_places','a.account_id','account_displayname','log_stamps','log_check_out'); //set column field database for order and search
    var $order = array('account_lastname' => 'asc'); // default order 

    public function _get_datatables_query($gate,$check,$status,$id)
    {
        $this->db->select($this->column);
        $this->db->from($this->table);
        $this->db->join('account b', 'b.id = a.account_id', 'inner');
        if($gate == 11){
            $this->db->where('places_id', $gate); 
        }else{ 
            $this->db->where('log_gate', $gate);  
        }
        if($check == 2){
            $this->db->where('log_check_out', 2);  
        }

        $i = 0;
        $dataarray = '';
        foreach ($this->column as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    //$this->db->like($item, $_POST['search']['value']);
                    $dataarray = '(';
                    $dataarray .= $item ." like '%".$_POST['search']['value']."%'"; 
                }
                else
                {
                    $dataarray .= "or ".$item ." like '%".$_POST['search']['value']."%'"; 
                    //$this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
                    $dataarray .= ')';
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }
        if($dataarray != ''){
            $this->db->where($dataarray);
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($gate,$check,$status,$id)
    {
        $this->_get_datatables_query($gate,$check,$status,$id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($gate,$check,$status,$id)
    {
        $this->_get_datatables_query($gate,$check,$status,$id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($gate,$check,$status,$id)
    {
        $this->db->from($this->table);
        $this->db->join('account b', 'b.id = a.account_id', 'inner');
        if($gate == 11){
            $this->db->where('places_id', $gate); 
        }else{ 
            $this->db->where('log_gate', $gate);  
        }
        if($check == 2){
            $this->db->where('log_check_out', 2);  
        }
        return $this->db->count_all_results();
    }

    function cek_session($user_id,$user_name,$admin){ 
        $hasil = array();
        if($admin == 1){
            $sql = "SELECT id,account_username,account_displayname,account_group FROM wooz_admin
                    where account_username = '".$user_name."' and id = '".$user_id."' ";
            $query = $this->db->query($sql);
            $hasil = $query->row();
        }
        return $hasil;
    }
    
}
