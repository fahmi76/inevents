<?php

class winner_m extends CI_Model {

	var $table = 'account a';
	var $column = array('b.id', 'a.account_displayname', 'c.nama', 'b.account_id', 'c.data_status', 'c.datelock'); //set column field database for order and search
	var $order = array('a.account_lastname' => 'asc'); // default order

	public function _get_datatables_query($places, $id) {
		$this->db->select($this->column);
		$this->db->from($this->table);
		$this->db->join('winner_account b', 'b.account_id = a.id', 'inner');
		$this->db->join('winner c', 'c.id = b.winner_id', 'inner');

		$i = 0;
		$dataarray = '';
		foreach ($this->column as $item) // loop column
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					//$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					//$this->db->like($item, $_POST['search']['value']);
					$dataarray = '(';
					$dataarray .= $item . " like '%" . $_POST['search']['value'] . "%'";
				} else {
					$dataarray .= "or " . $item . " like '%" . $_POST['search']['value'] . "%'";
					//$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column) - 1 == $i) //last loop
				//$this->db->group_end(); //close bracket
				{
					$dataarray .= ')';
				}

			}
			$column[$i] = $item; // set column array variable to order processing
			$i++;
		}
		if ($dataarray != '') {
			$this->db->where($dataarray);
		}
		$this->db->where('c.places_id', $places);
		$this->db->where('b.winner_id', $id);
		$this->db->where('b.id !=', 16);

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($places, $id) {
		$this->_get_datatables_query($places, $id);
		if ($_POST['length'] != -1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($places, $id) {
		$this->_get_datatables_query($places, $id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($places, $id) {
		$this->db->from($this->table);
		$this->db->join('winner_account b', 'b.account_id = a.id', 'inner');
		$this->db->join('winner c', 'c.id = b.winner_id', 'inner');
		$this->db->where('c.places_id', $places);
		$this->db->where('b.winner_id', $id);
		$this->db->where('b.id !=', 16);
		return $this->db->count_all_results();
	}

	function list_visitor($id) {
		$sql = "SELECT b.id,a.account_displayname,c.nama FROM `wooz_winner_account` b
                inner join wooz_account a on a.id = b.account_id
                inner join wooz_winner c on c.id = b.winner_id where c.places_id = '" . $id . "' and c.id != 16";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_visitor_cat($places, $id) {
		$sql = "SELECT b.id,a.account_displayname,c.nama,b.account_id,c.data_status,c.datelock FROM `wooz_winner_account` b
                inner join wooz_account a on a.id = b.account_id
                inner join wooz_winner c on c.id = b.winner_id where c.places_id = '" . $places . "' and b.winner_id = '" . $id . "' and b.id != 16 order by a.account_lastname";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_presenter() {
		$sql = "SELECT b.id,c.account_displayname,b.account_id,a.nama,b.presenter_status,a.data_status,a.datelock FROM wooz_winner a inner join wooz_presenter_account b on b.cat_winner_id = a.number inner join wooz_account c on c.id = b.account_id ";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function cek_winner($id) {
		$sql = "SELECT id,nama FROM wooz_winner where id = '" . $id . "'";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function list_category($id) {
		$sql = "SELECT id,nama,data_status,toogle FROM wooz_winner where places_id = '" . $id . "' and id != 16";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_gate() {
		#$sql = "select id,nama,data_status from wooz_winner order by id asc";
		$sql = "SELECT id,nama FROM wooz_winner where places_id = '31' and id != 16";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function get_statususer($id) {
		$sql = "SELECT log_check,log_check_out,log_status_check,log_stamps FROM `wooz_log_user_gate` where account_id = '" . $id . "' and places_id = 31 ORDER BY `id` DESC";
		$querys = $this->db->query($sql);
		$hasil = $querys->row();
		return $hasil;
	}

	function get_gate($id) {
		$sql = "select * from wooz_gate where id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function getuser($id) {
		$sql = "SELECT b.id,a.account_displayname,b.winner_id,b.account_id FROM `wooz_winner_account` b
                inner join wooz_account a on a.id = b.account_id
                inner join wooz_winner c on c.id = b.winner_id where b.id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function getuserpresenter($id) {
		$sql = "SELECT b.id,b.account_id,c.account_displayname,b.cat_winner_id as winner_id FROM wooz_winner a inner join wooz_presenter_account b on b.cat_winner_id = a.id inner join wooz_account c on c.id = b.account_id where b.id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function list_rfid() {
		$sql = "select a.id,b.nama,a.rfid,c.account_displayname from wooz_visitor_rfid a inner join wooz_visitor b on b.id = a.visitor_id "
			. "left join wooz_account_merchant c on c.id = a.account_id order by a.id desc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function get_rfid($id) {
		$sql = "select * from wooz_visitor_rfid where id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function list_vendor() {
		$sql = "select a.id,b.nama,a.rfid,c.account_displayname,c.account_groups,c.account_avatar,d.log_stamps,d.log_check from wooz_visitor_rfid a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
			. "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate = 11 and d.log_status = 1 and d.log_stamps >= '2015-01-31 16:00:00' order by a.id desc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}
	function list_backstage() {
		$sql = "select a.id,b.nama,a.rfid,c.account_displayname,c.account_groups,c.account_avatar,d.log_stamps,d.log_check from wooz_visitor_rfid a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
			. "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate = 10 and d.log_status = 1 and d.log_stamps >= '2015-01-31 16:00:00' order by a.id desc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}
	function list_vendor_fail() {
		$sql = "select a.id,b.nama,a.rfid,c.account_displayname,c.account_groups,c.account_avatar,d.log_stamps,d.log_check from wooz_visitor_rfid a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
			. "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate = 11 and d.log_status = 2 and d.log_stamps >= '2015-01-31 16:00:00' order by a.id desc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}
	function list_backstage_fail() {
		$sql = "select a.id,b.nama,a.rfid,c.account_displayname,c.account_groups,c.account_avatar,d.log_stamps,d.log_check from wooz_visitor_rfid a "
			. "inner join wooz_visitor b on b.id = a.visitor_id "
			. "left join wooz_account_merchant c on c.id = a.account_id "
			. "left join wooz_log_user_gate d on d.account_id = a.account_id "
			. "where d.log_gate = 10 and d.log_status = 2 and d.log_stamps >= '2015-01-31 16:00:00' order by a.id desc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}
}
