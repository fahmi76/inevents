<?php

class insightsosmed_m extends CI_Model {

    function list_checkin($gate,$check,$status) {       
        $sql = "select distinct(a.account_id), a.id,b.nama,c.account_rfid,c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,c.account_avatar,d.log_stamps as time_upload,d.log_check from wooz_seat_account a "
            . "inner join wooz_visitor b on b.id = a.visitor_id "
            . "left join wooz_account_merchant c on c.id = a.account_id "
            . "left join wooz_log_user_gate d on d.account_id = a.account_id "
            . "where d.log_gate1 = ".$gate." and d.log_gate = ".$gate." and d.log_status = ".$status." and d.log_stamps >= '2015-01-31 16:00:00' group by a.account_id order by a.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }

    function list_tabel($id){
        $sql = "select id,field_name from wooz_form_regis where places_id = '".$id."'";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function ambil_content_data($acc_id,$places_id,$content_id){
        $sql = "select a.id,content,field_name,field_type from wooz_form_regis_detail a inner join wooz_form_regis b on b.id = a.form_regis_id
            where a.account_id = '".$acc_id."' and a.places_id = '".$places_id."' and form_regis_id = '".$content_id."' ";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;  
    }

    function list_all_gate($id) {       
        $sql = "select badge_id,log_fb_places,d.account_id,c.account_displayname,c.account_gw_token,log_gate,c.account_gw_refresh,c.account_gw_time,d.log_stamps as time_upload,d.log_check_out,d.log_date,d.log_time from wooz_account c "
            . "left join wooz_log_user_gate d on d.account_id = c.id "
            . "where d.places_id = '".$id."' and log_check in (1,2) and d.log_stamps >= '2015-01-31 16:00:00' "
            ."order by d.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }
    function list_all_reregis($id) {       
        $sql = "select c.account_displayname,c.account_fbid,c.account_tw_username,CONVERT_TZ(d.landing_joindate,'+00:00','-06:00') as time_upload from wooz_account c inner join wooz_landing d on d.account_id = c.id "
            . "where d.landing_register_form = '".$id."' group by d.account_id order by d.id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }

    function list_photos($id) {
        if($id == 1){
            $sql = "select distinct(d.account_id),c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,d.time_upload from wooz_account_sosmed c "
                . "left join wooz_photos d on d.account_id = c.id "
                . "where d.places_id = 7 group by d.account_id order by d.id desc";
        }else{
            $sql = "select c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,d.time_upload from wooz_account_sosmed c "
                . "left join wooz_photos d on d.account_id = c.id "
                . "where d.places_id = 7 order by d.id desc";
        }
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }

    function list_tagging(){
        $sql = "select c.account_displayname,c.account_gw_token,c.account_gw_refresh,c.account_gw_time,d.time_upload from wooz_account_sosmed c "
            . "inner join wooz_tagging d on d.account_id = c.id "
            . "order by d.id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  

    }

    function count_list_all_gate($id){
        $total = $this->list_checkall($id);
        $hasil = count($total);
        return $hasil;
        
    }

    function get_gate($id){
        $sql = "SELECT places_name FROM `wooz_places` where id = '".$id."' order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil->places_name;
    }

    function list_check($gate){
        $sql = "SELECT account_id,account_displayname,account_fbid,account_tw_username,CONVERT_TZ(log_stamps,'+00:00','-06:00') as time_upload FROM `wooz_log` a "
            ."inner join wooz_account b on b.id = a.account_id where places_id = '".$gate."' and log_stamps >= '2016-06-11 00:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }

    function list_checkall($gate){
        $sql = "SELECT a.id,a.account_displayname,a.account_fbid,a.account_tw_username,CONVERT_TZ(b.log_stamps,'+00:00','-06:00') as time_booth2,CONVERT_TZ(c.log_stamps,'+00:00','-06:00') as time_booth3,CONVERT_TZ(d.log_stamps,'+00:00','-06:00') as time_boothp ,CONVERT_TZ(e.log_stamps,'+00:00','-06:00') as time_booth1 FROM  wooz_account a inner join wooz_log b on b.account_id = a.id inner join wooz_log c on c.account_id = b.account_id inner join wooz_log d on d.account_id = c.account_id inner join wooz_log e on e.account_id = d.account_id where b.places_id = 16 and c.places_id = 17 and d.places_id = 18 and e.places_id = 19 and b.log_stamps >= '2016-06-11 00:00:00' and c.log_stamps >= '2016-06-11 00:00:00' and d.log_stamps >= '2016-06-11 00:00:00' and e.log_stamps >= '2016-06-11 00:00:00' group by a.id";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }

    function list_arrive($id) {       
        $sql = "SELECT distinct(account_id),account_displayname,account_gw_token,account_gw_refresh,account_gw_time,landing_joindate as time_upload FROM `wooz_landing` a "
            ."inner join wooz_account_sosmed b on b.id = a.account_id where landing_register_form = '6'";
        if($id == 1){
            $sql .= " and landing_from_regis = 0";
        }else{
            $sql .= " and landing_from_regis = 2";
        }
        $sql .= " group by account_id";
       
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;  
    }
    
    function list_places($id){
        $sql = "SELECT id,places_name FROM `wooz_places` where places_parent = '".$id."' order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function count_list_regis($id){
        $sql = "SELECT count(distinct(account_id)) as total_account "
                . "FROM `wooz_landing` where landing_register_form = '".$id."'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

    function log_event(){
        $sql = "SELECT places_id,count(distinct(account_id)) as total_account,count(id) as total "
                . "FROM `wooz_log_user` where places_id in (10,14)  group by places_id order by places_id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function logcheckinreregis($id){
        $sql = "SELECT count(distinct(account_id)) as total_account "
                . "FROM `wooz_log_user_gate` where log_gate = '".$id."' and log_status = 2 and log_check = 1 and log_stamps >= '2015-01-31 16:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

    function logcheckinstaff($id){
        $sql = "SELECT count(account_id) as total_account FROM `wooz_log` a "
            ."inner join wooz_account b on b.id = a.account_id where places_id = '".$id."' and log_stamps >= '2016-06-11 00:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function logcheckoutstaff($id){
        $sql = "SELECT count(id) as total_account "
                . "FROM `wooz_log_user_gate` where log_gate = '".$id."' and log_check = 1 and log_check_out = 2 and log_stamps >= '2015-01-31 16:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function arrive_event($id){
        $sql = "SELECT count(distinct(account_id)) as total_account FROM `wooz_landing` where landing_register_form = 6";
        if($id == 1){
            $sql .= " and landing_from_regis = 0";
        }else{
            $sql .= " and landing_from_regis = 2";
        }
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
        
    }

    function count_user_photos(){
        $sql = "SELECT count(distinct(account_id)) as total_account FROM `wooz_photos` where  places_id = 7";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

    function count_photos(){
        $sql = "SELECT count(id) as total_account FROM `wooz_photos` where  places_id = 7";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

    function count_tag_photos(){
        $sql = "SELECT count(id) as total_account FROM `wooz_tagging`";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function hourregtotal($rowplaces,$rowdate,$table,$places){
        $this->db->select('DISTINCT(hour(date_add)) AS thedate,'.$places.', count(distinct(account_id)) as total');
        $this->db->where($places, $rowplaces);
        $this->db->where("HOUR(date_add)",$rowdate);
        $this->db->from($table);
        $this->db->group_by("thedate"); 
        $this->db->order_by("thedate", "asc");
        $total = $this->db->get()->row();
        return $total;
    }
    
    function check_hour($gate){
        $sql = "SELECT DISTINCT(hour(CONVERT_TZ(log_stamps,'+00:00','-06:00'))) AS thedate, count(id) as total FROM (`wooz_log`) WHERE `places_id` = '".$gate."' and log_stamps >= '2016-06-11 00:00:00' GROUP BY `thedate` ORDER BY `thedate` asc ";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function check_hour_regis($gate){
        $sql = "SELECT DISTINCT(hour(CONVERT_TZ(landing_joindate,'+00:00','-06:00'))) AS thedate, count(distinct(account_id)) as total FROM (`wooz_landing`) WHERE `landing_register_form` = '".$gate."' and landing_joindate >= '2016-06-11 00:00:00' GROUP BY `thedate` ORDER BY `thedate` asc ";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function get_pre(){
        $sql = "SELECT count(id) as total "
                . "FROM `wooz_account_merchant`";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function regissosmed(){
        $sql = "SELECT count(id) as total FROM `wooz_account_merchant` where account_fbid is not null or account_twid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_fb(){
        $sql = "SELECT count(id) as total,sum(account_fb_friends) as friends "
                . "FROM `wooz_account_merchant` where account_fbid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_tw(){
        $sql = "SELECT count(id) as total,sum(account_tw_follow) as friends "
                . "FROM `wooz_account_merchant` where account_twid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
     
    function spotlikeinfo($spot){
        $sql = "SELECT distinct(a.account_id), c.`facebook_page_name`, d.`category_name` 
            FROM `wooz_landing` as a LEFT JOIN `wooz_user_likes` as b ON b.`account_id` = a.`account_id` 
            LEFT JOIN `wooz_like` as c ON c.`id` = b.`likes_id` 
            LEFT JOIN `wooz_like_category` as d ON d.`id` = c.`category_id` 
            LEFT JOIN `wooz_account` as e ON e.`id` = a.`account_id` 
            WHERE a.`landing_register_form` = ".$spot." ";
        $query = $this->db->query($sql);
        $row = $query->result();
        return $row;
    }

    function getsosmed(){
        $sql = "SELECT a.account_id,account_fbid,account_twid,landing_fb_like,landing_fb_like_event,landing_tw_follow,landing_tw_follow_event,landing_fb_friends,landing_tw_friends FROM `wooz_landing` a inner join wooz_account b on b.id = a.account_id where landing_register_form = 15 group by a.account_id ";
        $query = $this->db->query($sql);
        $row = $query->result();
        return $row;
    }

    function get_log_fb($from){
        $sql = "SELECT count(id) as total FROM `wooz_log` where places_id in (".$from.") and log_fb != 0 ";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil->total;
    }
    function get_log_tw($from){
        $sql = "SELECT count(id) as total FROM `wooz_log` where places_id in (".$from.") and log_tw != 0 ";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil->total;
    }
}
