<?php

class quisioner_m extends CI_Model {

	function list_visitor($id) {
		$sql = "select id,question,survey_id from wooz_quisioner_soal where places_id = '" . $id . "' and data_status = 1 order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_survey($id, $survey) {
		$sql = "select id,question from wooz_quisioner_soal where places_id = '" . $id . "' and survey_id = '" . $survey . "' and data_status = 1 order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function list_answer($id) {
		$sql = "select id,answer,data_status from wooz_quisioner_answer where question_id = '" . $id . "' order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function get_name_places($id) {
		$sql = "select places_name,places_startdate,places_duedate from wooz_places where id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function get_list_setting($id) {
		$sql = "select id,title from wooz_quisioner_setting where places_id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function get_title($id) {
		$sql = "select id,title from wooz_quisioner_setting where id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function get_setting($id) {
		$sql = "select * from wooz_quisioner_setting where id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function get_setting_download($id) {
		$sql = "select * from wooz_quisioner_setting where places_id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function get_gate($id) {
		$sql = "select * from wooz_quisioner_soal where id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function get_answer($id) {
		$sql = "select * from wooz_quisioner_answer where id = " . $id;
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function count_answer($places_id, $question_id, $id) {
		$sql = "SELECT count(id) as total FROM `wooz_quisioner` where places_id = '" . $places_id . "' and question_id = '" . $question_id . "' and answer_id = '" . $id . "'";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function count_list_all_gate($id) {
		$sql = "SELECT count(id) as total_account FROM wooz_quisioner where places_id = '" . $id . "' ";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function list_gate($id) {
		$sql = "SELECT id,answer as nama FROM `wooz_quisioner_soal` where places_id = '" . $id . "' order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function logcheckinstaff($id) {
		$sql = "SELECT count(id) as total_account "
			. "FROM `wooz_quisioner` where question_id = '" . $id . "'";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function count_user_answer($id, $status) {
		$sql = "SELECT count(a.id) as total FROM `wooz_quisioner` a inner join wooz_quisioner_answer b on b.id = a.answer_id inner join wooz_quisioner_account c on c.account_rfid = a.account_id WHERE a.question_id = '" . $id . "'";
		if ($status == 1) {
			$sql .= " and b.data_status = 1";
		}
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil;
	}

	function get_user_answer($id, $status) {
		$sql = "SELECT c.*,d.question,b.answer,b.data_status,a.date_add FROM `wooz_quisioner` a inner join wooz_quisioner_answer b on b.id = a.answer_id inner join wooz_quisioner_account c on c.account_rfid = a.account_id inner join wooz_quisioner_soal d on d.id = a.question_id  WHERE a.question_id = '" . $id . "'";
		if ($status == 1) {
			$sql .= " and b.data_status = 1";
		}
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function check_hour($gate, $places_id) {
		$this->db->select('DISTINCT(hour(date_add)) AS thedate, count(id) as total');
		$this->db->where("question_id", $gate);
		$this->db->where("places_id", $places_id);
		$this->db->from('quisioner');
		$this->db->group_by("thedate");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->result();
		return $total;
	}

	function list_all_gate($id, $places_id = 0) {
		$sql = "SELECT c.question,b.answer,a.date_add "
			. "FROM `wooz_quisioner` a inner join wooz_quisioner_answer b on b.id = a.answer_id inner join wooz_quisioner_soal c on c.id = a.question_id ";
		$sql .= "where a.places_id = '" . $id . "'";
		if ($places_id != 0) {
			$sql .= " and a.question_id = '" . $places_id . "'";
		}
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function dateregday($family, $question, $sdate, $edate) {
		if ($family == 13) {
			//$question = 0;
		}
		$this->db->select("DISTINCT(date(date_add)) AS thedate");
		$this->db->where("places_id", $family);
		$this->db->where("question_id", $question);
		$this->db->where("date_add between '$sdate' and '$edate' ");
		$this->db->from('quisioner');
		$this->db->group_by("thedate,answer_id");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->result();
		return $total;
	}

	function reghour($family, $question, $sdate, $edate) {
		$this->db->select('DISTINCT(hour(date_add)) AS thedate,places_id, count(distinct(account_id)) as total');
		$this->db->where_in("places_id", $family);
		$this->db->where("question_id", $question);
		$this->db->where("date_add between '$sdate' and '$edate' ");
		$this->db->from('quisioner');
		$this->db->group_by("thedate,answer_id");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->result();
		return $total;
	}

	function regdayname($family, $question, $sdate, $edate) {
		$this->db->select('DISTINCT(DAYNAME(date_add)) AS thedate,places_id, count(distinct(account_id)) as total');
		$this->db->where_in("places_id", $family);
		$this->db->where("question_id", $question);
		$this->db->where("date_add between '$sdate' and '$edate' ");
		$this->db->from('quisioner');
		$this->db->group_by("thedate,answer_id");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->result();
		return $total;
	}

	function dateregtotal($rowplaces, $rowdate, $places) {
		$this->db->select('DISTINCT(date(date_add)) AS thedate,' . $places . ', count(distinct(account_id)) as total');
		$this->db->where($places, $rowplaces);
		$this->db->where("date_add like", '%' . $rowdate . '%');
		$this->db->from('quisioner');
		$this->db->group_by("thedate");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->row();
		return $total;
	}

	function hourregtotal($rowplaces, $rowdate, $places) {
		$this->db->select('DISTINCT(hour(date_add)) AS thedate,' . $places . ', count(distinct(account_id)) as total');
		$this->db->where($places, $rowplaces);
		$this->db->where("HOUR(date_add)", $rowdate);
		$this->db->from('quisioner');
		$this->db->group_by("thedate");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->row();
		return $total;
	}

	function daynameregtotal($rowplaces, $rowdate, $places) {
		$this->db->select('DISTINCT(DAYNAME(date_add)) AS thedate,' . $places . ', count(distinct(account_id)) as total');
		$this->db->where($places, $rowplaces);
		$this->db->where("DAYNAME(date_add)", $rowdate);
		$this->db->from('quisioner');
		$this->db->group_by("thedate");
		$this->db->order_by("thedate", "asc");
		$total = $this->db->get()->row();
		return $total;
	}

}
