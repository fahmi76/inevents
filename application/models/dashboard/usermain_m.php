<?php

class usermain_m extends CI_Model {
    
    var $table = 'account a';
    var $column = array('a.id','a.account_displayname','a.account_rfid'); //set column field database for order and search
    var $order = array('account_displayname' => 'asc'); // default order 

    public function _get_datatables_query($places_id)
    {
        $this->db->select($this->column);
        $this->db->from($this->table);
        $this->db->join('landing b', 'b.account_id = a.id', 'inner');
        $this->db->where('b.landing_register_form', $places_id);  

        $i = 0;
        $dataarray = '';
        foreach ($this->column as $item) // loop column 
        {
            if(isset($_POST['search']['value']) && $_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND. 
                    //$this->db->like($item, $_POST['search']['value']);
                    $dataarray = '(';
                    $dataarray .= $item ." like '%".$_POST['search']['value']."%'"; 
                }
                else
                {
                    $dataarray .= "or ".$item ." like '%".$_POST['search']['value']."%'"; 
                    //$this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
                    $dataarray .= ')';
            }
            $column[$i] = $item; // set column array variable to order processing
            $i++;
        }
        if($dataarray != ''){
            $this->db->where($dataarray);
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by('b.account_id');
    }

    function get_datatables($places_id)
    {
        $this->_get_datatables_query($places_id);
        if(isset($_POST['search']['value']) && $_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($places_id)
    {
        $this->_get_datatables_query($places_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($places_id)
    {
        $this->db->from($this->table);
        $this->db->join('landing b', 'b.account_id = a.id', 'inner');
        $this->db->where('b.landing_register_form', $places_id);  
        $this->db->group_by('b.account_id');

        return $this->db->count_all_results();
    }

    function cek_session($user_id,$user_name,$admin){ 
        $hasil = array();
        if($admin == 1){
            $sql = "SELECT id,account_username,account_displayname,account_group FROM wooz_admin
                    where account_username = '".$user_name."' and id = '".$user_id."' ";
            $query = $this->db->query($sql);
            $hasil = $query->row();
        }
        return $hasil;
    }
    
}
