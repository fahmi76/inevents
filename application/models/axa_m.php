<?php

class axa_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
    }

    function getplaceslanding($custom_page) {
        $result = array();
        $sql = "SELECT id,places_landing,places_backgroud,places_logo,places_model,places_type_registration,"
                . "places_duedate,places_name,places_fbid,places_twitter,places_tw_id,places_css,"
                . "places_avatar,places_cstatus_fb,places_desc "
                . "FROM wooz_places "
                . "WHERE places_landing = '$custom_page' order by id desc";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
            $a = strtotime($data->places_duedate);
            $b = time();
            if ($a > $b) {
                $result = $data;
            }
        }
        return $result;
    }

    function check_rfid() {
        $rfid = $this->input->post('rfid');
        $loc = $this->input->post('places');
        $sql = "SELECT id as account_id,account_username FROM wooz_account 
                where account_rfid = '" . $rfid . "'
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            #$this->checkin_point($hasil->account_id);
            return $hasil;
        } else {
            $sqls = "SELECT a.account_id,b.account_username FROM wooz_landing a "
                    . "left join wooz_account b on b.id = a.account_id "
                    . "where a.landing_rfid = '" . $rfid . "'";
            if ($loc == 1) {
                $sqls .= " and a.landing_register_form in (436)";
            } else {
                $sqls .= " and a.landing_register_form in (436)";
            }
            $sqls .= " order by a.id desc";
            $querys = $this->db->query($sqls);
            $hasils = $querys->row();
            if ($hasils) {
                #$this->checkin_point($hasils->account_id);
                return $hasils;
            } else {
                return false;
            }
        }
    }

    function get_data($id, $username) {
        $sql = "SELECT id,account_username,account_displayname,account_rfid from wooz_account 
                where id = " . $id . " and account_username = '" . $username . "'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            return $hasil;
        } else {
            return false;
        }
    }
    
    function cek_financial_gate($account_id){
        $sql = "SELECT creditcard from wooz_axa_data 
                where account_id = " . $account_id;
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {            
            return $hasil->creditcard;
        } else {
            return false;
        }
    }
    
    function cek_gate_done($loc,$account_id){
        $sql = "SELECT gate from wooz_axa_gate 
                where account_id = " . $account_id ." and gate = ".$loc;
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {            
            return false; //sudah pernah masuk
        } else {
            return true; //belum pernah masuk
        }
    }
    
    function update_gate($id, $loc) {
        $input['account_id'] = $id;
        $input['gate'] = $loc;
        $input['date_add'] = date('Y-m-d H:i:s');
        $this->db->insert('axa_gate', $input);
        return true;
    }
    
    function cek_redeem($id) {
        $redeem = 0;
        $sql = "SELECT sum(id) as total FROM wooz_axa_gate WHERE account_id = " . $id . " and gate in (1,2,3,4,5,6,7,8)";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil->total >= 3) {
            $redeem = 1;
            $sqlcek = "SELECT id FROM `wooz_axa_gate` WHERE account_id = " . $id . " and redeem != 0";
            $querycek = $this->db->query($sqlcek);
            $hasilcek = $querycek->row();
            if ($hasilcek) {
                $redeem = 2;
            }
        }
        return $redeem;
    }
    
    function redeem_save($id, $loc,$redeem) {
        $input['account_id'] = $id;
        $input['gate'] = $loc;
        $input['redeem'] = $redeem;
        $input['date_add'] = date('Y-m-d H:i:s');
        $this->db->insert('axa_gate', $input);
        return true;
    }
}
