<?php

class guinnessadmin_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function cek_promo(){        
        $sql = "SELECT * FROM wooz_magnum_promo_available
                order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_promo(){         
        $sql = "SELECT id,nama_promo,point_promo,total_barang,status FROM wooz_guinness_promo
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_discount(){         
        $sql = "SELECT id,nama_discount as nama_promo,point_discount as point_promo,status FROM wooz_magnum_discount
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_birthdate(){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo_birthday
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    function get_promo_active(){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_guinness_promo
                where status = 1
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_discount_active(){         
        $sql = "SELECT id,nama_discount as nama_promo,point_discount as point_promo,status FROM wooz_magnum_discount
                where status = 1
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_birthdate_active(){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo_birthday
                where status = 1
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_detail_promo($id){         
        $sql = "SELECT id,nama_promo,point_promo,status,total_barang FROM wooz_guinness_promo where id = ".$id."
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_detail_discount($id){         
        $sql = "SELECT id,nama_discount as nama_promo,point_discount as point_promo,status FROM wooz_magnum_discount
                where id = ".$id."
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_detail_birthdate($id){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo_birthday
                where id = ".$id."
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

    function searchhistory($barang, $tgl) {
        $sql = "SELECT b.account_email,b.account_displayname,b.account_fbid,
                b.account_tw_username,c.nama_promo,a.redeem_status,a.date_add
                FROM `wooz_guinness_point` a inner join wooz_account b on b.id = a.account_id 
                inner join wooz_guinness_promo c on c.id = a.redeem_id ";
        if ($barang != 0 && $tgl != 0) {
            $sql .= "WHERE a.redeem_id = $barang and a.date_add like '%$tgl%' and a.account_id not in (36518,6765,12110,52056,1230,14334) ";
        } elseif ($barang != 0 || $tgl != 0) {
            if ($barang != 0) {
                $sql .= "WHERE a.redeem_id = $barang and a.account_id not in (36518,6765,12110,52056,1230,14334) ";
            } else {
                $sql .= "WHERE a.date_add like '%$tgl%' and a.account_id not in (36518,6765,12110,52056,1230,14334) ";
            }
        } else {
            $sql .= "where a.account_id not in (36518,6765,12110,52056,1230,14334)";
        }
        $sql .= "order by a.id desc";
		
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
	
	function dateuserreg(){
        $sql = "SELECT distinct(date(landing_joindate)) AS thedate 
				FROM `wooz_landing` 
				WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and landing_register_form = 436 and landing_joindate between '2014-04-03' and '2014-06-30' 
				group by thedate order by thedate asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;	
	}
	
	function totaluserreg($date){
		$strdate = strtotime($date) + 86400; 
		$datenext = date('Y-m-d', $strdate);
        $sql = "SELECT count(distinct(account_id)) as total
				FROM `wooz_landing` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and landing_register_form = 436 and landing_joindate between '$date 06:00:00' and '$datenext 06:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil->total;	
	}
    
	function totaluserreg1($date){
		$strdate = strtotime($date) + 86400; 
		$datenext = date('Y-m-d', $strdate);
        $sql = "SELECT distinct(a.account_id), b.account_fbid,b.account_twid 
				FROM `wooz_landing` a inner join wooz_account b on b.id = a.account_id
				WHERE a.account_id not in (36518,6765,12110,52056,1230,14334) 
				and a.landing_register_form = 436 and a.landing_joindate between '$date 06:00:00' and '$datenext 06:00:00'";
        $query = $this->db->query($sql);
        $hasil = $query->result();
		$data['user'] = count($hasil);
		$x = 0;
		$y = 0;
		foreach($hasil as $row){
			if($row->account_fbid != ''){
				$fb = $x + 1;
				$x++;
			}
			if($row->account_twid != ''){
				$tw = $y + 1;
				$y++;
			}
		}
		$data['fb'] = $fb;
		$data['tw'] = $tw;
		
        return $data;	
	}
	
	function historyreg(){
        $sql = "SELECT distinct(a.account_id),b.account_displayname,b.account_email,b.account_fbid,b.account_tw_username 
				FROM `wooz_landing` a inner join wooz_account b on b.id = a.account_id 
				WHERE a.account_id not in (36518,6765,12110,52056,1230,14334) 
				and a.`landing_register_form` = 436";
        $query = $this->db->query($sql);
        $hasil = $query->result();
		
		return $hasil;
	}
	
	function cekdatehistoryreg($date){
        $sql = "SELECT DISTINCT(DATE_FORMAT(`landing_joindate`, '%Y-%m-%d')) AS thedate 
				FROM `wooz_landing` WHERE `landing_register_form` = 436 and account_id = ".$date;
        $query = $this->db->query($sql);
        $hasil = $query->result();
		
		foreach($hasil as $row){
			$data[] = $row->thedate;
		}
		return $data;
	}
	
	function datehistoryreg(){
        $sql = "SELECT DISTINCT(DATE_FORMAT(`landing_joindate`, '%Y-%m-%d')) AS thedate 
				FROM `wooz_landing` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and `landing_register_form` = 436 order by thedate asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
		$datelog = $this->datehistorylog();
		
		foreach($hasil as $row){
			$data[] = $row->thedate;
		}
		
		foreach($datelog as $row){		
			if(!in_array($row->thedate, $data)){
				$data[] = $row->thedate;
			}
		}
		
		return $data;
	}
	
	function datehistorylog(){
        $sql = "SELECT DISTINCT(DATE_FORMAT(`log_date`, '%Y-%m-%d')) AS thedate 
				FROM `wooz_log` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and `places_id` in (436,437,438) order by thedate asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
		return $hasil;
	}
	
	function cekdatehistorylog($date){
        $sql = "SELECT DISTINCT(DATE_FORMAT(`log_date`, '%Y-%m-%d')) AS thedate 
				FROM `wooz_log` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and `places_id` in (436,437,438) and account_id = ".$date;
        $query = $this->db->query($sql);
        $hasil = $query->result();
		
		foreach($hasil as $row){
			$data[] = $row->thedate;
		}
		return $data;
	}
	
	function cekdatehistorypurchase($date){
        $sql = "SELECT DISTINCT(DATE_FORMAT(`date_add`, '%Y-%m-%d')) AS thedate
				FROM `wooz_guinness_point` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and `transaksi_id` = 4 and account_id = ".$date;
        $query = $this->db->query($sql);
        $hasil = $query->result();
		
		foreach($hasil as $row){
			$data[] = $row->thedate;
		}
		return $data;
	}
	
	function cekdatehistoryback($date){
        $sql = "SELECT DISTINCT(DATE_FORMAT(`date_add`, '%Y-%m-%d')) AS thedate 
				FROM `wooz_guinness_point` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and `transaksi_id` = 2 and account_id = ".$date;
        $query = $this->db->query($sql);
        $hasil = $query->result();
		
		foreach($hasil as $row){
			$data[] = $row->thedate;
		}
		return $data;
	}
	
	function cekdatehistoryredeem($date){
        $sql = "SELECT DISTINCT(DATE_FORMAT(`date_add`, '%Y-%m-%d')) AS thedate
				FROM `wooz_guinness_point` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and `transaksi_id` = 3 and account_id = ".$date;
        $query = $this->db->query($sql);
        $hasil = $query->result();
		
		foreach($hasil as $row){
			$data[] = $row->thedate;
		}
		return $data;
	}
	
	function totalhistoryredeem($date,$account_id){
        $sql = "SELECT count(account_id) as total
				FROM `wooz_guinness_point` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and `transaksi_id` = 3 and account_id = ".$account_id." and date_add like '%".$date."%'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
		return $hasil->total;
	}
	
	function totalhistorypurchase($date,$account_id){
        $sql = "SELECT count(account_id) as total
				FROM `wooz_guinness_point` WHERE account_id not in (36518,6765,12110,52056,1230,14334) 
				and `transaksi_id` = 4 and account_id = ".$account_id." and date_add like '%".$date."%'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
		return $hasil->total;
	}
	
    function get_point($account_id) {
        $sql = "SELECT total_point FROM wooz_guinness_point "
                . "WHERE account_id = '" . $account_id . "' order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            return $hasil->total_point;
        } else {
            return 0;
        }
    }
}
