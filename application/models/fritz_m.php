<?php

class fritz_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
    }

    function getplaceslanding($custom_page) {
        $result = array();
        $sql = "SELECT id,places_landing,places_backgroud,places_logo,places_model,places_type_registration,"
                . "places_duedate,places_name,places_fbid,places_twitter,places_tw_id,places_css,"
                . "places_avatar,places_cstatus_fb,places_desc "
                . "FROM wooz_places "
                . "WHERE places_landing = '$custom_page' order by id desc";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();
        if ($data) {
            $a = strtotime($data->places_duedate);
            $b = time();
            if ($a > $b) {
                $result = $data;
            }
        }
        return $result;
    }

    function check_rfid() {
        $rfid = $this->input->post('rfid');
        $loc = $this->input->post('places');
        $sql = "SELECT id as account_id,account_username FROM wooz_account 
                where account_rfid = '" . $rfid . "'
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            #$this->checkin_point($hasil->account_id);
            return $hasil;
        } else {
            $sqls = "SELECT a.account_id,b.account_username FROM wooz_landing a "
                    . "left join wooz_account b on b.id = a.account_id "
                    . "where a.landing_rfid = '" . $rfid . "'";
            if ($loc == 1) {
                $sqls .= " and a.landing_register_form in (258)";
            } else {
                $sqls .= " and a.landing_register_form in (258)";
            }
            $sqls .= " order by a.id desc";
            $querys = $this->db->query($sqls);
            $hasils = $querys->row();
            if ($hasils) {
                #$this->checkin_point($hasils->account_id);
                return $hasils;
            } else {
                return false;
            }
        }
    }

    function get_data($id, $username) {
        $sql = "SELECT id,account_username,account_displayname,account_rfid from wooz_account 
                where id = " . $id . " and account_username = '" . $username . "'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        if ($hasil) {
            return $hasil;
        } else {
            return false;
        }
    }

	function checkpolling($account_id){
		$sql = "SELECT date from wooz_fritz_polling 
                where account_id = " . $account_id . " order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
		if($hasil){
			$margin = time() - mysql_to_unix($hasil->date);
			$marker = 300;
			if($marker < $margin){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	function viewpolling($id){
		$sql = "SELECT count(case when polling='1' then 1 else null end) as pollingA, 
				count(case when polling='2' then 1 else null end) as pollingB   
				FROM `wooz_fritz_polling` where places_id = '$id'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
		return $hasil;
	}
	
}
