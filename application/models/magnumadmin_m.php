<?php

class magnumadmin_m extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function cek_promo(){        
        $sql = "SELECT * FROM wooz_magnum_promo_available
                order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_promo(){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_discount(){         
        $sql = "SELECT id,nama_discount as nama_promo,point_discount as point_promo,status FROM wooz_magnum_discount
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_birthdate(){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo_birthday
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    function get_promo_active(){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo
                where status = 1
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_discount_active(){         
        $sql = "SELECT id,nama_discount as nama_promo,point_discount as point_promo,status FROM wooz_magnum_discount
                where status = 1
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_birthdate_active(){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo_birthday
                where status = 1
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }
    
    function get_detail_promo($id){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo where id = ".$id."
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_detail_discount($id){         
        $sql = "SELECT id,nama_discount as nama_promo,point_discount as point_promo,status FROM wooz_magnum_discount
                where id = ".$id."
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }
    
    function get_detail_birthdate($id){         
        $sql = "SELECT id,nama_promo,point_promo,status FROM wooz_magnum_promo_birthday
                where id = ".$id."
                order by id desc";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

}
