<?php
class statuser_db extends CI_model {
    function __construct(){
        parent::__construct();
    }

    function get_monthyear(){
        $this->db->select("DATE_FORMAT(account_joindate,'%M %Y') as monthyear",FALSE);
        $this->db->from('account');
        $this->db->group_by('monthyear');
        $query = $this->db->get();

        return $query;
    }

    function get_monthyear_page($a,$b){
        $this->db->select("DATE_FORMAT(account_joindate,'%M %Y') as monthyear",FALSE);
        $this->db->from('account');
        $this->db->group_by('monthyear');
        $this->db->limit($a,$b);
        $query = $this->db->get();

        return $query;
    }

    function get_usermonth($var){
        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status !=', '0');
        $this->db->where("DATE_FORMAT(account_joindate,'%M %Y')=",$var);
        $this->db->order_by('account_joindate','DESC');
        $query = $this->db->get();

        return $query;
    }

    function get_fulldate($var){
        $this->db->select("DATE_FORMAT(account_joindate,'%d %M %Y') as fulldate",FALSE);
        $this->db->from('account');
        $this->db->where("DATE_FORMAT(account_joindate,'%M %Y')=",$var);
        $this->db->group_by('fulldate');
        $this->db->order_by('account_joindate','DESC');
        $query = $this->db->get();

        return $query;
    }

    function get_userdate($var){
        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status !=', '0');
        $this->db->where("DATE_FORMAT(account_joindate,'%d %M %Y')=",$var);
        $this->db->order_by('account_joindate','DESC');
        $query = $this->db->get();

        return $query;
    }

    function get_joindate($a,$b,$c){
        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status !=', '0');
        $this->db->where("account_joindate BETWEEN '".$a."' AND '".$b."'", null, false);
        $this->db->limit($c['limit'],$c['offset']);
        $this->db->order_by('account_joindate','DESC');
        $query = $this->db->get();

        return $query;
    }

    function num_joindate($a,$b){
        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status !=', '0');
        $this->db->where("account_joindate BETWEEN '".$a."' AND '".$b."'", null, false);
        $this->db->order_by('account_joindate','DESC');
        $query = $this->db->get()->num_rows();

        return $query;
    }

    function num_monthyear($var){
        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status !=', '0');
        $this->db->where("DATE_FORMAT(account_joindate,'%M %Y')=",$var);
        $query = $this->db->get()->num_rows();

        return $query;
    }

    function num_fulldate($var){
        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status !=', '0');
        $this->db->where("DATE_FORMAT(account_joindate,'%d %M %Y')=",$var);
        $query = $this->db->get()->num_rows();

        return $query;
    }
}