<?php

class ford_m extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }
    
    function urllanding(){
        $date = date('Y-m-d');
        $traps = array();
        $this->db->select('places_landing_url,places_duedate');
        $this->db->from('places');
        $this->db->where('places_parent',0);
        $customs = $this->db->get()->result();
        foreach($customs as $row){
            if($row->places_duedate >= $date ){
                $traps[] = $row->places_landing_url;
            }
        }
        return $traps;
    }
    
    function cekemail($email,$id){
        $data = $this->db->get_where('account', array('account_email' => $email))->result();
        if(count($data) == 1){
            $datas = array('true');
            return $datas;
        }else{
            foreach($data as $row){
                if($row->id == $id){
                    $datas = array('false');
                    return $datas;
                }
            }
        }
    }

    function cekemailfb($email){
        $this->db->select('id');
        $data = $this->db->get_where('account', array('account_email' => $email))->row();
        return $data;
    }

	
    function gettable($field,$data,$table){
        $data = $this->db->get_where($table, array($field => $data))->row();
        return $data;
        
    }
    
	function cekuser($rfid){     
		$sql = "SELECT id,account_displayname,account_rfid,account_email FROM wooz_account WHERE account_rfid = $rfid";
		$hasil = $this->db->query($sql);
		$data = $hasil->row();
		if($data){
			$message = array(
				 'rfid' => $rfid,
				 'id' => $data->id, 
				 'name' => $data->account_displayname, 
				 'phone_number' => $data->account_rfid, 
				 'email' => $data->account_email, 
				 'lang' => 2
			);
			return $message;
		}else{
			$sqls = "SELECT account_id FROM wooz_landing WHERE landing_register_form = 1 and landing_rfid = $rfid order by id desc";
			$hasils = $this->db->query($sqls);
			$datas = $hasils->row();
			if($datas){
				$sql1 = "SELECT id,account_displayname,account_rfid,account_email FROM wooz_account WHERE id = $datas->account_id";
				$hasil1 = $this->db->query($sql1);
				$data1 = $hasil1->row();
				if($data1){
					$message = array(
						 'rfid' => $rfid,
						 'id' => $data1->id, 
						 'name' => $data1->account_displayname, 
						 'phone_number' => $data1->account_rfid, 
						 'email' => $data1->account_email, 
						 'lang' => 2
					);
					return $message;
				}else{
					$message = array(
							'status' => 0,
							'message' => 'card not registered'
					);
					return $message;
				}
			}else{
				$message = array(
                        'status' => 0,
                        'message' => 'card not registered'
                );
				return $message;
			}
		}	
	}

	function updateuser($id,$name,$email,$phone){
		$data['account_displayname'] = $name;
		$data['account_email'] = $email;

		$this->db->where('id', $id);
        $this->db->update('account', $data);
		return true;
	}

	function finduser($rfid,$id){ 
		$sql = "SELECT id,account_displayname,account_fbid,account_token,account_tw_token,account_tw_secret,account_email FROM wooz_account WHERE id = $id";
		$hasil = $this->db->query($sql);
		$data = $hasil->row();
		if($data){
			return $data;
		}else{
			$sqls = "SELECT account_id FROM wooz_landing WHERE places_id = 1 and landing_rfid = $rfid order by id desc";
			$hasils = $this->db->query($sqls);
			$datas = $hasils->row();
			if($datas){
				$sql1 = "SELECT id,account_displayname,account_fbid,account_token,account_tw_token,account_tw_secret,account_email FROM wooz_account WHERE id = $datas->account_id";
				$hasil1 = $this->db->query($sql1);
				$data1 = $hasil1->row();
				return $data1;
			}else{
				return false;
			}
		}
	}

}

?>
