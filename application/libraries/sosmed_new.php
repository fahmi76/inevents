<?php

class sosmed {

    function __construct() {
        $this->CI = get_instance();
        $this->CI->load->library('curl');
        $this->CI->load->database();
        require_once APPPATH . '/third_party/tmhOAuth.php';
        require_once APPPATH . '/third_party/tmhUtilities.php';

        $app_id = $this->CI->config->item('facebook_application_id');
        $secret_key = $this->CI->config->item('facebook_secret_key');

        $this->CI->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
            'fileUpload' => true
        ));
    }

    function get_like_fb($fb_page_id, $user) {
        try {
            $likes = $this->CI->facebook->api("/" . $user . "/likes/" . $fb_page_id);
            $get_like_fb = 0;
            if (!empty($likes['data'])) {
                $get_like_fb = 1;
            }
        } catch (FacebookApiException $e) {
            $get_like_fb = 0;
        }
        return $get_like_fb;
    }

    function get_friend_fb($user_token) {
        try {
            $info = $this->CI->facebook->api(array(
                'access_token' => $user_token,
                'method' => 'fql.query',
                'query' => 'SELECT uid2 FROM friend WHERE uid1=me()',
            ));
            $get_friend_fb = count($info);
        } catch (FacebookApiException $e) {
            error_log($e);
            $get_friend_fb = 0;
        }
        return $get_friend_fb;
    }

    function get_friend_fb_show($user_token) {
        try {
            $info = $this->CI->facebook->api(array(
                'access_token' => $user_token,
                'method' => 'fql.query',
                'query' => 'SELECT uid2 FROM friend WHERE uid1=me()',
            ));
            $get_friend_fb = $info;
        } catch (FacebookApiException $e) {
            error_log($e);
            $get_friend_fb = 0;
        }
        return $get_friend_fb;
    }
    
    function cek_token_fb($account_fb_id, $account_fb_token) {
        try {
            $info = $this->CI->facebook->api(array(
                'access_token' => $account_fb_token,
                'method' => 'fql.query',
                'query' => 'SELECT uid,name FROM user WHERE uid=me()',
            ));
            if ($info) {
                return 1;
            } else {
                return 0;
            }
        } catch (FacebookApiException $e) {
            error_log($e);
            return 0;
        }
    }

    function update_fb_status($copytext, $account_fb_name, $account_fb_id, $account_fb_token) {
        $options['picture'] = 'http://wooz.in' . $copytext->places_avatar;
        $options['link'] = 'http://wooz.in/woozpot/' . $copytext->places_nicename;
        $options['caption'] = $copytext->places_name;
        $options['name'] = $copytext->places_name;
        $options['message'] = $copytext->places_cstatus_fb;
        $options['description'] = $copytext->places_desc;
        $options['access_token'] = $account_fb_token;

        try {
            $url = $this->CI->facebook->api("/$account_fb_id/feed", 'post', $options);
            $hasil = json_encode($url);
            $id_hasil = json_decode($hasil);
            $fb_status = $id_hasil->id;
        } catch (FacebookApiException $e) {
            error_log($e);
            $fb_status = 0;
        }
        return $fb_status;
    }

    function update_fb_status_fritz($copytext, $account_fb_name, $account_fb_id, $account_fb_token) {
        $options['picture'] = 'http://wooz.in' . $copytext->places_avatar;
        $options['link'] = 'http://wooz.in/woozpot/' . $copytext->places_nicename;
        $options['caption'] = $copytext->places_name;
        $options['name'] = $copytext->places_cstatus_fb;
        $options['description'] = $copytext->places_cstatus_fb;
        $options['access_token'] = $account_fb_token;

        try {
            $url = $this->CI->facebook->api("/$account_fb_id/feed", 'post', $options);
            $hasil = json_encode($url);
            $id_hasil = json_decode($hasil);
            $fb_status = $id_hasil->id;
        } catch (FacebookApiException $e) {
            error_log($e);
            $fb_status = 0;
        }
        return $fb_status;
    }

    function update_fb_status_new($copytext, $account_fb_name, $account_fb_id, $account_fb_token) {
        $options['picture'] = 'http://wooz.in' . $copytext->places_avatar;
        $options['link'] = 'http://wooz.in/woozpot/' . $copytext->places_nicename;
        $options['caption'] = $copytext->places_name;
        $options['name'] = $copytext->places_name;
        $options['description'] = $account_fb_name . ' ' . $copytext->places_cstatus_fb;
        $options['access_token'] = $account_fb_token;

        try {
            $url = $this->CI->facebook->api("/$account_fb_id/feed", 'post', $options);
            $hasil = json_encode($url);
            $id_hasil = json_decode($hasil);
            $fb_status = $id_hasil->id;
        } catch (FacebookApiException $e) {
            error_log($e);
            $fb_status = 0;
        }
        return $fb_status;
    }

    function update_fb_status_custom($copytext, $account_fb_name, $account_fb_id, $account_fb_token, $text) {
        $options['picture'] = 'http://wooz.in' . $copytext->places_avatar;
        $options['link'] = 'http://wooz.in/woozpot/' . $copytext->places_nicename;
        $options['caption'] = $copytext->places_name;
        $options['name'] = $copytext->places_name;
        $options['description'] = $text;
        $options['access_token'] = $account_fb_token;

        try {
            $url = $this->CI->facebook->api("/$account_fb_id/feed", 'post', $options);
            $hasil = json_encode($url);
            $id_hasil = json_decode($hasil);
            $fb_status = $id_hasil->id;
        } catch (FacebookApiException $e) {
            error_log($e);
            $fb_status = 0;
        }
        return $fb_status;
    }

    function upload_photo_fb($copytext, $file_name, $account_fb_name, $account_fb_id, $account_fb_token) {
        $hasil = 0; //awal
        try {
            $param = array(
                'access_token' => $account_fb_token,
                'method' => 'fql.query',
                'query' => 'SELECT object_id FROM album WHERE owner="' . $account_fb_id . '" AND name="' . $copytext->places_album . '"'
            );
            $album = $this->CI->facebook->api($param);

            if (!$album) {
                try {
                    $options = array(
                        'access_token' => $account_fb_token,
                        'name' => $copytext->places_album,
                        'message' => $copytext->places_desc
                    );
                    $album = $this->CI->facebook->api($account_fb_id . '/albums', 'POST', $options);
                    $album_id = $album['id'];
                } catch (FacebookApiException $e) {
                    $hasil = 2; //create album error
                }
            } else {
                $album_id = $album[0]['object_id'];
            }

            try {
                if ($copytext->places_fb_caption) {
                    $text = $account_fb_name . ' ' . $copytext->places_fb_caption;
                } else {
                    $text = $copytext->places_cstatus_fb;
                }
                $text = $copytext->places_fb_caption;
                /* upload photo */
                $this->CI->facebook->setFileUploadSupport(true);
                $attachement = array(
                    'access_token' => $account_fb_token,
                    'caption' => 'uploaded foto',
                    'message' => $text,
                    'source' => '@' . $file_name
                );
                $upload = $this->CI->facebook->api($album_id . '/photos', 'POST', $attachement);
                $hasil = $upload['id'];
            } catch (FacebookApiException $e) {
                $hasil = 3; //upload photo error
            }
        } catch (FacebookApiException $e) {
            error_log($e);
            $hasil = 1; //list album error
        }
        return $hasil;
    }

    function upload_photo_fb_custom($copytext, $file_name, $account_fb_name, $account_fb_id, $account_fb_token, $text) {
        $hasil = 0; //awal
        try {
            $param = array(
                'access_token' => $account_fb_token,
                'method' => 'fql.query',
                'query' => 'SELECT object_id FROM album WHERE owner="' . $account_fb_id . '" AND name="' . $copytext->places_album . '"'
            );
            $album = $this->CI->facebook->api($param);

            if (!$album) {
                try {
                    $options = array(
                        'access_token' => $account_fb_token,
                        'name' => $copytext->places_album,
                        'message' => $copytext->places_desc
                    );
                    $album = $this->CI->facebook->api($account_fb_id . '/albums', 'POST', $options);
                    $album_id = $album['id'];
                } catch (FacebookApiException $e) {
                    $hasil = 2; //create album error
                }
            } else {
                $album_id = $album[0]['object_id'];
            }

            try {
                /* upload photo */
                $this->CI->facebook->setFileUploadSupport(true);
                $attachement = array(
                    'access_token' => $account_fb_token,
                    'caption' => 'uploaded foto',
                    'message' => $text,
                    'source' => '@' . $file_name
                );
                $upload = $this->CI->facebook->api($album_id . '/photos', 'POST', $attachement);
                $hasil = $upload['id'];
            } catch (FacebookApiException $e) {
                $hasil = 3; //upload photo error
            }
        } catch (FacebookApiException $e) {
            error_log($e);
            $hasil = 1; //list album error
        }
        return $hasil;
    }

    function upload_photo_fb_page($copytext, $file_name, $account_fb_id, $account_fb_token, $album_id) {
        $hasil = 0; //awal
        try {
            /* get album photo */
            $text = $copytext->places_cstatus_fb;
            /* upload photo */
            $this->CI->facebook->setFileUploadSupport(true);
            $attachement = array(
                'access_token' => $account_fb_token,
                'caption' => 'uploaded foto',
                'message' => $text,
                'no_story' => 1,
                'source' => '@' . $file_name
            );
            $upload = $this->CI->facebook->api($album_id . '/photos', 'POST', $attachement);
            $hasil = $upload['id'];
        } catch (FacebookApiException $e) {
            error_log($e);
            $hasil = 1; //list album error
        }
        return $hasil;
    }

    function photo_tag_fb($pid, $tag_params) {
        $hasil = 0;
        try {
            $hasil = $this->CI->facebook->api('/' . $pid . '/tags', 'POST', $tag_params);
            if (!$hasil) {
                $hasil = 0;
            }
        } catch (FacebookApiException $e) {
            error_log($e);
            die;
            $hasil = 0; //list album error
        }
        return $hasil;
    }

    function like_status_fb($status_id, $fb_id, $fb_token) {
        $hasil = 0;
        try {
            $options['uid'] = $fb_id;
            $options['access_token'] = $fb_token;
            $hasil = $this->CI->facebook->api($status_id . '/likes', 'POST', $options);
            if (!$hasil) {
                $hasil = 0;
            }
        } catch (FacebookApiException $e) {
            error_log($e);
            die;
            $hasil = 0; //list album error
        }
        return $hasil;
    }

    function tw_oauth($account_tw_token, $account_tw_secret) {
        $tmhOAuth = new tmhOAuth(array(
            'consumer_key' => $this->CI->config->item('twiiter_consumer_key'),
            'consumer_secret' => $this->CI->config->item('twiiter_consumer_secret'),
            'user_token' => $account_tw_token,
            'user_secret' => $account_tw_secret,
        ));
        return $tmhOAuth;
    }

    function follow_tw($account_tw_token, $account_tw_secret, $user_tw, $tw_id) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/friendships/show'), array('source_screen_name' => $user_tw, 'target_screen_name' => $tw_id));
        $follow_tw = 0;
        if ($code == 200) {
            $response = $tmhOAuth->response['response'];
            $hasil = json_decode($response);
            $follow = $hasil->relationship->source->following;
            if ($follow == 1) {
                $follow_tw = 1;
            }
        }
        return $follow_tw;
    }

    function post_follow_tw($account_tw_token, $account_tw_secret, $tw_fan_page) {
        $result_follow = 0;
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/friendships/create'), array('user_id' => $tw_fan_page, 'follow' => 'true'));
        if ($code == 200) {
            $resp = json_encode($tmhOAuth->response['response']);
            $resp1 = json_decode($resp);
            $twit_id = json_decode($resp1);
            if ($twit_id->following) {
                $result_follow = 1;
            }
        }
        return $result_follow;
    }

    function friend_tw($account_tw_token, $account_tw_secret, $user_tw) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);

        $totalfollow = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/users/show'), array('screen_name' => $user_tw));
        $friend_tw = 0;
        if ($totalfollow == 200) {
            $responsefollow = $tmhOAuth->response['response'];
            $respfollow = json_decode($responsefollow);
            if (isset($respfollow->followers_count) && $respfollow->followers_count != '') {
                $friend_tw = $respfollow->followers_count;
            }
        }
        return $friend_tw;
    }

    function update_tw($copytext, $account_tw_token, $account_tw_secret) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $statustw = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update'), array(
            'status' => $copytext
        ));
        $tw_status = 0;
        if ($statustw == 200) {
            $resp = json_encode($tmhOAuth->response['response']);
            $resp1 = json_decode($resp);
            $twit_id = json_decode($resp1);
            $tw_status = $twit_id->id_str;
        }
        return $tw_status;
    }

    function upload_photo_tw($status, $image, $account_tw_token, $account_tw_secret) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update_with_media'), array(
            'media[]' => "@{$image};type=image/jpeg;filename={$image}",
            'status' => $status,
                ), true, // use auth
                true  // multipart
        );
        $tw_status = 0;
        if ($code == 200) {
            $resp = json_decode($tmhOAuth->response['response'], true);
            $tw_status = $resp['id_str'];
        }
        return $tw_status;
    }

    function get_profile_picture_fb($account_fb_id, $account_fb_token) {
        try {
            /* get photo pid before tagging, convert return id form upload to pid */
            $response = $this->CI->facebook->api(array(
                'access_token' => $account_fb_token,
                'method' => 'fql.query',
                'query' => 'select object_id from album where owner=me() and type="profile"',
            ));

            foreach ($response as $pid) {
                $new_pid = $pid['object_id'];
            }

            $photo = $this->CI->facebook->api(array(
                'access_token' => $account_fb_token,
                'method' => 'fql.query',
                'query' => 'select src_big from photo WHERE album_object_id = ' . $new_pid,
            ));
            $x = 1;
            foreach ($photo as $image) {
                if ($x == 1 || $x == count($photo)) {
                    if ($x == 1) {
                        $data['photoawal'] = $image['src_big'];
                    }
                    if ($x == count($photo)) {
                        $data['photoakhir'] = $image['src_big'];
                    }
                }
                $x++;
            }
        } catch (FacebookApiException $e) {
            $data = array();
        }
        return $data;
    }

    function get_profile_picture_tw($user_tw, $account_tw_token, $account_tw_secret) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/users/show'), array('screen_name' => $user_tw));
        $data = array();
        if ($code == 200) {
            $resp = json_decode($tmhOAuth->response['response'], true);
            $foto = $resp['profile_image_url'];
            $link = explode('_normal', $foto);
            $url = $link[0] . $link[1];
            $data['photoawal'] = $url;
            $data['photoakhir'] = $url;
        }
        return $data;
    }

/////////////////////////////////// Friend Zone Usage ///////////////////////////////

    function get_userdata_fb($user_token) {
        try {
            $info = $this->CI->facebook->api(array(
                'access_token' => $user_token,
                'method' => 'fql.query',
                'query' => 'SELECT uid, name, activities, affiliations, age_range,
                            birthday_date, books, current_address, current_location,
                            education, hometown_location, inspirational_people, interests,
                            movies, music, sports, tv, work
                    
                            FROM user WHERE uid = me()',
            ));
            $get_friend_fb = $info;
        } catch (FacebookApiException $e) {
            error_log($e);
            $get_friend_fb = 0;
        }
        return $get_friend_fb;
    }

    function get_userfriend_fb($user_token) {
        try {
            $info = $this->CI->facebook->api(array(
                'access_token' => $user_token,
                'method' => 'fql.query',
                'query' => 'SELECT uid, name FROM user WHERE uid = me()
                            OR uid IN (SELECT uid2 FROM friend WHERE uid1 = me())',
            ));
            $get_friend_fb = $info;
        } catch (FacebookApiException $e) {
            error_log($e);
            $get_friend_fb = 0;
        }
        return $get_friend_fb;
    }

    function check_fbtoken($user_token) {
        try {
            $info = $this->CI->facebook->api(array(
                'access_token' => $user_token,
                'method' => 'fql.query',
                'query' => 'SELECT uid                   
                            FROM user WHERE uid = me()',
            ));
            $get_friend_fb = $info;
        } catch (FacebookApiException $e) {
            error_log($e);
            $get_friend_fb = 0;
        }
        return $get_friend_fb;
    }

}

?>