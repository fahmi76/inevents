<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once( APPPATH . 'libraries/fbnew/Facebook/GraphObject.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/GraphSessionInfo.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookSession.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookCurl.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookHttpable.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookCurlHttpClient.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookResponse.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookSDKException.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookRequestException.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookAuthorizationException.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookRequest.php' );
require_once( APPPATH . 'libraries/fbnew/Facebook/FacebookRedirectLoginHelper.php' );

use Facebook\GraphSessionInfo;
use Facebook\FacebookSession;
use Facebook\FacebookCurl;
use Facebook\FacebookHttpable;
use Facebook\FacebookCurlHttpClient;
use Facebook\FacebookResponse;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphObject;

class Fbnew {

    var $ci;
    var $helper;
    var $session;

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function facebook($url) {
        FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook'));
        $this->helper = new FacebookRedirectLoginHelper($url);

        if ($this->ci->session->userdata('fb_token')) {
            $this->session = new FacebookSession($this->ci->session->userdata('fb_token'));
            // Validate the access_token to make sure it's still valid
            try {
                if (!$this->session->validate()) {
                    $this->session = false;
                }
            } catch (Exception $e) {
                // Catch any exceptions
                $this->session = false;
            }
        } else {
            try {
                $this->session = $this->helper->getSessionFromRedirect();
            } catch (FacebookRequestException $ex) {
                // When Facebook returns an error
                $return = array();
            } catch (\Exception $ex) {
                // When validation fails or other local issues
                $return = array();
            }
        }
        if ($this->session) {
            
        } else {
            $this->helper->getLoginUrl($this->ci->config->item('permissions', 'facebook'));
        }
    }

    public function facebooks($url) {
        $facebook_default_scope = $this->ci->config->item('permissions', 'facebook');
        // init app with app id and secret
        FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook'));

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper($url);
        // see if a existing session exists
        if (isset($_SESSION) && isset($_SESSION['fb_token'])) {
            // create new session from saved access_token
            $session = new FacebookSession($_SESSION['fb_token']);

            // validate the access_token to make sure it's still valid
            try {
                if (!$session->validate()) {
                    $session = null;
                }
            } catch (Exception $e) {
                // catch any exceptions
                $session = null;
            }
        }

        if (!isset($session) || $session === null) {
            // no session exists

            try {
                $session = $helper->getSessionFromRedirect();
            } catch (FacebookRequestException $ex) {
                // When Facebook returns an error
                // handle this better in production code
                print_r($ex);
            } catch (Exception $ex) {
                // When validation fails or other local issues
                // handle this better in production code
                print_r($ex);
            }
        }
        // see if we have a session
        if (isset($session)) {
            // save the session
            $_SESSION['fb_token'] = $session->getToken();
            // create a session using saved token or the new one we generated at login
            $session = new FacebookSession($session->getToken());

            // graph api request for user data
            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
            // get response
            $graphObject = $response->getGraphObject()->asArray();
            $fb_data = array(
                'me' => $graphObject,
                'token' => $session->getToken(),
                'session' => $session->getSessionInfo(),
                'loginUrl' => $helper->getLoginUrl($facebook_default_scope)
            );

            $this->ci->session->set_userdata('fb_data', $fb_data);
        } else {
            $fb_data = array(
                'me' => null,
                'token' => null,
                'loginUrl' => $helper->getLoginUrl($facebook_default_scope)
            );
            $this->ci->session->set_userdata('fb_data', $fb_data);
        }

        return $fb_data;
    }

    public function get_login_url() {
        return $this->helper->getLoginUrl($this->ci->config->item('permissions', 'facebook'));
    }

    public function get_logout_url($url,$token) {
        print_r($url);die;
        // init app with app id and secret
        FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook'));

        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper($url);      
        // see if a existing session exists
        if (isset($_SESSION) && isset($_SESSION['fb_token'])) {
            // create new session from saved access_token
            $session = new FacebookSession($_SESSION['fb_token']);

            // validate the access_token to make sure it's still valid
            try {
                if (!$session->validate()) {
                    $session = null;
                }
            } catch (Exception $e) {
                // catch any exceptions
                $session = null;
            }
        }

        if (!isset($session) || $session === null) {
            // no session exists

            try {
                $session = $helper->getSessionFromRedirect();
            } catch (FacebookRequestException $ex) {
                // When Facebook returns an error
                // handle this better in production code
                print_r($ex);
            } catch (Exception $ex) {
                // When validation fails or other local issues
                // handle this better in production code
                print_r($ex);
            }
        }
        return $helper->getLogoutUrl($token, site_url('pandara/logout'));
    }

    public function get_user() {
        if ($this->session) {
            try {
                $request = (new FacebookRequest($this->session, 'GET', '/me'))->execute();
                $user = $request->getGraphObject()->asArray();

                return $user;
            } catch (FacebookRequestException $e) {
                return false;
            }
        }
    }

    public function token() {
        if ($this->session) {
            $info = $this->session->getSessionInfo();
            // getAppId
            echo "Appid: " . $info->getAppId() . "<br />";

            if ($info->getExpiresAt()) {
                $expireDate = $info->getExpiresAt()->format('Y-m-d');
                echo 'Session expire time: ' . $expireDate . "<br />";
            }
            // session token
            echo 'Session Token: ' . $this->session->getToken() . "<br />";
        }
    }

    public function destroy($token) {
        $fb_data = array(
            'me' => null,
            'token' => null,
            'session' => null,
            'loginUrl' => null
        );
        $this->ci->session->set_userdata('fb_data', $fb_data);
        $this->session = false;
        $session = new FacebookSession($token);
        return $session;
    }

}
