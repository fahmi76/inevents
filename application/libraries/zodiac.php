<?php

class zodiac {

    function __construct() {
        $this->CI = get_instance();
        $this->CI->load->library('curl');
        $this->CI->load->database();
        require_once APPPATH . '/third_party/tmhOAuth.php';
        require_once APPPATH . '/third_party/tmhUtilities.php';

        $app_id = $this->CI->config->item('facebook_application_id');
        $secret_key = $this->CI->config->item('facebook_secret_key');

        $this->CI->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
            'fileUpload' => true
        ));
    }

	function getSign($day,$month)	{
		$this->error	= (($day > 31)		|| ($day < 0))		? "not valid day" : "";
		$this->error	= (($month > 12)	|| ($month < 0))	? "not valid month" : "";
		switch ($month)	{
			case 1:
				$this->zodiac	= ($day <=20)	? "horo-capricorn.png" : "horo-aquarius.png";
			break;
			case 2:
				$this->error	= ($day > 29)	? "February has less than ".$day." days" : "";
				$this->zodiac	= ($day <=18)	? "horo-aquarius.png" : "horo-pisces.png";
			break;
			case 3:
				$this->zodiac	= ($day <=20)	? "horo-pisces.png" : "horo-aries.png";
			break;
			case 4:
				$this->error	= ($day > 30)	? "April has less than ".$day." days" : "";
				$this->zodiac	= ($day <=20)	? "horo-aries.png" : "horo-taurus.png";
			break;
			case 5:
				$this->zodiac	= ($day <=21)	? "horo-taurus.png" : "horo-gemini.png";
			break;
			case 6:
				$this->error	= ($day > 30)	? "June has less than ".$day." days" : "";
				$this->zodiac	= ($day <=22)	? "horo-gemini.png" : "horo-cancer.png";
			break;
			case 7:
				$this->zodiac	= ($day <=22)	? "horo-cancer.png" : "horo-leo.png";
			break;
			case 8:
				$this->zodiac	= ($day <=21)	? "horo-leo.png" : "horo-virgo.png";
			break;
			case 9:
				$this->error	= ($day > 30)	? "September has less than ".$day." days" : "";
				$this->zodiac	= ($day <=23)	? "horo-virgo.png" : "horo-libra.png";
			break;
			case 10:
				$this->zodiac	= ($day <=23)	? "horo-libra.png" : "horo-scorpio.png";
			break;
			case 11:
				$this->error	= ($day > 30)	? "November has less than ".$day." days" : "";
				$this->zodiac	= ($day <=21)	? "horo-scorpio.png" : "horo-sagittarius.png";
			break;
			case 12:
				$this->zodiac	= ($day <=22)	? "horo-sagittarius.png" : "horo-capricorn.png";
			break;
		}
                return $this->zodiac;
	}

	function getSign1($day,$month)	{
		$this->error	= (($day > 31)		|| ($day < 0))		? "not valid day" : "";
		$this->error	= (($month > 12)	|| ($month < 0))	? "not valid month" : "";
		switch ($month)	{
			case 1:
				$this->zodiac	= ($day <=20)	? "capricorn" : "aquarius";
			break;
			case 2:
				$this->error	= ($day > 29)	? "February has less than ".$day." days" : "";
				$this->zodiac	= ($day <=18)	? "aquarius" : "pisces";
			break;
			case 3:
				$this->zodiac	= ($day <=20)	? "pisces" : "aries";
			break;
			case 4:
				$this->error	= ($day > 30)	? "April has less than ".$day." days" : "";
				$this->zodiac	= ($day <=20)	? "aries" : "taurus";
			break;
			case 5:
				$this->zodiac	= ($day <=21)	? "taurus" : "gemini";
			break;
			case 6:
				$this->error	= ($day > 30)	? "June has less than ".$day." days" : "";
				$this->zodiac	= ($day <=22)	? "gemini" : "cancer";
			break;
			case 7:
				$this->zodiac	= ($day <=22)	? "cancer" : "leo";
			break;
			case 8:
				$this->zodiac	= ($day <=21)	? "leo" : "virgo";
			break;
			case 9:
				$this->error	= ($day > 30)	? "September has less than ".$day." days" : "";
				$this->zodiac	= ($day <=23)	? "virgo" : "libra";
			break;
			case 10:
				$this->zodiac	= ($day <=23)	? "libra" : "scorpio";
			break;
			case 11:
				$this->error	= ($day > 30)	? "November has less than ".$day." days" : "";
				$this->zodiac	= ($day <=21)	? "scorpio" : "sagittarius";
			break;
			case 12:
				$this->zodiac	= ($day <=22)	? "sagittarius" : "capricorn";
			break;
		}
                return $this->zodiac;
	}

        
        	function getMonth($month)	{
		switch ($month)	{
			case 1:
				$this->month	= "January";
			break;
			case 2:
				$this->month	= "February";
			break;
			case 3:
				$this->month	= "March";
			break;
			case 4:
				$this->month	= "April";
			break;
			case 5:
				$this->month	= "May";
			break;
			case 6:
				$this->month	= "June";
			break;
			case 7:
				$this->month	= "july";
			break;
			case 8:
				$this->month	= "Agust";
			break;
			case 9:
				$this->month	= "September";
			break;
			case 10:
				$this->month	= "October";
			break;
			case 11:
				$this->month	= "November";
			break;
			case 12:
				$this->month	= "December";
			break;
		}
                return $this->month;
       	}
        
        
        
	function isOk()	{
		if(isset($this->error) && $this->error != "")
			return FALSE;
		else
			return TRUE;
	}

	function error()	{
		if(isset($this->error) && $this->error != "")
			return $this->error;
	}

	function displaySigns()	{
		$this->view	= $this->zodiac;
		return $this->view;
	}
    
  
}

?>