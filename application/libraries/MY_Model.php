<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends Model {

    protected $table = "";

    function  __construct() {
        parent::Model();
        $this->load->database();
    }

    /**
     * Setting the table name
     * 
     * @param string $tablename
     * @return void
     */
    function setTable($tablename=''){
        $this->table = $tablename;
        return;
    }

    /**
     * Inserting into database
     *
     * @param array $data
     * @return int id
     */
    function insert($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update the database
     *
     * @param int $id
     * @param array $data
     */
    function update($id, $data) {
        if ($id != NULL) {
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
        }
    }

    /**
     * Deleting Row
     * 
     * @param <type> $id
     */
    function delete($id) {
        if ($id != NULL) {
            $this->db->where('id', $id);
            $this->db->delete($this->table);
        }
    }
}

?>
