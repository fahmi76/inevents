<?php

class Arthurday {

    protected $token;
    protected $CI;

    function __construct($token='') {
        $this->token = $token;
        $this->CI = get_instance();
        $this->CI->load->library('curl');
        $this->CI->load->database();
    }

    function set_token($token) {
        $this->token = $token;
    }

    function get_profile() {
		try{
			$url = sprintf("https://graph.facebook.com/me?&access_token=%s", $this->token);
			$res = $this->CI->curl->simple_get($url);
			//$res = file_get_contents($url);
			return $res;
		}catch (FacebookApiException $e) {
			error_log($e);
        } 

    }

    function get_photos($limit=2) {
		try{
			$url = sprintf("https://graph.facebook.com/me/photos?limit=%d&fields=id,name,picture,link&access_token=%s", $limit, $this->token);
			$res = $this->CI->curl->simple_get($url);
			//$res = file_get_contents($url);
			return $res;
		}catch (FacebookApiException $e) {
			error_log($e);
        } 
    }

    function get_status($limit=10) {
		try{
			$url = sprintf("https://graph.facebook.com/me/posts?limit=%d&fields=id,message,created_time,caption,description&access_token=%s", $limit, $this->token);
			$res = $this->CI->curl->simple_get($url);
			//$res = file_get_contents($url);
			return $res;
		}catch (FacebookApiException $e) {
			error_log($e);
        } 
    }

    function get_interest() {
		try{
			$url = sprintf("https://graph.facebook.com/me/interests?access_token=%s", $this->token);
			$res = $this->CI->curl->simple_get($url);
			//$res = file_get_contents($url);
			return $res;
		}catch (FacebookApiException $e) {
			error_log($e);
        } 
    }

    function get_likes() {
		try{
			$url = sprintf("https://graph.facebook.com/me/likes?access_token=%s", $this->token);
			$res = $this->CI->curl->simple_get($url);
			//$res = file_get_contents($url);
			return $res;
		}catch (FacebookApiException $e) {
			error_log($e);
        } 
    }

    function get_friends() {
		try{
			$url = sprintf("https://api.facebook.com/method/friends.get?format=JSON&access_token=%s", $this->token);
			$res = count(json_decode($this->CI->curl->simple_get($url)));
			//$res = count(json_decode(file_get_contents($url)));
			return $res;
		}catch (FacebookApiException $e) {
			error_log($e);
        } 
    }

    function get_list_friends() {
		try{
			$url = sprintf("https://api.facebook.com/method/friends.get?format=JSON&access_token=%s", $this->token);
			$res = $this->CI->curl->simple_get($url);
			//$res = count(json_decode(file_get_contents($url)));
			return $res;
		}catch (FacebookApiException $e) {
			error_log($e);
        } 
    }

    /**
     * @deprecated
     */
    function get_all() {
        $profile = $this->get_profile();
        $status = $this->get_status();
        $photos = $this->get_photos();
        $interest = $this->get_interest();
        $friends = $this->get_friends();
        $lsfriends = $this->get_list_friends();
        $likes = $this->get_likes();
    }

    /**
     * memasukan ke dalam table traps
     *
     * @param int $id_user
     */
    function insert_all($id_user) {

        $this->change_traps();
        $profile = $this->get_profile();
#        $status = $this->get_status();
#        $photos = $this->get_photos();
        $interest = $this->get_interest();
        $lsfriends = $this->get_list_friends();
        $friends = count(json_decode($lsfriends));
        $likes = $this->get_likes();

        $obj_profile = json_decode($profile);
		if(isset($obj_profile->id)&&$obj_profile->id != '')
		{
			$fb_id = $obj_profile->id;

			$param['fb_about'] = $profile;
			$param['fb_photos'] = '';
			$param['fb_status'] = '';
			$param['fb_interest'] = $interest;
			$param['fb_friends'] = $friends;
			$param['fb_friendlists'] = $lsfriends;
			$param['fb_likes'] = $likes;
			$param['fb_log_id'] = 0;
			$this->insert_traps($id_user, $fb_id, $param);
		}
    }

    function insert_traps($id_user, $fb_id, $param=array()) {
        $row = $this->CI->db->get_where('traps', array('id_user' => $id_user))->row();

        $param['id_user'] = $id_user;
        $param['fb_id'] = $fb_id;
        if ($fb_id != 0 && $fb_id != '') {
            if ($row) {
                $this->CI->db->where('id_user', $id_user);
                $this->CI->db->update('traps', $param);
                $id = $row->id;
            } else {
                $this->CI->db->insert('traps', $param);
                $id = $this->CI->db->insert_id();
            }
        }
        return $id;
    }

    function set_log($id_user, $fb_log_id) {
        $param['fb_log_id'] = $fb_log_id;
        $this->CI->db->where('id_user', $id_user);
        $this->CI->db->update('traps', $param);
    }


    function check_traps() {
        $this->CI->db->select('id,fb_friends');
        $this->CI->db->from('traps');
        $row = $this->CI->db->get()->result();

        $type = 0;
        foreach ($row as $fbf) {
            if (is_numeric($fbf->fb_friends) === TRUE) {
                $type = 1;
            } else {
                $type = 0;
            }
        }

        return $type;
    }

    function change_traps() {
        if ($this->check_traps() === 0) {
            $this->CI->load->dbforge();
            $fields = array(
                'fb_friends_bak' => array('type' => 'LONGTEXT')
            );
            $this->CI->dbforge->add_column('traps', $fields);

            $this->CI->db->select('');
            $this->CI->db->from('traps');
            $row = $this->CI->db->get();

            foreach ($row->result() as $fbf) {
                $data['fb_friends_bak'] = $fbf->fb_friends;
                $this->CI->db->where('id', $fbf->id);
                $this->CI->db->update('traps', $data);
            }

            $fields2 = array(
                'fb_friends' => array(
                    'name' => 'fb_friends',
                    'type' => 'INT',
                    'constraint' => 10,
                    'unsigned' => TRUE,
                    'null' => FALSE,
                    'default' => 0
                )
            );
            $this->CI->dbforge->modify_column('traps', $fields2);

            foreach ($row->result() as $fbfr) {
                $num_friends = count(json_decode($fbfr->fb_friends));
                $data2['fb_friends'] = $num_friends;
                $this->CI->db->where('id', $fbfr->id);
                $this->CI->db->update('traps', $data2);
            }

            $this->CI->dbforge->drop_column('traps', 'fb_friends_bak');
            return true;
        } else {
            return true;
        }
    }

}
