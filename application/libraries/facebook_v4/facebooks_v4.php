<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once( APPPATH . 'libraries/facebook_v4/Facebook/GraphObject.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/GraphSessionInfo.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookSession.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookCurl.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookHttpable.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookCurlHttpClient.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookResponse.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookSDKException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRequestException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookAuthorizationException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRequest.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRedirectLoginHelper.php' );

use Facebook\GraphSessionInfo;
use Facebook\FacebookSession;
use Facebook\FacebookCurl;
use Facebook\FacebookHttpClient;
use Facebook\FacebookCurlHttpClient;
use Facebook\FacebookResponse;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphObject;

class Facebooks_v4 {

    var $ci;
    var $helper;
    var $session;

    public function __construct($url) {
        $this->ci = & get_instance();
        FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook'));
        $this->helper = new FacebookRedirectLoginHelper($url['url']);

        if ($this->ci->session->userdata('fb_token')) {
            $this->session = new FacebookSession($this->ci->session->userdata('fb_token'));

            // Validate the access_token to make sure it's still valid
            try {
                if (!$this->session->validate()) {
                    $this->session = false;
                }
            } catch (Exception $e) {
                // Catch any exceptions
                #echo '1<br />';
                #print_r($e);
                $this->session = false;
            }
        } else {
            try {
                $this->session = $this->helper->getSessionFromRedirect();
            } catch (FacebookRequestException $ex) {
                #echo '2<br />';
                #print_r($ex);
                // When Facebook returns an error
            } catch (\Exception $ex) {
                #echo '3<br />';
                #print_r($ex);
                // When validation fails or other local issues
            }
        }

        if ($this->session) {
            $this->ci->session->set_userdata('fb_token', $this->session->getToken());

            $this->session = new FacebookSession($this->session->getToken());
        }
    }

    public function get_logout_tokens($token, $url) {
        if ($token) {
            return $this->helper->getLogoutUrl_token($token, $url);
        }
        return false;
    }

    public function get_like_fb($token, $fbid, $page_id) {
        $get_like_fb = 0;
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $request = (new FacebookRequest($session, 'GET', '/' . $fbid . '/likes/' . $page_id))->execute();
                $user = $request->getGraphObject()->asArray();
                if ($user) {
                    $get_like_fb = 1;
                }
            } catch (FacebookRequestException $e) {
                $get_like_fb = 0;
            }
        } else {
            $get_like_fb = 0;
        }
        return $get_like_fb;
    }

    public function get_friend_fb($token, $fbid) {
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $request = (new FacebookRequest($session, 'GET', '/' . $fbid . '/friends'))->execute();
                $user = $request->getGraphObject()->asArray();
                if ($user) {
                    $get_friend_fb = 0;
                    if ($user['summary']) {
                        $get_friend_fb = $user['summary']->total_count;
                    }
                }else{
                    $get_friend_fb = 0;
                }
            } catch (FacebookRequestException $e) {
                $get_friend_fb = 0;
            }
        } else {
            $get_friend_fb = 0;
        }
        return $get_friend_fb;
    }

    public function update_status($param, $fbid, $token) {
        try {
            $session = new FacebookSession($token);
            $response = (new FacebookRequest(
                    $session, 'POST', '/' . $fbid . '/feed', array(
                'name' => $param['name'],
                'caption' => $param['caption'],
                'link' => $param['link']
                    )
                    ))->execute()->getGraphObject()->asArray();
            $response_id = $response['id'];
        } catch (FacebookRequestException $e) {
            $response_id = 0;
        }
        return $response_id;
    }

    public function destroy() {
        $this->ci->session->set_userdata('fb_token', '');
        $this->session = false;
        $session = new FacebookSession($this->session);
        return $session;
    }

    function get_album($venue, $image, $fb_name, $fbid, $token){        
        $album_id = '';
        
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $request = (new FacebookRequest($session, 'GET', '/' . $fbid . '/albums'))->execute();
                $user = $request->getGraphObject()->asArray();
                if ($user) {
                    foreach ($user['data'] as $row) {
                        if($row->name == $venue->places_album){
                            $album_id = $row->id;
                            break; 
                        }
                    }
                }else{
                    $album_id = '';
                }
            } catch (FacebookRequestException $e) {
                $album_id = '';
            }
        } else {
            $album_id = '';
        }
        if($album_id == ''){
            try {
                $session = new FacebookSession($token);
                $response = (new FacebookRequest(
                        $session, 'POST', '/' . $fbid . '/albums', array(
                            'name' => $venue->places_album,
                            'message' => $venue->places_desc
                        )
                        ))->execute()->getGraphObject()->asArray();
                $album_id = $response['id'];
            } catch (FacebookRequestException $e) {
                $album_id = 0;
            }
        }
        return $album_id;    
    }

    function post_photo($venue, $image, $fb_name, $fbid, $token,$album){
        $photo_id = 0;
        
        if($album == 0){
            $album = $fbid;
        }
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $response = (new FacebookRequest(
                        $session, 'POST', '/' . $album . '/photos', array(
                            'caption' => $venue->places_fb_caption,
                            #'url' => '@'.$image
                            #'source' => new CURLFile($image, 'image/jpeg'),
                            'source' => '@'.$image
                        )
                        ))->execute()->getGraphObject()->asArray();
                $photo_id = $response['id'];
            } catch (FacebookRequestException $e) {
                $photo_id = 0;
            }
        } else {
            $photo_id = 0;
        }
        return $photo_id;

    }
    function post_photo_fanpage($venue, $image, $fb_name, $fbid, $token,$album){
        $photo_id = 0;
        
        if($album == 0){
            $album = $fbid;
        }
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $response = (new FacebookRequest(
                        $session, 'POST', '/' . $album . '/photos', array(
                            'caption' => $venue->places_fb_caption,
                            #'url' => '@'.$image
                            #'source' => new CURLFile($image, 'image/jpeg'),
                            'no_story' => 1,
                            'source' => '@'.$image
                        )
                        ))->execute()->getGraphObject()->asArray();
                $photo_id = $response['id'];
            } catch (FacebookRequestException $e) {
                $photo_id = 0;
            }
        } else {
            $photo_id = 0;
        }
        return $photo_id;

    }
}
