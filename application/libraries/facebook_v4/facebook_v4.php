<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once( APPPATH . 'libraries/facebook_v4/Facebook/GraphObject.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/GraphSessionInfo.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookSession.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookCurl.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookHttpable.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookCurlHttpClient.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookResponse.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookSDKException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRequestException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookAuthorizationException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRequest.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRedirectLoginHelper.php' );

use Facebook\GraphSessionInfo;
use Facebook\FacebookSession;
use Facebook\FacebookCurl;
use Facebook\FacebookHttpable;
use Facebook\FacebookCurlHttpClient;
use Facebook\FacebookResponse;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphObject;

class Facebook_v4 {

    var $ci;
    var $helper;
    var $session;

    public function __construct($url) {
        $this->ci = & get_instance();
        FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook'));
        $this->helper = new FacebookRedirectLoginHelper($url['url']);

        if ($this->ci->session->userdata('fb_token')) {
            $this->session = new FacebookSession($this->ci->session->userdata('fb_token'));

            // Validate the access_token to make sure it's still valid
            try {
                if (!$this->session->validate()) {
                    $this->session = false;
                }
            } catch (Exception $e) {
                // Catch any exceptions
                #echo '1<br />';
                #print_r($e);
                $this->session = false;
            }
        } else {
            try {
                $this->session = $this->helper->getSessionFromRedirect();
            } catch (FacebookRequestException $ex) {
                #echo '2<br />';
                #print_r($ex);
                // When Facebook returns an error
            } catch (\Exception $ex) {
                #echo '3<br />';
                #print_r($ex);
                // When validation fails or other local issues
            }
        }

        if ($this->session) {
            $this->ci->session->set_userdata('fb_token', $this->session->getToken());

            $this->session = new FacebookSession($this->session->getToken());
        }
    }

    public function get_login_url() {
        return $this->helper->getLoginUrl($this->ci->config->item('permissions', 'facebook'));
    }

    public function get_logout_url($url) {
        if ($this->session) {
            return $this->helper->getLogoutUrl($this->session, $url);
        }
        return false;
    }

    public function get_logout_tokens($url,$token){
        if ($token) {
            return $this->helper->getLogoutUrl($token, $url);
        }
        return false;
    }
    
    public function get_user() {
        if ($this->session) {
            try {
                $request = (new FacebookRequest($this->session, 'GET', '/me'))->execute();
                $user = $request->getGraphObject()->asArray();

                return $user;
            } catch (FacebookRequestException $e) {
                return false;
            }
        }else{
            return false;
        }
    }

    public function token() {
        $token['date'] = null;
        $token['token'] = null;
        if ($this->session) {
            $info = $this->session->getSessionInfo();

            if ($info->getExpiresAt()) {
                $expireDate = $info->getExpiresAt()->format('Y-m-d');
                $token['date'] = $expireDate;
            }else{
                $token['date'] = '0000-00-00';
            }
            $token['token'] = $this->session->getToken();
            // session token
            return $token;
        }else{
            return false;
        }
    }

    public function image_pp() {
        if ($this->session) {
            try {
                $request = (new FacebookRequest($this->session, 'GET', '/me/picture?type=large&redirect=false'))->execute();
                $user = $request->getGraphObject()->asArray();
                return $user;
            } catch (FacebookRequestException $e) {
                return false;
            }
        }else{
            return false;
        }
    }

    public function destroy() {
        $this->ci->session->set_userdata('fb_token', '');
        $this->session = false;
        $session = new FacebookSession($this->session);
        return $session;
    }

}
