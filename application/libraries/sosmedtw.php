<?php

class sosmedtw {

    function __construct() {
        $this->CI = get_instance();
        $this->CI->load->library('curl');
        $this->CI->load->database();
        require_once APPPATH . '/third_party/tmhOAuth.php';
        require_once APPPATH . '/third_party/tmhUtilities.php';
        
    }

    function tw_oauth($account_tw_token, $account_tw_secret) {
        $tmhOAuth = new tmhOAuth(array(
            'consumer_key' => $this->CI->config->item('twiiter_consumer_key'),
            'consumer_secret' => $this->CI->config->item('twiiter_consumer_secret'),
            'user_token' => $account_tw_token,
            'user_secret' => $account_tw_secret,
        ));
        return $tmhOAuth;
    }

    function follow_tw($account_tw_token, $account_tw_secret, $user_tw, $tw_id) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/friendships/show'), array('source_screen_name' => $user_tw, 'target_screen_name' => $tw_id));
        $follow_tw = 0;
        if ($code == 200) {
            $response = $tmhOAuth->response['response'];
            $hasil = json_decode($response);
            $follow = $hasil->relationship->source->following;
            if ($follow == 1) {
                $follow_tw = 1;
            }
        }
        return $follow_tw;
    }

    function post_follow_tw($account_tw_token, $account_tw_secret, $tw_fan_page) {
        $result_follow = 0;
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/friendships/create'), array('user_id' => $tw_fan_page, 'follow' => 'true'));
        if ($code == 200) {
            $resp = json_encode($tmhOAuth->response['response']);
            $resp1 = json_decode($resp);
            $twit_id = json_decode($resp1);
            if ($twit_id->following) {
                $result_follow = 1;
            }
        }
        return $result_follow;
    }

    function friend_tw($account_tw_token, $account_tw_secret, $user_tw) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);

        $totalfollow = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/users/show'), array('screen_name' => $user_tw));
        $friend_tw = 0;
        if ($totalfollow == 200) {
            $responsefollow = $tmhOAuth->response['response'];
            $respfollow = json_decode($responsefollow);
            if (isset($respfollow->followers_count) && $respfollow->followers_count != '') {
                $friend_tw = $respfollow->followers_count;
            }
        }
        return $friend_tw;
    }

    function update_tw($copytext, $account_tw_token, $account_tw_secret) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $statustw = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update'), array(
            'status' => $copytext
        ));
        $tw_status = 0;
        if ($statustw == 200) {
            $resp = json_encode($tmhOAuth->response['response']);
            $resp1 = json_decode($resp);
            $twit_id = json_decode($resp1);
            $tw_status = $twit_id->id_str;
        }
        return $tw_status;
    }

    function upload_photo_tw($status, $image, $account_tw_token, $account_tw_secret) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update_with_media'), array(
            'media[]' => "@{$image};type=image/jpeg;filename={$image}",
            'status' => $status,
                ), true, // use auth
                true  // multipart
        );
        $tw_status = 0;
        if ($code == 200) {
            $resp = json_decode($tmhOAuth->response['response'], true);
            $tw_status = $resp['id_str'];
        }
        return $tw_status;
    }
    
    function get_profile_picture_tw($user_tw, $account_tw_token, $account_tw_secret) {
        $tmhOAuth = $this->tw_oauth($account_tw_token, $account_tw_secret);
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/users/show'), array('screen_name' => $user_tw));
        $data = array();
        if ($code == 200) {
            $resp = json_decode($tmhOAuth->response['response'], true);
            $foto = $resp['profile_image_url'];
            $link = explode('_normal', $foto);
            $url = $link[0] . $link[1];
            $data['photoawal'] = $url;
            $data['photoakhir'] = $url;
        }
        return $data;
    }
}
