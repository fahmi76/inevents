<?php


class like_interest {

    protected $token;
    protected $CI;

    function __construct($token='') {
        $this->token = $token;
        $this->CI = get_instance();
        $this->CI->load->library('curl');
        $this->CI->load->database();
    }

    function set_token($token) {
        $this->token = $token;
    }
    
    function get_interest() {
        $url = sprintf("https://graph.facebook.com/me/interests?access_token=%s", $this->token);
        $res = $this->CI->curl->simple_get($url);
        #xdebug($res);die;
        //$res = file_get_contents($url);
        return $res;
    }

    function get_likes() {
        $url = sprintf("https://graph.facebook.com/me/likes?access_token=%s", $this->token);
        $res = $this->CI->curl->simple_get($url);
        #xdebug($res);die;
        //$res = file_get_contents($url);
        return $res;
    }

    function get_all() {
        $interest = $this->get_interest();
        $likes = $this->get_likes();
    }

    /**
     * memasukan ke dalam table traps
     *
     * @param int $id_user
     */
    function insert_like($id_user) {
        try{
            #$url = "https://graph.facebook.com/me/likes?access_token=".$tokens;
            #$res = $this->curl->simple_get($url);

			$url = sprintf("https://graph.facebook.com/me/likes?access_token=%s", $this->token);
            $res = $this->CI->curl->simple_get($url);
            $likes = json_decode($res);
            $like_id = array();
            $like_ids = array();
            if (isset($likes->data) && count($likes->data) != 0 ) {
				
                foreach($likes->data as $row){
					if((isset($row->name)&&$row->name!='')&&(isset($row->category)&&$row->category!='')){
						$param1['category_name'] = strip_tags(trim($row->category));
						$rows_cat = $this->CI->db->get_where('like_category', array('category_name' => strip_tags(trim($row->category))))->row();
						if(!$rows_cat){
							$this->CI->db->insert('like_category', $param1);
							$id = $this->CI->db->insert_id();
							$param['category_id'] = $id;                    
						}else
							$param['category_id'] = $rows_cat->id;
							
						$param['facebook_page_name'] = strip_tags(trim($row->name));
						$param['facebook_page_id'] = $row->id;
						
						$rows = $this->CI->db->get_where('like', array('facebook_page_id' => $row->id))->row();

						if (!$rows) {
							$this->CI->db->insert('like', $param);
							$id = $this->CI->db->insert_id();
							$like_ids[] = $id;
						}else{
							$this->CI->db->where('facebook_page_id', $row->id);
							$this->CI->db->update('like', $param);
							$like_ids[] = $rows->id;
						}
                    }
                }
				#xdebug($like_ids);die;
                $this->CI->db->select('id,likes_id');
                $this->CI->db->where('account_id',$id_user);
                $this->CI->db->from('user_likes');
                $user_like = $this->CI->db->get()->result();
                $query = array();
                foreach($user_like as $row)
                {
                    $query[]=$row->likes_id;
					if(in_array($row->likes_id,$like_ids)){
						$params['user_like_lastupdate'] = date("Y-m-d H:i:s");
						$params['status'] = 1;
						$this->CI->db->where('id', $row->id);
						$this->CI->db->update('user_likes', $params);
					}
                }
                $result = array_diff($like_ids, $query);
                foreach($result as $row){
                    $params['account_id'] = $id_user;
                    $params['likes_id'] = $row;
                    $params['status'] = 1;
                    $this->CI->db->insert('user_likes', $params);
                    $id = $this->CI->db->insert_id();          
                }
                $like_id = array_merge($like_ids,$query);
				}
				if(isset($like_id) && !empty($like_id) && $like_id!="") {
					$this->CI->db->select('id');
					$this->CI->db->where('account_id',$id_user);
					$this->CI->db->where_not_in('likes_id',$like_id);
					$this->CI->db->from('user_likes');
					$next = $this->CI->db->get()->result();
					foreach($next as $row){
						$params2['user_like_lastupdate'] = date("Y-m-d H:i:s");
						$params2['status'] = 0;
						$this->CI->db->where('id', $row->id);
						$this->CI->db->update('user_likes', $params2); 
					}
				}
				if(isset($like_id))
				(unset) $like_id;
				if(isset($query))
				(unset) $query;
				if(isset($like_ids))
				(unset) $like_ids;
				if(isset($param))
				(unset) $param;
            }catch (FacebookApiException $e) {
            error_log($e);
            }           
    }

    function insert_interest($id_user){
		try{
            #$url = "https://graph.facebook.com/me/interests?access_token=".$tokens;
            #$res = $this->curl->simple_get($url);
            $url = sprintf("https://graph.facebook.com/me/interests?access_token=%s", $this->token);
			$res = $this->CI->curl->simple_get($url);
            $interests = json_decode($res);
            $interest_id = array();
            $interest_ids = array();
            if (isset($interests->data) && count($interests->data) != 0 ) {
                foreach($interests->data as $row){
					if((isset($row->name)&&$row->name!='')&&(isset($row->category)&&$row->category!='')){
						$param1['category_name'] = strip_tags(trim($row->category));
						$rows_cat = $this->CI->db->get_where('interest_category', array('category_name' => strip_tags(trim($row->category))))->row();
						if(!$rows_cat){
							$this->CI->db->insert('interest_category', $param1);
							$id = $this->CI->db->insert_id();
							$param['category_id'] = $id;                    
						}else
							$param['category_id'] = $rows_cat->id;
							
						$param['facebook_page_name'] = strip_tags(trim($row->name));
						$param['facebook_page_id'] = $row->id;
						
						$rows = $this->CI->db->get_where('interest', array('facebook_page_id' => $row->id))->row();

						if (!$rows) {
							$this->CI->db->insert('interest', $param);
							$id = $this->CI->db->insert_id();
							$interest_ids[] = $id;
						}else{
							$this->CI->db->where('facebook_page_id', $row->id);
							$this->CI->db->update('interest', $param);
							$interest_ids[] = $rows->id;
						}
                    }
                }
                $this->CI->db->select('id,interest_id');
                $this->CI->db->where('account_id',$id_user);
                $this->CI->db->from('user_interest');
                $user_interest = $this->CI->db->get()->result();
                $query = array();
                foreach($user_interest as $row)
                {
                    $query[]=$row->interest_id;
					if(in_array($row->interest_id,$interest_ids)){
						$params['user_interest_lastupdate'] = date("Y-m-d H:i:s");
						$params['status'] = 1;
						$this->CI->db->where('id', $row->id);
						$this->CI->db->update('user_interest', $params);
					}
                }
                $result = array_diff($interest_ids, $query);
                foreach($result as $row){
                    $params['account_id'] = $id_user;
                    $params['interest_id'] = $row;
                    $params['status'] = 1;
                    $this->CI->db->insert('user_interest', $params);
                    $id = $this->CI->db->insert_id();          
                }
                $interest_id = array_merge($interest_ids,$query);
				}
				if(isset($interest_id) && !empty($interest_id) && $interest_id!="") {
					$this->CI->db->select('id');
					$this->CI->db->where('account_id',$id_user);
					$this->CI->db->where_not_in('interest_id',$interest_id);
					$this->CI->db->from('user_interest');
					$next = $this->CI->db->get()->result();
					foreach($next as $row){
						$params2['user_interest_lastupdate'] = date("Y-m-d H:i:s");
						$params2['status'] = 0;
						$this->CI->db->where('id', $row->id);
						$this->CI->db->update('user_interest', $params2); 
					}
				}
				if(isset($interest_id))
				(unset) $interest_id;
				if(isset($query))
				(unset) $query;
				if(isset($interest_ids))
				(unset) $interest_ids;
				if(isset($param))
				(unset) $param;
            }catch (FacebookApiException $e) {
            error_log($e);
            }        
    }
    
}

?>
