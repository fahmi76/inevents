<?php
class Foursquare {

	/**
	 * There are 3 types of requests we can make.
	 * 1. Non-Authenticated requests - require no form of authentication to use
	 * 2. Basic-Auth requests - uses a username and password for a user to make the request
	 * 3. oAuth requests - uses oAuth to make requests
	 *
	 **/

	private $_url_api			= 'http://api.foursquare.com/v1/';
	private $_api_format		= 'json';

	private $_methods = array(
            'checkins'		=> array('http' => 'get',	'auth' => TRUE),
            'checkin'		=> array('http' => 'post',	'auth' => TRUE),
            'history'		=> array('http' => 'get',	'auth' => TRUE),
            'user'		=> array('http' => 'get',	'auth' => TRUE),
            'friends'		=> array('http' => 'get',	'auth' => TRUE),
            'venues'		=> array('http' => 'get',	'auth' => FALSE),
            'venue'		=> array('http' => 'get',	'auth' => TRUE),
            'categories'		=> array('http' => 'get',	'auth' => FALSE),
            'addvenue'		=> array('http' => 'post',	'auth' => TRUE),
            'proposeedit'		=> array('http' => 'post',	'auth' => TRUE),
            'venue/flagclosed'		=> array('http' => 'post',	'auth' => TRUE),
            'venue/flagmislocated'		=> array('http' => 'post',	'auth' => TRUE),
            'venue/flagduplicate'		=> array('http' => 'post',	'auth' => TRUE),
            'tips'		=> array('http' => 'get',	'auth' => TRUE),
            'addtip'		=> array('http' => 'post',	'auth' => TRUE),
            'tip/marktodo'		=> array('http' => 'post',	'auth' => TRUE),
            'tip/markdone'		=> array('http' => 'post',	'auth' => TRUE),
            'tip/unmark'		=> array('http' => 'post',	'auth' => TRUE),
            'tip/detail'		=> array('http' => 'get',	'auth' => TRUE),
            'todos'		=> array('http' => 'get',	'auth' => TRUE),
            'friend/requests'		=> array('http' => 'get',	'auth' => TRUE),
            'friend/approve'		=> array('http' => 'post',	'auth' => TRUE),
            'friend/deny'		=> array('http' => 'post',	'auth' => TRUE),
            'friend/sendrequest'		=> array('http' => 'post',	'auth' => TRUE),
            'test'		=> array('http' => 'get',	'auth' => FALSE),
/*
            'friend/requests'		=> array('http' => 'get',	'auth' => TRUE),
            'friend/requests'		=> array('http' => 'get',	'auth' => TRUE),
            'friend/requests'		=> array('http' => 'get',	'auth' => TRUE),
            'friend/requests'		=> array('http' => 'get',	'auth' => TRUE),
            'friend/requests'		=> array('http' => 'get',	'auth' => TRUE),
            'friend/requests'		=> array('http' => 'get',	'auth' => TRUE),
            'friend/requests'		=> array('http' => 'get',	'auth' => TRUE),
*/
         );
 

	private $_conn;
	public $oauth;

	function __construct()
	{
		$this->_conn = new Foursquare_Connection();
	}

	public function auth($username, $password)
	{
		$this->deauth();
		$this->_conn->auth($username, $password);
	}

	public function oauth($consumer_key, $consumer_secret, $access_token = NULL, $access_token_secret = NULL)
	{
                session_start();
                $this->deauth();

		$this->oauth = new EpiFoursquare($consumer_key, $consumer_secret, $access_token, $access_token_secret);
		$this->oauth->setToken($access_token, $access_token_secret);

		if ( $access_token === NULL && $access_token_secret === NULL && !isset($_GET['oauth_token']) )
		{
			$url = $this->oauth->getAuthorizeUrlFs();
                        $_SESSION['secret'] = $url['oauth_token_secret'];
                        
                        $loginurl = $url['url'] . "?oauth_token=" . $url['oauth_token'];
                        
                        header('Location: '.$loginurl);
		}
		elseif ( $access_token === NULL && $access_token_secret === NULL && isset($_GET['oauth_token']) )
		{
			$access_token = $_GET['oauth_token'];
			$this->oauth->setToken($access_token, $_SESSION['secret']);

			$info = $this->oauth->getAccessToken();

                        if ( $info->oauth_token && $info->oauth_token_secret )
			{
				$response = array(
								'access_token' => $info->oauth_token,
								'access_token_secret' => $info->oauth_token_secret
							);

				$this->oauth->setToken($response['access_token'], $response['access_token_secret']);
                                session_unset();
                                session_destroy();

				return $response;
			}
		}

		return TRUE;
	}

	public function deauth()
	{
		$this->oauth = NULL;
		$this->_conn->deauth();
	}

	public function search($method, $params = array())
	{
		$url = $this->_url_api_search.$method.'.'.$this->_api_format;

		return $this->_conn->get($url, $params);
	}

	public function call($method, $params = array())
	{
		// Firstly, assume we are using a GET non-authenticated call.

		$http = 'get';
		$auth = FALSE;

		// Now we get our http and auth options from the methods array.

		if ( isset($this->_methods[$method]) )
		{
			$http = $this->_methods[$method]['http'];
			$auth = $this->_methods[$method]['auth'];
		}

		if ( $auth === TRUE && ( $this->_conn->authed() || $this->oauth === NULL) )
		{
			// method requires auth, and we have not authed yet.
			return NULL;
		}

		if ( $this->oauth !== NULL )
		{
			$parts = explode('/', $method);

			if ( count($parts) > 1 )
			{
				$method_string = $http.'_'.$parts[0].ucfirst($parts[1]);
			}
			else
			{
				$method_string = $http.'_'.$parts[0];
			}

			$data = $this->oauth->$method_string($params);
			return $data->response;
		}

		$url = $this->_url_api . $method . '.' .$this->_api_format;

		return $this->_conn->$http($url, $params);
	}
}

class Foursquare_Connection {

	private $_curl						= NULL;
	private $_auth_method				= NULL;
	private $_auth_user					= NULL;
	private $_auth_pass					= NULL;

	function __construct()
	{
	}

	private function _init()
	{
		$this->_curl = curl_init();

		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, TRUE);

		if ( $this->_auth_method == 'basic' )
		{
			curl_setopt($this->_curl, CURLOPT_USERPWD, "$this->_auth_user:$this->_auth_pass");
		}
	}

	public function authed()
	{
		if ( $this->_auth_method === NULL ) return FALSE;

		return TRUE;
	}

	public function auth($username, $password)
	{
		$this->deauth();

		$this->_auth_method = 'basic';
		$this->_auth_user	= $username;
		$this->_auth_pass	= $password;
	}

	public function deauth($auth_method = NULL)
	{
		if ( $auth_method == 'basic' || NULL )
		{
			$this->_auth_user			= NULL;
			$this->_auth_pass			= NULL;
		}

		$this->_auth_method			= NULL;
	}

	public function get($url, $params = array())
	{
		$this->_init();

		if ( is_array($params) && !empty($params) )
		{
			$url = $url . '?' . $this->_params_to_query($params);
		}

		curl_setopt($this->_curl, CURLOPT_URL, $url);

		return $this->deserialize(curl_exec($this->_curl));
	}

	public function post($url, $params = array())
	{
		$this->_init();

		if ( is_array($params) && !empty($params) )
		{
			curl_setopt($this->_curl, CURLOPT_POSTFIELDS, $this->_params_to_query($params));
		}

		curl_setopt($this->_curl, CURLOPT_POST, TRUE);
		curl_setopt($this->_curl, CURLOPT_URL, $url);

		return $this->deserialize(curl_exec($this->_curl));
	}

	private function _params_to_query($params)
	{
		if ( !is_array($params) || empty($params) )
		{
			return '';
		}

		$query = '';

		foreach	( $params as $key => $value )
		{
			$query .= $key . '=' . $value . '&';
		}

		return substr($query, 0, strlen($query) - 1);;
	}

	private function deserialize($result)
	{
		return json_decode($result);
	}
}

/*
 * From here on, it's the EpiTwitter class and its dependencies.
 * It works pretty well, but the goal is to eventually port this all to fresh code, using common connection
 * and response libraries to the basic auth.
 */


/*
 *  Class to integrate with Foursquare's API.
 *    Authenticated calls are done using OAuth and require access tokens for a user.
 *    API calls which do not require authentication do not require tokens
 *
 *  Full documentation available on github
 *    http://wiki.github.com/jmathai/foursquare-async
 *
 *  @author Jaisen Mathai <jaisen@jmathai.com>
 */
class EpiFoursquare extends EpiOAuth
{
  const EPIFOURSQUARE_SIGNATURE_METHOD = 'HMAC-SHA1';
  const EPIFOURSQUARE_AUTH_OAUTH = 'oauth';
  const EPIFOURSQUARE_AUTH_BASIC = 'basic';
  protected $requestTokenUrl= 'http://foursquare.com/oauth/request_token';
  protected $accessTokenUrl = 'http://foursquare.com/oauth/access_token';
  protected $authorizeUrl   = 'http://foursquare.com/oauth/authorize';
  //protected $authenticateUrl= 'http://foursquare.com/oauth/authorize'; // In case four square implements sign in with like Twitter
  protected $apiUrl         = 'http://api.foursquare.com';
  protected $userAgent      = 'EpiFoursquare (http://github.com/jmathai/foursquare-async/tree/)';
  protected $apiVersion     = 'v1';
  protected $isAsynchronous = false;

  /* OAuth methods */
  public function delete($endpoint, $params = null)
  {
    return $this->request('DELETE', $endpoint, $params);
  }

  public function get($endpoint, $params = null)
  {
    return $this->request('GET', $endpoint, $params);
  }

  public function post($endpoint, $params = null)
  {
    return $this->request('POST', $endpoint, $params);
  }

  /* Basic auth methods */
  public function delete_basic($endpoint, $params = null, $username = null, $password = null)
  {
    return $this->request_basic('DELETE', $endpoint, $params, $username, $password);
  }

  public function get_basic($endpoint, $params = null, $username = null, $password = null)
  {
    return $this->request_basic('GET', $endpoint, $params, $username, $password);
  }

  public function post_basic($endpoint, $params = null, $username = null, $password = null)
  {
    return $this->request_basic('POST', $endpoint, $params, $username, $password);
  }

  public function useApiVersion($version = null)
  {
    $this->apiVersion = $version;
  }

  public function useAsynchronous($async = true)
  {
    $this->isAsynchronous = (bool)$async;
  }

  public function __construct($consumerKey = null, $consumerSecret = null, $oauthToken = null, $oauthTokenSecret = null)
  {
    parent::__construct($consumerKey, $consumerSecret, self::EPIFOURSQUARE_SIGNATURE_METHOD);
    $this->setToken($oauthToken, $oauthTokenSecret);
  }

  public function __call($name, $params = null/*, $username, $password*/)
  {
    $parts  = explode('_', $name);
    $method = strtoupper(array_shift($parts));
    $parts  = implode('_', $parts);
    $endpoint   = '/' . preg_replace('/[A-Z]|[0-9]+/e', "'/'.strtolower('\\0')", $parts) . '.json';
    /* HACK: this is required for list support that starts with a user id */
    $endpoint = str_replace('//','/',$endpoint);
    $args = !empty($params) ? array_shift($params) : null;

    // calls which do not have a consumerKey are assumed to not require authentication
    if($this->consumerKey === null)
    {
      if(!empty($params))
      {
        $username = array_shift($params);
        $password = !empty($params) ? array_shift($params) : null;
      }

      return $this->request_basic($method, $endpoint, $args, $username, $password);
    }

    return $this->request($method, $endpoint, $args);
  }

  private function getApiUrl($endpoint)
  {
    if(!empty($this->apiVersion))
      return "{$this->apiUrl}/{$this->apiVersion}{$endpoint}";
    else
      return "{$this->apiUrl}{$endpoint}";
  }

  private function request($method, $endpoint, $params = null)
  {
    $url = $this->getUrl($this->getApiUrl($endpoint));
    $resp= new EpiFoursquareJson(call_user_func(array($this, 'httpRequest'), $method, $url, $params, $this->isMultipart($params)), $this->debug);
    if(!$this->isAsynchronous)
      $resp->responseText;

    return $resp;
  }

  private function request_basic($method, $endpoint, $params = null, $username = null, $password = null)
  {
    $url = $this->getApiUrl($endpoint);
    if($method === 'GET')
      $url .= is_null($params) ? '' : '?'.http_build_query($params, '', '&');
    $ch  = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, $this->requestTimeout);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    if($method === 'POST' && $params !== null)
    {
      if($this->isMultipart($params))
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      else
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->buildHttpQueryRaw($params));
    }
    if(!empty($username) && !empty($password))
      curl_setopt($ch, CURLOPT_USERPWD, "{$username}:{$password}");

    $resp = new EpiFoursquareJson(EpiCurl::getInstance()->addCurl($ch), $this->debug);
    if(!$this->isAsynchronous)
      $resp->responseText;

    return $resp;
  }
}

class EpiFoursquareJson implements ArrayAccess, Countable, IteratorAggregate
{
  private $debug;
  private $__resp;
  public function __construct($response, $debug = false)
  {
    $this->__resp = $response;
    $this->debug  = $debug;
  }

  // ensure that calls complete by blocking for results, NOOP if already returned
  public function __destruct()
  {
    $this->responseText;
  }

  // Implementation of the IteratorAggregate::getIterator() to support foreach ($this as $...)
  public function getIterator ()
  {
    if ($this->__obj) {
      return new ArrayIterator($this->__obj);
    } else {
      return new ArrayIterator($this->response);
    }
  }

  // Implementation of Countable::count() to support count($this)
  public function count ()
  {
    return count($this->response);
  }

  // Next four functions are to support ArrayAccess interface
  // 1
  public function offsetSet($offset, $value)
  {
    $this->response[$offset] = $value;
  }

  // 2
  public function offsetExists($offset)
  {
    return isset($this->response[$offset]);
  }

  // 3
  public function offsetUnset($offset)
  {
    unset($this->response[$offset]);
  }

  // 4
  public function offsetGet($offset)
  {
    return isset($this->response[$offset]) ? $this->response[$offset] : null;
  }

  public function __get($name)
  {
    $accessible = array('responseText'=>1,'headers'=>1,'code'=>1);
    $this->responseText = $this->__resp->data;
    $this->headers      = $this->__resp->headers;
    $this->code         = $this->__resp->code;
    if(isset($accessible[$name]) && $accessible[$name])
      return $this->$name;
    elseif(($this->code < 200 || $this->code >= 400) && !isset($accessible[$name]))
      EpiFoursquareException::raise($this->__resp, $this->debug);

    // Call appears ok so we can fill in the response
    $this->response     = json_decode($this->responseText, 1);
    $this->__obj        = json_decode($this->responseText);

    if(gettype($this->__obj) === 'object')
    {
      foreach($this->__obj as $k => $v)
      {
        $this->$k = $v;
      }
    }

    if (property_exists($this, $name)) {
      return $this->$name;
    }
    return null;
  }

  public function __isset($name)
  {
    $value = self::__get($name);
    return !empty($name);
  }
}

class EpiFoursquareException extends Exception
{
  public static function raise($response, $debug)
  {
    $message = $response->data;

    switch($response->code)
    {
      case 400:
        throw new EpiFoursquareBadRequestException($message, $response->code);
      case 401:
        throw new EpiFoursquareNotAuthorizedException($message, $response->code);
      case 403:
        throw new EpiFoursquareForbiddenException($message, $response->code);
      case 404:
        throw new EpiFoursquareNotFoundException($message, $response->code);
      default:
        throw new EpiFoursquareException($message, $response->code);
    }
  }
}
class EpiFoursquareBadRequestException extends EpiFoursquareException{}
class EpiFoursquareNotAuthorizedException extends EpiFoursquareException{}
class EpiFoursquareForbiddenException extends EpiFoursquareException{}
class EpiFoursquareNotFoundException extends EpiFoursquareException{}
