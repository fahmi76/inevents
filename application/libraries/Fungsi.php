<?php
class Fungsi {

    function nicename($file) {
        $healthy = array(" ", ".",",","+","?","&","(",")","#","!","^","%","\'","@","}","{","]","[","|","","'","/","|",":",'"',"<",">",";","--");
        $yummy   = array("-", "",);
        $result	 = strtolower(str_replace($healthy, $yummy, trim($file)));
        return $result;
    }

    function excerpt($string,$max=1) {
        $get = explode(' ', strip_tags($string));
        $jml = count($get);
        $data = $get[0];
        if ($max!=1) {
            if($max<$jml) $do = $max; else $do = $jml;
            for($i=1;$i<$do;$i++) {
                $data = $data.' '.$get[$i];
            }
        }
        return $data;
    }

    function age ($date) {
        $asal = explode('-',$date);
        $now  = date('Y');
        $jadi = $now - $asal[0];
        return $jadi;
    }

    function dates($tanggal_f) {
        if(strstr($tanggal_f,' ')) {
            $waktu_f=explode(' ',$tanggal_f);
            $tanggal_asli_f=explode('-',$waktu_f[0]);
            $waktu_asli_f=explode(':',$waktu_f[1]);
            $tanggal_cetak_f=date('M d, Y', mktime($waktu_asli_f[0],$waktu_asli_f[1],$waktu_asli_f[2],$tanggal_asli_f[1],$tanggal_asli_f[2], $tanggal_asli_f[0]));
        }else {
            $tanggal_asli_f=explode('-',$tanggal_f);
            $tanggal_cetak_f=date('M d, Y', mktime(0,0,0,$tanggal_asli_f[1],$tanggal_asli_f[2], $tanggal_asli_f[0]));
        }
        return  $tanggal_cetak_f;
    }

    public function acak($masukan) {
        $pass_md5=md5(htmlentities($masukan,ENT_QUOTES));
        $pass_crypt=crypt($masukan,substr($pass_md5,7,2));
        $pass_md5=md5($pass_crypt);
        return $pass_md5;
    }

    public function search_q ($data) {
        $q = array();
        $edit1 = $this->nicename($data);
        $jadi = explode('-', $edit1);
        foreach ($jadi as $chk ) {
            if(strlen($chk)>2) $q[] = $chk;
        }
        return $q;
    }

    public function fnc_name($file) {
        $a = strlen($file);
        if($a>15) $jadi =  substr($file, 0, 14)."...";
        else $jadi = $file;
        return $jadi;
    }

    public function recreate($source) {
        $allowedTags='<i><br><br /><b><li><p><ol><strong><strike><s><em><u><ul><span>';
        $source = str_replace("'", '&apos;', $source);
		$source = str_replace("<p>&nbsp;</p>", '', $source);
        $source = strip_tags(stripslashes($source), $allowedTags);
        $source = preg_replace('/<(.*?)>/ie', "'<'.stripslashes('\\1').'>'", $source);
        $set = explode(' ', $source);
        $a = count($set);
        $b = 0;
        $jadi = '';
        while ($b<$a) {
            $jadi .= substr($set[$b], 0, 150);
            $b++;
            if($b!=$a) $jadi .= " ";
        }
        return $jadi;
    }

    public function recreate2($source) {
        $source = strip_tags(stripslashes($source));
        $source = preg_replace('/<(.*?)>/ie', "'<'.stripslashes('\\1').'>'", $source);
        $set = explode(' ', $source);
        $a = count($set);
        $b = 0;
        $jadi = '';
        while ($b<$a) {
            $jadi .= substr($set[$b], 8, 15);
            $b++;
            if($b!=$a) $jadi .= " ";
        }
        return $jadi;
    }

    public function recreate3($source) {
        $source = strip_tags(stripslashes($source));
        $source = preg_replace('/<(.*?)>/ie', "'<'.stripslashes('\\1').'>'", $source);
        $set = explode(' ', $source);
        $a = count($set);
        $b = 0;
        $jadi = '';
        while ($b<$a) {
            $jadi .= substr($set[$b], 20, 4);
            $b++;
            if($b!=$a) $jadi .= " ";
        }
        return $jadi;
    }

    public function fnc_get_extensi ($file) {
        $get = explode('.', $file);
        $result = count($get);
        return strtolower($get[$result-1]);
    }

    public function fnc_get_name ($file) {
        $get = explode('.', $file);
        $result = count($get);
        return strtolower($get[0]);
    }

    public function upload($data,$nama,$exe) {
        $temp_name = $data['tmp_name'];
        $file_name = $data['name'];
        $file_type = $data['type'];
        $file_size = $data['size'];
        $result    = $data['error'];
        $file_path = "./uploads/".$nama.'.'.strtolower($exe);

        if ($file_type=="image/gif"||$file_type=="image/jpeg"||$file_type=="image/pjpeg"||$file_type == "image/png") {
            if ( $file_size > 1200000) {
                return false;
            } else {
                $result = move_uploaded_file($temp_name, $file_path);
                if($result) return true; else return false;
            }
        } else {
            return false;
        }
    }

    public function thumb($data,$nama,$exe) {
        $temp_name = $data['tmp_name'];
        $file_name = $data['name'];
        $file_type = $data['type'];
        $file_size = $data['size'];
        $result    = $data['error'];
        $file_path = "./uploads/user/".$nama.'.'.strtolower($exe);

        if ($file_type=="image/gif"||$file_type=="image/jpeg"||$file_type=="image/pjpeg"||$file_type == "image/png") {
            if ( $file_size > 1200000) {
                return false;
            } else {
                $result = move_uploaded_file($temp_name, $file_path);
                if($result) return true; else return false;
            }
        } else {
            return false;
        }
    }

    public function fnc_edit_link ($file) {
        $get = explode('//', $file);
        $result = count($get);
        return "http://".$get[$result-1];
    }

    public function fnc_sum_string ($var_name,$min,$max) {
        if(strlen($var_name)>=$min && strlen($var_name)<=$max) {
            return true;
        }else {
            return false;
        }
    }

    public function fnc_replace_string ($file) {
        $healthy = array("'");
        $yummy   = array("&quot;");

        $newphrase = str_replace($healthy, $yummy, $file);
        return $newphrase;
    }

    function fnc_go_flag($flag) {
        if ($flag=='1')
            $data = " Defamation/Libel/Slander";
        elseif ($flag=='2')
            $data = " Copyright/Piracy issues";
        elseif ($flag=='3')
            $data = " Spam";
        elseif ($flag=='4')
            $data = " Nudity";
        elseif ($flag=='5')
            $data = " Hate or violence";
        elseif ($flag=='6')
            $data = " Impersonation";
        elseif ($flag=='7')
            $data = " Someone is posting my private information";
        else
            $data = " I think someone else is using my account";

        return  $data;
    }

	function insight_break($data) {
		$count = array_count_values($data);
		$keys = array_keys($count);

		$x = 0;
		
		foreach ($keys as $per) {
			//if( $count[$per] != '1' && $per != 'Interest' ) {
				$break[$x][0] = $count[$per];
				$break[$x][1] = $per . ' (' . $count[$per] . ')';
				$break[$x][2] = $per;
				$x++;
			//}
		}
		
		return $break;
	}
        
        function get_facebook_cookie($app_id, $app_secret) 
        {
            $args = array();
            parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
            ksort($args);
            return $args;
                    
        }
        
        function google_search_api($args, $referer = 'http://wooz.local/babi/', $endpoint = 'web')
        {
            $url = "http://ajax.googleapis.com/ajax/services/search/".$endpoint;

            if ( !array_key_exists('v', $args) )
		$args['v'] = '1.0';
 
            $url .= '?'.http_build_query($args, '', '&');
 
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // note that the referer *must* be set
            curl_setopt($ch, CURLOPT_REFERER, $referer);
            $body = curl_exec($ch);
            curl_close($ch);
            return json_decode($body);
        }

        function get_string_between($string, $start, $end)
        {
            $string = " ".$string;
            $ini = strpos($string,$start);
            if ($ini == 0) 
                return "";
            $ini += strlen($start);
            $len = strpos($string,$end,$ini) - $ini;
            return substr($string,$ini,$len);
        }

    function thumbnailer($filename, $maxw = 150, $maxh = 150) {
        $target = md5($filename).$maxw.$maxh.'.jpg';
        if(!is_file($_SERVER['DOCUMENT_ROOT'].'/uploads/thumb/'.$target)) {
            $get = explode('.', $filename);
            $result = count($get);
            $gambar = $get[$result-1];
            list($w, $h) = getimagesize($filename);

            $a = $maxw/$w;
            $b = $maxh/$h;

            if($a>$b) {
                $newwidth = (int) ($w*$a);
                $newheight = (int) ($h*$a);
                if($newwidth>$maxw) {
                    $ndx = (int) (($newwidth - $maxw)/2);
                    $dx = -$ndx;
                } else $dx = 0;
                if($newheight>$maxh) {
                    $ndy = (int) (($newheight - $maxh)/2);
                    $dy = -$ndy;
                } else $dy = 0;
            } else {
                $newwidth = (int) ($w*$b);
                $newheight = (int) ($h*$b);
                if($newwidth>$maxw) {
                    $ndx = (int) (($newwidth - $maxw)/2);
                    $dx = -$ndx;
                } else $dx = 0;
                if($newheight>$maxh) {
                    $ndy = (int) (($newheight - $maxh)/2);
                    $dy = -$ndy;
                } else $dy = 0;
            }

            $thumb = imagecreatetruecolor($maxw, $maxh);
            $white = imagecolorallocate($thumb, 255, 255, 255);

            imagefill($thumb, 0, 0, $white);

            if ($gambar=='jpg'||$gambar=='jpeg') {
                $source = imagecreatefromjpeg($filename);
            } else if ($gambar=='gif') {
                $source = imagecreatefromgif($filename);
            } else if ($gambar=='png') {
                $source = imagecreatefrompng($filename);
            }

            imagecopyresampled($thumb, $source, $dx, $dy, 0, 0, $newwidth, $newheight, $w, $h);
            imagejpeg($thumb, $_SERVER['DOCUMENT_ROOT'].'/uploads/thumb/'.$target);
            imagedestroy($thumb);
        }
        return $target;
    }

    public function errortoken($str){
        $mystring = $str;
        $token1   = 'Session has expired';
        $token2   = 'changed the password';
        $token3   = 'not authorized';
        $token4   = 'user logged out';
        
        $pos1 = strpos($mystring, $token1);
        $pos2 = strpos($mystring, $token2);
        $pos3 = strpos($mystring, $token3);
        $pos4 = strpos($mystring, $token4);

        if ($pos1 == true) {
            $jadi = 1;
        }elseif ($pos2 == true) {
            $jadi = 2;
        }elseif ($pos3 == true) {
            $jadi = 3;
        }elseif ($pos4 == true) {
            $jadi = 4;
        }else {
            $jadi = 5;
        }   
        return $jadi;        
    }

}