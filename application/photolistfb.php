<?php

class photolistfb extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
        $url = array('url' => base_url() . 'photolistfb');
        $this->load->library('facebook_v4/facebooks_v4', $url);
    }

    function main($id) {
        $sql = "SELECT account_fbid,account_token "
                . "from wooz_account_2 where id = ".$id;
        $hasil = $this->db->query($sql);
        $user = $hasil->row();
        /*
        $fbid = '100001995879634';
        $token = 'CAAC9m9VIR34BAAb9OrVkvoGHkNaWmZCNeRcDY9CEksnAKpgJcri62dy81l77tVIFma154JA3kFBhJG2mKsxAZBSKXrF5CHvCvhkVMKE9Ia6L932MOZCmTJisaCiYVDCLww8tbDu00A05Xzeijw2oWujQYZB25z1vOFZAM1BFpNl0JJlZC1VuZCPpwcWv6RldyTJZBkt9ta5nxiqEpLh4MZCtv';
        */
        $data = $this->facebooks_v4->get_album($user->account_fbid, $user->account_token);
        $content['admin'] = 3;
        $content['id'] = $id;
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('preview[]', 'preview', 'callback_preview_chk');
        if ($this->form_validation->run() === TRUE) {
            $preview = $this->input->post('preview', true);
			
            $content['image'] = $this->update_preview($preview);

            $content['error'] = 1;
            $content['text'] = "Data Berhasil Disimpan.";
            $content['content'] = $this->load->view('listphoto/notify', $content, true);
        } else {
            $content['data'] = $data;
            $content['content'] = $this->load->view('listphoto/list', $content, true);
        }

        $this->load->view('listphoto/body', $content);
    }


    function prints($image){    
        $filename = FCPATH.'/uploads/photocollage/' . $image;
        if (file_exists($filename)) {
            $content['file'] = 1;
        } else {
            $content['file'] = 2;
        }  
        $content['admin'] = 3;  
        $content['data'] = $image;
        $content['content'] = $this->load->view('listphoto/listdownloadpreview', $content, true);
        $this->load->view('listphoto/body', $content);
    }

	function done(){	
        $this->load->model('landing_m');
        $customs = $this->landing_m->getplaceslanding('hq');
		$this->custom_id = $customs->id;
        $this->tpl['card'] = 'Card';
        $this->custom_id = $customs->id;
        $this->custom_url = $customs->places_landing_url;
        $this->tpl['spot_id'] = $customs->id;
        $this->custom_model = $customs->places_model;
        $this->tpl['custom_model'] = $this->custom_model;
        $this->tpl['customs'] = $customs->places_landing_url;
        $this->tpl['background'] = $customs->places_backgroud;
        $this->tpl['logo'] = $customs->places_logo;
        $this->tpl['main_title'] = $customs->places_name;
        $this->tpl['type_registration'] = $customs->places_type_registration;
        $this->tpl['css'] = $customs->places_css;
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['page'] = "thankyou";
        $this->tpl['title'] = "Thankyou!";
        $this->tpl['TContent'] = $this->load->view('landings/signupdone1', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);
	}
	
    function preview_chk() {
        $preview = $this->input->post('preview', true);
        #if ($preview && count($preview) == 15) {
            return true;
        #}
        #$this->form_validation->set_message('preview_chk', 'Data preview image tidak memenuhi syarat');
        #return false;
    }

    function indexToCoords($index,$no,$tileWidth, $pxBetweenTiles, $leftOffSet, $topOffSet, $numberOfTiles)
    {
         $x = ($index % 2) * ($tileWidth + $pxBetweenTiles) + $leftOffSet;
         $y = floor($index / 2) * ($tileWidth + $pxBetweenTiles) + $topOffSet;
         return Array($x, $y);
    }

    function update_preview($preview) {
        #xdebug($preview);die;
        $srcImagePaths = $preview;
		$tileWidth = $tileHeight = 460;
        $numberOfTiles = 2;
        $pxBetweenTiles = 1;
        $leftOffSet = $topOffSet = 1;
         
        $mapWidth = $mapHeight = ($tileWidth + $pxBetweenTiles) * $numberOfTiles;
         
        $mapImage = imagecreatetruecolor($mapWidth, $mapHeight);
        $bgColor = imagecolorallocate($mapImage, 50, 40, 0);
        imagefill($mapImage, 0, 0, $bgColor);
        $no = 1;
        foreach ($srcImagePaths as $index => $srcImagePath)
        {
             list ($x, $y) = $this->indexToCoords($index,$no,$tileWidth, $pxBetweenTiles, $leftOffSet, $topOffSet, $numberOfTiles);
             $tileImg = imagecreatefromjpeg($srcImagePath);
             
             imagecopy($mapImage, $tileImg, $x, $y, 80, 0, $tileWidth, $tileHeight);
             imagedestroy($tileImg);
             $no++;
        }
         
        /*
         * RESCALE TO THUMB FORMAT
         */
        $thumbSize = 400;
        $thumbImage = imagecreatetruecolor($thumbSize, $thumbSize);
        imagecopyresampled($thumbImage, $mapImage, 0, 0, 0, 0, $thumbSize, $thumbSize, $mapWidth, $mapWidth);
        
        $realname = 'collage_' . time() . '.jpg';
        $filename = FCPATH.'/uploads/photocollage/' . $realname;

        imagejpeg($thumbImage, $filename, 100);
        imagedestroy($thumbImage);
        return $realname;
    }


    function downloadimage($preview) {
        $filePath = FCPATH.'/uploads/photocollage/' . $preview;
        $fileName = basename($filePath);
        $fileSize = filesize($filePath);

        // Output headers.
        header("Cache-Control: private");
        header("Content-Type: application/stream");
        header("Content-Length: ".$fileSize);
        header("Content-Disposition: attachment; filename=".$fileName);

        // Output file.
        readfile ($filePath);                   
        exit();
    }

    function cetak($preview){

    }
}
