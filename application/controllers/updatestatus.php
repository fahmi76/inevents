<?php

class updatestatus extends CI_controller {

	protected $tpl;

	function __construct() {
		parent::__construct();
		$this->tpl['TContent'] = null;
		$this->load->database();
		//$this->load->library('model');
		$this->load->library('session');
		$this->load->library('curl');
		$this->load->library('fungsi');
		$this->load->library('sosmed');
		$this->load->helper('url');
		$this->lang->load('form_validation', 'en');
		$this->load->helper('date');
		$this->tpl['assets_url'] = $this->config->item('assets_url');
		date_default_timezone_set('America/Port_of_Spain');

		$this->load->model(array('convert', 'landings_m'));
		$custom_page = $this->uri->segment(3, 0);
		if ($custom_page != '') {
			$customs = $this->landings_m->getplaceslanding($custom_page);
			if ($customs) {
				$this->station = $this->uri->segment(4, 0);
				$this->tpl['nice1'] = $custom_page;
				$this->tpl['nice2'] = $this->station;
				$this->custom_id = $customs->id;
				$this->custom_url = $custom_page;
				$this->tpl['spot_id'] = $customs->id;
				$this->custom_model = $customs->places_name;
				$this->tpl['places_name'] = $this->custom_model;
				$this->tpl['customs'] = $customs->places_landing;
				$this->tpl['background'] = $customs->places_backgroud;
				$this->tpl['logo'] = $customs->places_logo;
				$this->tpl['main_title'] = $customs->places_name;
				$this->tpl['css'] = $customs->places_css;
			} else {
				redirect('?status=custom_page_end_from_showing');
			}
		} else {
			redirect('?status=custom_page_not_found');
		}
	}

	function home() {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";
		$places = $this->station;
		$this->tpl['loc'] = $places;
		$this->db->where('places_nicename', $places);
		$this->db->from('places');
		$this->db->order_by('id', 'desc');
		$places_data = $this->db->get()->row();
		$this->tpl['title'] = $places_data->places_name . "-update";
		$this->tpl['places_image'] = $places_data->places_avatar;
		$this->tpl['places_text'] = $places_data->places_cstatus_fb;
		$this->tpl['places_booth'] = $places_data->places_name;
		$this->tpl['places_booth_id'] = $places_data->id;
		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('serial', 'Serial RFID', 'trim|required');

		if (($this->form_validation->run() === TRUE) && (isset($_POST['submit']))) {
			$rfid = $this->input->post('serial', true);
			redirect('updatestatus/home?card=' . $rfid . '&places=' . $places);
		}

		$this->tpl['TContent'] = $this->load->view('updatestatus/rfid_1', $this->tpl, true);
		$this->load->view('updatestatus/home', $this->tpl);
	}

	function updates() {
		$cserial = $this->input->post('card', true);
		$bserial = $this->input->post('booth', true);

		if ($cserial && $bserial) {
			$venue = $this->db->order_by('id', 'desc')->get_where('places', array('places_nicename' => $bserial))->row();
			if ($venue) {
				$sqluser = "select a.id,a.account_displayname,a.account_email,a.account_fbid,a.account_fb_name,a.account_token,a.account_tw_token,a.account_tw_secret FROM wooz_account a inner join wooz_landing b on b.account_id = a.id
                            where account_rfid = '" . $cserial . "' and landing_register_form = '" . $this->custom_id . "'";
				$queryuser = $this->db->query($sqluser);
				$user = $queryuser->row();

				if ($user) {
					$this->db->where('account_id', $user->id);
					$this->db->where('log_type', 1);
					$this->db->where('log_status', 1);
					$this->db->order_by('log_stamps', 'desc');
					$this->db->limit(1, 0);
					$this->db->from('log');
					$last = $this->db->get()->row();

					if ((isset($last->places_id)) && ($last->places_id == $venue->id)) {
						$marker = 22;
					} else {
						$marker = 22;
					}
					if (isset($last->log_stamps)) {
						$margin = time() - mysql_to_unix($last->log_stamps);
					} else {
						$margin = time();
					}

					if ($margin > $marker) {
						if (isset($_REQUEST['date'])) {
							$date = $sdate;
						} else {
							$date = date('Y-m-d');
						}

						if (isset($_REQUEST['time'])) {
							$time = $stime;
						} else {
							$time = date('H:i:s');
						}

						$db['account_id'] = $user->id;
						$db['places_id'] = $venue->id;
						$db['log_type'] = 1;
						$db['log_date'] = $date;
						$db['log_time'] = $time;
						$db['log_hash'] = 'v' . $this->fungsi->acak(time()) . '-' . $user->id;
						$db['log_status'] = 1;
						$upd = $this->db->insert('log', $db);
						$new = $this->db->insert_id();

						$data['status'] = false;
						$data['message']['name'] = $user->account_displayname;
						$data['message']['places'] = $venue->places_name;
						$data['message']['fb_stream'] = '0';
						$data['message']['fb_place'] = '0';
						$data['message']['fb_foto'] = '0';
						$data['message']['twitter'] = '0';

						if ($user->account_fbid != '' || $user->account_tw_token != '') {
							$image = FCPATH . $venue->places_avatar;
							if ($user->account_fbid && $user->account_token) {
								$hasil = 0;
								if ($venue->places_type == '2') {
									$url = array('url' => base_url() . 'landings/fb_token/' . $this->custom_url);
									$this->load->library('facebook_v4/facebooks_v4', $url);
									$album = 0;
									$album = $this->facebooks_v4->get_album($venue, $image, $user->account_fb_name, $user->account_fbid, $user->account_token);
									$hasil = $this->facebooks_v4->post_photo($venue, $image, $user->account_fb_name, $user->account_fbid, $user->account_token, $album);
									$this->db->update('log', array('log_fb' => $hasil), array('id' => $new));

									$data['status'] = true;
									$data['message']['fb_stream'] = '1';
									$data['message']['fb_foto'] = '1';
								} else {
									$text = $venue->places_cstatus_fb;
									$hasil = $this->sosmed->update_fb_status_custom($venue, $user->account_fb_name, $user->account_fbid, $user->account_token, $text);

									$this->db->update('log', array('log_fb' => $hasil), array('id' => $new));
									$data['status'] = true;
									$data['message']['fb_stream'] = '1';
								}
							}

							if ($user->account_tw_token && $user->account_tw_secret) {
								if ($venue->places_type == '2') {
									$hasil = $this->sosmed->upload_photo_tw($venue->places_cstatus_tw, $image, $user->account_tw_token, $user->account_tw_secret);
								} else {
									$texttw = $venue->places_cstatus_tw;
									$hasil = $this->sosmed->update_tw($texttw, $user->account_tw_token, $user->account_tw_secret);
								}

								if ($hasil != 0) {
									$this->db->update('log', array('log_tw' => $hasil), array('id' => $new));

									$data['status'] = true;
									$data['message']['twitter'] = '1';
								} else {
									$data['status'] = true;
									$data['message']['twitter'] = '0';
								}
							}

							if (!$data['status']) {
								#$data['message'] = 'user not checked in anywhere';
								$data['message'] = $user->account_displayname;
							}
							$return_json = '{"status":"' . json_encode($data['status']) . '",';
							$return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
							$return_json = $return_json . '"message2":' . json_encode($data['message']['places']) . '}';

							echo $return_json;
						} else {
							$station_id = $this->station;
							$places = $this->landings_m->emailplacesnice($station_id);
							if ($places) {
								$data['status'] = true;
								$data['message']['twitter'] = '1';
								$sqluseremail = "SELECT content FROM `wooz_form_regis_detail` where form_regis_id = 83 and account_id = '" . $user->id . "' ORDER BY `id`  DESC";
								$queryuseremail = $this->db->query($sqluseremail);
								$useremail = $queryuseremail->row();
								if ($useremail) {
									$hasil = $this->mandrill($places, $user->id, $useremail->content);
									if ($hasil != 0) {
										$this->db->update('log', array('log_fs' => $hasil), array('id' => $new));
									}
								}
								if (!$data['status']) {
									#$data['message'] = 'user not checked in anywhere';
									$data['message'] = $user->account_displayname;
								}
								$return_json = '{"status":"' . json_encode($data['status']) . '",';
								$return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
								$return_json = $return_json . '"message2":' . json_encode($data['message']['places']) . '}';

								echo $return_json;
							} else {
								$data = array(
									'status' => 0,
									'message' => array(
										'name' => $user->account_displayname,
										'error' => "account didn't connect with facebook or twitter",
									),
								);
								$return_json = '{"status":"' . json_encode($data['status']) . '",';
								$return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
								$return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

								echo $return_json;
							}
						}
					} else {
						$data = array(
							'status' => 0,
							'message' => array(
								'name' => $user->account_displayname,
								'error' => 'time limit not passed',
							),
						);
						$return_json = '{"status":"' . json_encode($data['status']) . '",';
						$return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
						$return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

						echo $return_json;
					}

				} else {
					$data = array(
						'status' => 0,
						'message' => array(
							'name' => 'Please try again',
							'error' => "card serial is not connected with any member",
						),
					);
					$return_json = '{"status":"' . json_encode($data['status']) . '",';
					$return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
					$return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

					echo $return_json;
				}
			} else {
				$data = array(
					'status' => 0,
					'message' => array(
						'name' => 'Please try again',
						'error' => 'device serial is not connected with any venue',
					),
				);
				$return_json = '{"status":"' . json_encode($data['status']) . '",';
				$return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
				$return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

				echo $return_json;
			}
		} else {
			$data = array(
				'status' => 0,
				'message' => array(
					'name' => 'Please try again',
					'error' => 'card serial and  device serial not received',
				),
			);
			$return_json = '{"status":"' . json_encode($data['status']) . '",';
			$return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
			$return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

			echo $return_json;
		}
	}

	function mandrill($emailspot, $acc_id, $email_receive) {
		$hasil_email = 0;
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;

		try {
			$this->mandrill->init($this->config->item('mandrill_api_key'));
			$mandrill_ready = TRUE;
		} catch (Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
		}
		$attch = array();
		if ($mandrill_ready) {
			$attachemail = json_decode($emailspot->places_email_attach);
			if (count($attachemail) > 0) {
				$xy = 1;
				foreach ($attachemail as $rowemail) {
					$image = FCPATH . '/uploads/apps/email/' . $rowemail;
					$attch[] = $this->mandrill->getAttachmentStruct($image);
				}
			}
			// $email_receive = 'hayria76@yahoo.com'; //test email
			//Send us some email!
			$email = array(
				'html' => $emailspot->places_custom_email, //Consider using a view file
				'subject' => $emailspot->places_subject_email,
				'from_email' => 'donotreply@wooz.in',
				'from_name' => $emailspot->places_email,
				//'to' => array(array('email' => $this->ambil_data_user($acc_id,30)))
				'to' => array(array('email' => $email_receive)),
				"attachments" => $attch,
			);
			$result = $this->mandrill->messages_send($email);
			$hasil_email = $result[0]['_id'];
		}
		return $hasil_email;
	}
}
