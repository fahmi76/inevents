<?php

class redeem extends CI_controller {

    protected $content;
    protected $limit = 2;

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('fungsi');
        $this->load->model('convert');
        $this->load->helper('date');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));
       $custom_page = $this->config->item('custom_page');
        if ($custom_page != '') {
            $customs = $this->db->get_where('places', array('places_landing' => $custom_page))->row();
            if ($customs) {
                $this->custom_id = $customs->id;
                $this->tpl['customs'] = $custom_page;
                $this->tpl['css'] = $customs->places_css;
                $this->tpl['main_title'] = $customs->places_name;
            } else {
                redirect('http://wooz.in?status=custom_page_not_registered');
            }
        } else {
            redirect('http://wooz.in?status=custom_page_not_found');
        }
    }

    function index() {
        $this->content['title'] = "User Management";

            redirect('redeem/main');

        $this->load->view('redeems/woozertoolz', $this->content);
    }


    function main() {
        $this->content['title'] = "User Management";
        $this->content['main_title'] = "wooz.in";
        $aidi = 1;
        if ($aidi == '') {
            redirect('redeem/logout');
        }

        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status', 1);
        $this->db->limit(5);
        $this->db->order_by('account_lastupdate', 'DESC');
        $this->content['lsacc'] = $this->db->get()->result();

        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status', 0);
        $this->db->limit(5);
        $this->db->order_by('account_lastupdate', 'DESC');
        $this->content['lsrem'] = $this->db->get()->result();

        $this->content['wooztoolz'] = $this->load->view('redeems/woozertoolzmain', $this->content, TRUE);
        $this->load->view('redeems/woozertoolz', $this->content);
    }

    function search() {
        $this->content['title'] = "User Result";
        $this->content['main_title'] = "wooz.in";
		$param['limit'] = $this->limit;
        $param['offset'] = $this->uri->segment(3, 0);
        $aidi = 1;
        if ($aidi == '') {
            redirect('redeem/logout');
        }

        $this->load->library('form_validation');

        $id = $this->session->userdata('aidi');
        $countacc = $this->db->get_where('account', array('id' => $id))->row();
        $this->content['woozer'] = $countacc;

        if ($this->input->post('sname') != '') {
            $this->form_validation->set_rules('sname', 'Full Name', 'trim|required');
        } elseif ($this->input->post('semail') != '') {
            $this->form_validation->set_rules('semail', 'Email', 'trim|required');
        } elseif ($this->input->post('sinstitusi') != '') {
            $this->form_validation->set_rules('sinstitusi', 'Institusi', 'trim|required');
        } else {
            $this->form_validation->set_rules('srfid', 'RFID', 'trim|required');
        }

        if ($this->form_validation->run() === TRUE) {
            $sname = $this->input->post('sname');
            $semail = $this->input->post('semail');
            $sinstitusi = $this->input->post('sinstitusi');
            $srfid = $this->input->post('srfid');
			$this->session->set_userdata('name', $sname);
			$this->session->set_userdata('email', $semail);
			$this->session->set_userdata('institusi', $sinstitusi);
			$this->session->set_userdata('rfid', $srfid);

            if ($sname != '') {
				$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
				$this->db->join('account', 'account.id = landing.account_id', 'left');
				$this->db->where('landing.landing_register_form', $this->custom_id);
                $this->db->where('account_status', 1);
                $this->db->like('account_displayname', $sname);
				$this->db->from('landing');
				$this->db->limit($param['limit'], $param['offset']);
                $lsname = $this->db->get()->result();
                $this->content['result'] = $lsname;

				$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
				$this->db->join('account', 'account.id = landing.account_id', 'left');
				$this->db->where('landing.landing_register_form', $this->custom_id);
                $this->db->where('account_status', 1);
                $this->db->like('account_displayname', $sname);
				$this->db->from('landing');
				$total = $this->db->count_all_results();

            } elseif ($semail != '') {
				$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
				$this->db->join('account', 'account.id = landing.account_id', 'left');
				$this->db->where('landing.landing_register_form', $this->custom_id);
                $this->db->where('account_status', 1);
                $this->db->like('account_email', $semail);
				$this->db->from('landing');
				$this->db->limit($param['limit'], $param['offset']);
                $lemail = $this->db->get()->result();
                $this->content['result'] = $lemail;

				$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
				$this->db->join('account', 'account.id = landing.account_id', 'left');
				$this->db->where('landing.landing_register_form', $this->custom_id);
                $this->db->where('account_status', 1);
                $this->db->like('account_email', $semail);
				$this->db->from('landing');

				$total = $this->db->count_all_results();

            } elseif ($sinstitusi != '') {
				$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
				$this->db->join('account', 'account.id = landing.account_id', 'left');
				$this->db->where('landing.landing_register_form', $this->custom_id);
                $this->db->where('account_status', 1);
                $this->db->like('account_phone', $sinstitusi);
				$this->db->from('landing');
				$this->db->limit($param['limit'], $param['offset']);
                $linstitusi = $this->db->get()->result();
                $this->content['result'] = $linstitusi;

				$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
				$this->db->join('account', 'account.id = landing.account_id', 'left');
				$this->db->where('landing.landing_register_form', $this->custom_id);
                $this->db->where('account_status', 1);
                $this->db->like('account_phone', $sinstitusi);
				$this->db->from('landing');
				$total = $this->db->count_all_results();
            } else {
                $this->db->select('');
                $this->db->from('account');
                $this->db->where('account_status', 1);
                $this->db->like('account_rfid', $srfid);
				$this->db->limit($param['limit'], $param['offset']);
                $lrfid = $this->db->get()->result();
                $this->content['result'] = $lrfid;

                $this->db->select('');
                $this->db->from('account');
                $this->db->where('account_status', 1);
                $this->db->like('account_rfid', $srfid);
				$total = $this->db->count_all_results();
            }

            $this->load->library('pagination');
            $config['base_url'] = site_url('/redeem/search/');
            $config['total_rows'] = $total;
            $config['per_page'] = $this->limit;

            $this->pagination->initialize($config);
            $this->content['paging'] = $this->pagination->create_links();
        } else {
				$sname = $this->session->userdata('name');
				$semail = $this->session->userdata('email');
				$sinstitusi = $this->session->userdata('institusi');
				$srfid = $this->session->userdata('rfid');

				if ($sname != '') {
					$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
					$this->db->join('account', 'account.id = landing.account_id', 'left');
					$this->db->where('landing.landing_register_form', $this->custom_id);
					$this->db->where('account_status', 1);
					$this->db->like('account_displayname', $sname);
					$this->db->from('landing');
					$this->db->limit($param['limit'], $param['offset']);
					$lsname = $this->db->get()->result();
					$this->content['result'] = $lsname;

					$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
					$this->db->join('account', 'account.id = landing.account_id', 'left');
					$this->db->where('landing.landing_register_form', $this->custom_id);
					$this->db->where('account_status', 1);
					$this->db->like('account_displayname', $sname);
					$this->db->from('landing');
					$total = $this->db->count_all_results();

				} elseif ($semail != '') {
					$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
					$this->db->join('account', 'account.id = landing.account_id', 'left');
					$this->db->where('landing.landing_register_form', $this->custom_id);
					$this->db->where('account_status', 1);
					$this->db->like('account_email', $semail);
					$this->db->from('landing');
					$this->db->limit($param['limit'], $param['offset']);
					$lemail = $this->db->get()->result();
					$this->content['result'] = $lemail;

					$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
					$this->db->join('account', 'account.id = landing.account_id', 'left');
					$this->db->where('landing.landing_register_form', $this->custom_id);
					$this->db->where('account_status', 1);
					$this->db->like('account_email', $semail);
					$this->db->from('landing');
					$total = $this->db->count_all_results();

				} elseif ($sinstitusi != '') {
					$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
					$this->db->join('account', 'account.id = landing.account_id', 'left');
					$this->db->where('landing.landing_register_form', $this->custom_id);
					$this->db->where('account_status', 1);
					$this->db->like('account_phone', $sinstitusi);
					$this->db->from('landing');
					$this->db->limit($param['limit'], $param['offset']);
					$linstitusi = $this->db->get()->result();
					$this->content['result'] = $linstitusi;

					$this->db->select('wooz_account.id, wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_phone,wooz_landing.landing_rfid');
					$this->db->join('account', 'account.id = landing.account_id', 'left');
					$this->db->where('landing.landing_register_form', $this->custom_id);
					$this->db->where('account_status', 1);
					$this->db->like('account_phone', $sinstitusi);
					$this->db->from('landing');
					$total = $this->db->count_all_results();
				} else {
					$this->db->select('');
					$this->db->from('account');
					$this->db->where('account_status', 1);
					$this->db->like('account_rfid', $srfid);
					$this->db->limit($param['limit'], $param['offset']);
					$lrfid = $this->db->get()->result();
					$this->content['result'] = $lrfid;

					$this->db->select('');
					$this->db->from('account');
					$this->db->where('account_status', 1);
					$this->db->like('account_rfid', $srfid);
					$total = $this->db->count_all_results();
				}

				$this->load->library('pagination');
				$config['base_url'] = site_url('/redeem/search/');
				$config['total_rows'] = $total;
				$config['per_page'] = $this->limit;

				$this->pagination->initialize($config);
				$this->content['paging'] = $this->pagination->create_links();
			}

        $this->content['wooztoolz'] = $this->load->view('redeems/woozertoolzresult', $this->content, TRUE);
        $this->load->view('redeems/woozertoolz', $this->content);
    }

    function edit() {
        $this->content['title'] = "User Edit";
        $this->content['main_title'] = "wooz.in";
        $aidi = 1;
        if ($aidi == '') {
            redirect('redeem/logout');
        }
        $id = $this->uri->segment(3, 0);

        $countacc = $this->db->get_where('account', array('id' => $id))->row();
        $this->content['ewoozer'] = $countacc;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('ename', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email');

        if ($this->form_validation->run() === TRUE) {
            $db['account_displayname'] = $this->input->post('ename', true);
            $db['account_rfid'] = $this->input->post('erfid', true);
            $db['account_email'] = $this->input->post('email', true);
            $db['account_phone'] = $this->input->post('fbuid', true);
	
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $db['account_lastupdate'] = mdate($datestring, $time);
            $this->db->where('id', $id);
            $this->db->update('account', $db);

            $landing = $this->db->get_where('landing', array('account_id' => $id, 'landing_register_form' => $this->custom_id))->row();
			$db1['landing_rfid'] = $this->input->post('erfid', true);
			$this->db->where('id', $landing->id);
            $this->db->update('landing', $db1);


            $this->content['notice'] = "User account has been successfully saved.";
        } else {
            $this->content['notice'] = "";
        }

        $this->content['wooztoolz'] = $this->load->view('redeems/woozertoolzedit', $this->content, TRUE);
        $this->load->view('redeems/woozertoolz', $this->content);
    }
/*
    function code() {
        $this->content['title'] = "Registration Code";
        $this->content['main_title'] = "wooz.in";

        $this->load->library('form_validation');
        $this->form_validation->set_rules('code', 'Registration Code', 'trim|required|callback_thecode');
        if ($this->form_validation->run() === TRUE) {
            $code = $this->input->post('code');
            $getacc = $this->db->get_where('account', array('account_verify' => $code))->row();
            
            redirect('redeem/rfidcode/'.$getacc->id.'/'.$getacc->account_verify);
        }

        $this->content['wooztoolz'] = $this->load->view('redeems/woozertoolzcode', $this->content, TRUE);
        $this->load->view('redeems/woozertoolz', $this->content);
    }

    function rfidcode() {
        $this->content['title'] = "User Information";
        $this->content['main_title'] = "wooz.in";

        #$user = $this->session->userdata('infouser');
		$user = $this->uri->segments[3];
		$code = $this->uri->segments[4];
        $infouser = $this->db->get_where('account', array('id' => $user, 'account_verify' => $code))->row();

        $this->content['name'] = $infouser->account_displayname;
        $this->content['location'] = $infouser->account_schcmp;
        $this->content['email'] = $infouser->account_email;
        $this->content['rfid'] = $infouser->account_rfid;
        $this->content['lastupdate'] = $infouser->account_lastupdate;
		
		if(isset($this->content['rfid']) && $this->content['rfid'] == 0){
			$this->content['data'] = 1;
		}
        $this->load->library('form_validation');
        if (isset($_POST['rfidcode'])) {
            $coderfid['account_rfid'] = $_POST['rfidcode'];
            $coderfid['account_lastupdate'] = date("Y-m-d H:i:s");

            if ($_POST['rfidcode'] != '') {
                $this->db->where('id', $infouser->id);
                $this->db->update('account', $coderfid);
            }

			$options['picture'] = 'http://wooz.in/uploads/apps/MSlogo.png';
            $options['message'] = "I've just checked in to #MSCampfire, ready for two days of exciting web and mobile action! http://spiffy.sg/developers/mscampfire-highlights-get-the-latest-updates";
            $options['name'] = 'MSCampfire 2012';
            $options['caption'] = 'wooz.in update';
            $options['description'] = 'Campfire is an action-packed conference designed especially for developers, designers, UX experts and entrepreneurs in the web technology space.';
            $options['link'] = 'http://spiffy.sg/developers/mscampfire-highlights-get-the-latest-updates';
										
			try{
				$url = $this->facebook->api("/$infouser->account_fbid/feed", 'post', $options);
				$hasil = json_encode($url);
				$id_hasil = json_decode($hasil); 
            }catch (FacebookApiException $e) {
				error_log($e);
			}

            $this->content['msgcode'] = 1;
        } else {
            $this->content['msgcode'] = '';
        }

        $this->content['wooztoolz'] = $this->load->view('redeems/woozertoolzrfidcode', $this->content, TRUE);
        $this->load->view('redeems/woozertoolz', $this->content);
    }
*/
    function thecode() {
        $str = $this->input->post('code');
        $new = $this->db->get_where('account', array('account_verify' => $str))->row();
        if (count($new) == 0) {
            $this->form_validation->set_message('thecode', 'A registration code is not found. Please register first.');
            return false;
        } else {
            return true;
        }
    }
	/*
    function thecode() {
        $str = $this->input->post('code');
        $new = $this->db->get_where('registration_code', array('fbpages_code' => $str))->row();
        if (count($new) == 0) {
            $this->form_validation->set_message('thecode', 'A registration code is not found. Please register first.');
            return false;
        } else {
            return true;
        }
    }
	*/
    function email() {
        $str = $this->input->post('email');
        $new = $this->db->get_where('account', array('account_email' => $str))->row();
        if (count($new) == 0) {
            $this->form_validation->set_message('email', 'Email Not Found.');
            return false;
        } else {
            return true;
        }
    }

    function pass() {
        $str = $this->fungsi->acak($this->input->post('pass', true));
        $new = $this->db->get_where('account', array('account_passwd' => $str))->row();
        if (count($new) == 0) {
            $this->form_validation->set_message('pass', 'Password Wrong.');
            return false;
        } else {
            return true;
        }
    }
}

?>
