<?php

class gate extends CI_controller {

    protected $tpl;

    function __construct() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        //$this->load->library('model');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->library('sosmed');
        $this->load->helper('url');
        $this->lang->load('form_validation', 'en');
        $this->load->helper('date');
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        date_default_timezone_set('America/Port_of_Spain');

        #$custom_page = $this->input->get('place', true);
        $custom_page = 'guardian';
        if ($custom_page != '') {
            $customs = $this->db->get_where('places', array('places_landing' => $custom_page))->row();
            if ($customs) {
                $a = strtotime($customs->places_duedate);
                $b = time();
                if ($customs->places_duedate != '0000-00-00' && $a > $b) {
                    $this->custom_id = $customs->id;
                    $this->custom_url = $customs->places_landing;
                    $this->spot_id = $this->input->get('places', true);
                    $this->twitter_name = $customs->places_twitter;
                    $this->tpl['spot_id'] = $this->input->get('place', true);
                    #$this->custom_model = $customs->places_model;
                    $this->tpl['customs'] = $customs->places_landing;
                    $this->tpl['css'] = $customs->places_css;
                    $this->tpl['background'] = $customs->places_backgroud;
                    $this->tpl['logo'] = $customs->places_logo;
                    $this->tpl['main_title'] = $customs->places_name;
                    $this->tpl['type_registration'] = 1;
                } else {
                    redirect('?status=' . $custom_page . '_custom_page_end_from_showing');
                }
            } else {
                redirect('?status=custom_page_not_registered');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index(){
        $this->tpl['page'] = "home";
        $this->tpl['body_class'] = '';
        $this->content['title'] = "| DS - tap";

        $this->db->from('gate');
        $this->tpl['gate'] = $this->db->get()->result();

        $this->tpl['TContent'] = $this->load->view('gate/listplaces', $this->tpl, true);
        $this->load->view('gate/home', $this->tpl);
    }

    function home($check,$gate) {
        $this->tpl['check'] = $check;
        $this->tpl['gate'] = $gate;
        $this->tpl['page'] = "home";
        $this->tpl['body_class'] = '';
        $this->tpl['title'] = "Gate";
        $this->tpl['already'] = 0;

        $this->db->where('id', $gate);
        $this->db->from('gate');
        $places_data = $this->db->get()->row();
        $this->tpl['places_name'] = $places_data->nama;

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('serial', 'Serial RFID', 'trim|required');

        if (($this->form_validation->run() === TRUE) && (isset($_POST['submit']))) {
            $rfid = $this->input->post('serial', true);
            $data = $this->cek_user_gate($rfid,$gate);
            if ($data['status'] == 1 || $data['status'] == 2) {
                $this->tpl['datauser'] = $data['data'];
                $this->tpl['datastatus'] = $data['status'];
				$this->tpl['already'] = $this->cek_gate($gate,$check,$data['data']['id']);
                $this->tpl['TContent'] = $this->load->view('gate/found', $this->tpl, true);
            }else {
                $this->tpl['TContent'] = $this->load->view('gate/notfound', $this->tpl, true);
            }
        } else {
            $this->tpl['TContent'] = $this->load->view('gate/rfid_1', $this->tpl, true);
        }
        $this->load->view('gate/home', $this->tpl);
    }

    function done($check,$gate,$id) {
        $date = date('Y-m-d');
        $time = date('h:i:s');
        $db['account_id'] = $id;
        $db['places_id'] = $this->custom_id;
        $db['log_type'] = 1;
        $db['log_date'] = $date;
        $db['log_time'] = $time;
        $db['log_gate'] = $gate;
        $db['log_check'] = $check;
        $db['log_hash'] = 'v' . $this->fungsi->acak(time()) . '-' . $id;
        $db['log_status'] = 1;
		$db['log_stamps'] = date('Y-m-d H:i:s');
        $upd = $this->db->insert('log_user_gate', $db);
        if($gate == 13){
            if($check == 1){
                $newdata['account_status'] = 3;
            }else{
                $newdata['account_status'] = 4;
            }
            $this->db->where('id', $id);
            $this->db->update('account', $newdata);
        }
        redirect('gate/home/'.$check.'/'.$gate);
    }
	
    function failed($check,$gate,$id) {
        $date = date('Y-m-d');
        $time = date('h:i:s');
        $db['account_id'] = $id;
        $db['places_id'] = $this->custom_id;
        $db['log_type'] = 2;
        $db['log_date'] = $date;
        $db['log_time'] = $time;
        $db['log_gate'] = $gate;
        $db['log_check'] = $check;
        $db['log_hash'] = 'v' . $this->fungsi->acak(time()) . '-' . $id;
        $db['log_status'] = 2;
		$db['log_stamps'] = date('Y-m-d H:i:s');
        $upd = $this->db->insert('log_user_gate', $db);
        redirect('gate/home/'.$check.'/'.$gate);
    }

    function listuser() {
        $this->tpl['page'] = "home";
        $this->tpl['body_class'] = '';
        $this->content['title'] = "| DS - tap";

        $sql = "select distinct(a.account_id),b.account_displayname,b.account_avatar,a.log_time "
                . "from wooz_account_pre b inner join wooz_log_user a on a.account_id = b.id "
                . "group by a.account_id order by a.id desc";
        $query = $this->db->query($sql);
        $this->tpl['data'] = $query->result();
        $this->tpl['TContent'] = $this->load->view('gate/listuser', $this->tpl, true);
        $this->load->view('gate/home', $this->tpl);
    }


    function cek_user_gate($rfid,$gate){
        $hasil = array(
                        'status' => 0,
                        'data' => array()
                    );
        $sql = "select a.id,a.account_displayname,a.account_email,a.account_avatar,"
                ."d.nama,d.gate_id,c.seat "
                ."from wooz_account a left join wooz_seat_account c on c.account_id = a.id "
                ."left join wooz_visitor d on d.id = c.visitor_id where a.account_rfid =  '" . $rfid . "'";
        $query = $this->db->query($sql);
        $data = $query->row();
        if($data){
            if(is_array(json_decode($data->gate_id))){
                $gates = json_decode($data->gate_id);
            }else{
                $gates = array();
            }
            
            if(in_array($gate, $gates)){
                $hasil = array(
                        'status' => 1,
                        'data' => array(
                                'id' => $data->id,
                                'name' => $data->account_displayname,
                                'email' => $data->account_email,
                                'avatar' => $data->account_avatar,
                                'visitor' => $data->nama,
                                'seat' => $data->seat
                            )
                    );
            }else{
                $hasil = array(
                        'status' => 2,
                        'data' =>  array(
                                'id' => $data->id,
                                'name' => $data->account_displayname,
                                'email' => $data->account_email,
                                'avatar' => $data->account_avatar,
                                'visitor' => $data->nama,
                                'seat' => $data->seat
                            )
                    );
            }
        }
        return $hasil;
    }

    function cek_gate($gate,$check,$acc_id){
        $hasil = 0;
        $sql = "select log_stamps from wooz_log_user_gate "
                ."where account_id = '".$acc_id."' and log_gate = '".$gate."' and log_check = '".$check."' order by id desc ";
        $query = $this->db->query($sql);
        $data = $query->row();
        if($data){
            $margin = time() - mysql_to_unix($data->log_stamps);
            if($margin <= 60){
                $hasil = 1;
            }
        }
        return $hasil;
    }
}
