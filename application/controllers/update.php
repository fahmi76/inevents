<?php

class update extends CI_controller {

    protected $tpl;

    function update() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
        //$this->load->library('model');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->library('twitter');
        $this->load->library('EpiFoursquare');
        $this->load->library('bitly', array(
            'bitly_login' => $this->config->item('bitly_login'),
            'bitly_apiKey' => $this->config->item('bitly_apiKey')
        ));
        $this->load->helper('url');
        $this->load->helper('date');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');

		include(APPPATH.'third_party/tmhOAuth.php');
		include(APPPATH.'third_party/tmhUtilities.php');

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));
    }

    function index() {
		$tes_mk_sta = mktime();
        $api_key = $this->config->item('facebook_api_key');
        $cserial = (int) strtolower($_REQUEST['card']);
        $bserial = strtolower($_REQUEST['places']);
        if ($cserial && $bserial) {
            $venue = $this->db->get_where('places', array('id' => $bserial, 'places_status' => 1))->row();
            if ($venue) {
                $isMulti = $this->db->get_where('card', array('card_number LIKE' => $cserial, 'card_status !=' => 0))->row();
                if ($isMulti) {
                    if ($isMulti->card_status == 2)
                        $test = true;
					else
                        $test = false;
                    $user = $this->db->get_where('account', array('id' => $isMulti->account_id, 'account_status' => 1))->row();
                } else {
                    $test = false;
                    $user = $this->db->get_where('account', array('account_rfid' => $cserial, 'account_status' => 1))->row();
					if(empty($user)){
						$this->db->join('account', 'account.id = landing.account_id', 'left');
						$this->db->where('landing.landing_register_form', 260);
						$this->db->where("landing.landing_rfid", $cserial);
						$this->db->from('landing');
						$user = $this->db->get()->row();
					}
                }
                if ($user) {
                    if (!$test) {
                        $this->db->where('account_id', $user->id);
                        $this->db->where('log_type', 1);
                        $this->db->where('log_status', 1);
                        $this->db->order_by('log_stamps', 'desc');
                        $this->db->limit(1, 0);
                        $this->db->from('log');
                        $last = $this->db->get()->row();

                        if ((isset($last->places_id))&&($last->places_id == $venue->id)) {
                            $marker = 32;
                        } else {
                            $marker = 3;
                        }
                        if(isset($last->log_stamps))
                        $margin = time() - mysql_to_unix($last->log_stamps);
                        else
                        $margin = time();

                        if ($margin > $marker) {
                            if (isset($_REQUEST['date']))
                                $date = $sdate; else
                                $date = date('Y-m-d');
                            if (isset($_REQUEST['time']))
                                $time = $stime; else
                                $time = date('h:i:s');

                            $db['account_id'] = $user->id;
                            $db['places_id'] = $venue->id;
                            $db['log_type'] = 1;
                            $db['log_date'] = $date;
                            $db['log_time'] = $time;
                            $db['log_hash'] = 'v' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                            $db['log_status'] = 1;
                            $upd = $this->db->insert('log', $db);
                            $new = $this->db->insert_id();

                            if ($venue->places_cstatus_fb == '' && $venue->places_cstatus_tw == '' && $venue->places_type != '3') {
                                if ($venue->places_avatar != "") {
                                    $options['picture'] = base_url() . $venue->places_avatar;
									#$options['picture'] = base_url() . 'uploads/icon/' . $venue->places_avatar;
                                } else {
                                    $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                                }

                                $options['name'] = $venue->places_name;
                                $options['caption'] = 'wooz.in update';
                                $options['description'] = $user->account_displayname . ' just wooz.in @ ' . $venue->places_name;
                                $options['link'] = site_url() . '/woozpot/' . $venue->places_nicename;
                            } else {
                                $options['caption'] = '';
                            }

                            $data['status'] = false;
                            $data['message']['name'] = $user->account_displayname;
                            $data['message']['fb_stream'] = '0';
                            $data['message']['fb_place'] = '0';
                            $data['message']['fb_foto'] = '0';
                            $data['message']['twitter'] = '0';
                            $data['message']['foursquare'] = '0';
                            $data['message']['badge'] = '0';
                            
                            if ($user->account_fbid != '' || $user->account_tw_token != '') {

                                if ($user->account_fbid && $user->account_token) {
                                    $options['access_token'] = $user->account_token;

                                    if ($venue->places_type == '3') {
                                        /* private place */
                                        if ($venue->places_cstatus_fb != '') {
                                            $options['message'] = $venue->places_cstatus_fb;
                                        }
                                        
										try{
											$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
											$hasil = json_encode($url);
											$id_hasil = json_decode($hasil);
											$this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));
										}catch (FacebookApiException $e) {
											error_log($e);
											$result = $e->getResult();
											$data['message']['error'] = $result;
										}

                                        $data['status'] = true;
                                        $data['message']['fb_stream']= '1';
                                    } elseif ($venue->places_type == '4') {
                                        /* photo share upload */
                                        $url = "https://graph.facebook.com/me/albums?access_token=" . $user->account_token;
                                        $info = json_decode($this->curl->simple_get($url));
                                        $info = $info->data;
                                        $up_to = false;

                                        foreach ($info as $alb) {
                                            if ($alb->name == $venue->places_album) {
                                                $up_to = $alb->id;
                                            }
                                        }

                                        if ($up_to) {
                                            $album_id = $up_to;
                                        } else {
                                            $options['name'] = $venue->places_album;
                                            $options['access_token'] = $user->account_token;
                                            $url = $this->facebook->api("/$user->account_fbid/albums", 'post', $options);
                                            $hasil = json_encode($url);
                                            $album = json_decode($hasil);
                                            $album_id = $album->id;
                                        }
									
										if ($venue->places_cstatus_fb != '') {
											$photo = $user->account_displayname . ' ' . $venue->places_cstatus_fb;
                                        }
                                        /* upload photo */
                                        $this->facebook->setFileUploadSupport(true);
                                        
										if($venue->id == 224){
											$today = date("H:i:s");
											if(($today >= "10:00:00") && ($today <= "16:00:00"))
												$images = '/uploads/apps/yamaha/Putri1.jpg';
											else
												$images = '/uploads/apps/yamaha/rina.JPG';
										}else{
											$images = $venue->places_avatar;
										}

                                        $attachement = array(
                                            'access_token' => $user->account_token,
											'caption' =>  'uploaded foto',
                                            'name' => $venue->places_cstatus_fb,
                                            'source' => '@' . FCPATH . $images
                                        );
                                        
                                        $upload = $this->facebook->api($album_id . '/photos', 'post', $attachement);

                                        $fbid = $upload['id'];
                                        
                                        /* get photo pid before tagging, convert return id form upload to pid */
                                        $response = $this->facebook->api(array(
                                                    'access_token' => $user->account_token,
                                                    'method' => 'fql.query',
                                                    'query' => 'SELECT pid FROM photo WHERE object_id ="' . $fbid . '" ',
                                                ));
                 
                                        foreach ($response as $pid) {
                                            $new_pid = $pid['pid'];
                                        }
                                       
                                        $this->db->update('log', array('log_fb' => $new_pid), array('id' => $new));

                                        $data['status'] = true;
                                        $data['message']['fb_stream']= '1';
                                        $data['message']['fb_foto']= '1';
                                    } elseif ($venue->places_type == '5') {
                                        /* like this, status and image only */
                                        if ($venue->places_avatar != "") {
                                            $options['picture'] = base_url() . $venue->places_avatar;
                                            #$options['picture'] = base_url() . 'uploads/icon/' . $venue->places_avatar;
                                        } else {
                                            $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                                        }
                                        if ($venue->places_cstatus_fb != '') {
                                            $options['description'] = $venue->places_cstatus_fb;
                                        }

                                        $options['name'] = $venue->places_name;
                                        $options['link'] = site_url() . '/woozpot/' . $venue->places_nicename;

										try{
											$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
											$hasil = json_encode($url);
											$id_hasil = json_decode($hasil);
											$this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));
										}catch (FacebookApiException $e) {
											error_log($e);
											$result = $e->getResult();
											$data['message']['error'] = $result;
										}

                                        $data['status'] = true;
                                        $data['message']['fb_stream']= '1';
                                    } else {
                                        if ($venue->places_cstatus_fb != '') {
                                            if ($venue->places_avatar != "") {
												$options['picture'] = base_url() . $venue->places_avatar;
											} else {
												$options['picture'] = base_url() . 'assets/images/smalllogo.png';
											}
											$options['name'] = $venue->places_name;
											$options['caption'] = 'wooz.in update';
											$options['description'] = $venue->places_desc;
											#$options['message'] = "Join. Connect. Woozin'";
											$options['message'] = $venue->places_cstatus_fb;
											$options['link'] = site_url() . '/woozpot/' . $venue->places_nicename;
                                        }

										try{
											$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
											$hasil = json_encode($url);
											$id_hasil = json_decode($hasil);
											$this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));
										}catch (FacebookApiException $e) {
											error_log($e);
											$result = $e->getResult();
											$data['message']['error'] = $result;
										}

                                        $data['status'] = true;
                                        $data['message']['fb_stream']= '1';
                                    }

                                    /* check in places */ 
                                    if ($venue->places_facebook != '0') {
                                        if ($venue->places_latitude != "") {
                                            $lat = $venue->places_latitude;
                                        } else {
                                            $lat = "-6.254639";
                                        }

                                        if ($venue->places_longitude != "") {
                                            $lng = $venue->places_longitude;
                                        } else {
                                            $lng = "106.799887";
                                        }

										$spot['place'] = $venue->places_facebook;
                                        $spot['coordinates'] = '{"latitude":"' . $lat . '", "longitude": "' . $lng . '"}';
                                        $spot['access_token'] = $user->account_token;

                                        $this->db->select('count(log_fb_places) as places');
                                        $this->db->where('account_id', $user->id);
                                        $this->db->where('log_fb_places !=', 0);
                                        $this->db->where('places_id', $venue->id);
                                        $this->db->from('log');
										$count = $this->db->get()->result();
										$places = $count[0]->places;
										//print_r($places);
										$this->db->select('distinct(log_fb_places) as places');
										$this->db->where('account_id', $user->id);
										$this->db->where('log_fb_places !=', 0);
										$this->db->where('places_id', $venue->id);
										$this->db->from('log');
										$count1 = $this->db->get()->result();
										$places1 = $count[0]->places;
										//print_r($count1);exit;
										if ($places=='1'&&$count1)//kl uda update
										{
											$data['status'] = true;
											$data['message']['fb_place']= '0';
										}
										else //kl blum update
										{
											$url = $this->facebook->api("/$user->account_fbid/checkins", 'post', $spot);
											$hasil = json_encode($url);
											$id_hasil = json_decode($hasil);
											$this->db->update('log', array('log_fb_places' => $id_hasil->id), array('id' => $new));

											$data['status'] = true;
											$data['message']['fb_place']= '1';
										}
                                    }


                                    /* update traps table 
                                    $this->load->library('arthurday');
                                    $this->arthurday->set_token($user->account_token);
                                    $this->arthurday->set_log($user->id, $new);*/
                                }

                                /* like stream
                                  if ($user->account_fbid && $user->account_token && $venue->id == '7') {
                                  $id_stream = '100001548649398_125840527477568';

                                  $options['uid'] = $user->account_fbid;
                                  $options['access_token'] = $user->account_token;
                                  $url = 'https://graph.facebook.com/' . $id_stream . '/likes';
                                  $info = $this->curl->simple_post($url, $options);

                                  $data['status'] = true;
                                  $data['message'] .= 'facebook done! ';
                                  }
                                 */

								$data['wooz_badge'] = $this->__wooz_badge($user, $venue);
                                $data['spot_badge'] = $this->__spot_badge($user, $venue);
								if($venue->places_parent == 260){
									$data['count_badge'] = $this->__count_badge($user, $venue);
									#$data['gad_email'] = $this->__gad_email($user, $venue);
								}

                                if ($user->account_tw_token && $user->account_tw_secret) {
                                    $link = site_url() . '/woozpot/' . $venue->places_nicename;
                                    if ($venue->places_cstatus_tw != '') {
                                        $options['description'] = $venue->places_cstatus_tw;
                                    } else {
                                        $shortened = $this->bitly->shorten($link);
                                        $options['description'] = "I'm wooz.in @ " . $venue->places_name . ' - ' . $shortened;
                                    }

									#include(APPPATH.'third_party/tmhOAuth.php');
									#include(APPPATH.'third_party/tmhUtilities.php');
									
									$consumer_key = $this->config->item('twiiter_consumer_key');
                                    $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
									$tmhOAuth = new tmhOAuth(array(
											'consumer_key' => $consumer_key,
											'consumer_secret' => $consumer_key_secret,
											'user_token' => $user->account_tw_token,
											'user_secret' => $user->account_tw_secret,
									));
									
									if ($venue->places_type == '4'){
										if($venue->id == 224){
											$today = date("H:i:s");
											if(($today >= "10:00:00") && ($today <= "16:00:00"))
												$images = '/uploads/apps/yamaha/Putri1.jpg';
											else
												$images = '/uploads/apps/yamaha/rina1.jpg';
										}else{
											$images = $venue->places_avatar;
										}
										$image = FCPATH . $images;

										$code = $tmhOAuth->request(
														'POST', 'https://upload.twitter.com/1/statuses/update_with_media.json', array(
													'media[]' => "@{$image};type=image/jpeg;filename={$image}",
													'status' => $options['description'],
														), true, // use auth
														true  // multipart
										);
									}else{	
										$code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update'), array(
										  'status' => $options['description']
										));							
									}

									$resp = json_encode($tmhOAuth->response['response']);
									$resp1 = json_decode($resp);
									$twit_id = json_decode($resp1);
									if ($code == 200) {
										$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

										$data['status'] = true;
										$data['message']['twitter']= '1';
									}else{
										$data['status'] = true;
										$data['message']['twitter']= '0';
									}
                                }

                                if ($user->account_fs_token != '' && $venue->places_foursquare != '0' && $venue->places_type != '3') {
                                    $consumer_key = $this->config->item('foursquare_api_key');
                                    $consumer_key_secret = $this->config->item('foursquare_secret_key');
                                    $redirect_uri = base_url() . 'index.php/home/foursquare/';
                                    $accessToken = $user->account_fs_token;

                                    $foursquare = new EpiFoursquare($consumer_key, $consumer_key_secret, $accessToken);
                                    $foursquare->setAccessToken($user->account_fs_token);
                                   
									$Lat = $venue->places_latitude;
                                    $long = $venue->places_longitude;
                                    $ll = $Lat. "," .$long;
                                    $paramcheckin = array(
                                        'oauth_token' => $accessToken,
                                        'venueId' => $venue->places_foursquare,
                                        'shout' => $options['caption'],
                                        'broadcast' => 'public'
                                    );
                                    $checkin = $foursquare->post('/checkins/add', $paramcheckin);
                                    $foursquare->post('/checkins/add', $paramcheckin);
                                    $this->db->update('log', array('log_fs' => 1), array('id' => $new));

                                    $data['status'] = true;
                                    $data['message']['foursquare']= '1';
                                }

                                if (!$data['status']) {
                                    $data['message'] = 'user not checked in anywhere';
                                }
                                echo json_encode($data);
                            } else {
                                $data = array(
                                    'status' => 0,
                                    'message' => "account didn't connect with facebook or twitter"
                                );
                                echo json_encode($data);
                            }
                        } else {
                            $data = array(
                                'status' => 0,
                                'message' => 'time limit not passed'
                            );
                            echo json_encode($data);
                        }
                    } else {
                        $data = array(
                            'status' => 1,
                            'message' => "Test Done!"
                        );
                        echo json_encode($data);
                    }
                } else {
                    $data = array(
                        'status' => 0,
                        'message' => 'card serial is not connected with any member'
                    );
                    echo json_encode($data);
                }
            } else {
                $data = array(
                    'status' => 0,
                    'message' => 'device serial is not connected with any venue'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 0,
                'message' => 'card serial and device serial not received'
            );
            echo json_encode($data);
        }
    }

    function __tanggal($tanggal_f) {
        if (strstr($tanggal_f, ' ')) {
            $waktu_f = explode(' ', $tanggal_f);
            $tanggal_asli_f = explode('-', $waktu_f[0]);
            $waktu_asli_f = explode(':', $waktu_f[1]);
            $tanggal_cetak_f = date('M d, Y', mktime($waktu_asli_f[0], $waktu_asli_f[1], $waktu_asli_f[2], $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        } else {
            $tanggal_asli_f = explode('-', $tanggal_f);
            $tanggal_cetak_f = date('M d, Y', mktime(0, 0, 0, $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        }

        return $tanggal_cetak_f;
    }

    function __wooz_badge($user, $venue) {
        $this->db->where('account_id', $user->id);
        $this->db->where('log_type', 1);
        $this->db->where('log_status', 1);
        $this->db->from('log');
        $count = $this->db->count_all_results();

        $wb = $this->db->get_where('badge', array('places_id' => 0, 'badge_type' => 2, 'badge_status' => 1))->result();
        foreach ($wb as $badge) {
            $have = $this->__got_badge($badge->id, $user->id);
            if ($have) {
                return true;
            } else {
                if ($badge->badge_value != '0' && $count == $badge->badge_value) {
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 'w' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;

                    $upd = $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($badge->badge_avatar != "") {
                        $options['picture'] = base_url() . 'uploads/badge/' . $badge->badge_avatar;
                    } else {
                        $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    }
                    $options['link'] = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                    $options['name'] = $badge->badge_name;
                    $options['caption'] = 'Wooz.in Pin Unlocked';
                    $options['description'] = ' Woohooo! '.  $user->account_displayname .' have just unlocked the "' . $badge->badge_name . '" badge on wooz.in' . $venue->places_name . ' - ' . $badge->badge_desc;

                    $message = "<h1>Dear " . $user->account_username . ",</h1>";
                    $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the" . $badge->badge_name . " pin @ " . $venue->places_name . ' - ' . $badge->badge_desc. "</p>";
                    $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
                    $message .= "<p>&nbsp;</p><p>- Wooz.In</p>";
                    
                    $data['status'] = false;

                    $config['charset'] = 'iso-8859-1';
                    $config['protocol'] = 'mail';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = FALSE;
                   
                    $this->load->library('email');

                    $this->email->initialize($config);
                    $this->email->from('contact@wooz.in', 'Wooz.In');
                    $this->email->to($user->account_email);

                    $this->email->subject("wooz.in pin unlocked!");
                    $this->email->message($message);

                    if (!$this->email->send()) {
                        $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {

                    
                    if ($user->account_fbid && $user->account_token) {
                        $options['access_token'] = $user->account_token;
                        $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                        $hasil = json_encode($url);
                        $id_hasil = json_decode($hasil);
                        $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));

                        $data['status'] = true;
                        $data['message']['badge']= '1';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {
                        $link = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                        #$shortened = $this->bitly->shorten($options['link']);
                        #$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $shortened;
						$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $link;
						#include(APPPATH.'third_party/tmhOAuth.php');
						#include(APPPATH.'third_party/tmhUtilities.php');
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}

                        $data['status'] = true;
                        $data['message'] .= 'twitter done! ';
                    }

                    return $data['status'];
                } 
                }
            }
        }
    }

    function __spot_badge($user, $venue) {
		$jumlah = 0;
        $chk = $this->db->get_where('places', array('id' => $venue->id, 'places_status' => 1))->row();
        if ($chk->places_parent != '0') {
            $chk = $this->db->get_where('places', array('id' => $chk->places_parent, 'places_status' => 1))->row();
        }
        if (count($chk) != 0) {
            $chk2 = $this->__have_badge($chk->id);
            foreach ($chk2 as $badge) {
                $have = $this->__got_badge($badge->id, $user->id);
                if (!$have) {
                    if ($badge->badge_type == '3' || $badge->badge_type == '4') {
                        $spots = json_decode($badge->badge_spot);
                    } else {
                        $spots = $this->__spot_family($chk->id);
                    }
                    if ($badge->badge_type == '1'||$badge->badge_type == '3') {
                        $this->db->select('distinct(places_id)');
                    }
                    
                    $this->db->where('account_id', $user->id);
                    $this->db->where('log_type', 1);
                    $this->db->where('log_status', 1);
					$sisi = '';
                    if ($badge->badge_type == '4') {
						for($sp=0;$sp<count($spots);$sp++) {
							if($sp===0) {
				                $sisi = '(places_id = '.$spots[$sp];
							} else {
				                $sisi .= ' or places_id = '.$spots[$sp];
							}
						}
					$sisi .= ')';
                    $this->db->where($sisi);
					} else {
	                    $this->db->where_in('places_id', $spots);
					}
                    $this->db->from('log');
                    $count = $this->db->get()->result();
                    if ($badge->badge_type == '1' || $badge->badge_type == '3') {
                        if (count($spots) === count($count)) {
                            $status = true;
                            $unlocked = 1;
                            $jumlah = $jumlah + 1;
                        } else {
                            $status = false;
                        }
                    } elseif ($badge->badge_type == '4') {
                        if (count($count) === 1) {
                            $status = true;
                            $unlocked = 1;
                            $jumlah = $jumlah + 1;
                        } else {
                            $status = false;
                        }
                    } else {
                        if ($badge->badge_value == count($spots)) {
                            $status = true;
                            $unlocked = 1;
                            $jumlah = $jumlah + 1;
                        } else {
                            $status = false;
                        }
                    }
                } else {
                    $status = false;
                }

                if ($status) {
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;
                    $upd = $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($badge->badge_avatar != "") {
                        $options['picture'] = base_url() . 'uploads/badge/' . $badge->badge_avatar;
                    } else {
                        $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    }

                    $options['link'] = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                    $options['name'] = $badge->badge_name;
                    $options['caption'] = 'Wooz.in Pin Unlocked';
                    $options['description'] = ' Woohooo! '.  $user->account_displayname .' have just unlocked the "' . $badge->badge_name . '" badge on wooz.in' . $venue->places_name . ' - ' . $badge->badge_desc;
                    
					$message = "<h1>Dear " . $user->account_displayname . ",</h1>";
                    $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the" . $badge->badge_name . " pin @ " . $venue->places_name . ' - ' . $badge->badge_desc. "</p>";
                    $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
                    $message .= "<p>&nbsp;</p><p>- Wooz.In</p>";
                    
                    $data['status'] = false;

                    $config['charset'] = 'iso-8859-1';
                    $config['protocol'] = 'mail';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = FALSE;
                    
                    $this->load->library('email');

                    $this->email->initialize($config);
                    $this->email->from('contact@wooz.in', 'Wooz.In');
                    $this->email->to($user->account_email);

                    $this->email->subject("wooz.in pin unlocked!");
                    $this->email->message($message);

                    if (!$this->email->send()) {
                        $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {
					
                    if ($user->account_fbid && $user->account_token) {
                        $options['access_token'] = $user->account_token;
                        $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                        $hasil = json_encode($url);
                        $id_hasil = json_decode($hasil);
                        $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));

                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {
                        #$shortened = $this->bitly->shorten($options['link']);
                        #$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $shortened;

						$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $options['link'];
						#include(APPPATH.'third_party/tmhOAuth.php');
						#include(APPPATH.'third_party/tmhUtilities.php');
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }
					return $data['status'];
                }
            }
          } //tambahan ubet
				/*	$sql = "select badge_name,badge_avatar from wooz_log,wooz_badge where wooz_log.badge_id not in ('0') and wooz_log.badge_id=wooz_badge.id and account_id = '".$user->id."' ";
                    $sql1 = $this->db->query($sql);
                    foreach($sql1->result_array() as $row)
					{
						$row1[]=$row;
					}
					$row2 = json_encode($row1);
					return $row2;		//end  */
        }
    }

    function __count_badge($user, $venue) {
	$jumlah = 0;
        $chk = $this->db->get_where('places', array('id' => $venue->id, 'places_status' => 1))->row();
        if ($chk->places_parent != '0') {
            $chk = $this->db->get_where('places', array('id' => $chk->places_parent, 'places_status' => 1))->row();
        }
        if (count($chk) != 0) {
            $chk2 = $this->__have_badge($chk->id);
            foreach ($chk2 as $badge) {
                $have = $this->__got_badge($badge->id, $user->id);
                if (!$have) {
                    if ($badge->badge_type == '2') {
						$this->db->select('distinct(places_id)');
                        $spots = json_decode($badge->badge_spot);
                        $this->db->where('account_id', $user->id);
                        #$this->db->where('log_type', 1);
						$this->db->where('log_type !=', 2);
                        $this->db->where('log_status', 1);                        
                        $this->db->where_in('places_id', $spots);
                        $this->db->from('log');
                        $count = $this->db->get()->result();
                        
                        if ($badge->badge_value == count($count)) {
                                $status = true;
                                $unlocked = 1;
                                $jumlah = $jumlah + 1;
                        } else {
                                $status = false;
                        }						
                    }else
                        $status = false;
                } else {
                    $status = false;
                }
				
                if ($status) {
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;
                    $upd = $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($badge->badge_avatar != "") {
                        $options['picture'] = base_url() . 'uploads/badge/' . $badge->badge_avatar;
                    } else {
                        $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    }

                    $options['link'] = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                    $options['name'] = $badge->badge_name;
					#$options['message'] = $badge->badge_desc;
                    $options['caption'] = 'Wooz.in Pin Unlocked';
                    $options['description'] = ' Woohooo! '.  $user->account_displayname .' have just unlocked the "' . $badge->badge_name . '" badge on wooz.in' . $venue->places_name . ' - ' . $badge->badge_desc;
                    if($badge->id == 31)
						$lagi = '1 kali lagi';
					else
						$lagi = '1 kali';
					
                    $message = "<h1>Dear " . $user->account_displayname . ",</h1>";
                    $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the" . $badge->badge_name . " pin @ " . $venue->places_name . " 2012</p>";
                    $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
                    $message .= "<p>&nbsp;</p><p>- Wooz.In</p>";
 			          
                    $data['status'] = false;

                    $config['charset'] = 'iso-8859-1';
                    $config['protocol'] = 'mail';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = FALSE;
                    
                    $this->load->library('email');

                    $this->email->initialize($config);
                    $this->email->from('contact@wooz.in', 'Wooz.In');
                    $this->email->to($user->account_email);

                    $this->email->subject("wooz.in pin unlocked!");
                    $this->email->message($message);

                    if (!$this->email->send()) {
                        $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {
					
                    if ($user->account_fbid && $user->account_token) {
                        $options['access_token'] = $user->account_token;
                        $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                        $hasil = json_encode($url);
                        $id_hasil = json_decode($hasil);
                        $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));

                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {
                        $shortened = $this->bitly->shorten($options['link']);
						$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $options['link'];
						#include(APPPATH.'third_party/tmhOAuth.php');
						#include(APPPATH.'third_party/tmhUtilities.php');
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }
					return $data['status'];
                }
            }
          }
        }
    }
    

    function __have_badge($id) {
        $chk = $this->db->get_where('badge', array('places_id' => $id, 'badge_status' => 1))->result();
        return $chk;
    }

    function __got_badge($id, $user) {
        $chk = $this->db->get_where('log', array('badge_id' => $id, 'account_id' => $user, 'log_type' => 2, 'log_status' => 1))->row();
        if (count($chk) === 0)
            return false;
        else
            return true;
    }

	function __gad_email($user, $venue){		
		$spots = array('248','249','250','251','252','253','254');
		$this->db->select('distinct(places_id)');
		$this->db->where('account_id', $user->id);
        #$this->db->where('log_type', 1);
		$this->db->where('log_type !=', 2);
        $this->db->where('log_status', 1);
        $this->db->where_in('places_id', $spots);
        $this->db->from('log');	
		$count = $this->db->get()->result();

		if(count($count) == 2){
			$db['account_id'] = $user->id;
			$db['places_id'] = $venue->id;
			$db['badge_id'] = 0;
			$db['log_type'] = 2;
			$db['log_date'] = date('Y-m-d');
			$db['log_time'] = date('h:i:s');
			$db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
			$db['log_status'] = 1;
			$upd = $this->db->insert('log', $db);
			$new = $this->db->insert_id();
			/* $message = "<h1>Dear " . $user->account_username . ",</h1>";
            $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the pin @ " . $venue->places_name . "</p>";
            $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
            $message .= "<p>&nbsp;</p><p>- Wooz.In</p>"; */
$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

<title>WOOZ.IN | Guinness Arthur's Day</title>
<style type='text/css'>
/* Client-specific Styles */
#outlook a{padding:0;} /* Force Outlook to provide a 'view in browser' button. */
body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */

/* Reset Styles */
body{margin:0; padding:0;}
img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
table td{border-collapse:collapse;}
#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

/* Template Styles */

body, #backgroundTable{
background-color:#FAFAFA;
}


#templatePreheader{
background-color:#FAFAFA;
}

.preheaderContent div{
background:#ec2b2b;
font-family:Arial;
font-size:10px;
line-height:100%;
text-align:left;
}

.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
color:#336699;
font-weight:normal;
text-decoration:underline;
}

#templateHeader{
background-color:#ec2b2b;
border-bottom:0;
}

.headerContent{
color:#202020;
font-family:Arial;
font-size:20px;
font-weight:bold;
line-height:100%;
padding:0;
text-align:center;
vertical-align:middle;
}

.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
color:#336699;
font-weight:normal;
text-decoration:underline;
}

#headerImage{
height:auto;
max-width:650px !important;
}

#templateContainer, .bodyContent{
background-color:#fff;
}

.bodyContent div{
color:#505050;
font-family:Arial;
font-size:14px;
line-height:150%;
text-align:left;
}

.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
color:#336699;
font-weight:normal;
text-decoration:underline;
}

.bodyContent img{
display:inline;
height:auto;
}

#templateFooter{
background-color:#fff;
border-top:0;
}

.footerContent{background:#ec2b2b}
.footerContent div{
color:#ff9898;
font-family:Arial;
font-size:12px;
line-height:125%;
text-align:center;
font-size:11px
}

.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
color:#eee;
font-weight:normal;
text-decoration:underline;
}

.footerContent img{
display:inline;
}
</style>
	</head>
    <body leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'>
    	<center>
        	<table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='backgroundTable'>
            	<tr>
                	<td align='center' valign='top'>
                    	<table border='0' cellpadding='0' cellspacing='0' width='650' id='templateContainer'>
                        	<tr>
                            	<td align='center' valign='top'>
                                	<table border='0' cellpadding='0' cellspacing='0' width='650' id='templateHeader' style='background:#ec2b2b;'>
                                        <tr>
                                            <td class='headerContent' style='height:10px;display:block'>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align='center' valign='top'>
                                    <!-- // Begin Template Body \\ -->
                                	<table border='0' cellpadding='0' cellspacing='0' width='650' id='templateBody'>
                                    	<tr>
                                            <td valign='top' class='bodyContent'>
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000;'>
                                                    <tr>
                                                        <td valign='top'>
															
                                                        <p style='text-align:center'>
                                                        	<img src='http://wooz.in/uploads/apps/gad2012/header2.jpg' ALIGN='center' style='max-width:450px;font-family:Arial, Helvetica, sans-serif;line-height:20px;color:#eeeeee;font-size:20px;font-weight:bold' alt='THANK YOU, FOR JOINING A CELEBRATION MADE OF MORE' style='vertical-align:top' />
                                                        </p>
														</td>
                                                    </tr>
                                                </table>
                                                
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000;'>
                                                    <tr>
                                                        <td valign='top'>
                                                        <p style='text-align:center'>
														<font color='white'>Sudah 2 titik yang lo check-in. 1 titik lagi maka lo akan punya kesempatan untuk lucky dip</font>
														</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000;'>
                                                    <tr>
                                                        <td valign='top'>
                                                        <p style='text-align:center'>
														</p>
                                                        	</td>
                                                    </tr>
                                                </table>
                                                
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000;'>
                                                    <tr>
                                                        <td valign='top' style='height: 150px;'>
                                                        <p style='text-align:center'>
														</p>
                                                        	</td>
                                                    </tr>
                                                </table>

                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000000 url(http://wooz.in/uploads/apps/gad2012/burung.png) no-repeat 0 0;padding-top:3px;border-top:1px solid #333'>
                                                    <tr>
                                                        <td valign='top' style='text-align:center;font-family:Arial, Helvetica, sans-serif;color:#eeeeee;font-size:14px'>
                                                        
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#333333;'>
                                                    <tr>
                                                        <td valign='top'>
                                                        	<img src='http://wooz.in/uploads/apps/gad2012/woozin-2.png' style='max-width:650px;vertical-align:top;font-family:Arial, Helvetica, sans-serif;line-height:20px;color:#eeeeee;' alt='WOOZ.IN is a technology service that provides integration Radio-Frequency technology and Social Networks. Owned by BRAIN. ' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align='center' valign='top'>
                                	<table border='0' cellpadding='10' cellspacing='0' width='650' id='templateFooter'>
                                    	<tr>
                                        	<td valign='top' class='footerContent' style='background:#EC2B2B'>
                                          <div style='color:#FF9898; font-family: Arial; font-size: 11px;line-height: 125%;text-align: center;'>
                                          	You received this email because you registered at Guinness Arthur's Day via Wooz.in<br />
																						If you do not want to receive any further emails, click <a href='#' style='color:#eee'>here</a> to unsubscribe � but you�ll miss out on future party alerts!
                                          </div>
                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
</body></html>";

            $config['charset'] = 'iso-8859-1';
            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = FALSE;
                   
            $this->load->library('email');

            $this->email->initialize($config);
            $this->email->from('contact@wooz.in', 'Wooz.In');
            $this->email->to($user->account_email);

            $this->email->subject("wooz.in Notification");
            $this->email->message($message);
			if (!$this->email->send()) {
				$this->db->update('account', array('account_fest' => 0), array('id' => $user->id));
				#echo $customs->account_displayname.' - gagal';
				#echo ' - gagal';
			}else{
				$this->db->update('account', array('account_fest' => 230), array('id' => $user->id));
				if ($user->account_tw_token && $user->account_tw_secret) {
                        #$shortened = $this->bitly->shorten($options['link']);
						$options['description'] = "baru aja check in di booth 2, Guinness Arthur's Day 2012 - Made of More, #JKT";
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }
				if ($user->account_fbid && $user->account_token) {
						$options['picture'] = base_url() . $venue->places_avatar;
						$options['link'] = 'http://wooz.in/woozpot/'. $venue->places_nicename;
						$options['name'] = $venue->places_nicename;
						$options['message'] = $user->account_displayname." baru aja check in di booth 2 perayaan Guinness Arthur's Day 2012 - Made of More di Jakarta buat dapetin hadiah keren!";
						$options['caption'] = 'Wooz.in Notification';
						$options['description'] = $venue->places_desc;
                        $options['access_token'] = $user->account_token;
                        $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                        $hasil = json_encode($url);
                        $id_hasil = json_decode($hasil);
                        $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));

                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }
			}			
		}
		if(count($count) == 5){
			$db['account_id'] = $user->id;
			$db['places_id'] = $venue->id;
			$db['badge_id'] = 0;
			$db['log_type'] = 2;
			$db['log_date'] = date('Y-m-d');
			$db['log_time'] = date('h:i:s');
			$db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
			$db['log_status'] = 1;
			$upd = $this->db->insert('log', $db);
			$new = $this->db->insert_id();
$message = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

<title>WOOZ.IN | Guinness Arthur's Day</title>
<style type='text/css'>
/* Client-specific Styles */
#outlook a{padding:0;} /* Force Outlook to provide a 'view in browser' button. */
body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */

/* Reset Styles */
body{margin:0; padding:0;}
img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
table td{border-collapse:collapse;}
#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

/* Template Styles */

body, #backgroundTable{
background-color:#FAFAFA;
}


#templatePreheader{
background-color:#FAFAFA;
}

.preheaderContent div{
background:#ec2b2b;
font-family:Arial;
font-size:10px;
line-height:100%;
text-align:left;
}

.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
color:#336699;
font-weight:normal;
text-decoration:underline;
}

#templateHeader{
background-color:#ec2b2b;
border-bottom:0;
}

.headerContent{
color:#202020;
font-family:Arial;
font-size:20px;
font-weight:bold;
line-height:100%;
padding:0;
text-align:center;
vertical-align:middle;
}

.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
color:#336699;
font-weight:normal;
text-decoration:underline;
}

#headerImage{
height:auto;
max-width:650px !important;
}

#templateContainer, .bodyContent{
background-color:#fff;
}

.bodyContent div{
color:#505050;
font-family:Arial;
font-size:14px;
line-height:150%;
text-align:left;
}

.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
color:#336699;
font-weight:normal;
text-decoration:underline;
}

.bodyContent img{
display:inline;
height:auto;
}

#templateFooter{
background-color:#fff;
border-top:0;
}

.footerContent{background:#ec2b2b}
.footerContent div{
color:#ff9898;
font-family:Arial;
font-size:12px;
line-height:125%;
text-align:center;
font-size:11px
}

.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
color:#eee;
font-weight:normal;
text-decoration:underline;
}

.footerContent img{
display:inline;
}
</style>
	</head>
    <body leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'>
    	<center>
        	<table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='backgroundTable'>
            	<tr>
                	<td align='center' valign='top'>
                    	<table border='0' cellpadding='0' cellspacing='0' width='650' id='templateContainer'>
                        	<tr>
                            	<td align='center' valign='top'>
                                	<table border='0' cellpadding='0' cellspacing='0' width='650' id='templateHeader' style='background:#ec2b2b;'>
                                        <tr>
                                            <td class='headerContent' style='height:10px;display:block'>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align='center' valign='top'>
                                    <!-- // Begin Template Body \\ -->
                                	<table border='0' cellpadding='0' cellspacing='0' width='650' id='templateBody'>
                                    	<tr>
                                            <td valign='top' class='bodyContent'>
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000;'>
                                                    <tr>
                                                        <td valign='top'>
															
                                                        <p style='text-align:center'>
                                                        	<img src='http://wooz.in/uploads/apps/gad2012/header2.jpg' ALIGN='center' style='max-width:450px;font-family:Arial, Helvetica, sans-serif;line-height:20px;color:#eeeeee;font-size:20px;font-weight:bold' alt='THANK YOU, FOR JOINING A CELEBRATION MADE OF MORE' style='vertical-align:top' />
                                                        </p>
														</td>
                                                    </tr>
                                                </table>
                                                
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000;'>
                                                    <tr>
                                                        <td valign='top'>
                                                        <p style='text-align:center'>
														<font color='white'>Sudah 2 titik yang lo check-in. 1 titik lagi maka lo akan punya kesempatan untuk lucky dip</font>
														</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000;'>
                                                    <tr>
                                                        <td valign='top'>
                                                        <p style='text-align:center'>
														</p>
                                                        	</td>
                                                    </tr>
                                                </table>
                                                
                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000;'>
                                                    <tr>
                                                        <td valign='top' style='height: 150px;'>
                                                        <p style='text-align:center'>
														</p>
                                                        	</td>
                                                    </tr>
                                                </table>

                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#000000 url(http://wooz.in/uploads/apps/gad2012/burung.png) no-repeat 0 0;padding-top:3px;border-top:1px solid #333'>
                                                    <tr>
                                                        <td valign='top' style='text-align:center;font-family:Arial, Helvetica, sans-serif;color:#eeeeee;font-size:14px'>
                                                        
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background:#333333;'>
                                                    <tr>
                                                        <td valign='top'>
                                                        	<img src='http://wooz.in/uploads/apps/gad2012/woozin-2.png' style='max-width:650px;vertical-align:top;font-family:Arial, Helvetica, sans-serif;line-height:20px;color:#eeeeee;' alt='WOOZ.IN is a technology service that provides integration Radio-Frequency technology and Social Networks. Owned by BRAIN. ' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align='center' valign='top'>
                                	<table border='0' cellpadding='10' cellspacing='0' width='650' id='templateFooter'>
                                    	<tr>
                                        	<td valign='top' class='footerContent' style='background:#EC2B2B'>
                                          <div style='color:#FF9898; font-family: Arial; font-size: 11px;line-height: 125%;text-align: center;'>
                                          	You received this email because you registered at Guinness Arthur's Day via Wooz.in<br />
																						If you do not want to receive any further emails, click <a href='#' style='color:#eee'>here</a> to unsubscribe � but you�ll miss out on future party alerts!
                                          </div>
                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
</body></html>";

            $config['charset'] = 'iso-8859-1';
            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = FALSE;
                   
            $this->load->library('email');

            $this->email->initialize($config);
            $this->email->from('contact@wooz.in', 'Wooz.In');
            $this->email->to($user->account_email);

            $this->email->subject("Wooz.in Notification");
            $this->email->message($message);
			if (!$this->email->send()) {
				$this->db->update('account', array('account_fest' => 0), array('id' => $user->id));
				#echo $customs->account_displayname.' - gagal';
				#echo ' - gagal';
			}else{
				$this->db->update('account', array('account_fest' => 230), array('id' => $user->id));
				if ($user->account_tw_token && $user->account_tw_secret) {
                        #$shortened = $this->bitly->shorten($options['link']);
						$options['description'] = "baru aja check in di booth 5, Guinness Arthur's Day 2012 - Made of More, #JKT";
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }
				if ($user->account_fbid && $user->account_token) {
						$options['picture'] = base_url() . $venue->places_avatar;
						$options['link'] = 'http://wooz.in/woozpot/'. $venue->places_nicename;
						$options['name'] = $venue->places_nicename;
						$options['message'] = $user->account_displayname." baru aja check in di booth 5 Guinness Arthur's Day 2012 - Made of More, Jakarta.";
						$options['caption'] = 'Wooz.in Notification';
						$options['description'] = $venue->places_desc;
                        $options['access_token'] = $user->account_token;
                        $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                        $hasil = json_encode($url);
                        $id_hasil = json_decode($hasil);
                        $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));

                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }
			}		
		}		
	}

    function __spot_family($id) {
        $this->db->select('');
        $this->db->where('id', $id);
        #$this->db->where('booth_id !=', 0);
        $this->db->where('places_parent', 0);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $data1 = $this->db->get()->result();

        $this->db->select('');
        #$this->db->where('booth_id !=', 0);
        $this->db->where('places_parent', $id);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $data2 = $this->db->get()->result();

		$data3 = array_merge($data1, $data2);

        foreach ($data3 as $row) {
            $x[] = $row->id;
        }

        return $x;
    }
    
    function __gwrefresh($userdata) {
        $getuser = $this->db->get_where('account', array('id' => $userdata))->row();

        $consumer_key = 'a64c308b64604d8ea683408f1c67b5ba';
        $consumer_key_secret = '08bee20693114586992ac0323df12fe0';
        $redirect_uri = base_url() . 'index.php/home/gowalla/';

        $gowalla = new GowallaPHP($consumer_key, $redirect_uri, $consumer_key_secret);

        if ($getuser->account_gw_token != '' || $getuser->account_gw_refresh != '' || $getuser->account_gw_time != '') {
            $daterefresh = date('D, d M Y H:i:s', $getuser->account_gw_time);
            if (time() > $getuser->account_gw_time) {
                $parms["refresh_token"] = $getuser->account_gw_refresh;
                $parms["client_id"] = $consumer_key;
                $parms["client_secret"] = $consumer_key_secret;
                $parms["access_token"] = $getuser->account_gw_token;

                $renew = $gowalla->refresh_token($parms);
                $data = json_decode($renew);

                $refresh_time = date('U', strtotime($data->expires_at));

                $up['account_gw_token'] = $data->access_token;
                $up['account_gw_refresh'] = $data->refresh_token;
                $up['account_gw_time'] = $refresh_time;

                $this->db->where('id', $getuser->id);
                $this->db->update('account', $up);

                return TRUE;
            } else {
                return TRUE;
            }
        } else {
            return TRUE;
        }
    }

}

