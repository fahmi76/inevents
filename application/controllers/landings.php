<?php

class landings extends CI_controller {

    protected $content;
    protected $perms;

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('convert', 'landings_m'));
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->model('convert');
        date_default_timezone_set('Asia/Jakarta');

        $this->load->helper('url');
        $this->load->library('sosmedtw');

        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');
        $custom_page = $this->uri->segment(3,0);
        if ($custom_page != '') {

            $customs = $this->landings_m->getplaceslanding($custom_page);
            if ($customs) {
                $accid = $this->uri->segment(4, 0);
                $landingid = $this->uri->segment(5, 0);
                $from = $this->uri->segment(6, 0);
                if($accid == 0){
                    $url = array('url' => base_url() . 'landings/fb_token/'.$custom_page);
                }else{
                    $url = array('url' => base_url() . 'landings/fb_token/'.$custom_page.'/'.$accid.'/'.$landingid.'/'.$from);
                }
                $this->load->library('facebook_v4', $url);
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;
                $regis_type = '["1","2","3"]';
                $this->tpl['regis_type'] = json_decode($regis_type);
                $this->tpl['lang_regis'] = 2;
                $this->fb_name = '';
                $this->tpl['fb_page'] = '';
                $this->tpl['tw_page'] = '';
                $this->twiiter_id_fan = '';
                $this->twiiter_name = '';
                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                    $this->fb_name = $customs->places_fb_page_id;
                }
                if ($customs->places_tw_id != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                    $this->twiiter_name = $customs->places_twitter;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $this->home();
    }

    function home() {
        header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Home";
        $retry = $this->input->get('retry', true);
        if ($retry) {
            $status = $this->input->get('status', true);
            if ($status) {
                $this->tpl['status'] = $status;
            }
            $this->tpl['retry'] = true;
        } else {
            $this->tpl['retry'] = false;
        }
        $login_url = $this->facebook_v4->get_login_url();
        $url = site_url('landings/logouts/'. $this->custom_url);
        $logout_url = $this->facebook_v4->get_logout_url($url);
        $token = $this->facebook_v4->token();
		#
        if ($logout_url) {
            $this->tpl['login'] = $logout_url;
        } else {
            $this->tpl['login'] = $login_url;
        }

        $this->tpl['TContent'] = $this->load->view('landings/default', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);
    }


    function step2s(){
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        if($accid == 0){
            redirect('landings/home/'.$this->custom_url);
        }
        $user = $this->db->get_where('account', array('id' => $accid))->row();
        $landingid = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);
        $this->tpl['from'] = $from;

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required'); 
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_used_en[' . $accid . ']');
        $this->form_validation->set_rules('tgl', 'Date', 'trim|required|numeric|callback_cek_data_tanggal_en');
        $this->form_validation->set_rules('bln', 'Month', 'trim|callback_cek_data_dropdown_bulan_en');
        $this->form_validation->set_rules('thn', 'Year', 'trim|required|numeric|callback_year_chk_en'); 
        if ($this->form_validation->run() === TRUE) {
            $data['account_displayname'] = $this->input->post('fullname', true);
            $data['account_username'] = $this->convert->nicename($this->input->post('fullname', true));
            $data['account_email'] = $this->input->post('email', true);
            $data['account_birthdate'] = $this->input->post('thn', true) . '-' . $this->input->post('bln', true) . '-' . $this->input->post('tgl', true);
            $data['account_gender'] = $this->input->post('gender', true);
            $data['account_phone'] = $this->input->post('telp', true);
            if($this->custom_id == 8){
                $data['account_status'] = 2;
                $data['account_fest'] = 1;
                $data['account_register1'] = 1;
            }else{
                $data['account_status'] = 1;
            }
            if ($accid == 0) {
                $this->db->insert('account', $data);
                $accid = $this->db->insert_id();
            } else {
                $this->db->where('id', $accid);
                $this->db->update('account', $data);
            }

            $landing['landing_from_regis'] = $from;
            $landing['account_id'] = $accid;
            $landing['landing_register_form'] = $this->custom_id;
            $landings_id = $this->landings_m->landingdata($accid, $this->custom_id, $landing);

            if($this->custom_id == 8){
                redirect('landings/step4/'.$places.'/'.$accid.'/'.$landingid.'/'.$from);
            }else{
                redirect('landings/step3/'.$places.'/'.$accid.'/'.$landingid.'/'.$from);
            }
        }
        if ($from == 'tw') {
            if($this->twiiter_name != ''){
                $this->tpl['likefollow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $this->twiiter_name);
            }
        }

        if ($from == 'fb') {
            $url = array('url' => base_url() . 'landings/step2/' . $this->custom_url . '/'.$accid.'/0/fb');
            $this->load->library('facebook_v4/facebooks_v4', $url);
            $url_logout = site_url('landings/logouts/'.$this->custom_url);
            $this->tpl['logout'] = $this->facebooks_v4->get_logout_tokens($user->account_token,$url_logout);
            if($this->fb_name != ''){
                $this->tpl['likefollow'] = $this->facebooks_v4->get_like_fb($user->account_token,$user->account_fbid,$this->fb_name);
            }
        }

        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landings/signupnext', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);
    }

    function step2(){
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        if($accid == 0){
            redirect('landings/home/'.$this->custom_url);
        }
        $user = $this->db->get_where('account', array('id' => $accid))->row();
        $landingid = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);
        $this->tpl['from'] = $from;

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required'); 
        $this->form_validation->set_rules('serial', 'RFID Serial', 'trim|callback_rfid_chk[' . $accid . ']|required');
        if ($this->form_validation->run() === TRUE) {
            $data['account_displayname'] = $this->input->post('fullname', true);
            $data['account_username'] = $this->convert->nicename($this->input->post('fullname', true));
            $data['account_rfid'] = $this->input->post('serial', true);
            $this->db->where('id', $accid);
            $this->db->update('account', $data);
            $landing['landing_from_regis'] = $from;
            $landing['account_id'] = $accid;
            $landing['landing_register_form'] = $this->custom_id;
            $landing['landing_rfid'] = $this->input->post('serial', true);
            $landings_id = $this->landings_m->landingdata($accid, $this->custom_id, $landing);

            redirect('landings/step4/'.$places.'/'.$accid.'/'.$landingid.'/'.$from);
            
        }
        if ($from == 'tw') {
            if($this->twiiter_name != ''){
                $this->tpl['likefollow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $this->twiiter_name);
            }
        }

        if ($from == 'fb') {
            $url = array('url' => base_url() . 'landings/step2/' . $this->custom_url . '/'.$accid.'/0/fb');
            $this->load->library('facebook_v4/facebooks_v4', $url);
            $url_logout = site_url('landings/logouts/'.$this->custom_url);
            $this->tpl['logout'] = $this->facebooks_v4->get_logout_tokens($user->account_token,$url_logout);
            if($this->fb_name != ''){
                $this->tpl['likefollow'] = $this->facebooks_v4->get_like_fb($user->account_token,$user->account_fbid,$this->fb_name);
            }
        }
        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landings/signupnextnew', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);

    }

    function step3(){
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        if($accid == 0){
            redirect('landings/home/'.$this->custom_url);
        }
        $user = $this->db->get_where('account', array('id' => $accid))->row();
        $landingid = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('serial', 'RFID Serial', 'trim|callback_rfid_chk[' . $accid . ']|required');
        if ($this->form_validation->run() === TRUE) {
            $data['account_rfid'] = $this->input->post('serial', true);
            $this->db->where('id', $accid);
            $this->db->update('account', $data);
            $landing['landing_from_regis'] = $from;
            $landing['account_id'] = $accid;
            $landing['landing_register_form'] = $this->custom_id;
            $landing['landing_rfid'] = $this->input->post('serial', true);
            $landings_id = $this->landings_m->landingdata($accid, $this->custom_id, $landing);

            redirect('landings/step4/'.$places.'/'.$accid.'/'.$landingid.'/'.$from);
        }
        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landings/activaterfid', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);
    }

    function step4(){
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        if($accid == 0){
            redirect('landings/home/'.$this->custom_url);
        }
        $user = $this->db->get_where('account', array('id' => $accid))->row();
        $landingid = $this->uri->segment(5, 0);
        $this->tpl['landingid'] = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);
        $this->tpl['from'] = $from;
        $this->tpl['places'] = $places;

        if($from == 'tw'){
            $login_url = $this->facebook_v4->get_login_url();
            $url_logout = site_url('landings/step4/'.$places.'/'.$accid.'/'.$landingid.'/'.$from);
            $logout_url = $this->facebook_v4->get_logout_url($url_logout);
            $token = $this->facebook_v4->token();
            if ($token) {
                $this->tpl['login'] = $logout_url;
            } else {
                $this->tpl['login'] = $login_url;
            }
        }

        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landings/signupskip', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);
    }

    function step5(){
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        if($accid == 0){
            redirect('landings/home/'.$this->custom_url);
        }
        $user = $this->db->get_where('account', array('id' => $accid))->row();
        $this->tpl['landingid'] = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);
        $this->tpl['from'] = $from;
        $this->tpl['places'] = $places;

        if ($from == 'fb') {
            $this->tpl['likefollow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $this->twiiter_name);
        }

        if ($from == 'tw') {
            $url = array('url' => base_url() . 'landings/step2/' . $this->custom_url . '/'.$accid.'/0/fb');
            $this->load->library('facebook_v4/facebooks_v4', $url);
            $this->tpl['likefollow'] = $this->facebooks_v4->get_like_fb($user->account_token,$user->account_fbid,$this->fb_name);
        }

        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landings/signupskip2', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);
    }

    function step6(){
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        if($accid == 0){
            redirect('landings/home/'.$this->custom_url);
        }
        $user = $this->db->get_where('account', array('id' => $accid))->row();
        $this->tpl['landingid'] = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);
        $this->tpl['from'] = $from;
        $this->tpl['places'] = $places;
        $text = $this->db->get_where('places', array('places_landing' => $places))->row();
        $this->tpl['text'] = $text;
        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landings/signupshare', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);
    }

    function share(){
        $urls = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        if($accid == 0){
            redirect('landings/home/'.$this->custom_url);
        }
        $user = $this->db->get_where('account', array('id' => $accid))->row();
        $landingid = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);
        $share = $this->uri->segment(7, 1);

        $places = $this->db->get_where('places', array('places_landing' => $urls))->row();
        if ($user->account_fbid && $user->account_token) {
            $url = array('url' => base_url() . 'landings/share/' . $this->custom_url . '/'.$accid.'/0/fb/1');
            $this->load->library('facebook_v4/facebooks_v4', $url);
            if($places->places_fb_page_id != ''){
                $landing['landing_fb_like'] = $this->facebooks_v4->get_like_fb($user->account_token, $user->account_fbid, $places->places_fb_page_id);
            }
            $landing['landing_fb_friends'] = $this->facebooks_v4->get_friend_fb($user->account_token, $user->account_fbid);
            if($share == 1){
                $text = $places->places_cstatus_fb;
                $status = array(
                    'link' => base_url(),
                    'name' => $places->places_name,
                    'caption' => $user->account_displayname." ".$text,//." @".$venue->places_address,
                    //'message' => '@'.$venue->places_address
                    'message' => ''
                );
                if ($places->places_type == 2) {
                    $image = FCPATH . $places->places_avatar;
                    $album = 0;
                    $album = $this->facebooks_v4->get_album($places, $image, $user->account_fb_name, $user->account_fbid, $user->account_token);
                    $landing['landing_fb_status'] = $this->facebooks_v4->post_photo($places, $image, $user->account_fb_name, $user->account_fbid, $user->account_token,$album);
                } else {
                    $landing['landing_fb_status'] = $this->facebooks_v4->update_status($status, $user->account_fbid, $user->account_token);
                }
            }
        }
        if ($user->account_tw_token && $user->account_tw_secret) {
            if($places->places_twitter != ''){
                $landing['landing_tw_follow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_twitter);
            }
            $landing['landing_tw_friends'] = $this->sosmedtw->friend_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username);
            if($share == 1){
                $texttw = $places->places_cstatus_tw;
                if ($places->places_type == 2) {
                    $image = FCPATH . $places->places_avatar;
                    $landing['landing_tw_status'] = $this->sosmedtw->upload_photo_tw($texttw, $image, $user->account_tw_token, $user->account_tw_secret);
                } else {
                    $landing['landing_tw_status'] = $this->sosmedtw->update_tw($texttw, $user->account_tw_token, $user->account_tw_secret);
                }

            }
        }
        $landings_id = $this->landings_m->landingdata($accid, $this->custom_id, $landing);
        redirect('landings/done/'.$urls.'/'.$accid.'/'.$landingid.'/'.$from);
    }

    function done(){
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        if($accid == 0){
            redirect('landings/home/'.$this->custom_url);
        }
        $user = $this->db->get_where('account', array('id' => $accid))->row();
        $this->tpl['landingid'] = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);
        $this->tpl['from'] = $from;
        $this->tpl['places'] = $places;
        $this->tpl['logoutfb'] = 0;

        if ($user->account_token) {
            $this->tpl['logoutfb'] = 1;
            $url = array('url' => base_url() . 'landings/done/' . $this->custom_url . '/'.$accid.'/0/fb');
            $this->load->library('facebook_v4/facebooks_v4', $url);
            $url_logout = site_url('landings/logouts/'.$places);
            $this->tpl['logout'] = $this->facebooks_v4->get_logout_tokens($user->account_token,$url_logout);
        }

        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landings/signupdone', $this->tpl, true);
        $this->load->view('landings/home', $this->tpl);
    }

    function fb_token() {
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        $landingid = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);

        $user_me = $this->facebook_v4->get_user();
        $token = $this->facebook_v4->token();
        if($accid == 0){
            $url = site_url('landings/logouts/'. $this->custom_url);
        }else{
            $url = site_url('landings/step4/'. $this->custom_url.'/'.$accid.'/'.$landingid.'/'.$from);
        }
        $logout_url = $this->facebook_v4->get_logout_url($url);

        if ($token && $user_me) {
            $data['account_fb_expired'] = $token['date'];
            $data['account_token'] = $token['token'];
            $data['account_fbid'] = $user_me['id'];
            $data['account_fb_name'] = $user_me['name'];
			$image_pp = $this->facebook_v4->image_pp();
            if($image_pp){
                $data['account_avatar'] = $image_pp->url;
            }
            if($accid == 0){
                $sql = "SELECT id FROM wooz_account
                    WHERE account_fbid = '" . $user_me['id'] . "'";
            }else{
                $sql = "SELECT id FROM wooz_account
                    WHERE id = '" . $accid . "'";
            }
            $query = $this->db->query($sql);
            $dataquery = $query->row();
            if ($dataquery) {
                $this->db->where('id', $dataquery->id);
                $this->db->update('account', $data);
                if($accid == 0){
                    $accid = $dataquery->id;
                    redirect('landings/step2/'.$this->custom_url.'/' . $accid . '/0/fb');
                }else{
                    redirect('landings/step5/'. $this->custom_url.'/'.$accid.'/'.$landingid.'/'.$from);
                }
                #redirect('landings/step5/'. $this->custom_url.'/'.$accid.'/'.$landingid.'/'.$from);
            } else {
                if($user_me['birthday']){
                    $birthday = explode('/', $user_me['birthday']);
                    $data['account_birthdate'] = $birthday[2].'-'.$birthday[0].'-'.$birthday[1];
                }
                if($user_me['email']){
                    $data['account_email'] = $user_me['email'];
                }
                if($user_me['gender']){
                    $data['account_gender'] = $user_me['gender'];
                }
                $data['account_displayname'] = $user_me['name'];
                $this->db->insert('account', $data);
                $accid = $this->db->insert_id();
                redirect('landings/step2/'.$this->custom_url.'/' . $accid . '/0/fb');
            }
            #$this->facebook_v4->destroy();
        }else{
            if($accid == 0){
                redirect('landings/logout/'.$this->custom_url);
            }else{
                redirect('landings/step4/'. $this->custom_url.'/'.$accid.'/'.$landingid.'/'.$from);
            }
        }
    }

    function logouts(){
        $places = $this->uri->segment(3,0);
        $accid = $this->uri->segment(4, 0);
        $landingid = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);

        $this->facebook_v4->destroy();
        if($accid == 0){
            redirect('landings/logout/'.$this->custom_url);
        }else{
            redirect('landings/step4/'. $this->custom_url.'/'.$accid.'/'.$landingid.'/'.$from);
        }
    }

    function logout() {
        $this->session->unset_userdata('aidi');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('session_id');
        $this->session->unset_userdata('ip_address');
        $this->session->unset_userdata('user_agent');
        $this->session->unset_userdata('last_activity');
        $this->session->unset_userdata('nice');
        $this->session->unset_userdata('grup');
        $this->session->unset_userdata('access');
        $this->session->unset_userdata('kode');

        #$this->facebook->destroySession();
        $this->_killFacebookCookies();
        $this->session->sess_destroy();
        session_destroy();
        unset($_SESSION);
        $past = time() - 3600;
        foreach ($_COOKIE as $key => $value) {
            setcookie($key, $value, $past, '/');
        }
		redirect('landings/home/' . $this->custom_url);
    }

    public function _killFacebookCookies() {
        // get your api key 
        $apiKey = $this->config->item('facebook_application_id');
        // get name of the cookie 

        $cookies = array('user', 'session_key', 'expires', 'ss');
        foreach ($cookies as $name) {
            setcookie($apiKey . '_' . $name, false, time() - 3600);
            unset($_COOKIE[$apiKey . '_' . $name]);
        }

        setcookie($apiKey, false, time() - 3600);
        unset($_COOKIE[$apiKey]);
    }

    function accountfound() {
        $this->tpl['page'] = "step3";
        $this->tpl['title'] = "Find different account";
        $this->tpl['likefollow'] = $this->input->get('likefollow', true);

        $this->tpl['from'] = $this->uri->segment(3, 0);
        $this->tpl['url'] = $this->input->get('url', true);
        $oldid = $this->uri->segment(4, 0);
        $newid = $this->uri->segment(5, 0);

        $this->tpl['user'] = $this->landing_m->gettable('id', $newid, 'account');
        $this->tpl['datalist'] = $this->landing_m->gettable('id', $oldid, 'account');

        $this->tpl['TContent'] = $this->load->view('landingnews/foundaccount', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);
    }

    function follow() {
        $userdata = $this->input->post('id');
        $data_user = $this->landings_m->gettable('id', $userdata, 'account');

        if ($data_user->account_tw_token != '' && $data_user->account_tw_secret != '') {
            $follow = $this->sosmedtw->post_follow_tw($data_user->account_tw_token, $data_user->account_tw_secret, $this->twiiter_id_fan);

            if ($follow == 1) {
                $data1['nama'] = "yes";
            } else {
                $data1['nama'] = "no";
            }
            echo json_encode($data1);
        }
    }

    function email_used_en($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('email');
        }
        $new = $this->db->get_where('account', array('account_email' => $str))->row();
        if ($new) {
            $news = $this->db->get_where('account', array('account_email' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $ceklandingemail = $this->landings_m->checkemail($str,$account_id);
                if($ceklandingemail){
                    return true;
                }else{
                    $this->form_validation->set_message('email_used_en', 'Registered email, please replace Email');
                    return false;
                }
            }
        } else {
            return true;
        }
    }

    function cek_data_dropdown_bulan_en($str) {
        if ($str != 0) {
            return true;
        } else {
            $this->form_validation->set_message('cek_data_dropdown_bulan_en', 'The %s field is required');
            return false;
        }
    }

    function cek_data_tanggal_en($str) {
        $day = $this->input->post('tgl');
        $month = $this->input->post('bln');
        $year = $this->input->post('thn');
        if($day && $month && $year){
            if (checkdate($month, $day, $year)) {
                return TRUE;
            } else {
                $this->form_validation->set_message('cek_data_tanggal_en', 'Enter the correct date of birth.');
                return FALSE;
            }
        }else{
            $this->form_validation->set_message('cek_data_tanggal_en', 'Enter the correct date of birth.');
            return FALSE;
        }
    }

    function year_chk_en($str) {
        $yearnow = date('Y');
        $syarat = $yearnow - 13;
        $syarat1 = $yearnow - 83;
        if (($str >= $syarat1) && ($str <= $syarat)) {
            return true;
        } else {
            $this->form_validation->set_message('year_chk_en', 'Year of birth does not qualify');
            return false;
        }
    }

    function rfid_chk($str, $account_id) {
        $str = strtolower($str);
        $chekrfid = $this->landings_m->check_rfid($str, $account_id);
        if (!$chekrfid) {
            $this->form_validation->set_message('rfid_chk', 'RFID already registered');
            return false;
        } else {
            return true;
        }
    }
}

?>
