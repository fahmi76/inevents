<?php

class report extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model(array('convert', 'landing_m'));

		$this->load->library('fungsi');
		$this->load->library('session');
		$this->load->library('curl');
		#date_default_timezone_set('Asia/Jakarta');
		date_default_timezone_set('America/Port_of_Spain');

		$this->load->helper('url');
		#$this->load->library('sosmed');

		$this->tpl['assets_url'] = $this->config->item('assets_url');
		$this->tpl['uploads_url'] = $this->config->item('uploads_url');
	}

	function index() {
		$this->tpl['main_title'] = 'Report';
		$this->tpl['core'] = 'Report';
		$this->tpl['title'] = "Inevents";
		$this->tpl['data'] = $this->datatabel();

		$this->tpl['TContent'] = $this->load->view('regis/report', $this->tpl, true);
		$this->load->view('regis/homereport', $this->tpl);
	}

	function booth($places, $nice) {
		$places = $this->get_id_places($nice);
		$this->load->model('dashboard/insight_m');
		$this->tpl['main_title'] = 'Report';
		$this->tpl['core'] = 'Report';
		$this->tpl['title'] = "Inevents";
		$this->tpl['gate'] = $places->id;
		$this->tpl['check'] = 1;
		$this->tpl['status'] = 1;
		$this->tpl['id'] = $places->places_parent;
		$this->tpl['tabel'] = $this->insight_m->list_tabel($places->places_parent);

		$this->tpl['TContent'] = $this->load->view('regis/reportbooth', $this->tpl, true);
		$this->load->view('regis/homereportbooth', $this->tpl);
	}

	function get_id_places($nice) {
		$sql = "SELECT id,places_parent FROM `wooz_places` where places_landing = '" . $nice . "' order by id desc";
		$query = $this->db->query($sql);
		$data = $query->row();
		return $data;
	}
	public function ajax_list($gate, $check, $status, $id) {
		$this->load->model('dashboard/mainstation_m');
		$this->load->model('dashboard/insight_m');
		$list = $this->mainstation_m->get_datatables($gate, $check, $status, $id);
		$data = array();
		$no = $_POST['start'];
		$tabel = $this->insight_m->list_tabel($id);
		foreach ($list as $person) {
			$no++;
			$row = array();
			if ($person->log_fb_places == 1) {
				$row[] = $person->account_displayname . ' (GUEST)';
			} else {
				$row[] = $person->account_displayname;
			}
			foreach ($tabel as $rowtabel) {
				$datas = $this->insight_m->ambil_content_data($person->account_id, $id, $rowtabel->id);
				if ($datas) {
					$row[] = $datas->content;
				} else {
					$row[] = '';
				}
			}
			$row[] = $person->log_stamps;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->mainstation_m->count_all($gate, $check, $status, $id),
			"recordsFiltered" => $this->mainstation_m->count_filtered($gate, $check, $status, $id),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function user() {
		$this->tpl['main_title'] = 'Report - User';
		$this->tpl['core'] = 'Report - User';
		$this->tpl['title'] = "Inevents";
		$this->tpl['data'] = $this->datauser();

		$this->tpl['TContent'] = $this->load->view('regis/reportuser', $this->tpl, true);
		$this->load->view('regis/homereport', $this->tpl);
	}

	function datauser() {
		$sql = "SELECT distinct(b.account_id),a.account_displayname,b.landing_joindate FROM `wooz_account` a inner join wooz_landing b on b.account_id = a.id where b.landing_register_form = 24";
		$query = $this->db->query($sql);
		$data = $query->result();

		$result = array();
		foreach ($data as $row) {
			$result[] = array(
				'date' => $row->landing_joindate,
				'name' => $row->account_displayname,
				'id' => $row->account_id,
				//'email' => $this->get_datauser(43,$row->account_id),
				//'guest' => $this->get_datauser(49,$row->account_id),
				//'shuttle' => $this->get_datauser(50,$row->account_id),
				//'elevator' => $this->get_datauser(51,$row->account_id)
			);
		}
		//xdebug($result);
		return $result;
	}

	function get_datauser($form_id, $acc_id) {
		$hasil = '';
		$sql = "select a.content "
			. "from wooz_form_regis_detail a where a.form_regis_id = '" . $form_id . "' and account_id = '" . $acc_id . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$hasil = $data->content;
		}
		return $hasil;
	}

	function ambil_data_user($acc_id, $form_id) {
		$sql = "SELECT count(distinct(account_id)) as total FROM `wooz_form_regis_detail` where form_regis_id = '" . $acc_id . "' and content = 'Yes' and date_add like '%" . $form_id . "%'";
		$query = $this->db->query($sql);
		$data = $query->row();
		return $data->total;
	}

	function datatabel() {
		$sql = "SELECT DISTINCT(DATE_FORMAT(date_add, '%Y/%m/%d')) AS thedate,DATE_FORMAT(date_add, '%Y-%m-%d') as dataasli,count(distinct(account_id)) as rsvp FROM `wooz_form_regis_detail` where places_id = 24 GROUP BY thedate ORDER BY thedate asc";
		$query = $this->db->query($sql);
		$data = $query->result();

		$result = array();
		foreach ($data as $row) {
			$result[] = array(
				'date' => $row->thedate,
				'rsvp' => $this->ambil_data_user(48, $row->dataasli),
				'guest' => $this->ambil_data_user(49, $row->dataasli),
				'shuttle' => $this->ambil_data_user(50, $row->dataasli),
				'elevator' => $this->ambil_data_user(51, $row->dataasli),
			);
		}
		return $result;
	}

	function list_tabel() {
		$data = $this->datatabel();
		$tabel = "<style type='text/css'>
    .datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 2px solid #006699; -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px; }.datagrid table td, .datagrid table th { padding: 6px 20px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; color:#FFFFFF; font-size: 16px; font-weight: bold; border-left: 2px solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00496B; border-left: 2px solid #E1EEF4;font-size: 13px;border-bottom: 3px solid #E1EEF4;font-weight: normal; }.datagrid table tbody .alt td { background: #E1EEF4; color: #00496B; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }
</style>";
		$tabel .= '<div class="datagrid"><table>
            <thead><tr><th>Date</th><th>#RSVP</th><th># WITH GUEST</th><th># SOUTH SHUTTLE</th><th># TAKE THE ELEVATOR HOME</th></tr></thead>
            <tbody>';
		foreach ($data as $row) {
			$tabel .= "<tr>
                    <td>" . $row['date'] . "</td>
                    <td>" . $row['rsvp'] . "</td>
                    <td>" . $row['guest'] . "</td>
                    <td>" . $row['shuttle'] . "</td>
                    <td>" . $row['elevator'] . "</td>
                </tr>";
		}
		$tabel .= '</tbody>
        </table></div>';
		return $tabel;
	}

	function mandrill() {
		$hasil_email = 0;
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;

		try {
			$this->mandrill->init($this->config->item('mandrill_api_key'));
			$mandrill_ready = TRUE;
		} catch (Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
		}

		if ($mandrill_ready) {
			//Send us some email!
			$email = array(
				'html' => $this->list_tabel(), //Consider using a view file
				'subject' => 'Registration data',
				'from_email' => 'donotreply@wooz.in',
				'from_name' => 'inevents',
				//'to' => array(array('email' => $this->ambil_data_user($acc_id,30)))
				'to' => array(array('email' => 'owrightalexis@gmail.com')),
			);
			$result = $this->mandrill->messages_send($email);
			$hasil_email = $result[0]['_id'];
		}
		//return $hasil_email;
		redirect('report');
	}

	function download() {
		$id = 24;
		$this->load->library('excel');
		$this->load->model('dashboard/user_m');
		$this->load->model('dashboard/visitor_m');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

		$objPHPExcel->setActiveSheetIndex(0);

		$tabel = $this->user_m->list_tabel($id);
		if ($id == 20) {
			$header = array('First Name', 'Last Name');
		} else {
			$header = array('Name');
		}
		foreach ($tabel as $tabelrow) {
			$header[] = $tabelrow->field_name;
		}
		$header[] = 'Date add';

		$col = 0;
		foreach ($header as $field) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			$col++;
		}
		$user = $this->datauser();
		// Fetching the table data
		$row = 2;
		foreach ($user as $data) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $data['name']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $this->get_datauser(43, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $this->get_datauser(44, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $this->get_datauser(45, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $this->get_datauser(46, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $this->get_datauser(47, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $this->get_datauser(48, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $this->get_datauser(49, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $this->get_datauser(50, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $this->get_datauser(51, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $data['date']);

			$row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		// Sending headers to force the user to download the file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="datauser.xls"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}

}
