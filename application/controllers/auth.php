<?php

class auth extends CI_controller {

    protected $tpl;
    protected $setting;

    function auth() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->load->database();

        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->library('twitter');
        $this->load->library('EpiFoursquare');
        $this->load->model('convert');

        $this->load->helper('url');
        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');

        $this->tpl['fb_perms'] = $fb_perms;
        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));
    }

    function login() {
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
        $this->tpl['page'] = "login";
        $this->tpl['title'] = "Login";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        $this->session->unset_userdata('idadmin');
        $this->session->unset_userdata('aidi');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('nice');
        $this->session->unset_userdata('grup');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('maillogin', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('passlogin', 'Password', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $data['account_email'] = $this->input->post('maillogin', true);
            $data['account_passwd'] = $this->fungsi->acak($this->input->post('passlogin', true));

            $count = $this->db->get_where('account', $data)->row();

            if ($count) {
                if ($count->account_status == 1) {
                    $keyword['aidi'] = $count->id;
                    $keyword['nama'] = $count->account_displayname;
                    $keyword['nice'] = $count->account_username;
                    $keyword['grup'] = $count->account_group;
                    $this->session->set_userdata($keyword);
                    //print_r($this->session->set_userdata($keyword));exit;

                    $row = $this->db->get_where('account', array('id' => $userdata))->row();

                    $up['account_lastlogin'] = date('Y-m-d h:i:s');
                    $up['account_typelogin'] = 'web';
                    $up['account_countlogin'] = $count->account_countlogin + 1;
                    $this->db->where('id', $count->id);
                    $this->db->update('account', $up);

                    redirect('profile');
                } else {
                    $this->tpl['notice'] = 'Error Found';
                    $this->tpl['value'] = "Account Not Activated Yet";
                    $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                }
            } else {
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['value'] = "Wrong Email or Password";
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
            }
        } else {
            $this->tpl['TContent'] = $this->load->view('auth/login', $this->tpl, true);
        }

        $this->load->view('body', $this->tpl);
    }

    function logout() {
        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        $this->session->unset_userdata('aidi');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('nice');
        $this->session->unset_userdata('grup');
        $this->session->sess_destroy();
		session_destroy();
		unset($_SESSION);
        $this->session->sess_destroy();
        redirect();
    }

    /*
      function joinlama() {
      $this->tpl['page'] = "join";
      $this->tpl['title'] = "Registrasi";

      $userdata = $this->session->userdata('aidi');
      $this->tpl['user'] = $userdata;

      if(isset($userdata)&&$userdata!="") {
      redirect('woozer/profile');
      }

      $this->load->helper(array('form'));
      $this->load->library('form_validation');

      $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required');
      $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[5]|max_length[15]|alpha_numeric');
      $this->form_validation->set_rules('password2', 'Re-Type Password', 'trim|required|matches[password1]');
      $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_used');

      if ($this->form_validation->run() === TRUE) {

      $data['account_displayname'] = $this->input->post('fullname',true);
      $data['account_username'] = $this->convert->nicename($this->input->post('fullname',true));
      $data['account_passwd'] = $this->fungsi->acak($this->input->post('password1',true));
      $data['account_email'] = $this->input->post('email',true);
      $data['account_verify'] = $this->fungsi->recreate2($this->fungsi->acak($data['account_username']));
      $data['account_status'] = 1;

      $config['charset'] = 'iso-8859-1';
      $config['protocol'] = 'mail';
      $config['mailtype'] = 'html';
      $config['wordwrap'] = FALSE;

      $message = "<h1>Hi ".$data['account_displayname'].",</h1>";
      $message .= "<p>Please <a href=\"".site_url()."/auth/activate/".$data['account_verify']."/".$data['account_username']."/\"><strong>click here</strong></a> to confirm your email</p>";
      $message .= "<p>&nbsp;</p><p>Once you confirm that this is your email address, you'll be able to submit your stories to <strong>Wooz.In</strong>.</p>";
      $message .= "<p>&nbsp;</p><p>If the \"click here\" link isn't supported by your email program, click this link or paste it into your web browser:</p>";
      $message .= "<p>&nbsp;</p><p><strong>".site_url()."/auth/activate/".$data['account_verify']."/".$data['account_username']."/</strong></p>";
      $message .= "<p>&nbsp;</p><p>See you back on Wooz.In!</p>";
      $message .= "<p>&nbsp;</p><p>- Wooz.In Team</p>";

      $this->load->library('email');

      $this->email->initialize($config);
      $this->email->from('contact@wooz.in', 'Wooz.In');
      $this->email->to($data['account_email']);

      $this->email->subject("Wooz.In Account Activation");
      $this->email->message($message);

      if ( ! $this->email->send()) {
      $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
      $this->tpl['notice'] = 'Error Found';
      $this->tpl['TContent'] = $this->load->view('notice',$this->tpl,true);
      } else {
      $this->db->insert('account',$data);
      $new = $this->db->insert_id();
      if(!$new) {
      $this->tpl['value'] = "Maaf, terdapat kesalahan pada database kami, harap menghubungi administrator";
      $this->tpl['notice'] = 'Error Found';
      $this->tpl['TContent'] = $this->load->view('notice',$this->tpl,true);
      } else {
      $count = $this->db->get_where('account',array('id' => $new))->row();

      $keyword['aidi'] = $count->id;
      $keyword['nama'] = $count->account_displayname;
      $keyword['nice'] = $count->account_username;
      $keyword['grup'] = $count->account_group;
      $this->session->set_userdata($keyword);

      redirect('woozer/'.$count->account_username);

      $this->tpl['value'] = "Terima kasih, silakan cek kembali e-mail Anda untuk konfirmasi registrasi dan anda bisa Login sekarang :)";
      $this->tpl['notice'] = 'Thank You';
      $this->tpl['TContent'] = $this->load->view('notice',$this->tpl,true);
      }
      }
      } else {
      $this->tpl['TContent'] = $this->load->view('auth/join',$this->tpl,true);
      }

      $this->load->view('body',$this->tpl);
      }
     */

    function cek() {
        $this->tpl['page'] = "cek account";
        $this->tpl['title'] = "cek account";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        if (isset($userdata) && $userdata != "") {
            redirect('profile');
        }
        $type = $this->uri->segment(2, 1);
        $this->tpl['pole'] = $type;

        $this->tpl['TContent'] = $this->load->view('auth/cek', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function signup() {
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
        $this->tpl['page'] = "signup";
        $this->tpl['title'] = "signup";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        if (isset($userdata) && $userdata != "") {
            redirect('profile');
        }
        $type = $this->uri->segment(2, 1);
        $this->tpl['pole'] = $type;

        $this->tpl['TContent'] = $this->load->view('auth/ceksignup', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function signupdone() {
        $this->tpl['page'] = "signup";
        $this->tpl['title'] = "signup";
        $this->tpl['user'] = '';
        $pole = $this->uri->segment(2, 0);
        $userdata = $this->session->userdata('aidi');
        if (isset($userdata) && $userdata == '')
            $userdata = $this->session->userdata('kode');
        $data = $this->db->get_where('account', array('id' => $userdata, 'account_status' => 1))->row();

        if ($pole == 'step1') {
            $data1 = $this->fungsi->recreate2($this->fungsi->acak($data->account_username));
            $this->db->update('account', array('account_verify' => $data1), array('id' => $userdata));
            $this->tpl['datalist'] = $data;
            $this->tpl['pole'] = $pole;
        } elseif ($pole == 'step2') {
            $type = $this->uri->segment(2, 2);
            $this->tpl['pole'] = $type;
        } else {
            $config['charset'] = 'iso-8859-1';
            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = FALSE;

            $message = "<h1>Hi " . $data->account_username . ",</h1>";
            $message .= "<p>Please <a href=\"" . site_url() . "/auth/activate/" . $data->id . "/" . $data->account_verify . "/\"><strong>click here</strong></a> to confirm your email</p>";
            $message .= "<p>&nbsp;</p><p>Once you confirm that this is your email address, you'll be able to submit your stories to <strong>Wooz.In</strong>.</p>";
            $message .= "<p>&nbsp;</p><p>If the \"click here\" link isn't supported by your email program, click this link or paste it into your web browser:</p>";
            $message .= "<p>&nbsp;</p><p><strong>" . site_url() . "/auth/activate/" . $data->id . "/" . $data->account_verify . "/</strong></p>";
            $message .= "<p>&nbsp;</p><p>See you back on Wooz.In!</p>";
            $message .= "<p>&nbsp;</p><p>- Wooz.In Team</p>";

            $this->load->library('email');

            $this->email->initialize($config);
            $this->email->from('contact@wooz.in', 'Wooz.In');
            $this->email->to($data->account_email);

            $this->email->subject("Wooz.In Account Activation");
            $this->email->message($message);

            if (!$this->email->send()) {
                $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
            } else {
                $this->tpl['datalist'] = $data;
                $this->tpl['pole'] = 'step3';
            }
        }
        $this->tpl['TContent'] = $this->load->view('auth/cekdone', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function join() {
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
        $this->session->unset_userdata('mode');
        $this->session->unset_userdata('token');
        $this->tpl['page'] = "signup";
        $this->tpl['title'] = "Registrasi";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        if (isset($userdata) && $userdata != "") {
            redirect('profile');
        }

        $this->tpl['TContent'] = $this->load->view('auth/signup', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function joinnew() {
        $this->tpl['page'] = "signupnext";
        $this->tpl['title'] = "Registrasi";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        if (isset($userdata) && $userdata != "") {
            redirect('profile');
        }
        $user = $this->facebook->getUser();

        $mode = $this->session->userdata('mode');
        $this->facebook->setExtendedAccessToken();
        $token = $this->facebook->getAccessToken();

        if (!$token) {
            redirect('signup');
        }

        if ($user) {
            $info = $this->facebook->api(array(
                        'access_token' => $token,
                        'method' => 'fql.query',
                        'query' => 'SELECT uid,name,pic_big,birthday_date,sex,email,hometown_location FROM user WHERE uid=me()',
                    ));
        }

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        /*
          $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required');
          $this->form_validation->set_rules('serial', 'RFID Serial', 'trim|numeric|callback_rfid_chk');
          $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[5]|max_length[15]|alpha_numeric');
          $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_used');
         */
        $this->form_validation->set_rules('serial', 'RFID Serial', 'trim|required|numeric|callback_rfid_chk');

        if ($this->form_validation->run() === TRUE) {
            $data['account_rfid'] = $this->input->post('serial', true);

            $data['account_displayname'] = $info[0]['name'];
            $data['account_username'] = $this->convert->nicename($info[0]['name']);
            $data['account_email'] = $info[0]['email'];
            $data['account_verify'] = $this->fungsi->recreate2($this->fungsi->acak($data['account_username']));
            $data['account_status'] = 1;
            /*
              $data['account_passwd'] = $this->fungsi->acak($this->input->post('password1', true));
              $data['account_verify'] = $this->fungsi->recreate2($this->fungsi->acak($data['account_username']));
             */
            if ($mode == 'facebook' && $user) {
                $data['account_fbid'] = $user;
                $data['account_token'] = $token;

                if ($info[0]['birthday_date'] != "") {
                    $a = explode('/', $info[0]['birthday_date']);
                    $a = $a[2] . '-' . $a[0] . '-' . $a[1];
                    $data['account_birthdate'] = $a;
                }

                if (isset($info[0]['sex']) && $info[0]['sex'] != "") {
                    $data['account_gender'] = $info[0]['sex'];
                }

                if (isset($info[0]['hometown_location']) && $info[0]['hometown_location'] != "") {
                    $data['account_location'] = $info[0]['hometown_location']['city'];
                }

                if ($info[0]['pic_big'] != "") {
                    $data['account_avatar'] = $info[0]['pic_big'];
                }
            }

            $config['charset'] = 'iso-8859-1';
            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = FALSE;

            $message = "<h1>Hi " . $data['account_displayname'] . ",</h1>";
            $message .= "<p>Please <a href=\"" . site_url() . "/auth/activate/" . $data['account_verify'] . "/" . $data['account_username'] . "/\"><strong>click here</strong></a> to confirm your email</p>";
            $message .= "<p>&nbsp;</p><p>Once you confirm that this is your email address, you'll be able to submit your stories to <strong>Wooz.In</strong>.</p>";
            $message .= "<p>&nbsp;</p><p>If the \"click here\" link isn't supported by your email program, click this link or paste it into your web browser:</p>";
            $message .= "<p>&nbsp;</p><p><strong>" . site_url() . "/auth/activate/" . $data['account_verify'] . "/" . $data['account_username'] . "/</strong></p>";
            $message .= "<p>&nbsp;</p><p>See you back on Wooz.In!</p>";
            $message .= "<p>&nbsp;</p><p>- Wooz.In Team</p>";

            $this->load->library('email');

            $this->email->initialize($config);
            //$this->email->from('contact@wooz.in', 'Wooz.In');
            $this->email->from('hayria76@gmail.com', 'tes');
            $this->email->to($data['account_email']);

            //$this->email->subject("Wooz.In Account Activation");
            $this->email->subject("tes");
            $this->email->message($message);

            if (!$this->email->send()) {
                $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
            } else {

                $this->db->insert('account', $data);
                $new = $this->db->insert_id();

                if (!$new) {
                    $this->tpl['value'] = "Maaf, terdapat kesalahan pada database kami, harap menghubungi administrator";
                    $this->tpl['notice'] = 'Error Found';
                    $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                } else {
                    $count = $this->db->get_where('account', array('id' => $new))->row();
                    $setting = $this->db->get_where('setting', array('id' => 1))->row();

                    $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    $options['link'] = base_url();
                    $options['name'] = $setting->site_status . ' activation';
                    $options['description'] = $count->account_displayname . ' just activated ' . $setting->site_status;
                    $options['message'] = "Join. Connect. Woozin'";
                    $options['access_token'] = $count->account_token;

                    //$url = $this->facebook->api("/$count->account_fbid/feed", 'post',$options); 

                    $this->load->library('arthurday');
                    $this->arthurday->set_token($count->account_token);
                    $this->arthurday->insert_all($new);

                    if (isset($userdata) && $userdata == '') {
                        $keyword['kode'] = $count->id;
                        $this->session->set_userdata($keyword);
                    }

                    $type = $this->uri->segment(2, 2);
                    $this->tpl['pole'] = $type;
                    redirect('signupskip/' . $type);

                    $this->tpl['value'] = "Terima kasih, silakan cek kembali e-mail Anda untuk konfirmasi registrasi dan anda bisa Login sekarang :)";
                    $this->tpl['notice'] = 'Thank You';
                    $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                }
            }
        } else {
            if ($mode == 'facebook' && is_array($token)) {
                $this->tpl['nama'] = $info[0]['name'];
                $this->tpl['email'] = $info[0]['email'];
            }

            $this->tpl['TContent'] = $this->load->view('auth/signupnext', $this->tpl, true);
        }

        $this->load->view('body', $this->tpl);
    }

    function joinskip() {
        $type = $this->uri->segment(2, 0);
        $this->tpl['pole'] = $type;
        $this->tpl['page'] = "signupnext";
        $this->tpl['title'] = "Registrasi";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        $kode = $this->session->userdata('kode');

        if (isset($userdata) && $userdata != "") {
            redirect('profile');
        }

        if ($kode == '') {
            redirect('signup');
        }

        $this->tpl['TContent'] = $this->load->view('auth/signupskip', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function joindone() {
        $type = $this->uri->segment(2, 0);
        $this->tpl['pole'] = $type;
        $this->tpl['page'] = "signupnext";
        $this->tpl['title'] = "Registrasi";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        $kode = $this->session->userdata('kode');
        if (isset($userdata) && $userdata != "") {
            redirect('profile');
        }

        if ($kode == '') {
            redirect('signup');
        }

        $row = $this->db->get_where('account', array('id' => $kode))->row();

        $keyword['aidi'] = $row->id;
        $keyword['nama'] = $row->account_displayname;
        $keyword['nice'] = $row->account_username;
        $keyword['grup'] = $row->account_group;
        $this->session->set_userdata($keyword);

        $this->session->unset_userdata('kode');

        redirect('profile');
    }

    function activate() {
        $this->tpl['page'] = "auth/activate";
        $this->tpl['title'] = "Aktivasi";

        $value1 = $this->uri->segment(3, 0);
        $value2 = $this->uri->segment(4, 0);

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        $data = $this->db->get_where('account', array('id' => $value1, 'account_status' => 1))->row();
        if (!is_array($data)) {
            $chk1 = $this->fungsi->recreate2($this->fungsi->acak($data->account_username));

            if ($chk1 == $value2) {
                $chk2 = $this->db->get_where('account', array('account_verify' => $value2, 'id' => $value1))->row();
                if (count($chk2) == 0) {
                    $chk3 = $this->db->get_where('account', array('account_verify' => $value2))->row();
                    if (count($chk3)) {
                        $username = $this->fungsi->nicename($chk3->account_displayname);
                        if ($username == $value2)
                            $chk2 = $chk3;
                    }
                }
                if (count($chk2) == 0) {
                    $this->tpl['value'] = "User not found";
                    $this->tpl['notice'] = 'Error Found';
                    $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                } else {
                    if ($chk2->account_verify != $chk1) {
                        $this->tpl['value'] = "Verification code not found";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {
                        $this->db->update('account', array('account_status' => 1, 'account_verify' => 1), array('id' => $chk2->id));

                        $keyword['aidi'] = $chk2->id;
                        $keyword['nama'] = $chk2->account_displayname;
                        $keyword['nice'] = $chk2->account_username;
                        $keyword['grup'] = $chk2->account_group;
                        $this->session->set_userdata($keyword);

                        redirect('profile');

                        $this->tpl['value'] = "Your Account now verified";
                        $this->tpl['notice'] = 'Thank You';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    }
                }
            } else {
                $this->tpl['value'] = "Verification code not found";
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
            }
        } else {
            $this->tpl['value'] = "Verification code not found";
            $this->tpl['notice'] = 'Error Found';
            $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
        }
        $this->load->view('body', $this->tpl);
    }

    function forgotpassword() {
        $this->tpl['page'] = "auth/forgotpassword";
        $this->tpl['title'] = "Forgot Password";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        if ($userdata == "") {
            $this->load->helper(array('form'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_available');

            if ($this->form_validation->run() === TRUE) {
                $this->db->where('account_email', $this->input->post('email', true));
                $this->db->select();
                $data = $this->db->get('account')->row();
                $conf2 = substr($this->fungsi->acak(date("y-m-d H:i:s")), 10, 10);
                $passwd = $this->fungsi->acak($conf2);

                $message = "<h1>Hi " . $data->account_displayname . ",</h1>";
                $message .= "<p>&nbsp;</p><p>this is your new password</p>";
                $message .= "<p>&nbsp;</p><p>Password : '" . $conf2 . "'</p>";
                $message .= "<p>&nbsp;</p><p>Once you've logged in, don't forget to change your password to a familiar one</p>";
                $message .= "<p>&nbsp;</p><p>See you back on Wooz.in!</p>";
                $message .= "<p>&nbsp;</p><p>- Wooz.in Team</p>";

                $config['charset'] = 'iso-8859-1';
                $config['protocol'] = 'mail';
                $config['mailtype'] = 'html';
                $config['wordwrap'] = FALSE;

                $this->load->library('email');
                $this->email->initialize($config);
                $this->email->from('contact@wooz.in', 'Wooz.in');
                $this->email->to($data->email);
                $this->email->subject("Wooz.in Password Retrieval");
                $this->email->message($message);

                if (!$this->email->send()) {
                    $this->tpl['value'] = "Your Password not updated, please try again";
                    $this->tpl['notice'] = 'Error Found';
                    $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                } else {
                    $sql = "UPDATE m_user SET passwrd='" . $passwd . "' WHERE id_user=?";
                    $row = $this->db->query($sql, array($data->id_user));

                    $this->tpl['value'] = "Please Check Your Email For Your New Password";
                    $this->tpl['notice'] = 'Thank You';
                    $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                }
            } else {
                $this->tpl['TContent'] = $this->load->view('auth/password', $this->tpl, true);
            }
        } else {
            $this->tpl['value'] = "Please Change Your Password Through Your Profile Page";
            $this->tpl['notice'] = 'Error Found';
            $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
        }
        $this->load->view('body', $this->tpl);
    }

    function revoke() {
        $type = $this->uri->segment(3, 0);
        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        if (!$userdata) {
            redirect('login');
        }

        $row = $this->db->get_where('account', array('id' => $userdata))->row();
        switch ($type) {
            case 'fb':
                $this->db->where('id', $userdata);
                $db['account_fbid'] = NULL;
                $db['account_token'] = NULL;
                $this->db->update('account', $db);
                redirect('profile');
                break;
            case 'tw':
                $this->db->where('id', $userdata);
                $db['account_tw_token'] = NULL;
                $db['account_tw_secret'] = NULL;
                $this->db->update('account', $db);
                redirect('profile');
                break;
            case 'fs':
                $this->db->where('id', $userdata);
                $db['account_fs_token'] = NULL;
                $db['account_fs_secret'] = NULL;
                $this->db->update('account', $db);
                redirect('profile');
                break;
            case 'gw':
                $this->db->where('id', $userdata);
                $db['account_gw_token'] = NULL;
                $db['account_gw_secret'] = NULL;
                $this->db->update('account', $db);
                redirect('profile');
                break;
            default:
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['value'] = "You can't access this page";
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                break;
        }
    }

    function rfid_chk($str) {
        $str = strtolower($str);
        if ($str != "") {
            $row = $this->db->get_where('account', array('account_rfid' => $str))->row();
            if (count($row) != 0) {
                $this->form_validation->set_message('rfid_chk', 'Serial already registered');
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    function username_chk($str) {
        $userdata = $this->session->userdata('aidi');

        $str = strtolower($str);
        $row = $this->db->get_where('account', array('account_username' => $str))->row();
        if (count($row) != 0 && (isset($userdata) && $row->id != $userdata) && $str == 'admin' && $str == 'index' && $str == 'page') {
            $this->form_validation->set_message('username_chk', 'Username already used');
            return false;
        } else {
            return true;
        }
    }

    function email_available($str) {
        $this->db->where('account_email', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('account')->row();
        if ($new->jumlah == 0) {
            $this->form_validation->set_message('email_available', 'Email Not Registered');
            return false;
        } else {
            return true;
        }
    }

    function email_used($str) {
        $this->db->where('account_email', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('account')->row();
        if ($new->jumlah != 0) {
            $this->form_validation->set_message('email_used', 'Email Already Registered');
            return false;
        } else {
            return true;
        }
    }

    function __nicename($file) {
        $healthy = array(" ", ".", ",", "?", "&", "(", ")", "#", "!", "^", "%", "\'", "@", "}", "{", "]", "[", "|", "", "'", "/", "|", ":", '"', "<", ">", ";", "--");
        $yummy = array("-", "",);
        $result = strtolower(str_replace($healthy, $yummy, trim($file)));

        $this->db->select('');
        $this->db->where('account_username', $result);
        $this->db->from('account');
        $c = $this->db->count_all_results();

        if ($c) {
            $this->db->select('');
            $this->db->like('account_username', $result, 'after');
            $this->db->from('account');
            $c = $this->db->count_all_results();

            if ($c) {
                $this->db->select('');
                $this->db->like('account_username', $result, 'after');
                $this->db->order_by('id', 'desc');
                $this->db->limit(1, 0);
                $res = $this->db->get('account')->row();

                $slice = explode('-', $res->account_username);
                $cc = count($slice);
                if (is_numeric($slice[$cc - 1])) {
                    $slice[$cc - 1] = $slice[$cc - 1] + 1;
                    $jadi = implode('-', $slice);
                } else {
                    $jadi = $result . '-1';
                }
            } else {
                $jadi = $result . '-1';
            }
        } else {
            $jadi = $result;
        }
        return $jadi;
    }

    function __tanggal($tanggal_f) {
        if (strstr($tanggal_f, ' ')) {
            $waktu_f = explode(' ', $tanggal_f);
            $tanggal_asli_f = explode('-', $waktu_f[0]);
            $waktu_asli_f = explode(':', $waktu_f[1]);
            $tanggal_cetak_f = date('M d, Y', mktime($waktu_asli_f[0], $waktu_asli_f[1], $waktu_asli_f[2], $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        } else {
            $tanggal_asli_f = explode('-', $tanggal_f);
            $tanggal_cetak_f = date('M d, Y', mktime(0, 0, 0, $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        }
        return $tanggal_cetak_f;
    }

}

