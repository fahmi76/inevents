<?php

class polling extends CI_controller {

    protected $tpl;

    function __construct() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->model('fritz_m');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->helper('url');
        $this->load->helper('date');
        date_default_timezone_set('Asia/Jakarta');

        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $custom_page = $this->input->get('url', true);
        if ($custom_page != '') {
            $customs = $this->fritz_m->getplaceslanding($custom_page);
            if ($customs) {
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;

                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                }
                if ($customs->places_twitter != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        redirect('polling/form?url='.$this->custom_url.'&loc=1');
    }

    function form() {
        $loc = $this->input->get('loc', true);
        $loc = 1;
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Polling";
        $this->load->library('form_validation');
        $this->form_validation->set_rules('rfid', 'RFID', 'required');
        if ($this->form_validation->run() === TRUE) {
			$rfid = $this->input->post('rfid');
            $chek_rfid = $this->fritz_m->check_rfid();
            if ($chek_rfid) {
                $check_polling = $this->fritz_m->checkpolling($chek_rfid->account_id);
				if($check_polling){
					redirect('polling/found/' . $loc . '/' . $chek_rfid->account_id . '/' . $chek_rfid->account_username.'?url='.$this->custom_url);
				}else{
					redirect('polling/notpolling/' . $loc . '/' . $chek_rfid->account_id . '/' . $chek_rfid->account_username.'?url='.$this->custom_url);
				}
            } else {
                redirect('polling/notfound/' . $loc.'?url='.$this->custom_url);
            }
        }
        $this->tpl['loc'] = $loc;
        $this->tpl['TContent'] = $this->load->view('polling/rfid', $this->tpl, true);
        $this->load->view('polling/home', $this->tpl);
    }

    function found($loc, $account_id = null, $username = null) {
        if (!$loc || !$account_id || !$username) {
            redirect('polling?url='.$this->custom_url);
        }
        $data = $this->fritz_m->get_data($account_id, $username);
        if (!$data) {
            redirect('polling/notfound/' . $loc.'?url='.$this->custom_url);
        }
		
        $this->tpl['loc'] = $loc;
        $this->tpl['data'] = $data;
        $this->tpl['TContent'] = $this->load->view('polling/found_v', $this->tpl, true);
        $this->load->view('polling/home', $this->tpl);
    }
	
    function foundtest($loc, $account_id = null, $username = null) {
        if (!$loc || !$account_id || !$username) {
            redirect('polling?url='.$this->custom_url);
        }
        $data = $this->fritz_m->get_data($account_id, $username);
        if (!$data) {
            redirect('polling/notfound/' . $loc.'?url='.$this->custom_url);
        }
		
        $this->tpl['loc'] = $loc;
        $this->tpl['data'] = $data;
        $this->tpl['TContent'] = $this->load->view('polling/foundtest', $this->tpl, true);
        $this->load->view('polling/home', $this->tpl);
    }

	function pilih($polling,$loc, $account_id = null, $username = null){
        if (!$polling || !$loc || !$account_id || !$username) {
            redirect('polling?url='.$this->custom_url);
        }
		if($polling == 1){
			$datauser = $this->fritz_m->get_data($account_id, $username);
			$this->curl->simple_post('updatestatus/home?place=socialite-polling-cruise', array('card'=> $datauser->account_rfid, 'booth' => 484));
		}else{
			$datauser = $this->fritz_m->get_data($account_id, $username);
			$this->curl->simple_post('updatestatus/home?place=socialite-polling-dicaprio', array('card'=> $datauser->account_rfid, 'booth' => 483));
		}
		$data['polling'] = $polling;
		$data['account_id'] = $account_id;
		$data['places_id'] = $this->custom_id;
		$data['status'] = 1;
		$data['date'] = date('Y-m-d H:i:s');
		$data['dates'] = date('Y-m-d');
		$data['times'] = date('H:i:s');
		$sql = $this->db->insert_string('fritz_polling', $data) . ' ON DUPLICATE KEY UPDATE date=date+1';
		$this->db->query($sql);
		#$this->db->insert('fritz_polling', $data);
        redirect('polling/finish/'.$loc.'/'.$account_id.'?url='.$this->custom_url);
	}
	
    function notfound($loc) {
        $this->tpl['loc'] = $loc;
        $this->tpl['TContent'] = $this->load->view('polling/notfound', $this->tpl, true);
        $this->load->view('polling/home', $this->tpl);
    }

    function notpolling($loc) {
        $this->tpl['loc'] = $loc;
        $this->tpl['TContent'] = $this->load->view('polling/notpolling', $this->tpl, true);
        $this->load->view('polling/home', $this->tpl);
    }
	
    function finish($loc = 1,$id) {
		/*
		$update_status = $this->fritz_m->cek_log_status($id,436);
		if($update_status){
			$this->curl->simple_post('updatestatus/home?place=guinness-live-unplugged', array('card'=> $rfid, 'booth' => 436));
		} */
        $this->tpl['loc'] = $loc;
        $this->tpl['TContent'] = $this->load->view('polling/signupdone', $this->tpl, true);
        $this->load->view('polling/home', $this->tpl);
    }

	function view(){
        $this->tpl['page'] = "Home";
        $this->tpl['title'] = "View Polling";
		$this->tpl['data']  = $this->fritz_m->viewpolling($this->custom_id);
        $this->tpl['TContent'] = $this->load->view('polling/viewpolling', $this->tpl, true);

        $this->load->view('polling/home', $this->tpl);
	}
	
	function view1(){
        $this->tpl['page'] = "Home";
        $this->tpl['title'] = "View Polling";
		$this->tpl['data']  = $this->fritz_m->viewpolling();
        $this->tpl['TContent'] = $this->load->view('polling/viewpolling1', $this->tpl, true);

        $this->load->view('polling/home', $this->tpl);
	}

	function viewtest(){
        $this->tpl['page'] = "Home";
        $this->tpl['title'] = "View Polling";
		$this->tpl['data']  = $this->fritz_m->viewpolling();
		
        $this->tpl['TContent'] = $this->load->view('polling/viewtest', $this->tpl, true);

        $this->load->view('polling/home', $this->tpl);
	}
	
	
	function datapolling(){
		$data = $this->fritz_m->viewpolling();
        array_push($json, array('pollingA' => $data->pollingA,
                'pollingB' => $data->pollingB));
        echo json_encode(array("json" => $json));
	
	}
	
}

?>
