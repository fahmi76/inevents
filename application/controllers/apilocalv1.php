<?php
class apilocalv1 extends CI_controller {

	protected $tpl;
	protected $placesid = 38;

	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('fungsi');
		$this->load->model('dashboard/user_m');
	}

	function index() {
		echo 'test';
	}

	function places($placesid) {
		//$placesid = $this->placesid;
		$data = array(
			'status' => 0,
			'name' => 'venue ended',
		);

		$sqluser = "select id,places_name,places_backgroud,places_logo,places_startdate,places_duedate from wooz_places where id = '" . $placesid . "'";
		$queryuser = $this->db->query($sqluser);
		$user = $queryuser->row();
		if ($user) {
			$data = array(
				'status' => 1,
				'id' => $user->id,
				'name' => $user->places_name,
				'background' => base_url() . $user->places_backgroud,
				'logo' => base_url() . $user->places_logo,
				'sdate' => $user->places_startdate,
				'edate' => $user->places_duedate,
			);
		}
		echo json_encode($data);
	}

	function gate($placesid) {
		//$placesid = $this->placesid;
		$data = array(
			'status' => 0,
			'data' => array(),
		);
		$sqluser = "select id,nama from wooz_gate where places_id = '" . $placesid . "'";
		$queryuser = $this->db->query($sqluser);
		$user = $queryuser->result();
		if ($user) {
			$data = array(
				'status' => 1,
			);
			foreach ($user as $row) {
				$data['data'][] = array(
					'id' => $row->id,
					'nama' => $row->nama,
				);
			}
		}
		echo json_encode($data);
	}

	function gateid($placesid, $id) {
		//$placesid = $this->placesid;
		$data = array(
			'status' => 0,
			'id' => '',
			'nama' => '',
		);
		$sqluser = "select id,nama from wooz_gate where places_id = '" . $placesid . "' and id = '" . $id . "'";
		$queryuser = $this->db->query($sqluser);
		$user = $queryuser->row();
		if ($user) {
			$data = array(
				'status' => 1,
				'id' => $user->id,
				'nama' => $user->nama,
			);
		}
		echo json_encode($data);
	}

	function user($placesid, $id = 0) {
		$data = array(
			'status' => 0,
			'data' => array(),
		);

		//$placesid = $this->placesid;
		$sqluser = "select a.id,b.id as landing_id,a.account_displayname,a.account_rfid,b.landing_joindate FROM wooz_account a inner join wooz_landing b on b.account_id = a.id where landing_register_form = '" . $placesid . "' and b.id > '" . $id . "' and landing_from_regis = 2 order by b.id asc";
		$queryuser = $this->db->query($sqluser);
		$user = $queryuser->result();
		if ($user) {
			$data = array(
				'status' => 1,
			);
			$placesdata = $this->list_tabel($placesid);
			foreach ($user as $row) {
				$dataother = array();
				foreach ($placesdata as $value) {
					$dataother[] = array(
						$value->field_nicename => $this->ambil_data_user($row->id, $value->id),
					);
				}
				$data['data'][] = array(
					'id' => $row->id,
					'landingid' => $row->landing_id,
					'nama' => $row->account_displayname,
					'account_rfid' => $row->account_rfid,
					'other' => $dataother,
					'date' => $row->landing_joindate,
				);
			}
		}
		echo json_encode($data);
	}

	function userpost($placesid) {
		$account_id = $this->input->post('account_id');
		$landing_id = $this->input->post('landing_id');

		$in_data['account_displayname'] = $this->input->post('account_displayname', true);
		$in_data['account_username'] = $this->user_m->nicename($this->input->post('account_displayname', true));
		$in_data['account_rfid'] = $this->input->post('account_rfid', true);

		if ($account_id != 0 && $landing_id != 0) {
			$in_data_landing['landing_level'] = '2';
			$this->db->where('id', $account_id);
			$this->db->update('account', $in_data);
		} else {
			$in_data_landing['landing_level'] = '1';
			$in_data['account_status'] = 1;
			$this->db->insert('account', $in_data);
			$account_id = $this->db->insert_id();
		}
		$in_data_landing['account_id'] = $account_id;
		$in_data_landing['landing_register_form'] = $placesid;
		$in_data_landing['landing_from_regis'] = $placesid;
		$in_data_landing['landing_rfid'] = $in_data['account_rfid'];
		$this->db->insert('landing', $in_data_landing);

		$form = $this->list_tabel($placesid);
		foreach ($form as $row) {
			$data_content = $this->input->post($row->field_nicename, true);
			if ($data_content) {
				$in_data_rfid['form_regis_id'] = $row->id;
				$in_data_rfid['account_id'] = $account_id;
				$in_data_rfid['places_id'] = $placesid;
				$in_data_rfid['content'] = $data_content;
				$in_data_rfid['date_add'] = date('Y-m-d H:i:s');
				$rowcontent = $this->user_m->cek_content_data($account_id, $placesid, $row->id);
				if ($rowcontent) {
					$this->db->where('id', $rowcontent->id);
					$this->db->update('form_regis_detail', $in_data_rfid);
				} else {
					$this->db->insert('form_regis_detail', $in_data_rfid);
				}
			}
		}
		$return = 0;
		if ($account_id != 0) {
			$return = $account_id;
		}
		echo $return;
	}

	function checkinpost($placesid) {

		$account_id = $this->input->post('account_id');
		$gate = $this->input->post('places_id');
		$badge_id = $this->input->post('badge_id');
		$log_fb_places = $this->input->post('log_fb_places');
		$time_checkin = $this->input->post('log_stamps');

		$db['account_id'] = $account_id;
		$db['badge_id'] = $badge_id;
		$db['log_fb_places'] = $log_fb_places;
		$db['places_id'] = $placesid;
		$db['log_type'] = 1;
		$db['log_date'] = date('Y-m-d');
		$db['log_time'] = date('H:i:s');
		$db['log_gate'] = 38;
		$db['log_check'] = 1;
		$db['log_hash'] = 'v' . time() . '-' . $account_id;
		$db['log_status'] = 1;
		$db['log_stamps'] = $time_checkin;
		$upd = $this->db->insert('log_user_gate', $db);
		$return = 1;
		echo $return;
	}

	function mandrill($sender, $subject, $content, $attachment, $acc_id) {
		$hasil_email = 0;
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;

		try {
			$this->mandrill->init($this->config->item('mandrill_api_key'));
			$mandrill_ready = TRUE;
		} catch (Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
		}

		if ($mandrill_ready) {
			//Send us some email!
			if ($attachment != '') {
				$image = FCPATH . $attachment;
				$attch = $this->mandrill->getAttachmentStruct($image);
				//Send us some email!
				$email = array(
					'text' => $content, //Consider using a view file
					'subject' => $subject,
					'from_email' => 'donotreply@wooz.in',
					'from_name' => $sender,
					'to' => array(array('email' => $this->ambil_data_user($acc_id, 30))),
					"attachments" => array($attch),
				);
			} else {
				$email = array(
					'text' => $content, //Consider using a view file
					'subject' => $subject,
					'from_email' => 'donotreply@wooz.in',
					'from_name' => $sender,
					'to' => array(array('email' => $this->ambil_data_user($acc_id, 30))),
				);
			}
			$result = $this->mandrill->messages_send($email);
			//$hasil_email = $result[0]['_id'];
			$hasil_email = $result;
		}
		return $hasil_email;
	}

	function list_tabel($id) {
		$sql = "select id,field_name,field_nicename from wooz_form_regis where places_id = '" . $id . "' order by id asc";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		return $hasil;
	}

	function ambil_data_user($acc_id, $form_id) {
		$sql = "select a.account_id,a.content "
			. "from wooz_form_regis_detail a where a.form_regis_id = '" . $form_id . "' and account_id = '" . $acc_id . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$hasil = $data->content;
		} else {
			$hasil = '';
		}
		return $hasil;
	}

	function list_quisioner($id) {
		$list = array(
			'status' => 0,
			'data' => array(),
		);

		$this->load->model('dashboard/quisioner_m');
		$data = $this->quisioner_m->list_visitor($id);
		if ($data) {
			$list['status'] = 1;

			foreach ($data as $row) {
				$listanswer = array();
				$answer = $this->quisioner_m->list_answer($row->id);
				foreach ($answer as $rowanswer) {
					$listanswer[] = array(
						'id' => $rowanswer->id,
						'name' => $rowanswer->answer,
						'status' => $rowanswer->data_status,
					);
				}

				$list['data'][] = array(
					'id' => $row->id,
					'name' => $row->question,
					'survey' => $row->survey_id,
					'answer' => $listanswer,
				);
			}
			$setting = $this->quisioner_m->get_setting_download($id);
			foreach ($setting as $value) {
				$list['setting'][] = array(
					'id' => $value->id,
					'title' => $value->title,
					'order' => $value->question_order,
					'finish' => $value->question_finish,
					'message' => $value->question_message,
				);
			}
		}
		echo json_encode($list);
	}

	function uploadquisioner($placesid) {
		$account_id = $this->input->post('account_id');
		$question = $this->input->post('question');
		$answer = $this->input->post('answer');
		$date_add = $this->input->post('date_add');

		$db['account_id'] = $account_id;
		$db['places_id'] = $placesid;
		$db['question_id'] = $question;
		$db['answer_id'] = $answer;
		$db['data_status'] = 1;
		$db['date_add'] = $date_add;
		$upd = $this->db->insert('quisioner', $db);
		$return = 1;
		echo $return;
	}

}
?>
