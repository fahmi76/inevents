<?php

class Photobooth extends CI_controller {

    protected $album_name = "Mandiri Karnaval Nusantara";

    function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'wooz.in - ';

        $this->load->database();
        $this->load->model('photobooth/photobooth_m');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->model('convert');
        $this->load->helper('url');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $fb_perms = $this->config->item('facebook_perms');

        $this->load->library('sosmed');
        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
            'fileUpload' => true
        ));

        $custom_page = $this->input->get('place', true);
        if ($custom_page != '') {
            $customs = $this->db->get_where('places', array('places_landing' => $custom_page))->row();
            if ($customs) {
                $this->custom_id = $customs->id;
                if ($customs->places_parent != 0) {
                    $this->custom_id_parent = $customs->places_parent;
                } else {
                    $this->custom_id_parent = $customs->id;
                }
                $this->custom_page = $custom_page;
                $this->tpl['customs'] = $custom_page;
                $this->tpl['customs_id'] = $this->custom_id;
                $this->tpl['css'] = $customs->places_css;
                $this->tpl['main_title'] .= $customs->places_name;
                $this->tpl['frame_big'] = 'http://wooz.in' . str_replace('photoframe/', 'photoframe/big_', $customs->places_frame);
                $this->tpl['frame_normal'] = $customs->places_frame;
				
                if ($custom_page == 'nescafeeventphoto') {
                    $this->tpl['card'] = 'Keychain';
                } elseif ($custom_page == 'echelonphoto') {
                    $this->tpl['card'] = 'Brochure';
                } else {
                    $this->tpl['card'] = 'Wristband';
                }

                if ($customs->places_fbid != '0')
                    $this->tpl['fb_page'] = $customs->places_fbid;
                if ($customs->places_twitter != '0')
                    $this->tpl['tw_page'] = $customs->places_twitter;
            } else {
                redirect('http://wooz.in?status=custom_page_not_registered');
            }
        } else {
            redirect('http://wooz.in?status=custom_page_not_found');
        }
    }

    function index() {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->tpl['page'] = "flash";
        $this->tpl['title'] = "Photo Booth";
        $this->tpl['name'] = $this->session->userdata('name');

        #$this->tpl['body'] = $this->load->view('photobooth/default1', $this->tpl, TRUE);
        $this->tpl['body'] = $this->load->view('photobooth/defaultcard', $this->tpl, TRUE);
        $this->load->view('photobooth/home', $this->tpl);
    }

    function setup() {
        $this->load->view('photobooth/setup');
    }

    function result() {
        $user_id = $this->input->get('user_id', true);
        $photos_id = $this->input->get('photos_id', true);
        if (!$user_id) {
            #echo 'user_not_uploaded';
            redirect('photobooth?place=' . $this->custom_page . '&status=user_not_uploaded');
        }
        if (!$photos_id) {
            #echo 'photo_not_uploaded';
            redirect('photobooth?place=' . $this->custom_page . '&status=photo_not_uploaded');
        }

        if (isset($_SESSION['photo_upload_time']) && $_SESSION['photo_upload_time'] != '') {
            $proc_start = $_SESSION['photo_upload_time'];
        } else {
            $proc_start = microtime(true);
        }
        $this->_session();

        $row = $this->db->get_where('account', array('id' => $user_id))->row();
        $photo = $this->db->get_where('photos', array('id' => $photos_id))->row();
        $copytext = $this->db->get_where('places', array('id' => $this->custom_id))->row();

        $image = FCPATH . 'uploads/photobooth/' . $photo->photo_upload;

        $get_session['img_temp'] = $photo->photo_upload;
        $this->session->set_userdata($get_session);
		$photos['twit_id'] = 0;
		$photos['pid'] = 0;
        if ($row->account_tw_token && $row->account_tw_secret) {
            $photos['twit_id'] = $this->sosmed->upload_photo_tw($copytext->places_tw_caption, $image, $row->account_tw_token, $row->account_tw_secret);
        }

        if ($row->account_fbid && $row->account_token) {
            $photos['pid'] = $this->sosmed->upload_photo_fb($copytext, $image, $row->account_fb_name, $row->account_fbid, $row->account_token);
        }
		if($copytext->id == 459){
			$album_page_id = '646018515473100';
			$fanpage_id = '387243381350616';
            $fanpage_token = 'CAACGFg9ewNIBAP1TSbCwmMa2bV0XZCWhw1ZAIJUxbqH3eRoMjKABZBjGAX5G3RXs54rrUw4RsW59jmdz6kGXldrFZB7GRZAc5b0H4wBFLJqrJ4MsKgpUYJoLArLKJWeDp9wamyIpPxZA8VdenjGNxeZBwYTbTqgK8xPCz4UO7hsFzbkLN4RZBxXNltHgNrqSaoUZD';
			$this->sosmed->upload_photo_fb_page($copytext, $image, $fanpage_id, $fanpage_token, $album_page_id);
		}
		
        $photos['code_photo'] = 'p' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $row->id;
        $proc_end = microtime(true);
        $photos['time_processed'] = $proc_end - $proc_start;
        if ($photos['twit_id'] == 0 && $photos['pid'] == 0) {
            $photos['email_id'] = $this->email_send($row->account_email, $copytext->places_tw_caption, $photo->photo_upload);
        }
        $photos['status'] = 1;
        $this->db->where('id', $photos_id);
        $this->db->update('photos', $photos);
		$this->photobooth_m->insertlog($photos_id);

        $os = array(0, 1, 2, 3);
        if (in_array($photos['pid'], $os) && $photos['twit_id'] == 0) {
            redirect('photobooth/phototag/' . $photos_id . '/' . $user_id . '/1?place=' . $this->custom_page, 'refresh');
        }
        redirect('photobooth/phototag/' . $photos_id . '/' . $user_id . '?place=' . $this->custom_page, 'refresh');
    }

    function phototag($photosid, $userid) {
        $this->tpl['title'] = "Photo Tagging";
        $this->tpl['js'] = "tagme.js";
        $this->tpl['js3'] = site_url() . "photobooth/tagresult/$photosid/$userid?place=" . $this->custom_page;

        if (isset($this->uri->segments[5]) && $this->uri->segments[5] != '')
            $this->tpl['hasil'] = $this->uri->segments[5];

        $img_temp = $this->session->userdata('img_temp');
        $get_photos = $this->db->get_where('photos', array('id' => $photosid))->row();

        $id_account = $this->session->userdata('id_user_tag_user');
        $get_name = $this->db->get_where('account', array('id' => $userid))->row();
        $this->tpl['photosid'] = $photosid;
        $this->tpl['userid'] = $userid;
        $this->tpl['fbid'] = $get_name->account_fbid;
        $this->tpl['name'] = $get_name->account_displayname;
        $this->tpl['tag'] = $this->session->userdata('status_rfid');
        $this->tpl['img_temp'] = $get_photos->photo_upload;
        $statustag = $this->input->post('status', true);
        
        if ($id_account) {
            $num = $this->db->get_where('tagging', array('id_photos' => $get_photos->id))->num_rows();
            $numcheck = $this->db->get_where('tagging', array('id_photos' => $get_photos->id, 'account_id' => $id_account))->row();

            /* send tag to facebook */
            $owner = $this->db->get_where('account', array('id' => $id_account))->row();
            $this->tpl['name'] = $owner->account_displayname;
            $xpost = $this->input->post('xpos', true);
            $ypost = $this->input->post('ypos', true);
            $x = ((75 + $xpost) / 640) * 100;
            $y = ((75 + $ypost) / 480) * 100;

            $tag_params = array(
                'access_token' => $get_name->account_token,
                'to' => $owner->account_fbid,
                'x' => $x,
                'y' => $y
            );
            $tagphoto = $this->sosmed->photo_tag_fb($get_photos->pid, $tag_params);
            
                $input = array(
                    'id_photos' => $get_photos->id,
                    'targetX' => $xpost,
                    'targetY' => $ypost,
                    'tagCounter' => $num + 1,
                    'account_id' => $id_account,
                    'status' => $tagphoto
                );

                if($xpost != 0 && $ypost != 0 && !$numcheck){
                    $this->db->insert('tagging', $input);
                }
        }
        $this->tpl['body'] = $this->load->view('photobooth/show', $this->tpl, TRUE);
        $this->load->view('photobooth/home', $this->tpl);
    }

    function tagresult($photosid, $userid) {
        // Defining Content Type to JS File
        header("Content-type: text/javascript");
        // File must not be taken from Cache
        header("Cache-Control:	 no-cache, must-revalidate");
        $this->db->select('');
        $this->db->join('account', 'account.id = tagging.account_id', 'left');
        $this->db->where('tagging.id_photos', $photosid);
        $this->db->order_by('tagging.id', 'desc');
        $this->db->from('tagging');
        $acc = $this->db->get()->result();

        echo "var tags = new Array();\n";
        $i = 0;

        if (count($acc) != 0) {
            foreach ($acc as $data) {
                echo "tags[" . $i . "] = {\"tagid\":" . $data->id . ",\"posx\":" . $data->targetX . ",\"posy\":" . $data->targetY . ",\"tagvalue\":\"" . $data->account_displayname . "\"}\n";
                $i++;
            }
        }
        echo "var totaltags = tags.length;";
    }

    function publish() {
        $this->tpl['page'] = "publish";
        $id_account = $this->session->userdata('id_user');
        $this->db->select('');
        $this->db->from('account');
        $this->db->where('id', $id_account);
        $acc = $this->db->get()->row();

        $photo = $this->session->userdata('img_temp');
        $pid = $this->session->userdata('pid');
        $input = array(
            'account_id' => $id_account,
            'photo_upload' => addslashes($photo),
            'code_photo' => $this->code($photo),
            'status' => 'released'
        );

        if (empty($pid)) {
            $this->db->insert('photos', $input);
            redirect(site_url('photobooth/endphotos'));
        } else {
            redirect(site_url('photobooth/endphotos'));
        }
    }

    function rfidtag($photosid, $userid) {
        /* $this->_session(); */
        $id_account = $userid;
        $rfid = $this->input->post('rfid');

        if (isset($rfid)) {
            $this->db->select('');
            $this->db->from('account');
            $this->db->where('account_rfid', $rfid);
            $acc1 = $this->db->get();

            if ($acc1->num_rows() === 0) {
                $data1['nama'] = "tidak ada";
                $data1['alamat'] = "Please Retry";

                echo json_encode($data1);
            } else {
                $rf = (int) $rfid;
                $this->db->select('');
                $this->db->from('account');
                $this->db->where('account_rfid', $rf);
                $acc = $this->db->get()->row();

                $this->db->select('');
                $this->db->join('photos', 'photos.id = tagging.id_photos', 'left');
                $this->db->where('photos.id', $photosid);
                $this->db->where('tagging.account_id', $acc->id);
                $this->db->from('tagging');
                $dataphoto = $this->db->get();

                if ($dataphoto->num_rows() === 0) {

                    $this->db->select('');
                    $this->db->from('account');
                    $this->db->where('account_rfid', $rfid);
                    $acc = $this->db->get()->row();

                    $photo = $this->session->userdata('img_temp');
                    $codephoto = $this->code($photo);

                    if ($acc->id === $id_account) {
                        $get_session['code_photo'] = $codephoto;
                        $get_session['id_user_tag_user'] = $acc->id;
                        $get_session['status_rfid'] = $acc->account_displayname;
                        $this->session->set_userdata($get_session);
                    } else {
                        $get_session['code_photo'] = $codephoto;
                        $get_session['id_user_tag_user'] = $acc->id;
                        $get_session['status_rfid'] = $acc->account_displayname;
                        $this->session->set_userdata($get_session);
                    }
                    $data1['nama'] = "ada";
                    $data1['alamat'] = $acc->account_displayname;

                    echo json_encode($data1);
                } else {
                    $get_session['id_user'] = $acc->id;
                    $get_session['id_user_tag_user'] = $acc->id;
                    $get_session['status_rfid'] = $acc->account_displayname;
                    $this->session->set_userdata($get_session);

                    $data1['nama'] = "sudah ada";
                    $data1['alamat'] = $acc->account_displayname;

                    echo json_encode($data1);
                }
            }
        } else {
            $data1['nama'] = "ulang";
            $data1['alamat'] = "Please Retry";

            echo json_encode($data1);
        }
    }

    /* callback */

    function check_rfid() {
        $rfid = $this->input->post('rfid');
        $this->db->select('count(account_id) as jumlah,account_id');
        $this->db->where('landing_register_form', $this->custom_id_parent);
        $this->db->where('landing_rfid', $rfid);
        $this->db->order_by("id", "desc");
        $acc = $this->db->get('landing')->row();
        if ($acc && isset($acc->jumlah) && $acc->jumlah == '1') {
            $this->db->select('account_rfid,account_displayname');
            $this->db->where('id', $acc->account_id);
            $acc1 = $this->db->get('account')->row();
            $get_session['id_user'] = $acc->account_id;
            $get_session['rfid'] = $acc1->account_rfid;
            $get_session['name'] = $acc1->account_displayname;
            $this->session->set_userdata($get_session);
            $data1['nama'] = "ada";
            $data1['alamat'] = $acc1->account_displayname;
            $data1['id'] = $acc->account_id;
            echo json_encode($data1);
        } else {
            $this->db->select('count(id) as jumlah, id,account_rfid,account_displayname');
            $this->db->where('account_rfid', $rfid);
            $acc = $this->db->get('account')->row();
            if ($acc && isset($acc->jumlah) && $acc->jumlah == '1') {
                $get_session['id_user'] = $acc->id;
                $get_session['rfid'] = $acc->account_rfid;
                $get_session['name'] = $acc->account_displayname;
                $this->session->set_userdata($get_session);
                $data1['nama'] = "ada";
                $data1['alamat'] = $acc->account_displayname;
                $data1['id'] = $acc->id;
                echo json_encode($data1);
            } else {
                $data1['nama'] = "ulang";
                $data1['alamat'] = "Please Retry";

                echo json_encode($data1);
            }
        }
    }

    /* callback */

    function code($input) {
        $output = base64_encode(md5($input));
        return $output;
    }

    function _session() {
        $rfid = $this->session->userdata('rfid');
        if (!empty($rfid)) {
            return TRUE;
        } else {
            redirect('photobooth/cancel');
        }
    }

    function cancel() {
        $this->tpl['page'] = "cancel";
        $this->tpl['title'] = "Cancel";
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('rfid');
        $this->session->unset_userdata('pid');
        $this->session->unset_userdata('img_temp');
        $this->session->unset_userdata('code_photo');
        $this->session->unset_userdata('status_rfid');
        $this->session->sess_destroy();
        session_destroy();
        $this->session->sess_destroy();
        $this->tpl['body'] = $this->load->view('photobooth/note_cancel', $this->tpl, TRUE);
        $this->load->view('photobooth/home', $this->tpl);
    }

    function thankyou() {
        $this->tpl['page'] = "thankyou";
        $this->tpl['title'] = "Finish";
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('rfid');
        $this->session->unset_userdata('pid');
        $this->session->unset_userdata('img_temp');
        $this->session->unset_userdata('code_photo');
        $this->session->unset_userdata('status_rfid');
        $this->session->sess_destroy();
        session_destroy();
        $this->session->sess_destroy();
        $this->tpl['body'] = $this->load->view('photobooth/note_thank', $this->tpl, TRUE);
        $this->load->view('photobooth/home', $this->tpl);
    }

    function email_send($email, $caption, $file_name) {
        $config['charset'] = 'iso-8859-1';
        $config['protocol'] = 'mail';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = FALSE;

        $this->load->library('email');

        $this->email->initialize($config);
        $this->email->from('donotreply@wooz.in', 'Wooz.In');
        $this->email->to($email);
        $this->email->subject("We're sending you this photo because we're having some difficulty sending it to Facebook. We don't want you to lose this moment, so here it is :)");
        $this->email->message($caption);
        $this->email->attach(FCPATH . 'uploads/photobooth/' . $file_name);
        $this->email->send();
    }

    function __count_badge($user, $venue) {
        $jumlah = 0;
        $chk = $this->db->get_where('places', array('id' => $venue->id, 'places_status' => 1))->row();
        if ($chk->places_parent != '0') {
            $chk = $this->db->get_where('places', array('id' => $chk->places_parent, 'places_status' => 1))->row();
        }
        if (count($chk) != 0) {
            $chk2 = $this->__have_badge($chk->id);
            foreach ($chk2 as $badge) {
                $have = $this->__got_badge($badge->id, $user->id);
                if (!$have) {
                    if ($badge->badge_type == '2') {
                        $this->db->select('distinct(places_id)');
                        $spots = json_decode($badge->badge_spot);
                        $this->db->where('account_id', $user->id);
                        #$this->db->where('log_type', 1);
                        $this->db->where('log_type !=', 2);
                        $this->db->where('log_status', 1);
                        $this->db->where_in('places_id', $spots);
                        $this->db->from('log');
                        $count = $this->db->get()->result();

                        if ($badge->badge_value == count($count)) {
                            $status = true;
                            $unlocked = 1;
                            $jumlah = $jumlah + 1;
                        } else {
                            $status = false;
                        }
                    } else
                        $status = false;
                } else {
                    $status = false;
                }

                if ($status) {
                    switch ($badge->id) {
                        case 39:
                            $venue_id = 406;
                            break;
                        case 40:
                            $venue_id = 406;
                            break;
                        case 41:
                            $venue_id = 406;
                            break;
                        case 42:
                            $venue_id = 406;
                            break;
                    }
                    $venue = $this->db->get_where('places', array('id' => $venue_id, 'places_status' => 1))->row();
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;
                    $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($user->account_fbid && $user->account_token) {

                        $options['access_token'] = $user->account_token;
                        $options['picture'] = base_url() . $venue->places_avatar;
                        #$options['name'] = $venue->places_name;
                        $options['name'] = "Arthur's Day Challenge!";
                        $options['caption'] = 'wooz.in update';
                        $options['description'] = $venue->places_desc;
                        $options['message'] = $venue->places_cstatus_fb;
                        $options['link'] = site_url() . '/woozpot/' . $venue->places_nicename;

                        try {
                            $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                            $hasil = json_encode($url);
                            $id_hasil = json_decode($hasil);
                            $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));
                        } catch (FacebookApiException $e) {
                            error_log($e);
                            $result = $e->getResult();
                            $data['message']['error'] = $result;
                        }
                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {

                        $tmhOAuth = new tmhOAuth(array(
                            'consumer_key' => $this->config->item('twiiter_consumer_key'),
                            'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
                            'user_token' => $user->account_tw_token,
                            'user_secret' => $user->account_tw_secret,
                        ));

                        $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update'), array(
                            'status' => $venue->places_cstatus_tw
                        ));

                        $resp = json_encode($tmhOAuth->response['response']);
                        $resp1 = json_decode($resp);
                        $twit_id = json_decode($resp1);
                        if ($code == 200) {
                            $this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

                            $data['status'] = true;
                        } else {
                            $data['status'] = true;
                        }
                    }
                    return $data['status'];
                }
            }
        }
    }

    function __have_badge($id) {
        $chk = $this->db->get_where('badge', array('places_id' => $id, 'badge_status' => 1))->result();
        return $chk;
    }

    function __got_badge($id, $user) {
        $chk = $this->db->get_where('log', array('badge_id' => $id, 'account_id' => $user, 'log_type' => 2, 'log_status' => 1))->row();
        if (count($chk) === 0)
            return false;
        else
            return true;
    }

}
