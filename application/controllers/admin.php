<?php

class Admin extends CI_controller {

    protected $limit = 10;
    protected $main = 'admin/body';
    protected $method = array('portfolio', 'news', 'career');

    function __construct() {
        parent::__construct();
		$this->output->enable_profiler(FALSE);
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->helper('tinymce');
        $this->load->library('fungsi');
        $this->load->library('session');
        $this->load->library('taggly');
        $this->load->model('convert');
        $this->load->library('Curl');
        $this->load->library('EpiFoursquare');
        $this->load->database();       
    }

    function index() {
        $sesi = $this->session->userdata('idadmin');
        $content['admin'] = $sesi;
        if ($sesi == "") {
            redirect('admin/login');
        } elseif($sesi == "10862"){
            redirect('admin/places');
        } else {
            $content['content'] = $this->load->view('admin/default', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function login() {
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
        $sesi = $this->session->userdata('idadmin');
        $content['admin'] = $sesi;

        #$this->session->sess_destroy();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userlogin', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('passlogin', 'Password', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $data['account_email'] = $this->input->post('userlogin', true);
            $data['account_passwd'] = $this->fungsi->acak($this->input->post('passlogin', true));
            $count = $this->db->get_where('account', $data)->row();
            if ($count) {
                if ($count->account_status == 1 && $count->account_group > 1) {
                    $keyword['idadmin'] = $count->id;
                    $this->session->set_userdata($keyword);
                    redirect('admin');
                }
            }
        }
        $content['content'] = $this->load->view('admin/login', $content, true);
        $this->load->view('admin/body', $content);
    }

    function logout() {
        $content['title'] = "Sign Out";
        $content['view'] = "Sign Out";
        $content['parent'] = 'home/logout';
        $this->session->sess_destroy();
        redirect('admin', 'refresh');
    }

    function places() {
        $sesi = $this->session->userdata('idadmin');
        $content['admin'] = $sesi;
        if ($sesi == "" || $sesi != 1) {
            redirect('admin', 'refresh');
        }

        $content['modules'] = 'places';
        $content['title'] = 'Places/Spot List';

        $param['limit'] = $this->limit;
        $param['offset'] = $this->uri->segment(3, 0);

//find
        $get_find = $this->input->post('find', true);
        if ($param['offset'] == 0) {
            $this->session->unset_userdata('find');
        }

        if ($get_find != "") {
            $find = $get_find;
            $this->session->set_userdata('find', $find);
            $param['offset'] == 0;
        } else {
            $find = $this->session->userdata('find');
        }

        if ($find)
            $param['find'] = $find; else
            $param['find'] = 0;
        $content['find'] = $param['find'];
//find
//order
        $get_ordr = $this->input->post('order', true);
        if ($param['offset'] == 0) {
            $this->session->unset_userdata('order');
        }

        if ($get_ordr != "") {
            $order = $get_ordr;
            $this->session->set_userdata('order', $order);
            $param['offset'] == 0;
        } else {
            $order = $this->session->userdata('order');
        }

        if ($order) {
            $param['order'] = $order;
            $content['order'] = $param['order'];
        } else {
            $param['order'] = 1;
            $content['order'] = $param['order'];
        }
//order
//query
        $userop = $this->db->get_where('account', array('id' => $sesi))->row();
        $content['userop'] = $userop;

        $this->db->select('');

        if ($userop->account_group == 3 && $userop->account_spots_perms != '') {
            $accspots = json_decode($userop->account_spots_perms);
            $this->db->where('id', $accspots[0]);

            $dataspots = array_shift($accspots);
            foreach ($accspots as $aspots) {
                $this->db->or_where('id', $aspots);
            }
        } else {
            $this->db->where('places_parent', 0);
            if($sesi == '10862'){
                $this->db->where('places_status =', '2');
            }else{
               $this->db->where('places_status !=', '0');
            }
        }

        if (isset($param['order']) && $param['order'] != 0) {
            if ($param['order'] == '4')
                $this->db->order_by("id", "asc");
            elseif ($param['order'] == '3')
                $this->db->order_by("places_name", "asc");
            elseif ($param['order'] == '2')
                $this->db->order_by("places_name", "desc");
            else
                $this->db->order_by("id", "desc");
        } else
            $this->db->order_by("id", "desc");

        if ($param['find']) {
            $keyword = $this->fungsi->search_q($param['find']);
            $k = 1;
            foreach ($keyword as $key) {
                if ($k == 1) {
                    $this->db->like('places_name', $key);
                    $this->db->or_like('places_nicename', $key);
                } else {
                    $this->db->or_like('places_name', $key);
                    $this->db->or_like('places_nicename', $key);
                }
                $k++;
            }
        }


        $this->db->limit($param['limit'], $param['offset']);
        $views = $this->db->get('places')->result();

        $this->db->select('');
        $this->db->where('places_parent', 0);
        $this->db->where('places_status !=', '0');
        if ($param['find']) {
            $keyword = $this->fungsi->search_q($param['find']);
            $k = 1;
            foreach ($keyword as $key) {
                if ($k == 1) {
                    $this->db->like('places_name', $key);
                    $this->db->or_like('places_nicename', $key);
                } else {
                    $this->db->or_like('places_name', $key);
                    $this->db->or_like('places_nicename', $key);
                }
                $k++;
            }
        }

        $this->db->from('places');
        $total = $this->db->count_all_results();
//query

        $this->load->library('pagination');
        $config['base_url'] = site_url('/admin/places/');
        $config['total_rows'] = $total;
        $config['per_page'] = $this->limit;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = "</span>";
        $config['next_link'] = 'next &raquo;';
        $config['next_tag_open'] = '<span class="nextprev">';
        $config['next_tag_close'] = "</span>";
        $config['prev_link'] = '&laquo; previous';
        $config['prev_tag_open'] = '<span class="nextprev">';
        $config['prev_tag_close'] = "</span>";

        $this->pagination->initialize($config);

        $content['datalist'] = $views;
        $content['tPaging'] = $this->pagination->create_links();
        $content['content'] = $this->load->view('admin/places-list', $content, true);

        $this->load->view('admin/body', $content);
    }

    function placesadd() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'places';
        $content['admin'] = $sesi;

        $content['title'] = "Data Places";

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $getuser = $this->db->get_where('account', array('id' => $sesi))->row();

        $cstatus_fb = $this->input->post('cstatus_fb', true);
        if ($cstatus_fb != '') {
           $this->form_validation->set_rules('cstatus_fb', 'Custom Status Facebook', 'min_length[5]|max_length[900]');
        }
        $cstatus_tw = $this->input->post('cstatus_tw', true);
        if ($cstatus_tw != '') {
           $this->form_validation->set_rules('cstatus_tw', 'Custom Status Twitter', 'min_length[5]|max_length[140]');
        }
        $cstatus_dm = $this->input->post('cstatus_dm', true);
        if ($cstatus_dm != '') {
           $this->form_validation->set_rules('cstatus_dm', 'Custom Twitter Direct Message', 'min_length[5]|max_length[100]');
        }
		$photo_cstatus_fb = $this->input->post('photo_cstatus_fb', true);
        if ($photo_cstatus_fb != '') {
			$this->form_validation->set_rules('photo_cstatus_fb', 'Photobooth Custom Status Facebook', 'min_length[5]|max_length[440]');
        }
        $photo_cstatus_tw = $this->input->post('photo_cstatus_tw', true);
        if ($photo_cstatus_tw != '') {
			$this->form_validation->set_rules('photo_cstatus_tw', 'Photobooth Custom Status Twitter', 'min_length[5]|max_length[140]');
        }
		$fb_album = $this->input->post('fb_album', true);
		if ($fb_album != '') {
		   $this->form_validation->set_rules('fb_album', 'Photobooth Album Facebook', 'min_length[5]|max_length[100]');
		}
		$spot_type = $this->input->post('type', true);
        if ($spot_type == '0') {
           $this->form_validation->set_rules('landing', 'Spot Landing Page URL', 'trim|required|callback_places_landing_url');
        }

        $this->form_validation->set_rules('name', 'Spot Name', 'trim|required|callback_places_used');
        $this->form_validation->set_rules('parent', 'Spot Parent', 'trim|required');
        $this->form_validation->set_rules('type', 'Spot Type', 'trim|required');
        $this->form_validation->set_rules('address', 'Spot Address', 'trim|required');
        $this->form_validation->set_rules('isi', 'Spot Description', 'trim|required');
        $this->form_validation->set_rules('sdate', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('edate', 'End Date', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data['places_parent'] = $this->input->post('parent', true);
            $data['places_name'] = $this->input->post('name', true);
            $data['places_type'] = $this->input->post('type', true);
            $data['places_nicename'] = $this->fungsi->nicename($data['places_name']);
            $data['places_address'] = $this->input->post('address', true);
            $data['places_startdate'] = date("Y-m-d", strtotime($this->input->post('sdate', true)));
            $data['places_duedate'] = date("Y-m-d", strtotime($this->input->post('edate', true)));
            $data['places_desc'] = $this->input->post('isi', true);
            $data['places_latitude'] = $this->input->post('latitude', true);
            $data['places_longitude'] = $this->input->post('longitude', true);
            $data['places_facebook'] = $this->input->post('facebook', true);
            $data['places_foursquare'] = $this->input->post('foursquare', true);

			$data['places_avatar'] = $this->input->post('icon', true);
			$data['places_landing'] = $this->input->post('landing', true);
			$data['places_css'] = $this->input->post('css', true);

            $data['places_email'] = $this->input->post('emailaddress', true);
            $data['places_subject_email'] = $this->input->post('subjectemail', true);
            $data['places_custom_email'] = $this->input->post('isiemail', true);   

			$data['places_fbid'] = $this->input->post('facebook_page', true);
			$data['places_twitter'] = $this->input->post('twitter_username', true);
			$data['places_frame'] = $this->input->post('frame', true);

            if ($cstatus_fb != '') {
                $data['places_cstatus_fb'] = $this->input->post('cstatus_fb', true);
            }
            if ($cstatus_tw != '') {
                $data['places_cstatus_tw'] = $this->input->post('cstatus_tw', true);
            }
            if ($cstatus_dm != '') {
                $data['places_dm'] = $this->input->post('cstatus_dm', true);
            }
            if ($photo_cstatus_fb != '') {
				$data['places_fb_caption'] = $this->input->post('photo_cstatus_fb', true);
            }
            if ($photo_cstatus_tw != '') {
				$data['places_tw_caption'] = $this->input->post('photo_cstatus_tw', true);
            }
			if ($fb_album != '') {
				$data['places_album'] = $this->input->post('fb_album', true);
			}

            $this->db->insert('places', $data);
            $new = $this->db->insert_id();

            $content['parent'] = 'places';
            $content['content'] = $this->load->view('admin/do', $content, true);
        } else {
            $content['content'] = $this->load->view('admin/places-add', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function placesedit() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'places';
        $content['admin'] = $sesi;

        $content['title'] = "Data Places";
        $id_data = $this->uri->segment(3, 0);

        $user = $this->db->get_where('places', array('id' => $id_data))->row();

        if ($user) {
            $this->load->helper(array('form'));
            $this->load->library('form_validation');

            $getuser = $this->db->get_where('account', array('id' => $sesi))->row();

            $cstatus_fb = $this->input->post('cstatus_fb', true);
            if ($cstatus_fb != '') {
                $this->form_validation->set_rules('cstatus_fb', 'Custom Status Facebook', 'min_length[5]|max_length[900]');
            }
            $cstatus_tw = $this->input->post('cstatus_tw', true);
            if ($cstatus_tw != '') {
                $this->form_validation->set_rules('cstatus_tw', 'Custom Status Twitter', 'min_length[5]|max_length[140]');
            }
			$cstatus_dm = $this->input->post('cstatus_dm', true);
			if ($cstatus_dm != '') {
			   $this->form_validation->set_rules('cstatus_dm', 'Custom Twitter Direct Message', 'min_length[5]|max_length[100]');
			}
            $photo_cstatus_fb = $this->input->post('photo_cstatus_fb', true);
            if ($photo_cstatus_fb != '') {
                $this->form_validation->set_rules('photo_cstatus_fb', 'Photobooth Custom Status Facebook', 'min_length[5]|max_length[440]');
            }
            $photo_cstatus_tw = $this->input->post('photo_cstatus_tw', true);
            if ($photo_cstatus_tw != '') {
                $this->form_validation->set_rules('photo_cstatus_tw', 'Photobooth Custom Status Twitter', 'min_length[5]|max_length[140]');
            }
			$fb_album = $this->input->post('fb_album', true);
			if ($fb_album != '') {
			   $this->form_validation->set_rules('fb_album', 'Photobooth Album Facebook', 'min_length[5]|max_length[100]');
			}
			$spot_type = $this->input->post('type', true);
			if ($spot_type == '0') {
			   $this->form_validation->set_rules('landing', 'Spot Landing Page URL', 'trim|required|callback_places_landing_url');
			}

            $this->form_validation->set_rules('name', 'Spot Name', 'trim|required');
            $this->form_validation->set_rules('parent', 'Spot Parent', 'trim|required');
            $this->form_validation->set_rules('type', 'Spot Type', 'trim|required');
            $this->form_validation->set_rules('address', 'Spot Address', 'trim|required');
            $this->form_validation->set_rules('isi', 'Spot Description', 'trim|required');
            $this->form_validation->set_rules('sdate', 'Start Date', 'trim|required');
            $this->form_validation->set_rules('edate', 'End Date', 'trim|required');

            if ($this->form_validation->run() === true) {
                $data['places_parent'] = $this->input->post('parent', true);
                $data['places_name'] = $this->input->post('name', true);
                $data['places_type'] = $this->input->post('type', true);
                $data['places_nicename'] = $this->fungsi->nicename($data['places_name']);
                $data['places_address'] = $this->input->post('address', true);
                $data['places_startdate'] = date("Y-m-d", strtotime($this->input->post('sdate', true)));
                $data['places_duedate'] = date("Y-m-d", strtotime($this->input->post('edate', true)));
                $data['places_desc'] = $this->input->post('isi', true);
                $data['places_latitude'] = $this->input->post('latitude', true);
                $data['places_longitude'] = $this->input->post('longitude', true);
                $data['places_facebook'] = $this->input->post('facebook', true);
                $data['places_foursquare'] = $this->input->post('foursquare', true);

				$data['places_avatar'] = $this->input->post('icon', true);
				$data['places_landing'] = $this->input->post('landing', true);
				$data['places_css'] = $this->input->post('css', true);

				$data['places_email'] = $this->input->post('emailaddress', true);
				$data['places_subject_email'] = $this->input->post('subjectemail', true);
				$data['places_custom_email'] = $this->input->post('isiemail', true);   

				$data['places_fbid'] = $this->input->post('facebook_page', true);
				$data['places_twitter'] = $this->input->post('twitter_username', true);
				$data['places_frame'] = $this->input->post('frame', true);

                if ($cstatus_fb != '') {
                    $data['places_cstatus_fb'] = $this->input->post('cstatus_fb', true);
                }
                if ($cstatus_tw != '') {
                    $data['places_cstatus_tw'] = $this->input->post('cstatus_tw', true);
                }
				if ($cstatus_dm != '') {
					$data['places_dm'] = $this->input->post('cstatus_dm', true);
				}

                if ($photo_cstatus_fb != '') {
                    $data['places_fb_caption'] = $this->input->post('photo_cstatus_fb', true);
                }
                if ($photo_cstatus_tw != '') {
                    $data['places_tw_caption'] = $this->input->post('photo_cstatus_tw', true);
                }
				if ($fb_album != '') {
					$data['places_album'] = $this->input->post('fb_album', true);
				}

                $this->db->where('id', $id_data);
                $this->db->update('places', $data);

                $content['parent'] = 'places';
                $content['content'] = $this->load->view('admin/do', $content, true);
            } else {
                $content['datalist'] = $user;
                $content['content'] = $this->load->view('admin/places-edit', $content, true);
            }
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function placesdelete() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['admin'] = $sesi;

        $id_data = $this->uri->segment(3, 0);

        $content['title'] = "Hapus Data Places";
        $content['modules'] = 'places';
        $content['parent'] = 'places';

        $this->db->where('id', $id_data);
        $this->db->from('places');
        $count = $this->db->count_all_results();

        if ($count != 0) {
            $data['places_status'] = 0;

            $this->db->where('id', $id_data);
            $this->db->update('places', $data);

            $content['content'] = $this->load->view('admin/do', $content, true);
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function insight() {
        $sesi = $this->session->userdata('idadmin');
        $content['admin'] = $sesi;
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['modules'] = 'insight';
        $content['title'] = 'Insight/Spot List';

        $param['limit'] = $this->limit;
        $param['offset'] = $this->uri->segment(3, 0);

//find
        $get_find = $this->input->post('find', true);
        if ($param['offset'] == 0) {
            $this->session->unset_userdata('find');
        }

        if ($get_find != "") {
            $find = $get_find;
            $this->session->set_userdata('find', $find);
            $param['offset'] == 0;
        } else {
            $find = $this->session->userdata('find');
        }

        if ($find)
            $param['find'] = $find; else
            $param['find'] = 0;
        $content['find'] = $param['find'];
//find
//order
        $get_ordr = $this->input->post('order', true);
        if ($param['offset'] == 0) {
            $this->session->unset_userdata('order');
        }

        if ($get_ordr != "") {
            $order = $get_ordr;
            $this->session->set_userdata('order', $order);
            $param['offset'] == 0;
        } else {
            $order = $this->session->userdata('order');
        }

        if ($order)
            $param['order'] = $order; else
            $param['order'] = 1;
        $content['order'] = $param['order'];
//order
//query
        $this->db->select('');
        $this->db->where('places_parent', 0);
        $this->db->where('places_status !=', '0');

        if (isset($param['order']) && $param['order'] != 0) {
            if ($param['order'] == '4')
                $this->db->order_by("id", "asc");
            elseif ($param['order'] == '3')
                $this->db->order_by("places_name", "asc");
            elseif ($param['order'] == '2')
                $this->db->order_by("places_name", "desc");
            else
                $this->db->order_by("id", "desc");
        } else
            $this->db->order_by("id", "desc");

        if ($param['find']) {
            $keyword = $this->fungsi->search_q($param['find']);
            $k = 1;
            foreach ($keyword as $key) {
                if ($k == 1) {
                    $this->db->like('places_name', $key);
                    $this->db->or_like('places_nicename', $key);
                } else {
                    $this->db->or_like('places_name', $key);
                    $this->db->or_like('places_nicename', $key);
                }
                $k++;
            }
        }

        $this->db->limit($param['limit'], $param['offset']);
        $views = $this->db->get('places')->result();

        $this->db->select('');
        $this->db->where('places_parent', 0);
        $this->db->where('places_status !=', '0');
        if ($param['find']) {
            $keyword = $this->fungsi->search_q($param['find']);
            $k = 1;
            foreach ($keyword as $key) {
                if ($k == 1) {
                    $this->db->like('places_name', $key);
                    $this->db->or_like('places_nicename', $key);
                } else {
                    $this->db->or_like('places_name', $key);
                    $this->db->or_like('places_nicename', $key);
                }
                $k++;
            }
        }

        $this->db->from('places');
        $total = $this->db->count_all_results();
//query

        $this->load->library('pagination');
        $config['base_url'] = site_url('/admin/insight/');
        $config['total_rows'] = $total;
        $config['per_page'] = $this->limit;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = "</span>";
        $config['next_link'] = 'next &raquo;';
        $config['next_tag_open'] = '<span class="nextprev">';
        $config['next_tag_close'] = "</span>";
        $config['prev_link'] = '&laquo; previous';
        $config['prev_tag_open'] = '<span class="nextprev">';
        $config['prev_tag_close'] = "</span>";

        $this->pagination->initialize($config);

        $content['datalist'] = $views;
        $content['tPaging'] = $this->pagination->create_links();
        $content['content'] = $this->load->view('admin/insight-list', $content, true);

        $this->load->view('admin/body', $content);
    }

	function insightviewstat(){
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'insight';
        $content['admin'] = $sesi;

        $content['title'] = "Data Places";
        $event = $this->uri->segments[3];
        $content['event'] = $event;  
		if($this->uri->segment(4, 0)){
			$this->load->view('admin/insight-viewstat-excel', $content);
			$content['content'] = $this->load->view('admin/insight-viewstat-excel', $content, true);
		}else{
			$content['content'] = $this->load->view('admin/insight-viewstat', $content, true);
			$this->load->view('admin/body', $content);
		}  
	}

    function insightview123() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'insight';
        $content['admin'] = $sesi;

        $content['title'] = "Data Places";
        $id_data = $this->uri->segment(3, 0);
		$content['location'] = $id_data;

        $places = $this->db->get_where('places', array('id' => $id_data))->row();

        if ($places) {
            $family = $this->convert->family($places->id);

			$this->db->select('distinct(wooz_photos.account_id), traps.fb_interest,traps.fb_likes,traps.fb_friends');
			$this->db->where_in('photos.places_id', $places->id);
			#$this->db->where('log.log_status', 1);

			//if ($places->places_startdate != '0000-00-00' && $places->places_duedate != '0000-00-00') {
				//	$this->db->where("log.log_stamps between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
			//}

			$this->db->join('traps', 'traps.id_user = photos.account_id', 'left');
			$this->db->order_by('photos.account_id', 'desc');
			$this->db->from('photos');
				
			$traps5 = $this->db->get()->result();
			$content['data'] = $traps5;
			if(empty($traps5))
			{ 
				$this->db->select('distinct(wooz_log.account_id), traps.fb_interest,traps.fb_likes,traps.fb_friends');
				$this->db->where_in('log.places_id', $family);
				$this->db->where('log.log_status', 1);

				if ($places->places_startdate != '0000-00-00' && $places->places_duedate != '0000-00-00') {
					$this->db->where("log.log_stamps between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
				}

				$this->db->join('traps', 'traps.id_user = log.account_id', 'left');
				$this->db->order_by('log.account_id', 'desc');
				$this->db->from('log');
					
				$traps = $this->db->get()->result();
				$content['data'] = $traps;
			}

			#xdebug($content['data']);die;
			#$content['data'] = $traps1;
			#$content['tw'] = file_get_contents("http://wooz.in/api/twitfollow/".$places->id);
			$content['rfid'] = $places->places_rfid;
			$this->db->select('wooz_account.account_gender, wooz_account.account_birthdate, landing.landing_fb_friends, landing.landing_tw_friends');
			$this->db->join('account', 'account.id = landing.account_id', 'left');
			$this->db->where("landing_register_form", $places->id);
			#$this->db->where("landing.landing_joindate between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
			$this->db->from('landing');
			$content['user'] = $this->db->get()->result();
            $content['totaluser'] = count($content['user']);
            $content['action'] = $this->convert->action($places->id);
			if($content['action']['all'] == 0)
			$content['action'] = $this->convert->action1($places->id);
            $content['photos'] = $this->convert->photos($places->id);

            $content['title'] = $places->places_name;
            $content['datalist'] = $places;
            $content['content'] = $this->load->view('admin/insight-view', $content, true);
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function insightview() {
		if($this->uri->segment(4, 0)){
			$content['excel'] = 1;
		}else{
			$content['excel'] = 0;
		}
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'insight';
        $content['admin'] = $sesi;

        $content['title'] = "Data Places";
        $id_data = $this->uri->segment(3, 0);
		$content['location'] = $id_data;

        $places = $this->db->get_where('places', array('id' => $id_data))->row();
		$content['places'] = $places;
        if ($places) {
            $family = $this->convert->family($places->id);
			$content['family'] = $family;
			$this->db->select('distinct(wooz_log.account_id), interest.facebook_page_name,interest_category.category_name');
			$this->db->where_in('log.places_id', $family);
			#$this->db->where('log.log_stamps >=', '2013-05-25 12:02:02');
			$this->db->where("log.log_stamps between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");

			//if ($places->places_startdate != '0000-00-00' && $places->places_duedate != '0000-00-00') {
				//	$this->db->where("log.log_stamps between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
			//}

			$this->db->join('user_interest', 'user_interest.account_id = log.account_id', 'right');
			$this->db->join('interest', 'interest.id = user_interest.interest_id', 'left');
			$this->db->join('interest_category', 'interest_category.id = interest.category_id', 'left');
			$this->db->where('user_interest.status', 1);
			$this->db->order_by('log.account_id', 'desc');
			$this->db->from('log');
					
			$traps3 = $this->db->get()->result();
			$content['datainterest'] = $traps3;

			$this->db->select('count(distinct(wooz_log.account_id)) as total');
			$this->db->where_in('log.places_id', $family);
			$this->db->where('log.log_stamps >=', '2013-05-25 12:02:02');
			$this->db->from('log');
					
			$traps6 = $this->db->get()->row();
			$content['useraktif'] = $traps6->total;

			$this->db->select('account.id, like.facebook_page_name,like_category.category_name');
			
			$this->db->join('user_likes', 'user_likes.account_id = account.id', 'right');
			$this->db->join('like', 'like.id = user_likes.likes_id', 'left');
			$this->db->join('like_category', 'like_category.id = like.category_id', 'left');
			$this->db->where('user_likes.status', 1);
			$this->db->order_by('account.id', 'desc');
			$this->db->from('account');
					
			$traps2 = $this->db->get()->result();
			$content['datalike'] = $traps2;
			
			$content['rfid'] = $places->places_rfid;
			$this->db->select('distinct(wooz_landing.account_id),wooz_account_sosmed.account_gender, wooz_account_sosmed.account_birthdate, landing.landing_fb_friends, landing.landing_tw_friends');
			$this->db->join('account_sosmed', 'account_sosmed.id = landing.account_id', 'left');
			$this->db->where("landing_register_form", $places->id);
			#$this->db->where('landing.landing_joindate >=', '2013-05-25 12:02:02');
			$this->db->where("landing.landing_joindate between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
			$this->db->group_by("account_id"); 
			$this->db->from('landing');
			$content['user'] = $this->db->get()->result();
            $content['totaluser'] = count($content['user']);
            $content['actiontotal'] = $this->convert->actiontotal($places->id);
            $content['action'] = $this->convert->action($places->id);
			$content['action1'] = $this->convert->action1($places->id);
			$sqllike = 'SELECT distinct(account_id) FROM `wooz_landing` where landing_register_form = '.$id_data.' 
						and (landing_fb_like = 1 or landing_fb_like_event = 1) group by account_id';
			$querylike = $this->db->query($sqllike);
			$content['fblike'] = $querylike->result();
			$sqllike = 'SELECT distinct(account_id) FROM `wooz_landing` where landing_register_form = '.$id_data.' 
						and (landing_tw_follow = 1 or landing_tw_follow_event = 1) group by account_id';
			$querylike = $this->db->query($sqllike);
			$content['twfollow'] = $querylike->result();
			if($content['action']['all'] == 0)
				$content['action'] = $this->convert->action1($places->id);
            $content['photos'] = $this->convert->photos($places->id);

            $content['title'] = $places->places_name;
            $content['datalist'] = $places;
            $content['content'] = $this->load->view('admin/insight-viewnew', $content, true);
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function insightview1() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'insight';
        $content['admin'] = $sesi;

        $content['title'] = "Data Places";
        $id_data = $this->uri->segment(3, 0);
		$content['location'] = $id_data;

        $places = $this->db->get_where('places', array('id' => $id_data))->row();

        if ($places) {
            $family = $this->convert->family($places->id);

			$this->db->select('distinct(wooz_photos.account_id), traps.fb_interest,traps.fb_likes,traps.fb_friends');
			$this->db->where_in('photos.places_id', $places->id);
			#$this->db->where('log.log_status', 1);

			if ($places->places_startdate != '0000-00-00' && $places->places_duedate != '0000-00-00') {
				$this->db->where("photos.time_upload between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
			}

			$this->db->join('user_likes', 'traps.id_user = photos.account_id', 'left');
			$this->db->join('likes', 'traps.id_user = photos.account_id', 'left');
			$this->db->order_by('photos.account_id', 'desc');
			$this->db->from('photos');
				
			$traps5 = $this->db->get()->result();
			$content['data'] = $traps5;
			if(empty($traps5))
			{ 
				$this->db->select('distinct(wooz_log.account_id), traps.fb_interest,traps.fb_likes,traps.fb_friends');
				$this->db->where_in('log.places_id', $family);
				$this->db->where('log.log_status', 1);

				if ($places->places_startdate != '0000-00-00' && $places->places_duedate != '0000-00-00') {
					$this->db->where("log.log_stamps between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
				}

				$this->db->join('traps', 'traps.id_user = log.account_id', 'left');
				$this->db->order_by('log.account_id', 'desc');
				$this->db->from('log');
					
				$traps = $this->db->get()->result();
				$content['data'] = $traps;
			}

			#xdebug($content['data']);die;
			#$content['data'] = $traps1;
			#$content['tw'] = file_get_contents("http://wooz.in/api/twitfollow/".$places->id);
			$content['rfid'] = $places->places_rfid;
			$this->db->select('wooz_account.account_gender, wooz_account.account_birthdate, landing.landing_fb_friends, landing.landing_tw_friends');
			$this->db->join('account', 'account.id = landing.account_id', 'left');
			$this->db->where("landing_register_form", $places->id);
			$this->db->where("landing.landing_joindate between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
			$this->db->from('landing');
			$content['user'] = $this->db->get()->result();
            $content['totaluser'] = count($content['user']);
            $content['action'] = $this->convert->action($places->id);
			if($content['action']['all'] == 0)
			$content['action'] = $this->convert->action1($places->id);
            $content['photos'] = $this->convert->photos($places->id);

            $content['title'] = $places->places_name;
            $content['datalist'] = $places;
            $content['content'] = $this->load->view('admin/insight-view', $content, true);
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function pin() {
        $sesi = $this->session->userdata('idadmin');
        $content['admin'] = $sesi;
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $userop = $this->db->get_where('account', array('id' => $sesi))->row();
        $content['userop'] = $userop;

        $content['modules'] = 'pin';
        $content['title'] = 'Pin List';

        $param['limit'] = $this->limit;
        $param['offset'] = $this->uri->segment(3, 0);

//find
        $get_find = $this->input->post('find', true);
        if ($param['offset'] == 0) {
            $this->session->unset_userdata('find');
        }

        if ($get_find != "") {
            $find = $get_find;
            $this->session->set_userdata('find', $find);
            $param['offset'] == 0;
        } else {
            $find = $this->session->userdata('find');
        }

        if ($find)
            $param['find'] = $find; else
            $param['find'] = 0;
        $content['find'] = $param['find'];
//find
//order
        $get_ordr = $this->input->post('order', true);
        if ($param['offset'] == 0) {
            $this->session->unset_userdata('order');
        }

        if ($get_ordr != "") {
            $order = $get_ordr;
            $this->session->set_userdata('order', $order);
            $param['offset'] == 0;
        } else {
            $order = $this->session->userdata('order');
        }

        if ($order)
            $param['order'] = $order; else
            $param['order'] = 1;
        $content['order'] = $param['order'];
//order
//query
        $userop = $this->db->get_where('account', array('id' => $sesi))->row();
        $content['userop'] = $userop;
        $spots_perms = json_decode($userop->account_spots_perms);

        $this->db->select('');
        $this->db->where('badge_status !=', 0);
        if ($userop->account_group == 3) {
            foreach ($spots_perms as $key => $sptperms) {
                if ($key == 0) {
                    $this->db->where('places_id', $sptperms);
                } else {
                    $this->db->or_where('places_id', $sptperms);
                }
            }
        }

        if (isset($param['order']) && $param['order'] != 0) {
            if ($param['order'] == '4')
                $this->db->order_by("id", "asc");
            elseif ($param['order'] == '3')
                $this->db->order_by("badge_name", "asc");
            elseif ($param['order'] == '2')
                $this->db->order_by("badge_name", "desc");
            else
                $this->db->order_by("id", "desc");
        } else
            $this->db->order_by("id", "desc");

        if ($param['find']) {
            $keyword = $this->fungsi->search_q($param['find']);
            $k = 1;
            foreach ($keyword as $key) {
                if ($k == 1) {
                    $this->db->like('badge_name', $key);
                } else {
                    $this->db->or_like('badge_name', $key);
                }
                $k++;
            }
        }

        $this->db->limit($param['limit'], $param['offset']);
        $views = $this->db->get('badge')->result();

        $this->db->select('');
        $this->db->where('badge_status !=', '0');
        if ($param['find']) {
            $keyword = $this->fungsi->search_q($param['find']);
            $k = 1;
            foreach ($keyword as $key) {
                if ($k == 1) {
                    $this->db->like('badge_name', $key);
                } else {
                    $this->db->or_like('badge_name', $key);
                }
                $k++;
            }
        }

        $this->db->from('badge');
        $total = $this->db->count_all_results();
//query

        $this->load->library('pagination');
        $config['base_url'] = site_url('/admin/pin/');
        $config['total_rows'] = $total;
        $config['per_page'] = $this->limit;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = "</span>";
        $config['next_link'] = 'next &raquo;';
        $config['next_tag_open'] = '<span class="nextprev">';
        $config['next_tag_close'] = "</span>";
        $config['prev_link'] = '&laquo; previous';
        $config['prev_tag_open'] = '<span class="nextprev">';
        $config['prev_tag_close'] = "</span>";

        $this->pagination->initialize($config);

        $content['datalist'] = $views;
        $content['tPaging'] = $this->pagination->create_links();
        $content['content'] = $this->load->view('admin/badge-list', $content, true);

        $this->load->view('admin/body', $content);
    }

    function pinadd() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'pin';
        $content['admin'] = $sesi;

        $content['title'] = "Data Pin";

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Pin Name', 'trim|required');
        $this->form_validation->set_rules('places', 'Pin Spot', 'trim|required');
        $this->form_validation->set_rules('type', 'Pin Type', 'trim|required');
        $this->form_validation->set_rules('isi', 'Pin Description', 'trim|required');

        if ($this->form_validation->run() === true) {
            if (isset($_FILES['icon'])) {
                $j = count($_FILES['icon']['name']);

                if ($j != 0) {
                    $file1 = $_FILES['icon'];
                    $name1 = $_FILES['icon']['name'];
                    $nama1 = $this->fungsi->nicename(date("ymdhis-") . $this->fungsi->fnc_get_name($name1));
                    $exe1 = $this->fungsi->fnc_get_extensi($name1);
                    $upl1 = $this->fungsi->upload($file1, 'badge/' . $nama1, $exe1);
                    if ($upl1) {
                        $data['badge_avatar'] = $nama1 . '.' . $exe1;
                    }
                }
            }

            $data['places_id'] = $this->input->post('places', true);
            $data['badge_type'] = $this->input->post('type', true);
            $data['badge_name'] = $this->input->post('name', true);
            $data['badge_value'] = $this->input->post('value', true);
            $data['badge_desc'] = $this->input->post('isi', true);
            $data['badge_spot'] = json_encode($this->input->post('spots', true));

            $this->db->insert('badge', $data);
            $new = $this->db->insert_id();

            $content['parent'] = 'pin';
            $content['content'] = $this->load->view('admin/do', $content, true);
        } else {
            $content['content'] = $this->load->view('admin/badge-add', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function pinedit() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'pin';
        $content['admin'] = $sesi;

        $content['title'] = "Data Badge";
        $id_data = $this->uri->segment(3, 0);

        $user = $this->db->get_where('badge', array('id' => $id_data))->row();
        if ($user) {
            $this->load->helper(array('form'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Pin Name', 'trim|required');
            $this->form_validation->set_rules('places', 'Pin Spot', 'trim|required');
            $this->form_validation->set_rules('type', 'Pin Type', 'trim|required');
            $this->form_validation->set_rules('isi', 'Pin Description', 'trim|required');

            if ($this->form_validation->run() === true) {
                if (isset($_FILES['icon'])) {
                    $j = count($_FILES['icon']['name']);

                    if ($j != 0) {
                        $file1 = $_FILES['icon'];
                        $name1 = $_FILES['icon']['name'];
                        $nama1 = $this->fungsi->nicename(date("ymdhis-") . $this->fungsi->fnc_get_name($name1));
                        $exe1 = $this->fungsi->fnc_get_extensi($name1);
                        $upl1 = $this->fungsi->upload($file1, 'badge/' . $nama1, $exe1);
                        if ($upl1) {
                            $data['badge_avatar'] = $nama1 . '.' . $exe1;
                        }
                    }
                }

                $data['places_id'] = $this->input->post('places', true);
                $data['badge_type'] = $this->input->post('type', true);
                $data['badge_name'] = $this->input->post('name', true);
                $data['badge_value'] = $this->input->post('value', true);
                $data['badge_desc'] = $this->input->post('isi', true);
                $data['badge_spot'] = json_encode($this->input->post('spots', true));

                $this->db->where('id', $id_data);
                $this->db->update('badge', $data);

                $content['parent'] = 'pin';
                $content['content'] = $this->load->view('admin/do', $content, true);
            } else {
                $content['datalist'] = $user;
                $content['content'] = $this->load->view('admin/badge-edit', $content, true);
            }
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

	function pinunlocked() {
        $sesi = $this->session->userdata('idadmin');
        $content['admin'] = $sesi;
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $userop = $this->db->get_where('account', array('id' => $sesi))->row();
        $content['userop'] = $userop;

        $content['modules'] = 'pin';
        $content['title'] = 'Pin List';

        $param['limit'] = $this->limit;
        $param['offset'] = $this->uri->segment(3, 0);

//find
        $get_find = $this->input->post('find', true);
        if ($param['offset'] == 0) {
            $this->session->unset_userdata('find');
        }

        if ($get_find != "") {
            $find = $get_find;
            $this->session->set_userdata('find', $find);
            $param['offset'] == 0;
        } else {
            $find = $this->session->userdata('find');
        }

        if ($find)
            $param['find'] = $find; else
            $param['find'] = 0;
        $content['find'] = $param['find'];
//find
        $id_data = $this->uri->segment(3, 0);
        $badge = $this->db->get_where('log', array('badge_id' => $id_data))->result();
        foreach ($badge as $id){
            $user = $this->db->get_where('account', array('id' => $id->account_id))->row();
            $use[]=$user;
        }
        
        $this->load->library('pagination');
        $config['base_url'] = site_url('/admin/pin/');
        $config['total_rows'] = $total;
        $config['per_page'] = $this->limit;
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = "</span>";
        $config['next_link'] = 'next &raquo;';
        $config['next_tag_open'] = '<span class="nextprev">';
        $config['next_tag_close'] = "</span>";
        $config['prev_link'] = '&laquo; previous';
        $config['prev_tag_open'] = '<span class="nextprev">';
        $config['prev_tag_close'] = "</span>";

        $this->pagination->initialize($config);

        $content['datalist'] = $use;
        $content['tPaging'] = $this->pagination->create_links();
        $content['content'] = $this->load->view('admin/unlocked-pin', $content, true);

        $this->load->view('admin/body', $content);
    }
	
    function pindelete() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['admin'] = $sesi;

        $id_data = $this->uri->segment(3, 0);

        $content['title'] = "Hapus Data Pin";
        $content['modules'] = 'pin';
        $content['parent'] = 'pin';

        $this->db->where('id', $id_data);
        $this->db->from('badge');
        $count = $this->db->count_all_results();

        if ($count != 0) {
            $data['badge_status'] = 0;

            $this->db->where('id', $id_data);
            $this->db->update('badge', $data);

            $content['content'] = $this->load->view('admin/do', $content, true);
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function user() {
        $sesi = $this->session->userdata('idadmin');
        $content['admin'] = $sesi;
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $level = $this->db->get_where('account', array('id' => $sesi))->row();
        if ($level->account_group == 9) {

            $content['modules'] = 'user';
            $content['title'] = 'User List';

            $param['limit'] = $this->limit;
            $param['offset'] = $this->uri->segment(3, 0);

//find
            $get_find = $this->input->post('find', true);
            if ($param['offset'] == 0) {
                $this->session->unset_userdata('find');
            }

            if ($get_find != "") {
                $find = $get_find;
                $this->session->set_userdata('find', $find);
                $param['offset'] == 0;
            } else {
                $find = $this->session->userdata('find');
            }

            if ($find)
                $param['find'] = $find; else
                $param['find'] = 0;
            $content['find'] = $param['find'];
//find
//order
            $get_ordr = $this->input->post('order', true);
            if ($param['offset'] == 0) {
                $this->session->unset_userdata('order');
            }

            if ($get_ordr != "") {
                $order = $get_ordr;
                $this->session->set_userdata('order', $order);
                $param['offset'] == 0;
            } else {
                $order = $this->session->userdata('order');
            }

            if ($order)
                $param['order'] = $order; else
                $param['order'] = 1;
            $content['order'] = $param['order'];
//order
//status
            $get_stat = $this->input->post('status', true);
            if ($param['offset'] == 0) {
                $this->session->unset_userdata('status');
            }

            if ($get_stat != "") {
                $status = $get_stat;
                $this->session->set_userdata('status', $order);
                $param['offset'] == 0;
            } else {
                $status = $this->session->userdata('status');
            }

            if ($status)
                $param['status'] = $status; else
                $param['status'] = 4;
            $content['status'] = $param['status'];
//status
//query
            if (isset($param['order']) && $param['order'] != 0) {
                if ($param['order'] == '5' || $param['order'] == '6') {
                    $this->db->select('*,(SELECT COUNT(*) FROM wooz_log WHERE log_status=1 AND log_type=1 AND wooz_log.account_id=wooz_account.id) as spot');
                } elseif ($param['order'] == '7' || $param['order'] == '8') {
                    $this->db->select('*,(SELECT COUNT(*) FROM wooz_log WHERE log_status=1 AND log_type=2 AND wooz_log.account_id=wooz_account.id) as pin');
                } else {
                    $this->db->select('');
                }
            } else {
                $this->db->select('');
            }
            $this->db->where('account_status !=', '0');

            if (isset($param['order']) && $param['order'] != 0) {
                if ($param['order'] == '5')
                    $this->db->order_by("spot", "desc");
                elseif ($param['order'] == '6')
                    $this->db->order_by("spot", "asc");
                elseif ($param['order'] == '7')
                    $this->db->order_by("pin", "desc");
                elseif ($param['order'] == '8')
                    $this->db->order_by("pin", "asc");
                elseif ($param['order'] == '4')
                    $this->db->order_by("id", "asc");
                elseif ($param['order'] == '3')
                    $this->db->order_by("account_displayname", "asc");
                elseif ($param['order'] == '2')
                    $this->db->order_by("account_displayname", "desc");
                else
                    $this->db->order_by("id", "desc");
            } else
                $this->db->order_by("id", "desc");

            if (isset($param['status']) && $param['status'] != 0) {
                if ($param['status'] == '3')
                    $this->db->where('account_group', 0);
                if ($param['status'] == '2')
                    $this->db->where('account_status !=', 1);
                if ($param['status'] == '1')
                    $this->db->where('account_status', 1);
                else
                    $this->db->where('account_status !=', 0);
            } else
                $this->db->where('account_status !=', 0);

            if ($param['find']) {
                $keyword = $this->fungsi->search_q($param['find']);
                $k = 1;
                foreach ($keyword as $key) {
                    if ($k == 1) {
                        $this->db->like('account_username', $key);
                        $this->db->or_like('account_displayname', $key);
                    } else {
                        $this->db->or_like('account_username', $key);
                        $this->db->or_like('account_displayname', $key);
                    }
                    $k++;
                }
            }

            $this->db->limit($param['limit'], $param['offset']);
            $views = $this->db->get('account')->result();

            $this->db->select('');
            $this->db->where('account_status !=', '0');
            if ($param['find']) {
                $keyword = $this->fungsi->search_q($param['find']);
                $k = 1;
                foreach ($keyword as $key) {
                    if ($k == 1) {
                        $this->db->like('account_username', $key);
                        $this->db->or_like('account_displayname', $key);
                    } else {
                        $this->db->or_like('account_username', $key);
                        $this->db->or_like('account_displayname', $key);
                    }
                    $k++;
                }
            }

            if (isset($param['status']) && $param['status'] != 0) {
                if ($param['status'] == '3')
                    $this->db->where('account_group', 0);
                if ($param['status'] == '2')
                    $this->db->where('account_status !=', 1);
                if ($param['status'] == '1')
                    $this->db->where('account_status', 1);
                else
                    $this->db->where('account_status !=', 0);
            } else
                $this->db->where('account_status !=', 0);

            $this->db->from('account');
            $total = $this->db->count_all_results();
//query

            $this->load->library('pagination');
            $config['base_url'] = site_url('/admin/user/');
            $config['total_rows'] = $total;
            $config['per_page'] = $this->limit;
            $config['cur_tag_open'] = '<span class="current">';
            $config['cur_tag_close'] = "</span>";
            $config['next_link'] = 'next &raquo;';
            $config['next_tag_open'] = '<span class="nextprev">';
            $config['next_tag_close'] = "</span>";
            $config['prev_link'] = '&laquo; previous';
            $config['prev_tag_open'] = '<span class="nextprev">';
            $config['prev_tag_close'] = "</span>";

            $this->pagination->initialize($config);

            $content['datalist'] = $views;
            $content['tPaging'] = $this->pagination->create_links();
            $content['content'] = $this->load->view('admin/user-list', $content, true);
        } else {
            $content['parent'] = "";
            $content['error'] = "Anda tidak diperbolehkan mengakses halaman ini";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function usergroup() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'user';
        $content['admin'] = $sesi;

        $content['title'] = "Ubah Hak Akses";
        $id_data = $this->uri->segment(3, 0);
        $user = $this->db->get_where('account', array('id' => $id_data))->row();
        if ($user) {
            $this->load->helper(array('form'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('id_group', 'id_group', 'required');
            if ($this->form_validation->run() === true) {
                $data['account_group'] = $this->input->post('id_group', true);

                if ($this->input->post('id_group') == 3 && $this->input->post('spots') != '') {
                    $data['account_spots_perms'] = json_encode($this->input->post('spots'));
                }
                $this->db->where('id', $id_data);
                $this->db->update('account', $data);

                $content['parent'] = 'user';
                $content['content'] = $this->load->view('admin/do', $content, true);
            } else {
                $content['datalist'] = $user;
                $content['content'] = $this->load->view('admin/user-group', $content, true);
            }
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function useradd() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'user';
        $content['admin'] = $sesi;

        $content['title'] = "Data User";

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required|callback_username_used');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password1', 'Password', 'required');
        $this->form_validation->set_rules('password2', 'Re-type Password', 'required|matches[password1]');

        if ($this->form_validation->run() === true) {
            $data['username'] = $this->input->post('username', true);
            $data['email'] = $this->input->post('email', true);
            $data['isi'] = $this->input->post('isi', true);
            $disp = $this->input->post('displayname', true);
            if ($disp != "") {
                $data['displayname'] = $this->input->post('displayname', true);
            } else {
                $data['displayname'] = $this->input->post('username', true);
            }
            $data['nicename'] = $this->convert->nicename($data['displayname'], 'user');
            $data['passwrd'] = $this->fungsi->acak($this->input->post('password1', true));

            $this->db->insert('m_user', $data);
            $new = $this->db->insert_id();

            $content['parent'] = 'user';
            $content['content'] = $this->load->view('admin/do', $content, true);
        } else {
            $content['content'] = $this->load->view('admin/user-add', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function useredit() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['modules'] = 'user';
        $content['admin'] = $sesi;

        $content['title'] = "Data User";
        $id_data = $sesi;
        $user = $this->convert->convert_id($id_data, 'user');
        if ($user) {
            $this->load->helper(array('form'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('displayname', 'Display Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

            $pass = $this->input->post('password1', true);
            if ($pass != "") {
                $this->form_validation->set_rules('password1', 'password', 'required');
                $this->form_validation->set_rules('password2', 're-type password', 'required|matches[password1]');
            }

            if ($this->form_validation->run() === true) {
                $data['displayname'] = $this->input->post('displayname', true);
                $data['email'] = $this->input->post('email', true);
                $data['isi'] = $this->input->post('isi', true);
                $data['nicename'] = $this->convert->nicename($this->input->post('displayname', true), 'user');
                if ($pass != "") {
                    $data['passwrd'] = $this->fungsi->acak($this->input->post('password1', true));
                }

                $this->db->where('id_user', $id_data);
                $this->db->update('m_user', $data);

                $content['parent'] = 'useredit';
                $content['content'] = $this->load->view('admin/do', $content, true);
            } else {
                $content['datalist'] = $user;
                $content['content'] = $this->load->view('admin/user-edit', $content, true);
            }
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

    function userdelete() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $content['admin'] = $sesi;

        $id_data = $this->uri->segment(3, 0);

        $content['title'] = "Hapus data user";
        $content['modules'] = 'user';
        $content['parent'] = 'user';

        $this->db->where('id', $id_data);
        $this->db->from('account');
        $count = $this->db->count_all_results();

        if ($count != 0) {
            $data['account_status'] = 0;

            $this->db->where('id', $id_data);
            $this->db->update('account', $data);

            $content['content'] = $this->load->view('admin/do', $content, true);
        } else {
            $content['error'] = "Data tidak ditemukan";
            $content['content'] = $this->load->view('admin/notify', $content, true);
        }
        $this->load->view('admin/body', $content);
    }

	function listreg($id){
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $this->load->model('statuser_db');

        $content['title'] = "List User";
        $content['modules'] = 'list';
        $content['parent'] = 'userlist';
        $content['admin'] = $sesi;
			
        $sql = "select b.account_displayname,b.account_email,b.account_fbid,b.account_tw_username,b.account_phone
				from wooz_landing a inner join wooz_account b on b.id = a.account_id where a.landing_register_form = '".$id."'
				GROUP BY a.`account_id`";
        $query = $this->db->query($sql);
        $content['data'] = $query->result();

        $content['content'] = $this->load->view('admin/list_registrasi', $content, true);
        $this->load->view('admin/body', $content);
	
	}
	
	function loguser($id){
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $this->load->model('statuser_db');

        $content['title'] = "List User";
        $content['modules'] = 'list';
        $content['parent'] = 'userlist';
        $content['admin'] = $sesi;
		$family = $this->convert->familyparent($id);		
		$arr_family = implode($family,",");
		$total = count($family);
        $sql = "SELECT a.account_id,b.account_displayname,b.account_email,b.account_fbid,b.account_tw_username,
				b.account_phone,count(distinct(a.places_id)) as total
				FROM `wooz_log` a inner join wooz_account b on b.id = a.account_id inner join wooz_landing c on c.account_id = a.account_id
				where c.landing_register_form = ".$id." and a.places_id in (".$arr_family.") group by a.account_id having total = ".$total;
		
		$query = $this->db->query($sql);
        $content['data'] = $query->result();
		
        $content['content'] = $this->load->view('admin/log_user', $content, true);
        $this->load->view('admin/body', $content);
	}
	
    function userstat() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $this->load->model('statuser_db');

        $content['title'] = "Stat User";
        $content['modules'] = 'stat';
        $content['parent'] = 'userstat';
        $content['admin'] = $sesi;

        $param['limit'] = 7;
        $param['offset'] = $this->uri->segment(3, 0);

        $monthyear = $this->statuser_db->get_monthyear();
        foreach ($monthyear->result() as $myear) {
            $content['monthyear'][] = $myear;
        }

        $content['content'] = $this->load->view('admin/user-stat', $content, true);
        $this->load->view('admin/body', $content);
    }

    function userstatdate() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $this->load->model('statuser_db');

        $content['title'] = "Stat User";
        $content['modules'] = 'stat';
        $content['parent'] = 'userstat';
        $content['admin'] = $sesi;

        $urlmonth = $this->uri->segment(3, 0);
        $result = ucfirst(str_replace("-", " ", trim($urlmonth)));

        $fulldate = $this->statuser_db->get_fulldate($result);
        foreach ($fulldate->result() as $fdate) {
            $content['monthyear'][] = $fdate;
        }
        $content['x'] = $result;
        $this->load->view('admin/user-month', $content);
        //$content['content'] = $this->load->view('admin/user-month',$content,true);
        //$this->load->view('admin/body',$content);
    }

    function userjoindate() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }
        $this->load->model('statuser_db');

        $content['title'] = "Stat User";
        $content['modules'] = 'stat';
        $content['parent'] = 'userstat';
        $content['admin'] = $sesi;

        $param['limit'] = $this->limit;
        $param['offset'] = $this->uri->segment(3, 0);

        $startdate = $this->input->post('startdate', TRUE);
        $duedate = $this->input->post('duedate', TRUE);

        if ($startdate != '' && $duedate != '') {
            $this->session->unset_userdata('sdate');
            $this->session->unset_userdata('ddate');
            $sdate = date("Y-m-d", strtotime($startdate));
            $ddate = date("Y-m-d", strtotime($duedate));
            $this->session->set_userdata('sdate', $sdate);
            $this->session->set_userdata('ddate', $ddate);
            $param['offset'] == 0;
        } else {
            if ($param['offset'] == 0 && $startdate == '' && $duedate == '') {
                $this->session->unset_userdata('sdate');
                $this->session->unset_userdata('ddate');
            } else {
                $sdate = $this->session->userdata('sdate');
                $ddate = $this->session->userdata('ddate');
            }
        }

        $total = $this->statuser_db->num_joindate($sdate, $ddate);
        $content['total'] = $total;

        $this->load->library('pagination');
        $config['base_url'] = site_url('/admin/userjoindate/');
        $config['total_rows'] = $total;
        $config['per_page'] = $param['limit'];
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = "</span>";
        $config['next_link'] = 'next &raquo;';
        $config['next_tag_open'] = '<span class="nextprev">';
        $config['next_tag_close'] = "</span>";
        $config['prev_link'] = '&laquo; previous';
        $config['prev_tag_open'] = '<span class="nextprev">';
        $config['prev_tag_close'] = "</span>";

        $this->pagination->initialize($config);
        $content['tPaging'] = $this->pagination->create_links();

        $joindate = $this->statuser_db->get_joindate($sdate, $ddate, $param);
        foreach ($joindate->result() as $jdate) {
            $content['joindate'][] = $jdate;
        }

        $content['content'] = $this->load->view('admin/user-join', $content, true);
        $this->load->view('admin/body', $content);
    }

    function banner() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['title'] = "Banner List";
        $content['modules'] = 'banner';
        $content['parent'] = 'banner';
        $content['admin'] = $sesi;

        $param['limit'] = $this->limit;
        $param['offset'] = $this->uri->segment(3, 0);

        $this->db->select('');
        $this->db->from('banner');
        $this->db->where('banner_status', 0);
        $this->db->order_by('id', 'DESC');
        $total = $this->db->count_all_results();
        $content['num'] = $total;

        $this->db->select('');
        $this->db->order_by('id', 'DESC');
        $this->db->where('banner_status', 0);
        $this->db->limit($param['limit'], $param['offset']);
        $gbanner = $this->db->get('banner')->result();

        $this->load->library('pagination');
        $config['base_url'] = site_url('/admin/banner/');
        $config['total_rows'] = $total;
        $config['per_page'] = $param['limit'];
        $config['cur_tag_open'] = '<span class="current">';
        $config['cur_tag_close'] = "</span>";
        $config['next_link'] = 'next &raquo;';
        $config['next_tag_open'] = '<span class="nextprev">';
        $config['next_tag_close'] = "</span>";
        $config['prev_link'] = '&laquo; previous';
        $config['prev_tag_open'] = '<span class="nextprev">';
        $config['prev_tag_close'] = "</span>";

        $this->pagination->initialize($config);
        $content['tPaging'] = $this->pagination->create_links();

        foreach ($gbanner as $gxbanner) {
            $content['databanner'][] = $gxbanner;
        }

        $content['content'] = $this->load->view('admin/banner-list', $content, true);
        $this->load->view('admin/body', $content);
    }

    function bannerevent() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['title'] = "Banner List";
        $content['modules'] = 'banner';
        $content['parent'] = 'banner';
        $content['admin'] = $sesi;

        $this->db->select('');
        $this->db->from('banner');
        $this->db->where('banner_status', 1);
        $this->db->where('banner_cat', 'event');
        $this->db->order_by('id', 'DESC');
        $gbanner = $this->db->get();
        $content['num'] = $gbanner->num_rows();

        foreach ($gbanner->result() as $gxbanner) {
            $content['databanner'][] = $gxbanner;
        }

        $this->load->view('admin/banner-event', $content);
    }

    function bannerother() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['title'] = "Banner List";
        $content['modules'] = 'banner';
        $content['parent'] = 'banner';
        $content['admin'] = $sesi;

        $this->db->select('');
        $this->db->from('banner');
        $this->db->where('banner_status', 1);
        $this->db->where('banner_cat', 'others');
        $this->db->order_by('id', 'DESC');
        $gbanner = $this->db->get();
        $content['num'] = $gbanner->num_rows();

        foreach ($gbanner->result() as $gxbanner) {
            $content['databanner'][] = $gxbanner;
        }

        $this->load->view('admin/banner-other', $content);
    }

    function banneradd() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['title'] = "Add New Banner";
        $content['modules'] = 'banner';
        $content['parent'] = 'banner';
        $content['admin'] = $sesi;

        $cfg['upload_path'] = './uploads/banner';
        $cfg['allowed_types'] = 'gif|jpg|png|bmp';
        $this->load->library('upload', $cfg);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('bannertitle', 'Banner Title', 'trim|required');
        $this->form_validation->set_rules('bannercat', 'Banner Category', 'trim|required');
        $this->form_validation->set_rules('bannerurl', 'Banner URL', 'trim|required|callback_valid_url');
        //$this->form_validation->set_rules('bannerimg', 'Banner Image', 'callback_chk_image');

        if ($this->form_validation->run() === true) {
            if (isset($_FILES['bannerimg'])) {
                if ($_FILES['bannerimg']['name'] != '') {
                    $pict = 'bannerimg';
                    $this->upload->do_upload($pict);
                    $banner_upload = $this->upload->data();

                    $new_pic = "wooz-" . uniqid() . ".png";

                    $config['image_library'] = "gd2";
                    $config['source_image'] = "./uploads/banner/" . $banner_upload['file_name'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = FALSE;

                    if ($this->input->post('bannercat') == "event") {
                        $config['width'] = 576;
                        $config['height'] = 351;
                    } else {
                        $config['width'] = 264;
                        $config['height'] = 144;
                    }

                    $config['new_image'] = "./uploads/banner/" . $new_pic;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                }

                $datestring = "%Y-%m-%d %h:%i:%s";
                $time = time();

                $insert['banner_title'] = addslashes($this->input->post('bannertitle'));
                $insert['banner_cat'] = addslashes($this->input->post('bannercat'));
                $insert['banner_url'] = addslashes($this->input->post('bannerurl'));
                $insert['upload_date'] = mdate($datestring, $time);
                $insert['banner_status'] = 0;
                $insert['banner_image'] = substr_replace($new_pic, '_thumb.png', -4);
                unlink($config['source_image']);
                $this->db->insert('banner', $insert);
            }

            $content['content'] = $this->load->view('admin/do', $content, true);
        } else {
            $content['imgerror'] = '';
            $content['content'] = $this->load->view('admin/banner-add', $content, true);
        }

        $this->load->view('admin/body', $content);
    }

    function banneredit() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $id = $this->uri->segment(3, 0);
        $getbanner = $this->db->get_where('banner', array('id' => $id))->row();
        $content['banner'] = $getbanner;

        $content['title'] = "Edit Banner";
        $content['modules'] = 'banner';
        $content['parent'] = 'banner';
        $content['admin'] = $sesi;

        $this->load->library('form_validation');

        $cfg['upload_path'] = './uploads/banner';
        $cfg['allowed_types'] = 'gif|jpg|png|bmp';
        $this->load->library('upload', $cfg);

        $this->form_validation->set_rules('bannertitle', 'Banner Title', 'trim|required');
        $this->form_validation->set_rules('bannercat', 'Banner Category', 'trim|required');
        $this->form_validation->set_rules('bannerurl', 'Banner URL', 'trim|required|callback_valid_url');
        //$this->form_validation->set_rules('bannerimg', 'Banner Image', 'required|callback_chk_image');

        if ($this->form_validation->run() === true) {
            if (isset($_FILES['bannerimg'])) {
                $insert['banner_title'] = addslashes($this->input->post('bannertitle'));
                $insert['banner_cat'] = addslashes($this->input->post('bannercat'));
                $insert['banner_url'] = addslashes($this->input->post('bannerurl'));

                $datestring = "%Y-%m-%d %h:%i:%s";
                $time = time();
                $insert['change_date'] = mdate($datestring, $time);

                if ($_FILES['bannerimg']['name'] != '') {
                    $pict = 'bannerimg';
                    $this->upload->do_upload($pict);
                    $banner_upload = $this->upload->data();

                    $new_pic = "wooz-" . uniqid() . ".png";

                    $config['image_library'] = "gd2";
                    $config['source_image'] = "./uploads/banner/" . $banner_upload['file_name'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = FALSE;
                    if ($this->input->post('bannercat') == "event") {
                        $config['width'] = 576;
                        $config['height'] = 351;
                    } else {
                        $config['width'] = 264;
                        $config['height'] = 144;
                    }
                    $config['new_image'] = "./uploads/banner/" . $new_pic;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $insert['banner_image'] = substr_replace($new_pic, '_thumb.png', -4);
                    unlink($config['source_image']);
                }
                $this->db->where('id', $id);
                $this->db->update('banner', $insert);
            }
            $content['content'] = $this->load->view('admin/do', $content, true);
        } else {
            $content['content'] = $this->load->view('admin/banner-edit', $content, true);
        }

        $this->load->view('admin/body', $content);
    }

    function bannerdel() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['title'] = "Delete Banner";
        $content['modules'] = 'banner';
        $content['parent'] = 'banner';
        $content['admin'] = $sesi;

        $id = $this->uri->segment(3, 0);
        $getbanner = $this->db->get_where('banner', array('id' => $id))->row();
        $file = "./uploads/banner/" . $getbanner->banner_image;

        $this->db->where('id', $id);
        $this->db->delete('banner');
        unlink($file);

        $content['content'] = $this->load->view('admin/do', $content, true);
        $this->load->view('admin/body', $content);
    }

    function banneractive() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['title'] = "Activate Banner";
        $content['modules'] = 'banner';
        $content['parent'] = 'banner';
        $content['admin'] = $sesi;

        $id = $this->uri->segment(3, 0);
        $getbanner = $this->db->get_where('banner', array('id' => $id))->row();

        if ($getbanner->banner_cat == 'event') {
            $this->db->select('');
            $this->db->from('banner');
            $this->db->where('banner_status', 1);
            $this->db->where('id !=', $id);
            $this->db->where('banner_cat', 'event');
            $getactive = $this->db->get()->row();

            if (count($getactive) != 0) {
                $idactive = $getactive->id;
                $old['banner_status'] = 0;
                $this->db->where('id', $idactive);
                $this->db->update('banner', $old);
            }

            $new['banner_status'] = 1;
            $this->db->where('id', $id);
            $this->db->update('banner', $new);
        } else {
            $this->db->select('');
            $this->db->from('banner');
            $this->db->where('banner_status', 1);
            $this->db->where('id !=', $id);
            $this->db->where('banner_cat', 'others');
            $this->db->order_by('id', 'ASC');
            $getactive = $this->db->get()->row();

            $this->db->select('');
            $this->db->from('banner');
            $this->db->where('banner_status', 1);
            $this->db->where('id !=', $id);
            $this->db->where('banner_cat', 'others');
            $this->db->order_by('id', 'ASC');
            $numactive = $this->db->count_all_results();

            if ($numactive == 4) {
                $idactive = $getactive->id;
                $old['banner_status'] = 0;
                $this->db->where('id', $idactive);
                $this->db->update('banner', $old);
            }

            $new['banner_status'] = 1;
            $this->db->where('id', $id);
            $this->db->update('banner', $new);
        }
        redirect('admin/banner');
    }

    function setting() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['title'] = "Information";
        $content['modules'] = 'site information';
        $content['parent'] = 'setting';
        $content['admin'] = $sesi;

        $this->db->select('');
        $this->db->from('setting');
        $this->db->order_by('id', 'desc');
        $content['setting'] = $this->db->get()->row();

        $content['content'] = $this->load->view('admin/setting', $content, true);
        $this->load->view('admin/body', $content);
    }

    function settingedit() {
        $sesi = $this->session->userdata('idadmin');
        if ($sesi == "") {
            redirect('admin', 'refresh');
        }

        $content['modules'] = 'site information';
        $content['admin'] = $sesi;

        $this->db->select('');
        $this->db->from('setting');
        $this->db->where('id', 1);
        $content['setting'] = $this->db->get()->row();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('site_name', 'Site Name', 'trim|required');
        $this->form_validation->set_rules('site_description', 'Site Description', 'trim|required');
        $this->form_validation->set_rules('site_url', 'Site URL', 'trim|required');
        if ($this->form_validation->run() === true) {
            $db['site_name'] = $this->input->post('site_name');
            $db['site_description'] = trim($this->input->post('site_description'));
            $db['site_url'] = $this->input->post('site_url');
            $db['site_mobile_url'] = $this->input->post('site_mobile_url');
            $db['site_status'] = trim($this->input->post('site_status'));
            $db['facebook_id'] = $this->input->post('facebook_id');
            $db['facebook_key'] = $this->input->post('facebook_key');
            $db['facebook_secret'] = $this->input->post('facebook_secret');
            $db['facebook_perms'] = $this->input->post('facebook_perms');
            $db['facebook_callback'] = $this->input->post('facebook_callback');
            $db['twitter_key'] = $this->input->post('twitter_key');
            $db['twitter_secret'] = $this->input->post('twitter_secret');
            $db['twitter_callback'] = $this->input->post('twitter_callback');
            $db['foursquare_key'] = $this->input->post('foursquare_key');
            $db['foursquare_secret'] = $this->input->post('foursquare_secret');
            $db['foursquare_callback'] = $this->input->post('foursquare_callback');
            $db['gowalla_key'] = $this->input->post('gowalla_key');
            $db['gowalla_secret'] = $this->input->post('gowalla_secret');
            $db['gowalla_callback'] = $this->input->post('gowalla_callback');

            $this->db->select('');
            $this->db->from('setting');
            $this->db->where('id', 1);
            $count = $this->db->get()->num_rows();

            if ($count == 0) {
                $this->db->insert('setting', $db);
            } else {
                $this->db->where('id', 1);
                $this->db->update('setting', $db);
            }
        }

        $this->load->view('admin/setting-edit', $content);
    }

    function username_used($str) {
        $this->db->where('account_username', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('account')->row();
        if ($new->jumlah != 0) {
            $this->form_validation->set_message('username_used', 'Username Already Used');
            return false;
        } else {
            return true;
        }
    }

    function places_used($str) {
        $this->db->where('places_name', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('places')->row();
        if ($new->jumlah != 0) {
            $this->form_validation->set_message('places_used', 'Spot Name Already Used');
            return false;
        } else {
            return true;
        }
    }

    function places_landing_url($str) {
        $this->db->where('places_landing', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('places')->row();
        if ($new->jumlah != 0) {
            $this->form_validation->set_message('places_landing_url', 'Spot Landing Page URL Already Used');
            return false;
        } else {
            return true;
        }
    }

	function boothn_used($str) {
        $this->db->where('booth_name', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('booth')->row();
        if ($new->jumlah != 0) {
            $this->form_validation->set_message('boothn_used', 'Booth Name Already Used');
            return false;
        } else {
            return true;
        }
    }

    function booths_used($str) {
        $this->db->where('booth_serial', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('booth')->row();
        if ($new->jumlah != 0) {
            $this->form_validation->set_message('booths_used', 'Booth Serial Already Registered');
            return false;
        } else {
            return true;
        }
    }

    function valid_url() {
        $url = $this->input->post('bannerurl');
        $pattern = '/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i';
        if (!preg_match($pattern, $url) === TRUE) {
            $this->form_validation->set_message('bannerurl', 'Banner URL is Incorrect');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function chk_image() {
        if (!$this->upload->do_upload()) {
            $this->form_validation->set_message('bannerimg', 'xx');
            return false;
        } else {
            return true;
        }
    }

	function demograph() {
            $this->db->select('account_birthdate');
            $this->db->where('account_register_form', 39);
            $this->db->from('account');
            $content = $this->db->get()->result();

            $age = array('40plus' => 0, '30-40' => 0, '26-30' => 0, '21-25' => 0, 'under' => 0, 'unk' => 0);

			foreach($content as $row) {
				$a = $row->account_birthdate;
				if ($this->fungsi->age($a) > 39)
					$age['40plus']++;
				elseif ($this->fungsi->age($a) > 30)
					$age['30-40']++;
				elseif ($this->fungsi->age($a) > 25)
					$age['26-30']++;
				elseif ($this->fungsi->age($a) > 20)
					$age['21-25']++;
				elseif ($this->fungsi->age($a) < 21)
					$age['under']++;
			}
?>
				<h3 class="ins">Age Range</h3>
                <p>
					Age 40 ++ : <?php echo $age['40plus'];?>
                <br />Age 31 - 40 : <?php echo $age['30-40'];?>
                <br />Age 26 - 30 : <?php echo $age['26-30'];?>
                <br />Age 21 - 25 : <?php echo $age['21-25'];?>
                <br />Under 21 : <?php echo $age['under'];?>
                <br />Unknown : <?php echo $age['unk'];?>
                </p>

<?php
	}

	function impression() {
		$app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');

        $this->load->library('facebook',array(
                'appId'  => $app_id,
                'secret' => $secret_key,
                'cookie' => true,
				'fileUpload' => true
        ));

		$row = $this->db->get_where('account', array('id' => 3))->row();

		$response = $this->facebook->api(array(
			'access_token'=> $row->account_token,
			'method' => 'fql.query',
			'query' => 'SELECT uid2 FROM friend WHERE uid1 = 100001539173207'
		));
		xDebug($response);	
	}
	
}

