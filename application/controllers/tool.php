<?php

class Tool extends CI_controller {

    protected $content;

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('fungsi');
        $this->load->model('convert');
        $this->load->helper('date');
    }

    function index() {
        $this->content['title'] = "User Management";

        $aidi = $this->session->userdata('aidi');
        if ($aidi == "") {
            redirect('tool/login');
        } else {
            redirect('tool/main');
        }

        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function login() {
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
        $this->content['title'] = "User Management";

        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email');
        $this->form_validation->set_rules('pass', 'Password', 'trim|required|callback_pass');

        if ($this->form_validation->run() === TRUE) {
            $db['account_email'] = $this->input->post('email', true);
            $db['account_passwd'] = $this->fungsi->acak($this->input->post('pass', true));

            $count = $this->db->get_where('account', $db)->row();
            if ($count->account_status == 1 && $count->account_group == 9) {
                $keyword['aidi'] = $count->id;
                $this->session->set_userdata($keyword);
                redirect('tool/main');
            } else {
                redirect('tool/logout');
            }
        }

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzlogin', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function main() {
        $this->content['title'] = "User Management";
        $aidi = $this->session->userdata('aidi');
        if ($aidi == '') {
            redirect('tool/logout');
        }

        $useracc = $this->db->get_where('account', array('id' => $aidi))->row();
        $this->content['name'] = $useracc->account_displayname;

        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status', 1);
        $this->db->limit(5);
        $this->db->order_by('account_lastupdate', 'DESC');
        $this->content['lsacc'] = $this->db->get()->result();

        $this->db->select('');
        $this->db->from('account');
        $this->db->where('account_status', 0);
        $this->db->limit(5);
        $this->db->order_by('account_lastupdate', 'DESC');
        $this->content['lsrem'] = $this->db->get()->result();

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzmain', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function search() {
        $this->content['title'] = "User Result";
        $aidi = $this->session->userdata('aidi');
        if ($aidi == '') {
            redirect('tool/logout');
        }

        $useracc = $this->db->get_where('account', array('id' => $aidi))->row();
        $this->content['name'] = $useracc->account_displayname;

        $this->load->library('form_validation');

        $id = $this->session->userdata('aidi');
        $countacc = $this->db->get_where('account', array('id' => $id))->row();
        $this->content['woozer'] = $countacc;

        if ($this->input->post('sname') != '') {
            $this->form_validation->set_rules('sname', 'Full Name', 'trim|required');
        } elseif ($this->input->post('semail') != '') {
            $this->form_validation->set_rules('semail', 'Email', 'trim|required');
        } elseif ($this->input->post('srfid1') != '') {
            $this->form_validation->set_rules('srfid1', 'RFID', 'trim|required');
        } else {
            $this->form_validation->set_rules('srfid', 'RFID', 'trim|required');
        }

        if ($this->form_validation->run() === TRUE) {
            $sname = $this->input->post('sname');
            $semail = $this->input->post('semail');
            #$srfid = (int) $this->input->post('srfid');
			$srfid = $this->input->post('srfid');
			$srfid1 = $this->input->post('srfid1');

            if ($sname != '') {
                $this->db->select('');
                $this->db->from('account');
                $this->db->where('account_status', 1);
                $this->db->like('account_displayname', $sname);
                $lsname = $this->db->get()->result();

                $this->content['result'] = $lsname;
            } elseif ($semail != '') {
                $this->db->select('');
                $this->db->from('account');
                $this->db->where('account_status', 1);
                $this->db->like('account_email', $semail);
                $lemail = $this->db->get()->result();

                $this->content['result'] = $lemail;
            } elseif ($srfid1 != '') {
                $this->db->select('landing.id,account.account_displayname,account.account_email, places.places_name');
                $this->db->from('landing');
				$this->db->join('account', 'account.id = landing.account_id', 'left');
				$this->db->join('places', 'places.id = landing.landing_register_form', 'left');
                $this->db->where('landing_rfid', $srfid1);
                $lrfid1 = $this->db->get()->result();

                $this->content['result'] = $lrfid1;
				$this->content['result1'] = 'landing';
            }else {
                $this->db->select('');
                $this->db->from('account');
                $this->db->where('account_status', 1);
                $this->db->like('account_rfid', $srfid);
                $lrfid = $this->db->get()->result();

                $this->content['result'] = $lrfid;
            }
        }

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzresult', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function edit() {
        $this->content['title'] = "User Edit";
        $aidi = $this->session->userdata('aidi');
        if ($aidi == '') {
            redirect('tool/logout');
        }

        $useracc = $this->db->get_where('account', array('id' => $aidi))->row();
        $this->content['name'] = $useracc->account_displayname;

        $id = $this->uri->segment(3, 0);

        $countacc = $this->db->get_where('account', array('id' => $id))->row();
        $this->content['ewoozer'] = $countacc;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('ename', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email');
        $npass = $this->input->post('npass', true);
        $rpass = $this->input->post('rpass', true);

        if ($npass != "" || $rpass != '') {
            $this->form_validation->set_rules('npass', 'New Password', 'trim|required|min_length[5]|max_length[15]|alpha_numeric');
            $this->form_validation->set_rules('rpass', 'Re-Type Password', 'trim|required|matches[npass]');
        }

        if ($this->form_validation->run() === TRUE) {
            if ($npass != "" || $rpass != '') {
                $db['account_passwd'] = $this->fungsi->acak($this->input->post('npass', true));
            }

            if (isset($_FILES['avatar'])) {
                $j = count($_FILES['avatar']['name']);

                if ($j != '0') {
                    $file1 = $_FILES['avatar'];
                    $name1 = $_FILES['avatar']['name'];
                    $nama1 = $this->fungsi->nicename(date("ymdhis-") . $this->fungsi->fnc_get_name($name1));
                    $exe1 = $this->fungsi->fnc_get_extensi($name1);
                    $upl1 = $this->fungsi->thumb($file1, $nama1, $exe1);
                    if ($upl1) {
                        $db['account_avatar'] = $nama1 . '.' . $exe1;
                    }
                }
            }

            $db['account_displayname'] = $this->input->post('ename', true);
            $db['account_rfid'] = $this->input->post('erfid', true);
            $db['account_birthdate'] = $this->input->post('ebirth', true);
            $db['account_url'] = $this->input->post('eurl', true);
            $db['account_location'] = $this->input->post('elocation', true);
            $db['account_profile'] = trim($this->input->post('profil', true));
            $db['account_username'] = $this->convert->nicename($this->input->post('ename', true));
            $db['account_group'] = $this->input->post('id_group', true);
            $db['account_fbid'] = $this->input->post('fbuid', true);
            $db['account_token'] = $this->input->post('fbtoken', true);
            $db['account_tw_token'] = $this->input->post('twtoken', true);
            $db['account_tw_secret'] = $this->input->post('twsecret', true);
            $db['account_fs_token'] = $this->input->post('fstoken', true);

            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $db['account_lastupdate'] = mdate($datestring, $time);

            $this->db->where('id', $id);
            $this->db->update('account', $db);

            $this->content['notice'] = "User account has been successfully saved.";
        } else {
            $this->content['notice'] = "";
        }

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzedit', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function editlanding() {
        $this->content['title'] = "User Edit";
        $aidi = $this->session->userdata('aidi');
        if ($aidi == '') {
            redirect('tool/logout');
        }

        $useracc = $this->db->get_where('account', array('id' => $aidi))->row();
        $this->content['name'] = $useracc->account_displayname;

        $id = $this->uri->segment(3, 0);
		
        $this->db->select('landing.id,landing.landing_rfid,account.account_displayname,account.account_email, places.places_name');
        $this->db->from('landing');
		$this->db->join('account', 'account.id = landing.account_id', 'left');
		$this->db->join('places', 'places.id = landing.landing_register_form', 'left');
        $this->db->where('landing.id', $id);
        $countacc = $this->db->get()->row();

        $this->content['ewoozer'] = $countacc;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('ename', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email');

        if ($this->form_validation->run() === TRUE) {
            $db['landing_rfid'] = $this->input->post('erfid', true);

            $this->db->where('id', $id);
            $this->db->update('landing', $db);

            $this->content['notice'] = "User account has been successfully saved.";
        } else {
            $this->content['notice'] = "";
        }

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzeditlanding', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function remove() {
        $this->content['title'] = "User Edit";
        $aidi = $this->session->userdata('aidi');
        if ($aidi == '') {
            redirect('tool/logout');
        }

        $useracc = $this->db->get_where('account', array('id' => $aidi))->row();
        $this->content['name'] = $useracc->account_displayname;

        $id = $this->uri->segment(3, 0);

        $countacc = $this->db->get_where('account', array('id' => $id))->row();
        $this->content['dwoozer'] = $countacc;

        if (isset($_GET['removed']) && $_GET['removed'] == 1) {
            $this->db->where('id', $id);
            $this->db->delete('account');

            $this->db->where('id_user', $id);
            $this->db->delete('traps');

            $this->db->where('account_id', $id);
            $this->db->delete('log');
        }

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzremove', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function removelanding() {
        $this->content['title'] = "User Edit";
        $aidi = $this->session->userdata('aidi');
        if ($aidi == '') {
            redirect('tool/logout');
        }

        $useracc = $this->db->get_where('account', array('id' => $aidi))->row();
        $this->content['name'] = $useracc->account_displayname;

        $id = $this->uri->segment(3, 0);

        $this->db->select('landing.id,landing.landing_rfid,account.account_displayname,account.account_email, places.places_name');
        $this->db->from('landing');
		$this->db->join('account', 'account.id = landing.account_id', 'left');
		$this->db->join('places', 'places.id = landing.landing_register_form', 'left');
        $this->db->where('landing.id', $id);
        $countacc = $this->db->get()->row();
        $this->content['dwoozer'] = $countacc;

        if (isset($_GET['removed']) && $_GET['removed'] == 1) {
            $this->db->where('id', $id);
            $this->db->delete('landing');
        }

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzremovelanding', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function logout() {
        $this->session->unset_userdata('aidi');
        $this->session->sess_destroy();
        redirect('tool/login', 'refesh');
    }

    function code() {
        $this->content['title'] = "Registration Code";

        $this->load->library('form_validation');
        $this->form_validation->set_rules('code', 'Registration Code', 'trim|required|callback_thecode');
        if ($this->form_validation->run() === TRUE) {
            $code = $this->input->post('code');
            $getacc = $this->db->get_where('registration_code', array('fbpages_code' => $code))->row();
            $infoacc = $this->db->get_where('account', array('id' => $getacc->account_id))->row();

            $this->session->set_userdata('infouser', $infoacc->id);
            redirect('tool/rfidcode');
        }

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzcode', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function rfidcode() {
        $this->content['title'] = "User Information";

        $user = $this->session->userdata('infouser');
        $infouser = $this->db->get_where('account', array('id' => $user))->row();

        $this->content['chkavatar'] = preg_match('(http://)', $infouser->account_avatar);
        $this->content['avatar'] = $infouser->account_avatar;
        $this->content['name'] = $infouser->account_displayname;
        $this->content['location'] = $infouser->account_location;
        $this->content['email'] = $infouser->account_email;
        $this->content['rfid'] = $infouser->account_rfid;

        $this->load->library('form_validation');
        if (isset($_POST['rfidcode'])) {
            $coderfid['account_rfid'] = $_POST['rfidcode'];

            if ($_POST['rfidcode'] != '') {
                $this->db->where('id', $infouser->id);
                $this->db->update('account', $coderfid);
            }
            $this->content['msgcode'] = 1;
        } else {
            $this->content['msgcode'] = '';
        }

        $this->content['wooztoolz'] = $this->load->view('woozer/woozertoolzrfidcode', $this->content, TRUE);
        $this->load->view('woozer/woozertoolz', $this->content);
    }

    function thecode() {
        $str = $this->input->post('code');
        $new = $this->db->get_where('registration_code', array('fbpages_code' => $str))->row();
        if (count($new) == 0) {
            $this->form_validation->set_message('thecode', 'A registration code is not found. Please register first.');
            return false;
        } else {
            return true;
        }
    }

    function email() {
        $str = $this->input->post('email');
        $new = $this->db->get_where('account', array('account_email' => $str))->row();
        if (count($new) == 0) {
            $this->form_validation->set_message('email', 'Email Not Found.');
            return false;
        } else {
            return true;
        }
    }

    function pass() {
        $str = $this->fungsi->acak($this->input->post('pass', true));
        $new = $this->db->get_where('account', array('account_passwd' => $str))->row();
        if (count($new) == 0) {
            $this->form_validation->set_message('pass', 'Password Wrong.');
            return false;
        } else {
            return true;
        }
    }

}
