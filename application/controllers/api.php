<?php

class api extends CI_controller {
    
    protected $tpl;
    
    function api() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        $this->load->library('curl');
        $this->load->library('fungsi');
		$this->load->library('tweet');
        $this->load->model('convert');
        //$this->load->library('model');
		//$this->output->enable_profiler(TRUE);
        $setting = $this->db->get_where('setting', array('id' => 1))->row();

        $app_id = $setting->facebook_id;
        $secret_key = $setting->facebook_secret;
        $api_key = $setting->facebook_key;
        $fb_perms = $setting->facebook_perms;
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));
    }
    
    function index() {
        if (isset($_REQUEST['card'])&&$_REQUEST['card']!='') {
            $cserial = (int) $_REQUEST['card'];
            if ($cserial) {
                $user = $this->db->get_where('account', array('account_rfid LIKE ' => '%' . $cserial, 'account_status' => 1))->row();
                if ($user) {
                    $message = array('rfid' => $card, 'nama' => $user->account_displayname, 'email' => $user->account_email, 'gender' => $user->account_gender, 'birthday' => $user->account_birthdate);
                    $data = array(
                            'status' => 1,
                            'message' => $message
                    );
                    echo json_encode($data);
                } else {
                    $data = array(
                            'status' => 0,
                            'message' => 'user not registered'
                    );
                    echo json_encode($data);
                }
            } else {
                $data = array(
                        'status' => 0,
                        'message' => 'card serial not received'
                );
                echo json_encode($data);
            }
        }
    }

    function receiver() {
			$cserial = (int) strtolower($_GET['cardID']);
			$bserial = strtolower($_GET['readerID']);
            if ($cserial && $bserial) {
				$db['card_id'] = $cserial;
				$db['reader_id'] = $bserial;
				$db['status'] = 1;
				$db['url'] = $_SERVER['REQUEST_URI'];
                $upd = $this->db->insert('receiver', $db);
                $new = $this->db->insert_id(); 
				$data = array(
                        'status' => 1,
                        'message' => 'Hiksa elek'
                );
                echo json_encode($data);              
            } else {
				$db['card_id'] = $cserial;
				$db['reader_id'] = $bserial;
				$db['status'] = 0;
				$db['url'] = $_SERVER['REQUEST_URI'];
                $upd = $this->db->insert('receiver', $db);
                $new = $this->db->insert_id();
                $data = array(
                        'status' => 0,
                        'message' => 'card serial not received'
                );
                echo json_encode($data);
            }
    }

    function token() {
        if (isset($_REQUEST['card'])&&$_REQUEST['card']!='') {
            $cserial = $_REQUEST['card'];
			if ($cserial) {
                $user = $this->db->get_where('account', array('account_rfid like' => '%'.$cserial.'%'))->row();
				if ($user) {
                    $message = array('id' => $user->id, 'name' => $user->account_displayname, 'fbid' => $user->account_fbid, 'fb_token' => $user->account_token, 'tw_token' => $user->account_tw_token, 'tw_secret' => $user->account_tw_secret);
                    $data = array(
                            'status' => true,
                            'message' => $message
                    );
                    echo json_encode($data);
                } else {
                    $data = array(
                            'status' => false,
                            'message' => 'user not registered'
                    );
                    echo json_encode($data);
                }
            } else {
                $data = array(
                        'status' => false,
                        'message' => 'card serial not received'
                );
                echo json_encode($data);
            }
        }
    }
 
    function tokennew() {
        if (isset($_REQUEST['card'])&&$_REQUEST['card']!='') {
            $cserial = $_REQUEST['card'];
			if ($cserial) {
                $user = $this->db->get_where('account', array('id' => $cserial))->row();
				if ($user) {
                    $message = array('fbid' => $user->account_fbid, 'fb_token' => $user->account_token);
                    $data = array(
                            'fbid' => $user->account_fbid, 'fb_token' => $user->account_token
                    );
                    echo json_encode($data);
                } else {
                    $data = array(
                            'status' => 0,
                            'message' => 'user not registered'
                    );
                    echo json_encode($data);
                }
            } else {
                $data = array(
                        'status' => 0,
                        'message' => 'card serial not received'
                );
                echo json_encode($data);
            }
        }
    }

    function userrfid() {
        if (isset($_REQUEST['card'])&&$_REQUEST['card']!='') {
            $cserial = $_REQUEST['card'];
            if ($cserial) {
                $user = $this->db->get_where('account', array('account_rfid' => $cserial, 'account_status' => 1))->row();
                if ($user) {
                    echo '1';
                } else {
                    echo '0';
                }
            } else {
                echo '0';
            }
        } else {
			echo '0';
		}
    }
 
	function photo_from_local() {
		if(isset($_REQUEST['namafile']) && $_REQUEST['namafile']!='') {
			$img = $_REQUEST['namafile'];
			$full = '/home/woozin/public_html/uploads/photobooth/'.$img;
			if(is_file($full)) {
				echo '1';
			} else {
				echo '0';
			}
		} else {
			echo '0';
		}
	}

    function code($input) {
        $output = base64_encode(md5($input));
        return $output;
    }

    function idbytexml() {
        echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $this->db->select('distinct(account_id), account_displayname');
        $this->db->where('log_status', 1);
        $this->db->where_in('places_id', array('37','38'));
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $recent = $this->db->get()->result();
        ?>
<member><?php foreach($recent as $row) { ?><user><name><?php echo $row->account_displayname;?></name></user><?php } ?></member>
        <?php
    }

    function clearxml() {
        echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $this->db->select('distinct(account_id), account_displayname');
        $this->db->where('log_status', 1);
        $this->db->where_in('places_id', array('98'));
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $recent = $this->db->get()->result();
        ?>
<member><?php foreach($recent as $row) { ?><user><name><?php echo $row->account_displayname;?></name></user><?php } ?></member>
        <?php
    }

    function gadxml() {
        echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";
		?>
<gallery>
     <settings>
          <buttons>
               <showSlideshowButton>1</showSlideshowButton>
               <showFacebookButton>1</showFacebookButton>
               <showTwitterButton>1</showTwitterButton>
               <showFullscreenButton>1</showFullscreenButton>
               <showImageNavigationButtons>1</showImageNavigationButtons>
          </buttons>
          <background>
               <bgColor>16777215</bgColor>
               <transparentBG>1</transparentBG>
          </background>
          <translation>
               <fullscreenMode>Fullscreen mode</fullscreenMode>
               <clickToView>Click to view</clickToView>
               <startSlideshow>Start slideshow</startSlideshow>
               <stopSlideshow>Stop slideshow</stopSlideshow>
          </translation>
          <caption>
               <showCaption>1</showCaption>
          </caption>
          <image>
               <bgTintColor>0</bgTintColor>
               <scaleMode>fit</scaleMode>
               <padding>50</padding>
          </image>
          <thumbnail>
               <alpha>70</alpha>
               <width>50</width>
               <height>30</height>
          </thumbnail>
          <slideshow>
               <start>0</start>
               <delay>2.5</delay>
          </slideshow>
     </settings>
     <galleryName>Aura Gallery</galleryName>
		<?php
        $this->db->select('photo_upload');
        $this->db->where('status', 1);
        $this->db->where_in('places_id', array('222'));
        $this->db->order_by('id', 'desc');
		$this->db->limit(10);
        $this->db->from('photos');
        $recent = $this->db->get()->result();
		#xdebug($recent);die;
        ?>
		<items>
		<?php foreach($recent as $row) { ?>
			<item>
               <thumb><img src="http://wooz.in/uploads/photobooth/<?php echo $row->photo_upload;?>"/></thumb>
               <source><img src="http://wooz.in/uploads/photobooth/<?php echo $row->photo_upload;?>"/></source>
			</item><?php } ?>
		  </items>
		</gallery>
        <?php
    }

    function magnumxml() {
        echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $this->db->select('distinct(account_id), account_tw_username,account_displayname');
        $this->db->where('landing_register_form', 360);
        #$this->db->where('account_twid !=', 0);
		#$this->db->where_in('account_id', array('24809','24853','24813','24815','24852','24863','24866'));
        $this->db->where('account_tw_username !=', '');
        $this->db->join('account', 'account.id = landing.account_id', 'left');
        #$this->db->order_by('log.id', 'desc');
        $this->db->from('landing');
        $recent = $this->db->get()->result();
        ?>
<names><?php foreach($recent as $row) { ?><nama><?php echo $row->account_tw_username;?> - <?php echo $row->account_displayname;?></nama><?php } ?></names>
        <?php
    }

    function magnumwinxml() {
        echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $this->db->select('distinct(account_id), account_tw_username');
        $this->db->where('landing_register_form', 208);
        #$this->db->where('account_twid !=', 0);
		$this->db->where_in('account_id', array('24809','24853','24813','24815','24852','24863','24866'));
        $this->db->where('account_tw_username !=', '');
        $this->db->join('account', 'account.id = landing.account_id', 'left');
        #$this->db->order_by('log.id', 'desc');
        $this->db->from('landing');
        $recent = $this->db->get()->result();
        ?>
<names><?php foreach($recent as $row) { ?><nama><?php echo $row->account_tw_username;?></nama><?php } ?></names>
        <?php
    }
    function smirnoffxml() {
header("Expires: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
header("Cache-Control: no-cache, must-revalidate" );
header("Pragma: no-cache" );
header("Content-Type: text/xml; charset=utf-8");

        echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";
		
		$this->db->select('distinct(account_id), account_displayname');
        $this->db->where('log_status', 1);
        $this->db->where_in('places_id', array('261','262','263','264','265','266','267','268','269','270'));
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->order_by('log.id', 'asc');
        $this->db->from('log');
        $recent = $this->db->get()->result();
        ?>
<member><?php foreach($recent as $row) { ?><user><name><?php echo $row->account_displayname;?></name></user><?php } ?></member>
        <?php
    }

    function photo() {
		$limit = false;
		if(isset($_GET['time'])&&$_GET['time']!='') {
			$limit = date('Y-m-d H:i:s', $_GET['time']);
		}

        $this->db->select('account_displayname, photo_upload, time_upload');
        $this->db->where('places_id', '104');
        $this->db->join('account', 'account.id = photos.id_account', 'left');
		if($limit)
			$this->db->where('time_upload >', $limit);
		else
			$this->db->limit(5, 0);

        $this->db->order_by('photos.id', 'desc');
        $this->db->from('photos');
        $recent = $this->db->get()->result();
		$data = array(); $x = 0;
		foreach($recent as $image) {
			$data[$x]['user'] = $image->account_displayname;
			$data[$x]['file'] = 'http://wooz.in/uploads/photobooth/'. $image->photo_upload;
			$data[$x]['time'] = $image->time_upload;
			$x++;
		}
		if(count($data) != 0)
			echo json_encode(array('result' => $data));
		else
			echo json_encode(array('result' => array('status' => 0, 'message' => 'no data')));
    }
	
	function gad(){
		$cserial = $this->input->get('no_rfid',true);
		if(!$cserial){
			$cserial = $this->input->post('no_rfid',true);
		}
        if (isset($cserial) && $cserial !='') {
			$user = $this->cekuser($cserial);
			if($user){
				$photo = $this->photogad($user);
				echo json_encode($photo); 
			}else{ //belum registrasi
				$data = array(
					'status' => 0,
                    'name' => 'no'
				);
				echo json_encode($data);
			}
		}else{ //rfid tidak ada
			$data = array(
					'status' => 0,
                    'name' => 'no'
            );
            echo json_encode($data);		
		}
		
	}
	
	function printsukses(){
		$files = $this->input->get('file',true);
		if(!$files){
			$files = $this->input->post('file',true);
		}
        $db['time_processed1'] = 1;		
        $this->db->where('photo_upload', $files);
        $update = $this->db->update('photos', $db);
		echo 'sukses';
	}
	
	function photogad($user){
		$sql = "SELECT photo_upload,time_upload,time_processed1 FROM `wooz_photos` 
				WHERE places_id in ('401', '402','403','404') and `account_id` = ".$user->id." 
				order by id asc";
		$hasil = $this->db->query($sql);
		$data = $hasil->result();
		
		if($data){
			$datas = array();
			for($i=2;$i<=count($data);$i=$i+3){
				if(isset($data[$i]->time_processed1) && $data[$i]->time_processed1 == 0){
					$datas[]=$data[$i];
				}
			}
			if($datas){				
				$file = "http://wooz.in/updategad?card=" . $user->account_rfid . "&places=394";
				$rss = file_get_contents($file);
				$photo = array(
							'file_photo' => $datas[0]->photo_upload,
							'url_file_photo' => 'http://wooz.in/uploads/photobooth/'.$datas[0]->photo_upload,
							'description' => 'Gue baru aja mengabadikan momen kemeriahan perayaan #ArthursDay 2013 di Jakarta yang Luar Biasa.',
							'jam' => date("H:i", strtotime($datas[0]->time_upload))
						   );
				$dataphoto = array(
							'status' => 1,
							'name' => $user->account_displayname,
							'avatar' => $user->account_avatar,
							'photo' => $photo
						   );
			}else{
				$photo = array(
							'file_photo' => '',
							'url_file_photo' => '',
							'description' => '',
							'jam' => date("H:i")
						   );
				$dataphoto = array(
							'status' => 2,
							'name' => $user->account_displayname,
							'avatar' => $user->account_avatar,
							'photo' => $photo
						   );
			}			
		}else{
			$photo = array(
                        'file_photo' => '',
                        'url_file_photo' => '',
						'description' => '',
                        'jam' => date("H:i")
					   );
			$dataphoto = array(
						'status' => 2,
                        'name' => $user->account_displayname,
                        'avatar' => $user->account_avatar,
                        'photo' => $photo
					   );
		}
		return $dataphoto;
	}
	
	function cekuser($no_rfid){
		$user = $this->db->get_where('account', array('account_rfid' => $no_rfid, 'account_status' => 1))->row();
        if ($user) {
			return $user;
        } else { 
			$this->db->select('wooz_account.*');
			$this->db->join('account', 'account.id = landing.account_id', 'left');
			$this->db->where('landing.landing_register_form', 381);
			$this->db->where("landing.landing_rfid", $no_rfid);
			$this->db->from('landing');
			$acc = $this->db->get()->row();
            if($acc){
				return $acc;
			}else{
				return false;
            }
		}
	}

}
