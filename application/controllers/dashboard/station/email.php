<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class email extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function main($id = 0) {
        if ($id == 0 ) {
            redirect('dashboard');
        }
        $data['datauser'] = $this->visitor_m->get_gate($id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $id;
        $data['places'] = $this->visitor_m->get_gate_places($id);

        $this->load->library('form_validation');
        $cekid = $id;
        $this->form_validation->set_rules('name', 'Sender Name', 'trim|required');
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
        $this->form_validation->set_rules('content', 'Content', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $in_data['nama'] = $this->input->post('name', true);
            $in_data['subject'] = $this->input->post('subject', true);
            $in_data['content'] = $this->input->post('content', true);
            $in_data['places_id'] = $id;
            $in_data['gate_id'] = 20;
            $uploadfile = $this->do_upload();
            if($uploadfile['status']){
                $header_post = str_replace(' ', '_', $_FILES["file"]['name']);
                $in_data['attachment'] = '/uploads/file/'.$header_post;
                $data['message'] = 'success';
            }else{
                $data['message'] = $uploadfile['message'];
            }
            if($data['places']){
                $this->db->where('id', $data['places']->id);
                $this->db->update('gate_email', $in_data);
            }else{
                $this->db->insert('gate_email', $in_data);
            }

            $data['content'] = $this->load->view('dashboard/gate/successemail', $data, true);
        } else {
            $data['content'] = $this->load->view('dashboard/gate/email', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function do_upload(){
        $name = str_replace(' ', '_', $_FILES["file"]['name']);
        $config =  array(
                  'upload_path'     => FCPATH."/uploads/file/",
                  'upload_url'      => base_url()."uploads/file/",
                  'allowed_types'   => 'gif|jpg|png|doc|docx|xls|xlsx|pdf',
                  'overwrite'       => TRUE,
                  'file_name'       => $name
                );

        $this->load->library('upload', $config);
        if($this->upload->do_upload('file'))
        {
            $data = array('status' => true,'message' => '');
        }
        else
        {
            $data = array('status' => false,'message' => $this->upload->display_errors());
        }
        return $data;
    }

    function delete(){
        $id = $this->input->post('places_id');
        $in_data['attachment'] = '';
        $this->db->where('places_id', $id);
        $this->db->update('gate_email', $in_data);
        if(!empty($ret)) echo json_encode('ok');
        else echo json_encode('failed');

    }
}
