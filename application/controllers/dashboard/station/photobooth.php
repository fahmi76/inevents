<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class photobooth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/station_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'Station';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->station_m->list_gate();
        
        $data['content'] = $this->load->view('dashboard/gate/home',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function main($id = 0){
        if ($id == 0) {
            redirect('dashboard');
        }
        $data['page'] = 'Home';
        $data['title'] = 'Photobooth';
        $data['places_id'] = $id;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['places'] = $this->station_m->get_name_places($id);
        $data['data'] = $this->station_m->list_gate_event($id,4);
        
        $data['content'] = $this->load->view('dashboard/station/photobooth',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function add($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $data['places_id'] = $id;
        $data['page'] = 'Home';
        $data['title'] = 'Station';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places'] = $this->station_m->get_name_places($id);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name Station', 'trim|required|callback_visitor_used[' . $id . ']');
        $this->form_validation->set_rules('album', 'Facebook Album', 'trim|required');
        $this->form_validation->set_rules('facebook', 'Facebook message', 'trim|required');
        $this->form_validation->set_rules('twitter', 'Twitter message', 'trim|required');
        $this->form_validation->set_rules('image', 'Frame', 'callback_handle_upload');

        if ($this->form_validation->run() === TRUE) {
            $in_data['places_name'] = $this->input->post('name', true);
            $in_data['places_nicename'] = str_replace(' ', '-', $this->input->post('name', true));
            $header_post = '';
            if($this->do_upload()){
                $header_post = str_replace(' ', '_', $_FILES["image"]['name']);
                $in_data['places_avatar'] = '/uploads/photoframe/'.$header_post;
                $in_data['places_frame'] = '/uploads/photoframe/'.$header_post;
            }
            $in_data['places_parent'] = $id;
            $in_data['places_type'] = 4;
            $in_data['places_model'] = 4;
            $in_data['places_type_registration'] = 0;
            $in_data['places_address'] = 'Trinidad & Tobago';
            $in_data['places_desc'] = $this->input->post('name', true);
            $in_data['places_landing'] = str_replace(' ', '-', $this->input->post('name', true));
            $in_data['places_album'] = $this->input->post('album');
            $in_data['places_fb_caption'] = $this->input->post('facebook');
            $in_data['places_tw_caption'] = $this->input->post('twitter');
            $in_data['places_cstatus_fb'] = $this->input->post('facebook');
            $in_data['places_cstatus_tw'] = $this->input->post('twitter');

            $this->db->insert('places', $in_data);
            $data['content'] = $this->load->view('dashboard/station/successphoto', $data, true);
        } else {
            $data['content'] = $this->load->view('dashboard/station/photoboothadd', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function edit($id = 0,$places_id = 0) {
        if ($id == 0 || $places_id == 0) {
            redirect('dashboard');
        }
        $data['datauser'] = $this->station_m->get_gate($id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $places_id;
        $data['places'] = $this->station_m->get_name_places($places_id);

        $this->load->library('form_validation');
        $cekid = $id.'-'.$places_id;
        $this->form_validation->set_rules('name', 'Name Station', 'trim|required|callback_visitor_usededit('.$cekid.')');
        $this->form_validation->set_rules('album', 'Facebook Album', 'trim|required');
        $this->form_validation->set_rules('facebook', 'Facebook message', 'trim|required');
        $this->form_validation->set_rules('twitter', 'Twitter message', 'trim|required');
        $this->form_validation->set_rules('image', 'Frame', 'callback_handle_uploadedit');

        if ($this->form_validation->run() === TRUE) {
            $in_data['places_name'] = $this->input->post('name', true);
            $in_data['places_nicename'] = str_replace(' ', '-', $this->input->post('name', true));
            $header_post = '';
            if($this->do_upload()){
                $header_post = str_replace(' ', '_', $_FILES["image"]['name']);
                $in_data['places_avatar'] = '/uploads/photoframe/'.$header_post;
                $in_data['places_frame'] = '/uploads/photoframe/'.$header_post;
            }
            $in_data['places_type'] = 4;
            $in_data['places_model'] = 4;
            $in_data['places_desc'] = $this->input->post('name', true);
            $in_data['places_landing'] = str_replace(' ', '-', $this->input->post('name', true));
            $in_data['places_album'] = $this->input->post('album');
            $in_data['places_fb_caption'] = $this->input->post('facebook');
            $in_data['places_tw_caption'] = $this->input->post('twitter');
            $in_data['places_cstatus_fb'] = $this->input->post('facebook');
            $in_data['places_cstatus_tw'] = $this->input->post('twitter');

            $this->db->where('id', $id);
            $this->db->update('places', $in_data);

            $data['content'] = $this->load->view('dashboard/station/successphoto', $data, true);
        } else {
            $data['content'] = $this->load->view('dashboard/station/photoboothedit', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function visitor_usededit($str,$id) {
        $cek = explode('-', $id);
        $gate_id = $cek[0];
        $places_id = $cek[1];
        if (!isset($str)) {
            $str = $this->input->post('name');
        }
        $new = $this->db->get_where('places', array('places_name' => $str,'places_parent' => $places_id))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('places', array('places_name' => $str, 'id' => $gate_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('visitor_usededit', 'Photobooth already registered');
                return false;
            }
        } else {
            return true;
        }
    }

    function handle_uploadedit()
    {
        if (isset($_FILES['image']) && !empty($_FILES['image']['name']))
        {
            $test = getimagesize($_FILES["image"]["tmp_name"]);
            $width = $test[0];
            $height = $test[1];
            if ($width != 640 && $height != 480){
                // throw an error because nothing was uploaded
                $this->form_validation->set_message('handle_uploadedit', "Your images must be 640x480 pixel!");
                return false;
            }
            if ($this->do_upload())
            {
                // set a $_POST value for 'image' that we can use later
                return true;
            }
            else
            {
                // possibly do some clean up ... then throw an error
                $this->form_validation->set_message('handle_uploadedit', $this->upload->display_errors());
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    function visitor_used($str, $id) {
        $str = strtolower($str);
        if ($str != "") {
            $row = $this->db->get_where('places', array('places_name' => $str, 'places_parent' => $id))->row();
            if (count($row) != 0) {
                $this->form_validation->set_message('visitor_used', 'Station already added');
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    function handle_upload()
    {
        if (isset($_FILES['image']) && !empty($_FILES['image']['name']))
        {
            $test = getimagesize($_FILES["image"]["tmp_name"]);
            $width = $test[0];
            $height = $test[1];
            if ($width != 640 && $height != 480){
                // throw an error because nothing was uploaded
                $this->form_validation->set_message('handle_upload', "Your images must be 640x480 pixel!");
                return false;
            }
            if ($this->do_upload())
            {
                // set a $_POST value for 'image' that we can use later
                return true;
            }
            else
            {
                // possibly do some clean up ... then throw an error
                $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
                return false;
            }
        }
        else
        {
            // throw an error because nothing was uploaded
            $this->form_validation->set_message('handle_upload', "You must upload an image!");
            return false;
        }
    }
    function do_upload(){
        $name = str_replace(' ', '_', $_FILES["image"]['name']);
        $config =  array(
                  'upload_path'     => FCPATH."/uploads/photoframe/",
                  'upload_url'      => base_url()."uploads/photoframe/",
                  'allowed_types'   => "png",
                  'overwrite'       => TRUE,
                  'file_name'       => $name
                );

        $this->load->library('upload', $config);
        if($this->upload->do_upload('image'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function delete($id = 0) {
        $id = $this->input->post('places_id');
        $where['id'] = $id;
        $query = $this->db->query("DELETE FROM wooz_log where places_id in (18)");
        $query = $this->db->query("DELETE FROM wooz_photos where places_id in (18)");
        if(!empty($ret)) echo json_encode('ok');
        else echo json_encode('failed');
    }

}
