<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/station_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'Station';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->station_m->list_gate();
        
        $data['content'] = $this->load->view('dashboard/gate/home',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function main($id = 0){
        if ($id == 0) {
            redirect('dashboard');
        }
        $data['page'] = 'Home';
        $data['title'] = 'Station';
        $data['places_id'] = $id;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['places'] = $this->station_m->get_name_places($id);
        $data['data'] = $this->station_m->list_gate_event($id);
        
        $data['content'] = $this->load->view('dashboard/station/main',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function show($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['data'] = $this->user_m->getuser($id);

        $data['content'] = $this->load->view('dashboard/user/show', $data, true);
        $this->load->view('dashboard/main', $data);
    }

    function deleteall(){
        $places_id = $this->input->post('places_id',true);
        $query = $this->db->query("DELETE a,c from wooz_account a 
            inner join wooz_landing c on c.account_id = a.id where landing_register_form = '".$places_id."'");
        
        $query = $this->db->query("DELETE FROM wooz_log where places_id in (16,17,18)");
        $query = $this->db->query("DELETE FROM wooz_photos where places_id in (18)");
        #$query = $this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6");

        if(!empty($ret)) echo json_encode('ok');
        else echo json_encode('failed');

    }

    function delete($id = 0) {
        $id = $this->input->post('places_id');
        $where['id'] = $id;
        $query = $this->db->query("DELETE FROM wooz_log where places_id in (16,17)");
        if(!empty($ret)) echo json_encode('ok');
        else echo json_encode('failed');
    }

}
