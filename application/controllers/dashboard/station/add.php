<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class add extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/station_m');
		$this->load->helper('form');

		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_id = $cek_admin->id;
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	function index() {
		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['parent'] = 'Add';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name Gate', 'trim|required|callback_visitor_used');

		if ($this->form_validation->run() === TRUE) {
			$in_data['nama'] = $this->input->post('name', true);
			$in_data['data_status'] = 1;
			$in_data['date_add'] = date('Y-m-d H:i:s');

			$this->db->insert('gate', $in_data);
			$data['content'] = $this->load->view('dashboard/gate/success', $data, true);
		} else {
			$data['content'] = $this->load->view('dashboard/gate/add', $data, true);
		}
		$this->load->view('dashboard/main', $data);
	}

	function main($id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$data['places_id'] = $id;
		$data['page'] = 'Home';
		$data['title'] = 'Station';
		$data['parent'] = 'Add';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places'] = $this->station_m->get_name_places($id);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name Station', 'trim|required|callback_visitor_used[' . $id . ']');
		// $this->form_validation->set_rules('share', 'Share', 'trim|required');
		// $this->form_validation->set_rules('facebook', 'Facebook message', 'trim|required');
		// $this->form_validation->set_rules('twitter', 'Twitter message', 'trim|required');
		$this->form_validation->set_rules('image', 'Image', 'callback_handle_upload');

		if ($this->form_validation->run() === TRUE) {
			$in_data['places_name'] = $this->input->post('name', true);
			$in_data['places_nicename'] = str_replace(' ', '-', $this->input->post('name', true));
			$header_post = '';
			if ($this->do_upload()) {
				$header_post = str_replace(' ', '_', $_FILES["image"]['name']);
				$in_data['places_avatar'] = '/uploads/apps/event/' . $header_post;
			}
			$in_data['places_parent'] = $id;
			$in_data['places_type'] = $this->input->post('share');
			$in_data['places_model'] = $this->input->post('share');
			$in_data['places_type_registration'] = 0;
			$in_data['places_address'] = 'Trinidad & Tobago';
			$in_data['places_desc'] = $this->input->post('name', true);
			$in_data['places_landing'] = str_replace(' ', '-', $this->input->post('name', true));
			$in_data['places_cstatus_fb'] = $this->input->post('facebook');
			$in_data['places_cstatus_tw'] = $this->input->post('twitter');
			$in_data['places_fb_caption'] = $this->input->post('facebook');
			$emailattch = array();
			if (isset($_FILES['attach'])) {
				$filesCount = count($_FILES['attach']['name']);
				for ($i = 0; $i < $filesCount; $i++) {
					if ($_FILES['attach']['error'][$i] == 0) {
						$_FILES['userFile']['name'] = $_FILES['attach']['name'][$i];
						$_FILES['userFile']['type'] = $_FILES['attach']['type'][$i];
						$_FILES['userFile']['tmp_name'] = $_FILES['attach']['tmp_name'][$i];
						$_FILES['userFile']['error'] = $_FILES['attach']['error'][$i];
						$_FILES['userFile']['size'] = $_FILES['attach']['size'][$i];
						$this->do_upload_email();
						$name = str_replace(' ', '_', $_FILES['attach']['name'][$i]);
						$emailattch[] = $name;
					}
				}
			}
			$in_data['places_email_attach'] = json_encode($emailattch);
			$in_data['places_tw_caption'] = $this->input->post('twitter');
			$in_data['places_email'] = $this->input->post('sender_name');
			$in_data['places_subject_email'] = $this->input->post('subject');
			$in_data['places_custom_email'] = $this->input->post('content');

			$in_data['places_startdate'] = date("Y-m-d", strtotime($this->input->post('sdate', true)));
			$in_data['places_duedate'] = date("Y-m-d", strtotime($this->input->post('edate', true)));

			$this->db->insert('places', $in_data);
			$data['content'] = $this->load->view('dashboard/station/success', $data, true);
		} else {
			$data['content'] = $this->load->view('dashboard/station/add', $data, true);
		}
		$this->load->view('dashboard/main', $data);
	}
	function visitor_used($str, $id) {
		$str = strtolower($str);
		if ($str != "") {
			$row = $this->db->get_where('places', array('places_name' => $str, 'places_parent' => $id))->row();
			if (count($row) != 0) {
				$this->form_validation->set_message('visitor_used', 'Station already added');
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	function handle_upload() {
		// $share = $this->input->post('share');
		// if (!$share) {
		// 	// throw an error because nothing was uploaded
		// 	$this->form_validation->set_message('handle_upload', "You must choose share option!");
		// 	return false;
		// }

		$share = 1;
		if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
			$test = getimagesize($_FILES["image"]["tmp_name"]);
			$width = $test[0];
			$height = $test[1];
			if ($share == 1) {
				if ($width != 200 && $height != 200) {
					// throw an error because nothing was uploaded
					$this->form_validation->set_message('handle_upload', "Your images must be 200x200 pixel!");
					return false;
				}
			}
			if ($this->do_upload()) {
				// set a $_POST value for 'image' that we can use later
				return true;
			} else {
				// possibly do some clean up ... then throw an error
				$this->form_validation->set_message('handle_upload', $this->upload->display_errors());
				return false;
			}
		} else {
			// throw an error because nothing was uploaded
			$this->form_validation->set_message('handle_upload', "You must upload an image!");
			return false;
		}
	}
	function do_upload() {
		$name = str_replace(' ', '_', $_FILES["image"]['name']);
		$config = array(
			'upload_path' => FCPATH . "/uploads/apps/event/",
			'upload_url' => base_url() . "uploads/apps/event/",
			'allowed_types' => "jpg|png|jpeg",
			'overwrite' => TRUE,
			'file_name' => $name,
		);

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('image')) {
			return true;
		} else {
			return false;
		}
	}

	function do_upload_email() {
		$name = str_replace(' ', '_', $_FILES["userFile"]['name']);
		$config = array(
			'upload_path' => FCPATH . "/uploads/apps/email/",
			'upload_url' => base_url() . "uploads/apps/email/",
			'allowed_types' => '*',
			'overwrite' => TRUE,
			'file_name' => $name,
		);

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('userFile')) {
			return true;
		} else {
			return false;
		}
	}
}
