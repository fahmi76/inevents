<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class edit extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/station_m');
		$this->load->helper('form');

		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_id = $cek_admin->id;
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	function main($id = 0, $places_id = 0) {
		if ($id == 0 || $places_id == 0) {
			redirect('dashboard');
		}
		$data['id'] = $id;
		$data['datauser'] = $this->station_m->get_gate($id);
		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['parent'] = 'Add';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $places_id;
		$data['places'] = $this->station_m->get_name_places($places_id);

		$this->load->library('form_validation');
		$cekid = $id . '-' . $places_id;
		$this->form_validation->set_rules('name', 'Name Station', 'trim|required|callback_visitor_used(' . $cekid . ')');
		// $this->form_validation->set_rules('share', 'Share', 'trim|required');
		// $this->form_validation->set_rules('facebook', 'Facebook message', 'trim|required');
		// $this->form_validation->set_rules('twitter', 'Twitter message', 'trim|required');
		$this->form_validation->set_rules('image', 'Image', 'callback_handle_upload');

		if ($this->form_validation->run() === TRUE) {
			$in_data['places_name'] = $this->input->post('name', true);
			$in_data['places_nicename'] = str_replace(' ', '-', $this->input->post('name', true));
			$header_post = '';
			if ($this->do_upload()) {
				$header_post = str_replace(' ', '_', $_FILES["image"]['name']);
				$in_data['places_avatar'] = '/uploads/apps/event/' . $header_post;
			}
			$in_data['places_type'] = $this->input->post('share');
			$in_data['places_model'] = $this->input->post('share');
			$in_data['places_desc'] = $this->input->post('name', true);
			$in_data['places_landing'] = str_replace(' ', '-', $this->input->post('name', true));
			$in_data['places_cstatus_fb'] = $this->input->post('facebook');
			$in_data['places_cstatus_tw'] = $this->input->post('twitter');
			$in_data['places_fb_caption'] = $this->input->post('facebook');
			$in_data['places_tw_caption'] = $this->input->post('twitter');
			$xy = 0;
			$emailattch = array();
			if (isset($_FILES['attach'])) {
				$filesCount = count($_FILES['attach']['name']);
				for ($i = 0; $i < $filesCount; $i++) {
					if ($_FILES['attach']['error'][$i] == 0) {
						$_FILES['userFile']['name'] = $_FILES['attach']['name'][$i];
						$_FILES['userFile']['type'] = $_FILES['attach']['type'][$i];
						$_FILES['userFile']['tmp_name'] = $_FILES['attach']['tmp_name'][$i];
						$_FILES['userFile']['error'] = $_FILES['attach']['error'][$i];
						$_FILES['userFile']['size'] = $_FILES['attach']['size'][$i];
						$this->do_upload_email();
						$name = str_replace(' ', '_', $_FILES['attach']['name'][$i]);
						$emailattch[] = $name;
						$xy++;
					}
				}
			}
			if ($xy != 0) {
				$in_data['places_email_attach'] = json_encode($emailattch);
			}
			$in_data['places_email'] = $this->input->post('sender_name');
			$in_data['places_subject_email'] = $this->input->post('subject');
			$in_data['places_custom_email'] = $this->input->post('content');

			$this->db->where('id', $id);
			$this->db->update('places', $in_data);

			$data['content'] = $this->load->view('dashboard/station/success', $data, true);
		} else {
			$data['content'] = $this->load->view('dashboard/station/edit', $data, true);
		}
		$this->load->view('dashboard/main', $data);
	}

	function visitor_used($str, $id) {
		$cek = explode('-', $id);
		$gate_id = $cek[0];
		$places_id = $cek[1];
		if (!isset($str)) {
			$str = $this->input->post('name');
		}
		$new = $this->db->get_where('places', array('places_name' => $str, 'places_parent' => $places_id))->row();
		if (count($new) >= 1) {
			$news = $this->db->get_where('places', array('places_name' => $str, 'id' => $gate_id))->row();
			if ($news) {
				return true;
			} else {
				$this->form_validation->set_message('visitor_used', 'Gate already registered');
				return false;
			}
		} else {
			return true;
		}
	}

	function deletefile($id = 0, $places_id = 0, $file) {
		$data = $this->station_m->get_gate($id);
		$attach = json_decode($data->places_email_attach);
		$emailattch = array();
		foreach ($attach as $row) {
			if ($row != $file) {
				$emailattch[] = $row;
			}

		}
		$in_data['places_email_attach'] = json_encode($emailattch);
		$this->db->where('id', $id);
		$this->db->update('places', $in_data);
		redirect('dashboard/station/edit/main/' . $id . '/' . $places_id);
	}

	function handle_upload() {
		// $share = $this->input->post('share');
		// if (!$share) {
		// 	// throw an error because nothing was uploaded
		// 	$this->form_validation->set_message('handle_upload', "You must choose share option!");
		// 	return false;
		// }
		$share = 1;
		if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
			$test = getimagesize($_FILES["image"]["tmp_name"]);
			$width = $test[0];
			$height = $test[1];
			if ($share == 1) {
				if ($width != 200 && $height != 200) {
					// throw an error because nothing was uploaded
					$this->form_validation->set_message('handle_upload', "Your images must be 200x200 pixel!");
					return false;
				}
			}
			if ($this->do_upload()) {
				// set a $_POST value for 'image' that we can use later
				return true;
			} else {
				// possibly do some clean up ... then throw an error
				$this->form_validation->set_message('handle_upload', $this->upload->display_errors());
				return false;
			}
		} else {
			return true;
		}
	}
	function do_upload() {
		$name = str_replace(' ', '_', $_FILES["image"]['name']);
		$config = array(
			'upload_path' => FCPATH . "/uploads/apps/event/",
			'upload_url' => base_url() . "uploads/apps/event/",
			'allowed_types' => "jpg|png|jpeg",
			'overwrite' => TRUE,
			'file_name' => $name,
		);

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('image')) {
			return true;
		} else {
			return false;
		}
	}
	function do_upload_email() {
		$name = str_replace(' ', '_', $_FILES["userFile"]['name']);
		$config = array(
			'upload_path' => FCPATH . "/uploads/apps/email/",
			'upload_url' => base_url() . "uploads/apps/email/",
			'allowed_types' => '*',
			'overwrite' => TRUE,
			'file_name' => $name,
		);

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('userFile')) {
			return true;
		} else {
			return false;
		}
	}
}
