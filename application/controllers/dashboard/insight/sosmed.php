<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sosmed extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/insightsosmed_m');
        $this->load->helper('form');
        $this->load->library('fungsi');
        $this->load->library('taggly');
        
        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id,$this->user_name,$this->admin_logged);
        if($cek_admin){
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        }else{
            redirect('dashboard/login');
        }
    }
    
    function main($id){
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $id;
        $gate = $this->insightsosmed_m->list_places($id);
        $data['count_regis'] = $this->insightsosmed_m->count_list_regis($id);
        $data['datalike'] = $this->insightsosmed_m->spotlikeinfo($id);
        $data['datasosmed'] = $this->insightsosmed_m->getsosmed($id);
        $data['datalogfb'] = $this->insightsosmed_m->get_log_fb(16,17,19);
        $data['datalogtw'] = $this->insightsosmed_m->get_log_tw(16,17,19);
        $data['dataphotofb'] = $this->insightsosmed_m->get_log_fb(18);
        $data['dataphototw'] = $this->insightsosmed_m->get_log_tw(18);
        $places = '(';
        $x = 0;
        foreach ($gate as $row) {
            $data['station'][] = array(
                                    'id' => $row->id,
                                    'nama' => $row->places_name,
                                    'total' => $this->insightsosmed_m->logcheckinstaff($row->id)
                                );

            $data['grafikcheckin'][] = array(
                                                'nama' => $row->places_name,
                                                'data' => $this->hourchartactivity($row->id,1,1)
                                            );
            $places .= $row->id;
            if($x == (count($gate)-1)){
                $places .= ')';
            }else{
                $places .= ',';
            }
            $x++;
            #$data['grafikcheckin'][$row->nama] = $this->hourchartactivity($row->id,1,1);
        }
        
        $data['grafikregis'] = array(
                                            'nama' => 'registration',
                                            'data' => $this->hourchartregis($id,1,1)
                                        );
        $data['count_all'] = $this->insightsosmed_m->count_list_all_gate($places);

        $data['content'] = $this->load->view('dashboard/insight/sosmed',$data, true);
        $this->load->view('dashboard/main',$data);

    }

    public function mainlist($gate,$check,$status,$id)
    {
        if($check == 1){
            $data['from'] = 1;
        }elseif($check == 2){
            $data['from'] = 2;
        }{
            $data['from'] = 3;
        }
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard - Insight';
        if($gate == 11){
            $data['check'] = 'All';
        }else{
            $data['check'] = $this->insightsosmed_m->get_gate($gate);
        }
        $data['status'] = $status;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $id;
        $data['gate'] = $gate;
        $data['checkid'] = $check;
        
        $data['tabel'] = $this->insightsosmed_m->list_tabel($id);
        

        $data['content'] = $this->load->view('dashboard/insight/listcheck',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    public function ajax_list($gate,$check,$status,$id)
    {
        $list = $this->main_m->get_datatables($gate,$check,$status,$id);
        $data = array();
        $no = $_POST['start'];
        $tabel = $this->insightsosmed_m->list_tabel($id);
        foreach ($list as $person) {
            $no++;
            $row = array();
            if($person->log_fb_places == 1){
                $row[] = $person->account_displayname.' (GUEST)';
            }else{
                $row[] = $person->account_displayname;
            }
            foreach($tabel as $rowtabel){
                $datas = $this->insightsosmed_m->ambil_content_data($person->account_id,$id,$rowtabel->id);
                if($datas){
                    $row[] = $datas->content;
                }else{
                    $row[] = '';
                }
            }
            if($person->log_check_out == 2){
                $row[] = 'Check-out';
            }else{
                $row[] = 'Currently Check-in';
            }
            $row[] = $person->log_stamps;
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->main_m->count_all($gate,$check,$status,$id),
                        "recordsFiltered" => $this->main_m->count_filtered($gate,$check,$status,$id),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

	function checkin($gate,$places){
        $data['from'] = 1;
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = $this->insightsosmed_m->get_gate($gate);
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $places;
        $data['gate'] = $gate;
        $data['data'] = $this->insightsosmed_m->list_check($gate);

        $data['content'] = $this->load->view('dashboard/insight/listregis',$data, true);
        $this->load->view('dashboard/main',$data);
	}
    
    function regis($id){
        $data['from'] = 8;
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = '';
        $data['status'] = 3;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $id;
        
        $data['data'] = $this->insightsosmed_m->list_all_reregis($id);
        
        $data['content'] = $this->load->view('dashboard/insight/listregis',$data, true);
        $this->load->view('dashboard/main',$data);
    }
	
    function listall($id){
        $data['from'] = 3;
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = 'All';
        $data['status'] = 3;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $id;
        
        $gated = $this->insightsosmed_m->list_places($id);
        $places = '(';
        $x = 0;
        foreach ($gated as $row) {
            $places .= $row->id;
            if($x == (count($gated)-1)){
                $places .= ')';
            }else{
                $places .= ',';
            }
            $x++;
        }
        
        $data['data'] = $this->insightsosmed_m->list_checkall($places);
        
        $data['content'] = $this->load->view('dashboard/insight/listall',$data, true);
        $this->load->view('dashboard/main',$data);
    }

	function arrive($id){
        if($id == 1){
            $data['from'] = 4;
        }else{
            $data['from'] = 9;
        }
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = '';
        $data['status'] = 3;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->insightsosmed_m->list_arrive($id);
        
        $data['content'] = $this->load->view('dashboard/gate/lists',$data, true);
        $this->load->view('dashboard/main',$data);
	}
	
    function photos($id){
        if($id == 1){
            $data['from'] = 5;
        }else{
            $data['from'] = 6;
        }
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = '';
        $data['status'] = 3;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->insightsosmed_m->list_photos($id);
        
        $data['content'] = $this->load->view('dashboard/gate/listphoto',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function tagging(){
        $data['from'] = 7;
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = '';
        $data['status'] = 3;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->insightsosmed_m->list_tagging();
        
        $data['content'] = $this->load->view('dashboard/gate/listphoto',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function download($id,$gate,$places_id){
        $data['places_id'] = $places_id;
        switch ($id) {
            case 1:
                $name = $this->insightsosmed_m->get_gate($gate);
                $names = explode(' ', $name);
                $data['title'] = 'List-User-Station'.$names[0].'-'.$names[1];
                $data['status'] = $name;
                $data['data'] = $this->insightsosmed_m->list_check($gate);
                $data['id'] = $id; 
                $this->load->view('dashboard/insight/excelstation', $data);
                break;
            case 2:
                $data['title'] = 'List-User-All-Station';
                $data['status'] = 'All-Station';
                $gated = $this->insightsosmed_m->list_places($places_id);
                $places = '(';
                $x = 0;
                foreach ($gated as $row) {
                    $places .= $row->id;
                    if($x == (count($gated)-1)){
                        $places .= ')';
                    }else{
                        $places .= ',';
                    }
                    $x++;
                }
                
                $data['data'] = $this->insightsosmed_m->list_checkall($places);
                $data['id'] = $id; 
                $this->load->view('dashboard/insight/excelstation', $data);
                break;
            case 3:
                $data['title'] = 'List-User-Registration';
                $data['status'] = 'Registration';
                $data['data'] = $this->insightsosmed_m->list_all_reregis($places_id);
                $data['id'] = $id; 
                $this->load->view('dashboard/insight/excelstation', $data);
                break;
            case 4:
                $data['title'] = 'Insight-Sosial-Media';
                $data['status'] = 'Insight';

                $data['datalike'] = $this->insightsosmed_m->spotlikeinfo($places_id);
                $data['datasosmed'] = $this->insightsosmed_m->getsosmed($places_id);
                $data['datalogfb'] = $this->insightsosmed_m->get_log_fb(16,17,19);
                $data['datalogtw'] = $this->insightsosmed_m->get_log_tw(16,17,19);
                $data['dataphotofb'] = $this->insightsosmed_m->get_log_fb(18);
                $data['dataphototw'] = $this->insightsosmed_m->get_log_tw(18);
                $data['id'] = $id; 
                $this->load->view('dashboard/insight/excelinsight', $data);
                break;
            default:
                break;
        }

    }

	function delete($id,$gate,$places_id){
        switch ($id) {
            case 1:
                $this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '".$gate."' and log_check = 1");
                break;
            case 2:
                $this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '".$gate."'  and log_check = 2");
                break;
            case 3:
                $this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '".$gate."' ");
                break;
            case 4:
                $this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6 and landing_from_regis = 0");
                break;
            case 5:
                $this->db->query("DELETE FROM wooz_photos WHERE places_id = 7");
                break;
            case 6:
                $this->db->query("DELETE FROM wooz_photos WHERE places_id = 7");
                break;
            case 7:
                $this->db->query("TRUNCATE TABLE wooz_tagging");
                break;
            case 8:
                $this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = 14 and log_check = 1 and log_status = 2");
                break;
            case 9:
                $this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6 and landing_from_regis = 2");
                break;
            default:
                break;
        }
        redirect('dashboard/insight/home/main/'.$places_id);

	}

    function hourchartregis($gate,$check,$status){
        $actvhour = $this->insightsosmed_m->check_hour_regis($gate,$check,$status);
        $hours = array();
        foreach ($actvhour as $row){
            if($row->thedate == 0){
                $row->thedate = 24;
            }
            #$hours[]['date']=$row->thedate;
            #$hours[]['total']=$row->total;
            $hours[] = array(
                        'date' => $row->thedate,
                        'total' => $row->total
                    );
        }      
        sort($hours);
        return $hours;
    }

    function hourchartactivity($gate,$check,$status){
        $actvhour = $this->insightsosmed_m->check_hour($gate,$check,$status);
        $hours = array();
        foreach ($actvhour as $row){
			if($row->thedate == 0){
				$row->thedate = 24;
			}
            #$hours[]['date']=$row->thedate;
            #$hours[]['total']=$row->total;
            $hours[] = array(
						'date' => $row->thedate,
						'total' => $row->total
					);
        }      
		sort($hours);
        return $hours;
    }

    function downloadgraph($id){
        if($id == 1){
            $file = 'check-in-staff.png';
            $post_file = $_POST['img_val'.$id];
        }else{
            $file = 'check-in-backstage.png';
            $post_file = $_POST['img_val'.$id];
        }
        $dir = FCPATH.'uploads/grafik/'.$file;
        //Get the base-64 stgring from data
        $filteredData=substr($post_file, strpos($post_file, ",")+1);

        //Decode the string
        $unencodedData=base64_decode($filteredData);

        //Save the image
        file_put_contents($dir, $unencodedData);

        header('Pragma: public');
        header('Cache-Control: public, no-cache');
        header('Content-Type: application/octet-stream');
        header('Content-Length: ' . filesize($dir));
        header('Content-Disposition: attachment; filename="' . basename($dir) . '"');
        header('Content-Transfer-Encoding: binary');

        readfile($dir);
    }
}