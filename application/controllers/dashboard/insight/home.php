<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/insight_m');
		$this->load->helper('form');

		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	public function index() {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['preregis'] = $this->insight_m->get_pre();
		$data['arrive_event'] = $this->insight_m->arrive_event(1);
		$data['arrive_event_sosmed'] = $this->insight_m->arrive_event(2);
		$data['checkinre'] = $this->insight_m->logcheckinreregis(14);
		$data['count_photos'] = $this->insight_m->count_photos();
		$data['count_tag_photos'] = $this->insight_m->count_tag_photos();
		$data['count_user_photos'] = $this->insight_m->count_user_photos();
		$data['count_all'] = $this->insight_m->count_list_all_gate();
		$gate = $this->insight_m->list_gate();
		foreach ($gate as $row) {
			$data['checkin'][] = array(
				'id' => $row->id,
				'nama' => $row->nama,
				'total' => $this->insight_m->logcheckinstaff($row->id),
			);
			$data['checkout'][] = array(
				'id' => $row->id,
				'nama' => $row->nama,
				'total' => $this->insight_m->logcheckoutstaff($row->id),
			);
			$data['grafikcheckout'][$row->nama] = $this->hourchartactivity($row->id, 2, 1);
			$data['grafikcheckin'][$row->nama] = $this->hourchartactivity($row->id, 1, 1);
		}
		$data['content'] = $this->load->view('dashboard/insight/main', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function mainall($id) {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;
		$gate = $this->insight_m->list_gate($id);
		$station = $this->insight_m->list_places($id);
		$data['count_all'] = $this->insight_m->count_list_all_gate($id);

		foreach ($gate as $row) {
			$data['checkin'][] = array(
				'id' => $row->id,
				'nama' => $row->nama,
				'total' => $this->insight_m->logcheckinstaff($row->id),
			);
			$data['checkout'][] = array(
				'id' => $row->id,
				'nama' => $row->nama,
				'total' => $this->insight_m->logcheckoutstaff($row->id),
			);
			$data['grafikcheckout'][] = array(
				'nama' => $row->nama,
				'data' => $this->hourchartactivity($row->id, 2, 1),
			);
			$data['grafikcheckin'][] = array(
				'nama' => $row->nama,
				'data' => $this->hourchartactivity($row->id, 1, 1),
			);
		}
		foreach ($station as $rows) {
			$data['station'][] = array(
				'id' => $rows->id,
				'nama' => $rows->nama,
				'total' => $this->insight_m->logcheckinstation($rows->id),
			);
		}
		$data['content'] = $this->load->view('dashboard/insight/mainall', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function main($id) {
		if ($id == 34) {
			redirect('dashboard/insight/home/mainall/' . $id);
		}
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;

		$data['count_all'] = $this->insight_m->count_list_all_gate($id);
		$gate = $this->insight_m->list_gate($id);
		if ($id == 20) {
			$sql = "SELECT sum(content) as total FROM `wooz_form_regis_detail` WHERE form_regis_id = 27 ";
			$query = $this->db->query($sql);
			$data['count_guest'] = $query->row()->total;
			$sql = "SELECT sum(content) as total FROM `wooz_form_regis_detail` WHERE form_regis_id = 28 ";
			$query = $this->db->query($sql);
			$data['count_guest_child'] = $query->row()->total;

		}
		if ($id == 21) {
			$sql = "SELECT count(distinct(account_id)) as total FROM `wooz_landing` WHERE landing_from_regis = 2 and landing_register_form = 21";
			$query = $this->db->query($sql);
			$data['count_pre'] = $query->row()->total;
			$sql = "SELECT count(distinct(account_id)) as total FROM `wooz_landing` WHERE landing_from_regis = 21 and landing_register_form = 21 and landing_level = 2";
			$query = $this->db->query($sql);
			$data['count_re'] = $query->row()->total;
			$sql = "SELECT count(distinct(account_id)) as total FROM `wooz_landing` WHERE landing_from_regis = 21 and landing_register_form = 21 and landing_level = 1";
			$query = $this->db->query($sql);
			$data['count_new'] = $query->row()->total;

		}
		if ($id == 37) {
			$sqlteam = "SELECT b.content,count(distinct(a.account_id)) as total FROM `wooz_log_user_gate` a inner join wooz_form_regis_detail b on b.account_id = a.account_id WHERE a.places_id = 37 and log_gate = '38' and b.form_regis_id = 90 group by b.content order by content asc";
			$queryteam = $this->db->query($sqlteam);
			$data['team'] = $queryteam->result();

		}
		foreach ($gate as $row) {
			if ($id == 21) {
				$sql = "SELECT count(account_id) as total FROM `wooz_log_user_gate`  a inner join wooz_account b on b.id = a.account_id WHERE places_id = 21 and log_gate = '" . $row->id . "' and log_email != 0";
				$query = $this->db->query($sql);
				$emaildata = $query->row()->total;
				$data['checkin'][] = array(
					'id' => $row->id,
					'nama' => $row->nama,
					'total' => $this->insight_m->logcheckinstaff($row->id),
					'email' => $emaildata,
				);
			} else {
				$data['checkin'][] = array(
					'id' => $row->id,
					'nama' => $row->nama,
					'total' => $this->insight_m->logcheckinstaff($row->id),
				);
			}
			$data['checkout'][] = array(
				'id' => $row->id,
				'nama' => $row->nama,
				'total' => $this->insight_m->logcheckoutstaff($row->id),
			);
			$data['grafikcheckout'][] = array(
				'nama' => $row->nama,
				'data' => $this->hourchartactivity($row->id, 2, 1),
			);
			$data['grafikcheckin'][] = array(
				'nama' => $row->nama,
				'data' => $this->hourchartactivity($row->id, 1, 1),
			);
			#$data['grafikcheckin'][$row->nama] = $this->hourchartactivity($row->id,1,1);
		}

		$data['content'] = $this->load->view('dashboard/insight/main', $data, true);
		$this->load->view('dashboard/main', $data);

	}

	public function mainlist($gate, $check, $status, $id) {
		if ($check == 1) {
			$data['from'] = 1;
		} elseif ($check == 2) {
			$data['from'] = 2;
		}{
			$data['from'] = 3;
		}
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard - Insight';
		if ($gate == 11) {
			$data['check'] = 'All';
		} else {
			$data['check'] = $this->insight_m->get_gate($gate);
		}
		$data['status'] = $status;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;
		$data['gate'] = $gate;
		$data['checkid'] = $check;

		$data['tabel'] = $this->insight_m->list_tabel($id);

		$data['content'] = $this->load->view('dashboard/insight/listcheck', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	public function ajax_list($gate, $check, $status, $id) {
		$this->load->model('dashboard/mainstation_m');
		$list = $this->mainstation_m->get_datatables($gate, $check, $status, $id);
		$data = array();
		$no = $_POST['start'];
		$tabel = $this->insight_m->list_tabel($id);
		foreach ($list as $person) {
			$no++;
			$row = array();
			if ($person->log_fb_places == 1) {
				$row[] = $person->account_displayname . ' (GUEST)';
			} else {
				$row[] = $person->account_displayname;
			}
			foreach ($tabel as $rowtabel) {
				$datas = $this->insight_m->ambil_content_data($person->account_id, $id, $rowtabel->id);
				if ($datas) {
					$row[] = $datas->content;
				} else {
					$row[] = '';
				}
			}
			$row[] = 'Currently Check-in';
			$row[] = $person->log_stamps;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->mainstation_m->count_all($gate, $check, $status, $id),
			"recordsFiltered" => $this->mainstation_m->count_filtered($gate, $check, $status, $id),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function checkin($gate, $check, $status, $id) {
		if ($check == 1) {
			$data['from'] = 1;
		} else {
			$data['from'] = 2;
		}
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = $this->insight_m->get_gate($gate);
		$data['status'] = $status;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;
		$data['gate'] = $gate;

		$data['tabel'] = $this->insight_m->list_tabel($id);
		$data['data'] = $this->insight_m->list_check($gate, $check, $status);
		// xdebug($data);
		$data['content'] = $this->load->view('dashboard/gate/list', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function reregis() {
		$data['from'] = 8;
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = '';
		$data['status'] = 3;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['data'] = $this->insight_m->list_all_reregis();

		$data['content'] = $this->load->view('dashboard/gate/list', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function listregis($id, $from) {
		$data['from'] = $from;
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = 'All';
		$data['status'] = 3;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;

		$data['tabel'] = $this->insight_m->list_tabel($id);

		if ($from == 1) {
			$data['titles'] = 'Pre-registration';
			$sql = "SELECT account_id,account_displayname,landing_joindate as time_upload FROM `wooz_landing` a inner join wooz_account b on b.id = a.account_id WHERE landing_from_regis = 2 and landing_register_form = 21";

		} elseif ($from == 2) {
			$data['titles'] = 'new-registration';
			$sql = "SELECT account_id,account_displayname,landing_joindate as time_upload FROM `wooz_landing` a inner join wooz_account b on b.id = a.account_id WHERE landing_from_regis = 21 and landing_register_form = 21 and landing_level = 2";

		} else {
			$data['titles'] = 'Re-registration';
			$sql = "SELECT account_id,account_displayname,landing_joindate as time_upload FROM `wooz_landing` a inner join wooz_account b on b.id = a.account_id WHERE landing_from_regis = 21 and landing_register_form = 21 and landing_level = 1";
		}

		$query = $this->db->query($sql);
		$data['data'] = $query->result();

		if ($this->input->get('download') == 1) {
			$this->load->view('dashboard/insight/excelregis', $data);
		} else {
			$data['content'] = $this->load->view('dashboard/gate/listregis', $data, true);
			$this->load->view('dashboard/main', $data);

		}
	}

	function listall($id) {
		$data['from'] = 3;
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = 'All';
		$data['status'] = 3;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;

		$data['tabel'] = $this->insight_m->list_tabel($id);

		$data['data'] = $this->insight_m->list_all_gate($id);

		$data['content'] = $this->load->view('dashboard/gate/listall', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function arrive($id) {
		if ($id == 1) {
			$data['from'] = 4;
		} else {
			$data['from'] = 9;
		}
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = '';
		$data['status'] = 3;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['data'] = $this->insight_m->list_arrive($id);

		$data['content'] = $this->load->view('dashboard/gate/lists', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function photos($id) {
		if ($id == 1) {
			$data['from'] = 5;
		} else {
			$data['from'] = 6;
		}
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = '';
		$data['status'] = 3;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['data'] = $this->insight_m->list_photos($id);

		$data['content'] = $this->load->view('dashboard/gate/listphoto', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function tagging() {
		$data['from'] = 7;
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = '';
		$data['status'] = 3;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['data'] = $this->insight_m->list_tagging();

		$data['content'] = $this->load->view('dashboard/gate/listphoto', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function download($id, $gate, $places_id) {
		$data['places_id'] = $places_id;
		$data['gate'] = $gate;
		$data['from'] = 1;
		switch ($id) {
		case 1:
			$data['title'] = 'List-User-Check-in';
			$data['status'] = 'Check-in';
			$data['tabel'] = $this->insight_m->list_tabel($places_id);
			$data['user'] = $this->insight_m->list_check($gate, 1, 1);
			break;
		case 2:
			$data['title'] = 'List-User-Check-Out';
			$data['status'] = 'Check-in';
			$data['from'] = 2;
			$data['tabel'] = $this->insight_m->list_tabel($places_id);
			$data['user'] = $this->insight_m->list_check($gate, 2, 1);
			break;
		case 3:
			$data['title'] = 'List-User-Check-in-and-Check-out';
			$data['status'] = 'Check-in';
			$data['tabel'] = $this->insight_m->list_tabel($places_id);
			$data['user'] = $this->insight_m->list_all_gate($places_id);
			break;
		case 4:
			$data['title'] = 'List-User-Arrive';
			$data['status'] = 'Arrive';
			$data['user'] = $this->insight_m->list_arrive(1);
			break;
		case 5:
			$data['title'] = 'List-User-Photo';
			$data['status'] = 'Photo';
			$data['user'] = $this->insight_m->list_photos(1);
			break;
		case 6:
			$data['title'] = 'List-User-Photo-Taken';
			$data['status'] = 'Photo';
			$data['user'] = $this->insight_m->list_photos(2);
			break;
		case 7:
			$data['title'] = 'List-User-Tagging';
			$data['status'] = 'Tagging';
			$data['user'] = $this->insight_m->list_tagging();
			break;
		case 8:
			$data['title'] = 'List-User-Checkin-Reregis';
			$data['status'] = 'Checkin';
			$data['user'] = $this->insight_m->list_all_reregis();
			break;
		case 9:
			$data['title'] = 'List-User-Activated-sosmed';
			$data['status'] = 'Arrive';
			$data['user'] = $this->insight_m->list_arrive(2);
			break;
		default:
			break;
		}
		$data['id'] = $id;
		if ($id == 3) {
			$this->load->view('dashboard/insight/excelall', $data);
		} else {
			$this->load->view('dashboard/insight/excel', $data);
		}

	}

	function delete($id, $gate, $places_id) {
		switch ($id) {
		case 1:
			$this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '" . $gate . "' and log_check = 1");
			break;
		case 2:
			$this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '" . $gate . "'  and log_check = 2");
			break;
		case 3:
			$this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '" . $gate . "' ");
			break;
		case 4:
			$this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6 and landing_from_regis = 0");
			break;
		case 5:
			$this->db->query("DELETE FROM wooz_photos WHERE places_id = 7");
			break;
		case 6:
			$this->db->query("DELETE FROM wooz_photos WHERE places_id = 7");
			break;
		case 7:
			$this->db->query("TRUNCATE TABLE wooz_tagging");
			break;
		case 8:
			$this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = 14 and log_check = 1 and log_status = 2");
			break;
		case 9:
			$this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6 and landing_from_regis = 2");
			break;
		default:
			break;
		}
		redirect('dashboard/insight/home/main/' . $places_id);

	}

	function hourchartactivity($gate, $check, $status) {
		$actvhour = $this->insight_m->check_hour($gate, $check, $status);
		$hours = array();
		foreach ($actvhour as $row) {
			if ($row->thedate == 0) {
				$row->thedate = 24;
			}
			#$hours[]['date']=$row->thedate;
			#$hours[]['total']=$row->total;
			$hours[] = array(
				'date' => $row->thedate,
				'total' => $row->total,
			);
		}
		sort($hours);
		return $hours;
	}

	function downloadgraph($id) {
		if ($id == 1) {
			$file = 'check-in-staff.png';
			$post_file = $_POST['img_val' . $id];
		} else {
			$file = 'check-in-backstage.png';
			$post_file = $_POST['img_val' . $id];
		}
		$dir = FCPATH . 'uploads/grafik/' . $file;
		//Get the base-64 stgring from data
		$filteredData = substr($post_file, strpos($post_file, ",") + 1);

		//Decode the string
		$unencodedData = base64_decode($filteredData);

		//Save the image
		file_put_contents($dir, $unencodedData);

		header('Pragma: public');
		header('Cache-Control: public, no-cache');
		header('Content-Type: application/octet-stream');
		header('Content-Length: ' . filesize($dir));
		header('Content-Disposition: attachment; filename="' . basename($dir) . '"');
		header('Content-Transfer-Encoding: binary');

		readfile($dir);
	}
}