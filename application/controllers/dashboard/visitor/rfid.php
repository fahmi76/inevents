<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class rfid extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function index() {
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->visitor_m->list_rfid();
        
        $data['content'] = $this->load->view('dashboard/visitor/list_rfid',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function add() {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;

        $this->load->library('form_validation');

        $this->form_validation->set_rules('rfid', 'RFID', 'trim|required|callback_rfid_used');
        $this->form_validation->set_rules('visitor', 'Visitor Category', 'required');

        if ($this->form_validation->run() === TRUE) {
            $in_data['rfid'] = $this->input->post('rfid', true);
            $in_data['visitor_id'] = $this->input->post('visitor', true);
            $in_data['data_status'] = 1;
            $in_data['date_add'] = date('Y-m-d H:i:s');

            $this->db->insert('visitor_rfid', $in_data);
            $data['content'] = $this->load->view('dashboard/visitor/rfid-success', $data, true);
        } else {
            $data['visitor'] = $this->visitor_m->list_visitor();
            $data['content'] = $this->load->view('dashboard/visitor/rfid-add', $data, true);
        }
        $this->load->view('dashboard/main', $data);        
    }

    function edit($id){        
        $data['datauser'] = $this->visitor_m->get_rfid($id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;

        $this->load->library('form_validation');

        $this->form_validation->set_rules('rfid', 'RFID', 'trim|required|callback_rfid_used_edit('.$id.')');
        $this->form_validation->set_rules('visitor', 'Visitor Category', 'required');

        if ($this->form_validation->run() === TRUE) {
            $in_data['rfid'] = $this->input->post('rfid', true);
            $in_data['visitor_id'] = $this->input->post('visitor', true);

            $this->db->where('id', $id);
            $this->db->update('visitor_rfid', $in_data);

            $data['content'] = $this->load->view('dashboard/visitor/rfid-success', $data, true);
        } else {
            $data['visitor'] = $this->visitor_m->list_visitor();
            $data['content'] = $this->load->view('dashboard/visitor/rfid-edit', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function delete($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $datauser = $this->user_m->getuser($id);
        if ($datauser->account_avatar != '') {
            $path = FCPATH . "uploads/user/" . $datauser->account_avatar . "";
            unlink($path);
        }
        $where['id'] = $id;
        $this->db->delete("account_pre", $where);
        
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/delete', $data, true);
        $this->load->view('dashboard/main', $data);
    }

    function rfid_used($str) {
        $str = strtolower($str);
        if ($str != "") {
            $row = $this->db->get_where('visitor_rfid', array('rfid' => $str))->row();
            if (count($row) != 0) {
                $this->form_validation->set_message('rfid_used', 'RFID already added');
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    function rfid_used_edit($str,$id) {
        if (!isset($str)) {
            $str = $this->input->post('rfid');
        }
        $new = $this->db->get_where('visitor_rfid', array('rfid' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('visitor_rfid', array('rfid' => $str, 'id' => $id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('rfid_used_edit', 'RFID already added');
                return false;
            }
        } else {
            return true;
        }
    }
}
