<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class edit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function main($id = 0,$places_id =0) {
        if ($id == 0 || $places_id == 0) {
            redirect('dashboard');
        }
        $data['datauser'] = $this->visitor_m->getuser($id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $places_id;
        $data['places'] = $this->visitor_m->get_name_places($places_id);
        $this->load->library('form_validation');

        $cekid = $id.'-'.$places_id;
        $this->form_validation->set_rules('name', 'Name', 'trim|required('.$cekid.')');
        $this->form_validation->set_rules('gate', 'Gate', 'required');

        if ($this->form_validation->run() === TRUE) {
            $in_data['nama'] = $this->input->post('name', true);
            $in_data['gate_id'] = json_encode($this->input->post('gate', true));

            $this->db->where('id', $id);
            $this->db->update('visitor', $in_data);

            $data['content'] = $this->load->view('dashboard/visitor/success', $data, true);
        } else {
            $data['gate'] = $this->visitor_m->list_gate_event($places_id);
            $data['content'] = $this->load->view('dashboard/visitor/edit', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function visitor_used($str,$id) {
        $cek = explode('-', $id);
        $gate_id = $cek[0];
        $places_id = $cek[1];
        if (!isset($str)) {
            $str = $this->input->post('name');
        }
        $new = $this->db->get_where('visitor', array('nama' => $str,'places_id' => $places_id))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('visitor', array('nama' => $str, 'id' => $gate_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('visitor_used', 'Visitor already registered');
                return false;
            }
        } else {
            return true;
        }
    }
}
