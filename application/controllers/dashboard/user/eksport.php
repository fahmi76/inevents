<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class eksport extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/user_m');
		$this->load->helper('form');
		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_id = $cek_admin->id;
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	public function index() {
		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['parent'] = 'Add';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['content'] = $this->load->view('dashboard/user/eksport', $data, true);
		$this->load->view('dashboard/main', $data);
	}
	public function main($id) {
		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['parent'] = 'Add';
		$data['places_id'] = $id;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['content'] = $this->load->view('dashboard/user/eksport', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function data($id) {
		ini_set('memory_limit', '-1');
		//load the excel library
		$file = $_FILES['userfile']['tmp_name'];
		$this->load->library('excel');

		//read file from path
		$objPHPExcel = PHPExcel_IOFactory::load($file);

		//get only the Cell Collection
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
			$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
			//header will/should be in row 1 only. of course this can be modified to suit your need.
			if ($row == 1) {
				$header[$row][$column] = $data_value;
			} else {
				$arr_data[$row][$column] = $data_value;
			}
		}
		if ($id == 31) {
			$iheader = 0;
			foreach ($header[1] as $headervalue) {
				if ($iheader >= 9) {
					$return[] = $this->cek_winner_content($headervalue, $id);
				}
				$iheader++;
			}
		}
		if ($id == 32) {
			$iheader = 0;
			foreach ($header[1] as $headervalue) {
				if ($iheader >= 4) {
					$return[] = $this->cek_winner_content($headervalue, $id);
				}
				$iheader++;
			}
		}
		foreach ($arr_data as $value) {
			if ($id == 24) {
				if (isset($value['A'])) {
					$account['account_displayname'] = $value['A'];
					$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
					$account['account_fest'] = 1;
					$account['account_status'] = 2;
					$account['account_rfid'] = 0;

					if (isset($value['K'])) {
						$account['account_lastlogin'] = $value['K'];
					}
					if (isset($value['F'])) {
						$rfid = $value['F'];
						if (strlen($value['F']) < 10) {
							$total = 10 - strlen($value['F']);
							$nol = '';
							for ($i = 1; $i <= $total; $i++) {
								$nol .= '0';
							}
							$rfid = $nol . $rfid;
						}
						$account['account_rfid'] = $rfid;
					} else {
						$account['account_rfid'] = '';
					}

					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = $account['account_rfid'];
					$this->db->insert('landing', $in_data_landing);

					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 43);
					}
					if (isset($value['C'])) {
						$this->save_event_1($acc, $id, $value['C'], 44);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $value['D'], 45);
					}
					if (isset($value['E'])) {
						$this->save_event_1($acc, $id, $value['E'], 46);
					}
					if (isset($value['F'])) {
						$this->save_event_1($acc, $id, $account['account_rfid'], 47);
					}
					if (isset($value['G'])) {
						$this->save_event_1($acc, $id, $value['G'], 48);
					}
					if (isset($value['H'])) {
						$this->save_event_1($acc, $id, $value['H'], 49);
					}
					if (isset($value['I'])) {
						$this->save_event_1($acc, $id, $value['I'], 50);
					}
					if (isset($value['J'])) {
						$this->save_event_1($acc, $id, $value['J'], 51);
					}
				}
			} elseif ($id == 42) {
				if (isset($value['A'])) {
					$account['account_displayname'] = $value['A'];
					$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
					$account['account_fest'] = 1;
					$account['account_status'] = 2;
					$account['account_rfid'] = 0;

					if (isset($value['G'])) {
						$rfid = $value['G'];
						if (strlen($value['G']) < 10) {
							$total = 10 - strlen($value['G']);
							$nol = '';
							for ($i = 1; $i <= $total; $i++) {
								$nol .= '0';
							}
							$rfid = $nol . $rfid;
						}
						$account['account_rfid'] = $rfid;
					} else {
						$account['account_rfid'] = '';
					}

					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = $account['account_rfid'];
					$this->db->insert('landing', $in_data_landing);

					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 92);
					}
					if (isset($value['C'])) {
						$this->save_event_1($acc, $id, $value['C'], 93);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $value['D'], 94);
					}
					if (isset($value['E'])) {
						$this->save_event_1($acc, $id, $value['E'], 95);
					}
					if (isset($value['F'])) {
						$this->save_event_1($acc, $id, $value['F'], 96);
					}
					if (isset($value['G'])) {
						$this->save_event_1($acc, $id, $account['account_rfid'], 97);
					}
				}
			} elseif ($id == 25) {
				if (isset($value['A'])) {
					if (isset($value['D'])) {
						$rfid = $value['D'];
						if (strlen($value['D']) < 10) {
							$total = 10 - strlen($value['D']);
							$nol = '';
							for ($i = 1; $i <= $total; $i++) {
								$nol .= '0';
							}
							$rfid = $nol . $rfid;
						}
						$account['account_rfid'] = $rfid;
					} else {
						$account['account_rfid'] = '';
					}
					$account['account_displayname'] = $value['A'];
					$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
					$account['account_fest'] = 1;
					$account['account_status'] = 2;
					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = $account['account_rfid'];
					$this->db->insert('landing', $in_data_landing);

					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 53);
					}
					if (isset($value['C'])) {
						$this->save_event_1($acc, $id, $value['C'], 54);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $account['account_rfid'], 55);
					}
				}
			} elseif ($id == 30) {
				if (isset($value['A'])) {
					if (isset($value['E'])) {
						$rfid = $value['E'];
						if (strlen($value['E']) < 10) {
							$total = 10 - strlen($value['E']);
							$nol = '';
							for ($i = 1; $i <= $total; $i++) {
								$nol .= '0';
							}
							$rfid = $nol . $rfid;
						}
						$account['account_rfid'] = $rfid;
					} else {
						$account['account_rfid'] = '';
					}
					$account['account_displayname'] = $value['A'];
					$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
					$account['account_fest'] = 1;
					$account['account_status'] = 2;
					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = $account['account_rfid'];
					$this->db->insert('landing', $in_data_landing);

					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 66);
					}
					if (isset($value['C'])) {
						$this->save_event_1($acc, $id, $value['C'], 67);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $value['D'], 68);
					}
					if (isset($value['E'])) {
						$this->save_event_1($acc, $id, $account['account_rfid'], 69);
					}
				}
			} elseif ($id == 31) {
				if (isset($value['A'])) {
					if (isset($value['H'])) {
						$rfid = $value['H'];
						if (strlen($value['H']) < 10) {
							$total = 10 - strlen($value['H']);
							$nol = '';
							for ($i = 1; $i <= $total; $i++) {
								$nol .= '0';
							}
							$rfid = $nol . $rfid;
						}
						$account['account_rfid'] = $rfid;
					} else {
						$account['account_rfid'] = '';
					}
					$account['account_lastname'] = "";
					$account['account_firstname'] = "";
					if (isset($value['A'])) {
						$account['account_lastname'] = $value['A'];
					}
					if (isset($value['B'])) {
						$account['account_firstname'] = $value['B'];
					}
					$account['account_displayname'] = $account['account_firstname'] . " " . $account['account_lastname'];
					$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
					$account['account_fest'] = 1;
					$account['account_status'] = 2;
					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = $account['account_rfid'];
					$this->db->insert('landing', $in_data_landing);

					if (isset($value['A'])) {
						$this->save_event_1($acc, $id, $value['A'], 70);
					}
					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 71);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $value['D'], 72);
					}
					if (isset($value['E'])) {
						$this->save_event_1($acc, $id, $value['E'], 73);
					}
					if (isset($value['F'])) {
						$this->save_event_1($acc, $id, $value['F'], 74);
					} else {
						$this->save_event_1($acc, $id, 0, 74);
					}
					if (isset($value['G'])) {
						$this->save_event_1($acc, $id, $value['G'], 75);
					}
					if (isset($value['H'])) {
						$this->save_event_1($acc, $id, $account['account_rfid'], 76);
					}

					if (isset($value['I']) && ($value['I'] != 'NA' && $value['I'] != 'NA ')) {
						$presenter = explode(',', $value['I']);
						foreach ($presenter as $rowpre) {
							$rowpre = explode('-', $rowpre);
							$urutan = 'a';
							if (isset($rowpre[1]) && $rowpre[1] != '') {
								$urutan = $rowpre[1];
							}
							$hasilpresent = $rowpre[0];
							$this->save_presenter_1($acc, $id, $hasilpresent, 31, $urutan);
						}
					}

					$keyheader = array_keys($header[1]);
					for ($i = 9; $i < count($keyheader); $i++) {
						$header_keys = $this->cek_winner_content($header[1][$keyheader[$i]], $id);
						if (isset($value[$keyheader[$i]]) && ($value[$keyheader[$i]] != 'NA ' && $value[$keyheader[$i]] != 'NA')) {
							echo
							$this->save_winner_1($acc, $id, $value[$keyheader[$i]], $header_keys);
						}
					}

				}
			} elseif ($id == 32) {
				if (isset($value['A'])) {
					if (isset($value['A'])) {
						$account['account_displayname'] = $value['A'];
					}
					$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
					$account['account_fest'] = 1;
					$account['account_status'] = 2;
					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = 0;
					$this->db->insert('landing', $in_data_landing);
					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 77);
					}
					if (isset($value['C'])) {
						$this->save_event_1($acc, $id, $value['C'], 78);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $value['D'], 79);
					}

					$keyheader = array_keys($header[1]);
					for ($i = 4; $i < count($keyheader); $i++) {
						$header_keys = $this->cek_winner_content($header[1][$keyheader[$i]], $id);
						if (isset($value[$keyheader[$i]]) && ($value[$keyheader[$i]] != 'NA ' && $value[$keyheader[$i]] != 'NA')) {
							echo
							$this->save_winner_1($acc, $id, $value[$keyheader[$i]], $header_keys);
						}
					}

				}
			} elseif ($id == 34) {
				if (isset($value['A'])) {
					if (isset($value['H'])) {
						$rfid = $value['H'];
						if (strlen($value['H']) < 10) {
							$total = 10 - strlen($value['H']);
							$nol = '';
							for ($i = 1; $i <= $total; $i++) {
								$nol .= '0';
							}
							$rfid = $nol . $rfid;
						}
						$account['account_rfid'] = $rfid;
					} else {
						$account['account_rfid'] = '';
					}
					$account['account_displayname'] = $value['A'];
					$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
					$account['account_fest'] = 2;
					$account['account_status'] = 2;
					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = $account['account_rfid'];
					$this->db->insert('landing', $in_data_landing);

					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 80);
					}
					if (isset($value['C'])) {
						$this->save_event_1($acc, $id, $value['C'], 81);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $value['D'], 82);
					}
					if (isset($value['E'])) {
						$this->save_event_1($acc, $id, $value['E'], 83);
					}
					if (isset($value['F'])) {
						$this->save_event_1($acc, $id, $value['F'], 84);
					}
					if (isset($value['G'])) {
						$this->save_event_1($acc, $id, $value['G'], 85);
					}
					if (isset($value['H'])) {
						$this->save_event_1($acc, $id, $account['account_rfid'], 86);
					}
				}
			} elseif ($id == 37) {
				if (isset($value['A'])) {
					$account['account_displayname'] = $value['A'];
					$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
					$account['account_fest'] = 1;
					$account['account_status'] = 2;
					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = 0;
					$this->db->insert('landing', $in_data_landing);

					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 88);
					}
					if (isset($value['C'])) {
						$this->save_event_1($acc, $id, $value['C'], 89);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $value['D'], 90);
					}
					if (isset($value['E'])) {
						$this->save_event_1($acc, $id, $value['E'], 91);
					}
				}
			} elseif ($id == 29) {
				if (isset($value['A'])) {
					/*
						if (isset($value['C'])) {
							$rfid = $value['C'];
							if (strlen($value['C']) < 10) {
								$total = 10 - strlen($value['C']);
								$nol = '';
								for ($i = 1; $i <= $total; $i++) {
									$nol .= '0';
								}
								$rfid = $nol . $rfid;
							}
							$account['account_rfid'] = $rfid;
						} else {
							$account['account_rfid'] = '';
						}
						$account['account_displayname'] = $value['A'];
						$account['account_username'] = $this->user_m->nicename($account['account_displayname']);
						$account['account_fest'] = 1;
						$account['account_status'] = 2;
						$this->db->insert('account', $account);

						$acc = $this->db->insert_id();

						$in_data_landing['account_id'] = $acc;
						$in_data_landing['landing_register_form'] = $id;
						$in_data_landing['landing_rfid'] = $account['account_rfid'];
						$this->db->insert('landing', $in_data_landing);

						if (isset($value['B'])) {
							$this->save_event_1($acc, $id, $value['B'], 64);
						}
						if (isset($value['C'])) {
							$this->save_event_1($acc, $id, $account['account_rfid'], 65);
						}
					*/
					//$account['name'] = $value['B'];
					// $account['gender'] = $value['C'];
					// $account['size'] = $value['D'];
					// $account['subdir'] = $value['E'];
					// $account['no_hp'] = $value['O'];
					// $account['email'] = $value['G'];
					// $account['line'] = $value['H'];
					// $account['twitter'] = $value['I'];
					// $account['instagram'] = $value['J'];
					if (isset($value['O'])) {
						$no_hp = $value['O'];
					} else {
						$no_hp = $value['B'];
					}
					if (isset($value['N'])) {
						$db_update['jabatan'] = $value['N'];
					} else {
						$db_update['jabatan'] = "";
					}

					$sqluser = "select id FROM users
	                            where no_hp like '%" . $no_hp . "%' or name like '%" . $no_hp . "%'";
					$queryuser = $this->db->query($sqluser);
					$datauser = $queryuser->row();
					if ($datauser) {
						print_r($db_update);
						$this->db->where('id', $datauser->id);
						$this->db->update('users', $db_update);
					}
					// $account['freetime'] = $freetime;
					// $this->db->insert('users', $account);

				}

			} else {
				if (isset($value['A'])) {
					$account['account_firstname'] = $value['A'];
					$account['account_lastname'] = $value['A'];
					$account['account_displayname'] = $value['A'];
					$account['account_username'] = $this->user_m->nicename($value['A']);
					$account['account_fest'] = 1;
					$account['account_status'] = 2;
					if (isset($value['E'])) {
						$rfid = $value['E'];
						if (strlen($value['E']) < 10) {
							$total = 10 - strlen($value['E']);
							$nol = '';
							for ($i = 1; $i <= $total; $i++) {
								$nol .= '0';
							}
							$rfid = $nol . $rfid;
						}
						$account['account_rfid'] = $rfid;
					} else {
						$account['account_rfid'] = '';
					}
					$this->db->insert('account', $account);

					$acc = $this->db->insert_id();

					$in_data_landing['account_id'] = $acc;
					$in_data_landing['landing_register_form'] = $id;
					$in_data_landing['landing_rfid'] = $account['account_rfid'];
					$this->db->insert('landing', $in_data_landing);

					if ($account['account_rfid'] != '') {
						$this->save_event_1($acc, $id, $account['account_rfid'], 22);
					}
					if (isset($value['B'])) {
						$this->save_event_1($acc, $id, $value['B'], 19);
					}
					if (isset($value['C'])) {
						$this->save_event_1($acc, $id, $value['C'], 20);
					}
					if (isset($value['D'])) {
						$this->save_event_1($acc, $id, $value['D'], 21);
					}
					if (isset($value['E'])) {
						$this->save_event_1($acc, $id, $value['E'], 23);
					}
				}

			}
		}

		if ($id == 42) {
			redirect('dashboard/user/eksport/uploadfoto/' . $id);
		} else {
			redirect('dashboard/user/home/main/' . $id);
		}
	}

	function uploadfoto($id) {
		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['parent'] = 'Add';
		$data['places_id'] = $id;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['content'] = $this->load->view('dashboard/user/eksportzip', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function datazip($id) {
		if (isset($_FILES['file']['name'][0]) && $_FILES['file']['name'][0] != '') {
			for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
				$_FILES['files']['name'] = $_FILES['file']['name'][$i];
				$_FILES['files']['type'] = $_FILES['file']['type'][$i];
				$_FILES['files']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
				$_FILES['files']['error'] = $_FILES['file']['error'][$i];
				$_FILES['files']['size'] = $_FILES['file']['size'][$i];
				$name = str_replace(' ', '_', $_FILES['files']['name']);
				$config = array(
					'upload_path' => FCPATH . "/uploads/user/",
					'upload_url' => base_url() . "uploads/user/",
					'allowed_types' => "gif|jpg|png|jpeg|pdf",
					'overwrite' => TRUE,
					'file_name' => $name,
				);
				$this->load->library('upload');
				$this->upload->initialize($config);
				$upload = $this->upload->do_upload('files');
			}
			redirect('dashboard/user/home/main/' . $id);
		}
	}

	function save_event_1($acc_id, $id, $value, $form) {
		$in_data_rfid['form_regis_id'] = $form;
		$in_data_rfid['account_id'] = $acc_id;
		$in_data_rfid['places_id'] = $id;
		$in_data_rfid['content'] = $value;
		$in_data_rfid['date_add'] = date('Y-m-d H:i:s');

		$rowcontent = $this->user_m->cek_content_data($acc_id, $id, $form);
		if ($rowcontent) {
			$this->db->where('id', $rowcontent->id);
			$this->db->update('form_regis_detail', $in_data_rfid);
		} else {
			$this->db->insert('form_regis_detail', $in_data_rfid);
		}
		return true;
	}

	function save_presenter_1($acc_id, $id, $value, $form, $urutan) {
		$in_data_rfid['account_id'] = $acc_id;
		$in_data_rfid['places_id'] = $id;
		$in_data_rfid['winner_id'] = $form;
		$in_data_rfid['presenter_status'] = $urutan;
		$in_data_rfid['cat_winner_id'] = $value;
		$in_data_rfid['data_status'] = 0;
		$in_data_rfid['date_add'] = date('Y-m-d H:i:s');

		$rowcontent = $this->user_m->cek_content_presenter($acc_id, $id, $value);
		if ($rowcontent) {
			$this->db->where('id', $rowcontent->id);
			$this->db->update('presenter_account', $in_data_rfid);
		} else {
			$this->db->insert('presenter_account', $in_data_rfid);
		}
		return true;
	}

	function cek_winner_content($content, $places_id) {
		$contentnew = explode(' (', $content);
		$idnumber = explode(')', $contentnew[1]);
		$winners['places_id'] = $places_id;
		$winners['nama'] = $contentnew[0];
		$winners['number'] = $idnumber[0];
		$winners['toogle'] = 0;
		$winners['date_add'] = date('Y-m-d H:i:s');
		$winners['data_status'] = 1;

		$rowcontent = $this->user_m->cek_winner($contentnew[0], $places_id);
		if ($rowcontent) {
			$new = $rowcontent->id;
		} else {
			$this->db->insert('winner', $winners);
			$new = $this->db->insert_id();
		}
		return $new;
	}

	function save_winner_1($acc_id, $id, $value, $form) {
		$in_data_rfid['account_id'] = $acc_id;
		$in_data_rfid['places_id'] = $id;
		$in_data_rfid['winner_id'] = $form;
		$in_data_rfid['data_status'] = 0;
		$in_data_rfid['date_add'] = date('Y-m-d H:i:s');

		$rowcontent = $this->user_m->cek_content_winner($acc_id, $id, $form);
		if ($rowcontent) {
			$this->db->where('id', $rowcontent->id);
			$this->db->update('winner_account', $in_data_rfid);
		} else {
			$this->db->insert('winner_account', $in_data_rfid);
		}
		return true;
	}

	function download($id) {
		$this->load->library('excel');
		$this->load->model('dashboard/visitor_m');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

		$objPHPExcel->setActiveSheetIndex(0);

		$tabel = $this->user_m->list_tabel($id);
		if ($id == 20) {
			$header = array('First Name', 'Last Name');
		} else {
			$header = array('Name');
		}
		foreach ($tabel as $tabelrow) {
			$header[] = $tabelrow->field_name;
		}
		/*
			        $tabelnew = $this->user_m->list_winner();
			        foreach($tabelnew as $rowsnew){
			            $header[] = $rowsnew->nama.' ('.$rowsnew->number.')';
		*/

		$places = $this->visitor_m->get_name_places($id);

		$header1 = array('example-name');
		foreach ($tabel as $tabelrow) {
			$header1[] = 'example-' . $tabelrow->field_name;
		}

		$col = 0;
		foreach ($header as $field) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			$col++;
		}
		// Fetching the table data
		$col = 0;
		foreach ($header as $data) {
			if ($data == 'access') {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 2, 'Main');
			} else {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 2, 'data');
			}
			$col++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		// Sending headers to force the user to download the file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="example-form-' . $places->places_name . '.xls"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}
}
