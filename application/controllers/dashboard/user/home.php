<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/user_m');
		$this->load->helper('form');

		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	public function index() {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['data'] = $this->user_m->list_user();

		$data['content'] = $this->load->view('dashboard/user/main', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	public function main($id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;
		$data['tabel'] = $this->user_m->list_tabel($id);
		$data['data'] = $this->user_m->list_user_event($id);
		$data['content'] = $this->load->view('dashboard/user/main', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function show($id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['data'] = $this->user_m->getuser($id);

		$data['content'] = $this->load->view('dashboard/user/show', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function delete($id = 0, $places_id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$datauser = $this->user_m->getuser($id, $places_id);

		$where['id'] = $id;
		$this->db->delete("account", $where);
		#$query = $this->db->query("DELETE FROM wooz_log_user_gate WHERE account_id = '$id'");
		$query = $this->db->query("DELETE FROM wooz_landing WHERE account_id = '$id' and landing_register_form = '$places_id'");
		$query = $this->db->query("DELETE FROM wooz_form_regis_detail WHERE account_id = '$id' and places_id = '$places_id'");

		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $places_id;

		$data['content'] = $this->load->view('dashboard/user/delete', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function deleteall() {
		#xdebug($this->input->post('places_id',true));die;
		$places_id = $this->input->post('places_id', true);
		$query = $this->db->query("DELETE a,b,c from wooz_account a
            inner join `wooz_form_regis_detail` b on b.account_id = a.id
            inner join wooz_landing c on c.account_id = a.id where places_id = '" . $places_id . "'");
		$query = $this->db->query("DELETE FROM wooz_log_user_gate WHERE places_id = '" . $places_id . "'");
		$query = $this->db->query("DELETE FROM wooz_winner_account WHERE places_id = '" . $places_id . "'");
		if ($places_id == 31) {
			// $query = $this->db->query("TRUNCATE wooz_presenter_account");
			$query = $this->db->query("DELETE FROM wooz_presenter_account WHERE places_id = '" . $places_id . "'");
			$query = $this->db->query("DELETE FROM wooz_winner WHERE places_id = '" . $places_id . "' and id != 16");
		}
		#$query = $this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6");

		if (!empty($ret)) {
			echo json_encode('ok');
		} else {
			echo json_encode('failed');
		}

	}

	function deletecheck() {
		#xdebug($this->input->post('places_id',true));die;
		$places_id = $this->input->post('places_id', true);
		$query = $this->db->query("DELETE FROM wooz_log_user_gate WHERE places_id = '" . $places_id . "'");
		#$query = $this->db->query("DELETE FROM wooz_winner_account WHERE places_id = '".$places_id."'");
		#$query = $this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6");
		if ($places_id == 34) {
			$query = $this->db->query("DELETE a FROM wooz_log a inner join wooz_places b on b.id = a.places_id WHERE b.places_parent = '" . $places_id . "'");
		}
		if (!empty($ret)) {
			echo json_encode('ok');
		} else {
			echo json_encode('failed');
		}

	}

}
