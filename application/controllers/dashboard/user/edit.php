<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class edit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/user_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function main($id,$places_id) {
        $data['datauser'] = $this->user_m->getuser($id,$places_id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['places_id'] = $places_id;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $form = $this->visitor_m->list_form_regis($places_id);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        foreach ($form as $row) {
            switch ($row->field_type) {
                case 'radio':
                    if($row->field_required == 1){
                        $this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
                    }else{
                        $this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
                    }
                    break;
                default:
                    if($row->field_required == 1){
                        $this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
                    }else{
                        $this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
                    }
                    break;
            }
        }
        $this->form_validation->set_rules('rfid', 'RFID', 'trim|callback_rfid_used[' . $id.'-'.$places_id . ']');

        if ($this->form_validation->run() === TRUE) {
            $in_data['account_displayname'] = $this->input->post('name', true);
            $in_data['account_username'] = $this->user_m->nicename($this->input->post('name', true));
            $in_data['account_rfid'] = $this->input->post('rfid', true);
            $in_data['account_status'] = 2;           
            $this->db->where('id', $id);
            $this->db->update('account', $in_data);
            $in_data_landing['landing_rfid'] = $in_data['account_rfid'];
            $this->db->where('id', $id);
            $this->db->where('landing_register_form', $places_id);
            $this->db->update('landing', $in_data_landing);

            foreach ($form as $row) {
                if($row->field_type == 'file'){
                    $header_post = '';
                    if($_FILES){
                        if($this->do_upload($row->field_nicename)){
                            $header_post = str_replace(' ', '_', $_FILES[$row->field_nicename]['name']);
                            $in_data_rfid['form_regis_id'] = $row->id;
                            $in_data_rfid['account_id'] = $id;
                            $in_data_rfid['places_id'] = $places_id;
                            $in_data_rfid['content'] = $header_post;
                            $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
                            $rowcontent = $this->user_m->cek_content_data($id,$places_id,$row->id);
                            if($rowcontent){
                                $this->db->where('id', $rowcontent->id);
                                $this->db->update('form_regis_detail', $in_data_rfid);
                            }else{
                                $this->db->insert('form_regis_detail', $in_data_rfid);
                            }
                        }
                    }
                }else{
                    $data_content = $this->input->post($row->field_nicename, true);
                    if($data_content){
                        $in_data_rfid['form_regis_id'] = $row->id;
                        $in_data_rfid['account_id'] = $id;
                        $in_data_rfid['places_id'] = $places_id;
                        $in_data_rfid['content'] = $data_content;
                        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
                        
                        $rowcontent = $this->user_m->cek_content_data($id,$places_id,$row->id);
                        if($rowcontent){
                            $this->db->where('id', $rowcontent->id);
                            $this->db->update('form_regis_detail', $in_data_rfid);
                        }else{
                            $this->db->insert('form_regis_detail', $in_data_rfid);
                        } 
                    }
                }
            }
            $data['content'] = $this->load->view('dashboard/user/success', $data, true);
        } else {
            $data['form'] = $form;
            $data['visitor'] = $this->visitor_m->list_visitor();
            $data['content'] = $this->load->view('dashboard/user/edit', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function do_upload($file){
        $name = str_replace(' ', '_', $_FILES[$file]['name']);
        $config =  array(
                  'upload_path'     => FCPATH."/uploads/user/",
                  'upload_url'      => base_url()."uploads/user/",
                  'allowed_types'   => "gif|jpg|png|jpeg",
                  'overwrite'       => TRUE,
                  'file_name'       => $name
                );

        $this->load->library('upload', $config);
        if($this->upload->do_upload($file))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function downloadfile($file) {
        $file_path = FCPATH . 'uploads/usermerchant/' . $file['name'];
        if (move_uploaded_file($file['tmp_name'], $file_path)) {
            return 1;
        } else {
            return 0;
        }
    }
    
    function rfid_used($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('rfid');
        }
        $pecah = explode('-',$account_id);
        $account_id = $pecah[0];
        $places_id = $pecah[1];

        $sqluser = "select a.id FROM wooz_account a inner join wooz_landing b on b.account_id = a.id
                    where account_rfid = '" . $str."' and landing_register_form = '".$places_id."'";
        $queryuser = $this->db->query($sqluser);
        $new = $queryuser->row();
        if (count($new) >= 1) {
            $sqluser1 = "select a.id FROM wooz_account a inner join wooz_landing b on b.account_id = a.id
                        where account_rfid = '" . $str."' and a.id = ".$account_id." and landing_register_form = '".$places_id."'";
            $queryuser1 = $this->db->query($sqluser1);
            $news = $queryuser1->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('rfid_used', 'RFID already registered');
                return false;
            }
        } else {
            return true;
        }
    }

}
