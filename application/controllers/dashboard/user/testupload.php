<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class testupload extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/user_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/eksporttest',$data, true);
        $this->load->view('dashboard/main',$data);
    }
    public function main($id) {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['places_id'] = $id;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/eksport',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function data(){
        //load the excel library
        $file = $_FILES['userfile']['tmp_name'];
        $this->load->library('excel');
         
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
         
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        
        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            //header will/should be in row 1 only. of course this can be modified to suit your need.
            $arr_data[$row][$column] = $data_value;
        } 
        $x = 1;
        foreach ($arr_data as $value) { 
            $account_email = $this->cekdatauser($value['B']);
            xdebug($account_email->account_id);
            xdebug($value['B']);
            $UNIX_DATE = ($value['F'] - 25569) * 86400;
            $timecheckin = gmdate("d-m-Y H:i:s", $UNIX_DATE);
            xdebug($timecheckin);
            $db['account_id'] = $account_email->account_id;
            $db['places_id'] = 21;
            $db['log_type'] = 1;
            $db['log_date'] = gmdate("Y-m-d", $UNIX_DATE);
            $db['log_time'] = gmdate("H:i:s", $UNIX_DATE);
            $db['log_gate'] = 23;
            $db['log_check'] = 1;
            $db['log_hash'] = 'v' . time() . '-' . $account_email->account_id;
            $db['log_status'] = 1;
            $db['log_stamps'] = gmdate("Y-m-d H:i:s", $UNIX_DATE);
            $upd = $this->db->insert('log_user_gate', $db);
            echo $x;
            $x++;
        }
    }

    function cekdatauser($email){
        $sql = "SELECT account_id FROM wooz_form_regis_detail where content = '".$email."' and form_regis_id = 30 and places_id = 21";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        return $hasil;
    }

    function uploadfoto($id){
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['places_id'] = $id;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/eksportzip',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function datazip($id){
        if(isset($_FILES['file']['name'][0]) && $_FILES['file']['name'][0] != ''){
            for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
                $_FILES['files']['name']= $_FILES['file']['name'][$i];
                $_FILES['files']['type']= $_FILES['file']['type'][$i];
                $_FILES['files']['tmp_name']= $_FILES['file']['tmp_name'][$i];
                $_FILES['files']['error']= $_FILES['file']['error'][$i];
                $_FILES['files']['size']= $_FILES['file']['size'][$i];   
                $name = str_replace(' ', '_', $_FILES['files']['name']); 
                $config =  array(
                      'upload_path'     => FCPATH."/uploads/user/",
                      'upload_url'      => base_url()."uploads/user/",
                      'allowed_types'   => "gif|jpg|png|jpeg|pdf",
                      'overwrite'       => TRUE,
                      'file_name'       => $name
                    );
                $this->load->library('upload');
                $this->upload->initialize($config);
                $upload = $this->upload->do_upload('files');
            }
            redirect('dashboard/user/home/main/'.$id);
        }
    }
    
    function save_event_1($acc_id,$id,$value,$form){
        $in_data_rfid['form_regis_id'] = $form;
        $in_data_rfid['account_id'] = $acc_id;
        $in_data_rfid['places_id'] = $id;
        $in_data_rfid['content'] = $value;
        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
        
        $rowcontent = $this->user_m->cek_content_data($acc_id,$id,$form);
        if($rowcontent){
            $this->db->where('id', $rowcontent->id);
            $this->db->update('form_regis_detail', $in_data_rfid);
        }else{
            $this->db->insert('form_regis_detail', $in_data_rfid);
        } 
        return true;
    }

    function save_presenter_1($acc_id,$id,$value,$form,$urutan){
        $in_data_rfid['account_id'] = $acc_id;
        $in_data_rfid['places_id'] = $id;
        $in_data_rfid['winner_id'] = $form;
        $in_data_rfid['presenter_status'] = $urutan;
        $in_data_rfid['cat_winner_id'] = $value;
        $in_data_rfid['data_status'] = 0;
        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
        
        $rowcontent = $this->user_m->cek_content_presenter($acc_id,$id,$value);
        if($rowcontent){
            $this->db->where('id', $rowcontent->id);
            $this->db->update('presenter_account', $in_data_rfid);
        }else{
            $this->db->insert('presenter_account', $in_data_rfid);
        } 
        return true;
    }

    function cek_winner_content($content){
        $contentnew = explode(' (', $content);
        $idnumber = explode(')', $contentnew[1]);
        $winners['places_id'] = 11;
        $winners['nama'] = $contentnew[0];
        $winners['number'] = $idnumber[0];
        $winners['toogle'] = 0;
        $winners['date_add'] = date('Y-m-d H:i:s');
        $winners['data_status'] = 1;
        $rowcontent = $this->user_m->cek_winner($contentnew[0]);
        if($rowcontent){
            $new = $rowcontent->id;
        }else{
            $this->db->insert('winner',$winners);
            $new = $this->db->insert_id();
        } 
        return $new;
    }

    function save_winner_1($acc_id,$id,$value,$form){
        $in_data_rfid['account_id'] = $acc_id;
        $in_data_rfid['places_id'] = $id;
        $in_data_rfid['winner_id'] = $form;
        $in_data_rfid['data_status'] = 0;
        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
        
        $rowcontent = $this->user_m->cek_content_winner($acc_id,$id,$form);
        if($rowcontent){
            $this->db->where('id', $rowcontent->id);
            $this->db->update('winner_account', $in_data_rfid);
        }else{
            $this->db->insert('winner_account', $in_data_rfid);
        } 
        return true;
    }

    function download($id){
        $this->load->library('excel');
        $this->load->model('dashboard/visitor_m');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        $objPHPExcel->setActiveSheetIndex(0);

        $tabel = $this->user_m->list_tabel($id);
        if($id == 20){
            $header = array('First Name','Last Name');
        }else{
            $header = array('Name');
        }
        foreach ($tabel as $tabelrow) {
            $header[] = $tabelrow->field_name;
        }
        /*
        $tabelnew = $this->user_m->list_winner();
        foreach($tabelnew as $rowsnew){
            $header[] = $rowsnew->nama.' ('.$rowsnew->number.')';
        } */

        $places = $this->visitor_m->get_name_places($id);

        $header1 = array('example-name');
        foreach ($tabel as $tabelrow) {
            $header1[] = 'example-'.$tabelrow->field_name;
        }

        $col = 0;
        foreach ($header as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
        // Fetching the table data
        $col = 0;
        foreach($header as $data)
        {
            if($data == 'access'){
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 2, 'Main');
            }else{
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 2, 'data');
            }
            $col++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="example-form-'.$places->places_name.'.xls"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }
}
