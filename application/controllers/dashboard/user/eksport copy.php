<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class eksport extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/user_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/eksport',$data, true);
        $this->load->view('dashboard/main',$data);
    }
    public function main($id) {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['places_id'] = $id;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/eksport',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function data($id){
        //load the excel library
        $file = $_FILES['userfile']['tmp_name'];
        $this->load->library('excel');
         
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
         
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        
        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            //header will/should be in row 1 only. of course this can be modified to suit your need.
            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        } 

        $keyheader = array_keys($header[1]);

        /*
        $az = 1;
        foreach($header[1] as $row){
            echo $az++;
            $account['places_id'] = 11;
            $account['field_name'] = strtolower($row);
            $account['field_nicename'] = str_replace(' ', '-', strtolower($row));
            $account['field_required'] = 0;
            $account['field_is_text'] = 1;
            $account['date_add'] = date('Y-m-d H:i:s');
            $account['status'] = 1;
            if($az > 10){
                $winners['places_id'] = 11;
                $winners['nama'] = $row;
                $winners['date_add'] = date('Y-m-d H:i:s');
                $winners['data_status'] = 1;
                $this->db->insert('winner',$winners);
                $new = $this->db->insert_id();
                echo $new.'-'.$row.'<br>';
            xdebug($winners);

            }
            #$this->db->insert('form_regis',$account);
        } */
        foreach ($arr_data as $value) { /*
            $account['account_firstname'] = $value['B'];
            $account['account_lastname'] = $value['A'];
            $account['account_displayname'] = $value['C'];
            $account['account_username'] = $this->user_m->nicename($value['C']);
            $account['account_fest'] = 1;
            $account['account_status'] = 2;
            if($id == 9){
                if(isset($value['B'])){
                    $rfid = $value['B'];
                    if(strlen($value['B']) < 10){
                        $total = 10 - strlen($value['B']);
                        $nol = '';
                        for($i=1;$i<=$total;$i++){
                            $nol .= '0';
                        }
                        $rfid = $nol.$rfid;
                    }
                    $account['account_rfid'] = $rfid;
                }else{
                    $account['account_rfid'] = '';
                }
                $this->db->insert('account',$account);
                $acc = $this->db->insert_id();

                $in_data_landing['account_id'] = $acc;
                $in_data_landing['landing_register_form'] = $id;
                $in_data_landing['landing_rfid'] = $account['account_rfid'];
                $this->db->insert('landing', $in_data_landing);
                
                if($account['account_rfid'] != ''){
                    $this->save_event_1($acc,$id,$account['account_rfid'],6);
                }
                if(isset($value['C'])){
                    $this->save_event_1($acc,$id,$value['C'],3);
                }
                if(isset($value['D'])){
                    $header_post = str_replace(' ', '_', $value['D']);
                    $this->save_event_1($acc,$id,$header_post,5);
                }
                if(isset($value['E'])){
                    $this->save_event_1($acc,$id,$value['E'],4);
                }
            }else{
                if(isset($value['I'])){
                    $rfid = $value['I'];
                    if(strlen($value['I']) < 10){
                        $total = 10 - strlen($value['I']);
                        $nol = '';
                        for($i=1;$i<=$total;$i++){
                            $nol .= '0';
                        }
                        $rfid = $nol.$rfid;
                    }
                    $account['account_rfid'] = $rfid;
                }else{
                    $account['account_rfid'] = '';
                }
                $this->db->insert('account',$account);
                $acc = $this->db->insert_id();

                $in_data_landing['account_id'] = $acc;
                $in_data_landing['landing_register_form'] = $id;
                $in_data_landing['landing_rfid'] = $account['account_rfid'];
                $this->db->insert('landing', $in_data_landing);
                
                if($account['account_rfid'] != ''){
                    $this->save_event_1($acc,$id,$account['account_rfid'],18);
                } /*
                if(isset($value['B'])){
                    $this->save_event_1($acc,$id,$value['B'],7);
                }
                if(isset($value['C'])){
                    $this->save_event_1($acc,$id,$value['C'],8);
                } 
                if(isset($value['D'])){
                    $this->save_event_1($acc,$id,$value['D'],13);
                }
                if(isset($value['E'])){
                    $this->save_event_1($acc,$id,$value['E'],14);
                }
                if(isset($value['F'])){
                    $this->save_event_1($acc,$id,$value['F'],15);
                }
                if(isset($value['G'])){
                    $this->save_event_1($acc,$id,$value['G'],16); 
                }
                if(isset($value['H'])){
                    if($value['H'] == 1){
                        $access = 'Main';
                    }elseif($value['H'] == 2){
                        $access = 'VIP';
                    }else{
                        $access = 'Both';
                    }
                    $this->save_event_1($acc,$id,$access,17); //ACCESS
                }
                if(isset($value['J']) && $value['J'] != 'NA'){
                    $presenter = explode(',', $value['J']);
                    foreach($presenter as $rowpre){
                        $hasilpresent = $rowpre + 16;
                        $this->save_presenter_1($acc,$id,$hasilpresent,16); 
                    }
                }
                for($i=10;$i<count($keyheader);$i++){
                    echo $keyheader[$i];
                    echo '<br>';
                }
                if(isset($value['K'])  && $value['K'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['K'],17); 
                }
                if(isset($value['L']) && $value['L'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['L'],18); 
                }
                if(isset($value['M']) && $value['M'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['M'],19); 
                }
                if(isset($value['N']) && $value['N'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['N'],20); 
                }
                if(isset($value['O']) && $value['O'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['O'],21); 
                }
                if(isset($value['P']) && $value['P'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['P'],22); 
                }
                if(isset($value['Q']) && $value['Q'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['Q'],23); 
                }
                if(isset($value['R']) && $value['R'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['R'],24); 
                }
                if(isset($value['S']) && $value['S'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['S'],25); 
                }
                if(isset($value['T']) && $value['T'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['T'],26); 
                }
                if(isset($value['U']) && $value['U'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['U'],27); 
                }
                if(isset($value['V']) && $value['V'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['V'],28); 
                }
                if(isset($value['W']) && $value['W'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['W'],29); 
                }
                if(isset($value['X']) && $value['X'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['X'],30); 
                }
                if(isset($value['Y']) && $value['Y'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['Y'],31); 
                }
                if(isset($value['Z']) && $value['Z'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['Z'],32); 
                }
                if(isset($value['AA']) && $value['AA'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AA'],33); 
                }
                if(isset($value['AB']) && $value['AB'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AB'],34); 
                }
                if(isset($value['AC']) && $value['AC'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AC'],35); 
                }
                if(isset($value['AD']) && $value['AD'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AD'],36); 
                }
                if(isset($value['AE']) && $value['AE'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AE'],37); 
                }
                if(isset($value['AF']) && $value['AF'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AF'],38); 
                }
                if(isset($value['AG']) && $value['AG'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AG'],39); 
                }
                if(isset($value['AH']) && $value['AH'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AH'],40); 
                }
                if(isset($value['AI']) && $value['AI'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AI'],41); 
                }
                if(isset($value['AJ']) && $value['AJ'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AJ'],42); 
                }
                if(isset($value['AK']) && $value['AK'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AK'],43); 
                }
                if(isset($value['AL']) && $value['AL'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AL'],44); 
                }
                if(isset($value['AM']) && $value['AM'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AM'],45); 
                }
                if(isset($value['AN']) && $value['AN'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AN'],46); 
                }
                if(isset($value['AO']) && $value['AO'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AO'],47); 
                }
                if(isset($value['AP']) && $value['AP'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AP'],48); 
                }
                if(isset($value['AQ']) && $value['AQ'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AQ'],49); 
                }
                if(isset($value['AR']) && $value['AR'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AR'],50); 
                }
                if(isset($value['AS']) && $value['AS'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AS'],51); 
                }
                if(isset($value['AT']) && $value['AT'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AT'],52); 
                }
                if(isset($value['AU']) && $value['AU'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AU'],53); 
                }
                if(isset($value['AV']) && $value['AV'] != 'NA'){
                    $this->save_winner_1($acc,$id,$value['AV'],54); 
                } */

                for($i=10;$i<count($keyheader);$i++){
                    echo $keyheader[$i];
                    echo '-';
                    if(isset($value[$keyheader[$i]])  && $value[$keyheader[$i]] != 'NA'){
                        echo $value[$keyheader[$i]];
                    }
                    echo '<br>';
                }
            #}
            #echo $value['D'].'-selesai<br>';
        }
        die;
        if($id == 9){
            redirect('dashboard/user/eksport/uploadfoto/'.$id);
        }else{
            redirect('dashboard/user/home/main/'.$id);
        }
    }

    function uploadfoto($id){
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['places_id'] = $id;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/eksportzip',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function datazip($id){
        if(isset($_FILES['file']['name'][0]) && $_FILES['file']['name'][0] != ''){
            for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
                $_FILES['files']['name']= $_FILES['file']['name'][$i];
                $_FILES['files']['type']= $_FILES['file']['type'][$i];
                $_FILES['files']['tmp_name']= $_FILES['file']['tmp_name'][$i];
                $_FILES['files']['error']= $_FILES['file']['error'][$i];
                $_FILES['files']['size']= $_FILES['file']['size'][$i];   
                $name = str_replace(' ', '_', $_FILES['files']['name']); 
                $config =  array(
                      'upload_path'     => FCPATH."/uploads/user/",
                      'upload_url'      => base_url()."uploads/user/",
                      'allowed_types'   => "gif|jpg|png|jpeg|pdf",
                      'overwrite'       => TRUE,
                      'file_name'       => $name
                    );
                $this->load->library('upload');
                $this->upload->initialize($config);
                $upload = $this->upload->do_upload('files');
            }
            redirect('dashboard/user/home/main/'.$id);
        }
    }
    
    function save_event_1($acc_id,$id,$value,$form){
        $in_data_rfid['form_regis_id'] = $form;
        $in_data_rfid['account_id'] = $acc_id;
        $in_data_rfid['places_id'] = $id;
        $in_data_rfid['content'] = $value;
        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
        
        $rowcontent = $this->user_m->cek_content_data($acc_id,$id,$form);
        if($rowcontent){
            $this->db->where('id', $rowcontent->id);
            $this->db->update('form_regis_detail', $in_data_rfid);
        }else{
            $this->db->insert('form_regis_detail', $in_data_rfid);
        } 
        return true;
    }

    function save_presenter_1($acc_id,$id,$value,$form){
        $in_data_rfid['account_id'] = $acc_id;
        $in_data_rfid['places_id'] = $id;
        $in_data_rfid['winner_id'] = $form;
        $in_data_rfid['cat_winner_id'] = $value;
        $in_data_rfid['data_status'] = 0;
        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
        
        $rowcontent = $this->user_m->cek_content_presenter($acc_id,$id,$value);
        if($rowcontent){
            $this->db->where('id', $rowcontent->id);
            $this->db->update('presenter_account', $in_data_rfid);
        }else{
            $this->db->insert('presenter_account', $in_data_rfid);
        } 
        return true;
    }

    function save_winner_1($acc_id,$id,$value,$form){
        $in_data_rfid['account_id'] = $acc_id;
        $in_data_rfid['places_id'] = $id;
        $in_data_rfid['winner_id'] = $form;
        $in_data_rfid['data_status'] = 0;
        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
        
        $rowcontent = $this->user_m->cek_content_winner($acc_id,$id,$form);
        if($rowcontent){
            $this->db->where('id', $rowcontent->id);
            $this->db->update('winner_account', $in_data_rfid);
        }else{
            $this->db->insert('winner_account', $in_data_rfid);
        } 
        return true;
    }

    function download($id){
        $this->load->library('excel');
        $this->load->model('dashboard/visitor_m');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        $objPHPExcel->setActiveSheetIndex(0);

        $tabel = $this->user_m->list_tabel($id);
        $header = array('name');
        foreach ($tabel as $tabelrow) {
            $header[] = $tabelrow->field_name;
        }
        $tabelnew = $this->user_m->list_winner();
        foreach($tabelnew as $rowsnew){
            $header[] = $rowsnew->nama;
        }

        $places = $this->visitor_m->get_name_places($id);

        $header1 = array('example-name');
        foreach ($tabel as $tabelrow) {
            $header1[] = 'example-'.$tabelrow->field_name;
        }

        $col = 0;
        foreach ($header as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
        // Fetching the table data
        $col = 0;
        foreach($header1 as $data)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 2, 'data');
            $col++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="example-form-'.$places->places_name.'.xls"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }
}
