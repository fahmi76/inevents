<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class eksport extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/user_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/eksport',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function data(){
        //load the excel library
        $file = $_FILES['userfile']['tmp_name'];
        $this->load->library('excel');
         
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);
         
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        
        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            //header will/should be in row 1 only. of course this can be modified to suit your need.
            if ($row == 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }
        
        foreach ($arr_data as $value) {
            $account['account_displayname'] = $value['D'];
            if(isset($value['T'])){
                $rfid = $value['T'];
                if(strlen($value['T']) < 10){
                    $total = 10 - strlen($value['T']);
                    $nol = '';
                    for($i=1;$i<=$total;$i++){
                        $nol .= '0';
                    }
                    $rfid = $nol.$rfid;
                }
                $account['account_rfid'] = $rfid;
            }else{
                $account['account_rfid'] = '';
            }
            $account['account_username'] = $this->user_m->nicename($value['D']);
            $this->db->insert('account',$account);
            $acc = $this->db->insert_id();

            if(isset($value['U']) && $value['U'] == 'winner'){
                $seat['visitor_id'] = 5;
            }else{
                $seat['visitor_id'] = '';
            }
            $seat['seat'] = $value['C'];
            $seat['account_id'] = $acc;
            $seat['data_status'] = 1;
            $seat['date_add'] = date('Y-m-d H:i:s');
            $this->db->insert('seat_account',$seat);

            $winex['account_id'] = $acc;
            $winex['data_status'] = 1;
            $winex['date_add'] = date('Y-m-d H:i:s');
            $win5 = $value['E'];

            if($win5 != 'NA'){
                $winex['winner_id'] = 1;
                $this->db->insert('winner_account',$winex);
            }
            $win6 = $value['F'];
            if($win6 != 'NA'){
                $winex['winner_id'] = 2;
                $this->db->insert('winner_account',$winex);
            }
            $win7 = $value['G'];
            if($win7 != 'NA'){
                $winex['winner_id'] = 3;
                $this->db->insert('winner_account',$winex);
            }
            $win8 = $value['H'];
            if($win8 != 'NA'){
                $winex['winner_id'] = 4;
                $this->db->insert('winner_account',$winex);
            }
            $win9 = $value['I'];
            if($win9 != 'NA'){
                $winex['winner_id'] = 5;
                $this->db->insert('winner_account',$winex);
            }
            $win10 = $value['J'];
            if($win10 != 'NA'){
                $winex['winner_id'] = 6;
                $this->db->insert('winner_account',$winex);
            }
            $win11 = $value['K'];
            if($win11 != 'NA'){
                $winex['winner_id'] = 7;
                $this->db->insert('winner_account',$winex);
            }
            $win12 = $value['L'];
            if($win12 != 'NA'){
                $winex['winner_id'] = 8;
                $this->db->insert('winner_account',$winex);
            }
            $win13 = $value['M'];
            if($win13 != 'NA'){
                $winex['winner_id'] = 9;
                $this->db->insert('winner_account',$winex);
            }
            $win14 = $value['N'];
            if($win14 != 'NA'){
                $winex['winner_id'] = 10;
                $this->db->insert('winner_account',$winex);
            }
            $win15 = $value['O'];
            if($win15 != 'NA'){
                $winex['winner_id'] = 11;
                $this->db->insert('winner_account',$winex);
            }
            $win16 = $value['P'];
            if($win16 != 'NA'){
                $winex['winner_id'] = 12;
                $this->db->insert('winner_account',$winex);
            }
            $win17 = $value['Q'];
            if($win17 != 'NA'){
                $winex['winner_id'] = 13;
                $this->db->insert('winner_account',$winex);
            }
            $win18 = $value['R'];
            if($win18 != 'NA'){
                $winex['winner_id'] = 14;
                $this->db->insert('winner_account',$winex);
            }
            $win19 = $value['S'];
            if($win19 != 'NA'){
                $winex['winner_id'] = 15;
                $this->db->insert('winner_account',$winex);
            }
            
            #echo $value['D'].'-selesai<br>';
        }
        redirect('dashboard/user');
    }

    function dataexcel(){
        include(APPPATH . 'third_party/excel_reader2.php');
        $data = new Spreadsheet_Excel_Reader($_FILES['userfile']['tmp_name']);

        $baris = $data->rowcount($sheet_index=0);
        // import data excel mulai baris ke-2 (karena baris pertama adalah nama kolom)
        /*
        for($x=5; $x<=19;$x++){
            $win[] = $data->val(1,$x);/*
            $win['nama'] = $data->val(1,$x);
            $win['data_status'] = 1;
            $win['date_add'] = date('Y-m-d H:i:s');
            $rowhasil = $this->db->get_where('winner', array('nama' => $win['nama']))->row();
            if($rowhasil){
                $this->db->where('id', $rowhasil->id);
                $this->db->update('winner', $win);
            }else{
                $this->db->insert('winner', $win);
            } 
        }
        xdebug($win);die;*/
        for ($i=2; $i<=$baris; $i++)
        {   
            $account['account_displayname'] = $data->val($i, 4);
            xdebug($account);die;
            $this->db->insert('account',$account);
            $acc = $this->db->insert_id();
            
            $seat['seat'] = $data->val($i, 3);
            $seat['account_id'] = $acc;
            $seat['data_status'] = 1;
            $seat['date_add'] = date('Y-m-d H:i:s');
            $this->db->insert('seat_account',$seat);
            
            $winex['account_id'] = $acc;
            $winex['data_status'] = 1;
            $winex['date_add'] = date('Y-m-d H:i:s');

            $win5 = $data->val($i,5);
            if($win5 != 'NA '){
                $winex['winner_id'] = 1;
                $this->db->insert('winner_account',$winex);
            }
            $win6 = $data->val($i,6);
            if($win6 != 'NA '){
                $winex['winner_id'] = 2;
                $this->db->insert('winner_account',$winex);
            }
            $win7 = $data->val($i,7);
            if($win7 != 'NA '){
                $winex['winner_id'] = 3;
                $this->db->insert('winner_account',$winex);
            }
            $win8 = $data->val($i,8);
            if($win8 != 'NA '){
                $winex['winner_id'] = 4;
                $this->db->insert('winner_account',$winex);
            }
            $win9 = $data->val($i,9);
            if($win9 != 'NA '){
                $winex['winner_id'] = 5;
                $this->db->insert('winner_account',$winex);
            }
            $win10 = $data->val($i,10);
            if($win10 != 'NA '){
                $winex['winner_id'] = 6;
                $this->db->insert('winner_account',$winex);
            }
            $win11 = $data->val($i,11);
            if($win11 != 'NA '){
                $winex['winner_id'] = 7;
                $this->db->insert('winner_account',$winex);
            }
            $win12 = $data->val($i,12);
            if($win12 != 'NA '){
                $winex['winner_id'] = 8;
                $this->db->insert('winner_account',$winex);
            }
            $win13 = $data->val($i,13);
            if($win13 != 'NA '){
                $winex['winner_id'] = 9;
                $this->db->insert('winner_account',$winex);
            }
            $win14 = $data->val($i,14);
            if($win14 != 'NA '){
                $winex['winner_id'] = 10;
                $this->db->insert('winner_account',$winex);
            }
            $win15 = $data->val($i,15);
            if($win15 != 'NA '){
                $winex['winner_id'] = 11;
                $this->db->insert('winner_account',$winex);
            }
            $win16 = $data->val($i,16);
            if($win16 != 'NA '){
                $winex['winner_id'] = 12;
                $this->db->insert('winner_account',$winex);
            }
            $win17 = $data->val($i,17);
            if($win17 != 'NA '){
                $winex['winner_id'] = 13;
                $this->db->insert('winner_account',$winex);
            }
            $win18 = $data->val($i,18);
            if($win18 != 'NA '){
                $winex['winner_id'] = 14;
                $this->db->insert('winner_account',$winex);
            }
            $win19 = $data->val($i,19);
            if($win19 != 'NA '){
                $winex['winner_id'] = 15;
                $this->db->insert('winner_account',$winex);
            }
            
            echo $data->val($i, 4).'-selesai<br>';
        }
        redirect('dashboard/user');
    }

}
