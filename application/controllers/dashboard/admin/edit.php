<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class edit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/admin_m');
        $this->load->helper('form');
        $this->load->library('fungsi');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            if($this->admin_group == 8){
                redirect('dashboard');
            }
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function main($id) {
        $data['datauser'] = $this->admin_m->getuser($id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['list_event'] = $this->admin_m->list_event();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_used[' . $id . ']');
        $this->form_validation->set_rules('event', 'List Event', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run() === TRUE) {
            $in_data['account_displayname'] = $this->input->post('name', true);
            $in_data['account_email'] = $this->input->post('email', true);
            $in_data['account_passwd'] = $this->fungsi->acak($this->input->post('password', true));
            $in_data['account_status'] = $this->input->post('status', true);
            //$in_data['account_group'] = $this->input->post('role', true);

            $event = $this->input->post('event');
            $levent = '(';
            $x = 1;
            foreach($event as $row){
                $levent .= $row;
                if($x == count($event)){
                    $levent .= ')';
                }else{
                    $levent .= ',';
                }
                $x++;
            }
            $in_data['account_event'] = $levent;
            $this->db->where('id', $id);
            $this->db->update('admin', $in_data);
            $data['content'] = $this->load->view('dashboard/admin/success', $data, true);
        } else {
            $data['content'] = $this->load->view('dashboard/admin/edit', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function email_used($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('email');
        }
        $new = $this->db->get_where('admin', array('account_email' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('admin', array('account_email' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('email_used', 'Registered email, please replace Email');
                return false;
            }
        } else {
            return true;
        }
    }

}
