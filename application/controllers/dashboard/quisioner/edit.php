<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class edit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/quisioner_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function main($id = 0,$places_id = 0) {
        if ($id == 0 || $places_id == 0) {
            redirect('dashboard');
        }
        $data['datauser'] = $this->quisioner_m->get_gate($id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $places_id;
        $data['places'] = $this->quisioner_m->get_name_places($places_id);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'question', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $in_data['question'] = $this->input->post('name', true);

            $this->db->where('id', $id);
            $this->db->update('quisioner_soal', $in_data);

            $data['content'] = $this->load->view('dashboard/quisioner/success', $data, true);
        } else {
            $data['content'] = $this->load->view('dashboard/quisioner/edit', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }
}
