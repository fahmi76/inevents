<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/quisioner_m');
		$this->load->helper('form');

		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	public function index() {
		$data['page'] = 'Home';
		$data['title'] = 'Quisioner';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['data'] = $this->quisioner_m->list_gate();

		$data['content'] = $this->load->view('dashboard/quisioner/home', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function main($id = 0, $survey = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$data['page'] = 'Home';
		$data['title'] = 'Quisioner';
		$data['places_id'] = $id;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['survey'] = $this->quisioner_m->get_title($survey);
		$data['places'] = $this->quisioner_m->get_name_places($id);
		if ($id == 38) {
			$data['data'] = $this->quisioner_m->list_survey($id, $survey);
		} else {
			$data['data'] = $this->quisioner_m->list_visitor($id);
		}
		$data['content'] = $this->load->view('dashboard/quisioner/main', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function load_data($id, $survey) {
		$data['places_id'] = $id;
		if ($id == 38) {
			$data['data'] = $this->quisioner_m->list_survey($id, $survey);
		} else {
			$data['data'] = $this->quisioner_m->list_visitor($id);
		}

		$this->load->view('dashboard/quisioner/main_data', $data);

	}

	function edit_question($id) {
		$data = $this->quisioner_m->get_gate($id);
		$data->name = $data->question; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function question_add() {
		$this->_validate_question();
		$in_data['question'] = $this->input->post('name', true);
		$in_data['survey_id'] = $this->input->post('survey');
		$in_data['places_id'] = $this->input->post('id');
		$in_data['data_status'] = 1;
		$in_data['date_add'] = date('Y-m-d H:i:s');

		$this->db->insert('quisioner_soal', $in_data);
		echo json_encode(array("status" => TRUE));
	}

	public function question_update() {
		$this->_validate_question();
		$in_data['question'] = $this->input->post('name', true);

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('quisioner_soal', $in_data);
		echo json_encode(array("status" => TRUE));
	}

	function edit_answer($id) {
		$data = $this->quisioner_m->get_answer($id);
		$data->answer = $data->answer; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function answer_add() {
		$this->_validate_answer();
		$in_data['answer'] = $this->input->post('answer', true);
		$in_data['question_id'] = $this->input->post('id');
		if ($this->input->post('status', true)) {
			$in_data['data_status'] = 1;
		} else {
			$in_data['data_status'] = 0;
		}
		$in_data['date_add'] = date('Y-m-d H:i:s');

		$this->db->insert('quisioner_answer', $in_data);
		echo json_encode(array("status" => TRUE));
	}

	public function answer_update() {
		$this->_validate_answer();
		$in_data['answer'] = $this->input->post('answer', true);
		if ($this->input->post('status', true)) {
			$in_data['data_status'] = 1;
		} else {
			$in_data['data_status'] = 0;
		}
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('quisioner_answer', $in_data);
		echo json_encode(array("status" => TRUE));
	}

	function survey($id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$data['data'] = $this->quisioner_m->get_list_setting($id);
		$data['page'] = 'Home';
		$data['title'] = 'Quisioner';
		$data['parent'] = 'Add';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;
		$data['places'] = $this->quisioner_m->get_name_places($id);

		$data['content'] = $this->load->view('dashboard/quisioner/list', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function setting($id = 0, $survey = 0) {
		if ($id == 0 && $survey == 0) {
			redirect('dashboard');
		}
		$data['datauser'] = $this->quisioner_m->get_setting($survey);
		$data['page'] = 'Home';
		$data['title'] = 'Quisioner';
		$data['parent'] = 'Add';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;
		$data['places'] = $this->quisioner_m->get_name_places($id);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('fourth', 'Message finish page', 'trim|required');
		$this->form_validation->set_rules('third', 'Title finish page', 'trim|required');
		$this->form_validation->set_rules('second', 'Title second page', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
			$in_data['title'] = $this->input->post('first', true);
			$in_data['question_order'] = $this->input->post('second', true);
			$in_data['question_finish'] = $this->input->post('third', true);
			$in_data['question_message'] = $this->input->post('fourth', true);
			$in_data['status'] = $this->input->post('five', true);

			$this->db->where('id', $survey);
			$this->db->update('quisioner_setting', $in_data);

			$data['content'] = $this->load->view('dashboard/quisioner/success', $data, true);
		} else {
			$data['content'] = $this->load->view('dashboard/quisioner/editsetting', $data, true);
		}
		$this->load->view('dashboard/main', $data);

	}

	function add($id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$data['page'] = 'Home';
		$data['title'] = 'Quisioner';
		$data['parent'] = 'Add';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;
		$data['places'] = $this->quisioner_m->get_name_places($id);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('fourth', 'Message finish page', 'trim|required');
		$this->form_validation->set_rules('third', 'Title finish page', 'trim|required');
		$this->form_validation->set_rules('second', 'Title second page', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
			$in_data['question_task'] = "'Press to Start' button at bottom of screen";
			$in_data['question_tombol'] = "START";
			$in_data['title'] = $this->input->post('first', true);
			$in_data['question_order'] = $this->input->post('second', true);
			$in_data['question_finish'] = $this->input->post('third', true);
			$in_data['question_message'] = $this->input->post('fourth', true);
			$in_data['status'] = $this->input->post('five', true);
			$in_data['places_id'] = $id;

			$this->db->insert('quisioner_setting', $in_data);
			// $this->db->where('places_id', $id);
			// $this->db->update('quisioner_setting', $in_data);

			$data['content'] = $this->load->view('dashboard/quisioner/success', $data, true);
		} else {
			$data['content'] = $this->load->view('dashboard/quisioner/addsetting', $data, true);
		}
		$this->load->view('dashboard/main', $data);

	}
	function show($id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['data'] = $this->user_m->getuser($id);

		$data['content'] = $this->load->view('dashboard/user/show', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function delete($id = 0, $survey = 0) {
		$query = $this->db->query("DELETE a,b,c from wooz_quisioner_setting a inner join `wooz_quisioner_soal` b on b.survey_id = a.id inner join wooz_quisioner_answer c on c.question_id = a.id where b.survey_id = '" . $survey . "'");
		redirect('dashboard/quisioner/home/survey/' . $id);
	}

	function delete_places() {
		$id = $this->input->post('places_id');
		$wheres['question_id'] = $id;
		$this->db->delete("quisioner_answer", $wheres);
		$where['id'] = $id;
		$this->db->delete("quisioner_soal", $where);
		if (!empty($ret)) {
			echo json_encode('ok');
		} else {
			echo json_encode('failed');
		}

	}

	function delete_answer() {
		$id = $this->input->post('places_id');
		$where['id'] = $id;
		$this->db->delete("quisioner_answer", $where);
		if (!empty($ret)) {
			echo json_encode('ok');
		} else {
			echo json_encode('failed');
		}

	}

	function delete_quisonerdata() {
		$id = $this->input->post('places_id');
		$where['places_id'] = $id;
		$this->db->delete("quisioner", $where);
		$this->db->delete("quisioner_account", $where);
		if (!empty($ret)) {
			echo json_encode('ok');
		} else {
			echo json_encode('failed');
		}

	}

	private function _validate_question() {
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if ($this->input->post('name') == '') {
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'question is required';
			$data['status'] = FALSE;
		}

		if ($data['status'] === FALSE) {
			echo json_encode($data);
			exit();
		}
	}

	private function _validate_answer() {
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if ($this->input->post('answer') == '') {
			$data['inputerror'][] = 'answer';
			$data['error_string'][] = 'answer is required';
			$data['status'] = FALSE;
		}

		if ($data['status'] === FALSE) {
			echo json_encode($data);
			exit();
		}
	}

}
