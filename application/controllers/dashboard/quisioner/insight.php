<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class insight extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/quisioner_m');
		$this->load->helper('form');

		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	function hourchartactivity($gate, $id) {
		$actvhour = $this->quisioner_m->check_hour($gate, $id);
		$hours = array();
		foreach ($actvhour as $row) {
			if ($row->thedate == 0) {
				$row->thedate = 24;
			}
			#$hours[]['date']=$row->thedate;
			#$hours[]['total']=$row->total;
			$hours[] = array(
				'date' => $row->thedate,
				'total' => $row->total,
			);
		}
		sort($hours);
		return $hours;
	}

	function main($id) {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboards';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;

		$data['count_all'] = $this->quisioner_m->count_list_all_gate($id);
		$gate = $this->quisioner_m->list_visitor($id);
		foreach ($gate as $row) {
			$data['checkin'][] = array(
				'id' => $row->id,
				'nama' => $row->question,
				'total' => $this->quisioner_m->logcheckinstaff($row->id),
			);
			$data['grafikcheckin'][] = array(
				'nama' => $row->question,
				'data' => $this->piereport($row->id, $id),
			);
		}

		$data['content'] = $this->load->view('dashboard/quisioner/insight', $data, true);
		$this->load->view('dashboard/main', $data);

	}

	function piereport($question, $places_id) {
		$actvhour = $this->quisioner_m->list_answer($question);
		$hours = array();
		foreach ($actvhour as $row) {
			$total = $this->quisioner_m->count_answer($places_id, $question, $row->id);
			$hours[] = array(
				'date' => $row->answer,
				'total' => $total->total,
			);
		}
		return $hours;

	}

	public function mainlist($gate, $check, $status, $id) {
		if ($check == 1) {
			$data['from'] = 1;
		} elseif ($check == 2) {
			$data['from'] = 2;
		}{
			$data['from'] = 3;
		}
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard - Insight';
		if ($gate == 11) {
			$data['check'] = 'All';
		} else {
			$data['check'] = $this->insight_m->get_gate($gate);
		}
		$data['status'] = $status;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $id;
		$data['gate'] = $gate;
		$data['checkid'] = $check;

		$data['tabel'] = $this->insight_m->list_tabel($id);

		$data['content'] = $this->load->view('dashboard/insight/listcheck', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	public function ajax_list($gate, $check, $status, $id) {
		$list = $this->main_m->get_datatables($gate, $check, $status, $id);
		$data = array();
		$no = $_POST['start'];
		$tabel = $this->insight_m->list_tabel($id);
		foreach ($list as $person) {
			$no++;
			$row = array();
			if ($person->log_fb_places == 1) {
				$row[] = $person->account_displayname . ' (GUEST)';
			} else {
				$row[] = $person->account_displayname;
			}
			foreach ($tabel as $rowtabel) {
				$datas = $this->insight_m->ambil_content_data($person->account_id, $id, $rowtabel->id);
				if ($datas) {
					$row[] = $datas->content;
				} else {
					$row[] = '';
				}
			}
			if ($person->log_check_out == 2) {
				$row[] = 'Check-out';
			} else {
				$row[] = 'Currently Check-in';
			}
			$row[] = $person->log_stamps;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->main_m->count_all($gate, $check, $status, $id),
			"recordsFiltered" => $this->main_m->count_filtered($gate, $check, $status, $id),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	function listall($id, $places_id = 0) {
		$data['from'] = 3;
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = 'All';
		$data['status'] = 3;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $places_id;
		$data['id'] = $id;

		$data['data'] = $this->quisioner_m->list_all_gate($id, $places_id);

		$data['content'] = $this->load->view('dashboard/quisioner/listall', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function user($status, $id, $places_id = 0) {
		$data['from'] = 3;
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['check'] = 'All';
		$data['status'] = 3;
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['places_id'] = $places_id;
		$data['id'] = $id;
		$data['status'] = $status;

		$data['data'] = $this->quisioner_m->get_user_answer($places_id, $status);

		$data['content'] = $this->load->view('dashboard/quisioner/listalluser', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function userwin($status, $id, $places_id = 0, $account_id) {

		$in_data['account_status'] = 2;

		$this->db->where('id', $account_id);
		$this->db->update('quisioner_account', $in_data);
		redirect('dashboard/quisioner/insight/user/' . $status . '/' . $id . '/' . $places_id);
	}

	function download($id, $places_id = 0) {
		$data['question'] = $this->quisioner_m->get_gate($places_id);
		$data['data'] = $this->quisioner_m->list_all_gate($id, $places_id);
		$data['places_id'] = $places_id;
		$data['id'] = $id;
		$this->load->view('dashboard/quisioner/excelregis', $data);

	}

	function delete($id, $gate, $places_id) {
		switch ($id) {
		case 1:
			$this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '" . $gate . "' and log_check = 1");
			break;
		case 2:
			$this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '" . $gate . "'  and log_check = 2");
			break;
		case 3:
			$this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = '" . $gate . "' ");
			break;
		case 4:
			$this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6 and landing_from_regis = 0");
			break;
		case 5:
			$this->db->query("DELETE FROM wooz_photos WHERE places_id = 7");
			break;
		case 6:
			$this->db->query("DELETE FROM wooz_photos WHERE places_id = 7");
			break;
		case 7:
			$this->db->query("TRUNCATE TABLE wooz_tagging");
			break;
		case 8:
			$this->db->query("DELETE FROM wooz_log_user_gate WHERE log_gate = 14 and log_check = 1 and log_status = 2");
			break;
		case 9:
			$this->db->query("DELETE FROM wooz_landing WHERE landing_register_form = 6 and landing_from_regis = 2");
			break;
		default:
			break;
		}
		redirect('dashboard/insight/home/main/' . $places_id);

	}

	function downloadgraph($id) {
		$file = 'data-capture.png';
		$post_file = $_POST['img_val' . $id];
		$dir = FCPATH . 'uploads/grafik/' . $file;
		//Get the base-64 stgring from data
		$filteredData = substr($post_file, strpos($post_file, ",") + 1);

		//Decode the string
		$unencodedData = base64_decode($filteredData);

		//Save the image
		file_put_contents($dir, $unencodedData);
		header('Pragma: public');
		header('Cache-Control: public, no-cache');
		header('Content-Type: application/octet-stream');
		header('Content-Length: ' . filesize($dir));
		header('Content-Disposition: attachment; filename="' . basename($dir) . '"');
		header('Content-Transfer-Encoding: binary');

		readfile($dir);
	}

	function grafikall($id) {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$family = $this->quisioner_m->list_visitor($id);
		$placesdata = $this->quisioner_m->get_name_places($id);

		$this->load->library('form_validation');
		if ($this->input->post('sreg1') != '') {
			$this->form_validation->set_rules('sreg1', 'Start Date', 'trim|required');
		} elseif ($this->input->post('sreg2') != '') {
			$this->form_validation->set_rules('sreg2', 'End Date', 'trim|required');
		} else {
			$this->form_validation->set_rules('from', 'from', 'trim|required');
		}
		if ($this->form_validation->run() === TRUE) {
			$sreg1 = $this->input->post('sreg1');
			$sreg2 = $this->input->post('sreg2');
			$sreg1n = date('Y-m-d', strtotime($sreg1));
			$sreg2n = date('Y-m-d', strtotime($sreg2));
			$awal = strtotime($sreg1n);
			$akhir = strtotime($sreg2n);
		} else {
			$sreg1n = $placesdata->places_startdate;
			$sreg2n = $placesdata->places_duedate;
			$awal = strtotime($placesdata->places_startdate);
			$akhir = strtotime($placesdata->places_duedate);
		}
		$data['sdate'] = date("m/d/Y", $awal);
		$data['edate'] = date("m/d/Y", $akhir);
		foreach ($family as $row) {
			$data['data'][] = array(
				'name' => $row->question,
				'chartregisday' => $this->datechartactivity($id, $row->id, $sreg1n, $sreg2n, $row->question),
				'chartregishour' => $this->hourchartactivityq($id, $row->id, $sreg1n, $sreg2n, $row->question),
				'chartregdayname' => $this->daynamechartactivity($id, $row->id, $sreg1n, $sreg2n, $row->question),
			);
		}
		$data['content'] = $this->load->view('dashboard/quisioner/grafikregis_v', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function datechartactivity($places, $question, $sdate, $edate, $ques) {
		$xaxis = 'Date';
		$yaxis = 'Total Answer question ' . $ques;
		//grafik total activity per day
		$actvday = $this->quisioner_m->dateregday($places, $question, $sdate, $edate);
		$dates = array();
		foreach ($actvday as $row) {
			#$chart->xAxis->categories[] = date('j F Y', strtotime($row->thedate));
			$dates[] = $row->thedate;
		}
		$dateactday['axis']['categories'] = array_values(array_unique($dates));
		$family = $this->quisioner_m->list_answer($question);
		foreach ($family as $rowplaces) {
			//$name = $this->quisioner_m->names($rowplaces,'places','places_name');
			$totals = array();
			foreach ($dateactday['axis']['categories'] as $rowdate) {
				$total = $this->quisioner_m->dateregtotal($rowplaces->id, $rowdate, 'answer_id');
				if ($total) {
					$totals[] = (int) $total->total;
				} else {
					$totals[] = (int) 0;
				}
			}
			$dateactchart[] = array('name' => $rowplaces->answer, 'data' => $totals);
		}
		$chartactvday = $this->chart("Total Answer Per Date", $xaxis, $yaxis, "chartactvday" . $question, $dateactday, $dateactchart);
		//end
		return $chartactvday;
	}

	function hourchartactivityq($places, $question, $sdate, $edate, $ques) {
		$xaxis = 'Date';
		$yaxis = 'Total Answer question ' . $ques;
		//grafik total activity per hour
		$actvhour = $this->quisioner_m->reghour($places, $question, $sdate, $edate);
		$hours = array();
		$acthour = array();
		foreach ($actvhour as $row) {
			$hours[] = $row->thedate;
		}
		$thedates1[] = array_values(array_unique($hours));
		foreach ($thedates1[0] as $row) {
			$acthour[] = $row;
		}
		$dateacthour['axis']['categories'] = $acthour;
		$family = $this->quisioner_m->list_answer($question);
		foreach ($family as $rowplaces) {
			$totals1 = array();
			foreach ($thedates1[0] as $rowdate) {
				$total = $this->quisioner_m->hourregtotal($rowplaces->id, $rowdate, 'answer_id');
				if ($total) {
					$totals1[] = (int) $total->total;
				} else {
					$totals1[] = (int) 0;
				}
			}
			$houractchart[] = array('name' => $rowplaces->answer, 'data' => $totals1);
		}
		$chartactvhour = $this->chart("Total Answer Per Hour", $xaxis, $yaxis, "chartactvhour" . $question, $dateacthour, $houractchart);
		//end
		return $chartactvhour;
	}

	function daynamechartactivity($places, $question, $sdate, $edate, $ques) {
		$xaxis = 'Date';
		$yaxis = 'Total User';
		//grafik total activity per day
		$actvdayname = $this->quisioner_m->regdayname($places, $question, $sdate, $edate);
		$datesname = array();
		$chartdayname = array();
		foreach ($actvdayname as $row) {
			$datesname[] = $row->thedate;
		}
		$thedates2[] = array_values(array_unique($datesname));
		foreach ($thedates2[0] as $row) {
			$chartdayname[] = $row;
		}
		$dateactdayname['axis']['categories'] = $chartdayname;
		$family = $this->quisioner_m->list_answer($question);
		foreach ($family as $rowplaces) {
			$totals = array();
			foreach ($thedates2[0] as $rowdate) {
				$total = $this->quisioner_m->daynameregtotal($rowplaces->id, $rowdate, 'answer_id');
				if ($total) {
					$totals[] = (int) $total->total;
				} else {
					$totals[] = (int) 0;
				}
			}
			$daynameactchart[] = array('name' => $rowplaces->answer, 'data' => $totals);
		}
		$chartactvdayname = $this->chart("Total Answer Per Day", $xaxis, $yaxis, "chartactvdayname" . $question, $dateactdayname, $daynameactchart);
		//end
		return $chartactvdayname;
	}

	function chart($title, $xaxis, $yaxis, $div, $actvday, $datachart) {
		//grafik total spot per day
		$this->load->library('highcharts');

		$this->highcharts->set_type('column'); // drauwing type
		$this->highcharts->set_title($title); // set chart title: title, subtitle(optional)
		$this->highcharts->set_axis_titles($xaxis, $yaxis); // axis titles: x axis,  y axis

		$this->highcharts->set_xAxis($actvday['axis']); // pushing categories for x axis labels
		foreach ($datachart as $chart) {
			$this->highcharts->set_serie($chart);
		}
		$credits = new stdClass();

		$credits->href = 'http://wooz.in/';
		$credits->text = "wooz.in";
		$this->highcharts->set_credits($credits);

		$this->highcharts->render_to($div); // choose a specific div to render to graph

		$chartgrafik = $this->highcharts->render(); // we render js and div in same time
		return $chartgrafik;
		//end
	}

}