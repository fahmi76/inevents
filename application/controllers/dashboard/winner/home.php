<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Port_of_Spain');
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/winner_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    public function main($id) {
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['id'] = $id; 
        $data['data'] = $this->winner_m->list_category($id);
        
        $data['content'] = $this->load->view('dashboard/winner/main',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    public function mainlist($places_id,$id)
    {
        $data['page'] = 'Home';
        $data['title'] = 'Winner';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['pages'] = 1;
        $data['id'] = $id;
        $data['places_id'] = $places_id;
        
        $data['content'] = $this->load->view('dashboard/winner/listwinner',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    public function ajax_list($places_id,$id)
    {
        $list = $this->winner_m->get_datatables($places_id,$id);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $person) {
            $no++;
            $row = array();
            $row[] = $person->account_displayname;
            $row[] = $person->nama;
            $getstatus = $this->winner_m->get_statususer($person->account_id);
            if($getstatus){
                if($getstatus->log_check_out == 2){
                    if($getstatus->log_status_check == 1){
                        $row[] = '<small class=" text-muted label label-warning">temporary check out</small>';
                    }else{
                        $row[] = '<small class=" text-muted label label-important">permanent check out</small>';
                    }
                }else{
                    if($person->data_status == 2){
                        if($getstatus->log_stamps <= $person->datelock){
                            $row[] = '<small class=" text-muted label label-success">checked in</small>';
                        }else{
                            $row[] = '<small class=" text-muted label label-info">Arrived after lock</small>';
                        }
                    }else{
                        $row[] = '<small class=" text-muted label label-success">checked in</small>';
                    }
                }
                $row[] = $getstatus->log_stamps;
            }else{
                $row[] = '<small class="text-muted label label-primary">none</small>';
                $row[] = '0000-00-00 00:00:00';
            }
            //add html for action
            $row[] = '<a class="btn btn-info" href="javascript:void()" title="Edit" onclick="edit_person('."'".$person->id."'".')"><i class="halflings-icon white edit"></i> Edit</a>
                  <a class="btn btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$person->id."'".')"><i class="halflings-icon white trash"></i> Delete</a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->winner_m->count_all($places_id,$id),
                        "recordsFiltered" => $this->winner_m->count_filtered($places_id,$id),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function lock($places_id,$id,$lock){
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['pages'] = 1;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;

        $in_data['data_status'] = $lock;
        $in_data['datelock'] = date('Y-m-d H:i:s');

        $this->db->where('id', $id);
        $this->db->update('winner', $in_data);
        
        $data['content'] = $this->load->view('dashboard/winner/success', $data, true);
        $this->load->view('dashboard/main',$data);
    }

    function presenter($places_id){
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['pages'] = 2;
        $data['data'] = $this->winner_m->list_presenter();
        $data['places_id'] = $places_id;
        
        $data['content'] = $this->load->view('dashboard/winner/mainlist',$data, true);
        $this->load->view('dashboard/main',$data);

    }

    function cektoogle($places_id=0,$id = 0,$toogle = 0){
        if ($id == 0) {
            redirect('dashboard');
        }
        $where['toogle'] = $toogle;
        $this->db->where('id', $id);
        $this->db->update('winner', $where);
        
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['pages'] = 1;
        
        $data['content'] = $this->load->view('dashboard/winner/success', $data, true);
        $this->load->view('dashboard/main', $data);
    }

    function show($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['data'] = $this->user_m->getuser($id);

        $data['content'] = $this->load->view('dashboard/winner/show', $data, true);
        $this->load->view('dashboard/main', $data);
    }

    function deletecat($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $where['id'] = $id;
        $this->db->delete("winner", $where);
        
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['pages'] = 1;
        
        $data['content'] = $this->load->view('dashboard/winner/delete', $data, true);
        $this->load->view('dashboard/main', $data);
    }

    function delete($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $where['id'] = $id;
        $this->db->delete("winner_account", $where);
        
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['pages'] = 1;
        
        $data['content'] = $this->load->view('dashboard/winner/delete', $data, true);
        $this->load->view('dashboard/main', $data);
    }

    function deletepresenter($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $where['id'] = $id;
        $this->db->delete("presenter_account", $where);
        
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['pages'] = 2;
        
        $data['content'] = $this->load->view('dashboard/winner/delete', $data, true);
        $this->load->view('dashboard/main', $data);
    }
    
    function search($places) {

        // process posted form data (the requested items like province)
        $keyword = $this->input->post('term');
        $data['response'] = 'false'; //Set default response
        $sql = "SELECT a.id,a.account_displayname as hasil FROM wooz_account as a inner join wooz_landing b on b.account_id = a.id
            WHERE a.account_displayname like '%" . $keyword . "%' and b.landing_register_form = 11";
        $query = $this->db->query($sql);
        $dataquery = $query->result();

        if (!empty($dataquery)) {
            $data['response'] = 'true'; //Set response
            $data['message'] = array(); //Create array
            foreach ($dataquery as $row) {
                $data['message'][] = array(
                    'value' => $row->hasil,
                    'id' => $row->id
                );  //Add a row to array
            }
        }
        if ('IS_AJAX') {
            echo json_encode($data); //echo json string if ajax request
        } else {
            $this->load->view('dashboard/show', $data); //Load html view of search results
        }
    }

    function download($places_id,$id){
        $this->load->model('dashboard/insight_m');
        $data['places_id'] = '11';
        $data['pages'] = $id;
        switch ($id) {
            case 1:
                $data['title'] = 'List-User-Winner';
                $data['status'] = 'Winner';
                $data['name'] = $this->winner_m->cek_winner($places_id);
                #$data['tabel'] = $this->insight_m->list_tabel(11);
                $data['user'] = $this->winner_m->list_visitor_cat(11,$places_id);
                break;
            default:
                $data['title'] = 'List-User-Presenter';
                $data['status'] = 'Presenter';
                #$data['tabel'] = $this->insight_m->list_tabel(11);
                $data['user'] = $this->winner_m->list_presenter();
                break;
        }
        $data['id'] = $id; 
        $this->load->view('dashboard/winner/excel', $data);

    }
}
