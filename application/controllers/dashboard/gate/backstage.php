<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class backstage extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = 'Success';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->visitor_m->list_backstage();
        
        $data['content'] = $this->load->view('dashboard/gate/list',$data, true);
        $this->load->view('dashboard/main',$data);
    }
	
    public function failed() {
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = 'Failed';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->visitor_m->list_backstage_fail();
        
        $data['content'] = $this->load->view('dashboard/gate/list',$data, true);
        $this->load->view('dashboard/main',$data);
    }


}
