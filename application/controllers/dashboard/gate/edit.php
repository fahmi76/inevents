<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class edit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function main($id = 0,$places_id = 0) {
        if ($id == 0 || $places_id == 0) {
            redirect('dashboard');
        }
        $data['datauser'] = $this->visitor_m->get_gate($id);
        $data['dataemail'] = $this->visitor_m->get_gate_email($id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['places_id'] = $places_id;
        $data['places'] = $this->visitor_m->get_name_places($places_id);

        $this->load->library('form_validation');
        $cekid = $id.'-'.$places_id;
        $this->form_validation->set_rules('name', 'Name Gate', 'trim|required|callback_visitor_used('.$cekid.')');

        if ($this->form_validation->run() === TRUE) {
            $in_data['nama'] = $this->input->post('name', true);

            $this->db->where('id', $id);
            $this->db->update('gate', $in_data);
            if($places_id == 21){
                $in_data_email['nama'] = $this->input->post('nameemail', true);
                $in_data_email['subject'] = $this->input->post('subject', true);
                $in_data_email['content'] = $this->input->post('content', true);
                $in_data_email['places_id'] = $places_id;
                $in_data_email['gate_id'] = $id;
                $uploadfile = $this->do_upload();
                if($uploadfile['status']){
                    $header_post = str_replace(' ', '_', $_FILES["file"]['name']);
                    $in_data_email['attachment'] = '/uploads/file/'.$header_post;
                }

                if($data['dataemail']){
                    $this->db->where('id', $data['dataemail']->id);
                    $this->db->update('gate_email', $in_data_email);
                }else{
                    $this->db->insert('gate_email', $in_data_email);
                }
            }
            $data['content'] = $this->load->view('dashboard/gate/success', $data, true);
        } else {
            $data['content'] = $this->load->view('dashboard/gate/edit', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function visitor_used($str,$id) {
        $cek = explode('-', $id);
        $gate_id = $cek[0];
        $places_id = $cek[1];
        if (!isset($str)) {
            $str = $this->input->post('name');
        }
        $new = $this->db->get_where('gate', array('nama' => $str,'places_id' => $places_id))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('gate', array('nama' => $str, 'id' => $gate_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('visitor_used', 'Gate already registered');
                return false;
            }
        } else {
            return true;
        }
    }

    function do_upload(){
        $name = str_replace(' ', '_', $_FILES["file"]['name']);
        $config =  array(
                  'upload_path'     => FCPATH."/uploads/file/",
                  'upload_url'      => base_url()."uploads/file/",
                  'allowed_types'   => 'gif|jpg|png|doc|docx|xls|xlsx|pdf',
                  'overwrite'       => TRUE,
                  'file_name'       => $name
                );

        $this->load->library('upload', $config);
        if($this->upload->do_upload('file'))
        {
            $data = array('status' => true,'message' => '');
        }
        else
        {
            $data = array('status' => false,'message' => $this->upload->display_errors());
        }
        return $data;
    }
    function deleteemail(){
        $id = $this->input->post('places_id');
        $in_data['attachment'] = '';
        $this->db->where('id', $id);
        $this->db->update('gate_email', $in_data);
        if(!empty($ret)) echo json_encode('ok');
        else echo json_encode('failed');

    }
}
