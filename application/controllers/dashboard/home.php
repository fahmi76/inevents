<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/user_m');
		$this->load->model('dashboard/places_m');
		$this->load->helper('form');

		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->user_event = $this->session->userdata('user_event');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	function index() {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		if ($this->user_event == '') {
			$data['data'] = $this->places_m->list_places();
		} else {
			$data['data'] = $this->places_m->list_places_admin($this->user_event);
		}

		$data['content'] = $this->load->view('dashboard/event/main', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	public function report() {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		if ($this->user_event == '') {
			$data['data'] = $this->places_m->list_places(1);
		} else {
			$data['data'] = $this->places_m->list_places_admin($this->user_event);
		}

		$data['content'] = $this->load->view('dashboard/event/main', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	public function indexsd() {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['data'] = $this->user_m->list_user();
		$data['places_id'] = 9;
		$data['content'] = $this->load->view('dashboard/user/main', $data, true);
		$this->load->view('dashboard/main', $data);
	}

}