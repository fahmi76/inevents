<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('dashboard/main_m');
		$this->load->model('dashboard/places_m');
		$this->load->helper('form');

		$this->user_id = $this->session->userdata('user_id');
		$this->user_name = $this->session->userdata('user_name');
		$this->user_event = $this->session->userdata('user_event');
		$this->admin_logged = $this->session->userdata('is_logged_in');
		$cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
		if ($cek_admin) {
			$this->admin_group = $cek_admin->account_group;
			$this->admin_name = $cek_admin->account_displayname;
		} else {
			redirect('dashboard/login');
		}
	}

	public function index() {
		$data['page'] = 'Home';
		$data['title'] = 'Dashboard';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		if ($this->user_event == '') {
			$data['data'] = $this->places_m->list_places();
		} else {
			$data['data'] = $this->places_m->list_places_admin($this->user_event);
		}

		$data['content'] = $this->load->view('dashboard/event/main', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function show($id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;
		$data['data'] = $this->user_m->getuser($id);

		$data['content'] = $this->load->view('dashboard/user/show', $data, true);
		$this->load->view('dashboard/main', $data);
	}

	function delete_places() {
		$id = $this->input->post('places_id');
		$where['id'] = $id;
		$this->db->delete("places", $where);
		if (!empty($ret)) {
			echo json_encode('ok');
		} else {
			echo json_encode('failed');
		}

	}

	function delete($id = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$datauser = $this->user_m->getuser($id);
		if ($datauser->account_avatar != '') {
			$path = FCPATH . "uploads/user/" . $datauser->account_avatar . "";
			unlink($path);
		}
		$where['id'] = $id;
		$this->db->delete("account_pre", $where);

		$data['page'] = 'Home';
		$data['title'] = 'User';
		$data['admin_name'] = $this->admin_name;
		$data['admin_group'] = $this->admin_group;

		$data['content'] = $this->load->view('dashboard/user/delete', $data, true);
		$this->load->view('dashboard/main', $data);
	}

}
