<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class add extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function index() {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Event Name', 'trim|required');
        $this->form_validation->set_rules('sdate', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('edate', 'End Date', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $sdate = explode('/',$this->input->post('sdate',true));
            $edate = explode('/',$this->input->post('edate',true));
            $in_data['places_name'] = $this->input->post('name', true);
            $in_data['places_nicename'] = str_replace(' ', '-', $this->input->post('name', true));
            $header_post = '';
            if($this->do_upload()){
                $header_post = str_replace(' ', '_', $_FILES["file"]['name']);
                $in_data['places_avatar'] = '/uploads/apps/event/'.$header_post;
                $in_data['places_backgroud'] = '/uploads/apps/event/'.$header_post;
                $in_data['places_logo'] = '/uploads/apps/socialite/social-transparent.png';
            }
            $in_data['places_type'] = 1;
            $in_data['places_model'] = 1;
            $in_data['places_type_registration'] = 4;
            $in_data['places_address'] = 'Trinidad & Tobago';
            $in_data['places_desc'] = $this->input->post('name', true);
            $in_data['places_landing'] = str_replace(' ', '-', $this->input->post('name', true));

            $in_data['places_startdate'] = $sdate[2].'-'.$sdate[0].'-'.$sdate[1];
            $in_data['places_duedate'] = $edate[2].'-'.$edate[0].'-'.$edate[1];
            $in_data['places_date'] = date('Y-m-d H:i:s');
            
            $this->db->insert('places', $in_data);
            $data['content'] = $this->load->view('dashboard/event/success', $data, true);
        } else {
            $data['content'] = $this->load->view('dashboard/event/add', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function do_upload(){
        $name = str_replace(' ', '_', $_FILES["file"]['name']);
        $config =  array(
                  'upload_path'     => FCPATH."/uploads/apps/event/",
                  'upload_url'      => base_url()."uploads/apps/event/",
                  'allowed_types'   => "jpg|png|jpeg",
                  'overwrite'       => TRUE,
                  'file_name'       => $name
                );

        $this->load->library('upload', $config);
        if($this->upload->do_upload('file'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
