<?php

class home extends CI_controller {

    protected $tpl;

    function __construct() {
        parent:: __construct();
        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';

        if ($this->config->item('language_abbr') != "")
            $lang = $this->config->item('language_abbr');
        else
            $lang = $this->config->item('language');

        #$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        #$this->output->set_header("Pragma: no-cache");
        //$this->output->enable_profiler(TRUE);
        $this->load->database();
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        //$this->load->library('tweet');
        //$this->load->library('EpiFoursquare');
        $this->load->model(array('convert', 'home_m'));
        date_default_timezone_set('Asia/Jakarta');

        $this->load->helper('url');
        $this->url = $this->config->item('landing_url');
        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');

        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));
    }

    function index() {
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Home";

        $userdata = $this->session->userdata('aidi');
        if (isset($userdata) && $userdata == '')
            $userdata = $this->session->userdata('kode');
        $this->tpl['history'] = $this->db->get_where('log', array('account_id' => $userdata, 'log_status' => 1))->row();
        $this->tpl['user'] = $userdata;
        $acc = $this->db->get_where('account', array('id' => $userdata, 'account_status' => 1))->row();
        if (isset($userdata) && $userdata != '') {
            $this->tpl['token'] = $acc->account_token;
        }

        if ($userdata != '' && $acc->account_group == 9) {
            $this->tpl['recent'] = $this->convert->recent_admin();
        } else {
            $this->tpl['recent'] = $this->convert->recent();
        }

        $this->db->select('');
        $this->db->from('banner');
        $this->db->where('banner_cat', 'event');
        $this->db->where('banner_status', 1);
        $this->tpl['banner'] = $this->db->get()->row();

        $this->db->select('');
        $this->db->from('banner');
        $this->db->where('banner_cat', 'others');
        $this->db->where('banner_status', 1);
        $this->db->order_by('id', 'DESC');
        $sidebanner = $this->db->get();

        $this->tpl['csbanner'] = count($sidebanner->result());
        foreach ($sidebanner->result() as $sban)
            $this->tpl['sidebanner'][] = $sban;

        $this->db->select('');
        $this->db->where('places_parent', 0);
        $this->db->where('places_status', 1);
        $this->db->where('places_type <', 3);
        $this->db->order_by("id", "desc");
        $this->db->limit(3, 0);
        $this->db->from('places');
        $this->tpl['spots'] = $this->db->get()->result();

        $this->db->select('places.*, log.places_id');
        $this->db->select('count(wooz_log.id) as jumlah');
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->where('places.places_type <', 3);
        $this->db->where('log_status', 1);
        $this->db->where('log_type', 1);
        $this->db->order_by("jumlah", "desc");
        $this->db->group_by('places_id');
        $this->db->limit(3, 0);
        $this->db->from('log');
        $this->tpl['active'] = $this->db->get()->result();
        $this->tpl['TContent'] = $this->load->view('home/default', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function about() {
        $this->tpl['page'] = "about";
        $this->tpl['title'] = "About";
        $this->tpl['core'] = 'about';

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        $this->tpl['TContent'] = $this->load->view('home/about', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function privacy() {
        $this->tpl['page'] = "privacy";
        $this->tpl['title'] = "Privacy Policy";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        $this->tpl['TContent'] = $this->load->view('home/privacy', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function tos() {
        $this->tpl['page'] = "termonservice";
        $this->tpl['title'] = "Terms Of Service";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        $this->tpl['TContent'] = $this->load->view('home/tos', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function revoketresemme($type, $spotid, $tresemmeid, $id, $url) {
        switch ($type) {
            case 'fb':
                $db['account_fbid'] = NULL;
                $this->db->where('id', $id);
                $this->db->update('account', $db);
                $sql = "SELECT account_token
                        FROM wooz_account
                        WHERE id = " . $id . "";
                $query = $this->db->query($sql);
                $user = $query->row();
                $url = $this->facebook->getLogoutUrl(array('next' => site_url('kiyora/registrasi/found/' . $spotid . '/' . $tresemmeid . '/' . $id . '?url=' . $url), 'access_token' => $user->account_token));
                redirect($url);
                break;
            case 'tw':
                $db['account_twid'] = NULL;
                $this->db->where('id', $id);
                $this->db->update('account', $db);
                redirect('kiyora/registrasi/found/' . $spotid . '/' . $tresemmeid . '/' . $id . '?url=' . $url);
                break;
            default:
                redirect('kiyora/registrasi/found/' . $spotid . '/' . $tresemmeid . '/' . $id . '?url=' . $url);
                break;
        }
    }

    function revoke() {
        $type = $this->uri->segment(3, 0);
        $userdata = $this->session->userdata('aidi');
        if (!$userdata) {
            redirect('login');
        }
        $this->tpl['user'] = $userdata;
        $row = $this->db->get_where('account', array('id' => $userdata))->row();
        switch ($type) {
            case 'fb':
                $this->db->where('id', $userdata);
                $db['account_fbid'] = NULL;
                $db['account_token'] = NULL;
                $this->db->update('account', $db);
                redirect('profile');
                break;
            case 'tw':
                $this->db->where('id', $userdata);
                $db['account_tw_token'] = NULL;
                $db['account_tw_secret'] = NULL;
                $this->db->update('account', $db);
                redirect('profile');
                break;
            case 'fs':
                $this->db->where('id', $userdata);
                $db['account_fs_token'] = NULL;
                $this->db->update('account', $db);
                redirect('profile');
                break;
            case 'gw':
                $this->db->where('id', $userdata);
                $db['account_gw_token'] = NULL;
                $db['account_gw_refresh'] = NULL;
                $db['account_gw_time'] = NULL;
                $db['account_gw_user'] = NULL;
                $this->db->update('account', $db);
                redirect('profile');
                break;
            default:
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['value'] = "You can't access this page";
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                break;
        }
    }

    function fb_new_regis($goto, $sect, $accid, $spotid, $count, $likefollow) {
        $user = $this->facebook->getUser();
        $tokenfb = $this->facebook->setExtendedAccessToken();
        try {
            $tokens['input_token'] = $tokenfb['access_token'];
            $tokens['access_token'] = $this->config->item('facebook_app_token');
            $tokendebug = $this->facebook->api("/debug_token", 'get', $tokens);
            if (isset($tokendebug['data']['expires_at']) && $tokendebug['data']['expires_at'] != '') {
                $data['account_fb_expired'] = date("Y-m-d", $tokendebug['data']['expires_at']);
            }
        } catch (FacebookApiException $e) {
            if ($goto == 'landing') {
                redirect($this->url . '/home?url=' . $sect . '&places=' . $spotid . '&retry=true&status=token_not_found');
            } elseif ($goto == 'cekrfid') {
                redirect('cekrfid/home?url=' . $sect . '&retry=true&status=token_not_found');
            } else {
                redirect($this->url . '/step3?url=' . $sect . '&acc=' . $accid . '&from=tw&places=' . $spotid . '&likefollow=' . $likefollow . '&retry=true&status=token_not_found');
            }
        }
        $data['account_token'] = $tokenfb['access_token'];
        $data['account_fbid'] = $user;
        try {
            $info = $this->facebook->api(array(
                'access_token' => $tokenfb['access_token'],
                'method' => 'fql.query',
                'query' => 'SELECT uid,name,email,birthday_date,sex,pic_big,hometown_location FROM user WHERE uid=me()',
            ));
            
            if (isset($info[0]['hometown_location']) && $info[0]['hometown_location'] != "" && isset($info[0]['hometown_location']['name']) && $info[0]['hometown_location']['name'] != "") {
                $data['account_location'] = $info[0]['hometown_location']['name'];
            }
            $data['account_fb_name'] = $info[0]['name'];
            if ($info[0]['pic_big'] != "") {
                $data['account_avatar'] = $info[0]['pic_big'];
            }
            if ($info[0]['email'] != "") {
                $data['account_email'] = $info[0]['email'];
            }
            if ($info[0]['birthday_date'] != "") {
                $date = $info[0]['birthday_date'];
                $a = explode('/', $date);
                $data['account_birthdate'] = $a[2].'-'.$a[0].'-'.$a[1];
            }
            if ($info[0]['sex'] != "") {
                $data['account_gender'] = $info[0]['sex'];
            }
        } catch (FacebookApiException $e) {
            if ($goto == 'landing') {
                redirect($this->url . '/home?url=' . $sect . '&places=' . $spotid . '&retry=true&status=token_not_found');
            } elseif ($goto == 'cekrfid') {
                redirect('cekrfid/home?url=' . $sect . '&retry=true&status=token_not_found');
            } else {
                redirect($this->url . '/step3?url=' . $sect . '&acc=' . $accid . '&from=tw&places=' . $spotid . '&likefollow=' . $likefollow . '&retry=true&status=token_not_found');
            }
        }

        if ($goto == 'landing') {
            if ($count == 1) {
                $this->home_m->update($accid, $data);
                redirect($this->url . '/step2/?url=' . $sect . '&acc=' . $accid . '&from=fb&places=' . $spotid);
            } elseif ($count == 2) {
                $data['account_displayname'] = $info[0]['name'];
                $new = $this->home_m->insert('account', $data);
                redirect($this->url . '/step2/?url=' . $sect . '&acc=' . $new . '&from=fb&places=' . $spotid);
            } else {
                redirect($this->url . '/home?url=' . $sect . '&places=' . $spotid . '&retry=true&status=please_try_again');
            }
        } else {
            $this->home_m->update($accid, $data);
            if ($goto == 'cekrfid') {
                redirect('cekrfid/home/' . $accid . '?url=' . $sect);
            } else {
                redirect($this->url . '/step4?url=' . $sect . '&acc=' . $accid . '&from=tw&places=' . $spotid . '&likefollow=' . $likefollow);
            }
        }
    }

    function fb_tresemme_regis($goto, $sect, $accid, $spotid, $count, $likefollow) {
        $user = $this->facebook->getUser();
        $tokenfb = $this->facebook->setExtendedAccessToken();
        try {
            $tokens['input_token'] = $tokenfb['access_token'];
            $tokens['access_token'] = $this->config->item('facebook_app_token');
            $tokendebug = $this->facebook->api("/debug_token", 'get', $tokens);
            if (isset($tokendebug['data']['expires_at']) && $tokendebug['data']['expires_at'] != '') {
                $data['account_fb_expired'] = date("Y-m-d", $tokendebug['data']['expires_at']);
            }
        } catch (FacebookApiException $e) {
            redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '?status=token_not_found&url=' . $sect);
        }
        $data['account_token'] = $tokenfb['access_token'];
        $data['account_fbid'] = $user;
        try {
            $info = $this->facebook->api(array(
                'access_token' => $tokenfb['access_token'],
                'method' => 'fql.query',
                'query' => 'SELECT uid,name,pic_big,hometown_location FROM user WHERE uid=me()',
            ));
            if (!$info) {
                redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '?status=token_not_found&url=' . $sect);
            }
            if (isset($info[0]['hometown_location']) && $info[0]['hometown_location'] != "" && isset($info[0]['hometown_location']['name']) && $info[0]['hometown_location']['name'] != "") {
                $data['account_location'] = $info[0]['hometown_location']['name'];
            }
            $data['account_fb_name'] = $info[0]['name'];
            if ($info[0]['pic_big'] != "") {
                $data['account_avatar'] = $info[0]['pic_big'];
            }
        } catch (FacebookApiException $e) {
            redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '?status=token_not_found&url=' . $sect);
        }
        $sql = "SELECT account_displayname,account_email,account_birthdate,account_phone,account_profession,account_gender 
                    FROM wooz_account_kiyora
                    WHERE id = '" . $accid . "'";
        $query = $this->db->query($sql);
        $dataquery = $query->row();
        if ($dataquery) {
            $data['account_displayname'] = $dataquery->account_displayname;
            $data['account_email'] = $dataquery->account_email;
            $data['account_birthdate'] = $dataquery->account_birthdate;
            $data['account_phone'] = $dataquery->account_phone;
            $data['account_profession'] = $dataquery->account_profession;
            $data['account_gender'] = $dataquery->account_gender;
            $data['account_username'] = $this->convert->nicename($dataquery->account_displayname);
        } else {
            redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '?status=user_not_found&url=' . $sect);
        }
        if ($count != 0) {
            if ($likefollow != 0) {
                $count = $likefollow;
            }
            $this->home_m->update($count, $data);
        } else {
            if ($likefollow != 0) {
                $this->home_m->update($likefollow, $data);
                $count = $likefollow;
            } else {
                $count = $this->home_m->insert('account', $data);
            }
        }
        $datakiyora['account_id'] = $count;
        $datakiyora['account_status'] = 1;
        $this->db->where('id', $accid);
        $update = $this->db->update('account_kiyora', $datakiyora);
        redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '/' . $count . '?url=' . $sect);
    }

    function facebook() {
        $this->tpl['page'] = "home/facebook";
        $this->tpl['title'] = "Facebook Connect";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        $goto = $this->uri->segment(3, 0);
        $sect = $this->uri->segment(4, 0);
        $accid = $this->uri->segment(5, 0);
        $spotid = $this->uri->segment(6, 0);
        $likefollow = $this->uri->segment(7, 0);

        if (isset($goto) && $goto != '0') {
            $next = true;
        } else {
            $next = false;
        }

        $pageuri = $this->session->userdata('access');

        $data['account_fbid'] = NULL;
        $data['account_token'] = NULL;

        $user = $this->facebook->getUser();
        $data['account_fbid'] = $user;

        if ($data['account_fbid']) {
            $check = $this->home_m->getfb($data['account_fbid'], 'account');
            if (count($check) !== 0) {
                $count = 1; /* ada di database */
            } else {
                $count = 2; /* ga ada di database */
            }
        } else {
            /* fbid not found */
            $user = $this->facebook->getUser();
            $data['account_fbid'] = $user;
            $count = 0;
        }

        if (isset($goto) && $goto == 'landing') {
            if ($count == 1) {
                $accid = $check->id;
            }
            $this->fb_new_regis($goto, $sect, $accid, $spotid, $count, $likefollow);
        }
        if (isset($goto) && $goto == 'landingfb2') {
            if ($count == 1) {
                if ($accid != $check->id) {
                    redirect($this->url . '/accountfound/tw/' . $check->id . '/' . $accid . '?url=' . $sect . '&places=' . $spotid . '&from=tw&likefollow=' . $likefollow);
                }
                $accid = $check->id;
            }
            $this->fb_new_regis($goto, $sect, $accid, $spotid, $count, $likefollow);
        }
        if (isset($goto) && $goto == 'cekrfid') {
            if ($count == 1) {
                if ($accid != $check->id) {
                    redirect('cekrfid/accountfound/tw/' . $check->id . '/' . $accid . '?url=' . $sect . '&from=tw');
                }
                $accid = $check->id;
            }
            $this->fb_new_regis($goto, $sect, $accid, $spotid, $count);
        }

        if (isset($goto) && $goto == 'tresemme') {
            $acc_old_id = 0;
            if ($check) {
                $acc_old_id = $check->id;
            }
            $this->fb_tresemme_regis($goto, $sect, $accid, $spotid, $acc_old_id, $likefollow);
        }

        if (isset($goto) && $goto == 'tresemmeshare') {
            $redirect = base_url() . 'kiyora/registrasi/share/' . $accid . '/' . $spotid . '/' . $likefollow . '?share=1&url=' . $sect;
            redirect($redirect);
        }

        if (isset($goto) && $goto == 'step2fb') {
            $likefollow2 = $this->uri->segment(8, 0);
            $from = $this->uri->segment(9, 0);
            $sql = "SELECT id,places_landing "
                    . "FROM wooz_places "
                    . "WHERE id = '$sect' order by id desc";
            $hasil = $this->db->query($sql);
            $data = $hasil->row();

            $redirect = base_url() . 'landing/stepshare?url=' . $data->places_landing . '&acc=' . $accid . '&from=' . $from . '&loc=' . $loc . '&likefollow=' . $likefollow . '&likefollow2=' . $likefollow2;
            redirect($redirect);
        }
        if ($count === 1) {
            if ($next) {
                if ($goto == 'done') {
                    #redirect('cekdone/step1/'.$check->id);
                    $keyword['aidi'] = $check->id;
                    $keyword['nama'] = $check->account_displayname;
                    $keyword['nice'] = $check->account_username;
                    $keyword['grup'] = $check->account_group;
                    $this->session->set_userdata($keyword);
                    redirect('profile');
                } else {
                    if ($goto == 'mobile')
                        redirect($goto . '/profile');
                    elseif ($goto != 'customs') {
                        if ($goto == 'landing')
                            redirect('landing/step2/?url=' . $sect . '&acc=' . $check->id . '&from=fb&places=' . $spotid);
                        elseif ($goto == 'landingfb2') {
                            if ($accid != $check->id) {
                                redirect('landing/accountfound/tw/' . $check->id . '/' . $accid . '?url=' . $sect . '&places=' . $spotid);
                            } else {
                                $this->facebook->setExtendedAccessToken();
                                $token = $this->facebook->getAccessToken();
                                try {
                                    $tokens['input_token'] = $token;
                                    $tokens['access_token'] = $this->config->item('facebook_app_token');
                                    $tokendebug = $this->facebook->api("/debug_token", 'get', $tokens);
                                    if ($tokendebug) {
                                        if (isset($tokendebug['data']['issued_at']) && $tokendebug['data']['issued_at'] != '') {
                                            $data['account_fb_created'] = date("Y-m-d", $tokendebug['data']['issued_at']);
                                        }
                                        #else{
                                        #	redirect('step3?status=new&url=' . $sect .'&acc='.$check->id.'&from=tw&places='.$spotid);
                                        #redirect('home?url='.$this->custom_url.'&retry=true&status=time_token_not_valid');
                                        #}
                                        $data['account_fb_expired'] = date("Y-m-d", $tokendebug['data']['expires_at']);
                                    } else {
                                        redirect('landing/step3?status=new&url=' . $sect . '&acc=' . $check->id . '&from=tw&places=' . $spotid);
                                        #redirect('home?url='.$this->custom_url.'&retry=true&status=time_token_not_valid');
                                    }
                                } catch (FacebookApiException $e) {
                                    redirect('landing/step3?status=new&url=' . $sect . '&acc=' . $check->id . '&from=tw&places=' . $spotid);
                                    #redirect('home?url='.$this->custom_url.'&retry=true&status=token_not_valid');
                                }
                                $data['account_token'] = $token;
                                $this->home_m->update($check->id, $data);
                                redirect('landing/step4?status=new&url=' . $sect . '&acc=' . $check->id . '&from=tw&places=' . $spotid);
                            }
                        } elseif ($goto == 'loginfb') {
                            $holycowid = "SELECT `account_id` FROM `wooz_landing` WHERE `landing_register_form` = 285 and `account_id` = " . $check->id . " ORDER BY `landing_joindate` DESC ";
                            $hasilholycowid = $this->db->query($holycowid);
                            $resultholycowid = $hasilholycowid->row();
                            if ($resultholycowid) {
                                $keyword['uid'] = $check->id;
                                $keyword['nice'] = $check->account_username;
                                $keyword['grup'] = $check->account_group;
                                $this->session->set_userdata($keyword);
                                redirect('holycow/profile');
                            } else {
                                redirect('holycow/error');
                            }
                        } elseif ($goto == 'customfb') {
                            redirect('/sstep2?next=' . $sect);
                        } elseif ($goto == 'customfbs') {
                            redirect('/step3fb?next=' . $sect);
                        } elseif ($goto == 'heinekenfb') {
                            redirect('/heineken/step2?next=' . $sect);
                        } elseif ($goto == 'heinekenfbs') {
                            redirect('/heineken/step3fb?next=' . $sect);
                        } else
                            redirect($goto . '/step2');
                    }else {
                        redirect('/step2?next=' . $sect);
                    }
                }
            } elseif ($pageuri == 'mobile') {
                redirect('mobile/profile');
            } else {
                #redirect('cekdone/step1/'.$check->id);
                $keyword['aidi'] = $check->id;
                $keyword['nama'] = $check->account_displayname;
                $keyword['nice'] = $check->account_username;
                $keyword['grup'] = $check->account_group;
                $this->session->set_userdata($keyword);
                redirect('profile');
            }
        } elseif ($count === 2) {
            /* fbid not match with any user_id in database */
            if ($next) {
                if ($goto == 'done') {
                    #redirect('cekdone/step1');
                    redirect('signupnext');
                } else {
                    if ($goto == 'mobile') {
                        redirect($goto . '/profile');
                    } elseif ($goto != 'customs') {
                        if ($goto == 'landing') {
                            redirect('landing/step2?status=new&url=' . $sect . '&from=fb&places=' . $spotid);
                        } elseif ($goto == '3' || $goto == 'profile' || $goto == 'landingfb2') {
                            $this->facebook->setExtendedAccessToken();
                            $token = $this->facebook->getAccessToken();

                            try {
                                $tokenfb['input_token'] = $token;
                                $tokenfb['access_token'] = $this->config->item('facebook_app_token');
                                $tokendebug = $this->facebook->api("/debug_token", 'get', $tokenfb);
                                if ($tokendebug) {
                                    if (isset($tokendebug['data']['issued_at']) && $tokendebug['data']['issued_at'] != '') {
                                        $data['account_fb_expired'] = date("Y-m-d", $tokendebug['data']['expires_at']);
                                        $data['account_fb_created'] = date("Y-m-d", $tokendebug['data']['issued_at']);
                                    } else {
                                        if ($goto == '3') {
                                            redirect('signupskip/' . $goto . '/' . $accid . '/?retry=true&status=token_not_found');
                                        } elseif ($goto == 'landingfb2') {
                                            redirect('landing/step3?status=new&url=' . $sect . '&acc=' . $accid . '&from=tw&places=' . $spotid);
                                        } else {
                                            redirect('profile?retry=true&status=token_not_found');
                                        }
                                    }
                                } else {
                                    if ($goto == '3') {
                                        redirect('signupskip/' . $goto . '/' . $accid . '/?retry=true&status=token_not_found');
                                    } elseif ($goto == 'landingfb2') {
                                        redirect('landing/step3?status=new&url=' . $sect . '&acc=' . $accid . '&from=tw&places=' . $spotid);
                                    } else {
                                        redirect('profile?retry=true&status=token_not_found');
                                    }
                                }
                            } catch (FacebookApiException $e) {
                                if ($goto == '3') {
                                    redirect('signupskip/' . $goto . '/' . $accid . '/?retry=true&status=token_not_found');
                                } elseif ($goto == 'landingfb2') {
                                    redirect('landing/step3?status=new&url=' . $sect . '&acc=' . $accid . '&from=tw&places=' . $spotid);
                                } else {
                                    redirect('profile?retry=true&status=token_not_found');
                                }
                            }
                            $data['account_token'] = $token;

                            try {
                                $info = $this->facebook->api(array(
                                    'access_token' => $token,
                                    'method' => 'fql.query',
                                    'query' => 'SELECT uid,pic_big,hometown_location FROM user WHERE uid=me()',
                                ));
                                if (isset($info[0]['hometown_location']) && $info[0]['hometown_location'] != "" && isset($info[0]['hometown_location']['name']) && $info[0]['hometown_location']['name'] != "") {
                                    $data['account_location'] = $info[0]['hometown_location']['name'];
                                }

                                if (isset($info[0]['pic_big']) && $info[0]['pic_big'] != "") {
                                    $data['account_avatar'] = $info[0]['pic_big'];
                                }
                            } catch (FacebookApiException $e) {
                                if ($goto == '3') {
                                    redirect('signupskip/' . $goto . '/' . $accid . '/?retry=true&status=token_not_found');
                                } elseif ($goto == 'landingfb2') {
                                    redirect('landing/step3?status=new&url=' . $sect . '&acc=' . $accid . '&from=tw&places=' . $spotid);
                                } else {
                                    redirect('profile?retry=true&status=token_not_found');
                                }
                            }
                            if ($goto == '3') {
                                $this->home_m->update($accid, $data);
                                redirect('signupskip/' . $goto . '/' . $accid);
                            } elseif ($goto == 'landingfb2') {
                                $this->home_m->update($accid, $data);
                                redirect('landing/step4?status=new&url=' . $sect . '&acc=' . $accid . '&from=tw&places=' . $spotid);
                            } else {
                                $keyword['aidi'] = $this->session->userdata('kode');
                                $this->session->set_userdata($keyword);
                                $userdata = $this->session->userdata('kode');
                                $this->home_m->update($userdata, $data);
                                redirect('profile');
                            }
                        } elseif ($goto == 'customfb') {
                            redirect('/sstep2?next=' . $sect);
                        } elseif ($goto == 'customfbs') {
                            redirect('/step3fb?next=' . $sect);
                        } elseif ($goto == 'heinekenfb') {
                            redirect('/heineken/step2?next=' . $sect);
                        } elseif ($goto == 'loginfb') {
                            redirect('holycow/error');
                        } elseif ($goto == 'heinekenfbs') {
                            redirect('/heineken/step3fb?next=' . $sect);
                        } else {
                            redirect($goto . '/step2?status=new');
                        }
                    } else {
                        #if($default == 'default'){
                        #   redirect('/sstep2?status=new&next=' . $sect);                                                            
                        #}else{
                        redirect('/step2?status=new&next=' . $sect);
                        #}
                    }
                }
            } elseif ($pageuri == 'mobile') {
                redirect('mobile/signupnext');
            } else {
                #redirect('signupnext/1');
                redirect('signupnext?from=fb&acc=0&fb=new');
            }
        } else {
            if ($next) {
                if ($goto == 'customfb') {
                    redirect('/sstep2?next=' . $sect);
                } elseif ($goto == 'customfbs') {
                    redirect('/step3fb?status=new');
                } elseif ($goto == 'heinekenfb') {
                    redirect('/heineken/step2?next=' . $sect);
                } elseif ($goto == 'heinekenfbs') {
                    redirect('/heineken/step3fb?status=new');
                } elseif ($goto == 'landing') {
                    $dataceklanding['url'] = 'home?url=' . $sect . '&places=' . $spotid . '&retry=true&status=user_not_found';
                    $dataceklanding['data_status'] = 4;
                    $this->db->insert('landing_cek', $dataceklanding);
                    redirect('landing/home?url=' . $sect . '&places=' . $spotid . '&retry=true&status=user_not_found');
                } elseif ($goto == 'landingfb2') {
                    redirect('landing/step3?url=' . $next . '&places=' . $spotid . '&retry=true&status=user_not_found&from=tw');
                } elseif ($goto == 'loginfb') {
                    redirect('holycow/error&retry=true&status=user_not_found');
                } else {
                    redirect('home?retry=true');
                }
            } else {
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['value'] = "Please retry to connect your facebook account, and better to clear you cache first";
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
            }
        }
        $this->load->view('body', $this->tpl);
    }

    function twit() {
        include(APPPATH . 'third_party/tmhOAuth.php');
        include(APPPATH . 'third_party/tmhUtilities.php');

        $this->tpl['page'] = "home/twitter";
        $this->tpl['title'] = "Twitter Connect";

        $goto = $this->uri->segment(3, 0);
        $next = $this->uri->segment(4, 0);
        $accid = $this->uri->segment(5, 0);
        $spotid = $this->uri->segment(6, 0);
        $likefollow = $this->uri->segment(7, 0);

        $tmhOAuth = new tmhOAuth(array(
            date_default_timezone_set('Asia/Jakarta'),
            'consumer_key' => $this->config->item('twiiter_consumer_key'),
            'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
        ));
        /* check for user session. if nothing, goto start */
        $userdata = $this->session->userdata('aidi');
        if ($userdata == '') {
            if ($goto != 'landing' && $goto != 'login' && $goto != 'landingtw2' && $goto != '3' && $goto != 'signup' && $goto != 'customtw' && $goto != 'customtws' && $goto != 'heinekentw' && $goto != 'logintw' && $goto != 'cekrfid' && $goto != 'tresemme') {
                #redirect('?retry=true');
                redirect('homes?retry=true');
            }
        }
        $this->tpl['user'] = $userdata;

        $twitcancel = $this->input->get('denied', true);
        if ($twitcancel) {
            if ($goto == 'landing')
                redirect('landing/home?url=' . $next . '&places=' . $spotid);
            elseif ($goto == 'landingtw2')
                redirect('landing/step3?url=' . $next . '&places=' . $spotid . '&from=fb&acc=' . $accid . '&likefollow=' . $likefollow);
        }
        if (isset($_REQUEST['oauth_verifier'])) {
            /* oauth_verifier */
            $tmhOAuth->config['user_token'] = $_SESSION['oauth']['oauth_token'];
            $tmhOAuth->config['user_secret'] = $_SESSION['oauth']['oauth_token_secret'];

            $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/access_token', ''), array(
                'oauth_verifier' => $_REQUEST['oauth_verifier']
            ));

            if ($code == 200) {
                /* oauth verifier success */
                unset($_SESSION['oauth']);
                $access_token = $tmhOAuth->extract_params($tmhOAuth->response['response']);
                if (is_array($access_token) && count($access_token) != '0') {
                    /* update user token at database */
                    $db['account_tw_token'] = $access_token['oauth_token'];
                    $db['account_tw_secret'] = $access_token['oauth_token_secret'];
                    $db['account_tw_username'] = $access_token['screen_name'];

                    if ($goto != 'landing') {
                        #$this->home_m->update($userdata, $db, 'account');
                        #$this->db->where('id', $userdata);
                        #$this->db->update('account', $db); 
                    }
                    /* get additional user data */
                    $tmhOAuth->config['user_token'] = $access_token['oauth_token'];
                    $tmhOAuth->config['user_secret'] = $access_token['oauth_token_secret'];
                    $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/account/verify_credentials'));
                    #print_r($code);die;
                    if ($code == 200) {
                        /* get data success */
                        $response = json_decode($tmhOAuth->response['response']);
                        $crd['account_tw_username'] = $access_token['screen_name'];
                        $crd['account_tw_name'] = $response->name;
                        $crd['account_twid'] = $access_token['user_id'];
                        $crd['account_tw_token'] = $access_token['oauth_token'];
                        $crd['account_tw_secret'] = $access_token['oauth_token_secret'];

                        if ($goto == 'tresemme') {
                            $this->twittresemme($goto, $next, $accid, $spotid, $likefollow, $crd);
                        }

                        $row = $this->home_m->getaccount('account_twid', $crd['account_twid'], 'account');
                        if ($row) {
                            if ($row->account_avatar == '') {
                                $get_avatar = explode('_normal', $response->profile_image_url);
                                $crd['account_avatar'] = $get_avatar[0] . $get_avatar[1];
                            }
                            if ($row->account_displayname == '') {
                                $crd['account_displayname'] = $response->name;
                            }
                            if ($row->account_twid == '') {
                                $crd['account_twid'] = $response->id;
                            }
                            if ($row->account_profile == '') {
                                $crd['account_profile'] = $response->description;
                            }
                            if ($row->account_location == '') {
                                $crd['account_location'] = $response->location;
                            }
                            if ($row->account_url == '') {
                                $crd['account_url'] = $response->url;
                            }
                        } else {
                            $get_avatar = explode('_normal', $response->profile_image_url);
                            $crd['account_avatar'] = $get_avatar[0] . $get_avatar[1];
                            $crd['account_twid'] = $response->id;
                            $crd['account_profile'] = $response->description;
                            $crd['account_location'] = $response->location;
                            $crd['account_url'] = $response->url;
                        }
                        #print_r($crd);die;
                        $os = array("landing", "login", "landingtw2", "3", "signup", "customtws", "customtw", "heinekentw", "logintw", "cekrfid");
                        if (in_array($goto, $os)) {
                            if ($row) {
                                if ($goto == 'logintw') {
                                    $holycowid = "SELECT `account_id` FROM `wooz_landing` WHERE `landing_register_form` = 285 and `account_id` = " . $row->id . " ORDER BY `landing_joindate` DESC ";
                                    $hasilholycowid = $this->db->query($holycowid);
                                    $resultholycowid = $hasilholycowid->row();
                                    if ($resultholycowid) {
                                        $keyword['uid'] = $row->id;
                                        $keyword['nice'] = $row->account_username;
                                        $keyword['grup'] = $row->account_group;
                                        $this->session->set_userdata($keyword);
                                        redirect('holycow/profile');
                                    } else {
                                        redirect('holycow/error');
                                    }
                                }
                                if ($row->id != $accid) {
                                    if ($goto == 'landingtw2') {
                                        redirect('landing/accountfound/fb/' . $row->id . '/' . $accid . '?url=' . $next . '&places=' . $spotid . '&likefollow=' . $likefollow);
                                    } if ($goto == 'cekrfid') {
                                        redirect('cekrfid/accountfound/fb/' . $row->id . '/' . $accid . '?url=' . $next);
                                    } else {
                                        $this->home_m->update($row->id, $crd);
                                        $new = $row->id;
                                        if ($goto == '1') {
                                            redirect('cekdone/step1/' . $new);
                                        }
                                    }
                                } else {
                                    $this->home_m->update($row->id, $crd);
                                    $new = $row->id;
                                    if ($goto == 'signup') {
                                        redirect('cekdone/step1/' . $new);
                                    }
                                }
                            } else {
                                if ($goto == 'logintw') {
                                    redirect('holycow/error');
                                }
                                if ($accid != '0') {
                                    $this->home_m->update($accid, $crd);
                                    $new = $accid;
                                } else {
                                    if ($goto == 'login') {
                                        redirect('signup');
                                    } else {
                                        $crd['account_displayname'] = $response->name;
                                        $this->db->insert('account', $crd);
                                        $new = $this->db->insert_id();
                                    }
                                }
                            }
                        } else {
                            $this->home_m->update($userdata, $crd);
                        }
                    }

                    if (isset($goto) && $goto != '0') {
                        if ($goto == 'customs') {
                            if ($default == 'defaulttw') {
                                redirect('/sstep4?next=' . $next);
                            } else
                                redirect('/step4');
                        } elseif ($goto == 'landingtw2') {
                            redirect('landing/step4?url=' . $next . '&from=fb&acc=' . $new . '&places=' . $spotid . '&likefollow=' . $likefollow);
                        } elseif ($goto == 'cekrfid') {
                            redirect('cekrfid/home/' . $new . '?url=' . $next);
                        } elseif ($goto == 'landing') {
                            redirect('landing/step2?url=' . $next . '&from=tw&acc=' . $new . '&places=' . $spotid);
                        } elseif ($goto == '3') {
                            redirect('signupskip/' . $goto . '/' . $accid);
                        } elseif ($goto == 'signup') {
                            #redirect('signupnext?from=tw&acc='.$new);
                            redirect('profile');
                        } elseif ($goto == 'login') {
                            $keyword['aidi'] = $new;
                            $this->session->set_userdata($keyword);
                            redirect('profile');
                        } elseif ($goto == 'customtws' || $goto == 'heinekentws') {
                            if ($goto == 'heinekentws') {
                                redirect('/heineken/step4?next=' . $next);
                            } else {
                                redirect('/sstep4?next=' . $next . '&camp=' . $spotid);
                            }
                        } elseif ($goto == 'customtw' || $goto == 'heinekentw') {
                            $new_session = $this->db->get_where('account', array('id' => $new))->row();
                            $keyword['aidi'] = $new_session->id;
                            $keyword['nice'] = $new_session->account_username;
                            $keyword['grup'] = $new_session->account_group;
                            $this->session->set_userdata($keyword);
                            if ($goto == 'heinekentw') {
                                redirect('heineken/step2s?next=' . $next);
                            } else {
                                redirect('/step2s?next=' . $next . '&camp=' . $spotid);
                            }
                        } else {
                            /*
                              $update = 'just activated @wooz_in, connecting my online and offline activies';
                              $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
                              'status' => $update
                              )); */
                            redirect('signupskip/1');
                        }
                    } else {
                        redirect('profile');
                    }
                }
            } else {
                /* oauth verifier failed */
                if (isset($goto) && $goto != '0') {
                    if ($goto == 'customs')
                        redirect('/step3?status=oauth_verifier_error');
                    elseif ($goto == 'landing')
                        redirect('landing/home?url=' . $next . '&places=' . $spotid . '&status=oauth_verifier_error');
                    elseif ($goto == 'landingtw2')
                        redirect('landing/step3?url=' . $next . '&places=' . $spotid . '&status=oauth_verifier_error&from=fb&acc=' . $accid . '&likefollow=' . $likefollow);
                    elseif ($goto == 'cekrfid')
                        redirect('cekrfid/home/' . $accid . '?url=' . $next . '&status=oauth_verifier_error');
                    elseif ($goto == 'customtw')
                        redirect('homes?status=oauth_verifier_error&camp=' . $spotid);
                    elseif ($goto == 'tresemme')
                        redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '?status=oauth_verifier_error&url=' . $next);
                    elseif ($goto == 'customtws')
                        redirect('/sstep3?status=oauth_verifier_error&camp=' . $spotid);
                    elseif ($goto == 'heinekentw')
                        redirect('heineken?status=oauth_verifier_error');
                    elseif ($goto == 'heinekentws')
                        redirect('/heineken/step3?status=oauth_verifier_error');
                    else
                        redirect('signupskip/2?status=oauth_verifier_error');
                } else {
                    redirect('profile?status=oauth_verifier_error');
                }
            }
        } else {
            /* twitter connect start */
            if ($next != '0') {
                if ($goto == 'landingtw2') {
                    $callback = site_url('home/twit/' . $goto . '/' . $next . '/' . $accid . '/' . $spotid . '/' . $likefollow);
                } else {
                    #$callback = site_url('home/twit/' . $goto . '/' . $next);
                    $callback = site_url('home/twit/' . $goto . '/' . $next . '/' . $accid . '/' . $spotid . '/' . $likefollow);
                }
            } else {
                if ($accid != '0') {
                    $callback = site_url('home/twit/' . $goto . '/' . $next . '/' . $accid . '/' . $spotid . '/' . $likefollow);
                } else {
                    #$callback = site_url('home/twit/' . $goto);
                    $callback = site_url('home/twit/' . $goto . '/' . $next . '/' . $accid . '/' . $spotid . '/' . $likefollow);
                }
            }

            $params = array('oauth_callback' => $callback);
            $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/request_token', ''), $params);
            if ($code == 200) {
                /* token request available */
                $_SESSION['oauth'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
                $url = $tmhOAuth->url('oauth/authorize', '') . "?oauth_token={$_SESSION['oauth']['oauth_token']}&force_login=1";
                redirect($url);
            } else {
                /* token request not available */
                if (isset($goto) && $goto != '0') {  // return to connect step 
                    if ($goto == 'customs')
                        redirect('step3?status=twitter_start_error');
                    elseif ($goto == 'landing')
                        redirect('landing/home?url=' . $next . '&places=' . $spotid . '&status=twitter_start_error');
                    elseif ($goto == 'landingtw2')
                        redirect('landing/step3?url=' . $next . '&places=' . $spotid . '&status=twitter_start_error&from=fb&acc=' . $accid . '&likefollow=' . $likefollow);
                    elseif ($goto == 'cekrfid')
                        redirect('cekrfid/home/' . $accid . '?url=' . $next . '&status=twitter_start_error');
                    elseif ($goto == 'customtw')
                        redirect('homes?status=twitter_start_error&camp=' . $spotid);
                    elseif ($goto == 'tresemme')
                        redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '?status=twitter_start_error&url=' . $next);
                    elseif ($goto == 'customtws')
                        redirect('sstep3?status=twitter_start_error&camp=' . $spotid);
                    elseif ($goto == 'heinekentw')
                        redirect('heineken?status=twitter_start_error');
                    elseif ($goto == 'heinekentws')
                        redirect('heineken/step3?status=twitter_start_error');
                    else
                        redirect('signupskip/2?status=twitter_start_error');
                } else {
                    redirect('profile?status=twitter_start_error'); // return to profile
                }
            }
        }
    }

    function twitter(){
        include(APPPATH . 'third_party/tmhOAuth.php');
        include(APPPATH . 'third_party/tmhUtilities.php');

        $goto = $this->uri->segment(3, 0);
        $next = $this->uri->segment(4, 0);
        $places = $this->uri->segment(5, 0);
        $accid = $this->uri->segment(6, 0);
        $landing = $this->uri->segment(7, 0);
        $from = $this->uri->segment(8, 0);

        $tmhOAuth = new tmhOAuth(array(
            'consumer_key' => $this->config->item('twiiter_consumer_key'),
            'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
        ));

        $twitcancel = $this->input->get('denied', true);
        if ($twitcancel) {
            redirect($goto .'/'.$next.'/'.$places.'/'.$accid.'/'.$landing.'/'.$from);
        }

        if (isset($_REQUEST['oauth_verifier'])) {
            /* oauth_verifier */
            $tmhOAuth->config['user_token'] = $_SESSION['oauth']['oauth_token'];
            $tmhOAuth->config['user_secret'] = $_SESSION['oauth']['oauth_token_secret'];

            $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/access_token', ''), array(
                'oauth_verifier' => $_REQUEST['oauth_verifier']
            ));

            if ($code == 200) {
                /* oauth verifier success */
                unset($_SESSION['oauth']);
                $access_token = $tmhOAuth->extract_params($tmhOAuth->response['response']);
                if (is_array($access_token) && count($access_token) != '0') {
                    /* update user token at database */
                    $db['account_tw_token'] = $access_token['oauth_token'];
                    $db['account_tw_secret'] = $access_token['oauth_token_secret'];
                    $db['account_tw_username'] = $access_token['screen_name'];
                    /* get additional user data */
                    $tmhOAuth->config['user_token'] = $access_token['oauth_token'];
                    $tmhOAuth->config['user_secret'] = $access_token['oauth_token_secret'];
                    $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/account/verify_credentials'));

                    if ($code == 200) {
                        /* get data success */
                        $response = json_decode($tmhOAuth->response['response']);
                        $crd['account_tw_username'] = $access_token['screen_name'];
                        $crd['account_twid'] = $access_token['user_id'];
                        $crd['account_tw_token'] = $access_token['oauth_token'];
                        $crd['account_tw_secret'] = $access_token['oauth_token_secret'];
                        #$crd['account_tw_follow'] = $response->followers_count;

                        $row = $this->home_m->gettwitter($crd['account_twid'], $accid);

                        if ($row) {
                            if ($row->account_avatar == '') {
                                $get_avatar = explode('_normal', $response->profile_image_url);
                                $crd['account_avatar'] = $get_avatar[0] . $get_avatar[1];
                            }
                            if ($row->account_displayname == '') {
                                $crd['account_displayname'] = $response->name;
                            }
                            if ($row->account_profile == '') {
                                $crd['account_profile'] = $response->description;
                            }
                            if ($row->account_location == '') {
                                $crd['account_location'] = $response->location;
                            }
                            if ($row->account_url == '') {
                                $crd['account_url'] = $response->url;
                            }
                        } else {
                            $get_avatar = explode('_normal', $response->profile_image_url);
                            $crd['account_avatar'] = $get_avatar[0] . $get_avatar[1];
                            $crd['account_profile'] = $response->description;
                            $crd['account_location'] = $response->location;
                            $crd['account_url'] = $response->url;
                        }

                        $os = array("landing","landingnew","landingnewtw2", "login", "landingtw2", "3", "signup");
                        if (in_array($goto, $os)) {
                            if ($row) {
                                if ($row->id != $accid) {
                                    if ($goto == 'landingtw2') {
                                        redirect($this->url . '/accountfound?url=' . $next . '&places=' . $spotid . '&from=fb&accold=' . $row->id . '&accnew=' . $accid . '&loc=' . $loc . '&likefollow=' . $likefollow);
                                    } elseif ($goto == 'landingnewtw2') {
                                        redirect($this->urlnew . '/accountfound?url=' . $next . '&places=' . $spotid . '&from=fb&accold=' . $row->id . '&accnew=' . $accid . '&loc=' . $loc . '&likefollow=' . $likefollow);
                                    } else {
                                        $this->home_m->update($row->id, $crd);
                                        $new = $row->id;
                                    }
                                } else {
                                    $this->home_m->update($row->id, $crd);
                                    $accid = $row->id;
                                }
                            } else {
                                if ($accid != '0') {
                                    $this->home_m->update($accid, $crd);
                                } else {
                                    $crd['account_displayname'] = $response->name;
                                    $this->db->insert('account', $crd);
                                    $accid = $this->db->insert_id();
                                }
                            }
                        } else {
                            if($accid != 0){
                                $this->home_m->update($accid, $crd);
                            }else{
                                if ($row) {
                                    $this->home_m->update($row->id, $crd);
                                    $accid = $row->id;
                                }else{
                                    $crd['account_displayname'] = $response->name;
                                    $this->db->insert('account', $crd);
                                    $accid = $this->db->insert_id();
                                }
                            }
                        }
                        if($next == 'step4'){
                            redirect($goto .'/step5/'.$places.'/'.$accid.'/'.$landing.'/'.$from);
                        }else{
                            redirect($goto .'/step2/'.$places.'/'.$accid.'/'.$landing.'/'.$from);
                        }
                    } else {
                        redirect($goto .'/'.$next.'/'.$places.'/'.$accid.'/'.$landing.'/'.$from.'?retry=true&status=oauth_verifier_error');
                    }
                }else{
                    redirect($goto .'/'.$next.'/'.$places.'/'.$accid.'/'.$landing.'/'.$from.'?retry=true&status=oauth_verifier_error');
                }
            } else {
                /* oauth verifier failed */
                redirect($goto .'/'.$next.'/'.$places.'/'.$accid.'/'.$landing.'/'.$from.'?retry=true&status=oauth_verifier_error');
            }
        } else {
            /* twitter connect start */   
            $callback = site_url('home/twitter/'.$goto .'/'.$next.'/'.$places.'/'.$accid.'/'.$landing.'/'.$from);
                

            $params = array('oauth_callback' => $callback);
            $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/request_token', ''), $params);
            if ($code == 200) {
                /* token request available */
                $_SESSION['oauth'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
                $url = $tmhOAuth->url('oauth/authorize', '') . "?oauth_token={$_SESSION['oauth']['oauth_token']}&force_login=1";
                redirect($url);
            } else {
                /* token request not available */
                redirect($goto .'/'.$next.'/'.$places.'/'.$accid.'/'.$landing.'/'.$from.'?retry=true&status=twitter_start_error');
            }
        }
    }

    function twittresemme($goto, $next, $accid, $spotid, $likefollow, $crd) {
        if ($likefollow == 0) {
            $sqlaccount = "SELECT id FROM wooz_account
                            WHERE account_twid = '" . $crd['account_twid'] . "'";
            $queryaccount = $this->db->query($sqlaccount);
            $dataqueryaccount = $queryaccount->row();
            $likefollow = $dataqueryaccount->id;
        }
        $sql = "SELECT account_displayname,account_email,account_birthdate,account_phone,account_location,account_gender 
                    FROM wooz_account
                    WHERE id = '" . $accid . "'";
        $query = $this->db->query($sql);
        $dataquery = $query->row();
        if ($dataquery) {
            $crd['account_displayname'] = $dataquery->account_displayname;
            $crd['account_email'] = $dataquery->account_email;
            $crd['account_birthdate'] = $dataquery->account_birthdate;
            $crd['account_phone'] = $dataquery->account_phone;
            $crd['account_location'] = $dataquery->account_location;
            $crd['account_gender'] = $dataquery->account_gender;
            $crd['account_username'] = $this->convert->nicename($dataquery->account_displayname);
        } else {
            redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '?status=token_not_found&url=' . $next);
        }
        if ($likefollow != 0) {
            $this->home_m->update($likefollow, $crd);
        } else {
            $likefollow = $this->home_m->insert('account', $crd);
        }
        redirect('kiyora/registrasi/found/' . $spotid . '/' . $accid . '/' . $likefollow . '?url=' . $next);
    }

    function foursquare() {
        include(APPPATH . 'third_party/FoursquareAPI.class.php');
        $this->tpl['page'] = "home/foursquare";
        $this->tpl['title'] = "Foursquare Connect";

        $pageuri = $this->session->userdata('access');
        $userdata = $this->session->userdata('aidi');
        if (isset($userdata) && $userdata == '')
            $userdata = $this->session->userdata('kode');

        $this->tpl['user'] = $userdata;
        $goto = $this->uri->segment(3, 0);

        $getuser = $this->db->get_where('account', array('id' => $userdata))->row();
        #$setting = $this->db->get_where('setting', array('id' => 1))->row();
        #$consumer_key = $setting->foursquare_key;
        #$consumer_key_secret = $setting->foursquare_secret;
        #$redirect_uri = $setting->foursquare_callback;
        $consumer_key = $this->config->item('foursquare_api_key');
        $consumer_key_secret = $this->config->item('foursquare_secret_key');
        $redirect_uri = $this->config->item('foursquare_redirect_uri');

        $foursquare = new FoursquareAPI($consumer_key, $consumer_key_secret);

        if ($userdata != '') {
            if ($getuser->account_fs_token != '') { //jika ada fs token d database
                $foursquare->setAccessToken($getuser->account_fs_token);
                if (isset($goto) && $goto != '0')
                    if ($goto == 'beswandjarum')
                        redirect($goto . '/signupdone');
                    else
                        redirect('signupdone');
                else
                    redirect('profile');
            } else {
                if (array_key_exists("code", $_GET)) {
                    $token = $foursquare->GetToken($_GET['code'], $redirect_uri);
                }

                if (!isset($token)) {
                    redirect($foursquare->AuthenticationLink($redirect_uri));
                    // Otherwise display the token
                } else {
                    $up['account_fs_token'] = $token;
                    $this->db->where('id', $userdata);
                    $this->db->update('account', $up);
                    if (isset($goto) && $goto != '0')
                        if ($goto == 'beswandjarum')
                            redirect($goto . '/signupdone');
                        else
                            redirect('signupdone');
                    else
                        redirect('profile');
                }
            }
        } else {
            redirect('signup');
        }
    }

    function rfid() {
        $this->tpl['page'] = "home/rfid";
        $this->tpl['title'] = "RFID Registration";

        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;

        if (!$userdata) {
            redirect('login');
        }
        $row = null;

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('rfid', 'Serial Number', 'trim|required|callback_rfid_chk');

        if ($this->form_validation->run() === TRUE) {
            $this->db->where('id', $userdata);
            $db['account_rfid'] = strtolower($this->input->post('rfid', true));
            $this->db->update('account', $db);

            $row = $this->db->get_where('account', array('id' => $userdata))->row();
            redirect('woozer/' . $row->account_username);
        } else {
            $row = $this->db->get_where('account', array('id' => $userdata))->row();
            if ($row->account_rfid)
                $this->tpl['rfid'] = $row->account_rfid;

            $this->tpl['datalist'] = $row;
            $this->tpl['TContent'] = $this->load->view('home/rfid', $this->tpl, true);
        }
        $this->load->view('body', $this->tpl);
    }

    function rfid_chk($str) {
        $str = strtolower($str);
        $str = (int) $str;
        $row = $this->db->get_where('account', array('account_rfid LIKE ' => '%' . $str . '%'))->result();
        if (count($row) != 0) {
            $this->form_validation->set_message('rfid_chk', 'Serial already registered');
            return false;
        } else {
            $row = $this->db->get_where('card', array('card_number LIKE' => '%' . $str . '%', 'card_status !=' => 0))->result();
            if (count($row) != 0) {
                $this->form_validation->set_message('rfid_chk', 'Serial already registered');
                return false;
            } else {
                return true;
            }
            return true;
        }
    }

    function username_chk($str) {
        $userdata = $this->session->userdata('aidi');

        $str = strtolower($str);
        $row = $this->db->get_where('account', array('account_username' => $str))->row();
        if (count($row) != 0 && (isset($userdata) && $row->id != $userdata) && $str == 'admin' && $str == 'index' && $str == 'page') {
            $this->form_validation->set_message('username_chk', 'Username already used');
            return false;
        } else {
            return true;
        }
    }

    function email_available($str) {
        $this->db->where('account_email', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('account')->row();
        if ($new->jumlah == 0) {
            $this->form_validation->set_message('email_available', 'Email Not Registered');
            return false;
        } else {
            return true;
        }
    }

    function email_used($str) {
        $this->db->where('account_email', $str);
        $this->db->select('count(id) as jumlah');
        $new = $this->db->get('account')->row();
        if ($new->jumlah != 0) {
            $this->form_validation->set_message('email_used', 'Email Already Registered');
            return false;
        } else {
            return true;
        }
    }

    function __nicename($file) {
        $healthy = array(" ", ".", ",", "?", "&", "(", ")", "#", "!", "^", "%", "\'", "@", "}", "{", "]", "[", "|", "", "'", "/", "|", ":", '"', "<", ">", ";", "--", "+");
        $yummy = array("-", "",);
        $result = strtolower(str_replace($healthy, $yummy, trim($file)));

        $this->db->select('');
        $this->db->where('account_username', $result);
        $this->db->from('account');
        $c = $this->db->count_all_results();

        if ($c) {
            $this->db->select('');
            $this->db->like('account_username', $result, 'after');
            $this->db->from('account');
            $c = $this->db->count_all_results();

            if ($c) {
                $this->db->select('');
                $this->db->like('account_username', $result, 'after');
                $this->db->order_by('id', 'desc');
                $this->db->limit(1, 0);
                $res = $this->db->get('account')->row();

                $slice = explode('-', $res->account_username);
                $cc = count($slice);
                if (is_numeric($slice[$cc - 1])) {
                    $slice[$cc - 1] = $slice[$cc - 1] + 1;
                    $jadi = implode('-', $slice);
                } else {
                    $jadi = $result . '-1';
                }
            } else {
                $jadi = $result . '-1';
            }
        } else {
            $jadi = $result;
        }
        return $jadi;
    }

    function __tanggal($tanggal_f) {
        if (strstr($tanggal_f, ' ')) {
            $waktu_f = explode(' ', $tanggal_f);
            $tanggal_asli_f = explode('-', $waktu_f[0]);
            $waktu_asli_f = explode(':', $waktu_f[1]);
            $tanggal_cetak_f = date('M d, Y', mktime($waktu_asli_f[0], $waktu_asli_f[1], $waktu_asli_f[2], $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        } else {
            $tanggal_asli_f = explode('-', $tanggal_f);
            $tanggal_cetak_f = date('M d, Y', mktime(0, 0, 0, $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        }
        return $tanggal_cetak_f;
    }

    function __unlock() {
        $this->db->select('account.*, badge.badge_name, places.places_name');
        $this->db->where('badge.id', 3);
        $this->db->where('log_type', 2);
        $this->db->where('log_status', 1);
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->join('places', 'places.id = log.places_id', 'left');
        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
        $this->db->order_by('log.id', 'desc');
        $this->db->from('log');
        $new = $this->db->get()->result();

        echo '<ul>';
        foreach ($new as $row) {
            $this->db->select('distinct(places_id)');
            $this->db->where('account_id', $row->id);
            $this->db->where('log_type', 1);
            $this->db->where('log_status', 1);
            $this->db->where_in('places_id', array('3', '4', '5', '6'));
            $this->db->from('log');
            $count = $this->db->get()->result();


            echo '<li>';
            echo '(' . count($count) . ') ' . $row->account_displayname . ' - ' . $row->places_name;
            echo '</li>';
        }
        echo '</ul>';
    }

    function registrasi() {
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Home";

        $userdata = $this->session->userdata('aidi');

        $this->tpl['user'] = $userdata;

        if (isset($_GET['retry']) && $_GET['retry'] == 'true') {
            $this->tpl['retry'] = true;
        } else {
            $this->tpl['retry'] = false;
        }

        $this->tpl['TContent'] = $this->load->view('test/default', $this->tpl, true);
        $this->load->view('test/home', $this->tpl);
    }

}
