<?php

class cekrfid extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'wooz.in - ';
        $this->tpl['fb_page'] = 'http://www.facebook.com/pages/Woozin/157614240949967';
        $this->tpl['tw_page'] = 'wooz_in';

        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->library('sosmed');
        $this->load->model('convert');
		$this->lang->load('form_validation','en');

        $this->load->helper('url');

        $this->tpl['fb_url'] = $this->config->item('facebook_url');
        $this->tpl['tw_url'] = $this->config->item('twitter_url');
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');
        $url = $this->config->item('url');
        $loc = $this->input->get('loc', true);

        if (isset($loc) && $loc != '') {
            $this->tpl['loc'] = $loc;
            $this->loc = $loc;
        } else {
            $this->tpl['loc'] = 1;
            $this->loc = 1;
        }

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true
        ));

        $custom_page = $this->input->get('url', true);
        if ($custom_page != '') {
            $customs = $this->db->get_where('places', array('places_landing' => $custom_page))->row();
            if (count($customs) != 0) {
                $a = strtotime($customs->places_duedate);
                $b = time();
                if ($customs->places_duedate != '0000-00-00' && $a > $b) {
                    $this->custom_id = $customs->id;
                    $this->custom_url = $customs->places_landing;
                    $this->twitter_name = $customs->places_twitter;
                    $this->spot_id = $this->input->get('places', true);
                    $this->tpl['spot_id'] = $this->spot_id;
                    #$this->custom_model = $customs->places_model;
                    $this->tpl['customs'] = $customs->places_landing;
                    $this->tpl['css'] = $customs->places_css;
                    $this->tpl['background'] = $customs->places_backgroud;
                    $this->tpl['logo'] = $customs->places_logo;
                    $this->tpl['main_title'] = $customs->places_name;
                    $this->tpl['type_registration'] = 1;
                    if ($customs->id == 1) {
                        $this->tpl['card'] = 'card';
                    } else {
                        $this->tpl['card'] = 'Wristband';
                    }
                    if ($customs->places_fbid != '0')
                        $this->tpl['fb_page'] = $customs->places_fbid;
                    if ($customs->places_twitter != '0')
                        $this->tpl['tw_page'] = $customs->places_twitter;
                } else {
                    redirect('?status=custom_page_end_from_showing');
                }
            } else {
                redirect('?status=custom_page_not_registered');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $this->tpl['url'] = $this->input->get('url', true);
        $this->tpl['title'] = "Activate RFID";
        $this->tpl['error'] = 0;
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('serial', 'Wristband', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $rfid = $this->input->post('serial', true);
            $data_user = $this->landing_m->gettable('account_rfid', $rfid, 'account');
            if ($data_user) {
                redirect('cekrfid/home/' . $data_user->id . '/' . $data_user->account_username . '?url=' . $this->custom_url);
            } else {
                $this->tpl['error'] = 1;
            }
        }
        $this->tpl['TContent'] = $this->load->view('cekrfid/awal', $this->tpl, true);
        $this->load->view('cekrfid/home', $this->tpl);
    }

    function home($id = null, $name = null) {
        $this->tpl['url'] = $this->input->get('url', true);
        $this->tpl['title'] = "Profile";
		
        if ($id) {
            $this->tpl['cek_fb'] = 0;
			$data = $this->landing_m->gettable('id', $id, 'account');
            $this->tpl['info'] = $data;
			if($data->account_fbid && $data->account_token){
				$this->tpl['cek_fb'] = $this->sosmed->cek_token_fb($data->account_fbid, $data->account_token);
			}
		
			$this->load->helper(array('form'));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('left', 'Wristband', 'trim|callback_rfid_chk[' . $id . ']|required');
			if ($this->form_validation->run() === TRUE) {
				$rfid = $this->input->post('left', true);
				$string = substr($rfid, 0, 10);
				$datachange['account_rfid'] = $string;
				$this->db->where('id', $id);
				$this->db->update('account', $datachange);
				redirect('cekrfid/changerfid/'. $id.'/'.$name.'?url='.$this->custom_url);
			}
			
            $this->tpl['TContent'] = $this->load->view('cekrfid/profile', $this->tpl, true);
            $this->load->view('cekrfid/home', $this->tpl);
        } else {
            $this->tpl['error'] = 1;
            $this->tpl['TContent'] = $this->load->view('cekrfid/awal', $this->tpl, true);
            $this->load->view('cekrfid/home', $this->tpl);
        }
    }
	
	function changerfid($id = null, $name = null){
        $this->tpl['url'] = $this->input->get('url', true);
        $this->tpl['title'] = "Profile";
        $this->tpl['id'] = $id;		
        $this->tpl['name'] = $name;
		
        $this->tpl['TContent'] = $this->load->view('cekrfid/changerfid', $this->tpl, true);
        $this->load->view('cekrfid/home', $this->tpl);
	}

    function rfid_chk($str, $id) {
        $str = strtolower($str);
        if ($str != "") {
            $row = $this->db->get_where('account', array('account_rfid' => $str))->row();
            if (count($row) != 0) {
                $rows = $this->db->get_where('account', array('account_rfid' => $str, 'id' => $id))->row();
                if ($rows) {
                    return true;
                } else {
                    $this->form_validation->set_message('rfid_chk', 'Serial Wristband already registered');
                    return false;
                }
            } else {
                return true;
            }
        } else {
            #return true;
            $this->form_validation->set_message('rfid_chk', 'Serial RFID not found');
            return false;
        }
    }
	
    function accountfound() {
        $this->tpl['page'] = "step3";
        $this->tpl['title'] = "Find different account";
        
        $this->tpl['from'] = $this->uri->segments[3];
        $this->tpl['url'] = $this->input->get('url');
        $oldid = $this->uri->segments[4];
        $newid = $this->uri->segments[5];

        $this->tpl['user'] = $this->landing_m->gettable('id', $newid, 'account');
        $this->tpl['datalist'] = $this->landing_m->gettable('id', $oldid, 'account');
        
        $this->tpl['TContent'] = $this->load->view('cekrfid/foundaccount', $this->tpl, true);
        $this->load->view('cekrfid/home', $this->tpl);
    }

}
