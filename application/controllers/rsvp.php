<?php

class rsvp extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model(array('convert', 'landing_m'));
		$this->load->model('dashboard/visitor_m');
		$this->load->model('dashboard/user_m');

		$this->load->library('fungsi');
		$this->load->library('session');
		$this->load->library('curl');
		#date_default_timezone_set('Asia/Jakarta');
		date_default_timezone_set('America/Port_of_Spain');

		$this->load->helper('url');
		#$this->load->library('sosmed');

		$this->tpl['assets_url'] = $this->config->item('assets_url');
		$this->tpl['uploads_url'] = $this->config->item('uploads_url');

		$custom_page = $this->uri->segment(3, 0);
		if ($custom_page != '') {
			$customs = $this->landing_m->getplaceslanding($custom_page);
			if ($customs) {
				$this->custom_page = $custom_page;
				$this->tpl['landings'] = $custom_page;
				$this->custom_id = $customs->id;
				$this->custom_url = $customs->places_landing;
				$this->tpl['spot_id'] = $customs->id;
				$this->custom_model = $customs->places_model;
				$this->tpl['custom_model'] = $this->custom_model;
				$this->tpl['customs'] = $customs->places_landing;
				$this->tpl['background'] = $customs->places_backgroud;
				$this->tpl['logo'] = $customs->places_logo;
				$this->tpl['main_title'] = $customs->places_name;
				$this->tpl['type_registration'] = $customs->places_type_registration;
				$this->tpl['css'] = $customs->places_css;
			} else {
				redirect('?status=custom_page_end_from_showing');
			}
		} else {
			redirect('?status=custom_page_not_found');
		}
	}

	function index() {
		$this->home();
	}

	function homeend() {
		header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->tpl['main_title'] = 'Registration';
		$this->tpl['core'] = 'Registration';
		$this->tpl['title'] = "Inevents";
		$this->tpl['TContent'] = $this->load->view('rsvp/regisend', $this->tpl, true);
		$this->load->view('rsvp/home', $this->tpl);
	}

	function home() {
		if ($this->custom_id == 24) {
			$this->homeend();
		} else {
			header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
			header("Cache-Control: no-cache, must-revalidate");
			header("Pragma: no-cache");
			$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
			$this->output->set_header("Pragma: no-cache");
			$this->tpl['main_title'] = 'Re-Registration';
			$this->tpl['core'] = 'Re-Registration';
			$this->tpl['title'] = "Inevents";

			$this->load->helper(array('form'));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('id', 'Nama', 'trim|required');

			if ($this->form_validation->run() === TRUE) {
				$id = $this->input->post('id');
				// $staff = $this->input->post('staff');
				// $sql = "SELECT id FROM `wooz_form_regis_detail` where places_id = '24' and form_regis_id = '45' and account_id = '" . $id . "' and content = '" . $staff . "'";
				// $query = $this->db->query($sql);
				// $hasil = $query->row();
				// if ($hasil) {
				redirect('rsvp/restep2/' . $this->custom_url . '/' . $id);
				// } else {
				// 	$this->tpl['TContent'] = $this->load->view('rsvp/notfounduser', $this->tpl, true);
				// }
			} else {
				$this->tpl['visitor'] = $this->visitor_m->list_visitor_event($this->custom_id);
				$this->tpl['TContent'] = $this->load->view('rsvp/formreregis', $this->tpl, true);
			}
			$this->load->view('rsvp/home', $this->tpl);
		}
	}

	function restep2($custom, $id) {
		$this->tpl['main_title'] = 'Re-Registration';
		$this->tpl['core'] = 'Re-Registration';
		$this->tpl['title'] = "Inevents";
		$sql = "SELECT account_displayname FROM wooz_account where id = '" . $id . "'";
		$query = $this->db->query($sql);
		$name = $query->row();
		$this->tpl['hasil'] = $name;
		$this->tpl['email'] = $this->ambil_data_user($id, 92);
		$this->tpl['organization'] = $this->ambil_data_user($id, 93);
		$this->tpl['staff'] = $this->ambil_data_user($id, 94);
		// $this->tpl['mtn'] = $this->ambil_data_user($id, 82);
		// $this->tpl['product'] = $this->ambil_data_user($id, 84);
		// $this->tpl['rfid'] = $this->ambil_data_user($id, 86);
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('organization', 'Business Unit', 'trim|required');
		$this->form_validation->set_rules('staffnumber', 'Staff Number', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		// $this->form_validation->set_rules('interested', 'Interested', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
			$this->savedataadd(92, $id, $this->input->post('email'));
			$this->savedataadd(93, $id, $this->input->post('organization'));
			$this->savedataadd(94, $id, $this->input->post('staffnumber'));
			$in_data_rfid['account_rfid'] = $this->input->post('rfid');
			$this->db->where('id', $id);
			$this->db->update('account', $in_data_rfid);

			$in_landing_rfid['landing_rfid'] = $this->input->post('rfid');
			$this->db->where('account_id', $id);
			$this->db->where('landing_register_form', $this->custom_id);
			$this->db->update('landing', $in_landing_rfid);
			// if ($email) {
			$emailspot = $this->landing_m->emailplaces($this->custom_id);
			// xdebug($emailspot);die;
			// $this->mandrill($emailspot, $id, $this->input->post('email'));
			// }
			redirect('rsvp/restep3/' . $this->custom_url . '/' . $id);
		} else {
			$this->tpl['TContent'] = $this->load->view('rsvp/restep2', $this->tpl, true);
			$this->load->view('rsvp/home', $this->tpl);
		}
	}

	function restep3($custom, $id) {
		$this->tpl['main_title'] = 'Re-Registration';
		$this->tpl['core'] = 'Re-Registration';
		$this->tpl['title'] = "Inevents";
		$sql = "SELECT account_displayname FROM wooz_account where id = '" . $id . "'";
		$query = $this->db->query($sql);
		$name = $query->row();
		$this->tpl['hasil'] = $name;
		$this->tpl['rsvp'] = $this->ambil_data_user($id, 96);
		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('rsvp', 'rsvp', 'trim|required');
		// $this->form_validation->set_rules('interested', 'Interested', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
			$this->savedataadd(96, $id, $this->input->post('rsvp'));
			$in_data_rfid['account_lastupdate'] = date('Y-m-d H:i:s');
			$this->db->where('id', $id);
			$this->db->update('account', $in_data_rfid);

			redirect('rsvp/redone/' . $this->custom_url);
		} else {
			$this->tpl['TContent'] = $this->load->view('rsvp/restep3', $this->tpl, true);
			$this->load->view('rsvp/home', $this->tpl);
		}
	}

	function do_upload($file) {
		$name = str_replace(' ', '_', $_FILES[$file]['name']);
		$config = array(
			'upload_path' => FCPATH . "/uploads/user/",
			'upload_url' => base_url() . "uploads/user/",
			'allowed_types' => "gif|jpg|png|jpeg",
			'overwrite' => TRUE,
			'file_name' => $name,
		);

		$this->load->library('upload', $config);
		if ($this->upload->do_upload($file)) {
			return true;
		} else {
			return false;
		}
	}
	function savedataadd($form_regis, $id, $content) {
		$in_data_rfid['form_regis_id'] = $form_regis;
		$in_data_rfid['account_id'] = $id;
		$in_data_rfid['places_id'] = $this->custom_id;
		$in_data_rfid['content'] = $content;
		$in_data_rfid['date_add'] = date('Y-m-d H:i:s');

		$rowcontent = $this->user_m->cek_content_data($id, $this->custom_id, $form_regis);
		if ($rowcontent) {
			$this->db->where('id', $rowcontent->id);
			$this->db->update('form_regis_detail', $in_data_rfid);
		} else {
			$this->db->insert('form_regis_detail', $in_data_rfid);
		}
	}
	function redone() {
		$this->tpl['page'] = "Done";
		$this->tpl['title'] = "Personal Information";

		$this->tpl['page'] = "thankyou";
		$this->tpl['title'] = "Thankyou!";
		$this->tpl['TContent'] = $this->load->view('rsvp/redone', $this->tpl, true);
		$this->load->view('rsvp/home', $this->tpl);
	}
	function done() {
		$this->tpl['page'] = "Done";
		$this->tpl['title'] = "Personal Information";

		$accid = $this->uri->segment(3, 0);
		$user = $this->db->get_where('account', array('id' => $accid))->row();
		$this->tpl['logoutfb'] = 0;

		$this->tpl['page'] = "thankyou";
		$this->tpl['title'] = "Thankyou!";
		$this->tpl['TContent'] = $this->load->view('rsvp/signupdone', $this->tpl, true);
		$this->load->view('rsvp/home', $this->tpl);
	}

	function logout() {
		$this->session->unset_userdata('session_id');
		$this->session->unset_userdata('ip_address');
		$this->session->unset_userdata('user_agent');
		$this->session->unset_userdata('last_activity');
		#$this->_killFacebookCookies();
		$this->session->sess_destroy();
		session_destroy();
		unset($_SESSION);
		$past = time() - 3600;
		foreach ($_COOKIE as $key => $value) {
			setcookie($key, $value, $past, '/');
		}
		if ($this->custom_id == 21) {
			redirect('http://www.fashionfocus.org');
		}
		redirect('rsvp/home/' . $this->custom_url);
	}

	function mandrill($emailspot, $acc_id, $email_receive) {
		$hasil_email = 0;
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;

		try {
			$this->mandrill->init($this->config->item('mandrill_api_key'));
			$mandrill_ready = TRUE;
		} catch (Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
		}
		$attch = array();
		if ($mandrill_ready) {
			$attachemail = json_decode($emailspot->places_email_attach);
			if (count($attachemail) > 0) {
				$xy = 1;
				foreach ($attachemail as $rowemail) {
					$image = FCPATH . '/uploads/apps/email/' . $rowemail;
					$attch[] = $this->mandrill->getAttachmentStruct($image);
				}
			}
			// $email_receive = 'hayria76@yahoo.com'; //test email
			//Send us some email!
			$email = array(
				'html' => $emailspot->places_custom_email, //Consider using a view file
				'subject' => $emailspot->places_subject_email,
				'from_email' => 'donotreply@wooz.in',
				'from_name' => $emailspot->places_email,
				//'to' => array(array('email' => $this->ambil_data_user($acc_id,30)))
				'to' => array(array('email' => $email_receive)),
				"attachments" => $attch,
			);
			$result = $this->mandrill->messages_send($email);
			$hasil_email = $result[0]['_id'];
		}
		return $hasil_email;
	}

	function ambil_data_user($acc_id, $form_id) {
		$sql = "select a.account_id,a.content "
			. "from wooz_form_regis_detail a where a.form_regis_id = '" . $form_id . "' and account_id = '" . $acc_id . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$hasil = $data->content;
		} else {
			$hasil = '';
		}
		return $hasil;
	}

	function rfid_used($str, $account_id) {
		if (!isset($str)) {
			$str = $this->input->post('rfid');
		}
		$new = $this->db->get_where('account_sosmed', array('account_rfid' => $str))->row();
		if (count($new) >= 1) {
			$news = $this->db->get_where('account_sosmed', array('account_rfid' => $str, 'id' => $account_id))->row();
			if ($news) {
				return true;
			} else {
				$this->form_validation->set_message('rfid_used', 'RFID already registered');
				return false;
			}
		} else {
			return true;
		}
	}

	function checknameawalnew($nicename) {

		// process posted form data (the requested items like province)
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response
		if (strlen($keyword) >= 2) {
			$sqluser = "select a.id,a.account_displayname as hasil FROM wooz_account a inner join wooz_landing b on b.account_id = a.id
                        where a.account_displayname like '%" . $keyword . "%' and landing_register_form = '" . $this->custom_id . "'";
			$query = $this->db->query($sqluser);
			$dataquery = $query->result();

			if (!empty($dataquery)) {
				$data['response'] = 'true'; //Set response
				$data['message'] = array(); //Create array
				foreach ($dataquery as $row) {
					$data['message'][] = array(
						'value' => $row->hasil,
						'id' => $row->id,
						'organization' => $this->ambil_data_user($row->id, 93),
						'email' => $this->ambil_data_user($row->id, 92),
					);
				}
			}
			if ('IS_AJAX') {
				echo json_encode($data); //echo json string if ajax request
			} else {
				echo json_encode($data); //echo json string if ajax request
			}
		} else {
			echo json_encode($data);
		}
	}

}
