<?php

class woozer extends CI_controller {

    protected $tpl;
    protected $limit = 7;

    function woozer() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();

        //$this->load->library('model');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->library('twitter');
        $this->load->library('EpiFoursquare');
        $this->load->model('convert');
        $this->load->helper('url');
        $this->load->helper('date');
        
        $setting = $this->db->get_where('setting', array('id' => 1))->row();
        if (count($setting) === 0) {
            $app_id = NULL;
            $secret_key = NULL;
            $api_key = NULL;
            $fb_perms = NULL;
        } else {
            $app_id = $setting->facebook_id;
            $secret_key = $setting->facebook_secret;
            $api_key = $setting->facebook_key;
            $fb_perms = $setting->facebook_perms;
        }

        $this->tpl['fb_perms'] = $fb_perms;
        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));

        $fbdata = $this->facebook->getUser();
        if ($fbdata) {
            $this->tpl['fbdata'] = $fbdata;
            $n = array('next' => site_url('logout'));
            $next = site_url('logout');
            $logoutlink = sprintf("https://www.facebook.com/logout.php?api_key=%s&next=%s", $api_key, $next);
            $this->session->set_userdata('fblogout', $logoutlink);
        } else {
            $logoutlink = site_url('logout');
            $this->session->set_userdata('fblogout', $logoutlink);
        }
    }

    function index() {
        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        $this->tpl['core'] = 'woozer';
        $this->tpl['title'] = "Woozer";

        $id_data = $this->uri->segment(2, 0);
        $mode = $this->uri->segment(3, 0);
        $data = $this->uri->segment(4, 0);
        $acc = $this->db->get_where('account', array('id' => $userdata, 'account_status' => 1))->row();

        if (isset($mode) && $mode != "0")
            $mode = $mode; else
            $mode = "default";
        if (isset($data) && $data != "0" && $data != "page")
            $data = $data; else
            $data = "0";

        if ($userdata != '' && $acc->account_group == 9) {
            $this->tpl['recent'] = $this->convert->recent_admin();
        } else {
            $this->tpl['recent'] = $this->convert->recent();
        }

        if ($id_data && $id_data != 'page') {
            $user = $this->db->get_where('account', array('account_username' => $id_data, 'account_status' => 1))->row();

            if ($user) {
		        $this->tpl['title'] = $user->account_displayname.' | '.$this->tpl['title'];

				$this->tpl['datalist'] = $user;
                switch ($mode) {
                    case "history":
						$this->tpl['title'] .= ' History';
                        
						$this->db->select('');
                        $this->db->where('log.account_id', $user->id);
                        $this->db->where('log.log_status', 1);
                        $this->db->where('log.log_type', 1);
                        $this->db->join('places', 'places.id = log.places_id', 'left');
                        $this->db->order_by('log.id', 'desc');
                        $this->db->from('log');		
						$this->tpl['data'] = $this->db->get()->result();
                        $this->tpl['TContent'] = $this->load->view('woozer/history-list', $this->tpl, true);
                        break;
                    case "pin":
						$this->tpl['title'] .= ' Pin';
                        if ($data != '0') {
                            $this->db->select('');
                            $this->db->where('log.account_id', $user->id);
                            $this->db->where('log.log_hash', $data);
                            $this->db->where('log.log_status', 1);
                            $this->db->where('log.log_type', 2);
                            $this->db->join('places', 'places.id = log.places_id', 'left');
                            $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                            $this->db->order_by('log.id', 'desc');
                            $this->db->from('log');
                            $this->tpl['data'] = $this->db->get()->row();
                            $this->tpl['TContent'] = $this->load->view('woozer/pin-detil', $this->tpl, true);
                        } else {
                            $this->db->select('');
                            $this->db->where('log.account_id', $user->id);
                            $this->db->where('log.log_status', 1);
                            $this->db->where('log.log_type', 2);
                            $this->db->join('places', 'places.id = log.places_id', 'left');
                            $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                            $this->db->order_by('log.id', 'desc');
                            $this->db->from('log');
                            $this->tpl['data'] = $this->db->get()->result();
                            $this->tpl['TContent'] = $this->load->view('woozer/pin-list', $this->tpl, true);
                        }
                        break;
                    default:
						$this->tpl['title'] .= ' Profile';

						$this->db->select('');
                        $this->db->where('log.account_id', $user->id);
                        $this->db->where('log.log_status', 1);
                        $this->db->where('log.log_type', 1);
                        $this->db->join('places', 'places.id = log.places_id', 'left');
                        $this->db->join('account', 'account.id = log.account_id', 'left');
                        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                        $this->db->order_by('log.id', 'desc');
                        $this->db->limit(7, 0);
                        $this->db->from('log');
                        $this->tpl['history'] = $this->db->get()->result();

                        $this->db->select('');
                        $this->db->where('log.account_id', $user->id);
                        $this->db->where('log.log_status', 1);
                        $this->db->where('log.log_type', 2);
                        $this->db->join('account', 'account.id = log.account_id', 'left');
                        $this->db->join('places', 'places.id = log.places_id', 'left');
                        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                        $this->db->order_by('log.id', 'desc');
                        $this->db->limit(7, 0);
                        $this->db->from('log');
                        $this->tpl['pin'] = $this->db->get()->result();

                        if ($user->id == $userdata) {
                            if ($user->account_fbid != '' && $user->account_token != '') {
                                $url = 'https://graph.facebook.com/' . $user->account_fbid . '/likes?access_token=' . $user->account_token;
                                $info = json_decode($this->curl->simple_get($url));
                                $this->tpl['token'] = count($info->data);
                            } else {
                                $this->tpl['token'] = 0;
                            }

                            $this->tpl['TContent'] = $this->load->view('woozer/detil', $this->tpl, true);
                        } else {
                            $this->tpl['token'] = 0;
                            $this->tpl['TContent'] = $this->load->view('woozer/public', $this->tpl, true);
                        }
                        break;
                }
            } else {
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['value'] = "Woozer Not Found";
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
            }
        } else {
            $param['limit'] = $this->limit;
            $param['offset'] = $this->uri->segment(3, 0);

            $this->db->select('');
            $this->db->where('account_status !=', 0);
            $this->db->order_by("account_displayname", "asc");
            $this->db->limit($param['limit'], $param['offset']);
            $this->db->from('account');
            $views = $this->db->get()->result();

            $this->db->select('');
            $this->db->where('account_status !=', 0);
            $this->db->from('account');
            $total = $this->db->count_all_results();

            $this->load->library('pagination');
            $config['base_url'] = site_url('/woozer/page/');
            $config['total_rows'] = $total;
            $config['per_page'] = $this->limit;

            $this->pagination->initialize($config);

            $this->tpl['datalist'] = $views;
            $this->tpl['paging'] = $this->pagination->create_links();
            $this->tpl['TContent'] = $this->load->view('woozer/list', $this->tpl, true);
        }

        $this->load->view('body', $this->tpl);
    }

    function spot() {
        $userdata = $this->session->userdata('aidi');
        $this->tpl['user'] = $userdata;
        $this->tpl['core'] = 'woozpot';
        $this->tpl['title'] = "Woozpot";

        $id_data = $this->uri->segment(2, 0);
        $acc = $this->db->get_where('account', array('id' => $userdata, 'account_status' => 1))->row();

        $mode = $this->uri->segment(3, 0);
        if (isset($mode) && $mode != "0")
            $mode = $mode; else
            $mode = "default";

        if ($userdata != '' && $acc->account_group == 9) {
            $this->tpl['recent'] = $this->convert->recent_admin();
        } else {
            $this->tpl['recent'] = $this->convert->recent();
        }

        if ($id_data && $id_data != 'page') {
            $places = $this->db->get_where('places', array('places_nicename' => $id_data, 'places_status' => 1))->row();
			if($places->id == 453){
				redirect('http://heinekenhouselagos.com/');
			}
			
            if ($places) {
		        $this->tpl['title'] = $places->places_name.' | '.$this->tpl['title'];

				if ($places->places_parent != '0') {
                    $this->db->where('log.places_id', $places->id);
                } else {
                    $family = $this->convert->family($places->id);
                    $this->db->where_in('log.places_id', $family);
                }

                $this->db->select('distinct(account_id), account.*');
                $this->db->where('log.log_status', 1);
                $this->db->where('log.log_type', 1);
                $this->db->join('account', 'account.id = log.account_id', 'left');
                $this->db->order_by('log.id', 'desc');
                $this->db->from('log');
                $this->tpl['history'] = $this->db->get()->result();
                $this->tpl['datalist'] = $places;
                $this->tpl['TContent'] = $this->load->view('woozpot/detil', $this->tpl, true);
            } else {
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['value'] = "WoozPot Not Found";
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
            }
        } else {
            $param['limit'] = $this->limit;
            $param['offset'] = $this->uri->segment(3, 0);

            $this->db->select('');
            $this->db->where('places_parent', 0);
            $this->db->where('places_status !=', 0);
	        $this->db->where('places_type <', 3);
            $this->db->order_by("places_name", "asc");
            $this->db->limit($param['limit'], $param['offset']);
            $this->db->from('places');
            $views = $this->db->get()->result();

            $this->db->select('');
            $this->db->where('places_parent', 0);
            $this->db->where('places_status !=', 0);
	        $this->db->where('places_type <', 3);
            $this->db->from('places');
            $total = $this->db->count_all_results();

            $this->load->library('pagination');
            $config['base_url'] = site_url('/woozpot/page/');
            $config['total_rows'] = $total;
            $config['per_page'] = $this->limit;

            $this->pagination->initialize($config);

            $this->tpl['datalist'] = $views;
            $this->tpl['paging'] = $this->pagination->create_links();
            $this->tpl['TContent'] = $this->load->view('woozpot/list', $this->tpl, true);
        }

        $this->load->view('body', $this->tpl);
    }

    function profile() {
        $userdata = $this->session->userdata('aidi');
        if(isset($userdata)&&$userdata == '')
            $userdata = $this->session->userdata('kode');
        $this->tpl['user'] = $userdata;
        $this->tpl['core'] = 'profile';
        $this->tpl['title'] = "Woozer";

        #$id_data = $this->uri->segment(2, 0);
        $mode = $this->uri->segment(2, 0);
        $data = $this->uri->segment(3, 0);
        $acc = $this->db->get_where('account', array('id' => $userdata, 'account_status' => 1))->row();
        if(is_array($acc))
            $id_data = 0;
        else
            $id_data = $acc->account_username;
        if (isset($mode) && $mode != "0")
            $mode = $mode; 
        else
            $mode = "default";
        if (isset($data) && $data != "0" && $data != "page")
            $data = $data; 
        else
            $data = "0";

        if ($userdata != '' && $acc->account_group == 9) {
            $this->tpl['recent'] = $this->convert->recent_admin();
        } else {
            $this->tpl['recent'] = $this->convert->recent();
        }
//xdebug($id_data);xdebug($mode);die;
        if ($id_data && $id_data != 'page') {
            $user = $this->db->get_where('account', array('account_username' => $id_data, 'account_status' => 1))->row();

            if ($user) {
		        $this->tpl['title'] = $user->account_displayname.' | '.$this->tpl['title'];
                        $this->tpl['datalist'] = $user;
                switch ($mode) {
                    case "history":
                        $this->tpl['title'] .= ' History';

                        $this->db->select('');
                        $this->db->where('log.account_id', $user->id);
                        $this->db->where('log.log_status', 1);
                        $this->db->where('log.log_type', 1);
                        $this->db->join('places', 'places.id = log.places_id', 'left');
                        $this->db->order_by('log.id', 'desc');
                        $this->db->from('log');		
						$this->tpl['data'] = $this->db->get()->result();
                        $this->tpl['TContent'] = $this->load->view('woozer/history-list', $this->tpl, true);
                        break;
                    case "pin":
						$this->tpl['title'] .= ' Pin';
                        if ($data != '0') {
                            $this->db->select('');
                            $this->db->where('log.account_id', $user->id);
                            $this->db->where('log.log_hash', $data);
                            $this->db->where('log.log_status', 1);
                            $this->db->where('log.log_type', 2);
                            $this->db->join('places', 'places.id = log.places_id', 'left');
                            $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                            $this->db->order_by('log.id', 'desc');
                            $this->db->from('log');
                            $this->tpl['data'] = $this->db->get()->row();
                            $this->tpl['TContent'] = $this->load->view('woozer/pin-detil', $this->tpl, true);
                        } else {
                            $this->db->select('');
                            $this->db->where('log.account_id', $user->id);
                            $this->db->where('log.log_status', 1);
                            $this->db->where('log.log_type', 2);
                            $this->db->join('places', 'places.id = log.places_id', 'left');
                            $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                            $this->db->order_by('log.id', 'desc');
                            $this->db->from('log');
                            $this->tpl['data'] = $this->db->get()->result();
                            $this->tpl['TContent'] = $this->load->view('woozer/pin-list', $this->tpl, true);
                        }
                        break;
                    default:
                        $this->tpl['title'] .= ' Profile';

                        $this->db->select('');
                        $this->db->where('log.account_id', $user->id);
                        $this->db->where('log.log_status', 1);
                        $this->db->where('log.log_type', 1);
                        $this->db->join('places', 'places.id = log.places_id', 'left');
                        $this->db->join('account', 'account.id = log.account_id', 'left');
                        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                        $this->db->order_by('log.id', 'desc');
                        $this->db->limit(7, 0);
                        $this->db->from('log');
                        $this->tpl['history'] = $this->db->get()->result();

                        $this->db->select('');
                        $this->db->where('log.account_id', $user->id);
                        $this->db->where('log.log_status', 1);
                        $this->db->where('log.log_type', 2);
                        $this->db->join('account', 'account.id = log.account_id', 'left');
                        $this->db->join('places', 'places.id = log.places_id', 'left');
                        $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                        $this->db->order_by('log.id', 'desc');
                        $this->db->limit(7, 0);
                        $this->db->from('log');
                        $this->tpl['pin'] = $this->db->get()->result();

                        if ($user->id == $userdata) {
                            if ($user->account_fbid != '' && $user->account_token != '') {
                                $url = 'https://graph.facebook.com/' . $user->account_fbid . '/likes?access_token=' . $user->account_token;
                                #xdebug($url);die;
                                $info = json_decode($this->curl->simple_get($url));
                                if(isset($info) && $info != "")
                                    $this->tpl['token'] = count($info->data);
                                else
                                    $this->tpl['token'] = 0;                                
                            } else {
                                $this->tpl['token'] = 0;
                            }

                            $this->tpl['TContent'] = $this->load->view('woozer/detil', $this->tpl, true);
                        } else {
                            $this->tpl['token'] = 0;
                            $this->tpl['TContent'] = $this->load->view('woozer/public', $this->tpl, true);
                        }
                        break;
                }
            } else {
                $this->tpl['notice'] = 'Error Found';
                $this->tpl['value'] = "Woozer Not Found";
                $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
            }
        } else {
            $param['limit'] = $this->limit;
            $param['offset'] = $this->uri->segment(3, 0);

            $this->db->select('');
            $this->db->where('account_status !=', 0);
            $this->db->order_by("account_displayname", "asc");
            $this->db->limit($param['limit'], $param['offset']);
            $this->db->from('account');
            $views = $this->db->get()->result();

            $this->db->select('');
            $this->db->where('account_status !=', 0);
            $this->db->from('account');
            $total = $this->db->count_all_results();

            $this->load->library('pagination');
            $config['base_url'] = site_url('/woozer/page/');
            $config['total_rows'] = $total;
            $config['per_page'] = $this->limit;

            $this->pagination->initialize($config);

            $this->tpl['datalist'] = $views;
            $this->tpl['paging'] = $this->pagination->create_links();
            $this->tpl['TContent'] = $this->load->view('woozer/list', $this->tpl, true);
        }

        $this->load->view('body', $this->tpl);
    }

    function editprofile() {
        $this->tpl['page'] = "woozer/profile";
        $this->tpl['title'] = "Edit User Profile";
        $this->tpl['core'] = 'woozer';
        
        $userdata = $this->session->userdata('aidi');
        if(isset($userdata)&&$userdata == '')
            $userdata = $this->session->userdata('kode');        
        $this->tpl['user'] = $userdata;

        if (!$userdata) {
            redirect('login');
        }

        $row = null;

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('fullname', 'Display Name', 'trim|required');

        $pass = $this->input->post('password1', true);
        if ($pass != "") {
            $this->form_validation->set_rules('password1', 'password', 'required');
            $this->form_validation->set_rules('password2', 're-type password', 'required|matches[password1]');
        }

        if ($this->form_validation->run() === TRUE) {
            if ($pass != "") {
                $db['account_passwd'] = $this->fungsi->acak($this->input->post('password1', true));
            }

            if (isset($_FILES['avatar'])) {
                $j = count($_FILES['avatar']['name']);

                if ($j != '0') {
                    $file1 = $_FILES['avatar'];
                    $name1 = $_FILES['avatar']['name'];
                    $nama1 = $this->fungsi->nicename(date("ymdhis-") . $this->fungsi->fnc_get_name($name1));
                    $exe1 = $this->fungsi->fnc_get_extensi($name1);
                    $upl1 = $this->fungsi->thumb($file1, $nama1, $exe1);
                    if ($upl1) {
                        $db['account_avatar'] = $nama1 . '.' . $exe1;
                    }
                }
            }

            $db['account_displayname'] = $this->input->post('fullname', true);
            $db['account_rfid'] = $this->input->post('rfid', true);
            $db['account_birthdate'] = $this->input->post('birthday', true);
            $db['account_url'] = $this->input->post('url', true);
            $db['account_location'] = $this->input->post('location', true);
            $db['account_profile'] = $this->input->post('profile', true);

            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $db['account_lastupdate'] = mdate($datestring, $time);

            $this->db->where('id', $userdata);
            $this->db->update('account', $db);

            $row = $this->db->get_where('account', array('id' => $userdata))->row();
            $keyword['nama'] = $row->account_displayname;
            $keyword['nice'] = $row->account_username;
            $keyword['grup'] = $row->account_group;
            $this->session->set_userdata($keyword);

            redirect('editprofile?update=true');
        } else {
            if (isset($_REQUEST['update']) && $_REQUEST['update'] == 'true') {
                $this->tpl['data'] = true;
            } else {
                $this->tpl['data'] = false;
            }
            $row = $this->db->get_where('account', array('id' => $userdata))->row();
            $this->tpl['user'] = $row;
            $this->tpl['TContent'] = $this->load->view('woozer/profile', $this->tpl, true);
        }

        $this->load->view('body', $this->tpl);
    }    
  

    function card() {
        $this->tpl['page'] = "woozer/profile";
        $this->tpl['title'] = "Card List";
        $this->tpl['core'] = 'woozer';

        $userdata = $this->session->userdata('aidi');
        $row = $this->db->get_where('account', array('id' => $userdata))->row();
        $this->tpl['user'] = $row;

        if (!$userdata) {
            redirect('home/login');
        }

        $this->db->select('');
        $this->db->from('card');
        $this->db->where('account_id', $userdata);
        $this->db->where('card_status', 1);
        $card = $this->db->get();

        $this->tpl['card'] = $card;
        $this->tpl['TContent'] = $this->load->view('woozer/card-list', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function addcard() {
        $this->tpl['page'] = "woozer/profile";
        $this->tpl['title'] = "Add New Card";
        $this->tpl['core'] = 'woozer';

        $userdata = $this->session->userdata('aidi');
        $row = $this->db->get_where('account', array('id' => $userdata))->row();
        $this->tpl['user'] = $row;

        if (!$userdata) {
            redirect('home/login');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('rfid', 'RFID Card Number', 'trim|required');
        if ($this->form_validation->run() === TRUE) {
            $db['account_id'] = $userdata;
            $db['card_number'] = $this->input->post('rfid', true);
            $this->db->insert('card', $db);

            redirect('woozer/card');
        }

        $this->tpl['TContent'] = $this->load->view('woozer/addcard', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function editcard() {
        $this->tpl['page'] = "woozer/profile";
        $this->tpl['title'] = "Edit Card";
        $this->tpl['core'] = 'woozer';

        $userdata = $this->session->userdata('aidi');
        $row = $this->db->get_where('account', array('id' => $userdata))->row();
        $this->tpl['user'] = $row;

        if (!$userdata) {
            redirect('home/login');
        }

        $idcard = $this->uri->segment(3, 0);
        $rowcard = $this->db->get_where('card', array('id' => $idcard))->row();
        $this->tpl['card'] = $rowcard;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('rfid', 'RFID Card Number', 'trim|required');
        if ($this->form_validation->run() === TRUE) {
            $db['card_number'] = $this->input->post('rfid', true);
            $this->db->where('id', $idcard);
            $this->db->update('card', $db);

            redirect('woozer/card');
        }

        $this->tpl['TContent'] = $this->load->view('woozer/editcard', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function delcard() {
        $this->tpl['page'] = "woozer/profile";
        $this->tpl['title'] = "Card List";
        $this->tpl['core'] = 'woozer';

        $userdata = $this->session->userdata('aidi');
        $row = $this->db->get_where('account', array('id' => $userdata))->row();
        $this->tpl['user'] = $row;

        if (!$userdata) {
            redirect('home/login');
        }

        $idcard = $this->uri->segment(3, 0);
        $this->tpl['card'] = $idcard;

        $this->tpl['cards'] = $this->db->get_where('card', array('id' => $idcard))->row();

        if (isset($_GET['act']) && $_GET['act'] == 'delete') {
            $this->db->where('id', $idcard);
            $this->db->delete('card');
            redirect('woozer/card');
        }

        $this->tpl['TContent'] = $this->load->view('woozer/delcard', $this->tpl, true);
        $this->load->view('body', $this->tpl);
    }

    function username_chk($str) {
        $userdata = $this->session->userdata('aidi');

        $str = strtolower($str);
        $row = $this->db->get_where('account', array('account_username' => $str))->row();
        if (count($row) != 0 && (isset($userdata) && $row->id != $userdata) && $str == 'admin' && $str == 'index' && $str == 'page') {
            $this->form_validation->set_message('username_chk', 'Username already used');
            return false;
        } else {
            return true;
        }
    }

    function storecard() {
        $userdata = $this->session->userdata('aidi');
        $user = $this->db->get_where('account', array('id' => $userdata))->row();

        $insert = array('account_id' => $user->id, 'card_number' => $user->account_rfid);
        $this->db->insert('card', $insert);
    }

}

