<?php

class winner extends CI_controller {

	protected $tpl;

	function __construct() {
		parent::__construct();
		$this->tpl['TContent'] = null;
		$this->load->database();
		$this->load->model('dashboard/winner_m');
		//$this->load->library('model');
		$this->load->library('session');
		$this->load->library('curl');
		$this->load->library('fungsi');
		$this->load->library('sosmed');
		$this->load->helper('url');
		$this->lang->load('form_validation', 'en');
		$this->load->helper('date');
		$this->tpl['assets_url'] = $this->config->item('assets_url');
		date_default_timezone_set('America/Port_of_Spain');

		#$custom_page = $this->input->get('place', true);
		#$custom_page = 'First-Citizen-Sports-Awards';
		$custom_page = 'Sales-Awards-2017';
		if ($custom_page != '') {
			$customs = $this->db->get_where('places', array('places_landing' => $custom_page))->row();
			if ($customs) {
				$a = strtotime($customs->places_duedate);
				$b = time();
				if ($customs->places_duedate != '0000-00-00' && $a > $b) {
					$this->custom_id = $customs->id;
					$this->custom_url = $customs->places_landing;
					$this->spot_id = $this->input->get('places', true);
					$this->twitter_name = $customs->places_twitter;
					$this->tpl['spot_id'] = $this->input->get('place', true);
					#$this->custom_model = $customs->places_model;
					$this->tpl['customs'] = $customs->places_landing;
					$this->tpl['css'] = $customs->places_css;
					$this->tpl['background'] = $customs->places_backgroud;
					$this->tpl['logo'] = $customs->places_logo;
					$this->tpl['main_title'] = $customs->places_name;
					$this->tpl['type_registration'] = 1;
				} else {
					redirect('?status=' . $custom_page . '_custom_page_end_from_showing');
				}
			} else {
				redirect('?status=custom_page_not_registered');
			}
		} else {
			redirect('?status=custom_page_not_found');
		}
	}

	function index() {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$this->db->from('winner');
		$this->db->where('places_id', $this->custom_id);
		$this->db->where('id !=', 16);
		$this->db->order_by("id", "asc");
		$this->tpl['gate'] = $this->db->get()->result();
		//xdebug($this->tpl['gate']);die;
		$this->tpl['TContent'] = $this->load->view('winner/listplaces', $this->tpl, true);
		$this->load->view('winner/home', $this->tpl);
	}

	function listmc() {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$sqlwin = 'SELECT a.id,c.account_displayname,b.account_id,a.nama,b.presenter_status,a.data_status,a.datelock FROM wooz_winner a inner join wooz_presenter_account b on b.cat_winner_id = a.number inner join wooz_account c on c.id = b.account_id where b.places_id = 31 and a.places_id = 31';
		$querywin = $this->db->query($sqlwin);
		$this->tpl['gate'] = $querywin->result();

		$this->tpl['TContent'] = $this->load->view('winner/listmc', $this->tpl, true);
		$this->load->view('winner/home', $this->tpl);
	}

	function listcat($id) {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$sqlwin = 'SELECT cat_winner_id FROM `wooz_presenter_account` where account_id = "' . $id . '" and places_id = 31';
		$querywin = $this->db->query($sqlwin);
		$datacat = $querywin->result();
		$cat = array();
		if ($datacat) {
			foreach ($datacat as $row) {
				$cat[] = $row->cat_winner_id;
			}
		}

		$this->db->from('winner');
		$this->db->where('places_id', $this->custom_id);
		if ($cat) {
			$this->db->where_in('id', $cat);
		} else {
			$this->db->where('id !=', 16);
		}
		$this->tpl['gate'] = $this->db->get()->result();

		$this->tpl['TContent'] = $this->load->view('winner/listplaces', $this->tpl, true);
		$this->load->view('winner/home', $this->tpl);
	}

	function listwinner($id) {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$sqlwin = 'SELECT id,nama,data_status,datelock,toogle,number FROM wooz_winner where id = "' . $id . '"';
		$querywin = $this->db->query($sqlwin);
		$cat = $querywin->row();
		$this->tpl['cat'] = $cat;

		$sqlwin = 'SELECT b.id,b.account_displayname,a.presenter_status,c.content FROM wooz_presenter_account a inner join wooz_account b on b.id = a.account_id inner join wooz_form_regis_detail c on c.account_id = b.id  where cat_winner_id ="' . $cat->number . '" and c.form_regis_id = 74 order by a.presenter_status asc'; //79
		$querywin = $this->db->query($sqlwin);
		$presenter = $querywin->result();
		$this->tpl['presenter'] = array();
		if ($presenter) {
			$this->tpl['presenter'] = $presenter;
		}

		$sql = 'SELECT a.id,d.content,a.account_displayname,c.nama,b.id as win,b.data_status FROM `wooz_winner_account` b
                inner join wooz_account a on a.id = b.account_id
                inner join wooz_winner c on c.id = b.winner_id
                inner join wooz_form_regis_detail d on d.account_id = a.id
                where b.winner_id = "' . $id . ' " and d.form_regis_id = 74 ';
		$sql .= 'order by a.account_lastname asc';
		$query = $this->db->query($sql);
		$listuser = $query->result();
		$this->tpl['list'] = $query->result();

		$checkin = array();
		$notcheckin = array();
		foreach ($listuser as $row) {
			$getstatus = $this->winner_m->get_statususer($row->id);
			if ($getstatus) {
				$checkin[] = (object) array(
					'id' => $row->id,
					'account_displayname' => $row->account_displayname,
					'content' => $row->content,
				);
			} else {
				$notcheckin[] = (object) array(
					'id' => $row->id,
					'account_displayname' => $row->account_displayname,
					'content' => $row->content,
				);
			}
		}
		$this->tpl['listcheckin'] = $checkin;
		$this->tpl['listnotcheckin'] = $notcheckin;

		$this->tpl['TContent'] = $this->load->view('winner/listwinner', $this->tpl, true);
		$this->load->view('winner/home', $this->tpl);
	}

	function setwin($acc, $id) {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$in_data['data_status'] = 1;
		$in_data['date_take'] = date('Y-m-d H:i:s');
		$this->db->where('id', $id);
		$this->db->update('winner_account', $in_data);

		$sql = 'SELECT a.id,a.account_displayname,c.nama,b.id as win FROM `wooz_winner_account` b
                inner join wooz_account a on a.id = b.account_id
                inner join wooz_winner c on c.id = b.winner_id where b.id = "' . $id . '"';
		$query = $this->db->query($sql);
		$this->tpl['list'] = $query->row();

		$this->tpl['TContent'] = $this->load->view('winner/setwinner', $this->tpl, true);
		$this->load->view('winner/home', $this->tpl);
	}

	function done($id) {
		$in_data['data_status'] = 2;
		$this->db->where('id', $id);
		$this->db->update('winner_account', $in_data);
		redirect('winner');
	}

	function lock($id, $lock) {
		$this->tpl['page'] = 'Home';
		$this->tpl['title'] = 'User';
		$this->tpl['parent'] = 'Add';
		$this->tpl['pages'] = 1;
		$this->tpl['id'] = $id;

		$in_data['data_status'] = $lock;
		$in_data['datelock'] = date('Y-m-d H:i:s');

		$this->db->where('id', $id);
		$this->db->update('winner', $in_data);

		$this->tpl['TContent'] = $this->load->view('winner/signupdone', $this->tpl, true);
		$this->load->view('winner/home', $this->tpl);
	}

	function cektoogle($id = 0, $toogle = 0) {
		if ($id == 0) {
			redirect('dashboard');
		}
		$where['toogle'] = $toogle;
		$this->db->where('id', $id);
		$this->db->update('winner', $where);

		$this->tpl['page'] = 'Home';
		$this->tpl['title'] = 'User';
		$this->tpl['parent'] = 'Add';
		$this->tpl['pages'] = 1;
		$this->tpl['id'] = $id;

		$this->tpl['TContent'] = $this->load->view('winner/signupdone', $this->tpl, true);
		$this->load->view('winner/home', $this->tpl);
	}
	function show() {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$sql = "SELECT a.id,a.account_displayname,c.nama,b.id as win FROM `wooz_winner_account` b
                inner join wooz_account a on a.id = b.account_id
                inner join wooz_winner c on c.id = b.winner_id where b.data_status = 1 and b.date_add >= '2016-02-22 15:57:20'";
		$query = $this->db->query($sql);
		$this->tpl['list'] = $query->row();

		$this->tpl['TContent'] = $this->load->view('winner/showwinner', $this->tpl, true);
		$this->load->view('winner/home', $this->tpl);
	}

	function showjson() {
		$sql = "SELECT a.id,a.account_displayname,c.nama,b.id as win FROM `wooz_winner_account` b
                inner join wooz_account a on a.id = b.account_id
                inner join wooz_winner c on c.id = b.winner_id where b.data_status = 1 and b.date_add >= '2016-02-22 15:57:20'";
		$query = $this->db->query($sql);
		$data = $query->row();
		$json = array();
		if ($data) {
			array_push($json, array('name' => $data->account_displayname, 'category' => $data->nama));
		}
		echo json_encode(array("json" => $json));
	}

	function download($id) {
		$sqlwin = 'SELECT id,nama,data_status,datelock,toogle,number FROM wooz_winner where id = "' . $id . '"';
		$querywin = $this->db->query($sqlwin);
		$cat = $querywin->row();
		$this->tpl['cat'] = $cat;

		$sqlwin = 'SELECT b.id,b.account_displayname,a.presenter_status,c.content FROM wooz_presenter_account a inner join wooz_account b on b.id = a.account_id inner join wooz_form_regis_detail c on c.account_id = b.id  where cat_winner_id ="' . $cat->number . '" and c.form_regis_id = 74 order by a.presenter_status asc';
		$querywin = $this->db->query($sqlwin);
		$presenter = $querywin->result();
		$this->tpl['presenter'] = array();
		if ($presenter) {
			$this->tpl['presenter'] = $presenter;
		}

		$sql = 'SELECT a.id,d.content,a.account_displayname,c.nama,b.id as win,b.data_status FROM `wooz_winner_account` b
                inner join wooz_account a on a.id = b.account_id
                inner join wooz_winner c on c.id = b.winner_id
                inner join wooz_form_regis_detail d on d.account_id = a.id
                where b.winner_id = "' . $id . ' " and d.form_regis_id = 74 ';
		$sql .= 'order by a.account_lastname asc';
		$query = $this->db->query($sql);
		$listuser = $query->result();
		$this->tpl['list'] = $query->result();

		$checkin = array();
		$notcheckin = array();
		foreach ($listuser as $row) {
			$getstatus = $this->winner_m->get_statususer($row->id);
			if ($getstatus) {
				$checkin[] = (object) array(
					'id' => $row->id,
					'account_displayname' => $row->account_displayname,
					'content' => $row->content,
				);
			} else {
				$notcheckin[] = (object) array(
					'id' => $row->id,
					'account_displayname' => $row->account_displayname,
					'content' => $row->content,
				);
			}
		}
		$this->tpl['listcheckin'] = $checkin;
		$this->tpl['listnotcheckin'] = $notcheckin;
		$this->load->view('winner/downloadwinner', $this->tpl);
	}
}
