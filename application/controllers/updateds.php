<?php

class updateds extends CI_controller {

    protected $tpl;

    function updateds() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        //$this->load->library('model');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->library('twitter');
        $this->load->library('EpiFoursquare');
        $this->load->library('bitly', array(
            'bitly_login' => $this->config->item('bitly_login'),
            'bitly_apiKey' => $this->config->item('bitly_apiKey')
        ));
        $this->load->helper('url');
        $this->load->helper('date');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');

        include(APPPATH.'third_party/tmhOAuth.php');
        include(APPPATH.'third_party/tmhUtilities.php');

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));

        $custom_page = $this->config->item('custom_page');
        if ($custom_page != '') {
            $customs = $this->db->get_where('places', array('places_landing' => $custom_page))->row();
            if ($customs) {
                $this->custom_id = $customs->id;
					$this->tpl['customs'] = $custom_page;
					$this->tpl['css'] = $customs->places_css;
					#$this->tpl['main_title'] .= $customs->places_name;
            } else {
                redirect('http://wooz.in?status=custom_page_not_registered');
            }
        } else {
            redirect('http://wooz.in?status=custom_page_not_found');
        }

    }
/*
	 function index(){
		redirect();
	 }
 */
     function index(){
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Home";
		$this->tpl['body_class'] = '';
        $this->db->where('places_parent', $this->custom_id);
        $this->db->where('id != ', 314);
        $this->db->where_in('places_type', array("1","2","4"));
        $this->db->order_by('id', 'desc');
        $this->db->from('places');
        $this->tpl['spots'] = $this->db->get()->result();	
		
		if($this->custom_id){
			$this->db->where('id', $this->custom_id);
        	$this->db->from('places');
			$places_data = $this->db->get()->row();
			$this->tpl['body_class'] = $places_data->places_landing."-home";
		}	
        #$this->tpl['spots'] = $this->db->get_where('places', array('places_parent' => $this->custom_id,'places_type' => array("1","4")))->result();

        $this->tpl['TContent'] = $this->load->view('updateds/default', $this->tpl, true);
        $this->load->view('updateds/home', $this->tpl);
    }
	 
     function form() {        
        $this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
        $this->content['title'] = "| DS - tap";
        $places = $this->uri->segment(3, 0);
        
        $this->tpl['loc'] = $places;
		
		
        $this->db->where('id', $places);
        $this->db->from('places');
        $places_data = $this->db->get()->row();
		$this->tpl['places_name'] = $places_data->places_name;
        $this->tpl['title'] = $places_data->places_name."-update";
		$this->tpl['places_text'] = $places_data->places_cstatus_fb;
		if($places_data){
			$this->db->where('id', $places_data->places_parent);
        	$this->db->from('places');
			$places_data = $this->db->get()->row();
			$this->tpl['body_class'] = $places_data->places_landing."-update";
		}
		
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('serial', 'Serial RFID', 'trim|numeric|required');
        
        if (($this->form_validation->run() === TRUE)&&(isset($_POST['submit']))) {
                $rfid = $this->input->post('serial',true);     
                redirect('updateds/home?card='.$rfid.'&places='.$places);
        }
       
        $this->tpl['TContent'] = $this->load->view('updateds/rfid',$this->tpl,true);
        $this->load->view('updateds/home',$this->tpl);
    }  
    
    function home() {
        $tes_mk_sta = mktime();
        $api_key = $this->config->item('facebook_api_key');
        $cserial = strtolower($_POST['card']);
        $bserial = strtolower($_POST['booth']);
		if($bserial == 361){
			$bserial = 364;
		}
        if ($cserial && $bserial) {
            $venue = $this->db->get_where('places', array('id' => $bserial))->row();
            if ($venue) {
                $isMulti = $this->db->get_where('card', array('card_number' => $cserial, 'card_status !=' => 0))->row();
                if ($isMulti) {
                    if ($isMulti->card_status == 2)
                        $test = true;
					else
                        $test = false;
                    $user = $this->db->get_where('account', array('id' => $isMulti->account_id, 'account_status' => 1))->row();
                } else {
                    $test = false;
                    $user = $this->db->get_where('account', array('account_rfid' => $cserial, 'account_status' => 1))->row();
					if(empty($user)){
						$this->db->join('account', 'account.id = landing.account_id', 'left');
						$this->db->where('landing.landing_register_form', $this->custom_id);
						$this->db->where("landing.landing_rfid", $cserial);
						$this->db->from('landing');
						$user = $this->db->get()->row();
					}
                }

                if ($user) {
                    if (!$test) {
                        $this->db->where('account_id', $user->id);
                        $this->db->where('log_type', 1);
                        $this->db->where('log_status', 1);
                        $this->db->order_by('log_stamps', 'desc');
                        $this->db->limit(1, 0);
                        $this->db->from('log');
                        $last = $this->db->get()->row();

                        if ((isset($last->places_id))&&($last->places_id == $venue->id)) {
                            $marker = 1;
                        } else {
                            $marker = 1;
                        }
                        if(isset($last->log_stamps))
                        $margin = time() - mysql_to_unix($last->log_stamps);
                        else
                        $margin = time();

                        if ($margin > $marker) {
                            if (isset($_REQUEST['date']))
                                $date = $sdate; else
                                $date = date('Y-m-d');
                            if (isset($_REQUEST['time']))
                                $time = $stime; else
                                $time = date('h:i:s');

                            $db['account_id'] = $user->id;
                            $db['places_id'] = $venue->id;
                            $db['log_type'] = 1;
                            $db['log_date'] = $date;
                            $db['log_time'] = $time;
                            $db['log_hash'] = 'v' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                            $db['log_status'] = 1;
                            $upd = $this->db->insert('log', $db);
                            $new = $this->db->insert_id();

                            if ($venue->places_cstatus_fb == '' && $venue->places_cstatus_tw == '' && $venue->places_type != '3') {
                                if ($venue->places_avatar != "") {
                                    $options['picture'] = 'http://wooz.in'. $venue->places_avatar;
                                } else {
                                    $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                                }

                                $options['name'] = $venue->places_name;
                                $options['caption'] = 'wooz.in update';
                                $options['description'] = $user->account_displayname . ' just wooz.in @ ' . $venue->places_name;
                                $options['link'] = site_url() . '/woozpot/' . $venue->places_nicename;
                            } else {
                                $options['caption'] = '';
                            }

                            $data['status'] = false;
                            $data['message']['name'] = $user->account_displayname;
                            $data['message']['places'] = $venue->places_name;
                            $data['message']['fb_stream'] = '0';
                            $data['message']['fb_place'] = '0';
                            $data['message']['fb_foto'] = '0';
                            $data['message']['twitter'] = '0';
                            $data['message']['foursquare'] = '0';
                            $data['message']['badge'] = '0';
                            
                            if ($user->account_fbid != '' || $user->account_tw_token != '') {

                                if ($user->account_fbid && $user->account_token) {
                                    $options['access_token'] = $user->account_token;

                                    if ($venue->places_type == '3') {
                                        /* private place */
                                        if ($venue->places_cstatus_fb != '') {
                                            $options['message'] = $venue->places_cstatus_fb;
                                        }
                                        
										try{
											$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
											$hasil = json_encode($url);
											$id_hasil = json_decode($hasil);
											$this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));										
										}catch (FacebookApiException $e) {
											error_log($e);
										}	

                                        $data['status'] = true;
                                        $data['message']['fb_stream']= '1';
                                    } elseif ($venue->places_type == '4') {
                                        /* photo share upload */
										try{
											$url = "https://graph.facebook.com/me/albums?access_token=" . $user->account_token;
											#$info = json_decode($this->curl->simple_get($url));
											$info1 = json_decode($this->curl->simple_get($url));
											if(!isset($info1)){
												$hasil = 1;
											}
											else {
												$info = $info1;
												$info = $info->data;
												$up_to = false;

												foreach ($info as $alb) {
													if ($alb->name == $venue->places_name) {
													#if ($alb->name == $albumname) {
														$up_to = $alb->id;
													}
												}

												if ($up_to) {
													$album_id = $up_to;
												} else {
													$options['name'] = $venue->places_name;
													$options['access_token'] = $user->account_token;
													$url = $this->facebook->api("/$user->account_fbid/albums", 'post', $options);
													$hasil = json_encode($url);
													$album = json_decode($hasil);
													$album_id = $album->id;
												}
												
												if ($venue->places_cstatus_fb != '') {
													if ($venue->id == '182'){
														$add = "Having so much fun in the first ever music and ice cream festival in Indonesia!";
														$photo = $add .' '. $user->account_displayname . ' ' . $venue->places_cstatus_fb;
													}elseif ($venue->id == '187'){
														$add1 = "Come and join for the first ever music and ice cream festival in Indonesia!";
														$photo = $add1 .' '. $user->account_displayname . ' ' . $venue->places_cstatus_fb;
													} else {
														$photo = $user->account_displayname . ' ' . $venue->places_cstatus_fb;
														#$photo = $venue->places_cstatus_fb;
													}
														
												}
												/* upload photo */
												$this->facebook->setFileUploadSupport(true);
												
												$attachement = array(
													'access_token' => $user->account_token,
													'name' => $photo,
													'caption' => 'uploaded foto',
													'source' => '@' . FCPATH . $venue->places_avatar,
												);
												
												$upload = $this->facebook->api($album_id . '/photos', 'post', $attachement);
												$fbid = $upload['id'];

												/* like this photo */
												$access_token = $user->account_token;
												$options['uid'] = $user->account_fbid;
												$options['access_token'] = $user->account_token;
												$url = $this->facebook->api($fbid.'/likes', 'post', $options);
												
												/* get photo pid before tagging, convert return id form upload to pid */
												$response = $this->facebook->api(array(
															'access_token' => $user->account_token,
															'method' => 'fql.query',
															'query' => 'SELECT pid FROM photo WHERE object_id ="' . $fbid . '" ',
														));
						 
												foreach ($response as $pid) {
													$new_pid = $pid['pid'];
												}
											   
												$this->db->update('log', array('log_fb' => $new_pid), array('id' => $new));

												$data['status'] = true;
												$data['message']['fb_stream']= '1';
												$data['message']['fb_foto']= '1';
											}
										}catch (FacebookApiException $e) {
											error_log($e);
										}
                                        
                                    } elseif ($venue->places_type == '5') {
                                        /* like this, status and image only */
                                        if ($venue->places_avatar != "") {
                                            $options['picture'] = 'http://wooz.in'. $venue->places_avatar;
                                        } else {
                                            $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                                        }
                                        if ($venue->places_cstatus_fb != '') {
                                            $options['description'] = $venue->places_cstatus_fb;
                                        }

                                        $options['name'] = $venue->places_name;
                                        $options['link'] = site_url() . '/woozpot/' . $venue->places_nicename;
										
										try{
											$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
											$hasil = json_encode($url);
											$id_hasil = json_decode($hasil);
											$this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));
										}catch (FacebookApiException $e) {
											error_log($e);
										}	

                                        $data['status'] = true;
                                        $data['message']['fb_stream']= '1';
                                    } else {
                                        if ($venue->places_cstatus_fb != '') {
                                            if ($venue->places_avatar != "") {
												$options['picture'] = 'http://wooz.in' . $venue->places_avatar;
											} else {
												$options['picture'] = base_url() . 'assets/images/smalllogo.png';
											}
											
											$options['description'] = $venue->places_desc;
											
											if ($venue->id == '183' || $venue->id == '184' || $venue->id == '190' || $venue->id == '188'){
												$add = "Having so much fun in the first ever music and ice cream festival in Indonesia!";
												$options['message'] = $add .' '. $user->account_displayname . ' ' . $venue->places_cstatus_fb;
											}elseif ($venue->id == '180' || $venue->id == '181' || $venue->id == '185' || $venue->id == '186' || $venue->id == '191' || $venue->id == '189'){
												$add1 = "Come and join for the first ever music and ice cream festival in Indonesia!";
												$options['message'] = $add1 .' '. $user->account_displayname . ' ' . $venue->places_cstatus_fb;
											} elseif($venue->id == '198') {
												$options['message'] = $user->account_displayname . ' ' . $venue->places_cstatus_fb. 'ID:'. $user->account_displayname .'[username] di MTV EXIT Live in Bandung dengan Nu Green Tea (link: http://www.facebook.com/NuGreenTea) untuk bergabung melawan perdagangan orang. Bantu gue sebarin pesan ini dan ikut beraksi sekarang (link: http://act.mtvexit.org)';
											}elseif ($venue->id == '204' || $venue->id == '205' || $venue->id == '206'){
												$options['message'] = $venue->places_cstatus_fb;
											}elseif ($venue->id == '297'){
												$options['message'] = $user->account_displayname . ' ' . $venue->places_cstatus_fb;
											}else{
												#$options['message'] = $user->account_displayname . ' ' . $venue->places_cstatus_fb;
												$options['message'] = $venue->places_cstatus_fb;
											}
											if ($venue->id == '293'){
												$options['link'] = 'http://www.heineken.com/ng';
												$options['name'] = 'Indoor bar pod';
												$options['caption'] = 'update';											
											}elseif ($venue->id == '294'){
												#$options['link'] = 'http://www.heineken.com';
												$options['link'] = 'http://www.heineken.com/ng/ucl/landing/starplayer.aspx';
												$options['name'] = 'Starplayer room pod';
												$options['caption'] = 'update';											
											}elseif ($venue->id == '295'){
												#$options['link'] = 'http://www.heineken.com';
												$options['link'] = 'http://www.heineken.com/ng';
												$options['name'] = 'Champagne Fountain Pod';
												$options['caption'] = 'update';											
											}elseif ($venue->id == '297'){
												#$options['link'] = 'http://www.heineken.com';
												$options['link'] = 'http://www.heineken.com/ng';
												$options['name'] = 'Check in station';
												$options['caption'] = 'update';											
											}elseif ($venue->id == '317'){
												#$options['link'] = 'http://www.heineken.com';
												$options['link'] = 'http://www.heineken.com/ng';
												$options['name'] = 'Stairs landing pod';
												$options['caption'] = 'update';											
											}else{
												$options['name'] = $venue->places_name;
												$options['caption'] = 'wooz.in update';											
												$options['link'] = 'http://wooz.in/woozpot/' . $venue->places_nicename;
											}
                                        }

										try{
											$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
											$hasil = json_encode($url);
											$id_hasil = json_decode($hasil);
											$this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));
										}catch (FacebookApiException $e) {
											error_log($e);
											$result = $e->getResult();
											$data['message']['error'] = $result;
										}			
									/*	
											//update for page
											//token fan page dapur peduli = AAACGFg9ewNIBAFkhxi9AiiZB5iGT8BONQCOggDgSGAhGOvAzG2fuXpkfs1ctFGMZANtZChLp5RVQl95iE8ANCRwkYZBRibkFZAnpmL48mzdS9VaqPrYCw
											//id dapur peduli = 140338755994229
											if ($venue->id == '212'){
												if ($venue->places_cstatus_fb != '') {
													if ($venue->places_avatar != "") {
														$optionspage['picture'] = base_url() . $venue->places_avatar;
													} else {
														$optionspage['picture'] = base_url() . 'assets/images/smalllogo.png';
													}
													$optionspage['name'] = $venue->places_name;
													$optionspage['caption'] = 'wooz.in update';
													$optionspage['description'] = $venue->places_desc;
													$optionspage['message'] = $user->account_displayname . ' ' . $venue->places_cstatus_fb;
													$optionspage['link'] = site_url() . '/woozpot/' . $venue->places_nicename;
													$optionspage['access_token'] = 'AAACGFg9ewNIBAGdx5JaUzma7MVze6IbhH5nxUyqVChV2Ii5in4HZAEVNHYDtSPwuCmYWswcoZCZBM3h3Qa9wjo7G5eQxrEYhBBGZAN2zSZB0SFjOfjOw6 ';
												}

												try{
													$url = $this->facebook->api("/92040684078/feed", 'post', $optionspage);
												}catch (FacebookApiException $e) {
													error_log($e);
												}
											}
											//end 
											*/
                                        $data['status'] = true;
                                        $data['message']['fb_stream']= '1';
                                    }

                                    /* check in places */ 
                                    if ($venue->places_facebook != '0') {
                                        if ($venue->places_latitude != "") {
                                            $lat = $venue->places_latitude;
                                        } else {
                                            $lat = "-6.254639";
                                        }

                                        if ($venue->places_longitude != "") {
                                            $lng = $venue->places_longitude;
                                        } else {
                                            $lng = "106.799887";
                                        }

                     					$spot['message'] = $venue->places_cstatus_fb;
										$spot['place'] = $venue->places_facebook;
                                        $spot['coordinates'] = '{"latitude":"' . $lat . '", "longitude": "' . $lng . '"}';
                                        $spot['access_token'] = $user->account_token;

                                        $this->db->select('count(log_fb_places) as places');
                                        $this->db->where('account_id', $user->id);
                                        $this->db->where('log_fb_places !=', 0);
                                        $this->db->where('places_id', $venue->id);
                                        $this->db->from('log');
                                        $count = $this->db->get()->result();
                                        $places = $count[0]->places;
                                        //print_r($places);
                                        $this->db->select('distinct(log_fb_places) as places');
                                        $this->db->where('account_id', $user->id);
                                        $this->db->where('log_fb_places !=', 0);
                                        $this->db->where('places_id', $venue->id);
                                        $this->db->from('log');
                                        $count1 = $this->db->get()->result();
                                        $places1 = $count[0]->places;
                                        //print_r($count1);exit;
                                        if ($places=='1'&&$count1)//kl uda update
                                        {
                                            $data['status'] = true;
                                            $data['message']['fb_place']= '0';
                                        }
                                        else //kl blum update
                                        {
											try{
												$url = $this->facebook->api("/$user->account_fbid/checkins", 'post', $spot);
												$hasil = json_encode($url);
												$id_hasil = json_decode($hasil);
												$this->db->update('log', array('log_fb_places' => $id_hasil->id), array('id' => $new));
											}catch (FacebookApiException $e) {
												error_log($e);
											}

                                            $data['status'] = true;
                                            $data['message']['fb_place']= '1';
                                        }
                                    }


                                    /* update traps table 
                                    $this->load->library('arthurday');
                                    $this->arthurday->set_token($user->account_token);
                                    $this->arthurday->set_log($user->id, $new);
									*/
                                }

                                /* like stream
                                  if ($user->account_fbid && $user->account_token && $venue->id == '7') {
                                  $id_stream = '100001548649398_125840527477568';

                                  $options['uid'] = $user->account_fbid;
                                  $options['access_token'] = $user->account_token;
                                  $url = 'https://graph.facebook.com/' . $id_stream . '/likes';
                                  $info = $this->curl->simple_post($url, $options);

                                  $data['status'] = true;
                                  $data['message'] .= 'facebook done! ';
                                  }
                                 */

								#$data['wooz_badge'] = $this->__wooz_badge($user, $venue);
                                #$data['spot_badge'] = $this->__spot_badge($user, $venue);
                                #$data['count_badge'] = $this->__spesial_badge($user, $venue);
								if($venue->id == 286 || $venue->id == 353 || $venue->id == 354){
									#$this->holycow_point($user);
									$this->cek_holycow_point($user,$bserial);
								}
                                if ($user->account_tw_token && $user->account_tw_secret) {
                                    $link = site_url() . '/woozpot/' . $venue->places_nicename;
                                    if ($venue->places_cstatus_tw != '') {
                                        $options['description'] = $venue->places_cstatus_tw;
										#$options['description'] = $user->account_displayname . ' ' . $venue->places_cstatus_tw;
                                    } else {
                                        $shortened = $this->bitly->shorten($link);
                                        $options['description'] = "I'm wooz.in @ " . $venue->places_name . ' - ' . $shortened;
                                    }

                                    #include(APPPATH.'third_party/tmhOAuth.php');
                                    #include(APPPATH.'third_party/tmhUtilities.php');

                                    $consumer_key = $this->config->item('twiiter_consumer_key');
                                    $consumer_key_secret = $this->config->item('twiiter_consumer_secret');

                                    $tmhOAuth = new tmhOAuth(array(
                                        'consumer_key' => $consumer_key,
                                        'consumer_secret' => $consumer_key_secret,
                                        'user_token' => $user->account_tw_token,
                                        'user_secret' => $user->account_tw_secret,
                                    ));     
									
									if ($venue->places_type == '4'){
										$desc = $venue->places_cstatus_tw;
										$image = FCPATH . $venue->places_avatar;
										$code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update_with_media'),  array(
										'media[]' => "@{$image};type=image/jpeg;filename={$image}",
										'status' => $desc,
											), true, // use auth
											true  // multipart
										);
										$resp = json_decode($tmhOAuth->response['response'], true);
										if ($code == 200) {
											$new_tid = $resp['id_str'];
											$this->db->update('log', array('log_tw' => $new_tid), array('id' => $new));

											$data['status'] = true;
											$data['message']['twitter']= '1';
										}else{
											$data['status'] = true;
											$data['message']['twitter']= '0';
										}									
									}else {
										$code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update'), array(
										  'status' => $options['description']
										));		
										$resp = json_encode($tmhOAuth->response['response']);
										$resp1 = json_decode($resp);
										$twit_id = json_decode($resp1);
										if ($code == 200) {
											$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

											$data['status'] = true;
											$data['message']['twitter']= '1';
										}else{
											$data['status'] = true;
											$data['message']['twitter']= '0';
										}										
									}
                                }

                                if ($user->account_fs_token != '' && $venue->places_foursquare != '0' && $venue->places_type != '3') {
                                    $consumer_key = $this->config->item('foursquare_api_key');
                                    $consumer_key_secret = $this->config->item('foursquare_secret_key');
                                    $redirect_uri = base_url() . 'index.php/home/foursquare/';
                                    $accessToken = $user->account_fs_token;

                                    $foursquare = new EpiFoursquare($consumer_key, $consumer_key_secret, $accessToken);
                                    $foursquare->setAccessToken($user->account_fs_token);
                                   
									$Lat = $venue->places_latitude;
                                    $long = $venue->places_longitude;
                                    $ll = $Lat. "," .$long;
                                    $paramcheckin = array(
                                        'oauth_token' => $accessToken,
                                        'venueId' => $venue->places_foursquare,
                                        'shout' => $options['caption'],
                                        'broadcast' => 'public'
                                    );
                                    $checkin = $foursquare->post('/checkins/add', $paramcheckin);
                                    $foursquare->post('/checkins/add', $paramcheckin);
                                    $this->db->update('log', array('log_fs' => 1), array('id' => $new));

                                    $data['status'] = true;
                                    $data['message']['foursquare']= '1';
                                }

                                if (!$data['status']) {
                                    #$data['message'] = 'user not checked in anywhere';
                                    $data['message'] = $user->account_displayname;
                                }
                                $return_json = '{"status":"' . json_encode($data['status']) . '",';
                                $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                                $return_json = $return_json . '"message2":' . json_encode($data['message']['places']) . '}';
                                
                                echo $return_json;
                            } else {
                                $data = array(
                                    'status' => 0,
                                    'message' => array(
                                        'name' => $user->account_displayname,
                                        'error' =>"account didn't connect with facebook or twitter"
                                    )
                                );
                                $return_json = '{"status":"' . json_encode($data['status']) . '",';
                                $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                                $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';
                                
                                echo $return_json;
                            }
                        } else {
                            $data = array(
                                'status' => 0,
                                'message' => array(
                                    'name' => $user->account_displayname,
                                    'error' =>'time limit not passed'
                                )
                            );
                            $return_json = '{"status":"' . json_encode($data['status']) . '",';
                            $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                            $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

                            echo $return_json;
                        }
                    } else {
                        $data = array(
                            'status' => 1,
                            'message' => array(
                                'name' => $user->account_displayname,
                                'error' => "Test Done!"
                            )
                        );
                        $return_json = '{"status":"' . json_encode($data['status']) . '",';
                        $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                        $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

                        echo $return_json;
                    }
                } else {
                    $data = array(
                        'status' => 0,
                        'message' => array(
                                'name' => 'Please try again',
                                'error' => "card serial is not connected with any member"
                            )
                    );
                    $return_json = '{"status":"' . json_encode($data['status']) . '",';
                    $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                    $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

                    echo $return_json;
                }
            } else {
                $data = array(
                    'status' => 0,
                    'message' => array(
                        'name' => 'Please try again',
                        'error' => 'device serial is not connected with any venue'
                    )
                );
                $return_json = '{"status":"' . json_encode($data['status']) . '",';
                $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

                echo $return_json;
            }
        } else {
            $data = array(
                'status' => 0,
                'message' => array(
                        'name' => 'Please try again',
                        'error' => 'card serial and  device serial not received'
                    )
                );
                $return_json = '{"status":"' . json_encode($data['status']) . '",';
                $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

            echo $return_json;
        }
    }

    function __tanggal($tanggal_f) {
        if (strstr($tanggal_f, ' ')) {
            $waktu_f = explode(' ', $tanggal_f);
            $tanggal_asli_f = explode('-', $waktu_f[0]);
            $waktu_asli_f = explode(':', $waktu_f[1]);
            $tanggal_cetak_f = date('M d, Y', mktime($waktu_asli_f[0], $waktu_asli_f[1], $waktu_asli_f[2], $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        } else {
            $tanggal_asli_f = explode('-', $tanggal_f);
            $tanggal_cetak_f = date('M d, Y', mktime(0, 0, 0, $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        }

        return $tanggal_cetak_f;
    }

    function __wooz_badge($user, $venue) {
        $this->db->where('account_id', $user->id);
        $this->db->where('log_type', 1);
        $this->db->where('log_status', 1);
        $this->db->from('log');
        $count = $this->db->count_all_results();

        $wb = $this->db->get_where('badge', array('places_id' => 0, 'badge_type' => 2, 'badge_status' => 1))->result();
        foreach ($wb as $badge) {
            $have = $this->__got_badge($badge->id, $user->id);
            if ($have) {
                return true;
            } else {
                if ($badge->badge_value != '0' && $count == $badge->badge_value) {
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 'w' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;

                    $upd = $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($badge->badge_avatar != "") {
                        $options['picture'] = base_url() . 'uploads/badge/' . $badge->badge_avatar;
                    } else {
                        $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    }
                    $options['link'] = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                    $options['name'] = $badge->badge_name;
                    $options['caption'] = 'Wooz.in Pin Unlocked';
                    $options['description'] = ' Woohooo! '.  $user->account_displayname .' have just unlocked the "' . $badge->badge_name . '" badge on wooz.in' . $venue->places_name . ' - ' . $badge->badge_desc;

                    $message = "<h1>Dear " . $user->account_username . ",</h1>";
                    $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the" . $badge->badge_name . " pin @ " . $venue->places_name . ' - ' . $badge->badge_desc. "</p>";
                    $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
                    $message .= "<p>&nbsp;</p><p>- Wooz.In</p>";
                    
                    $data['status'] = false;

                    $config['charset'] = 'iso-8859-1';
                    $config['protocol'] = 'mail';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = FALSE;
                   
                    $this->load->library('email');

                    $this->email->initialize($config);
                    $this->email->from('donotreply@wooz.in', 'Wooz.In');
                    $this->email->to($user->account_email);

                    $this->email->subject("wooz.in pin unlocked!");
                    $this->email->message($message);

                    if (!$this->email->send()) {
                        $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {

                    
                    if ($user->account_fbid && $user->account_token) {
                        $options['access_token'] = $user->account_token;
						try{
							$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
							$hasil = json_encode($url);
							$id_hasil = json_decode($hasil);
							$this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));
						}catch (FacebookApiException $e) {
							error_log($e);
						}

                        $data['status'] = true;
                        $data['message']['badge']= '1';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {
                        $link = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                        #$shortened = $this->bitly->shorten($options['link']);
                        #$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $shortened;
						$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $link;
						#include(APPPATH.'third_party/tmhOAuth.php');
						#include(APPPATH.'third_party/tmhUtilities.php');
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }

                    return $data['status'];
                } 
                }
            }
        }
    }

    function __spesial_badge($user, $venue) {
		$jumlah = 0;
        $chk = $this->db->get_where('places', array('id' => $venue->id, 'places_status' => 1))->row();
        if ($chk->places_parent != '0') {
            $chk = $this->db->get_where('places', array('id' => $chk->places_parent, 'places_status' => 1))->row();
        }
        if (count($chk) != 0) {
            $chk2 = $this->__have_badge($chk->id);
            foreach ($chk2 as $badge) {
                $have = $this->__got_badge($badge->id, $user->id);
                if (!$have) {
                    if ($badge->badge_type == '5') {
                        $spots = json_decode($badge->badge_spot);
                        $this->db->select('distinct(places_id)');
                        $this->db->where('account_id', $user->id);
                        $this->db->where_in('log_type', array("1","6"));
                        $this->db->where('log_status', 1);
                        $this->db->where_in('places_id', $spots);
                        $this->db->from('log');
                        $count = $this->db->get()->result();    
                        if ($badge->badge_value == count($count)) {
                            $status = true;
                            $unlocked = 1;
                            $jumlah = $jumlah + 1;
                        } else {
                            $status = false;
                        }                        
                    }else {
                        $status = false;
                    }  
                } else {
                    $status = false;
                }

				if ($status) {
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;
                    $upd = $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($badge->badge_avatar != "") {
                        $options['picture'] = base_url() . 'uploads/badge/' . $badge->badge_avatar;
                    } else {
                        $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    }

                    $options['link'] = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                    $options['name'] = $badge->badge_name;
                    $options['caption'] = 'Wooz.in Pin Unlocked';
                    $options['description'] = ' Woohooo! '.  $user->account_displayname .' have just unlocked the "' . $badge->badge_name . '" badge on wooz.in' . $venue->places_name . ' - ' . $badge->badge_desc;
                    
                    $message = "<h1>Dear " . $user->account_displayname . ",</h1>";
                    $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the" . $badge->badge_name . " pin @ " . $venue->places_name . ' - ' . $badge->badge_desc. "</p>";
                    $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
                    $message .= "<p>&nbsp;</p><p>- Wooz.In</p>";
                    
                    $data['status'] = false;

                    $config['charset'] = 'iso-8859-1';
                    $config['protocol'] = 'mail';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = FALSE;
                    
                    $this->load->library('email');

                    $this->email->initialize($config);
                    $this->email->from('donotreply@wooz.in', 'Wooz.In');
                    $this->email->to($user->account_email);

                    $this->email->subject("wooz.in pin unlocked!");
                    $this->email->message($message);

                    if (!$this->email->send()) {
                        $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {
					
                    if ($user->account_fbid && $user->account_token) {
                        $options['access_token'] = $user->account_token;
                        $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                        $hasil = json_encode($url);
                        $id_hasil = json_decode($hasil);
                        $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));

                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {
                        #$shortened = $this->bitly->shorten($options['link']);
                        #$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $shortened;
						$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $options['link'];
						#include(APPPATH.'third_party/tmhOAuth.php');
						#include(APPPATH.'third_party/tmhUtilities.php');
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }
					return $data['status'];
                }
            }
          } 
        }
    }
	
    function __count_badge($user, $venue) {
		$jumlah = 0;
        $chk = $this->db->get_where('places', array('id' => $venue->id, 'places_status' => 1))->row();
        if ($chk->places_parent != '0') {
            $chk = $this->db->get_where('places', array('id' => $chk->places_parent, 'places_status' => 1))->row();
        }
        if (count($chk) != 0) {
            $chk2 = $this->__have_badge($chk->id);
            foreach ($chk2 as $badge) {
                $have = $this->__got_badge($badge->id, $user->id);
                if (!$have) {
                    if ($badge->badge_type == '2') {
                        $spots = json_decode($badge->badge_spot);
						$this->db->where('account_id', $user->id);
						$this->db->where('log_type', 1);
						$this->db->where('log_status', 1);
						$this->db->where_in('places_id', $spots);
						$this->db->from('log');
						$count = $this->db->get()->result();
						if ($badge->badge_value == count($count)) {
							$status = true;
							$unlocked = 1;
							$jumlah = $jumlah + 1;
						} else {
							$status = false;
						}
                    } else
						$status = false;
                } else {
                    $status = false;
                }

                if ($status) {
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;
                    $upd = $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($badge->badge_avatar != "") {
                        $options['picture'] = base_url() . 'uploads/badge/' . $badge->badge_avatar;
                    } else {
                        $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    }

                    $options['link'] = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                    $options['name'] = $badge->badge_name;
                    $options['caption'] = 'Wooz.in Pin Unlocked';
                    $options['description'] = ' Woohooo! '.  $user->account_displayname .' have just unlocked the "' . $badge->badge_name . '" badge on wooz.in' . $venue->places_name . ' - ' . $badge->badge_desc;
                    
                    $message = "<h1>Dear " . $user->account_displayname . ",</h1>";
                    $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the" . $badge->badge_name . " pin @ " . $venue->places_name . ' - ' . $badge->badge_desc. "</p>";
                    $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
                    $message .= "<p>&nbsp;</p><p>- Wooz.In</p>";
                    
                    $data['status'] = false;

                    $config['charset'] = 'iso-8859-1';
                    $config['protocol'] = 'mail';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = FALSE;
                    
                    $this->load->library('email');

                    $this->email->initialize($config);
                    $this->email->from('donotreply@wooz.in', 'Wooz.In');
                    $this->email->to($user->account_email);

                    $this->email->subject("wooz.in pin unlocked!");
                    $this->email->message($message);

                    if (!$this->email->send()) {
                        $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {
					
                    if ($user->account_fbid && $user->account_fb_token) {
                        $options['access_token'] = $user->account_fb_token;
                        $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                        $hasil = json_encode($url);
                        $id_hasil = json_decode($hasil);
                        $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));

                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {
                        #$shortened = $this->bitly->shorten($options['link']);
                        #$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $shortened;
						$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $options['link'];
						#include(APPPATH.'third_party/tmhOAuth.php');
						#include(APPPATH.'third_party/tmhUtilities.php');
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }
					return $data['status'];
                }
            }
          }
        }
    }

    function __spot_badge($user, $venue) {
		$jumlah = 0;
        $chk = $this->db->get_where('places', array('id' => $venue->id, 'places_status' => 1))->row();
        if ($chk->places_parent != '0') {
            $chk = $this->db->get_where('places', array('id' => $chk->places_parent, 'places_status' => 1))->row();
        }
        if (count($chk) != 0) {
            $chk2 = $this->__have_badge($chk->id);
            foreach ($chk2 as $badge) {
                $have = $this->__got_badge($badge->id, $user->id);
                if (!$have) {
                    if ($badge->badge_type == '3' || $badge->badge_type == '4') {
                        $spots = json_decode($badge->badge_spot);
                    } else {
                        $spots = $this->__spot_family($chk->id);
                    }
                    if ($badge->badge_type == '1'||$badge->badge_type == '3') {
                        $spots = json_decode($badge->badge_spot);
                        $this->db->select('distinct(places_id)');
                    }
                    
                    $this->db->where('account_id', $user->id);                    
					if ($badge->badge_type == '1') {
                        $this->db->where_in('log_type', array("1","6"));
                    }else{
                        $this->db->where('log_type', 1);
                    }
                    $this->db->where('log_status', 1);
					$sisi = '';
                    if ($badge->badge_type == '4') {
                        for($sp=0;$sp<count($spots);$sp++) {
                                if($sp===0) {
                        $sisi = '(places_id = '.$spots[$sp];
                                } else {
                        $sisi .= ' or places_id = '.$spots[$sp];
                                }
                        }
                        $sisi .= ')';
                        $this->db->where($sisi);
                    } else {
	                $this->db->where_in('places_id', $spots);
                    }
                    $this->db->from('log');
                    $count = $this->db->get()->result();
                    if ($badge->badge_type == '1' || $badge->badge_type == '3') {
                        if (count($spots) === count($count)) {
                            $status = true;
                            $unlocked = 1;
                            $jumlah = $jumlah + 1;
                        } else {
                            $status = false;
                        }
                    } elseif ($badge->badge_type == '4') {
                        if (count($count) === 1) {
                            $status = true;
                            $unlocked = 1;
                            $jumlah = $jumlah + 1;
                        } else {
                            $status = false;
                        }
                    } else {
                        if ($badge->badge_value == count($spots)) {
                            $status = true;
                            $unlocked = 1;
                            $jumlah = $jumlah + 1;
                        } else {
                            $status = false;
                        }
                    }
                } else {
                    $status = false;
                }

                if ($status) {
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;
                    $upd = $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($badge->badge_avatar != "") {
                        $options['picture'] = base_url() . 'uploads/badge/' . $badge->badge_avatar;
                    } else {
                        $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    }

                    $options['link'] = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                    $options['name'] = $badge->badge_name;
                    $options['caption'] = 'Wooz.in Pin Unlocked';
                    $options['description'] = ' Woohooo! '.  $user->account_displayname .' have just unlocked the "' . $badge->badge_name . '" badge on wooz.in' . $venue->places_name . ' - ' . $badge->badge_desc;
                    
					$message = "<h1>Dear " . $user->account_displayname . ",</h1>";
                    $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the" . $badge->badge_name . " pin @ " . $venue->places_name . ' - ' . $badge->badge_desc. "</p>";
                    $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
                    $message .= "<p>&nbsp;</p><p>- Wooz.In</p>";
                    
                    $data['status'] = false;

                    $config['charset'] = 'iso-8859-1';
                    $config['protocol'] = 'mail';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = FALSE;
                    
                    $this->load->library('email');

                    $this->email->initialize($config);
                    $this->email->from('donotreply@wooz.in', 'Wooz.In');
                    $this->email->to($user->account_email);

                    $this->email->subject("wooz.in pin unlocked!");
                    $this->email->message($message);

                    if (!$this->email->send()) {
                        $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {
					
                    if ($user->account_fbid && $user->account_token) {
                        $options['access_token'] = $user->account_token;
						try{
							$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
							$hasil = json_encode($url);
							$id_hasil = json_decode($hasil);
							$this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));
						}catch (FacebookApiException $e) {
							error_log($e);
						}

                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {
                        #$shortened = $this->bitly->shorten($options['link']);
                        #$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $shortened;
						$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $options['link'];
						#include(APPPATH.'third_party/tmhOAuth.php');
						#include(APPPATH.'third_party/tmhUtilities.php');
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }
                    return $data['status'];
                }
            }
          } //tambahan ubet
				/*	$sql = "select badge_name,badge_avatar from wooz_log,wooz_badge where wooz_log.badge_id not in ('0') and wooz_log.badge_id=wooz_badge.id and account_id = '".$user->id."' ";
                    $sql1 = $this->db->query($sql);
                    foreach($sql1->result_array() as $row)
					{
						$row1[]=$row;
					}
					$row2 = json_encode($row1);
					return $row2;		//end  */
        }
    }
	
	function cek_holycow_point($user,$venue){
		$datetoday = date("Y-m-d");
		$today = date("H:i:s");
		if(($today >= "00:00:00") && ($today <= "05:00:00")){
			$point = $this->db->get_where('holycow_point', array('account_id' => $user->id, 'places_id' => $venue, 'status' => 1, 'date like' => '%'.$datetoday.'%', 'time >=' => '00:00:00', 'time <=' => '05:00:00' ))->row();
			if(empty($point)){
				$this->holycow_point($user,$venue);
				return true;
			}else{
				return false;
			}
		}
		elseif(($today >= "11:00:00") && ($today <= "15:00:00")){
			$point = $this->db->get_where('holycow_point', array('account_id' => $user->id, 'places_id' => $venue, 'status' => 1, 'date like' => '%'.$datetoday.'%', 'time >=' => '11:00:00', 'time <=' => '15:00:00'))->row();
			if(empty($point)){
				$this->holycow_point($user,$venue);
				return true;
			}else{
				return false;
			}
		}
		elseif(($today >= "15:00:00") && ($today <= "24:00:00")){
			$point = $this->db->get_where('holycow_point', array('account_id' => $user->id, 'places_id' => $venue, 'status' => 1, 'date like' => '%'.$datetoday.'%', 'time >=' => '15:00:00', 'time <=' => '24:00:00'))->row();
			if(empty($point)){
				$this->holycow_point($user,$venue);
				return true;
			}else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	function holycow_point($user,$venue){
		$points['account_id'] = $user->id;
		$points['point'] = 1;
		$points['status'] = 1;
		$points['places_id'] = $venue;
		$points['time'] = date("H:i:s");
		$this->db->insert('holycow_point', $points);
        $new = $this->db->insert_id();

		#$user = $this->db->get_where('account', array('id' => $user))->row();
        $venue = $this->db->get_where('places', array('id' => $venue))->row();
		if ($user->account_fbid && $user->account_token) {
			$optionss['picture'] = base_url() . 'uploads/apps/holycow/Holycow-Bendera-Virtual.png';
			$optionss['message'] = 'Dapet bendera virtual lagi dari @steakholycow! Kumpulin terus aah…';
			$optionss['link'] = 'http://wooz.in/woozpot/'. $venue->places_nicename;
			$optionss['description'] = $venue->places_desc;
			$optionss['access_token'] = $user->account_token;
			
			try{
				$url = $this->facebook->api("/$user->account_fbid/feed", 'post', $optionss);
				$hasil = json_encode($url);
				$id_hasil = json_decode($hasil);  
				$this->db->update('holycow_point', array('log_fb' => $id_hasil->id), array('id' => $new));
			}catch(FacebookApiException $e){
				error_log($e);
			}
		}
		
		if ($user->account_tw_token && $user->account_tw_secret) {
			#$options['description'] = $venue->places_cstatus_tw;
			$options['description'] = 'Dapet bendera virtual lagi dari @steakholycow! Kumpulin terus aah…';
			$consumer_key = $this->config->item('twiiter_consumer_key');
            $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
			$tmhOAuth = new tmhOAuth(array(
				'consumer_key' => $consumer_key,
				'consumer_secret' => $consumer_key_secret,
				'user_token' => $user->account_tw_token,
				'user_secret' => $user->account_tw_secret,
			));
			$images = '/uploads/apps/holycow/Holycow-Bendera-Virtual.png';
			$image = FCPATH . $images;

			$code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update_with_media'),  array(
					'media[]' => "@{$image};type=image/jpeg;filename={$image}",
					'status' => $options['description'],
                ), true, // use auth
                true  // multipart
			);
			$resp = json_encode($tmhOAuth->response['response']);
			$resp1 = json_decode($resp);
			$twit_id = json_decode($resp1);
			if ($code == 200) {
				$this->db->update('holycow_point', array('log_tw' => $twit_id->id_str), array('id' => $new));

				$data['status'] = true;
				$data['message']['twitter']= '1';
			}else{
				$data['status'] = true;
				$data['message']['twitter']= '0';
			}
		}
	}

    function __have_badge($id) {
        $chk = $this->db->get_where('badge', array('places_id' => $id, 'badge_status' => 1))->result();
        return $chk;
    }

    function __got_badge($id, $user) {
        $chk = $this->db->get_where('log', array('badge_id' => $id, 'account_id' => $user, 'log_type' => 2, 'log_status' => 1))->row();
        if (count($chk) === 0)
            return false;
        else
            return true;
    }

    function __spot_family($id) {
        $this->db->select('');
        $this->db->where('id', $id);
        $this->db->where('booth_id !=', 0);
        $this->db->where('places_parent', 0);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $data1 = $this->db->get()->result();

        $this->db->select('');
        $this->db->where('booth_id !=', 0);
        $this->db->where('places_parent', $id);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $data2 = $this->db->get()->result();

        $data3 = array_merge($data1, $data2);
        foreach ($data3 as $row) {
            $x[] = $row->id;
        }
		if(!empty($x))
			return $x;
    }
    
    function __gwrefresh($userdata) {
        $getuser = $this->db->get_where('account', array('id' => $userdata))->row();

        $consumer_key = 'a64c308b64604d8ea683408f1c67b5ba';
        $consumer_key_secret = '08bee20693114586992ac0323df12fe0';
        $redirect_uri = base_url() . 'index.php/home/gowalla/';

        $gowalla = new GowallaPHP($consumer_key, $redirect_uri, $consumer_key_secret);

        if ($getuser->account_gw_token != '' || $getuser->account_gw_refresh != '' || $getuser->account_gw_time != '') {
            $daterefresh = date('D, d M Y H:i:s', $getuser->account_gw_time);
            if (time() > $getuser->account_gw_time) {
                $parms["refresh_token"] = $getuser->account_gw_refresh;
                $parms["client_id"] = $consumer_key;
                $parms["client_secret"] = $consumer_key_secret;
                $parms["access_token"] = $getuser->account_gw_token;

                $renew = $gowalla->refresh_token($parms);
                $data = json_decode($renew);

                $refresh_time = date('U', strtotime($data->expires_at));

                $up['account_gw_token'] = $data->access_token;
                $up['account_gw_refresh'] = $data->refresh_token;
                $up['account_gw_time'] = $refresh_time;

                $this->db->where('id', $getuser->id);
                $this->db->update('account', $up);

                return TRUE;
            } else {
                return TRUE;
            }
        } else {
            return TRUE;
        }
    }
/*
	function badge(){
		        $venue = $this->db->get_where('places', array('id' => '170', 'places_status' => 1))->row();
				$user = $this->db->get_where('account', array('id' => '6'))->row();
				$badge = $this->db->get_where('badge', array('id' => '21'))->row();
                    $db['account_id'] = $user->id;
                    $db['places_id'] = $venue->id;
                    $db['badge_id'] = $badge->id;
                    $db['log_type'] = 2;
                    $db['log_date'] = date('Y-m-d');
                    $db['log_time'] = date('h:i:s');
                    $db['log_hash'] = 's' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id;
                    $db['log_status'] = 1;
                    $upd = $this->db->insert('log', $db);
                    $new = $this->db->insert_id();

                    if ($badge->badge_avatar != "") {
                        $options['picture'] = base_url() . 'uploads/badge/' . $badge->badge_avatar;
                    } else {
                        $options['picture'] = base_url() . 'assets/images/smalllogo.png';
                    }

                    $options['link'] = site_url() . '/woozer/' . $user->account_username . '/pin/' . $db['log_hash'];
                    $options['name'] = $badge->badge_name;
                    $options['caption'] = 'Wooz.in Pin Unlocked';
                    $options['description'] = ' Woohooo! '.  $user->account_displayname .' have just unlocked the "' . $badge->badge_name . '" badge on wooz.in' . $venue->places_name . ' - ' . $badge->badge_desc;
                    
                    $message = "<h1>Dear " . $user->account_displayname . ",</h1>";
                    $message .= "<p>&nbsp;</p><p>Woohoo! Congrats you have just unlock the" . $badge->badge_name . " pin @ " . $venue->places_name . ' - ' . $badge->badge_desc. "</p>";
                    $message .= "<p>&nbsp;</p><p>Show it off to the world and check other cool badges and activity near you, just by log in to wooz.in</p>";
                    $message .= "<p>&nbsp;</p><p>- Wooz.In</p>";
                    
                    $data['status'] = false;

                    $config['charset'] = 'iso-8859-1';
                    $config['protocol'] = 'mail';
                    $config['mailtype'] = 'html';
                    $config['wordwrap'] = FALSE;
                    
                    $this->load->library('email');

                    $this->email->initialize($config);
                    $this->email->from('donotreply@wooz.in', 'Wooz.In');
                    $this->email->to($user->account_email);

                    $this->email->subject("wooz.in pin unlocked!");
                    $this->email->message($message);

                    if (!$this->email->send()) {
                        $this->tpl['value'] = "Maaf, email konfirmasi tidak dapat dikirim, mohon mencoba kembali";
                        $this->tpl['notice'] = 'Error Found';
                        $this->tpl['TContent'] = $this->load->view('notice', $this->tpl, true);
                    } else {
					
                    if ($user->account_fbid && $user->account_token) {
                        $options['access_token'] = $user->account_token;
                        $url = $this->facebook->api("/$user->account_fbid/feed", 'post', $options);
                        $hasil = json_encode($url);
                        $id_hasil = json_decode($hasil);
                        $this->db->update('log', array('log_fb' => $id_hasil->id), array('id' => $new));

                        $data['status'] = true;
                        $data['message'] = 'facebook done! ';
                    }

                    if ($user->account_tw_token && $user->account_tw_secret) {
                        #$shortened = $this->bitly->shorten($options['link']);
                        #$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $shortened;
						$options['description'] = 'Woohooo!  I have just unlock the "' . $badge->badge_name . '" badge on wooz.in ' . $options['link'];
						#include(APPPATH.'third_party/tmhOAuth.php');
						#include(APPPATH.'third_party/tmhUtilities.php');
									
						$consumer_key = $this->config->item('twiiter_consumer_key');
                        $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                                    
						$tmhOAuth = new tmhOAuth(array(
							'consumer_key' => $consumer_key,
							'consumer_secret' => $consumer_key_secret,
							'user_token' => $user->account_tw_token,
							'user_secret' => $user->account_tw_secret,
						));

						$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
						  'status' => $options['description']
						));

						$resp = json_encode($tmhOAuth->response['response']);
						$resp1 = json_decode($resp);
						$twit_id = json_decode($resp1);
						if ($code == 200) {
							$this->db->update('log', array('log_tw' => $twit_id->id_str), array('id' => $new));

							$data['status'] = true;
							$data['message']['twitter']= '1';
						}else{
							$data['status'] = true;
							$data['message']['twitter']= '0';
						}
                    }
					return $data['status'];
                }	
	}
*/

}

