<?php

class androidapicitizen extends CI_controller {

    protected $tpl;

    function androidapicitizen() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        $this->load->model('photobooth/photoboothcitizen_m');
        $this->load->library('curl');
        $this->load->library('fungsi');
        include(APPPATH . 'third_party/tmhOAuth.php');
        include(APPPATH . 'third_party/tmhUtilities.php');
        date_default_timezone_set('America/Port_of_Spain');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));
    }

    function main($placesid) {
        #$placesid = 259;
        $cserial = $this->input->get('no_rfid', true);
        if (!$cserial) {
            $cserial = $this->input->post('no_rfid', true);
        }
        if (isset($cserial) && $cserial != '') {
            $venue = $this->db->get_where('places', array('id' => $placesid))->row();
			$user = $this->db->get_where('account_pre', array('account_rfid' => $cserial))->row();
            if ($user) {
                $data = array(
                    'status' => 1,
                    'id' => $user->id,
                    'rfid' => $cserial,
                    'places_id' => $placesid,
                    'name' => $user->account_displayname
                );
                echo json_encode($data);
            } else { //belum registrasi
                $data = array(
                    'status' => 0,
                    'name' => 'not registered'
                );
                echo json_encode($data);
            }
        } else { //rfid tidak ada
            $data = array(
                'status' => 0,
                'name' => 'no rfid'
            );
            echo json_encode($data);
        }
    }

    function upload() {
        $upload_directory = '/home/socialite/public_html/uploads/mobilefirst/';

        $dataasli['post'] = json_encode($_POST);
        $dataasli['data_status'] = 0;
        $this->db->insert('data_post_ford', $dataasli);

        if (isset($_POST['upload'])) {
            if (!empty($_FILES['photo_path'])) {
                //check for image submitted
                if ($_FILES['photo_path']['error'] > 0) {
                    // check for error re file
                    $data = array(
                        'status' => 0,
                        'message' => 'no',
                        'name' => 'file photo error'
                    );
                    echo json_encode($data);
                } else {
                    $no_rfid = $this->input->post('no_rfid', true);
                    $acc_id = $this->input->post('acc_id', true);
                    $places_id = $this->input->post('places_id', true);
                    #$places_id = 459;
                    //move temp file to our server   
                    move_uploaded_file($_FILES['photo_path']['tmp_name'], $upload_directory . $_FILES['photo_path']['name']);
                    $venue = $this->db->get_where('places', array('id' => $places_id))->row();
					$user = $this->db->get_where('account_pre', array('account_rfid' => $no_rfid))->row();
                    if (!$user) {
                        $user = $this->db->get_where('account_pre', array('id' => $acc_id))->row();
                    }
					if($user){
						$data = array(
							'status' => 1,
							'message' => 'yes',
							'name' => $user->account_displayname
						);
						$upload = $this->uploads($no_rfid, $acc_id, $places_id, $_FILES['photo_path']['name']);
						echo json_encode($data);
					}else{
						$data = array(
							'status' => 0,
							'message' => 'no',
							'name' => 'user not found'
						);
						echo json_encode($data);
					}
                }
            } else {
                $data = array(
                    'status' => 0,
                    'message' => 'no',
                    'name' => 'file photo not found'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 0,
                'message' => 'no',
                'name' => 'Upload not found'
            );
            echo json_encode($data);
        }
    }

    function uploads($no_rfid, $acc_id, $places_id, $file_name) {
        $user = $this->db->get_where('account_pre', array('id' => $acc_id))->row();
        if (!$user) {
			$user = $this->db->get_where('account_pre', array('account_rfid' => $no_rfid))->row();
        }
        $venue = $this->db->get_where('places', array('id' => $places_id))->row();
        #$this->mergephoto($file_name, $venue->places_frame_url);
        if ($user) {
            $input = array(
                'account_id' => $user->id,
                'places_id' => $places_id,
                'photo_upload' => addslashes($file_name),
                'code_photo' => 'p' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id,
                'time_upload' => date("Y-m-d H:i:s"),
                'status' => 1
            );
            $photos = $this->photoboothcitizen_m->insert($input, 'photos_user');
            $image = '/home/socialite/public_html/uploads/mobilefirst/' . $file_name;
			$inputs['twit_id'] = 0;
			$inputs['pid'] = 0;
            if ($user->account_tw_token && $user->account_tw_secret) {
                $consumer_key = $this->config->item('twiiter_consumer_key');
                $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                $tmhOAuth = new tmhOAuth(array(
                    'consumer_key' => $consumer_key,
                    'consumer_secret' => $consumer_key_secret,
                    'user_token' => $user->account_tw_token,
                    'user_secret' => $user->account_tw_secret,
                ));
                $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update_with_media'), array(
                    'media[]' => "@{$image};type=image/jpeg;filename={$image}",
                    'status' => $venue->places_tw_caption,
                        #'status' => 'test',
                        ), true, // use auth
                        true  // multipart
                );
                $resp = json_encode($tmhOAuth->response['response']);
                $resp1 = json_decode($resp);
                $twit_id = json_decode($resp1);
                if ($code == 200) {
                    $inputs['twit_id'] = $twit_id->id_str;
                } else {
                    $inputs['twit_id'] = 0;
                }
            }

            if ($user->account_fbid && $user->account_token) {
                try {
                    /* check for photobooth album */
                    $url = "https://graph.facebook.com/me/albums?access_token=" . $user->account_token;
                    $info1 = json_decode($this->curl->simple_get($url));
                    if (!isset($info1)) {
                        $inputs['pid'] = 0;
                    } else {
                        $info = $info1;

                        $info = $info->data;
                        $up_to = false;
                        foreach ($info as $alb) {
                            if ($alb->name == $venue->places_album) {
                                $up_to = $alb->id;
                            }
                        }
                        if ($up_to) {
                            $album_id = $up_to;
                        } else {
                            try {
                                $options['name'] = $venue->places_album;
                                $options['access_token'] = $user->account_token;
                                $url = 'https://graph.facebook.com/' . $user->account_fbid . '/albums';
                                $album = json_decode($this->curl->simple_post($url, $options));
                                $album_id = $album->id;
                            } catch (FacebookApiException $e) {
                                error_log($e);
                                $inputs['pid'] = 0;
                            }
                        }
						
                        try {
                            $photo = ' '.$venue->places_fb_caption;
                            #$photo = 'test';
                            /* upload photo */
                            $this->facebook->setFileUploadSupport(true);
                            $attachement = array(
                                'access_token' => $user->account_token,
                                'caption' => 'uploaded foto',
                                'message' => $photo,
                                'source' => '@' . $image
                            );
                            $upload = $this->facebook->api($album_id . '/photos', 'POST', $attachement);
                            $new = $upload['id'];
                            /* get photo pid before tagging, convert return id form upload to pid */
                            $response = $this->facebook->api(array(
                                'access_token' => $user->account_token,
                                'method' => 'fql.query',
                                'query' => 'SELECT pid FROM photo WHERE object_id ="' . $new . '" ',
                            ));

                            foreach ($response as $pid) {
                                $new_pid = $pid['pid'];
                            }

                            if (isset($new_pid) && $new_pid != '0') {
                                $inputs['pid'] = $new_pid;
                            } else {
                                $inputs['pid'] = $new;
                            }
                        } catch (FacebookApiException $e) {
                            error_log($e);
                            $inputs['pid'] = 0;
                        }
                    }
                } catch (FacebookApiException $e) {
                    error_log($e);
                    $inputs['pid'] = 0;
                }
            }
            $this->photoboothcitizen_m->update($photos, $inputs);
            $this->photoboothcitizen_m->insertlog($photos);

            return true;
        } else {
            return false;
        }
    }

    function fb_fan_page($copytext, $file_name, $account_fb_id, $account_fb_token, $album_id) {
        $hasil = 0; //awal
        try {
            /* get album photo */
            $text = $copytext->places_cstatus_fb;
            /* upload photo */
            $this->facebook->setFileUploadSupport(true);
            $attachement = array(
                'access_token' => $account_fb_token,
                'caption' => 'uploaded foto',
                'message' => $text,
                'no_story' => 1,
                'source' => '@' . $file_name
            );
            $upload = $this->facebook->api($album_id . '/photos', 'POST', $attachement);
            $hasil = $upload['id'];
        } catch (FacebookApiException $e) {
            error_log($e);
            $hasil = 1; //list album error
        }
        return $hasil;
    }

    function cekuser($no_rfid, $placesid) {
        $user = $this->db->get_where('account', array('account_rfid' => $no_rfid))->row();
        if ($user) {
            return $user;
        } else {
            $this->db->select('wooz_account.*');
            $this->db->join('account', 'account.id = landing.account_id', 'left');
            $this->db->where('landing.landing_register_form', $placesid);
            $this->db->where("landing.landing_rfid", $no_rfid);
            $this->db->from('landing');
            $acc = $this->db->get()->row();
            if ($acc) {
                return $acc;
            } else {
                return false;
            }
        }
    }

}
