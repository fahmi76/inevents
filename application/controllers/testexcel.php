<?php

class testexcel extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model(array('convert', 'landing_m'));

		$this->load->library('fungsi');
		$this->load->library('session');
		$this->load->library('curl');
		date_default_timezone_set('Asia/Jakarta');

		$this->load->helper('url');
		#$this->load->library('sosmed');

		$this->tpl['assets_url'] = $this->config->item('assets_url');
		$this->tpl['uploads_url'] = $this->config->item('uploads_url');
	}

	function index() {
		$tabel = "<style type='text/css'>
    .datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 2px solid #006699; -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px; }.datagrid table td, .datagrid table th { padding: 6px 20px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; color:#FFFFFF; font-size: 16px; font-weight: bold; border-left: 2px solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00496B; border-left: 2px solid #E1EEF4;font-size: 13px;border-bottom: 3px solid #E1EEF4;font-weight: normal; }.datagrid table tbody .alt td { background: #E1EEF4; color: #00496B; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }
</style>";
		$tabel .= '<div class="datagrid"><table>
            <thead><tr><th>No</th><th>Image</th><th>Date</th></tr></thead>
            <tbody>';

		set_time_limit(0);
		$dirname = '/Applications/XAMPP/xamppfiles/htdocs/ineventsv2/photo/photo/';
		$dirdestination = '/Applications/XAMPP/xamppfiles/htdocs/byants/beta/uploads/images/';
		$ffs = scandir($dirname);
		$x = 1;
		foreach ($ffs as $ff) {
			if ($ff != '.DS_Store' && $ff != '..' && $ff != '.' && $ff != 'Thumbs.db') {
				$tabel .= "<tr>
                    <td>" . $x . "</td>
                    <td>" . $ff . "</td>
                    <td>" . date("d F Y H:i:s.", filemtime($dirname . $ff)) . "</td>
                </tr>";

				// echo $x.'-';
				// echo $ff.'<br/>';
				// echo "Last modified: ".date("F d Y H:i:s.",filemtime($dirname.$ff));
				// echo "<br />";
				$x++;
			}
		}
		$tabel .= '</tbody>
        </table></div>';
		echo $tabel;
	}

	function list_tabel() {

		$tabel = "<style type='text/css'>
    .datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 2px solid #006699; -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px; }.datagrid table td, .datagrid table th { padding: 6px 20px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; color:#FFFFFF; font-size: 16px; font-weight: bold; border-left: 2px solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00496B; border-left: 2px solid #E1EEF4;font-size: 13px;border-bottom: 3px solid #E1EEF4;font-weight: normal; }.datagrid table tbody .alt td { background: #E1EEF4; color: #00496B; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }
</style>";
		$tabel .= '<div class="datagrid"><table>
            <thead><tr><th>Date</th><th>#RSVP</th><th># WITH GUEST</th><th># SOUTH SHUTTLE</th><th># TAKE THE ELEVATOR HOME</th></tr></thead>
            <tbody>';
		foreach ($data as $row) {
			$tabel .= "<tr>
                    <td>" . $row['date'] . "</td>
                    <td>" . $row['rsvp'] . "</td>
                    <td>" . $row['guest'] . "</td>
                    <td>" . $row['shuttle'] . "</td>
                    <td>" . $row['elevator'] . "</td>
                </tr>";
		}
		$tabel .= '</tbody>
        </table></div>';
		echo $tabel;
	}

	function download() {
		$id = 24;
		$this->load->library('excel');
		$this->load->model('dashboard/user_m');
		$this->load->model('dashboard/visitor_m');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

		$objPHPExcel->setActiveSheetIndex(0);

		$tabel = $this->user_m->list_tabel($id);
		if ($id == 20) {
			$header = array('First Name', 'Last Name');
		} else {
			$header = array('Name');
		}
		foreach ($tabel as $tabelrow) {
			$header[] = $tabelrow->field_name;
		}
		$header[] = 'Date add';

		$col = 0;
		foreach ($header as $field) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			$col++;
		}
		$user = $this->datauser();
		// Fetching the table data
		$row = 2;
		foreach ($user as $data) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $data['name']);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $this->get_datauser(43, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $this->get_datauser(44, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $this->get_datauser(45, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $this->get_datauser(46, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $this->get_datauser(47, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $this->get_datauser(48, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $this->get_datauser(49, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $this->get_datauser(50, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $this->get_datauser(51, $data['id']));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $data['date']);

			$row++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		// Sending headers to force the user to download the file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="datauser.xls"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}

	function ambil_data_user($acc_id, $form_id) {
		$sql = "select a.account_id,a.content "
			. "from wooz_form_regis_detail a where a.form_regis_id = '" . $form_id . "' and account_id = '" . $acc_id . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$hasil = $data->content;
		} else {
			$hasil = '';
		}
		return $hasil;
	}

	function regis() {
		$tabel = '<table>
            <thead><tr><th>No</th><th>Nama</th><th>Organization</th><th>Business Tel. No.</th><th>Mobile Tel. No.</th><th>Email Address</th><th>Products & Services</th><th>Country</th><th>Category</th><th>Date add</th></tr></thead>
            <tbody>';
		$sql = "SELECT b.id,b.account_displayname,a.date_add FROM `wooz_form_regis_detail` a inner join wooz_account b on b.id = a.account_id WHERE a.places_id = 34 and a.form_regis_id = 85 and a.date_add like '%2017-07-09%' ORDER BY `b`.`id`  DESC";
		$query = $this->db->query($sql);
		$hasil = $query->result();
		$x = 1;
		foreach ($hasil as $row) {
			$tabel .= "<tr>
                    <td>" . $x++ . "</td>
                    <td>" . $row->account_displayname . "</td>
                    <td>" . $this->ambil_data_user($row->id, 80) . "</td>
                    <td>" . $this->ambil_data_user($row->id, 81) . "</td>
                    <td>" . $this->ambil_data_user($row->id, 82) . "</td>
                    <td>" . $this->ambil_data_user($row->id, 83) . "</td>
                    <td>" . $this->ambil_data_user($row->id, 84) . "</td>
                    <td>" . $this->ambil_data_user($row->id, 85) . "</td>
                    <td>" . $this->ambil_data_user($row->id, 87) . "</td>
                    <td>" . $row->date_add . "</td>
                </tr>";
		}
		$tabel .= '</tbody>
        </table>';
		echo $tabel;
	}
}
