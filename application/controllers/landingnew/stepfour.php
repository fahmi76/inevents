<?php

class stepfour extends CI_controller {

    protected $content;
    protected $perms;

    function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'wooz.in - ';
        $this->tpl['fb_page'] = 'http://www.facebook.com/pages/Woozin/157614240949967';
        $this->tpl['tw_page'] = 'wooz_in';

        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->model('convert');
        $this->load->library('sosmed');
        date_default_timezone_set('Asia/Jakarta');

        $this->load->helper('url');

        $this->tpl['fb_url'] = $this->config->item('facebook_url');
        $this->tpl['tw_url'] = $this->config->item('twitter_url');
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true
        ));

        $custom_page = $this->input->get('url', true);
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;
                
                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                    $this->facebook_name = $customs->places_fb_page_id;
                }
                if ($customs->places_twitter != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twitter_name = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $from = $this->input->get('from', TRUE);
        $url = $this->input->get('url', TRUE);
        $acc = $this->input->get('acc', TRUE);

        if ($from && $url && $acc) {
            $this->step4($from, $acc, $url);
        } else {
            redirect('?status=custom_page_not_registered');
        }
    }

    function step4($from, $acc, $url) {
        $data_user = $this->landing_m->gettable('id', $acc, 'account');
        $this->tpl['likefollow'] = $this->input->get('likefollow', true);

        $this->tpl['page'] = "step4";
        $this->tpl['title'] = "Get More info?";
        $this->tpl['id'] = $acc;
        $this->tpl['from'] = $from;
        $this->tpl['url'] = $url;
        $this->tpl['followerror'] = $this->input->get('follow', true);
        $this->tpl['user_name'] = $data_user->account_displayname;
        $this->tpl['fbid'] = $data_user->account_fbid;
        $this->tpl['fb_token'] = $data_user->account_token;
        if ($from == 'fb') {
            $this->tpl['likefollow2'] = $this->sosmed->follow_tw($data_user->account_tw_token, $data_user->account_tw_secret, $data_user->account_tw_username, $this->twitter_name);
        } else {
            $this->tpl['likefollow2'] = $this->sosmed->get_like_fb($this->facebook_name, $data_user->account_fbid);
        }
        $this->tpl['TContent'] = $this->load->view('landingnew/signupskip2', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

    function ceklikefollow(){
        $from = $this->input->get('from',true);
        $acc = $this->input->get('acc',true);
        $url = $this->input->get('url',true);
        $data_user = $this->db->get_where('account', array('id' => $acc))->row();
        $likefollow = $this->input->get('likefollow', true);
        
        $getlikefollow2 = $this->input->get('likefollow2',true);
        if($getlikefollow2 == 0){        
            if ($from == 'fb') {
                $likefollow2 = $this->sosmed->follow_tw($data_user->account_tw_token, $data_user->account_tw_secret, $data_user->account_tw_username, $this->twitter_name);
            } else {
                $likefollow2 = $this->sosmed->get_like_fb($this->facebook_name, $data_user->account_fbid);
            }
        }else{
            $likefollow2 = 0;
        }
		/*
		if($likefollow2 == 0){
			redirect(site_url('landing/step4?url=' . $url . '&places=' . $this->custom_id . '&from=' . $from . '&acc=' . $data_user->id.'&likefollow='.$likefollow.'&likefollow2='.$likefollow2));
		}*/
        redirect(site_url('landing/step5?url=' . $url . '&places=' . $this->custom_id . '&from=' . $from . '&acc=' . $data_user->id.'&likefollow='.$likefollow.'&likefollow2='.$likefollow2));
    }
    
}

?>
