<?php

class stepthird extends CI_controller {

    protected $content;
    protected $perms;

    function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'wooz.in - ';
        $this->tpl['fb_page'] = 'http://www.facebook.com/pages/Woozin/157614240949967';
        $this->tpl['tw_page'] = 'wooz_in';

        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->model('convert');
        $this->load->helper('url');
        date_default_timezone_set('Asia/Jakarta');

        $this->tpl['fb_url'] = $this->config->item('facebook_url');
        $this->tpl['tw_url'] = $this->config->item('twitter_url');
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');
        $this->load->library('sosmedtw');
        
        $custom_page = $this->input->get('url', true);
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $userid = $this->input->get('acc');
                $urls = array('url' => base_url() . 'landingnew/landing/fb_token?url=' . $customs->places_landing . '&acc='.$userid.'&places=' . $customs->id);
                $this->load->library('facebook_v4', $urls);
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;
                
                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                }
                if ($customs->places_twitter != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $from = $this->input->get('from', TRUE);
        $url = $this->input->get('url', TRUE);
        $acc = $this->input->get('acc', TRUE);

        if ($from && $url && $acc) {
            $this->step3($from, $acc, $url);
        } else {
            redirect('?status=custom_page_not_registered');
        }
    }

    function step3($from, $acc, $url) {
        $data_user = $this->landing_m->gettable('id', $acc, 'account');
        $this->tpl['likefollow'] = $this->input->get('likefollow', true);
        $this->tpl['page'] = "step3";
        if($from == 'fb'){
            $this->tpl['title'] = "Go With Twitter?";            
        }else{
            $this->tpl['title'] = "Go With Facebook?";
        }
        if($from == 'fb'){
            $login_url = $this->facebook_v4->get_login_url();
            $url_logout = site_url('landingnew/logouts?url=hq');
            $logout_url = $this->facebook_v4->get_logout_url($url_logout);
            $token = $this->facebook_v4->token();
            if ($logout_url) {
                $this->tpl['login'] = $logout_url;
            } else {
                $this->tpl['login'] = $login_url;
            }
        }
        $this->tpl['from'] = $from;
        $this->tpl['url'] = $url;

        $this->tpl['id'] = $acc;
        $this->tpl['user_name'] = $data_user->account_displayname;
        $this->tpl['TContent'] = $this->load->view('landingnews/signupskip', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);
    }

}

?>
