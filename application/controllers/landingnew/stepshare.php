<?php

class stepshare extends CI_controller {

    protected $content;
    protected $perms;

    function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'wooz.in - ';
        $this->tpl['fb_page'] = 'http://www.facebook.com/pages/Woozin/157614240949967';
        $this->tpl['tw_page'] = 'wooz_in';

        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->model('convert');
        $this->load->helper('url');
        date_default_timezone_set('Asia/Jakarta');

        $this->tpl['fb_url'] = $this->config->item('facebook_url');
        $this->tpl['tw_url'] = $this->config->item('twitter_url');
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');
        $this->load->library('sosmedtw');

        $custom_page = $this->input->get('url', true);
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $userid = $this->input->get('acc');
                $url = array('url' => base_url() . 'landingnew/landing/fb_token?url=' . $customs->places_landing . '&acc=' . $userid . '&places=' . $customs->id);
                $this->load->library('facebook_v4/facebooks_v4', $url);

                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;

                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                    $this->facebook_name = $customs->places_fbid;
                }
                if ($customs->places_twitter != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twitter_name = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $from = $this->input->get('from', TRUE);
        $url = $this->input->get('url', TRUE);
        $acc = $this->input->get('acc', TRUE);

        if ($from && $url && $acc) {
            $this->step5($from, $acc, $url);
        } else {
            redirect('?status=custom_page_not_registered');
        }
    }

    function step5($from, $acc, $url) {
        $data_user = $this->landing_m->gettable('id', $acc, 'account');
        $this->tpl['likefollow'] = $this->input->get('likefollow', true);
        $this->tpl['likefollow2'] = 0;
        $likefollow2 = $this->input->get('likefollow2', true);
        if ($likefollow2) {
            $this->tpl['likefollow2'] = $likefollow2;
        }
        $loc = $this->input->get('loc', true);
        $this->tpl['loc'] = 0;
        if ($loc) {
            $this->tpl['loc'] = $this->input->get('loc', true);
        }

        $this->tpl['page'] = "step5";
        $this->tpl['title'] = "Share status?";
        $this->tpl['from'] = $from;
        $this->tpl['url'] = $url;
        $this->tpl['places'] = $this->landing_m->gettable('id', $this->custom_id, 'places');

        $this->tpl['id'] = $acc;
        $this->tpl['data'] = $data_user;
        $this->tpl['TContent'] = $this->load->view('landingnews/signupshare', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);
    }

    function share() {
        $from = $this->input->get('from', true);
        $acc = $this->input->get('acc', true);
        $url = $this->input->get('url', true);
        $user = $this->db->get_where('account', array('id' => $acc))->row();
        $likefollow = $this->input->get('likefollow', true);
        $likefollow2 = $this->input->get('likefollow2', true);
        if ($this->custom_id == 436) {
            $this->point_guinness($acc);
        }
        $places = $this->db->get_where('places', array('places_landing' => $url))->row();
        if ($user->account_fbid && $user->account_token) {
            $landing['landing_fb_like'] = $this->facebooks_v4->get_like_fb($user->account_token, $user->account_fbid, $places->places_fb_page_id);
            $landing['landing_fb_friends'] = $this->facebooks_v4->get_friend_fb($user->account_token, $user->account_fbid);
            
            $text = $places->places_cstatus_fb;
            $status = array(
                    'link' => 'http://wooz.local/uploads/apps/echelon2013/logo_woozin.jpg',
                    'name' => $places->places_name,
                    'caption' => $text
            );
            if ($places->places_type == 4) {
                $image = FCPATH . $places->places_avatar;
                $landing['landing_fb_status'] = $this->facebooks_v4->upload_photo_fb_custom($places, $image, $user->account_fb_name, $user->account_fbid, $user->account_token, $text);
            } else {
                $landing['landing_fb_status'] = $this->facebooks_v4->update_status($status, $user->account_fbid, $user->account_token);
            }
        }
        if ($user->account_tw_token && $user->account_tw_secret) {
            $landing['landing_tw_follow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_twitter);
            if ($this->custom_id == 446) {
                $texttw = $places->places_cstatus_tw;
            } else {
                $texttw = $places->places_cstatus_tw;
            }
            if ($places->places_type == 4) {
                $image = FCPATH . $places->places_avatar;
                $landing['landing_tw_status'] = $this->sosmedtw->upload_photo_tw($texttw, $image, $user->account_tw_token, $user->account_tw_secret);
            } else {
                $landing['landing_tw_status'] = $this->sosmedtw->update_tw($texttw, $user->account_tw_token, $user->account_tw_secret);
            }
            $landing['landing_tw_friends'] = $this->sosmedtw->friend_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username);
        }
        if ($from == 'fb') {
            $landing['landing_fb_like_event'] = $likefollow;
            $landing['landing_tw_follow_event'] = $likefollow2;
        } else {
            $landing['landing_fb_like_event'] = $likefollow2;
            $landing['landing_tw_follow_event'] = $likefollow;
        }
        if (isset($user->account_group) && $user->account_group) {
            $landing['landing_level'] = $user->account_group;
        } else {
            $landing['landing_level'] = 1;
        }
        if ($this->custom_id == 453) {
            $landing['landing_rfid'] = 0;
        } else {
            $landing['landing_rfid'] = $user->account_rfid;
        }
        $landing['landing_from_regis'] = $from;
        $landing['account_id'] = $acc;
        $landing['landing_register_form'] = $this->custom_id;
        $this->db->insert('landing', $landing);
        redirect(site_url('landingnew/thankyou?url=' . $this->custom_url . '&places=' . $this->custom_id . '&from=' . $from . '&acc=' . $acc));
    }

    function noshare() {
        $from = $this->input->get('from', true);
        $acc = $this->input->get('acc', true);
        $url = $this->input->get('url', true);
        $user = $this->db->get_where('account', array('id' => $acc))->row();
        $likefollow = $this->input->get('likefollow', true);
        $likefollow2 = $this->input->get('likefollow2', true);

        if ($this->custom_id == 436) {
            $this->point_guinness($acc);
        }

        $places = $this->db->get_where('places', array('places_landing' => $url))->row();
        if ($user->account_fbid && $user->account_token) {
            $landing['landing_fb_like'] = $this->facebooks_v4->get_like_fb($user->account_token, $user->account_fbid, $places->places_fb_page_id);
            $landing['landing_fb_friends'] = $this->facebooks_v4->get_friend_fb($user->account_token, $user->account_fbid);
        }
        if ($user->account_tw_token && $user->account_tw_secret) {
            $landing['landing_tw_follow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_twitter);
            $landing['landing_tw_friends'] = $this->sosmedtw->friend_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username);
        }
        if ($from == 'fb') {
            $landing['landing_fb_like_event'] = $likefollow;
            $landing['landing_tw_follow_event'] = $likefollow2;
        } else {
            $landing['landing_fb_like_event'] = $likefollow2;
            $landing['landing_tw_follow_event'] = $likefollow;
        }
        if (isset($user->account_group) && $user->account_group) {
            $landing['landing_level'] = $user->account_group;
        } else {
            $landing['landing_level'] = 1;
        }
        if ($this->custom_id == 453) {
            $landing['landing_rfid'] = 0;
        } else {
            $landing['landing_rfid'] = $user->account_rfid;
        }
        $landing['landing_from_regis'] = $from;
        $landing['account_id'] = $acc;
        $landing['landing_register_form'] = $this->custom_id;
        $this->db->insert('landing', $landing);

        redirect(site_url('landingnew/thankyou?url=' . $this->custom_url . '&places=' . $this->custom_id . '&from=' . $from . '&acc=' . $acc));
    }

    function point_guinness($id) {
        $date = date('Y-m-d');
        $sqlcekpoint = "select * from wooz_guinness_point where account_id = " . $id . " "
                . "and date_add like '%" . $date . "%' and transaksi_id = 2 order by id desc";
        $querycekpoint = $this->db->query($sqlcekpoint);
        $hasilcekpoint = $querycekpoint->row();
        if (!$hasilcekpoint) {
            $db['account_id'] = $id;
            $db['transaksi_id'] = 2;
            $db['kode_transaksi'] = 2;
            $db['lokasi'] = 1;
            $db['redeem_point'] = 0;
            $db['date_add'] = date('Y-m-d H:i:s');
            $sqlcekpoint = "select * from wooz_guinness_point where account_id = " . $id . " "
                    . "order by id desc";
            $querycekpoint = $this->db->query($sqlcekpoint);
            $hasilcekpoint = $querycekpoint->row();

            if ($hasilcekpoint) {
                $db['point_awal'] = $hasilcekpoint->total_point;
                $db['total_point'] = $hasilcekpoint->total_point + 10;
            } else {
                $db['point_awal'] = 0;
                $db['total_point'] = 10;
            }
            $this->db->insert('guinness_point', $db);
            return true;
        } else {
            return false;
        }
    }

}
