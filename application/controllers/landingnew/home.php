<?php

class home extends CI_controller {

    protected $content;
    protected $perms;

    function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'wooz.in - ';
        $this->tpl['fb_page'] = 'http://www.facebook.com/pages/Woozin/157614240949967';
        $this->tpl['tw_page'] = 'wooz_in';

        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->model('convert');

        $this->load->helper('url');

        $this->fb_url = $this->config->item('facebook_url');
        $this->tpl['fb_url'] = $this->fb_url;
        $this->tw_url = $this->config->item('twitter_url');
        $this->tpl['tw_url'] = $this->tw_url;
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true
        ));
    }

    function index() {
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Home";
        $this->tpl['data'] = $this->landing_m->urllanding();
        #print_r($this->tpl['data']);die;
        $this->tpl['TContent'] = $this->load->view('landing/awal', $this->tpl, true);
        $this->load->view('landing/home', $this->tpl);
    }

}

?>
