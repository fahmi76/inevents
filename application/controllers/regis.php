<?php

class regis extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model(array('convert', 'landing_m'));
		$this->load->model('dashboard/visitor_m');
		$this->load->model('dashboard/user_m');

		$this->load->library('fungsi');
		$this->load->library('session');
		$this->load->library('curl');
		#date_default_timezone_set('Asia/Jakarta');
		date_default_timezone_set('America/Port_of_Spain');

		$this->load->helper('url');
		#$this->load->library('sosmed');

		$this->tpl['assets_url'] = $this->config->item('assets_url');
		$this->tpl['uploads_url'] = $this->config->item('uploads_url');

		$custom_page = $this->uri->segment(3, 0);
		if ($custom_page != '') {
			$customs = $this->landing_m->getplaceslanding($custom_page);
			if ($customs) {
				$this->custom_page = $custom_page;
				$this->tpl['landings'] = $custom_page;
				$this->custom_id = $customs->id;
				$this->custom_url = $customs->places_landing;
				$this->tpl['spot_id'] = $customs->id;
				$this->custom_model = $customs->places_model;
				$this->tpl['custom_model'] = $this->custom_model;
				$this->tpl['customs'] = $customs->places_landing;
				$this->tpl['background'] = $customs->places_backgroud;
				$this->tpl['logo'] = $customs->places_logo;
				$this->tpl['main_title'] = $customs->places_name;
				$this->tpl['type_registration'] = $customs->places_type_registration;
				$this->tpl['css'] = $customs->places_css;
			} else {
				redirect('?status=custom_page_end_from_showing');
			}
		} else {
			redirect('?status=custom_page_not_found');
		}
	}

	function index() {
		$this->home();
	}

	function preregis() {
		if ($this->custom_id == 25) {
			$this->homeend();

		} else {
			header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
			header("Cache-Control: no-cache, must-revalidate");
			header("Pragma: no-cache");
			$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
			$this->output->set_header("Pragma: no-cache");
			$this->tpl['main_title'] = 'Registration';
			$this->tpl['core'] = 'Registration';
			$this->tpl['title'] = "Inevents";
			$form = $this->visitor_m->list_form_regis($this->custom_id);

			$this->load->helper(array('form'));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'Nama', 'trim');
			foreach ($form as $row) {
				switch ($row->field_type) {
				case 'radio':
					if ($row->field_required == 1) {
						$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
					} else {
						$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
					}
					break;
				default:
					if ($row->field_required == 1) {
						if ($row->field_nicename != 'rfid') {
							$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
						}
					} else {
						$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
					}
					break;
				}
			}
			if ($this->form_validation->run() === TRUE) {
				$in_data['account_displayname'] = $this->input->post('name', true);
				$in_data['account_username'] = $this->user_m->nicename($this->input->post('name', true));
				$in_data['account_rfid'] = $this->input->post('rfid', true);
				$in_data['account_status'] = 3;
				$this->db->insert('account', $in_data);
				$acc_id = $this->db->insert_id();
				foreach ($form as $row) {
					if ($row->field_type == 'file') {
						$header_post = '';
						if ($this->do_upload($row->field_nicename)) {
							$header_post = str_replace(' ', '_', $_FILES[$row->field_nicename]['name']);
							$in_data_rfid['form_regis_id'] = $row->id;
							$in_data_rfid['account_id'] = $acc_id;
							$in_data_rfid['places_id'] = $this->custom_id;
							$in_data_rfid['content'] = $header_post;
							$in_data_rfid['date_add'] = date('Y-m-d H:i:s');
							$rowcontent = $this->user_m->cek_content_data($acc_id, $id, $row->id);
							if ($rowcontent) {
								$this->db->where('id', $rowcontent->id);
								$this->db->update('form_regis_detail', $in_data_rfid);
							} else {
								$this->db->insert('form_regis_detail', $in_data_rfid);
							}
						}
					} else {
						$data_content = $this->input->post($row->field_nicename, true);
						if ($data_content) {
							$in_data_rfid['form_regis_id'] = $row->id;
							$in_data_rfid['account_id'] = $acc_id;
							$in_data_rfid['places_id'] = $this->custom_id;
							$in_data_rfid['content'] = $data_content;
							$in_data_rfid['date_add'] = date('Y-m-d H:i:s');

							$rowcontent = $this->user_m->cek_content_data($acc_id, $this->custom_id, $row->id);
							if ($rowcontent) {
								$this->db->where('id', $rowcontent->id);
								$this->db->update('form_regis_detail', $in_data_rfid);
							} else {
								$this->db->insert('form_regis_detail', $in_data_rfid);
							}
						}
					}
				}
				$in_data_landing['account_id'] = $acc_id;
				$in_data_landing['landing_register_form'] = $this->custom_id;
				$in_data_landing['landing_rfid'] = $in_data['account_rfid'];
				$this->db->insert('landing', $in_data_landing);
				redirect('regis/done/' . $this->custom_url);
				// redirect('gateaward/regischeck/First-Citizen-Sports-Awards/1/35/0/' . $acc_id);
			} else {
				if ($this->custom_id == 34) {
					$file = base_url() . 'assets/country.json';
					$data = $this->curl->simple_get($file);
					$result = json_decode($data);
					$this->tpl['country'] = $result;
				}
				$this->tpl['form'] = $form;
				$this->tpl['visitor'] = $this->visitor_m->list_visitor_event($this->custom_id);
				$this->tpl['TContent'] = $this->load->view('regis/form', $this->tpl, true);
			}
			$this->load->view('regis/home', $this->tpl);

		}
	}

	function home() {
		if ($this->custom_id == 25) {
			$this->homeend();

		} else {
			header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
			header("Cache-Control: no-cache, must-revalidate");
			header("Pragma: no-cache");
			$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
			$this->output->set_header("Pragma: no-cache");
			$this->tpl['main_title'] = 'Registration';
			$this->tpl['core'] = 'Registration';
			$this->tpl['title'] = "Inevents";
			$form = $this->visitor_m->list_form_regis($this->custom_id);

			$this->load->helper(array('form'));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'Nama', 'trim');
			foreach ($form as $row) {
				switch ($row->field_type) {
				case 'radio':
					if ($row->field_required == 1) {
						$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
					} else {
						$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
					}
					break;
				default:
					if ($row->field_required == 1) {
						$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
					} else {
						$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
					}
					break;
				}
			}
			if ($this->form_validation->run() === TRUE) {
				$in_data['account_displayname'] = $this->input->post('name', true);
				$in_data['account_username'] = $this->user_m->nicename($this->input->post('name', true));
				$in_data['account_rfid'] = $this->input->post('rfid', true);
				$in_data['account_status'] = 3;

				$this->db->insert('account', $in_data);
				$acc_id = $this->db->insert_id();
				foreach ($form as $row) {
					if ($row->field_type == 'file') {
						$header_post = '';
						if ($this->do_upload($row->field_nicename)) {
							$header_post = str_replace(' ', '_', $_FILES[$row->field_nicename]['name']);
							$in_data_rfid['form_regis_id'] = $row->id;
							$in_data_rfid['account_id'] = $acc_id;
							$in_data_rfid['places_id'] = $this->custom_id;
							$in_data_rfid['content'] = $header_post;
							$in_data_rfid['date_add'] = date('Y-m-d H:i:s');
							$rowcontent = $this->user_m->cek_content_data($acc_id, $id, $row->id);
							if ($rowcontent) {
								$this->db->where('id', $rowcontent->id);
								$this->db->update('form_regis_detail', $in_data_rfid);
							} else {
								$this->db->insert('form_regis_detail', $in_data_rfid);
							}
						}
					} else {
						$data_content = $this->input->post($row->field_nicename, true);
						if ($data_content) {
							$in_data_rfid['form_regis_id'] = $row->id;
							$in_data_rfid['account_id'] = $acc_id;
							$in_data_rfid['places_id'] = $this->custom_id;
							$in_data_rfid['content'] = $data_content;
							$in_data_rfid['date_add'] = date('Y-m-d H:i:s');

							$rowcontent = $this->user_m->cek_content_data($acc_id, $this->custom_id, $row->id);
							if ($rowcontent) {
								$this->db->where('id', $rowcontent->id);
								$this->db->update('form_regis_detail', $in_data_rfid);
							} else {
								$this->db->insert('form_regis_detail', $in_data_rfid);
							}
						}
					}
				}
				if ($this->custom_id != 37) {
					$emailspot = $this->landing_m->emailplaces($this->custom_id);
					// xdebug($emailspot);die;
					$this->mandrill($emailspot, $acc_id, $this->input->post('email-address'));
				}
				$in_data_landing['account_id'] = $acc_id;
				$in_data_landing['landing_register_form'] = $this->custom_id;
				$in_data_landing['landing_rfid'] = $in_data['account_rfid'];
				$this->db->insert('landing', $in_data_landing);
				if ($this->custom_id != 37) {
					redirect('regis/done/' . $this->custom_url);
				} else {
					redirect('gateaward/regischeck/' . $this->custom_page . '/1/38/0/' . $acc_id);
				}
			} else {
				if ($this->custom_id == 34) {
					$file = base_url() . 'assets/country.json';
					$data = $this->curl->simple_get($file);
					$result = json_decode($data);
					$this->tpl['country'] = $result;
					$organizationdistinct = $this->visitor_m->list_distinct_data(80);
					$this->tpl['organizationdata'] = json_encode($organizationdistinct);
					// xdebug(json_encode($arrayorganization));die;
				}
				if ($this->custom_id == 37) {
					$department = $this->visitor_m->list_distinct_data(88);
					$this->tpl['department'] = json_encode($department);
					$branch = $this->visitor_m->list_distinct_data(89);
					$this->tpl['branch'] = json_encode($branch);
					$meal = $this->visitor_m->list_distinct_data(91);
					$this->tpl['meal'] = json_encode($meal);
				}
				$this->tpl['form'] = $form;
				$this->tpl['visitor'] = $this->visitor_m->list_visitor_event($this->custom_id);
				$this->tpl['TContent'] = $this->load->view('regis/form', $this->tpl, true);
			}
			$this->load->view('regis/home', $this->tpl);

		}
	}

	function homestaff() {
		header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->tpl['main_title'] = 'Registration';
		$this->tpl['core'] = 'Registration';
		$this->tpl['title'] = "Inevents";
		$form = $this->visitor_m->list_form_regis($this->custom_id);

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nama', 'trim');
		foreach ($form as $row) {
			switch ($row->field_type) {
			case 'radio':
				if ($row->field_required == 1) {
					$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
				} else {
					$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
				}
				break;
			default:
				if ($row->field_required == 1) {
					$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
				} else {
					$this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
				}
				break;
			}
		}

		if ($this->form_validation->run() === TRUE) {
			$in_data['account_displayname'] = $this->input->post('name', true);
			$in_data['account_username'] = $this->user_m->nicename($this->input->post('name', true));
			$in_data['account_rfid'] = $this->input->post('rfid', true);
			$in_data['account_status'] = 2;
			$this->db->insert('account', $in_data);
			$acc_id = $this->db->insert_id();
			foreach ($form as $row) {
				if ($row->field_type == 'file') {
					$header_post = '';
					if ($this->do_upload($row->field_nicename)) {
						$header_post = str_replace(' ', '_', $_FILES[$row->field_nicename]['name']);
						$in_data_rfid['form_regis_id'] = $row->id;
						$in_data_rfid['account_id'] = $acc_id;
						$in_data_rfid['places_id'] = $this->custom_id;
						$in_data_rfid['content'] = $header_post;
						$in_data_rfid['date_add'] = date('Y-m-d H:i:s');
						$rowcontent = $this->user_m->cek_content_data($acc_id, $id, $row->id);
						if ($rowcontent) {
							$this->db->where('id', $rowcontent->id);
							$this->db->update('form_regis_detail', $in_data_rfid);
						} else {
							$this->db->insert('form_regis_detail', $in_data_rfid);
						}
					}
				} else {
					$data_content = $this->input->post($row->field_nicename, true);
					if ($data_content) {
						$in_data_rfid['form_regis_id'] = $row->id;
						$in_data_rfid['account_id'] = $acc_id;
						$in_data_rfid['places_id'] = $this->custom_id;
						$in_data_rfid['content'] = $data_content;
						$in_data_rfid['date_add'] = date('Y-m-d H:i:s');

						$rowcontent = $this->user_m->cek_content_data($acc_id, $this->custom_id, $row->id);
						if ($rowcontent) {
							$this->db->where('id', $rowcontent->id);
							$this->db->update('form_regis_detail', $in_data_rfid);
						} else {
							$this->db->insert('form_regis_detail', $in_data_rfid);
						}
					}
				}
			}
			if ($this->custom_id == 21) {
				$places = $this->landing_m->emailplaces($this->custom_id);
				if ($places) {
					$sender = 'inevents';
					$subject = 'Registration';
					$content = 'inevents';
					if ($places->places_email) {
						$sender = $places->places_email;
					}
					if ($places->places_subject_email) {
						$subject = $places->places_subject_email;
					}
					if ($places->places_custom_email) {
						$content = $places->places_custom_email;
						$content = str_replace("{name}", $in_data['account_displayname'], $content);
					}
					$in_data_landing['landing_email'] = $this->mandrill($sender, $subject, $content, $acc_id);
				}
			}
			$in_data_landing['account_id'] = $acc_id;
			$in_data_landing['landing_register_form'] = $this->custom_id;
			$in_data_landing['landing_rfid'] = $in_data['account_rfid'];
			$this->db->insert('landing', $in_data_landing);
			redirect('regis/done/' . $this->custom_url);
		} else {
			$this->tpl['form'] = $form;
			$this->tpl['visitor'] = $this->visitor_m->list_visitor_event($this->custom_id);
			$this->tpl['TContent'] = $this->load->view('regis/form', $this->tpl, true);
		}
		$this->load->view('regis/home', $this->tpl);
	}

	function homeend() {
		header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->tpl['main_title'] = 'Registration';
		$this->tpl['core'] = 'Registration';
		$this->tpl['title'] = "Inevents";
		$this->tpl['TContent'] = $this->load->view('regis/regisend', $this->tpl, true);
		$this->load->view('regis/home', $this->tpl);
	}

	function reregis() {
		if ($this->custom_id == 24) {
			$this->homeend();
		} else {
			header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
			header("Cache-Control: no-cache, must-revalidate");
			header("Pragma: no-cache");
			$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
			$this->output->set_header("Pragma: no-cache");
			$this->tpl['main_title'] = 'Re-Registration';
			$this->tpl['core'] = 'Re-Registration';
			$this->tpl['title'] = "Inevents";

			$this->load->helper(array('form'));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('id', 'Nama', 'trim|required');

			if ($this->form_validation->run() === TRUE) {
				$id = $this->input->post('id');
				// $staff = $this->input->post('staff');
				// $sql = "SELECT id FROM `wooz_form_regis_detail` where places_id = '24' and form_regis_id = '45' and account_id = '" . $id . "' and content = '" . $staff . "'";
				// $query = $this->db->query($sql);
				// $hasil = $query->row();
				// if ($hasil) {
				redirect('regis/restep2/' . $this->custom_url . '/' . $id);
				// } else {
				// 	$this->tpl['TContent'] = $this->load->view('regis/notfounduser', $this->tpl, true);
				// }
			} else {
				$this->tpl['visitor'] = $this->visitor_m->list_visitor_event($this->custom_id);
				$this->tpl['TContent'] = $this->load->view('regis/formreregis', $this->tpl, true);
			}
			$this->load->view('regis/home', $this->tpl);
		}
	}

	function reregisstaff() {
		header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->tpl['main_title'] = 'Re-Registration';
		$this->tpl['core'] = 'Re-Registration';
		$this->tpl['title'] = "Inevents";

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'Nama', 'trim|required');
		$this->form_validation->set_rules('staff', 'Staff Number', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
			$id = $this->input->post('id');
			$staff = $this->input->post('staff');
			$sql = "SELECT id FROM `wooz_form_regis_detail` where places_id = '24' and form_regis_id = '45' and account_id = '" . $id . "' and content = '" . $staff . "'";
			$query = $this->db->query($sql);
			$hasil = $query->row();
			if ($hasil) {
				redirect('regis/restep2/' . $this->custom_url . '/' . $id);
			} else {
				$this->tpl['TContent'] = $this->load->view('regis/notfounduser', $this->tpl, true);
			}
		} else {
			$this->tpl['visitor'] = $this->visitor_m->list_visitor_event($this->custom_id);
			$this->tpl['TContent'] = $this->load->view('regis/formreregis', $this->tpl, true);
		}
		$this->load->view('regis/home', $this->tpl);
	}

	function restep2($custom, $id) {
		$this->tpl['main_title'] = 'Re-Registration';
		$this->tpl['core'] = 'Re-Registration';
		$this->tpl['title'] = "Inevents";
		$sql = "SELECT account_displayname FROM wooz_account where id = '" . $id . "'";
		$query = $this->db->query($sql);
		$name = $query->row();
		$this->tpl['hasil'] = $name;
		$this->tpl['email'] = $this->ambil_data_user($id, 83);
		$this->tpl['organization'] = $this->ambil_data_user($id, 80);
		$this->tpl['btn'] = $this->ambil_data_user($id, 81);
		$this->tpl['mtn'] = $this->ambil_data_user($id, 82);
		$this->tpl['product'] = $this->ambil_data_user($id, 84);
		$this->tpl['rfid'] = $this->ambil_data_user($id, 86);
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('rfid', 'RFID', 'trim|required');
		// $this->form_validation->set_rules('attending', 'Attending', 'trim|required');
		// $this->form_validation->set_rules('guest', 'Guest', 'trim|required');
		// $this->form_validation->set_rules('watertaxi', 'watertaxi', 'trim|required');
		// $this->form_validation->set_rules('interested', 'Interested', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
			$this->savedataadd(83, $id, $this->input->post('email'));
			$this->savedataadd(80, $id, $this->input->post('organization'));
			$this->savedataadd(81, $id, $this->input->post('business-tel-no'));
			$this->savedataadd(82, $id, $this->input->post('mobile-tel-no'));
			$this->savedataadd(84, $id, $this->input->post('products-services'));
			$this->savedataadd(86, $id, $this->input->post('rfid'));
			$in_data_rfid['account_rfid'] = $this->input->post('rfid');
			$this->db->where('id', $id);
			$this->db->update('account', $in_data_rfid);

			$in_landing_rfid['landing_rfid'] = $this->input->post('rfid');
			$this->db->where('account_id', $id);
			$this->db->where('landing_register_form', $this->custom_id);
			$this->db->update('landing', $in_landing_rfid);
			// if ($email) {
			$emailspot = $this->landing_m->emailplaces($this->custom_id);
			// xdebug($emailspot);die;
			$this->mandrill($emailspot, $id, $this->input->post('email'));
			// }
			redirect('regis/redone/' . $this->custom_url);
		} else {
			$this->tpl['TContent'] = $this->load->view('regis/restep2', $this->tpl, true);
			$this->load->view('regis/home', $this->tpl);
		}
	}

	function do_upload($file) {
		$name = str_replace(' ', '_', $_FILES[$file]['name']);
		$config = array(
			'upload_path' => FCPATH . "/uploads/user/",
			'upload_url' => base_url() . "uploads/user/",
			'allowed_types' => "gif|jpg|png|jpeg",
			'overwrite' => TRUE,
			'file_name' => $name,
		);

		$this->load->library('upload', $config);
		if ($this->upload->do_upload($file)) {
			return true;
		} else {
			return false;
		}
	}
	function savedataadd($form_regis, $id, $content) {
		$in_data_rfid['form_regis_id'] = $form_regis;
		$in_data_rfid['account_id'] = $id;
		$in_data_rfid['places_id'] = $this->custom_id;
		$in_data_rfid['content'] = $content;
		$in_data_rfid['date_add'] = date('Y-m-d H:i:s');

		$rowcontent = $this->user_m->cek_content_data($id, $this->custom_id, $form_regis);
		if ($rowcontent) {
			$this->db->where('id', $rowcontent->id);
			$this->db->update('form_regis_detail', $in_data_rfid);
		} else {
			$this->db->insert('form_regis_detail', $in_data_rfid);
		}
	}
	function redone() {
		$this->tpl['page'] = "Done";
		$this->tpl['title'] = "Personal Information";

		$this->tpl['page'] = "thankyou";
		$this->tpl['title'] = "Thankyou!";
		$this->tpl['TContent'] = $this->load->view('regis/redone', $this->tpl, true);
		$this->load->view('regis/home', $this->tpl);
	}
	function done() {
		$this->tpl['page'] = "Done";
		$this->tpl['title'] = "Personal Information";

		$accid = $this->uri->segment(3, 0);
		$user = $this->db->get_where('account', array('id' => $accid))->row();
		$this->tpl['logoutfb'] = 0;

		$this->tpl['page'] = "thankyou";
		$this->tpl['title'] = "Thankyou!";
		$this->tpl['TContent'] = $this->load->view('regis/signupdone', $this->tpl, true);
		$this->load->view('regis/home', $this->tpl);
	}

	function logout() {
		$this->session->unset_userdata('session_id');
		$this->session->unset_userdata('ip_address');
		$this->session->unset_userdata('user_agent');
		$this->session->unset_userdata('last_activity');
		#$this->_killFacebookCookies();
		$this->session->sess_destroy();
		session_destroy();
		unset($_SESSION);
		$past = time() - 3600;
		foreach ($_COOKIE as $key => $value) {
			setcookie($key, $value, $past, '/');
		}
		if ($this->custom_id == 21) {
			redirect('http://www.fashionfocus.org');
		}
		redirect('regis/home/' . $this->custom_url);
	}

	function mandrill($emailspot, $acc_id, $email_receive) {
		$hasil_email = 0;
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;

		try {
			$this->mandrill->init($this->config->item('mandrill_api_key'));
			$mandrill_ready = TRUE;
		} catch (Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
		}
		$attch = array();
		if ($mandrill_ready) {
			$attachemail = json_decode($emailspot->places_email_attach);
			if (count($attachemail) > 0) {
				$xy = 1;
				foreach ($attachemail as $rowemail) {
					$image = FCPATH . '/uploads/apps/email/' . $rowemail;
					$attch[] = $this->mandrill->getAttachmentStruct($image);
				}
			}
			// $email_receive = 'hayria76@yahoo.com'; //test email
			//Send us some email!
			$email = array(
				'html' => $emailspot->places_custom_email, //Consider using a view file
				'subject' => $emailspot->places_subject_email,
				'from_email' => 'donotreply@wooz.in',
				'from_name' => $emailspot->places_email,
				//'to' => array(array('email' => $this->ambil_data_user($acc_id,30)))
				'to' => array(array('email' => $email_receive)),
				"attachments" => $attch,
			);
			$result = $this->mandrill->messages_send($email);
			$hasil_email = $result[0]['_id'];
		}
		return $hasil_email;
	}

	function ambil_data_user($acc_id, $form_id) {
		$sql = "select a.account_id,a.content "
			. "from wooz_form_regis_detail a where a.form_regis_id = '" . $form_id . "' and account_id = '" . $acc_id . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$hasil = $data->content;
		} else {
			$hasil = '';
		}
		return $hasil;
	}

	function rfid_used($str, $account_id) {
		if (!isset($str)) {
			$str = $this->input->post('rfid');
		}
		$new = $this->db->get_where('account_sosmed', array('account_rfid' => $str))->row();
		if (count($new) >= 1) {
			$news = $this->db->get_where('account_sosmed', array('account_rfid' => $str, 'id' => $account_id))->row();
			if ($news) {
				return true;
			} else {
				$this->form_validation->set_message('rfid_used', 'RFID already registered');
				return false;
			}
		} else {
			return true;
		}
	}

	function checknameawalnew($nicename) {

		// process posted form data (the requested items like province)
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response
		if (strlen($keyword) >= 3) {
			$sqluser = "select a.id,a.account_displayname as hasil FROM wooz_account a inner join wooz_landing b on b.account_id = a.id
                        where a.account_displayname like '%" . $keyword . "%' and landing_register_form = '" . $this->custom_id . "'";
			$query = $this->db->query($sqluser);
			$dataquery = $query->result();

			if (!empty($dataquery)) {
				$data['response'] = 'true'; //Set response
				$data['message'] = array(); //Create array
				foreach ($dataquery as $row) {
					$data['message'][] = array(
						'value' => $row->hasil,
						'id' => $row->id,
						'organization' => $this->ambil_data_user($row->id, 80),
						'email' => $this->ambil_data_user($row->id, 83),
					);
				}
			}
			if ('IS_AJAX') {
				echo json_encode($data); //echo json string if ajax request
			} else {
				echo json_encode($data); //echo json string if ajax request
			}
		} else {
			echo json_encode($data);
		}
	}

}
