<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class stats extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->database();
        $this->load->library('session');
		#$this->load->library('graph');
        $this->load->library('curl');
        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');
        date_default_timezone_set('Asia/Jakarta');
        $this->tpl['fb_perms'] = $fb_perms;
        include(APPPATH.'third_party/tmhOAuth.php');
        include(APPPATH.'third_party/tmhUtilities.php');
        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
            'fileUpload' => true
        ));
	}

	function getlikeinterest($id){
        set_time_limit(0);
		$this->load->library('like_interest');
		
		$sql = "SELECT distinct(`account_id`),b.account_token,b.id FROM `wooz_photos` a left join wooz_account b on b.id = a.account_id WHERE account_token != '' and `places_id` = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->result();
		foreach($hasil as $row){
            $this->like_interest->set_token($row->account_token);
            $this->like_interest->insert_like($row->id);
            $this->like_interest->insert_interest($row->id);
			echo $row->account_id.' - sukses <br/>';
		}
	}
	
    function getfriend($id){
        set_time_limit(0);
        $this->load->library('sosmed');
        #$sql = "SELECT id,account_fbid,account_token,account_tw_token,account_tw_secret,account_tw_username FROM wooz_account WHERE account_fbid is not null or account_twid is not null";
        $sql = "SELECT distinct(`account_id`),b.account_fbid,b.account_token,b.id FROM `wooz_photos` a left join wooz_account b on b.id = a.account_id WHERE account_token != '' and `places_id` = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->result();
        
        foreach($hasil as $user){
            $landing['fb_friend'] = 0;
            $landing['tw_friend'] = 0;
            if($user->account_fbid && $user->account_token){
                //$landing['fb_friend'] = $this->sosmed->get_friend_fb($user->account_token);
                $datas = $this->sosmed->get_data_fb($user->account_fbid,$user->account_token);
                if($datas){
                    $params1['account_gender'] = $datas['gender'];
                    $params1['account_email'] = $datas['email'];
                    $this->db->where('id', $user->id);
                    $this->db->update('account', $params1);
                }
            }
            /*
            if($user->account_tw_token && $user->account_tw_secret){
                $landing['tw_friend'] = $this->sosmed->friend_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username);
            }
            $landing['account_id'] = $user->id;
            $this->db->insert('friend', $landing);
            */
            echo $user->id . ' - sukses <br/>';
            
        }
    }
	
	function likeinterestid($id){
		$this->load->library('like_interest');
		
		$sql = "SELECT account_token,id FROM wooz_account WHERE id = ".$id;
        $query = $this->db->query($sql);
        $hasil = $query->row();
        $this->like_interest->set_token($hasil->account_token);
        $this->like_interest->insert_like($hasil->id);
        $this->like_interest->insert_interest($hasil->id);
		echo $row->id.' - sukses <br/>';	
	}
	

    function likeinteresttrap() {
		$this->benchmark->mark('dog');
        $number = $this->uri->segment(3,0);
        $total =  17600;
		#$total =  176;
        
        if($number==0) $number = 2000;
	
        $row = $this->db->get_where('account',array('id'=> $number ))->row();
        if($row && $row->account_token !='') {
            #echo $row->account_displayname.'<br />';

            $this->load->library('like_interest');
            $this->like_interest->set_token($row->account_token);
            $this->like_interest->insert_like($row->id);
            $this->like_interest->insert_interest($row->id);
        }
        
        if( $number < $total ) {
            $number = $number + 1;
			$this->benchmark->mark('cat') ;
			$data['time'] = $this->benchmark->elapsed_time('dog', 'cat');
			$data['date'] = date("Y-m-d H:i:s");
			if(isset($row->id)&&$row->id!=''){
				$data['account_id'] = $row->id;
				$this->db->insert('traps_log', $data);
			}
			redirect('polling/likeinteresttrap/'.$number,'refresh');
            #echo 'Loading... Please Wait...<br />';
            #echo '<meta http-equiv="refresh" content="5;URL='.site_url().'api/likeinterest/'.$number.'">';
        } else {
            echo 'done<br />';
        }
    }

    function likeinterest() {
		$this->benchmark->mark('dog');
        $number = $this->uri->segment(3,0);
        $total =  52000;
        
        if($number==0) $number = 1;
        
        $row = $this->db->get_where('account',array('id'=> $number ))->row();
        if($row && $row->account_token !='') {
            #echo $row->account_displayname.'<br />';


            $this->load->library('like_interest_test');
            $this->like_interest_test->set_token($row->account_token);
            $this->like_interest_test->insert_like($row->id);
            $this->like_interest_test->insert_interest($row->id);
            #$this->insert_like($row->id,$row->account_token);
            #$this->insert_interest($row->id,$row->account_token);
        }
        
        if( $number < $total ) {
            $number = $number + 1;
			// something happens
			$this->benchmark->mark('cat') ;
			$data['time'] = $this->benchmark->elapsed_time('dog', 'cat');
			$data['date'] = date("Y-m-d H:i:s");
			if(isset($row->id)&&$row->id!=''){
				$data['account_id'] = $row->id;
				$this->db->insert('traps_log_test', $data);
			}
            #echo 'Loading... Please Wait...<br />';
			redirect('polling/likeinterest/'.$number,'refresh');
            #echo '<meta http-equiv="refresh" content="2;URL='.site_url().'polling/likeinterest/'.$number.'">';
        } else {
            echo 'done<br />';
        }
    }

    /**
     * memasukan ke dalam table traps
     *
     * @param int $id_user
     */
    function insert_like($id_user,$tokens) {
        try{
            $url = "https://graph.facebook.com/me/likes?access_token=".$tokens;
            $res = $this->curl->simple_get($url);

            #$url = sprintf("https://graph.facebook.com/me/likes?access_token=%s", $token);
            #$res = $this->curl->simple_get($url);
            $likes = json_decode($res);
            $like_id = array();
            $like_ids = array();
            if (isset($likes->data) && count($likes->data) != 0 ) {
                foreach($likes->data as $row){
                    $param['name'] = strip_tags(trim($row->name));
                    $param['category'] = strip_tags(trim($row->category));
                    $rows = $this->db->get_where('like_test', array('name' => strip_tags(trim($row->name)), 'category' => strip_tags(trim($row->category))))->row();

                    if (!$rows) {
                        $this->db->insert('like_test', $param);
                        $id = $this->db->insert_id();
                        $like_ids[] = $id;
                    }else{
                        $like_ids[] = $rows->id;
                    }
                    /*
                    $user_like = $this->db->get_where('user_likes', array('account_id' => $id_user, 'likes_id' => $like))->row();
                    if (count($user_like) === 0) {
                    $params['account_id'] = $id_user;
                    $params['likes_id'] = $like;
                    $params['status'] = 1;
                    $this->db->insert('user_likes', $params);
                    $id = $this->db->insert_id(); 
                    }else{  
                    $params1['user_like_lastupdate'] = date("Y-m-d H:i:s");
                    $params1['status'] = 1;
                    $this->db->where('id', $user_like->id);
                    $this->db->update('user_likes', $params1);
                    }
                    if(isset($user_like->likes_id) && $user_like->likes_id!="")
                    $like_id[] = $user_like->likes_id;
                    */
                }
                $this->db->select('likes_id');
                $this->db->where('account_id',$id_user);
                $this->db->from('user_likes_test');
                $user_like = $this->db->get()->result();
                $query = array();
                foreach($user_like as $row)
                {
                    $query[]=$row->likes_id;
                }
                $result = array_diff($like_ids, $query);
                foreach($result as $row){
                    $params['account_id'] = $id_user;
                    $params['likes_id'] = $row;
                    $params['status'] = 1;
                    $this->db->insert('user_likes_test', $params);
                    $id = $this->db->insert_id();          
                }
                $like_id = array_merge($like_ids,$query);
				}
				if(isset($like_id) && !empty($like_id) && $like_id!="") {
					$this->db->select('id');
					$this->db->where('account_id',$id_user);
					$this->db->where_not_in('likes_id',$like_id);
					$this->db->from('user_likes_test');
					$next = $this->db->get()->result();
					foreach($next as $row){
						$params2['user_like_lastupdate'] = date("Y-m-d H:i:s");
						$params2['status'] = 0;
						$this->db->where('id', $row->id);
						$this->db->update('user_likes_test', $params2); 
					}
				}
				if(isset($like_id))
				(unset) $like_id;
				if(isset($query))
				(unset) $query;
				if(isset($like_ids))
				(unset) $like_ids;
				if(isset($param))
				(unset) $param;
            }catch (FacebookApiException $e) {
            error_log($e);
            }           
    }

    function insert_interest($id_user,$tokens){
		try{
            $url = "https://graph.facebook.com/me/interests?access_token=".$tokens;
            $res = $this->curl->simple_get($url);
			#$url = sprintf("https://graph.facebook.com/me/interests?access_token=%s", $this->token);
			#$res = $this->curl->simple_get($url);
            $interests = json_decode($res);
            $interest_id = array();
            $interest_ids = array();
            if (isset($interests->data) && count($interests->data) != 0 ) {
                foreach($interests->data as $row){
                    $param['name'] = strip_tags(trim($row->name));
                    $param['category'] = strip_tags(trim($row->category));
                    $rows = $this->db->get_where('interest_test', array('name' => strip_tags(trim($row->name)), 'category' => strip_tags(trim($row->category))))->row();

                    if (!$rows) {
                        $this->db->insert('interest_test', $param);
                        $id = $this->db->insert_id();
                        $interest_ids[] = $id;
                    }else{
                        $interest_ids[] = $rows->id;
                    }
                    /*
                    $user_interest = $this->db->get_where('user_interests', array('account_id' => $id_user, 'interests_id' => $interest))->row();
                    if (count($user_interest) === 0) {
                    $params['account_id'] = $id_user;
                    $params['interests_id'] = $interest;
                    $params['status'] = 1;
                    $this->db->insert('user_interests', $params);
                    $id = $this->db->insert_id(); 
                    }else{  
                    $params1['user_interest_lastupdate'] = date("Y-m-d H:i:s");
                    $params1['status'] = 1;
                    $this->db->where('id', $user_interest->id);
                    $this->db->update('user_interests', $params1);
                    }
                    if(isset($user_interest->interests_id) && $user_interest->interests_id!="")
                    $interest_id[] = $user_interest->interests_id;
                    */
                }
                $this->db->select('interest_id');
                $this->db->where('account_id',$id_user);
                $this->db->from('user_interest_test');
                $user_interest = $this->db->get()->result();
                $query = array();
                foreach($user_interest as $row)
                {
                    $query[]=$row->interest_id;
                }
                $result = array_diff($interest_ids, $query);
                foreach($result as $row){
                    $params['account_id'] = $id_user;
                    $params['interest_id'] = $row;
                    $params['status'] = 1;
                    $this->db->insert('user_interest_test', $params);
                    $id = $this->db->insert_id();          
                }
                $interest_id = array_merge($interest_ids,$query);
				}
				if(isset($interest_id) && !empty($interest_id) && $interest_id!="") {
					$this->db->select('id');
					$this->db->where('account_id',$id_user);
					$this->db->where_not_in('interest_id',$interest_id);
					$this->db->from('user_interest_test');
					$next = $this->db->get()->result();
					foreach($next as $row){
						$params2['user_interest_lastupdate'] = date("Y-m-d H:i:s");
						$params2['status'] = 0;
						$this->db->where('id', $row->id);
						$this->db->update('user_interest_test', $params2); 
					}
				}
				if(isset($interest_id))
				(unset) $interest_id;
				if(isset($query))
				(unset) $query;
				if(isset($interest_ids))
				(unset) $interest_ids;
				if(isset($param))
				(unset) $param;
            }catch (FacebookApiException $e) {
            error_log($e);
            }        
    }

	function getBuku() {
        $this->tpl['title'] = 'menampilkan isi buku';
                //$data['detail'] = $this->buku_model->getBuku();//call the model and save the result in $detail
        $this->db->select('code_voucher');
        $this->db->order_by('id','ASC');
        $this->db->from('digitalk_store');
        $views = $this->db->get()->result();
        $this->tpl['datalist'] = $views;
        $this->tpl['TContent'] = $this->load->view('digitalk/buku_view',$this->tpl,true);
        $this->load->view('digitalk/home',$this->tpl);
    }
//function to export to excel
    function toExcelAll() {
        $this->db->select('code_voucher');
        $this->db->order_by('id','ASC');
        $this->db->from('digitalk_store');
        $views = $this->db->get()->result();
        $this->tpl['datalist'] = $views;
        $this->tpl['TContent'] = $this->load->view('digitalk/excel_view',$this->tpl,true);
        $this->load->view('digitalk/home',$this->tpl);
    }

    function token() {
        if (isset($_REQUEST['card'])&&$_REQUEST['card']!='') {
            $cserial = $_REQUEST['card'];
			if ($cserial) {
                $user = $this->db->get_where('account', array('account_rfid like' => $cserial))->row();
				if ($user) {
                    $message = array('id' => $user->id, 'name' => $user->account_displayname, 'rfid' => $user->account_rfid, 'fbid' => $user->account_fbid, 'fb_token' => $user->account_token, 'tw_token' => $user->account_tw_token, 'tw_secret' => $user->account_tw_secret);
                    $data = array(
                            'status' => 1,
                            'message' => $message
                    );
                    echo json_encode($data);
                } else {
					$this->db->select('wooz_account.*');
					$this->db->join('account', 'account.id = landing.account_id', 'left');
					$this->db->where('landing.landing_register_form', 214);
					$this->db->where("landing.landing_rfid", $cserial);
					$this->db->from('landing');
					$user = $this->db->get()->row();
					
					if($user){
						$message = array('id' => $user->id, 'name' => $user->account_displayname, 'rfid' => $user->account_rfid, 'fbid' => $user->account_fbid, 'fb_token' => $user->account_token, 'tw_token' => $user->account_tw_token, 'tw_secret' => $user->account_tw_secret);
						$data = array(
								'status' => 1,
								'message' => $message
						);
						echo json_encode($data);
					}else{
						$data = array(
								'status' => 0,
								'message' => 'user not registered'
						);
						echo json_encode($data);
					}
                }
            } else {
                $data = array(
                        'status' => 0,
                        'message' => 'card serial not received'
                );
                echo json_encode($data);
            }
        }
    }

    function tokenlanding() {
        if (isset($_REQUEST['card'])&&$_REQUEST['card']!='') {
            $cserial = $_REQUEST['card'];
            #$bserial = $_REQUEST['places'];
			if ($cserial) {
                $this->db->select('wooz_account.id, wooz_account.account_displayname, wooz_account.account_rfid, wooz_account.account_fbid, wooz_account.account_token, wooz_account.account_tw_token, wooz_account.account_tw_secret');
				#$this->db->join('account', 'account.id = landing.account_id', 'left');
				#$this->db->where('landing.landing_register_form', $bserial);
				#$this->db->where("landing.landing_rfid", $cserial);
				#$this->db->from('landing');
				$this->db->from('account');
				$this->db->where("account_rfid", $cserial);
				$user = $this->db->get()->row();
				if ($user) {
                    $message = array('id' => $user->id, 'name' => $user->account_displayname, 'rfid' => $user->account_rfid, 'fbid' => $user->account_fbid, 'fb_token' => $user->account_token, 'tw_token' => $user->account_tw_token, 'tw_secret' => $user->account_tw_secret);
                    $data = array(
                            'status' => 1,
                            'message' => $message
                    );
                    echo json_encode($data);
                } else {
					$this->db->select('wooz_account.id, wooz_account.account_displayname, wooz_account.account_rfid, wooz_account.account_fbid, wooz_account.account_token, wooz_account.account_tw_token, wooz_account.account_tw_secret');
					$this->db->join('account', 'account.id = landing.account_id', 'left');
					$this->db->where('landing.landing_register_form', 214);
					$this->db->where("landing.landing_rfid", $cserial);
					$this->db->from('landing');
					$user = $this->db->get()->row();
					if ($user) {
						$message = array('id' => $user->id, 'name' => $user->account_displayname, 'rfid' => $user->account_rfid, 'fbid' => $user->account_fbid, 'fb_token' => $user->account_token, 'tw_token' => $user->account_tw_token, 'tw_secret' => $user->account_tw_secret);
						$data = array(
								'status' => 1,
								'message' => $message
						);
						echo json_encode($data);
					} else {
						$data = array(
								'status' => 0,
								'message' => 'user not registered'
						);
						echo json_encode($data);
					
					}
                }
            } else {
                $data = array(
                        'status' => 0,
                        'message' => 'card serial not received'
                );
                echo json_encode($data);
            }
        }
    }

	function twitfollow(){
		$number = $this->uri->segment(3,0);
        include(APPPATH.'third_party/tmhOAuth.php');
        include(APPPATH.'third_party/tmhUtilities.php');
        $total = 0;
		$setting = $this->db->get_where('setting', array('id' => 1))->row();
		$consumer_key = $setting->twitter_key;
        $consumer_key_secret = $setting->twitter_secret;

		$row1 = $this->db->get_where('account',array('id'=> 16095, 'account_tw_token !=' => 0 ))->result();
		#xdebug($row1);die;
		foreach ($row1 as $row)
		{
			$tmhOAuth = new tmhOAuth(array(
				'consumer_key' => $consumer_key,
				'consumer_secret' => $consumer_key_secret,
				'user_token' => $row->account_tw_token,
				'user_secret' => $row->account_tw_secret,
			));
			$code = $tmhOAuth->request('GET', $tmhOAuth->url('1/account/totals'));
			$resp = json_encode($tmhOAuth->response['response']);
			$resp1 = json_decode($resp);
			$twit_id = json_decode($resp1);
			xdebug($twit_id->followers);die;
			if(isset($twit_id->followers) && $twit_id->followers != '')
				$total = $total + $twit_id->followers;
		}
		echo $total;
		#xdebug($twit_id->followers);die;
	}

	function twitimage(){
        $total = 0;
		$row = $this->db->get_where('account',array('id'=> 1761, 'account_tw_token !=' => 0 ))->row();
			$tmhOAuth = new tmhOAuth(array(
				'consumer_key' => $this->config->item('twiiter_consumer_key'),
				'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
				'user_token' => $row->account_tw_token,
				'user_secret' => $row->account_tw_secret,
			));
			$code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/show'), array(
            'id' => '219135288759422976'));
			$resp = json_encode($tmhOAuth->response['response']);
			$resp1 = json_decode($resp);
			$twit_id = json_decode($resp1);
			xdebug($twit_id);die;
	}
    function user() {
		#$sesi = $this->session->userdata('idadmin');
		$sesi = '1';
        $content['admin'] = $sesi;
		$loc = array("39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","158","159","160","161","162","163","164","165","166","167","168","169","170","171","172","173","174");

		$this->db->select('distinct(wooz_account.id)','account.account_displayname, account.account_email, places.places_name');
		$this->db->join('account', 'account.id = landing.account_id', 'left');
		$this->db->join('places', 'places.id = landing.landing_register_form', 'left');
		$this->db->where_in("landing_register_form", $loc);
		$this->db->from('landing');
		$this->db->order_by("places.places_name", "asc"); 
		#$this->db->where_not_in('landing.account_id', array('1','4','5','6','8','9','141','58'));
        $views = $this->db->get()->result();

		$content['loc'] = $loc;
		$content['datalist'] = $views;
        $content['content'] = $this->load->view('report/user', $content, true);
        $this->load->view('report/body', $content);
    }
	
	function pagephotoupload(){
		$fanpage_id = '107968509244366';
        $fanpage_token = 'CAACGFg9ewNIBALOBzfpZAmxbyw1Ezjf8KrVks5zAmqJ8RoTka8hlTS6O8FOY3g5b32Bm9BZCvIjy6nwdjRJ4TVfH64rXe4tQam6xAA0EtbGuDkOzkC3QuVhFQAKFrNivrPcrKT8dwN9MqOmZBA7q66NDIIgQtWWR5HzCf5medtU47EltbxP';
		
		$sql = "SELECT * FROM wooz_photos WHERE places_id in ('419','414','415','416') 
			and time_processed1 = 0 and `time_upload` >= '2014-01-26 06:09:15' order by id asc";
        $hasil = $this->db->query($sql);
        $data = $hasil->result();
		foreach($data as $row){
			$copytext = $this->db->get_where('places', array('id' => $row->places_id))->row();
			$this->facebook->setFileUploadSupport(true);
            $attachement1 = array(							
                    'access_token'=> $fanpage_token,
                    'caption' => 'uploaded foto',
                    'name' => $copytext->places_fb_caption,
					'no_story' => 1,
                    'source' => '@' . FCPATH . 'uploads/photobooth/' . $row->photo_upload
            );
            #print_r($attachement);
            $params1 = $this->facebook->api('659203674120844/photos','POST',$attachement1);	$params1['status'] = 1;
            $params1s['time_processed1'] = $params1['id'];
			$this->db->where('id', $row->id);
            $this->db->update('photos', $params1s);
			echo $row->id;
			echo '</br>';
		}
	}
	
	function fotoupload(){
		$this->db->select('photos.id,account.account_displayname, photos.photo_upload');
		$this->db->join('account', 'account.id = photos.account_id', 'left');
		$this->db->where("places_id", 197);
		$this->db->where("status !=", 2);
		$this->db->from('photos');
        $views = $this->db->get()->result();
		#xdebug(count($views));die;
		$copytext = $this->db->get_where('places', array('id' => 197))->row();
		for($i=2;$i<=count($views);$i++){
            try{
                $album_name = $copytext->places_album; //set album 
                $photo = $copytext->places_fb_caption;
                $fanpage_id = '107968509244366';	
                $fanpage_token = 'AAACGFg9ewNIBAHPC2MyiBZCTLahldzUALaJqGts8M6AvCFq1IQnSB6rNXlBxr65XyUoVjsLn8ZA2YvUNrBvsKYs0iwCxWjGVxzjzZAuCpWpE2NiH19e';

                $args = array('access_token' => $fanpage_token);
                $url1 = $this->facebook->api($fanpage_id .'/albums', 'get', $args);
                $info1 = $url1['data'];
                $up_to1 = false;

                foreach($info1 as $alb1) {
                    if($alb1['name'] == $album_name) {
                    $up_to1 = $alb1['id'];
                    }
                }
                if($up_to1) {
                    $album_id1 = $up_to1;
                }else {
                    try{
                        $args = array('name' => $album_name);
                        $args['access_token'] = $fanpage_token;
                        $url1 = $this->facebook->api($fanpage_id .'/albums', 'post', $args);
                        $album_id1 = $url1['id'];
                    }catch(FacebookApiException $e){
                        error_log($e);
                    }
                }
                // upload photo
                $this->facebook->setFileUploadSupport(true);
                $attachement1 = array(							
                    'access_token'=> $fanpage_token,
                    'caption' => 'uploaded foto',
                    'name' => $photo,
					'no_story' => 1,
                    'source' => '@' . FCPATH . 'uploads/photobooth/' . $views[$i]->photo_upload
                );
                #print_r($attachement);
                $upload1 = $this->facebook->api($album_id1.'/photos','POST',$attachement1);	
				echo $views[$i]->id .' -- '. $views[$i]->account_displayname;
				echo '</br>';
            } catch (FacebookApiException $e){
                error_log($e);
            }  
		}
	}

	function cek_user(){
		$fbid = $this->input->post('fb');
		$twid = $this->input->post('tw');
		$email = $this->input->post('email');
		if($email || $fbid || $twid){
			$sqlall = "SELECT id FROM wooz_account WHERE ";
			if($email){
				$sqlall .= "account_email = '".$email."'";
			}
			if($fbid){
				$sqlall .= " or account_fbid = '".$fbid."'";
			}
			if($twid){
				$sqlall .= " or account_twid = '".$twid."'";
			}
			$sqlall .= " and account_joindate = '2015-01-01 00:00:00'";
			$queryall = $this->db->query($sqlall);
			$hasilall = $queryall->row();
			if($hasilall){
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}
	
	function pandara_user(){
		$sql = "SELECT account_email,account_fbid,account_twid FROM wooz_account_pandara where account_id != ''";
        $hasil = $this->db->query($sql);
        $data = $hasil->result();
		$newuser = 0;
		$olduser = 0;
		foreach($data as $row){		
			echo '<pre>';
			print_r($row);
			echo '</pre>';
			#$responce = $this->curl->simple_post('http://wooz.in/testpolling/cek_user', 
			#	array('email1'=>$row->account_email,'fb' => $row->account_fbid,'tw' => $row->account_twid));
			$responce = $this->cek_user($row->account_email,$row->account_fbid,$row->account_twid);
			if($responce == 1){
				$olduser++;
			}else{
				$newuser++;
			}
			echo $responce.'<br>';
		}
		echo 'total user : '.count($data).'<br />';
		echo 'user baru : '.$newuser;
		echo '<br/>user lama : '.$olduser;
	
	}
	
	function newshare(){
		$url = array('url' => base_url() . 'testpolling/newshare');
        $this->load->library('facebook_v4/facebooks_v4', $url);
		
		$status = array(
                    'link' => 'http://wooz.in/woozpot/woozin-headquarters',
                    'name' => 'headquerter',
                    'caption' => 'test update',
					'description' => 'test description'
				);
		
		$fbid = '100001995879634';
		$fbtoken = 'CAACGFg9ewNIBAHHgB3QIe3w1h9p6phlR1SPjOENPeEFOG0BBuABUb90BfXMd72iP8UF5tZBrwMGkMcgjCAIosIMRZCc2oYOYhZCdE3Uk7IZBAcFP9x4V2hctu8gDbf1bOD4u3bIbPzZATuAmXgEfk4h7tV5GDyaZCOIxU2CJ2sWZCJTWYqv6q75XB6mUvM0LufVkqWaprsoBtNKWBboZB0Mh';
        $landing = $this->facebooks_v4->update_status($status, $fbid, $fbtoken);
		xdebug($landing);die;
	}
	
    function getlikesphoto(){
        $sql = "SELECT distinct(a.account_id),a.id,a.photo_upload,a.account_id,b.account_email,b.account_fbid,b.account_fb_name,a.pid,b.account_token
                FROM `wooz_photos` a left join wooz_account b on b.id = a.account_id 
                WHERE account_token != '' and pid != 0 and `places_id` in (25,26,27) group by a.account_id";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        #echo '<table><tr><td>photoid</td><td>link image</td><td>name</td><td>email</td><td>hasil</td><td>link photo fb</td></tr>';
        echo '<table><tr><td>photoid</td><td>link image</td><td>name</td><td>email</td><td>hasil</td></tr>';
        foreach ($hasil as $row) {
            echo '<tr>';
            echo '<td>'.$row->id.'</td>';
            echo '<td>http://wooz.in/photoemail/uploads/photobooth/'.$row->photo_upload.'</td>';
            echo '<td>'.$row->account_fb_name.'</td>';
            echo '<td>'.$row->account_email.'</td>';
            echo '<td>https://www.facebook.com/'.$row->account_fbid.'</td>';
            $url = array('url' => base_url() . 'stats');
            $this->load->library('facebook_v4/facebooks_v4', $url);
            #$likes = $this->facebooks_v4->get_like_photo($row->pid,$row->account_token);
            $links = $this->facebooks_v4->get_friend_fb($row->account_token,$row->account_fbid);

            #echo '<td>';
            #echo count($likes);
            #echo '</td>';
            echo '<td>';
            echo $links;
            echo '</td>';
            #echo '<td>'.$row->account_token.'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }

    function gettwitphoto(){
        $sql = "SELECT a.id,a.account_id,b.account_email,b.account_tw_username,b.account_displayname,
                b.account_tw_token,a.twit_id,b.account_tw_secret
                FROM `wooz_photos` a left join wooz_account b on b.id = a.account_id 
                WHERE twit_id != 0 and `places_id` = 14";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        echo '<table><tr><td>photoid</td><td>name</td><td>email</td><td>hasil</td><td>link photo fb</td></tr>';
        foreach ($hasil as $row) {
            echo '<tr>';
            echo '<td>'.$row->id.'</td>';
            echo '<td>'.$row->account_displayname.'</td>';
            echo '<td>'.$row->account_email.'</td>';
            echo '<td>@'.$row->account_tw_username.'</td>';
            echo '<td>';
            echo $this->showrw($row->twit_id,$row->account_tw_token,$row->account_tw_secret);
            echo '</td>';
            echo '<td>';
            echo $this->showtwit($row->twit_id,$row->account_tw_token,$row->account_tw_secret);
            echo '</td>';
            #echo '<td>'.$row->account_token.'</td>';
            echo '</tr>';

        }
    }
    function showtwit($twit,$token,$secret){
        $tmhOAuth = new tmhOAuth(array(
                            'consumer_key' => $this->config->item('twiiter_consumer_key'),
                            'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
                            'user_token' => $token,
                            'user_secret' => $secret,
                        ));
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/show'), array('id' => $twit));

        if ($code == 200) {
            $resp = json_decode($tmhOAuth->response['response'], true);
            $link = $resp['extended_entities']['media'][0]['expanded_url'];
        }else{
            $link = '';
        }
        return $link;
    }
    function showrw($twit,$token,$secret){
        $tmhOAuth = new tmhOAuth(array(
                            'consumer_key' => $this->config->item('twiiter_consumer_key'),
                            'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
                            'user_token' => $token,
                            'user_secret' => $secret,
                        ));
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/retweets'), array('id' => $twit));

        if ($code == 200) {
            $resp = json_decode($tmhOAuth->response['response'], true);
            $link = count($resp);
        }else{
            $link = 0;
        }
        return $link;
    }
    
    function followers($username,$token,$secret){
        $tmhOAuth = new tmhOAuth(array(
                        'consumer_key' => $this->config->item('twiiter_consumer_key'),
                        'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
                        'user_token' => $token,
                        'user_secret' => $secret,
                    ));
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/followers/ids'), array('screen_name' => $username));
            
        if ($code == 200) {
            $resp = json_decode($tmhOAuth->response['response'], true);
            #print_r($resp['ids']);die;
            $link = count($resp['ids']);
        }else{
            $link = 0;
        }
        return $link;
    }

    function movefile(){
        $sql = "SELECT photo_upload
            FROM `wooz_photos` a inner join wooz_account b on b.id = a.account_id 
            where places_id in (25,26,27) and account_id != 0 order by a.id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        foreach ($hasil as $row) {
            $dirname = FCPATH.'uploads/photobooth/'.$row->photo_upload;
            $dirnew = FCPATH.'uploads/freedom/'.$row->photo_upload;
            copy($dirname,$dirnew);
        }
        echo 'selesai';
    }

    function gettwitphotomagnum(){
        $sql = "SELECT b.account_twid,b.account_displayname,b.account_tw_token,b.account_tw_secret,b.account_tw_username,a.twit_id
            FROM `wooz_photos` a inner join wooz_account b on b.id = a.account_id 
            where places_id in (25,26,27) and account_id != 0 group by a.account_id order by a.id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        echo '<table><tr><td>name</td><td>twname</td><td>followers</td></tr>';
        foreach ($hasil as $row) {
            echo '<tr>';
            echo '<td>'.$row->account_displayname.'</td>';
            echo '<td>@'.$row->account_tw_username.'</td>';
            #echo '<td>';
            #echo $this->showrw($row->twit_id,$row->account_tw_token,$row->account_tw_secret);
            #echo '</td>';
            echo '<td>';
            echo $this->followers($row->account_tw_username,$row->account_tw_token,$row->account_tw_secret);
            echo '</td>';
            #echo '<td>'.$row->account_token.'</td>';
            echo '</tr>';

        }
    }

    function magnumcreme(){
        $this->load->library('taggly');
        $this->load->library('fungsi');
        $sql = "SELECT count(a.id) as total
            FROM `wooz_photos` a inner join wooz_account b on b.id = a.account_id 
            where places_id in (28) and account_id != 0 and time_upload not like '%2016-06-02%'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        echo 'Total Photo : '.$hasil->total;
        $sql = "SELECT count(distinct(b.id)) as total
            FROM `wooz_photos` a inner join wooz_account b on b.id = a.account_id 
            where places_id in (28) and account_id != 0 and time_upload not like '%2016-06-02%'";
        $query = $this->db->query($sql);
        $hasil = $query->row();
        echo '<br>Total user : '.$hasil->total.'<br>';
        $sql1 = "SELECT DISTINCT(DATE_FORMAT(time_upload, '%d %M %Y')) AS thedate,count(a.id) as total
            FROM `wooz_photos` a inner join wooz_account b on b.id = a.account_id 
            where places_id in (28) and account_id != 0 and time_upload not like '%2016-06-02%' group by thedate";
        $query1 = $this->db->query($sql1);
        $hasil1 = $query1->result();
        foreach($hasil1 as $row){
            echo 'Total Photo date '.$row->thedate.' : '.$row->total.'<br>';
        }

        $sql2 = "SELECT DISTINCT(DATE_FORMAT(time_upload, '%d %M %Y')) AS thedate, date_format(time_upload,'%Y-%m-%d') as dates,count(distinct(b.id)) as total
            FROM `wooz_photos` a inner join wooz_account b on b.id = a.account_id 
            where places_id in (28) and account_id != 0 and time_upload not like '%2016-06-02%' group by thedate";
        $query2 = $this->db->query($sql2);
        $hasil2 = $query2->result();
        foreach($hasil2 as $row){
            echo 'Total User Photo date '.$row->thedate.' : '.$row->total.'<br>';
        }

        $sql3 = "SELECT account_gender,count(distinct(a.account_id)) as total FROM `wooz_photos` a inner join wooz_account b on b.id = a.account_id where places_id in (28) and account_id != 0 and time_upload not like '%2016-06-02%' group by account_gender";
        $query3 = $this->db->query($sql3);
        $hasil3 = $query3->result();
        foreach($hasil3 as $row){
            echo 'Gender '.$row->account_gender.' : '.$row->total.'<br>';
        }

        $sql4 = "SELECT distinct(a.account_id), c.`facebook_page_name`, d.`category_name` 
            FROM `wooz_landing` as a LEFT JOIN `wooz_user_likes` as b ON b.`account_id` = a.`account_id` 
            LEFT JOIN `wooz_like` as c ON c.`id` = b.`likes_id` 
            LEFT JOIN `wooz_like_category` as d ON d.`id` = c.`category_id` 
            LEFT JOIN `wooz_account` as e ON e.`id` = a.`account_id` 
            left join wooz_photos as f on f.account_id = e.id
            WHERE places_id in (28) and f.account_id != 0 and time_upload not like '%2016-06-02%' ";
        $query4 = $this->db->query($sql4);
        $datalike = $query4->result();

        foreach ($datalike as $like) {
            if(isset($like->facebook_page_name) && $like->facebook_page_name != "")
                $like_inter[] = strip_tags(trim($like->facebook_page_name));
            if(isset($like->category_name) && $like->category_name != "")
                $like_ctgry[] = strip_tags(trim($like->category_name));
        }
        $config = array(
            'min_font' => 12,
            'max_font' => 45,
            'html_start' => '',
            'html_end' => '',
            'shuffle' => FALSE,
            'class' => 'tag1',
            'match_Class' => 'bold',
        );

        if(!empty($like_inter)){
            $tag3 = $this->fungsi->insight_break($like_inter);
            $likes_tag = $this->taggly->cloud($tag3, $config);
        }
        if(!empty($like_ctgry)){
            $tag4 = $this->fungsi->insight_break($like_ctgry);
            $likes_cat = $this->taggly->cloud($tag4, $config); 
        }
        echo '<p><legend class="legend-insight"> Likes (by Name)</legend>';
        if(isset($likes_tag)) { echo $likes_tag; }else{
                    echo 'Not Data Likes(by Name)'; }
        echo '</p><p>
                <legend class="legend-insight"> Likes (by Category)</legend>';
        if(isset($likes_cat)){ echo $likes_cat; }else{
                   echo 'Not Data Likes(by Category)'; }
        echo '</p>';
        echo '<b>List User Photo :</b>';
        $this->datausertable();

        foreach($hasil2 as $row){
            echo '<br><b>Total User Photo date '.$row->dates.' :</b><br>';
            $this->datausertable($row->dates);
        }

    }

    function datausertable($date = null){
        echo '<table class="tables">';
        echo '<thead>
            <tr>
                <th>
                    <strong>No.</strong>
                </th>
                <th>
                    <strong>Nama</strong>
                </th>
                <th>
                    <strong>Facebook</strong>
                </th>
                <th>
                    <strong>Gender</strong>
                </th>
                <th>
                    <strong>Total</strong>
                </th>           
            </tr>
        </thead>';
        echo '<tbody>';
        $sql1 = "SELECT DISTINCT(a.account_id),b.account_fb_name,concat('https://www.facebook.com/',b.account_fbid) as fb,b.account_gender,count(a.id) as total
            FROM `wooz_photos` a inner join wooz_account b on b.id = a.account_id 
            where places_id in (28) and account_id != 0 and time_upload not like '%2016-06-02%'";
        if($date){
            $sql1 .= " and time_upload like '%".$date."%'";
        }
        $sql1 .= " group by a.account_id order by total desc";
        $query1 = $this->db->query($sql1);
        $data1 = $query1->result();
        $x = 1;
        foreach($data1 as $row){     
            echo '<tr class="oddrow">';
            echo '<td class="firstcol">'.$x.'</td>';
            echo '<td class="dataitem">'.$row->account_fb_name.'</td>';
            echo '<td class="dataitem">'.$row->fb.'</td>';
            echo '<td class="dataitem">'.$row->account_gender.'</td>';
            echo '<td class="dataitem">'.$row->total.'</td>';
            echo '</tr>';
            $x++;
        }
        echo '</tbody>';
        echo '</table>';
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */