<?php

class androidapi extends CI_controller {

    protected $tpl;

    function androidapi() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        $this->load->model('photobooth/photobooth_m');
        $this->load->library('curl');
        $this->load->library('fungsi');
        include(APPPATH . 'third_party/tmhOAuth.php');
        include(APPPATH . 'third_party/tmhUtilities.php');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));
    }

    function main($placesid) {
        #$placesid = 259;
        $cserial = $this->input->get('no_rfid', true);
        if (!$cserial) {
            $cserial = $this->input->post('no_rfid', true);
        }
        if (isset($cserial) && $cserial != '') {
            $venue = $this->db->get_where('places', array('id' => $placesid))->row();
            $user = $this->cekuser($cserial, $venue->places_parent);
            if ($user) {
                $data = array(
                    'status' => 1,
                    'id' => $user->id,
                    'rfid' => $cserial,
                    'places_id' => $placesid,
                    'name' => $user->account_displayname
                );
                echo json_encode($data);
            } else { //belum registrasi
                $data = array(
                    'status' => 0,
                    'name' => 'not registered'
                );
                echo json_encode($data);
            }
        } else { //rfid tidak ada
            $data = array(
                'status' => 0,
                'name' => 'no rfid'
            );
            echo json_encode($data);
        }
    }

    function cekadmin() {
        $username = $this->input->get('username', true);
        if (!$username) {
            $username = $this->input->post('username', true);
        }
        $password = $this->input->get('pass', true);
        if (!$password) {
            $password = $this->input->post('pass', true);
        }
        if ($password && $username) {
            if ($username == 'admin' && $password == 'admin') {
                $data = array(
                    'status' => 1,
                    'name' => 'sukses'
                );
            } else {
                $data = array(
                    'status' => 0,
                    'name' => 'no'
                );
            }
        } else {
            $data = array(
                'status' => 2,
                'name' => 'no'
            );
        }
        echo json_encode($data);
    }

    function cekprint() {
        $dataasli['post'] = json_encode($_POST);
        $dataasli['data_status'] = 0;
        $this->db->insert('data_post_ford', $dataasli);
        $dataasli_id = $this->db->insert_id();
        $nama_file = $this->input->post('photo_path', true);
        $no_rfid = $this->input->post('no_rfid', true);
        $acc_id = $this->input->post('acc_id', true);
        $places_id = $this->input->post('places_id', true);
        if ($nama_file && $no_rfid && $acc_id && $places_id) {
            $data = array(
                'status' => 1,
                'name' => 'yes'
            );
        } else {
            $data = array(
                'status' => 0,
                'name' => 'no'
            );
        }
        echo json_encode($data);
    }

    function tagphoto() {
        $dataasli['post'] = json_encode($_POST);
        $dataasli['data_status'] = 0;
        $this->db->insert('data_post_ford', $dataasli);
        $dataasli_id = $this->db->insert_id();
        $nama_file = $this->input->post('photo_path', true);
        $no_rfid = $this->input->post('no_rfid', true);
        $acc_id = $this->input->post('acc_id', true);
        $places_id = $this->input->post('places_id', true);
        $xpos = $this->input->post('xpos', true);
        $ypos = $this->input->post('ypos', true);
        if ($nama_file && $no_rfid && $acc_id && $places_id && $xpos && $ypos) {
            $data = array(
                'status' => 1,
                'name' => 'yes'
            );
        } else {
            $data = array(
                'status' => 0,
                'name' => 'no'
            );
        }
        echo json_encode($data);
    }

    function upload() {
        $upload_directory = '/home/socialite/public_html/uploads/mobile/';

        $dataasli['post'] = json_encode($_POST);
        $dataasli['data_status'] = 0;
        $this->db->insert('data_post_ford', $dataasli);

        if (isset($_POST['upload'])) {
            if (!empty($_FILES['photo_path'])) {
                //check for image submitted
                if ($_FILES['photo_path']['error'] > 0) {
                    // check for error re file
                    $data = array(
                        'status' => 0,
                        'message' => 'no',
                        'name' => 'file photo error'
                    );
                    echo json_encode($data);
                } else {
                    $no_rfid = $this->input->post('no_rfid', true);
                    $acc_id = $this->input->post('acc_id', true);
                    $places_id = $this->input->post('places_id', true);
                    #$places_id = 459;
                    //move temp file to our server   
                    move_uploaded_file($_FILES['photo_path']['tmp_name'], $upload_directory . $_FILES['photo_path']['name']);
                    $venue = $this->db->get_where('places', array('id' => $places_id))->row();
                    $user = $this->cekuser($no_rfid, $venue->places_parent);
                    if (!$user) {
                        $user = $this->db->get_where('account', array('id' => $acc_id))->row();
                    }
                    $data = array(
                        'status' => 1,
                        'message' => 'yes',
                        'name' => $user->account_displayname
                    );
                    echo json_encode($data);
                    $upload = $this->uploads($no_rfid, $acc_id, $places_id, $_FILES['photo_path']['name']);
                }
            } else {
                $data = array(
                    'status' => 0,
                    'message' => 'no',
                    'name' => 'file photo not found'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 0,
                'message' => 'no',
                'name' => 'Upload not found'
            );
            echo json_encode($data);
        }
    }

    function uploads($no_rfid, $acc_id, $places_id, $file_name) {
        $user = $this->db->get_where('account', array('id' => $acc_id))->row();
        if (!$user) {
            $user = $this->cekuser($no_rfid, $places_id);
        }
        $venue = $this->db->get_where('places', array('id' => $places_id))->row();
        $this->mergephoto($file_name, $venue->places_frame_url);
        if ($user) {
            $input = array(
                'account_id' => $user->id,
                'places_id' => $places_id,
                'photo_upload' => addslashes($file_name),
                'code_photo' => 'p' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id,
                'time_upload' => date("Y-m-d H:i:s"),
                'status' => 1
            );
            $photos = $this->photobooth_m->insert($input, 'photos');
            $image = '/home/socialite/public_html/uploads/mobile/' . $file_name;

            if ($user->account_tw_token && $user->account_tw_secret) {
                $consumer_key = $this->config->item('twiiter_consumer_key');
                $consumer_key_secret = $this->config->item('twiiter_consumer_secret');
                $tmhOAuth = new tmhOAuth(array(
                    'consumer_key' => $consumer_key,
                    'consumer_secret' => $consumer_key_secret,
                    'user_token' => $user->account_tw_token,
                    'user_secret' => $user->account_tw_secret,
                ));
                $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update_with_media'), array(
                    'media[]' => "@{$image};type=image/jpeg;filename={$image}",
                    'status' => $venue->places_tw_caption,
                        #'status' => 'test',
                        ), true, // use auth
                        true  // multipart
                );
                $resp = json_encode($tmhOAuth->response['response']);
                $resp1 = json_decode($resp);
                $twit_id = json_decode($resp1);
                if ($code == 200) {
                    $inputs['twit_id'] = $twit_id->id_str;
                } else {
                    $inputs['twit_id'] = 0;
                }
            }

            if ($user->account_fbid && $user->account_token) {
                try {
                    /* check for photobooth album */
                    $url = "https://graph.facebook.com/me/albums?access_token=" . $user->account_token;
                    $info1 = json_decode($this->curl->simple_get($url));
                    if (!isset($info1)) {
                        $inputs['pid'] = 0;
                    } else {
                        $info = $info1;

                        $info = $info->data;
                        $up_to = false;
                        foreach ($info as $alb) {
                            if ($alb->name == $venue->places_album) {
                                $up_to = $alb->id;
                            }
                        }
                        if ($up_to) {
                            $album_id = $up_to;
                        } else {
                            try {
                                $options['name'] = $venue->places_album;
                                $options['access_token'] = $user->account_token;
                                $url = 'https://graph.facebook.com/' . $user->account_fbid . '/albums';
                                $album = json_decode($this->curl->simple_post($url, $options));
                                $album_id = $album->id;
                            } catch (FacebookApiException $e) {
                                error_log($e);
                                $inputs['pid'] = 0;
                            }
                        }
                        try {
                            $photo = $venue->places_fb_caption;
                            #$photo = 'test';
                            /* upload photo */
                            $this->facebook->setFileUploadSupport(true);
                            $attachement = array(
                                'access_token' => $user->account_token,
                                'caption' => 'uploaded foto',
                                'name' => $photo,
                                'source' => '@' . $image
                            );
                            $upload = $this->facebook->api($album_id . '/photos', 'POST', $attachement);
                            $new = $upload['id'];

                            /* get photo pid before tagging, convert return id form upload to pid */
                            $response = $this->facebook->api(array(
                                'access_token' => $user->account_token,
                                'method' => 'fql.query',
                                'query' => 'SELECT pid FROM photo WHERE object_id ="' . $new . '" ',
                            ));

                            foreach ($response as $pid) {
                                $new_pid = $pid['pid'];
                            }

                            if (isset($new_pid) && $new_pid != '0') {
                                $inputs['pid'] = $new_pid;
                            } else {
                                $inputs['pid'] = $new;
                            }
                        } catch (FacebookApiException $e) {
                            error_log($e);
                            $inputs['pid'] = 0;
                        }
                    }
                } catch (FacebookApiException $e) {
                    error_log($e);
                    $inputs['pid'] = 0;
                }
            }

            if ($venue->id == 459) {
                $album_page_id = '646018515473100';
                $fanpage_id = '387243381350616';
                $fanpage_token = 'CAACGFg9ewNIBAP1TSbCwmMa2bV0XZCWhw1ZAIJUxbqH3eRoMjKABZBjGAX5G3RXs54rrUw4RsW59jmdz6kGXldrFZB7GRZAc5b0H4wBFLJqrJ4MsKgpUYJoLArLKJWeDp9wamyIpPxZA8VdenjGNxeZBwYTbTqgK8xPCz4UO7hsFzbkLN4RZBxXNltHgNrqSaoUZD';
                $this->fb_fan_page($venue, $image, $fanpage_id, $fanpage_token, $album_page_id);
            }
            $this->photobooth_m->update($photos, $inputs);
            $this->photobooth_m->insertlog($photos);

            return true;
        } else {
            return false;
        }
    }

    function fb_fan_page($copytext, $file_name, $account_fb_id, $account_fb_token, $album_id) {
        $hasil = 0; //awal
        try {
            /* get album photo */
            $text = $copytext->places_cstatus_fb;
            /* upload photo */
            $this->facebook->setFileUploadSupport(true);
            $attachement = array(
                'access_token' => $account_fb_token,
                'caption' => 'uploaded foto',
                'message' => $text,
                'no_story' => 1,
                'source' => '@' . $file_name
            );
            $upload = $this->facebook->api($album_id . '/photos', 'POST', $attachement);
            $hasil = $upload['id'];
        } catch (FacebookApiException $e) {
            error_log($e);
            $hasil = 1; //list album error
        }
        return $hasil;
    }

    function cekuser($no_rfid, $placesid) {
        $user = $this->db->get_where('account', array('account_rfid' => $no_rfid))->row();
        if ($user) {
            return $user;
        } else {
            $this->db->select('wooz_account.*');
            $this->db->join('account', 'account.id = landing.account_id', 'left');
            $this->db->where('landing.landing_register_form', $placesid);
            $this->db->where("landing.landing_rfid", $no_rfid);
            $this->db->from('landing');
            $acc = $this->db->get()->row();
            if ($acc) {
                return $acc;
            } else {
                return false;
            }
        }
    }

    function mergephoto($file_path, $frame) {
        #$file = FCPATH . 'mobile/upload/' . $file_name;
        #$file = FCPATH . 'mobile/upload/0323758059_98749571.jpg';
        $file = '/home/woozin/public_html/uploads/magnuminfinitymobile/' . $file_path;
        $src = '/home/woozin/public_html/uploads/magnuminfinitymobile/' . $file_path;
        $w = 890;
        $h = 1260;
        /* photo setup */
        $simg = imagecreatefromjpeg($file);
        $targetImage = imagecreatetruecolor($w, $h);
        imagecolorallocate($targetImage, 0, 0, 0);
        imagecopyresampled($targetImage, $simg, 0, 0, 0, 0, $w, $h, 648, 1152);

        imagejpeg($targetImage, $src, 100);
        imagedestroy($targetImage);
        $this->mergephoto1($file_path, $frame);
    }

    function mergephoto1($file_path, $frame) {
        $file = '/home/woozin/public_html/uploads/magnuminfinitymobile/' . $file_path;
        $dest = imagecreatefrompng('/home/woozin/public_html/uploads/photoframe/koran.png');
        $src = imagecreatefromjpeg('/home/woozin/public_html/uploads/magnuminfinitymobile/' . $file_path);
        $w = 1200;
        $h = 1800;
        /* photo setup */
        $targetImage = imagecreatetruecolor($w, $h);
        imagecolorallocate($targetImage, 0, 0, 0);
        $default = imagecreatetruecolor($w, $h);
        imagecopyresampled($default, $src, 41, 435, 0, 0, $w, $h, $w, $h);
        imagecopyresampled($targetImage, $default, 0, 0, 0, 0, $w, $h, $w, $h);
        imagecopyresampled($targetImage, $dest, 0, 0, 0, 0, $w, $h, $w, $h);

        imagejpeg($targetImage, $file, 100);
        imagedestroy($default);
        imagedestroy($targetImage);
        imagedestroy($dest);
        imagedestroy($src);
    }

    /*
      function mergephoto($file_path,$frame){
      #$file = FCPATH . 'mobile/upload/' . $file_name;
      #$file = FCPATH . 'mobile/upload/0323758059_98749571.jpg';
      $file = '/home/woozin/public_html/uploads/vm/'.$file_path;
      #xdebug($file);
      #xdebug($frame);die;
      $dest  = imagecreatefrompng('/home/woozin/public_html'.$frame);
      $src = imagecreatefromjpeg('/home/woozin/public_html/uploads/vm/'.$file_path);
      $w = 640;
      $h = 480;
      /* photo setup
      $targetImage = imagecreatetruecolor($w, $h);
      imagecolorallocate($targetImage, 0, 0, 0);
      $default = imagecreatetruecolor($w, $h);
      imagecopyresampled($default, $src, 0, 0, 0, 0, $w, $h, $w, $h);
      imagecopyresampled($targetImage, $default, 0, 0, 0, 0, $w, $h, $w, $h);
      imagecopyresampled($targetImage, $dest, 0, 0, 0, 0, $w, $h, $w, $h);

      imagejpeg($targetImage, $file, 100);
      imagedestroy($default);
      imagedestroy($targetImage);
      imagedestroy($dest);
      imagedestroy($src);

      } */

    function photo() {
        $upload_directory = '/home/socialite/public_html/uploads/mobile/';
        if (isset($_POST['upload'])) {
            if (!empty($_FILES['photo_path'])) {
                //check for image submitted
                if ($_FILES['photo_path']['error'] > 0) {
                    // check for error re file
                    $data = array(
                        'status' => 0,
                        'message' => 'no',
                        'name' => 'file photo error'
                    );
                    echo json_encode($data);
                } else {
                    $places_id = 17;
                    //move temp file to our server      
                    move_uploaded_file($_FILES['photo_path']['tmp_name'], $upload_directory . $_FILES['photo_path']['name']);
                    $input = array(
                        'account_id' => 0,
                        'places_id' => $places_id,
                        'photo_file_url' => addslashes($_FILES['photo_path']['name']),
                        'code_photo' => 'p' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $_FILES['photo_path']['name'],
                        'date_add' => date("Y-m-d H:i:s"),
                        'data_status' => 1
                    );
                    $this->photobooth_m->insert($input, 'photos_api');
                    $data = array(
                        'status' => 1,
                        'message' => 'yes',
                        'name' => 'upload photo success'
                    );
                    echo json_encode($data);
                }
            } else {
                $data = array(
                    'status' => 0,
                    'message' => 'no',
                    'name' => 'file photo not found'
                );
                echo json_encode($data);
            }
        } else {
            $data = array(
                'status' => 0,
                'message' => 'no',
                'name' => 'Upload not found'
            );
            echo json_encode($data);
        }
    }

}
