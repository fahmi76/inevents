<?php
class statistik extends CI_controller {

    protected $tpl;

    function __construct() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        //$this->load->library('model');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        //$this->load->library('EpiFoursquare');
        $this->load->library('bitly', array(
            'bitly_login' => $this->config->item('bitly_login'),
            'bitly_apiKey' => $this->config->item('bitly_apiKey')
        ));
        $this->load->helper('url');
        $this->load->helper('date');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true
        ));

    }

	function index(){
        $this->tpl['core'] = 'Statistik';
        $this->tpl['title'] = "Statistik";
		$id = array("299","300","301","302","303","304");

        $this->db->select('id,places_name');
        $this->db->where_in('id', $id);
        $this->db->order_by('id', 'asc');
        #$this->db->limit(20,0);
        $this->db->from('places');
        $this->tpl['places'] = $this->db->get()->result();

        $this->db->select('distinct(account_id),wooz_account.account_displayname');
        $this->db->where_in('log.places_id', $id);
        $this->db->where('log.log_status', 1);
        $this->db->where('log.log_type', 1);
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->order_by('log.id', 'asc');
        #$this->db->limit(20,0);
        $this->db->from('log');

        $this->tpl['stat'] = $this->db->get()->result();
		$this->tpl['TContent'] = $this->load->view('heineken/statistik', $this->tpl, true);   
        $this->load->view('heineken/body', $this->tpl);	
	}

	function excel(){
        $this->tpl['core'] = 'Statistik';
        $this->tpl['title'] = "Statistik";
		$id = array("299","300","301","302","303","304");

        $this->db->select('id,places_name');
        $this->db->where_in('id', $id);
        $this->db->order_by('id', 'asc');
        #$this->db->limit(20,0);
        $this->db->from('places');
        $this->tpl['places'] = $this->db->get()->result();

        $this->db->select('distinct(account_id),wooz_account.account_displayname,wooz_account.account_email');
        $this->db->where_in('log.places_id', $id);
        $this->db->where('log.log_status', 1);
        $this->db->where('log.log_type', 1);
        $this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->order_by('log.id', 'asc');
        #$this->db->limit(20,0);
        $this->db->from('log');

        $this->tpl['stat'] = $this->db->get()->result();
		$this->tpl['TContent'] = $this->load->view('heineken/statistik_excel', $this->tpl, true);   
        $this->load->view('heineken/body', $this->tpl);	
	}


	function data(){
		echo '<table class="tables">';
		echo '<thead>
            <tr>
                <th>
                    <strong>No.</strong>
                </th>
                <th>
                    <strong>Nama Event</strong>
                </th>
                <th>
                    <strong>Lokasi Event</strong>
                </th>
                <th>
                    <strong>Start Date</strong>
                </th>
                <th>
                    <strong>End Date</strong>
                </th>		
                <th>
                    <strong>User Registrasi</strong>
                </th>			
                <th>
                    <strong>User New Registrasi</strong>
                </th>		 	
            </tr>
        </thead>';
		echo '<tbody>';
		$sql1 = "SELECT `id`,`places_name`,places_address,`places_startdate`,`places_duedate` 
				 FROM `wooz_places` WHERE `places_startdate` like '%2014%' and `places_parent` = 0 order by id asc";
		$query1 = $this->db->query($sql1);
        $data1 = $query1->result();
		$x = 1;
		foreach($data1 as $row){
			$sql2 = "SELECT count(distinct(account_id)) as total 
					 FROM `wooz_landing` 
					 WHERE `landing_register_form` = ".$row->id." 
					 and landing_joindate >= '".$row->places_startdate."' and landing_joindate <= '".$row->places_duedate."'";
			$query2 = $this->db->query($sql2);
			$data2 = $query2->row();
			$sql3 = "SELECT count(distinct(a.id)) as total 
					 FROM `wooz_account` a inner join wooz_landing b on b.account_id = a.id
					 WHERE b.`landing_register_form` = ".$row->id." 
					 and a.`account_joindate` >= '".$row->places_startdate."' and a.account_joindate <= '".$row->places_duedate."'";
			$query3 = $this->db->query($sql3);
			$data3 = $query3->row();
			$sql21 = "SELECT count(distinct(account_id)) as total 
					 FROM `wooz_landing` 
					 WHERE `landing_register_form` = ".$row->id;
			$query21 = $this->db->query($sql21);
			$data21 = $query21->row();
			$sql31 = "SELECT count(distinct(a.id)) as total 
					 FROM `wooz_account` a WHERE a.account_register_form = ".$row->id;
			$query31 = $this->db->query($sql31);
			$data31 = $query31->row();
			
			echo '<tr class="oddrow">';
			echo '<td class="firstcol">'.$x.'</td>';
			echo '<td class="dataitem">'.$row->places_name.'</td>';
			echo '<td class="dataitem">'.$row->places_address.'</td>';
			echo '<td class="dataitem">'.$row->places_startdate.'</td>';
			echo '<td class="dataitem">'.$row->places_duedate.'</td>';
			echo '<td class="dataitem">'.$data2->total.'</td>';
			echo '<td class="dataitem">'.$data3->total.'</td>';
			echo '<td class="dataitem">'.$data21->total.'</td>';
			echo '<td class="dataitem">'.$data31->total.'</td>';
			echo '</tr>';
			$x++;
		}
		echo '</tbody>';
		echo '</table>';
	}


    function getlikeinterest(){
        set_time_limit(0);
        $this->load->library('like_interest');
        
        $sql = "SELECT id,account_token FROM  wooz_account_sosmed WHERE account_token != ''";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        foreach($hasil as $row){
            $this->like_interest->set_token($row->account_token);
            $this->like_interest->insert_like($row->id);
            #$this->like_interest->insert_interest($row->id);
            echo $row->id.' - sukses <br/>';
        }
    }
    function getfriend(){
        set_time_limit(0);
        $this->load->library('sosmed');
        $sql = "SELECT id,account_fbid,account_token,account_tw_token,account_tw_secret,account_tw_username FROM wooz_account_sosmed 
                    WHERE account_fbid is not null or account_twid is not null";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        echo '<table>';
        echo '<tr><td>id</td><td>fb friend</td><td>tw friend</td><tr>';
        $tw_friend = 0;
        $fb_friend = 0;
        foreach($hasil as $user){
            $landing['fb_friend'] = 0;
            $landing['tw_friend'] = 0;
            echo '<tr><td>'.$user->id.'</td>';
            if($user->account_fbid && $user->account_token){
                $landing['fb_friend'] = $this->sosmed->get_friend_fb($user->account_token);
            }
            if($user->account_tw_token && $user->account_tw_secret){
                $landing['tw_friend'] = $this->sosmed->friend_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username);
            }
            echo '<td>'.$landing['fb_friend'].'</td>';
            echo '<td>'.$landing['tw_friend'].'</td>';
            #$landing['account_id'] = $user->id;
            #$this->db->insert('friend', $landing);
            #echo $user->id . ' - sukses <br/>';
            echo '</tr>';
            $fb_friend = $fb_friend + $landing['fb_friend'];
            $tw_friend = $tw_friend + $landing['tw_friend'];
        }
        echo 'fb friend total = '.$fb_friend.'<br>';
        echo 'tw friend total = '.$tw_friend;
    }

    function photobooth(){
        
    }
}
?>
