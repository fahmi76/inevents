<?php

class regis extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->model('dashboard/visitor_m');
        $this->load->model('dashboard/user_m');

        $this->load->library('fungsi');
        $this->load->library('session');
        $this->load->library('curl');
        #date_default_timezone_set('Asia/Jakarta');
        date_default_timezone_set('America/Port_of_Spain');

        $this->load->helper('url');
        #$this->load->library('sosmed');

        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');

        $custom_page = $this->uri->segment(3,0);
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }
    
    function index(){
        $this->home();
    }

    function home() {
        header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->tpl['main_title'] = 'Registration';
        $this->tpl['core'] = 'Registration';
        $this->tpl['title'] = "Inevents";
        $form = $this->visitor_m->list_form_regis($this->custom_id);

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama', 'trim|required');
        foreach ($form as $row) {
            switch ($row->field_type) {
                case 'radio':
                    if($row->field_required == 1){
                        $this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
                    }else{
                        $this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
                    }
                    break;
                default:
                    if($row->field_required == 1){
                        $this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim|required');
                    }else{
                        $this->form_validation->set_rules($row->field_nicename, $row->field_name, 'trim');
                    }
                    break;
            }
        }

        if ($this->form_validation->run() === TRUE) {
            $in_data['account_displayname'] = $this->input->post('name', true);
            $in_data['account_username'] = $this->user_m->nicename($this->input->post('name', true));
            $in_data['account_rfid'] = $this->input->post('rfid', true);
            $in_data['account_status'] = 2;
            $this->db->insert('account', $in_data);
            $acc_id = $this->db->insert_id();
            $in_data_landing['account_id'] = $acc_id;
            $in_data_landing['landing_register_form'] = $this->custom_id;
            $in_data_landing['landing_rfid'] = $in_data['account_rfid'];
            $this->db->insert('landing', $in_data_landing);
            foreach ($form as $row) {
                if($row->field_type == 'file'){
                    $header_post = '';
                    if($this->do_upload($row->field_nicename)){
                        $header_post = str_replace(' ', '_', $_FILES[$row->field_nicename]['name']);
                        $in_data_rfid['form_regis_id'] = $row->id;
                        $in_data_rfid['account_id'] = $acc_id;
                        $in_data_rfid['places_id'] = $this->custom_id;
                        $in_data_rfid['content'] = $header_post;
                        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
                        $rowcontent = $this->user_m->cek_content_data($acc_id,$id,$row->id);
                        if($rowcontent){
                            $this->db->where('id', $rowcontent->id);
                            $this->db->update('form_regis_detail', $in_data_rfid);
                        }else{
                            $this->db->insert('form_regis_detail', $in_data_rfid);
                        }
                    }
                }else{
                    $data_content = $this->input->post($row->field_nicename, true);
                    if($data_content){
                        $in_data_rfid['form_regis_id'] = $row->id;
                        $in_data_rfid['account_id'] = $acc_id;
                        $in_data_rfid['places_id'] = $this->custom_id;
                        $in_data_rfid['content'] = $data_content;
                        $in_data_rfid['date_add'] = date('Y-m-d H:i:s');
                        
                        $rowcontent = $this->user_m->cek_content_data($acc_id,$this->custom_id,$row->id);
                        if($rowcontent){
                            $this->db->where('id', $rowcontent->id);
                            $this->db->update('form_regis_detail', $in_data_rfid);
                        }else{
                            $this->db->insert('form_regis_detail', $in_data_rfid);
                        } 
                    }
                }
            }
            redirect('regis/done/'.$this->custom_url);
        } else {
            $this->tpl['form'] = $form;
            $this->tpl['visitor'] = $this->visitor_m->list_visitor_event($this->custom_id);
            $this->tpl['TContent'] = $this->load->view('regis/form', $this->tpl, true);
        }
        $this->load->view('regis/home', $this->tpl);
    }
    
    function do_upload($file){
        $name = str_replace(' ', '_', $_FILES[$file]['name']);
        $config =  array(
                  'upload_path'     => FCPATH."/uploads/user/",
                  'upload_url'      => base_url()."uploads/user/",
                  'allowed_types'   => "gif|jpg|png|jpeg",
                  'overwrite'       => TRUE,
                  'file_name'       => $name
                );

        $this->load->library('upload', $config);
        if($this->upload->do_upload($file))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function done(){
        $this->tpl['page'] = "Done";
        $this->tpl['title'] = "Personal Information";

        $accid = $this->uri->segment(3,0);
        $user = $this->db->get_where('account_sosmed', array('id' => $accid))->row();
        $this->tpl['logoutfb'] = 0;


        $this->tpl['page'] = "thankyou";
        $this->tpl['title'] = "Thankyou!";
        $this->tpl['TContent'] = $this->load->view('regis/signupdone', $this->tpl, true);
        $this->load->view('regis/home', $this->tpl);
    }

    function logout() {
        $this->session->unset_userdata('session_id');
        $this->session->unset_userdata('ip_address');
        $this->session->unset_userdata('user_agent');
        $this->session->unset_userdata('last_activity');
        #$this->_killFacebookCookies();
        $this->session->sess_destroy();
        session_destroy();
        unset($_SESSION);
        $past = time() - 3600;
        foreach ($_COOKIE as $key => $value) {
            setcookie($key, $value, $past, '/');
        }
        if($this->custom_id == 21){
            redirect('http://www.fashionfocus.org');
        }
        redirect('regis/home/'.$this->custom_url);
    }

    function rfid_used($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('rfid');
        }
        $new = $this->db->get_where('account_sosmed', array('account_rfid' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('account_sosmed', array('account_rfid' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('rfid_used', 'RFID already registered');
                return false;
            }
        } else {
            return true;
        }
    }

}
