<?php

class firstcitizens extends CI_controller {

    protected $tpl;

    function __construct() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        //$this->load->library('model');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->library('sosmed');
        $this->load->helper('url');
        $this->lang->load('form_validation', 'en');
        $this->load->helper('date');
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        date_default_timezone_set('Asia/Jakarta');

        #$custom_page = $this->input->get('place', true);
        $custom_page = 'firstcitizens';
        if ($custom_page != '') {
            $customs = $this->db->get_where('places', array('places_landing' => $custom_page))->row();
            if ($customs) {
                $a = strtotime($customs->places_duedate);
                $b = time();
                if ($customs->places_duedate != '0000-00-00' && $a > $b) {
                    $this->custom_id = $customs->id;
                    $this->custom_url = $customs->places_landing;
                    $this->spot_id = $this->input->get('places', true);
                    $this->twitter_name = $customs->places_twitter;
                    $this->tpl['spot_id'] = $this->input->get('place', true);
                    #$this->custom_model = $customs->places_model;
                    $this->tpl['customs'] = $customs->places_landing;
                    $this->tpl['css'] = $customs->places_css;
                    $this->tpl['background'] = $customs->places_backgroud;
                    $this->tpl['logo'] = $customs->places_logo;
                    $this->tpl['main_title'] = $customs->places_name;
                    $this->tpl['type_registration'] = 1;
                } else {
                    redirect('?status=' . $custom_page . '_custom_page_end_from_showing');
                }
            } else {
                redirect('?status=custom_page_not_registered');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $this->tpl['page'] = "home";
        $this->tpl['body_class'] = '';

        $this->tpl['TContent'] = $this->load->view('gate/listplaces', $this->tpl, true);
        $this->load->view('gate/home', $this->tpl);
    }

    function home($places_id) {
        $this->tpl['page'] = "home";
        $this->tpl['body_class'] = '';
        $this->tpl['places_id'] = $places_id;

        $this->db->where('id', $places_id);
        $this->db->from('places');
        $places_data = $this->db->get()->row();
        $this->tpl['places_name'] = $places_data->places_name;
        $this->tpl['title'] = $places_data->places_name . "-update";
        $this->tpl['places_text'] = $places_data->places_cstatus_fb;

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('serial', 'Serial RFID', 'trim|required');

        if (($this->form_validation->run() === TRUE) && (isset($_POST['submit']))) {
            $rfid = $this->input->post('serial', true);
            $sql = "select id,account_displayname,account_avatar,account_avatar_type "
                    . "from wooz_account_pre where account_rfid = '" . $rfid . "'";
            $query = $this->db->query($sql);
            $data = $query->row();
            if ($data) {
                $this->tpl['datauser'] = $data;
                $this->tpl['TContent'] = $this->load->view('gate/foundstation', $this->tpl, true);
            } else {
                $this->tpl['TContent'] = $this->load->view('gate/notfoundstation', $this->tpl, true);
            }
        } else {
            $this->tpl['TContent'] = $this->load->view('gate/rfid_1', $this->tpl, true);
        }
        $this->load->view('gate/home', $this->tpl);
    }

    function done($id,$places_id) {
		$sql = "select id,account_displayname,account_fbid,account_token,account_fb_name,account_tw_token,account_tw_secret "
                    . "from wooz_account_pre where id = '" . $id . "'";
        $query = $this->db->query($sql);
        $user = $query->row();
        $venue = $this->db->get_where('places', array('id' => $places_id))->row();
		$image = FCPATH . $venue->places_avatar;
		if($user->account_fbid && $user->account_token){
			$db['log_fb'] = $this->sosmed->upload_photo_fb_custom($venue, $image, $user->account_fb_name, 
				$user->account_fbid, $user->account_token, $venue->places_cstatus_fb);
		}
		if($user->account_tw_token && $user->account_tw_secret){
			$db['log_tw'] = $this->sosmed->upload_photo_tw($venue->places_cstatus_tw, $image, 
				$user->account_tw_token, $user->account_tw_secret);
		}
		
        $date = date('Y-m-d');
        $time = date('h:i:s');
        $db['account_id'] = $id;
        $db['places_id'] = $places_id;
        $db['log_type'] = 1;
        $db['log_date'] = $date;
        $db['log_time'] = $time;
        $db['log_hash'] = 'v' . $this->fungsi->acak(time()) . '-' . $id;
        $db['log_status'] = 1;
        $upd = $this->db->insert('log_user', $db);
        redirect('firstcitizens/home/'.$places_id);
    }

    function listuser() {
        $this->tpl['page'] = "home";
        $this->tpl['body_class'] = '';
        $this->content['title'] = "| DS - tap";

        $sql = "select distinct(a.account_id),b.account_displayname,b.account_avatar,a.log_time "
                . "from wooz_account_pre b inner join wooz_log_user a on a.account_id = b.id "
                . "group by a.account_id order by a.id desc";
        $query = $this->db->query($sql);
        $this->tpl['data'] = $query->result();
        $this->tpl['TContent'] = $this->load->view('gate/listuser', $this->tpl, true);
        $this->load->view('gate/home', $this->tpl);
    }

}
