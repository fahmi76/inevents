<?php

class event extends CI_controller {

	protected $tpl;

	function __construct() {
		parent::__construct();
		$this->tpl['TContent'] = null;
		$this->load->database();
		$this->load->library('curl');
		$this->load->library('fungsi');
		$this->tpl['assets_url'] = base_url() . 'assets';
	}

	function index() {
		$date = date('Y-m-d');
		$traps = array();
		$this->db->select('id,places_landing,places_name,places_duedate');
		$this->db->from('places');
		$this->db->where('places_parent', 0);
		$customs = $this->db->get()->result();
		foreach ($customs as $row) {
			if ($row->places_duedate >= $date) {
				$traps[] = array(
					'id' => $row->id,
					'url' => $row->places_landing,
					'name' => $row->places_name,
				);
			}
		}
		$this->tpl['data'] = $traps;
		$this->tpl['TContent'] = $this->load->view('events/default', $this->tpl, true);
		$this->load->view('events/home', $this->tpl);
	}
	function main($id) {
		$date = date('Y-m-d');
		$this->db->select('id,places_landing,places_name,places_duedate');
		$this->db->from('places');
		$this->db->where('id', $id);
		$customs = $this->db->get()->row();
		$this->tpl['places'] = $customs;
		$this->tpl['places_id'] = $id;
		$this->tpl['TContent'] = $this->load->view('events/main', $this->tpl, true);
		$this->load->view('events/home', $this->tpl);

	}

	function quisioner($id) {
		$date = date('Y-m-d');
		$this->db->select('id,status,title');
		$this->db->from('quisioner_setting');
		$this->db->where('places_id', $id);
		$customs = $this->db->get()->result();
		$this->tpl['places'] = $customs;
		$this->tpl['places_id'] = $id;
		$this->tpl['TContent'] = $this->load->view('events/quisioner', $this->tpl, true);
		$this->load->view('events/home', $this->tpl);
	}

	function location($id, $loc) {
		$date = date('Y-m-d');
		$traps = array();
		$this->db->select('id,places_landing,places_nicename,places_name,places_type,places_parent,places_duedate');
		$this->db->from('places');
		$this->db->where('places_landing', $loc);
		$this->db->or_where('places_parent', $id);
		$customs = $this->db->get()->result();
		$status = array();
		$photo = array();
		$private = array();
		$registrasi = array();
		foreach ($customs as $row) {
			if ($row->places_duedate >= $date) {
				if ($row->places_parent != 0) {
					if ($row->places_type == 6) {
						$photo[] = array(
							'url' => $row->places_landing,
							'nice' => $row->places_nicename,
							'name' => $row->places_name,
						);
					} elseif ($row->places_type == 3) {
						$private[] = array(
							'url' => $row->places_landing,
							'nice' => $row->places_nicename,
							'name' => $row->places_name,
						);
					} else {
						$status[] = array(
							'url' => $row->places_landing,
							'nice' => $row->places_nicename,
							'name' => $row->places_name,
						);

					}
				} else {
					$registrasi[] = array(
						'url' => $row->places_landing,
						'nice' => $row->places_nicename,
						'name' => $row->places_name,
					);
				}
			}
		}

		$this->tpl['places_id'] = $id;
		$this->tpl['registrasi'] = $registrasi;
		$this->tpl['photo'] = $photo;
		$this->tpl['status'] = $status;
		$this->tpl['registrasi'] = $registrasi;
		$this->tpl['private'] = $private;
		$this->tpl['TContent'] = $this->load->view('events/next', $this->tpl, true);
		$this->load->view('events/home', $this->tpl);
	}

}
