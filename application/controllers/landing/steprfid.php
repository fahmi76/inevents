<?php

class steprfid extends CI_controller {

    protected $content;
    protected $perms;

    function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'wooz.in - ';
        $this->tpl['fb_page'] = 'http://www.facebook.com/pages/Woozin/157614240949967';
        $this->tpl['tw_page'] = 'wooz_in';

        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->model('convert');
        date_default_timezone_set('Asia/Jakarta');

        $this->load->helper('url');

        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');
        $url = $this->config->item('url');
        $loc = $this->input->get('loc', true);

        if (isset($loc) && $loc != '') {
            $this->tpl['loc'] = $loc;
            $this->loc = $loc;
        } else {
            $this->tpl['loc'] = 1;
            $this->loc = 1;
        }

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true
        ));

        $custom_page = $this->input->get('url', true);
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $this->tpl['card'] = 'Card';
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;

                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                }
                if ($customs->places_twitter != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $from = $this->input->get('from', true);
        $userdata = $this->input->get('acc', true);
        $url = $this->input->get('url', true);
        $likefollow = $this->input->get('likefollow', true);
        if ($from && $userdata && $url) {
            $data_user = $this->landing_m->gettable('id', $userdata, 'account');

            $this->tpl['page'] = "step3";
            $this->tpl['title'] = "Activate RFID";

            $this->load->helper(array('form'));
            $this->load->library('form_validation');

            $this->form_validation->set_rules('serial', 'RFID Serial', 'trim|callback_rfid_chk[' . $userdata . ']|required');
            
            if ($this->form_validation->run() === TRUE) {
                $data['account_rfid'] = $this->input->post('serial', true);
                
                $this->landing_m->landingdata($userdata, $this->custom_id, $data['account_rfid']);
                $this->db->where('id', $userdata);
                $this->db->update('account', $data);
                
                redirect('landing/step3?url=' . $this->custom_url . '&acc=' . $userdata . '&from=' . $from . '&places=' . $this->custom_id . '&likefollow=' . $likefollow);

                #redirect('thankyou?url='.$url.'&acc='.$userdata.'&from='.$from);
            } else {
                $this->tpl['likefollow'] = $likefollow;
                $this->tpl['id'] = $userdata;
                $this->tpl['datalist'] = $data_user;
                $this->tpl['from'] = $from;
                $this->tpl['url'] = $url;
                $this->tpl['TContent'] = $this->load->view('landingnew/activaterfid', $this->tpl, true);
                $this->load->view('landingnew/home', $this->tpl);
            }
        } else {
            redirect('?status=custom_page_not_registered');
        }
    }

    function rfid_chk($str, $account_id) {
        $str = strtolower($str);
        $chekrfid = $this->landing_m->check_rfid($str, $account_id);
        if (!$chekrfid) {
            $this->form_validation->set_message('rfid_chk', 'Serial already registered');
            return false;
        } else {
            return true;
        }
    }

    function cekrfid() {
        $this->tpl['url'] = $this->input->get('url', true);
        $this->tpl['page'] = "step3";
        $this->tpl['title'] = "Activate RFID";
        $this->tpl['error'] = 0;
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('serial', 'CIMB Niaga Gear', 'trim|numeric|required');

        if ($this->form_validation->run() === TRUE) {
            $rfid = $this->input->post('serial', true);
            $data_user = $this->landing_m->gettable('account_rfid', $rfid, 'account');
            if ($data_user) {
                $this->tpl['name'] = $data_user->account_displayname;
                $this->tpl['acc_id'] = $data_user->id;
                $this->tpl['error'] = 1;
            } else {
                $this->tpl['error'] = 2;
            }
        }
        $accid = $this->input->get('accid', true);
        if ($accid) {
            $data_user = $this->landing_m->gettable('id', $accid, 'account');
            $this->tpl['name'] = $data_user->account_displayname;
            $this->tpl['acc_id'] = $data_user->id;
            $this->tpl['error'] = 3;
        }
        $this->tpl['TContent'] = $this->load->view('landingnew/cekrfid', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

    function changerfid() {
        $this->tpl['error'] = 0;
        $url = $this->input->get('url', true);
        $this->tpl['url'] = $url;
        $final = $this->input->get('final', true);
        $this->tpl['final'] = $final;
        $this->tpl['page'] = "step3";
        $this->tpl['title'] = "Activate RFID";
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if ($final == 2) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required');

            if ($this->form_validation->run() === TRUE) {
                $rfid = $this->input->post('email', true);
                $data_user = $this->landing_m->gettable('account_email', $rfid, 'account');
                if ($data_user) {
                    redirect(current_url() . '?url=' . $url . '&places=' . $this->spot_id . '&id=' . $data_user->id . '&final=1', 'refresh');
                } else {
                    $this->tpl['error'] = 2;
                }
            }
        } else {
            $accid = $this->input->get('id');
            $data_user = $this->landing_m->gettable('id', $accid, 'account');
            $this->tpl['name'] = $data_user->account_displayname;
            $this->tpl['acc_id'] = $data_user->id;

            $this->form_validation->set_rules('serial', 'CIMB Niaga Gear', 'trim|numeric|callback_rfid_chk[' . $data_user->id . ']|required');

            if ($this->form_validation->run() === TRUE) {
                $rfid['account_rfid'] = $this->input->post('serial', true);
                $this->landing_m->landingdata($data_user->id, $this->custom_id, $rfid['account_rfid']);
                $this->db->where('id', $data_user->id);
                $this->db->update('account', $rfid);
                $this->tpl['error'] = 3;
            }
        }

        $this->tpl['TContent'] = $this->load->view('landingnew/changerfid', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

}

?>
