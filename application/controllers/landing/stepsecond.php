<?php

class stepsecond extends CI_controller {

    protected $content;
    protected $perms;

    function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'wooz.in - ';
        $this->tpl['fb_page'] = 'http://www.facebook.com/pages/Woozin/157614240949967';
        $this->tpl['tw_page'] = 'wooz_in';
        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->library('sosmed');
        $this->load->library('fungsi');
        $this->load->model('convert');
        $this->load->helper('url');

        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true
        ));

        $custom_page = $this->input->get('url', true);
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;

                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                }
                if ($customs->places_twitter != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $from = $this->input->get('from', TRUE);
        $url = $this->input->get('url', TRUE);
        $acc = $this->input->get('acc', TRUE);
        if ($from && $url && $acc) {
            $this->step2($from, $acc, $url);
        } elseif ($from == 'email') {
            $this->step2($from, $acc, $url);
        } else {
            redirect('?status=custom_page_not_registered');
        }
    }

    function step2($from, $acc, $url) {
        $this->tpl['from'] = $from;
        $this->tpl['page'] = "step2";
        $this->tpl['title'] = "Personal Information";
        $this->tpl['id'] = $acc;
        $this->tpl['likefb'] = 0;
        $this->tpl['followtw'] = 0;
        $user = $this->db->get_where('account', array('id' => $acc))->row();
        if ($user) {
            $this->tpl['token'] = $user->account_token;
        }
        $places = $this->db->get_where('places', array('places_landing' => $url))->row();
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required');
        #$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
        #$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_used_en[' . $acc . ']');
        #$this->form_validation->set_rules('tgl', 'Date', 'trim|required|numeric|callback_cek_data_tanggal_en');
        #$this->form_validation->set_rules('bln', 'Month', 'trim|callback_cek_data_dropdown_bulan_en');
        #$this->form_validation->set_rules('thn', 'Year', 'trim|required|numeric|callback_year_chk_en');
        $this->form_validation->set_rules('gender', 'Gender', 'trim');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|callback_email_used_en[' . $acc . ']');
        $this->form_validation->set_rules('tgl', 'Date', 'trim|numeric');
        $this->form_validation->set_rules('bln', 'Month', 'trim');
        $this->form_validation->set_rules('thn', 'Year', 'trim|numeric');
        
        if ($this->form_validation->run() === TRUE) {
            $getlikefollow = $this->input->get('likefollow', true);
            if ($getlikefollow != 0) {
                if ($from == 'fb') {
                    $likefollow = $this->sosmed->get_like_fb($places->places_fb_page_id, $user->account_fbid);
                }
                if ($from == 'tw') {
                    $likefollow = $this->sosmed->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_twitter);
                }
            } else {
                $likefollow = 0;
            }
            $data['account_displayname'] = $this->input->post('fullname', true);
            $data['account_username'] = $this->convert->nicename($this->input->post('fullname', true));
            $data['account_email'] = $this->input->post('email', true);
            $data['account_birthdate'] = $this->input->post('thn', true) . '-' . $this->input->post('bln', true) . '-' . $this->input->post('tgl', true);
            $data['account_gender'] = $this->input->post('gender', true);
            $data['account_status'] = 1;
            
            if ($acc == 0) {
                $this->db->insert('account', $data);
                $acc = $this->db->insert_id();
            } else {
                $this->db->where('id', $acc);
                $this->db->update('account', $data);
            }
            redirect('landing/steprfid?url=' . $this->custom_url . '&places=' . $this->custom_id . '&acc=' . $acc . '&from=' . $from . '&likefollow=' . $likefollow);
        }
        if ($from == 'fb') {
            if (!$user->account_token) {
                redirect('landing/home?url=' . $this->custom_url . '&places=' . $this->custom_id . '&retry=true&status=token_not_found');
            }
            $this->tpl['likefollow'] = $this->sosmed->get_like_fb($places->places_fb_page_id, $user->account_fbid);
        }
        if ($from == 'tw') {
            $this->tpl['likefollow'] = $this->sosmed->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_twitter);
        }

        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landingnew/signupnext', $this->tpl, true);

        $this->tpl['TContent'] = $this->load->view('landingnew/signupnext', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

    function loginstep2() {
        $acc = $this->input->get('acc', true);
        $from = $this->input->get('from', true);
        $row = $this->db->get_where('account', array('id' => $acc))->row();
        $places = $this->db->get_where('places', array('id' => $this->custom_id))->row();

        $photos['landing_fb_status'] = $this->sosmed->update_fb_status($places, $row->account_fb_name, $row->account_fb_id, $row->account_fb_token);

        $sql = "SELECT id FROM wooz_landing "
                . "WHERE account_id = '$acc' and places_id = '$this->custom_id' order by id desc";
        $hasil = $this->db->query($sql);
        $data = $hasil->row();

        $this->db->where('id', $data->id);
        $this->db->update('landing', $photos);

        redirect('landing/steprfid?url=' . $this->custom_url . '&places=' . $this->custom_id . '&acc=' . $acc . '&from=' . $from);
    }

    function send_email($email, $name) {
        $config['charset'] = 'iso-8859-1';
        $config['protocol'] = 'mail';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = FALSE;

        $message = "<h3>Hi " . $name . ",</h3>";
        $message .= "<p>Hi there! Thank you for registering to Wooz.in, now you are ready to tap and share to the world.</p>";
        $message .= "<p>Share what you think is cool, check into places, taking photo's and get 101 more excitement and also cool additional benefit in events, concerts and also various places.</p>";
        $message .= "<p>Be sure to update your new social media with us, and regularly check the new cool activities that we have.</p>";
        $message .= "<p>&nbsp;</p><p>- Wooz.in</p>";

        $this->load->library('email');

        $this->email->initialize($config);
        $this->email->from('donotreply@wooz.in', 'Wooz.In');
        $this->email->to($email);

        $this->email->subject("Wooz.in Account Activation");
        $this->email->message($message);
        $send = $this->email->send();
        return $send;
    }

    function send_email_fritz($email, $message, $subject_email) {
        $config['charset'] = 'iso-8859-1';
        $config['protocol'] = 'mail';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = FALSE;

        $this->load->library('email');

        $this->email->initialize($config);
        $this->email->from('donotreply@wooz.in', 'Fritz');
        $this->email->to($email);

        $this->email->subject($subject_email);
        $this->email->message($message);
        $send = $this->email->send();
        return $send;
    }

    function cekemail() {
        $this->tpl['from'] = $_GET['from'];
        $this->tpl['rfid'] = $_GET['rfid'];
        $userdata = $_GET['acc'];
        $this->tpl['user'] = $this->landing_m->gettable('id', $userdata, 'account');
        $cekemail = $this->landing_m->emailfound($userdata);
        $this->tpl['datalist'] = $cekemail;
        $this->tpl['TContent'] = $this->load->view('landingnew/signupemail', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

    function emailfound() {
        #echo 'die';die;
        #url=hq&from=tw&acc=2&rfid=2121&checkid=1
        $this->tpl['from'] = $_GET['from'];
        $this->tpl['rfid'] = $_GET['rfid'];
        $this->tpl['userdata'] = $_GET['acc'];
        $this->tpl['user'] = $this->landing_m->gettable('id', $_GET['checkid'], 'account');
        $cekemail = $this->landing_m->emailfound($_GET['checkid']);
        $this->tpl['datalist'] = $cekemail;
        $this->tpl['TContent'] = $this->load->view('landingnew/signupemail', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

    function mergeaccount() {
        $this->landing_m->mergeuser($_GET['old'], $_GET['acc'], $_GET['from'], $this->custom_id);
        if (isset($this->uri->segments[2]) && $this->uri->segments[2] != '') {
            #print_r('verify/1?from='.$_GET['from'].'&url='.$_GET['url'].'&acc='.$_GET['acc'].'&rfid='.$_GET['rfid']);die;
            redirect('verify/1?from=' . $_GET['from'] . '&url=' . $_GET['url'] . '&acc=' . $_GET['acc'] . '&rfid=' . $_GET['rfid']);
        } else {
            #print_r('verify?from='.$_GET['from'].'&url='.$_GET['url'].'&acc='.$_GET['acc'].'&rfid='.$_GET['rfid']);die;
            redirect('verify?from=' . $_GET['from'] . '&url=' . $_GET['url'] . '&acc=' . $_GET['acc'] . '&rfid=' . $_GET['rfid']);
        }
        #redirect('cekemail?url='.$_GET['url'].'&from='.$_GET['from'].'&acc='.$_GET['old'].'&rfid='.$_GET['rfid']); 
    }

    function rfid_chk($str) {
        $str = strtolower($str);
        if ($str != "") {
            $row = $this->db->get_where('account', array('account_rfid' => $str))->row();
            if (count($row) != 0) {
                $this->form_validation->set_message('rfid_chk', 'Serial already registered');
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    function cek_data_dropdown_profession($str) {
        if ($str != '') {
            return true;
        } else {
            $this->form_validation->set_message('cek_data_dropdown_profession', 'The %s field is required.');
            return false;
        }
    }

    function cek_data_dropdown_favsport($str) {
        if ($str != '') {
            return true;
        } else {
            $this->form_validation->set_message('cek_data_dropdown_favsport', 'The %s field is required.');
            return false;
        }
    }

    function cek_profession() {
        $job = $this->input->post('Profession', true);
        if ($job == 'Others') {
            $jobother = $this->input->post('Professionother', true);
            if ($jobother) {
                return true;
            } else {
                $this->form_validation->set_message('cek_profession', 'The Other Profession field is required');
                return false;
            }
        } else {
            return true;
        }
    }

    function cek_favsport() {
        $job = $this->input->post('favsport', true);
        if ($job == 'Others') {
            $jobother = $this->input->post('favsportother', true);
            if ($jobother) {
                return true;
            } else {
                $this->form_validation->set_message('cek_favsport', 'The Other Favorite Sport field is required');
                return false;
            }
        } else {
            return true;
        }
    }

    function year_chk_en($str) {
        $yearnow = date('Y');
        $syarat = $yearnow - 13;
        $syarat1 = $yearnow - 83;
        if (($str >= $syarat1) && ($str <= $syarat)) {
            return true;
        } else {
            $this->form_validation->set_message('year_chk_en', 'Year of birth does not qualify');
            return false;
        }
    }

    function username_chk($str) {
        $userdata = $this->session->userdata('aidi');

        $str = strtolower($str);
        $row = $this->db->get_where('account', array('account_username' => $str))->row();
        if (count($row) != 0 && (isset($userdata) && $row->id != $userdata)) {
            $this->form_validation->set_message('username_chk', 'Username already used');
            return false;
        } else {
            return true;
        }
    }

    function email_available() {
        $str = $this->input->post('mob_email');
        $new = $this->db->get_where('account', array('account_email' => $str))->row();
        if (count($new) == 0) {
            $this->form_validation->set_message('email_available', 'Email Tidak Ditemukan');
            return false;
        } else {
            return true;
        }
    }
    
    function email_used_en($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('email');
        }
        $new = $this->db->get_where('account', array('account_email' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('account', array('account_email' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('email_used_en', 'Registered email, please replace Email');
                return false;
            }
        } else {
            return true;
        }
    }

    function telp_used_en($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('telp');
        }
        $valid = preg_match('/^([0-9]{5,14})$/', $str);
        if (!$valid) {
            $this->form_validation->set_message('telp_used_en', 'Format Phone Number is not appropriate, please change the Phone Number');
            return false;
        }
        $new = $this->db->get_where('account', array('account_phone' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('account', array('account_phone' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('telp_used_en', 'Registered Telephone Number, Please replace the Phone Number');
                return false;
            }
        } else {
            return true;
        }
    }

    function follow_used($str, $account_id) {
        $param = preg_split('/,/', $account_id);
        $id = $param[0];
        $from = $param[1];
        $fb = $param[2];
        $tw = $param[3];
        if ($str == 0) {
            $user = $this->db->get_where('account', array('id' => $id))->row();
            if ($from == 'fb') {
                $like = $this->sosmed->get_like_fb($fb, $user->account_fbid);
                if ($like == 0) {
                    $this->form_validation->set_message('follow_used', 'Like Facebook fan page dulu, Silahkan Like Facebook fan page');
                    return false;
                } else {
                    return true;
                }
            }
            if ($from == 'tw') {
                $follow = $this->sosmed->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $tw);

                if ($follow == 0) {
                    $this->form_validation->set_message('follow_used', 'Follow Twitter page dulu, Silahkan Follow Twitter page');
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return true;
        }
    }

    function cek_data_dropdown_bulan_en($str) {
        if ($str != 0) {
            return true;
        } else {
            $this->form_validation->set_message('cek_data_dropdown_bulan_en', 'The %s field is required');
            return false;
        }
    }

    function cek_data_tanggal_en($str) {
        $day = $this->input->post('tgl');
        $month = $this->input->post('bln');
        $year = $this->input->post('thn');

        if (checkdate($month, $day, $year)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('cek_data_tanggal_en', 'Enter the correct date of birth.');
            return FALSE;
        }
    }

    function __nicename($file) {
        $healthy = array(" ", ".", ",", "?", "&", "(", ")", "#", "!", "^", "%", "\'", "@", "}", "{", "]", "[", "|", "", "'", "/", "|", ":", '"', "<", ">", ";", "--");
        $yummy = array("-", "",);
        $result = strtolower(str_replace($healthy, $yummy, trim($file)));

        $this->db->select('');
        $this->db->where('account_username', $result);
        $this->db->from('account');
        $c = $this->db->count_all_results();

        if ($c) {
            $this->db->select('');
            $this->db->like('account_username', $result, 'after');
            $this->db->from('account');
            $c = $this->db->count_all_results();

            if ($c) {
                $this->db->select('');
                $this->db->like('account_username', $result, 'after');
                $this->db->order_by('id', 'desc');
                $this->db->limit(1, 0);
                $res = $this->db->get('account')->row();

                $slice = explode('-', $res->account_username);
                $cc = count($slice);
                if (is_numeric($slice[$cc - 1])) {
                    $slice[$cc - 1] = $slice[$cc - 1] + 1;
                    $jadi = implode('-', $slice);
                } else {
                    $jadi = $result . '-1';
                }
            } else {
                $jadi = $result . '-1';
            }
        } else {
            $jadi = $result;
        }
        return $jadi;
    }

    function __tanggal($tanggal_f) {
        if (strstr($tanggal_f, ' ')) {
            $waktu_f = explode(' ', $tanggal_f);
            $tanggal_asli_f = explode('-', $waktu_f[0]);
            $waktu_asli_f = explode(':', $waktu_f[1]);
            $tanggal_cetak_f = date('M d, Y', mktime($waktu_asli_f[0], $waktu_asli_f[1], $waktu_asli_f[2], $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        } else {
            $tanggal_asli_f = explode('-', $tanggal_f);
            $tanggal_cetak_f = date('M d, Y', mktime(0, 0, 0, $tanggal_asli_f[1], $tanggal_asli_f[2], $tanggal_asli_f[0]));
        }
        return $tanggal_cetak_f;
    }

}

?>
