<?php

class landing extends CI_controller {

    protected $content;
    protected $perms;

    function __construct() {
        parent::__construct();

        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'Wooz.in - ';
        $this->tpl['fb_page'] = 'http://www.facebook.com/pages/Woozin/157614240949967';
        $this->tpl['tw_page'] = 'wooz_in';

        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->model('convert');
        date_default_timezone_set('Asia/Jakarta');

        $this->load->helper('url');
        $this->load->library('sosmed');

        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true
        ));

        $custom_page = $this->input->get('url', true);
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;

                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                }
                if ($customs->places_twitter != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }

    function index() {
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Home";
        $this->tpl['data'] = $this->landing_m->urllanding();

        $this->tpl['TContent'] = $this->load->view('landingnew/awal', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

    function home() {
        header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Home";
        $userdata = $this->session->userdata('aidi');

        $this->tpl['user'] = $userdata;
        $retry = $this->input->get('retry', true);
        if ($retry) {
            $status = $this->input->get('status', true);
            if ($status) {
                $this->tpl['status'] = $status;
            }
            $this->tpl['retry'] = true;
        } else {
            $this->tpl['retry'] = false;
        }

        $this->tpl['TContent'] = $this->load->view('landingnew/default', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

    function logout() {
        $this->session->unset_userdata('aidi');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('session_id');
        $this->session->unset_userdata('ip_address');
        $this->session->unset_userdata('user_agent');
        $this->session->unset_userdata('last_activity');
        $this->session->unset_userdata('nice');
        $this->session->unset_userdata('grup');
        $this->session->unset_userdata('access');
        $this->session->unset_userdata('kode');

        $this->facebook->destroySession();
        #$this->_killFacebookCookies();
        $this->session->sess_destroy();
        session_destroy();
        unset($_SESSION);
        $past = time() - 3600;
        foreach ($_COOKIE as $key => $value) {
            setcookie($key, $value, $past, '/');
        }
        redirect('landing/home?url=' . $_GET['url'] . '&places=' . $_GET['places']);
    }

    public function _killFacebookCookies() {
        // get your api key 
        $apiKey = $this->config->item('facebook_application_id');
        // get name of the cookie 

        $cookies = array('user', 'session_key', 'expires', 'ss');
        foreach ($cookies as $name) {
            setcookie($apiKey . '_' . $name, false, time() - 3600);
            unset($_COOKIE[$apiKey . '_' . $name]);
        }

        setcookie($apiKey, false, time() - 3600);
        unset($_COOKIE[$apiKey]);
    }

    function accountfound() {
        $this->tpl['page'] = "step3";
        $this->tpl['title'] = "Find different account";
        $this->tpl['likefollow'] = $this->input->get('likefollow', true);

        $this->tpl['from'] = $this->uri->segment(3, 0);
        $this->tpl['url'] = $this->input->get('url', true);
        $oldid = $this->uri->segment(4, 0);
        $newid = $this->uri->segment(5, 0);

        $this->tpl['user'] = $this->landing_m->gettable('id', $newid, 'account');
        $this->tpl['datalist'] = $this->landing_m->gettable('id', $oldid, 'account');

        $this->tpl['TContent'] = $this->load->view('landingnew/foundaccount', $this->tpl, true);
        $this->load->view('landingnew/home', $this->tpl);
    }

    function follow() {
        $userdata = $this->input->post('id');
        $data_user = $this->landing_m->gettable('id', $userdata, 'account');

        if ($data_user->account_tw_token != '' && $data_user->account_tw_secret != '') {
            $follow = $this->sosmed->post_follow_tw($data_user->account_tw_token, $data_user->account_tw_secret, $this->twiiter_id_fan);

            if ($follow == 1) {
                $data1['nama'] = "yes";
            } else {
                $data1['nama'] = "no";
            }
            echo json_encode($data1);
        }
    }

}

?>
