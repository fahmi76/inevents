<?php

class testapi extends CI_controller {

    protected $tpl;
	protected $album_name = "Test album";
    
    function testapi() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        $this->load->library('curl');
        $this->load->library('fungsi');
        //$this->load->library('model');
    }    

    function event(){
        #$sql = "SELECT id,places_name,places_address,places_startdate FROM `wooz_places` where places_parent = 0";
        $sql = "SELECT id,places_name,places_address,places_startdate FROM `wooz_places` where id in (9)";
        $places = $this->db->query($sql)->result();
        $placesa[] = (object)array(
                    'id' => 1,
                    'places_name' => 'Conference',
                    'places_address' => 'Trinidad & Tobago',
                    'places_startdate' => '2015-07-08'
                );
        echo json_encode($placesa);
    }

    function userevent($id){
        $datauser = array();
        $sql = "SELECT b.id,b.account_displayname,b.account_email,b.account_gender,b.account_birthdate,b.account_phone,"
            ."concat('http://www.facebook.com/',b.account_fbid) as fb,account_tw_username,"
            ."b.account_tw_token,a.landing_joindate FROM `wooz_landing` a inner join wooz_account b on b.id = a.account_id"
            ." where a.landing_register_form = '".$id."' group by a.account_id";
        
        $user = $this->db->query($sql)->result();
        foreach($user as $row){
            $datauser[] = (object)array(
                    'name' => $row->account_displayname,
                    'company' => $this->formdata($row->id,7),
                    'invited' => $this->formdata($row->id,8),
                    'datalog' => $this->cek_log_status($id,$row->id),
                    #'dataphoto' => $this->cek_photo_status($id,$row->id),
                    'joindate' => $row->landing_joindate
                );
        }
        echo json_encode($datauser);
    }

    function formdata($account_id,$data){
        $sql = "SELECT content FROM `wooz_form_regis_detail` where places_id = 10 and form_regis_id = '".$data."' and account_id = '".$account_id."'";
        $user = $this->db->query($sql)->row();
        if($user){
            $data = $user->content;
        }else{
            $data = '';
        }
        return $data;    
    }

    function formregis($id){
        $sql = "SELECT * FROM `wooz_form_regis` where places_id = '".$id."'";
        $user = $this->db->query($sql)->result();
        xdebug($user);die;
    }

    function places_parent($id){
        $data = '';
        $sql = "SELECT id FROM `wooz_places` where places_parent = '".$id."' or id = '".$id."'";
        $user = $this->db->query($sql)->result();
        if($user){
            $data = '(';
            $x = 1;
            foreach($user as $row){
                $data .= $row->id;
                if($x < count($user)){
                    $data .= ',';
                }else{
                    $data .= ')';
                }
                $x++;
            }
        }
        return $data;
    }

    function cek_log_status($id,$account_id){
        $fb = 0;
        $tw = 0;
        $dataplaces = $this->places_parent($id);
        $sql = "SELECT id FROM `wooz_log_user_gate` where log_type != 6 and account_id = '".$account_id."'";
        $user = $this->db->query($sql)->result();

        $data_log = array(
                    'total' => count($user),
                    'total_fb' => $fb,
                    'total_tw' => $tw
                );
        return $data_log;
    }

    function cek_photo_status($id,$account_id){
        $fb = 0;
        $tw = 0;
        $dataplaces = $this->places_parent($id);
        $sql = "SELECT id,pid,twit_id FROM `wooz_photos` where account_id = '".$account_id."'";
        $user = $this->db->query($sql)->result();
        foreach($user as $row){
            if($row->pid != 0){
                $fb++;
            }
            if($row->twit_id != 0){
                $tw++;
            }
        }
        $data_photo = array(
                    'total' => count($user),
                    'total_fb' => $fb,
                    'total_tw' => $tw
                );
        return $data_photo;
    }

    function cekusernametw(){
        $sql = "SELECT id,account_displayname,account_tw_token FROM `wooz_account` where account_tw_username = '' and account_tw_token != '' limit 20";
        $user = $this->db->query($sql)->result();
        foreach ($user as $row) {
            echo $row->account_displayname;
            $twid = explode('-', $row->account_tw_token);
            $tw_name = $this->usernametw($twid[0]);
            xdebug($tw_name);
        }
        xdebug($user);
    }

    function usernametw($username){
        $tmhOAuth = new tmhOAuth(array(
                            'consumer_key' => $this->config->item('twiiter_consumer_key'),
                            'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
                            'user_token' => '239420882-98K65XXAkuvKrmm41E5P31OBXTJruyytjuikH5no',
                            'user_secret' => 'bYCiW583WrDUGXMw4pavxyrSilhmepU0EYCbwRw',
                        ));
        $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/users/show'), array('user_id' => $username));

        if ($code == 200) {
            $response = $tmhOAuth->response['response'];
            $hasil = json_decode($response);
            $username = $hasil->screen_name;
            xdebug($username);
        }else{
            $response = $tmhOAuth->response['response'];
            xdebug($response);

        }
        #xdebug($code);
        #xdebug($username);die;
        #return $username;
    }

    function getlikefb($id){
        $data = array(
                'name' => array(),
                'category' => array()
            );
        $sql = "SELECT facebook_page_name,category_name "
                ."FROM wooz_user_likes a inner join wooz_like b on b.id = a.likes_id "
                ."inner join wooz_like_category c on c.id = b.category_id "
                ."where a.account_id = '".$id."'";
        $user = $this->db->query($sql)->result();
        foreach($user as $row){
            $data['name'][] = $row->facebook_page_name;
            $data['category'][] = $row->category_name;
        }
        return json_encode($data);
    }

    function mandrill(){
        $this->load->model('landing_m');
       // $places = $this->landing_m->emailplaces(21);

        $sql = "select * from wooz_gate_email where gate_id = '23' order by id asc";
        $query = $this->db->query($sql);
        $places = $query->row();

        if($places){
            $sender = 'inevents';
            $subject = 'Registration';
            $content = 'inevents';
            $attachment = '';
            if($places->nama){
                $sender = $places->nama;
            }
            if($places->subject){
                $subject = $places->subject;
            }
            if($places->content){
                $content = $places->content;
            }
            if($places->attachment){
                $attachment = $places->attachment;
            }
        }
        $hasil_email = 0;
        $this->load->config('mandrill');
        $this->load->library('mandrill');
        $mandrill_ready = NULL;
        
        try {
            $this->mandrill->init( $this->config->item('mandrill_api_key') );
            $mandrill_ready = TRUE;
        } catch(Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        
        if( $mandrill_ready ) {
            //Send us some email!
            if($attachment != ''){
                $image = FCPATH.$attachment;
                $attch = $this->mandrill->getAttachmentStruct($image);  
                //Send us some email!
                $email = array(
                    'text' => $content, //Consider using a view file
                    'subject' => $subject,
                    'from_email' => 'donotreply@wooz.in',
                    'from_name' => $sender,
                    'to' => array(array('email' => 'hayria76@yahoo.com')),
                    "attachments" => array( $attch  )
                );  
            }else{
                $email = array(
                    'text' => $content, //Consider using a view file
                    'subject' => $subject,
                    'from_email' => 'donotreply@wooz.in',
                    'from_name' => $sender,
                    'to' => array(array('email' => 'hayria76@yahoo.com')) 
                );
            }

            $result = $this->mandrill->messages_send($email);
            //$hasil_email = $result[0]['_id'];
            $hasil_email = $result;
        }
        xdebug($result);
    }

    function ambil_data_user($acc_id,$form_id){
        $sql = "select a.account_id,a.content "
                ."from wooz_form_regis_detail a where a.form_regis_id = '".$form_id."' and account_id = '" . $acc_id . "'";
        $query = $this->db->query($sql);
        $data = $query->row();
        if($data){
            $hasil = $data->content;
        }else{
            $hasil = '';
        }
        return $hasil;
    }
}

?>
