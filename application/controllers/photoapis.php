<?php

class photoapis extends CI_controller {

    protected $tpl;
    
    function photoapis() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
		$this->load->model('photobooth_m');
        $this->load->library('curl');
        $this->load->library('fungsi');
        $this->load->library('sosmed');
        date_default_timezone_set('America/Port_of_Spain');
    }    
	
	function main($placesid){
		$cserial = $this->input->get('no_rfid',true);
		if(!$cserial){
			$cserial = $this->input->post('no_rfid',true);
		}
        if (isset($cserial) && $cserial !='') {
            $venue = $this->photobooth_m->getplaceslanding($placesid);
			if($venue){
				$user = $this->cekuser($cserial,$venue->places_parent);
				if ($user) {
                    $data = array(
                        'status' => 1,
                        'id' => $user->id,
                        'rfid' => $cserial,
                        'places_id' => $placesid,
                        'name' => $user->account_displayname
                    );
                    echo json_encode($data);
                } else { //belum registrasi
                    $data = array(
                        'status' => 0,
                        'name' => 'not registered'
                    );
                    echo json_encode($data);
                }
			}else{ //belum registrasi
                $data = array(
						'status' => 0,
                        'name' => 'venue ended'
                    );
                echo json_encode($data);
            }
		}else{ //rfid tidak ada
			$data = array(
					'status' => 0,
                    'name' => 'no rfid'
            );
            echo json_encode($data);		
		}
	
	}
	
    function cekadmin() {
        $username = $this->input->get('username', true);
        if (!$username) {
            $username = $this->input->post('username', true);
        }
        $password = $this->input->get('pass', true);
        if (!$password) {
            $password = $this->input->post('pass', true);
        }
        if ($password && $username) {
            if ($username == 'admin' && $password == 'admin') {
                $data = array(
                    'status' => 1,
                    'name' => 'sukses'
                );
            } else {
                $data = array(
                    'status' => 0,
                    'name' => 'no'
                );
            }
        } else {
            $data = array(
                'status' => 2,
                'name' => 'no'
            );
        }
        echo json_encode($data);
    }
	
	function cekprint(){	
		$dataasli['post'] = json_encode($_POST);
		$dataasli['data_status'] = 900;
		$this->db->insert('data_post_ford', $dataasli);
        $dataasli_id = $this->db->insert_id();
        $nama_file = $this->input->post('photo_path', true);
        $no_rfid = $this->input->post('no_rfid', true);
        $acc_id = $this->input->post('acc_id', true);
        $places_id = $this->input->post('places_id', true);
        if ($nama_file && $no_rfid && $acc_id && $places_id) {
			$dataprint = array(
							"photo_upload" => $nama_file,
							"account_id" => $acc_id,
							"places_id" => $places_id,
							"photos_id" => 1
						);
			$this->db->insert('photos_print', $dataprint);
            $data = array(
                    'status' => 1,
                    'name' => 'yes'
                );
        } else {
            $data = array(
                'status' => 0,
                'name' => 'no'
            );
        }
        echo json_encode($data);
	}
	
	function testtagphoto(){
        $nama_file = "12345_151209_225643_289.jpg";
        $no_rfid = "3456";
        $acc_id = "1109";
        $places_id = "7";
        $xpos = "212";
        $ypos = "187";
        if ($nama_file && $no_rfid && $acc_id && $places_id  && $xpos && $ypos) {
			$x = ((75 + $xpos) / 640) * 100;
            $y = ((75 + $ypos) / 480) * 100;
			
            $get_photos = $this->photobooth_m->getphototag($nama_file);
			$account_tag = $this->photobooth_m->getphotoaccounttag($acc_id);
			$num = $this->db->get_where('tagging', array('id_photos' => $get_photos->id))->num_rows();
			
			$tag_params = array(
                'access_token' => $get_photos->account_token,
                'to' => $account_tag->account_fbid,
                'x' => $x,
                'y' => $y
            );
            $tagphoto = $this->sosmed->photo_tag_fb($get_photos->pid, $tag_params);
            
			$input = array(
                    'id_photos' => $get_photos->id,
                    'targetX' => $xpos,
                    'targetY' => $ypos,
                    'tagCounter' => $num + 1,
                    'account_id' => $acc_id,
                    'status' => $tagphoto
                );
			$this->db->insert('tagging', $input);
            $data = array(
                    'status' => 1,
                    'name' => 'yes'
                );
        } else {
            $data = array(
                'status' => 0,
                'name' => 'no'
            );
        }
        echo json_encode($data);
	}

	function tagphoto(){	
		$dataasli['post'] = json_encode($_POST);
		$dataasli['data_status'] = 0;
		$this->db->insert('data_post_ford', $dataasli);
        $dataasli_id = $this->db->insert_id();
        $nama_file = $this->input->post('photo_path', true);
        $no_rfid = $this->input->post('no_rfid', true);
        $acc_id = $this->input->post('acc_id', true);
        $places_id = $this->input->post('places_id', true);
        $xpos = $this->input->post('xpos', true);
        $ypos = $this->input->post('ypos', true);
        if ($nama_file && $no_rfid && $acc_id && $places_id  && $xpos && $ypos) {
			$x = ((75 + $xpos) / 640) * 100;
            $y = ((75 + $ypos) / 480) * 100;
			
            $get_photos = $this->photobooth_m->getphototag($nama_file);
			$account_tag = $this->photobooth_m->getphotoaccounttag($acc_id);
			$num = $this->db->get_where('tagging', array('id_photos' => $get_photos->id))->num_rows();
            
			$tag_params = array(
                'access_token' => $get_photos->account_token,
                'to' => $account_tag->account_fbid,
                'x' => $x,
                'y' => $y
            );
            $tagphoto = $this->sosmed->photo_tag_fb($get_photos->pid, $tag_params);
            
			$input = array(
                    'id_photos' => $get_photos->id,
                    'targetX' => $xpos,
                    'targetY' => $ypos,
                    'tagCounter' => $num + 1,
                    'account_id' => $acc_id,
                    'status' => $tagphoto,
                    'time_upload' => date('Y-m-d H:i:s')
                );
			$this->db->insert('tagging', $input);
            $data = array(
                    'status' => 1,
                    'name' => 'yes'
                );
        } else {
            $data = array(
                'status' => 0,
                'name' => 'no'
            );
        }
        echo json_encode($data);
	}
	
	function upload(){
		$upload_directory='/home/inevents/public_html/uploads/photodesktop/';
		
		$dataasli['post'] = json_encode($_POST);
		$dataasli['data_status'] = 0;
		$this->db->insert('data_post_ford', $dataasli);
		
		if (isset($_POST['upload'])) {  
			if (!empty($_FILES['photo_path'])) { 
					//check for image submitted
				if ($_FILES['photo_path']['error'] > 0) { 
					// check for error re file
					$data = array(
						'status' => 0,
						'message' => 'no',
						'name' => 'file photo error'
					);
					echo json_encode($data);
				} else {
					$no_rfid = $this->input->post('no_rfid', true);
					$acc_id = $this->input->post('acc_id', true);
					$places_id = $this->input->post('places_id', true);
					//move temp file to our server   
					move_uploaded_file($_FILES['photo_path']['tmp_name'], 
					$upload_directory . $_FILES['photo_path']['name']);
					$venue = $this->db->get_where('places', array('id' => $places_id))->row(); 
					$user = $this->cekuser($no_rfid,$venue->places_parent);
					$data = array(
						'status' => 1,
						'message' => 'yes',
						'name' => $user->account_displayname
					);
					echo json_encode($data);
					$upload = $this->uploads($no_rfid,$acc_id,$places_id,$_FILES['photo_path']['name']);					
				}
			} else {
					$data = array(
						'status' => 0,
						'message' => 'no',
						'name' => 'file photo not found'
					);
					echo json_encode($data);
			}
		} else {
			$data = array(
					'status' => 0,
					'message' => 'no',
					'name' => 'Upload not found'
				);
			echo json_encode($data);
		}		
	}

	function uploads($no_rfid,$acc_id,$places_id,$file_name){
        $venue = $this->db->get_where('places', array('id' => $places_id))->row(); 
        $user = $this->cekuser($no_rfid,$venue->places_parent);
		#$this->mergephoto($file_name,$venue->places_frame_url);
		if ($user) {	
			$image = '/home/inevents/public_html/uploads/photodesktop/' . $file_name;
            $fb_pid = 0;
            $twit_id = 0;
			$fanpage_fb = 0;	
            
			if($user->account_tw_token && $user->account_tw_secret){
				$texttw = $venue->places_tw_caption;
				$twit_id = $this->sosmed->upload_photo_tw($texttw, $image, $user->account_tw_token, 
							$user->account_tw_secret);
			}
			
            if ($user->account_fbid && $user->account_token) {
                $url = array('url' => base_url() . 'photoapis/uploads');
                $this->load->library('facebook_v4/facebooks_v4', $url);
                $album = 0;
                $album = $this->facebooks_v4->get_album($venue, $image, $user->account_fb_name, $user->account_fbid, $user->account_token);
                $fb_pid = $this->facebooks_v4->post_photo($venue, $image, $user->account_fb_name, $user->account_fbid, $user->account_token,$album);
            }   

            $album_page_id = '1080915591978949';
            $fanpage_id = '138136172923567';
            $fanpage_token = 'EAACGFg9ewNIBANkO5Nd4HCvZAErsD1UNQXCISguJXMZAu6mhWiaxjdNM9Rclqd4W0xfuKDPiGJHOF23UJ2Trf5ZC7Kf1WufRjcBq3Azpbw4AHAZCyHHsXDvnBrCUVn7HyfYPbPTZBtY9b5o0jreJIWp2sZAWX8C7a3HRsJJJiZCEQZDZD';
            $fanpage_fb =  $this->facebooks_v4->post_photo_fanpage($venue, $image, '', $fanpage_id, $fanpage_token,$album_page_id);
            
			$input = array(
				'account_id' => $user->id,
                'places_id' => $places_id,
                'photo_upload' => addslashes($file_name),
				'pid' => $fb_pid,
				'twit_id' => $twit_id,
				'email_id' => $fanpage_fb,
                'code_photo' => 'p' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id,
                'time_upload' => date("Y-m-d H:i:s"),
                'status' => 1
            );
			$photos = $this->photobooth_m->insert($input,'photos');
			$this->photobooth_m->insertlog($photos);
			
            return true;                 
        }else{
            return false;
        }
	}
	
	function cekuser($no_rfid,$placesid){
        $sqluser = "select a.id,a.account_displayname,a.account_fbid,a.account_fb_name,a.account_token,a.account_tw_token,a.account_tw_secret FROM wooz_account a inner join wooz_landing b on b.account_id = a.id
                    where account_rfid = '" . $no_rfid."' and landing_register_form = '".$placesid."'";
        $queryuser = $this->db->query($sqluser);
        $user = $queryuser->row();
        if ($user) {
			return $user;
        } else { 
			return false;
		}
	}
	
}