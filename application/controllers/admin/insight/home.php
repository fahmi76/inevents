<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/insight_m');
        $this->load->helper('form');
        
        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id,$this->user_name,$this->admin_logged);
        if($cek_admin){
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        }else{
            redirect('dashboard/login');
        }
    }
    
    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['preregis'] = $this->insight_m->get_pre(); 
        $data['arrive_event'] = $this->insight_m->arrive_event();
        $gate = $this->insight_m->list_gate();
        foreach ($gate as $row) {
            $data['checkin'][] = array(
                                    'id' => $row->id,
                                    'nama' => $row->nama,
                                    'total' => $this->insight_m->logcheckinstaff($row->id)
                                );
            $data['checkout'][] = array(
                                    'id' => $row->id,
                                    'nama' => $row->nama,
                                    'total' => $this->insight_m->logcheckoutstaff($row->id)
                                );
            $data['grafikcheckout'][$row->nama] = $this->hourchartactivity($row->id,2,1);
            $data['grafikcheckin'][$row->nama] = $this->hourchartactivity($row->id,1,1);
        }
		
        $data['content'] = $this->load->view('dashboard/insight/main',$data, true);
        $this->load->view('dashboard/main',$data);
    }

	function checkin($gate,$check,$status){
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = $this->insight_m->get_gate($gate);
        $data['status'] = $status;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->insight_m->list_check($gate,$check,$status);
        
        $data['content'] = $this->load->view('dashboard/gate/list',$data, true);
        $this->load->view('dashboard/main',$data);
	}
	
	function arrive(){
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['check'] = '';
        $data['status'] = 3;
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->insight_m->list_arrive();
        
        $data['content'] = $this->load->view('dashboard/gate/list',$data, true);
        $this->load->view('dashboard/main',$data);
	}
	
	function download($id){
		if($id == 1){
			$file = 'check-in-staff.png';
			$post_file = $_POST['img_val'];
		}elseif($id == 2){
			$file = 'check-out-staff.png';
			$post_file = $_POST['img_val1'];
		}elseif($id == 3){
			$file = 'check-in-backstage.png';
			$post_file = $_POST['img_val2'];
		}else{
			$file = 'check-out-backstage.png';
			$post_file = $_POST['img_val3'];
		}
		$dir = '/home/socialite/public_html/uploads/grafik/'.$file;
		//Get the base-64 stgring from data
		$filteredData=substr($post_file, strpos($post_file, ",")+1);

		//Decode the string
		$unencodedData=base64_decode($filteredData);

		//Save the image
		file_put_contents($dir, $unencodedData);

		header('Pragma: public');
		header('Cache-Control: public, no-cache');
		header('Content-Type: application/octet-stream');
		header('Content-Length: ' . filesize($dir));
		header('Content-Disposition: attachment; filename="' . basename($dir) . '"');
		header('Content-Transfer-Encoding: binary');

		readfile($dir);
	}
	
    function hourchartactivity($gate,$check,$status){
        $actvhour = $this->insight_m->check_hour($gate,$check,$status);
        $hours = array();
        foreach ($actvhour as $row){
			if($row->thedate == 0){
				$row->thedate = 24;
			}
            #$hours[]['date']=$row->thedate;
            #$hours[]['total']=$row->total;
            $hours[] = array(
						'date' => $row->thedate,
						'total' => $row->total
					);
        }      
		sort($hours);
        return $hours;
    }
}