<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class checkin extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/insight_m');
        $this->load->helper('form');
        
        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id,$this->user_name,$this->admin_logged);
        if($cek_admin){
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        }else{
            redirect('dashboard/login');
        }
    }
    
    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->insight_m->list_checkin();
        
        $data['content'] = $this->load->view('dashboard/insight/checkin',$data, true);
        $this->load->view('dashboard/main',$data);
    }

}