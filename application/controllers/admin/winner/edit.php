<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class edit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/winner_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function main($id) {
        $data['datauser'] = $this->winner_m->getuser($id);
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('id', 'Name', 'trim|required');
        $this->form_validation->set_rules('gate', 'Gate', 'required');

        if ($this->form_validation->run() === TRUE) {
            $in_data['account_id'] = $this->input->post('id', true);
            $in_data['winner_id'] = $this->input->post('gate', true);

            $this->db->where('id', $id);
            $this->db->update('winner_account', $in_data);

            $data['content'] = $this->load->view('dashboard/winner/success', $data, true);
        } else {
            $data['gate'] = $this->winner_m->list_gate();
            $data['content'] = $this->load->view('dashboard/winner/edit', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function visitor_used($str,$id) {
        if (!isset($str)) {
            $str = $this->input->post('name');
        }
        $new = $this->db->get_where('visitor', array('nama' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('visitor', array('nama' => $str, 'id' => $id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('visitor_used', 'Visitor already registered');
                return false;
            }
        } else {
            return true;
        }
    }
}
