<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class add extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function index() {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name Gate', 'trim|required|callback_visitor_used');

        if ($this->form_validation->run() === TRUE) {
            $in_data['nama'] = $this->input->post('name', true);
            $in_data['data_status'] = 1;
            $in_data['date_add'] = date('Y-m-d H:i:s');

            $this->db->insert('gate', $in_data);
            $data['content'] = $this->load->view('dashboard/gate/success', $data, true);
        } else {
            $data['content'] = $this->load->view('dashboard/gate/add', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function visitor_used($str) {
        $str = strtolower($str);
        if ($str != "") {
            $row = $this->db->get_where('gate', array('nama' => $str))->row();
            if (count($row) != 0) {
                $this->form_validation->set_message('visitor_used', 'Visitor already added');
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
