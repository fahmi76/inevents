<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/user_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    public function index() {
        $data['page'] = 'Home';
        $data['title'] = 'Dashboard';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['data'] = $this->user_m->list_user();
        
        $data['content'] = $this->load->view('dashboard/user/main',$data, true);
        $this->load->view('dashboard/main',$data);
    }

    function show($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        $data['data'] = $this->user_m->getuser($id);

        $data['content'] = $this->load->view('dashboard/user/show', $data, true);
        $this->load->view('dashboard/main', $data);
    }

    function delete($id = 0) {
        if ($id == 0) {
            redirect('dashboard');
        }
        $datauser = $this->user_m->getuser($id);

        $where['id'] = $id;
        $this->db->delete("account", $where);
        $wheres['account_id'] = $id;
        $this->db->delete("seat_account", $wheres);
        
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/delete', $data, true);
        $this->load->view('dashboard/main', $data);
    }

    function deleteall(){
        $query = $this->db->query("TRUNCATE TABLE wooz_winner_account");
        $query = $this->db->query("TRUNCATE TABLE wooz_account");
        $query = $this->db->query("TRUNCATE TABLE wooz_seat_account");

        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;
        
        $data['content'] = $this->load->view('dashboard/user/delete', $data, true);
        $this->load->view('dashboard/main', $data);

    }

}
