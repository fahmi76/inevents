<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class add extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dashboard/main_m');
        $this->load->model('dashboard/user_m');
        $this->load->model('dashboard/visitor_m');
        $this->load->helper('form');

        $this->user_id = $this->session->userdata('user_id');
        $this->user_name = $this->session->userdata('user_name');
        $this->admin_logged = $this->session->userdata('is_logged_in');
        $cek_admin = $this->main_m->cek_session($this->user_id, $this->user_name, $this->admin_logged);
        if ($cek_admin) {
            $this->admin_id = $cek_admin->id;
            $this->admin_group = $cek_admin->account_group;
            $this->admin_name = $cek_admin->account_displayname;
        } else {
            redirect('dashboard/login');
        }
    }

    function index() {
        $data['page'] = 'Home';
        $data['title'] = 'User';
        $data['parent'] = 'Add';
        $data['admin_name'] = $this->admin_name;
        $data['admin_group'] = $this->admin_group;

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('rfid', 'RFID', 'trim|callback_rfid_used');
        $this->form_validation->set_rules('seat', 'Seat', 'trim|required');
        $this->form_validation->set_rules('visitor', 'Visitor Access', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $in_data['account_displayname'] = $this->input->post('name', true);
            $in_data['account_username'] = $this->user_m->nicename($this->input->post('name', true));
            $in_data['account_rfid'] = $this->input->post('rfid', true);
            $in_data['account_status'] = 2;

            $this->db->insert('account', $in_data);
            $acc_id = $this->db->insert_id();

            $in_data_rfid['seat'] = $this->input->post('seat', true);
            $in_data_rfid['account_id'] = $acc_id;
            $in_data_rfid['visitor_id'] = $this->input->post('visitor', true);
            $in_data_rfid['data_status'] = 1;
            $in_data_rfid['date_add'] = date('Y-m-d H:i:s');

            $row = $this->db->get_where('seat_account', array('account_id' => $acc_id))->row();
            if($row){
                $this->db->where('id', $row->id);
                $this->db->update('seat_account', $in_data_rfid);
            }else{
                $this->db->insert('seat_account', $in_data_rfid);
            }

            $data['content'] = $this->load->view('dashboard/user/success', $data, true);
        } else {
            $data['visitor'] = $this->visitor_m->list_visitor();
            $data['content'] = $this->load->view('dashboard/user/add', $data, true);
        }
        $this->load->view('dashboard/main', $data);
    }

    function downloadfile($file) {
        $file_path = FCPATH . 'uploads/usermerchant/' . $file['name'];
        if (move_uploaded_file($file['tmp_name'], $file_path)) {
            return 1;
        } else {
            return 0;
        }
    }

    function email_used($str) {
        if (!isset($str)) {
            $str = $this->input->post('email');
        }
        if($str){
            $new = $this->db->get_where('account_merchant', array('account_email' => $str))->row();
            if (count($new) >= 1) {
                $this->form_validation->set_message('email_used', 'Registered email, please replace Email');
                return false;
            } else {
                return true;
            }
        }else{
            return true;
        }
    }

    function rfid_used($str) {
        $str = strtolower($str);
        if ($str != "") {
            $row = $this->db->get_where('account', array('account_rfid' => $str))->num_rows();
            if ($row != 0) {
                $this->form_validation->set_message('rfid_used', 'RFID already registered');
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
