<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('fungsi');
        $this->load->model('dashboard/login_m');
    }

    function index() {
        $this->cek_login();
    }

    function cek_login() {
        $data['title'] = 'Login';
        $data['message_error'] = FALSE;
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email_login', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password_login', 'Password', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $validation = $this->login_m->getuser();
            if ($validation) {
                $user_login = $this->_createsesi($validation);
                $this->session->set_userdata($user_login);
                redirect('dashboard/home');
            } else {
                $data['message_error'] = TRUE;
            }
        }

        $this->load->view('dashboard/login', $data);
    }

    function _createsesi($validation) {
        $user_login = array(
            'user_id' => $validation->id,
            'user_name' => $validation->account_username,
            'is_logged_in' => true
        );
        return $user_login;
    }

    function logout() {
        $this->_removesesi();
        redirect('dashboard/login', 'refresh');
    }

    function _removesesi() {
        $arsesi = array(
            'user_id' => '',
            'user_name' => '',
            'is_logged_in' => ''
        );
        $this->session->set_userdata($arsesi);
    }

}
