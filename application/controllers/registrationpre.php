<?php

class registrationpre extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('convert', 'landing_m'));

        $this->load->library('fungsi');
        $this->load->library('session');
        $this->load->library('curl');
        #date_default_timezone_set('Asia/Jakarta');
        date_default_timezone_set('America/Port_of_Spain');

        $this->load->helper('url');
        #$this->load->library('sosmed');

        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');

        $custom_page = 'guardian';
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $accid = $this->uri->segment(3, 0);
                if($accid != 0){
                    $url = array('url' => base_url() . 'registrationpre/facebook/'.$accid);
                    $this->load->library('facebook_v4', $url);
                }
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;

                if ($customs->places_fbid != '0') {
                    $this->tpl['fb_page'] = $customs->places_fbid;
                }
                if ($customs->places_twitter != '0') {
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
    }
    
    function index(){
        $this->home();
    }

    function home() {
        header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->tpl['main_title'] = 'Registration';
        $this->tpl['core'] = 'Registration';
        $this->tpl['title'] = "Inevents";

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama', 'trim');
        $this->form_validation->set_rules('id', 'Nama', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $nama = $this->input->post('name', true);
            $id = $this->input->post('id', true);
            if($id && $nama){
                $sql = "select a.id
                        from wooz_account_sosmed as a  
                        where id = '" . $id . "'";
                $query = $this->db->query($sql);
                $data = $query->row();
                if ($data) {
                    $this->tpl['data'] = $data;
                    redirect('registrationpre/sosmed/' . $data->id);
                } else {
                    $this->tpl['core'] = 'User Not Found';
                    $this->tpl['title'] = "User Not Found";
                    $this->tpl['TContent'] = $this->load->view('registrationpre/notfound', $this->tpl, true);
                }
            }else{
                $this->tpl['core'] = 'User Not Found';
                $this->tpl['title'] = "User Not Found";
                $this->tpl['TContent'] = $this->load->view('registrationpre/notfound', $this->tpl, true);
            }
        } else {
            $this->tpl['TContent'] = $this->load->view('registrationpre/form', $this->tpl, true);
        }
        $this->load->view('registrationpre/home', $this->tpl);
    }

    function found($account_id = null) {
        if (!$account_id) {
            redirect('registrationpre/home');
        }
        $this->tpl['retry'] = false;
        $status = $this->input->get('status', true);
        if ($status) {
            $this->tpl['status'] = $status;
            $this->tpl['retry'] = true;
        }
        $this->tpl['main_title'] = 'registration';
        $this->tpl['core'] = 'User Found';
        $this->tpl['title'] = "User Found";

        $sql = "select * FROM wooz_account_sosmed where id = '" . $account_id."'";
        $query = $this->db->query($sql);
        $data = $query->row();
        $this->tpl['nama'] = $data->account_displayname;
        $this->tpl['id'] = $account_id;
        $this->tpl['info'] = $data;

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        /*
        $this->form_validation->set_rules('name', 'Full Name', 'trim|required'); 
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_used_en[' . $account_id . ']');
        $this->form_validation->set_rules('tgl', 'Date', 'trim|required|numeric|callback_cek_data_tanggal_en');
        $this->form_validation->set_rules('bln', 'Month', 'trim|callback_cek_data_dropdown_bulan_en');
        $this->form_validation->set_rules('thn', 'Year', 'trim|required|numeric|callback_year_chk_en');
        $this->form_validation->set_rules('company', 'Company', 'trim|required');
        $this->form_validation->set_rules('employee', 'Employee Class', 'trim|required');
        $this->form_validation->set_rules('department', 'Department', 'trim|required');
        */
        $this->form_validation->set_rules('rfid', 'RFID', 'trim|required|callback_rfid_used[' . $account_id . ']');

        if ($this->form_validation->run() === TRUE) {
            $newdata['account_rfid'] = $this->input->post('rfid');
            $newdata['account_register2'] = 1; /*
            $newdata['account_displayname'] = $this->input->post('name', true);
            $newdata['account_username'] = $this->convert->nicename($this->input->post('name', true));
            $newdata['account_email'] = $this->input->post('email', true);
            $newdata['account_birthdate'] = $this->input->post('thn', true) . '-' . $this->input->post('bln', true) . '-' . $this->input->post('tgl', true);
            $newdata['account_gender'] = $this->input->post('gender', true);
            /* add form 
            $newdata['account_gw_token'] = $this->input->post('company', true); //company
            $newdata['account_gw_refresh'] = $this->input->post('employee', true); //employer class
            $newdata['account_gw_time'] = $this->input->post('department', true); //department
            /* end form */

            $this->db->where('id', $account_id);
            $this->db->update('account_sosmed', $newdata);
            redirect('registrationpre/checkin/'.$account_id);
        }

        $this->tpl['TContent'] = $this->load->view('registrationpre/found', $this->tpl, true);
        $this->load->view('registrationpre/home', $this->tpl);
    }

    function checkin($account_id){
        if (!$account_id) {
            redirect('registrationpre/home');
        }
        $this->tpl['retry'] = false;
        $status = $this->input->get('status', true);
        if ($status) {
            $this->tpl['status'] = $status;
            $this->tpl['retry'] = true;
        }
        $this->tpl['main_title'] = 'registration';
        $this->tpl['core'] = 'User Found';
        $this->tpl['title'] = "User Found";

        $sql = "select account_displayname,account_gw_refresh,account_gw_time FROM wooz_account_sosmed where id = '" . $account_id."'";
        $query = $this->db->query($sql);
        $data = $query->row();
        $this->tpl['id'] = $account_id;
        $this->tpl['info'] = $data;

        $date = date('Y-m-d');
        $time = date('h:i:s');
        $db['account_id'] = $account_id;
        $db['places_id'] = $this->custom_id;
        $db['log_type'] = 1;
        $db['log_date'] = $date;
        $db['log_time'] = $time;
        $db['log_gate'] = 15;
        $db['log_check'] = 3;
        $db['log_hash'] = 'v' . $this->fungsi->acak(time()) . '-' . $account_id;
        $db['log_status'] = 1;
        $db['log_stamps'] = date('Y-m-d H:i:s');
        $upd = $this->db->insert('log_user_gate', $db);

        $this->tpl['TContent'] = $this->load->view('registrationpre/checkin', $this->tpl, true);
        $this->load->view('registrationpre/home', $this->tpl);

    }

    function activated(){
        header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->tpl['main_title'] = 'Registration';
        $this->tpl['core'] = 'Registration';
        $this->tpl['title'] = "Inevents";

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('rfid', 'RFID', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $rfid = $this->input->post('rfid', true);
            if($rfid){
                $sql = "select a.id
                        from wooz_account_sosmed as a  
                        where account_rfid = '" . $rfid . "'";
                $query = $this->db->query($sql);
                $data = $query->row();
                if ($data) {
                    $this->tpl['data'] = $data;
                    redirect('registrationpre/sosmed/' . $data->id);
                } else {
                    $this->tpl['core'] = 'User Not Found';
                    $this->tpl['title'] = "User Not Found";
                    $this->tpl['TContent'] = $this->load->view('registrationpre/notfound', $this->tpl, true);
                }
            }else{
                $this->tpl['core'] = 'User Not Found';
                $this->tpl['title'] = "User Not Found";
                $this->tpl['TContent'] = $this->load->view('registrationpre/notfound', $this->tpl, true);
            }
        } else {
            $this->tpl['TContent'] = $this->load->view('registrationpre/rfid', $this->tpl, true);
        }
        $this->load->view('registrationpre/home', $this->tpl);

    }
    
    function sosmed($account_id = null) {
        if (!$account_id) {
            redirect('registrationpre/home');
        }
        $this->tpl['retry'] = false;
        $status = $this->input->get('status', true);
        if ($status) {
            $this->tpl['status'] = $status;
            $this->tpl['retry'] = true;
        }
        $this->tpl['main_title'] = 'registration';
        $this->tpl['core'] = 'User Found';
        $this->tpl['title'] = "User Found";

        $sql = "select * FROM wooz_account_sosmed where id = '" . $account_id."'";
        $query = $this->db->query($sql);
        $data = $query->row();
        $this->tpl['id'] = $account_id;
        $this->tpl['info'] = $data;

        if($data->account_fbid == '' && $data->account_token == ''){
            $login_url = $this->facebook_v4->get_login_url();
            $url_logout = site_url('registrationpre/fblogouts/'.$account_id);
            $logout_url = $this->facebook_v4->get_logout_url($url_logout);
            $token = $this->facebook_v4->token();
            if ($logout_url) {
                $this->tpl['login'] = $logout_url;
            } else {
                $this->tpl['login'] = $login_url;
            }
        }

        $this->tpl['TContent'] = $this->load->view('registrationpre/signupskip', $this->tpl, true);
        $this->load->view('registrationpre/home', $this->tpl);

    }

    function fblogouts(){
        $accid = $this->uri->segment(3, 0);

        $this->facebook_v4->destroy();
        redirect('registrationpre/sosmed/'.$accid);
    }

    function share(){
        $accid = $this->uri->segment(3, 0);
        if($accid == 0){
            redirect('registrationpre/home');
        }
        $user = $this->db->get_where('account_sosmed', array('id' => $accid))->row();

        $text = $this->db->get_where('places', array('id' => $this->custom_id))->row();

        $this->tpl['text'] = $text;
        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('registrationpre/signupshare', $this->tpl, true);
        $this->load->view('registrationpre/home', $this->tpl);
    }

    function sharesosmed(){
        $this->load->library('sosmedtw');
        $accid = $this->uri->segment(3, 0);
        if($accid == 0){
            redirect('registrationpre/home');
        }
        $user = $this->db->get_where('account_sosmed', array('id' => $accid))->row();
        $share = $this->uri->segment(4, 1);

        $places = $this->db->get_where('places', array('id' => $this->custom_id))->row();
        if ($user->account_fbid && $user->account_token) {
            $url = array('url' => base_url() . 'registrationpre/sharesosmed/'.$accid.'/1');
            $this->load->library('facebook_v4/facebooks_v4', $url);
            if($places->places_fb_page_id != ''){
                $landing['landing_fb_like'] = $this->facebooks_v4->get_like_fb($user->account_token, $user->account_fbid, $places->places_fb_page_id);
            }
            $landing['landing_fb_friends'] = $this->facebooks_v4->get_friend_fb($user->account_token, $user->account_fbid);
            if($share == 1){
                $text = $places->places_cstatus_fb;
                $status = array(
                    'link' => base_url(),
                    'name' => $places->places_name,
                    'caption' => $user->account_displayname." ".$text,//." @".$venue->places_address,
                    //'message' => '@'.$venue->places_address
                    'message' => ''
                );
                if ($places->places_type == 4) {
                    $image = FCPATH . $places->places_avatar;
                    $album = 0;
                    $album = $this->facebooks_v4->get_album($places, $image, $user->account_fb_name, $user->account_fbid, $user->account_token);
                    $landing['landing_fb_status'] = $this->facebooks_v4->post_photo($places, $image, $user->account_fb_name, $user->account_fbid, $user->account_token,$album);
                } else {
                    $landing['landing_fb_status'] = $this->facebooks_v4->update_status($status, $user->account_fbid, $user->account_token);
                }
            }
        }
        if ($user->account_tw_token && $user->account_tw_secret) {
            if($places->places_twitter != ''){
                $landing['landing_tw_follow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_twitter);
            }
            $landing['landing_tw_friends'] = $this->sosmedtw->friend_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username);
            if($share == 1){
                $texttw = $places->places_cstatus_tw;
                if ($places->places_type == 4) {
                    $image = FCPATH . $places->places_avatar;
                    $landing['landing_tw_status'] = $this->sosmedtw->upload_photo_tw($texttw, $image, $user->account_tw_token, $user->account_tw_secret);
                } else {
                    $landing['landing_tw_status'] = $this->sosmedtw->update_tw($texttw, $user->account_tw_token, $user->account_tw_secret);
                }

            }
        }
        $landing['landing_rfid'] = $user->account_rfid;
        $landing['landing_from_regis'] = 2;
        $landing['account_id'] = $accid;
        $landing['landing_register_form'] = $this->custom_id;
        $this->db->insert('landing', $landing);
        redirect('registrationpre/sosmeddone/'.$accid);
    }

    function sosmeddone(){
        $this->tpl['page'] = "Done";
        $this->tpl['title'] = "Personal Information";

        $accid = $this->uri->segment(3,0);
        $user = $this->db->get_where('account_sosmed', array('id' => $accid))->row();
        $this->tpl['logoutfb'] = 0;

        if ($user->account_token) {
            $this->tpl['logoutfb'] = 1;
            $url = array('url' => base_url() . 'registrationpre/sosmeddone/'.$accid);
            $this->load->library('facebook_v4/facebooks_v4', $url);
            $url_logout = site_url('registrationpre/logoutsosmed');
            $this->tpl['logout'] = $this->facebooks_v4->get_logout_tokens($user->account_token,$url_logout);
        }

        $this->tpl['page'] = "thankyou";
        $this->tpl['title'] = "Thankyou!";
        $this->tpl['TContent'] = $this->load->view('registrationpre/done', $this->tpl, true);
        $this->load->view('registrationpre/home', $this->tpl);
    }

    function logoutsosmed() {
        $this->session->unset_userdata('session_id');
        $this->session->unset_userdata('ip_address');
        $this->session->unset_userdata('user_agent');
        $this->session->unset_userdata('last_activity');
        #$this->_killFacebookCookies();
        $this->session->sess_destroy();
        session_destroy();
        unset($_SESSION);
        $past = time() - 3600;
        foreach ($_COOKIE as $key => $value) {
            setcookie($key, $value, $past, '/');
        }
        redirect('registrationpre/home');
    }
    
    function done(){
        $this->tpl['page'] = "Done";
        $this->tpl['title'] = "Personal Information";

        $accid = $this->uri->segment(3,0);
        $user = $this->db->get_where('account_sosmed', array('id' => $accid))->row();
        $this->tpl['logoutfb'] = 0;

        if ($user->account_token) {
            $this->tpl['logoutfb'] = 1;
            $url = array('url' => base_url() . 'registrationpre/done/'.$accid);
            $this->load->library('facebook_v4/facebooks_v4', $url);
            $url_logout = site_url('registrationpre/logout');
            $this->tpl['logout'] = $this->facebooks_v4->get_logout_tokens($user->account_token,$url_logout);
        }

        $this->tpl['page'] = "thankyou";
        $this->tpl['title'] = "Thankyou!";
        $this->tpl['TContent'] = $this->load->view('registrationpre/signupdone', $this->tpl, true);
        $this->load->view('registrationpre/home', $this->tpl);
    }

    function logout() {
        $this->session->unset_userdata('session_id');
        $this->session->unset_userdata('ip_address');
        $this->session->unset_userdata('user_agent');
        $this->session->unset_userdata('last_activity');
        #$this->_killFacebookCookies();
        $this->session->sess_destroy();
        session_destroy();
        unset($_SESSION);
        $past = time() - 3600;
        foreach ($_COOKIE as $key => $value) {
            setcookie($key, $value, $past, '/');
        }
        redirect('registrationpre');
    }

    function facebook($accid = 0) {
        $user_me = $this->facebook_v4->get_user();
        $token = $this->facebook_v4->token();
        $url = site_url('registrationpre/sosmed/'. $accid);
        $logout_url = $this->facebook_v4->get_logout_url($url);

        if ($token && $user_me) {
            $data['account_fb_expired'] = $token['date'];
            $data['account_token'] = $token['token'];
            $data['account_fbid'] = $user_me['id'];
            $data['account_fb_name'] = $user_me['name'];
            $image_pp = $this->facebook_v4->image_pp();
            if($image_pp){
                $data['account_avatar'] = $image_pp->url;
            }

            $this->db->where('id', $accid);
            $this->db->update('account_sosmed', $data);
            redirect('registrationpre/sosmed/' . $accid);
        }else{
            redirect('registrationpre/sosmed/' . $accid);
        }
    }

    function twitter($id = 0) {
        include(APPPATH . 'third_party/tmhOAuth.php');
        include(APPPATH . 'third_party/tmhUtilities.php');
        $tmhOAuth = new tmhOAuth(array(
            date_default_timezone_set('Asia/Jakarta'),
            'consumer_key' => $this->config->item('twiiter_consumer_key'),
            'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
        ));
        $twitcancel = $this->input->get('denied', true);
        if ($twitcancel) {
            redirect('registrationpre/sosmed/' . $id);
        }
        if (isset($_REQUEST['oauth_verifier'])) {
            /* oauth_verifier */
            $tmhOAuth->config['user_token'] = $_SESSION['oauth']['oauth_token'];
            $tmhOAuth->config['user_secret'] = $_SESSION['oauth']['oauth_token_secret'];

            $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/access_token', ''), array(
                'oauth_verifier' => $_REQUEST['oauth_verifier']
            ));

            if ($code == 200) {
                /* oauth verifier success */
                unset($_SESSION['oauth']);
                $access_token = $tmhOAuth->extract_params($tmhOAuth->response['response']);
                if (is_array($access_token) && count($access_token) != '0') {
                    /* update user token at database */
                    $db['account_tw_token'] = $access_token['oauth_token'];
                    $db['account_tw_secret'] = $access_token['oauth_token_secret'];
                    $db['account_tw_username'] = $access_token['screen_name'];

                    /* get additional user data */
                    $tmhOAuth->config['user_token'] = $access_token['oauth_token'];
                    $tmhOAuth->config['user_secret'] = $access_token['oauth_token_secret'];
                    $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/account/verify_credentials'));

                    if ($code == 200) {
                        /* get data success */
                        $response = json_decode($tmhOAuth->response['response']);
                        $crd['account_tw_username'] = $access_token['screen_name'];
                        $crd['account_tw_name'] = $response->name;
                        $crd['account_twid'] = $access_token['user_id'];
                        $crd['account_tw_token'] = $access_token['oauth_token'];
                        $crd['account_tw_secret'] = $access_token['oauth_token_secret'];

                        $crd['account_twid'] = $response->id;
                        $crd['account_profile'] = $response->description;
                        $crd['account_location'] = $response->location;
                        $crd['account_url'] = $response->url;
                        /*
                        $totalfollow = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/users/show'), array('screen_name' =>  $crd['account_tw_username']));
                        $crd['account_tw_follow'] = 0;
                        if ($totalfollow == 200) {
                            $responsefollow = $tmhOAuth->response['response'];
                            $respfollow = json_decode($responsefollow);
                            if (isset($respfollow->followers_count) && $respfollow->followers_count != '') {
                                $crd['account_tw_follow'] = $respfollow->followers_count;
                            }
                        } */
                        $crd['account_lastupdate'] = date('Y-m-d H:i:s');
                        $this->db->where('id', $id);
                        $this->db->update('account_sosmed', $crd);
                    }
                    redirect('registrationpre/sosmed/' . $id);
                }
            } else {
                redirect('registrationpre/sosmed/' . $new.'?retry=true&status=twitter_oauth_error');
            }
        } else {
            /* twitter connect start */
            $callback = site_url('registrationpre/twitter/' . $id);


            $params = array('oauth_callback' => $callback);
            $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/request_token', ''), $params);
            if ($code == 200) {
                /* token request available */
                $_SESSION['oauth'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
                $url = $tmhOAuth->url('oauth/authorize', '') . "?oauth_token={$_SESSION['oauth']['oauth_token']}&force_login=1";
                redirect($url);
            } else {
                redirect('registrationpre/sosmed/' . $new.'?retry=true&status=twitter_start_error');
            }
        }
    }

    function email_used($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('email');
        }
        $new = $this->db->get_where('account_sosmed', array('account_email' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('account_sosmed', array('account_email' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('email_used', 'Registered email, please replace Email');
                return false;
            }
        } else {
            return true;
        }
    }

    function email_used_en($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('email');
        }
        $new = $this->db->get_where('account_sosmed', array('account_email' => $str))->row();
        if ($new) {
            $news = $this->db->get_where('account_sosmed', array('account_email' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('email_used_en', 'Registered email, please replace Email');
                return false;
            }
        } else {
            return true;
        }
    }

    function cek_data_dropdown_bulan_en($str) {
        if ($str != 0) {
            return true;
        } else {
            $this->form_validation->set_message('cek_data_dropdown_bulan_en', 'The %s field is required');
            return false;
        }
    }

    function cek_data_tanggal_en($str) {
        $day = $this->input->post('tgl');
        $month = $this->input->post('bln');
        $year = $this->input->post('thn');
        if($day && $month && $year){
            if (checkdate($month, $day, $year)) {
                return TRUE;
            } else {
                $this->form_validation->set_message('cek_data_tanggal_en', 'Enter the correct date of birth.');
                return FALSE;
            }
        }else{
                $this->form_validation->set_message('cek_data_tanggal_en', 'Enter the correct date of birth.');
                return FALSE;
        }
    }

    function year_chk_en($str) {
        $yearnow = date('Y');
        $syarat = $yearnow - 13;
        $syarat1 = $yearnow - 83;
        if (($str >= $syarat1) && ($str <= $syarat)) {
            return true;
        } else {
            $this->form_validation->set_message('year_chk_en', 'Year of birth does not qualify');
            return false;
        }
    }

    function rfid_used($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('rfid');
        }
        $new = $this->db->get_where('account_sosmed', array('account_rfid' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('account_sosmed', array('account_rfid' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('rfid_used', 'RFID already registered');
                return false;
            }
        } else {
            return true;
        }
    }
    
    function downloadfile($file) {
        $file_path = FCPATH . 'uploads/user/' . $file['name'];
        if (move_uploaded_file($file['tmp_name'], $file_path)) {
            return 1;
        } else {
            return 0;
        }
    }
    
    function search($places) {
        // process posted form data (the requested items like province)
        $keyword = $this->input->post('term');
        $data['response'] = 'false'; //Set default response
        switch ($places) {
            case 1:
                $sql = "SELECT a.id,a.account_displayname as hasil,account_gw_token,account_gw_refresh,account_gw_time FROM wooz_account_sosmed as a
            WHERE a.account_displayname like '%" . $keyword . "%' and a.account_status = 2 and account_fest = 1 and account_rfid = 0";
                break;
            case 2:
                $sql = "SELECT a.id,a.account_email as hasil,account_gw_token,account_gw_refresh,account_gw_time FROM wooz_account_sosmed as a
            WHERE a.account_email like '%" . $keyword . "%' and a.account_status = 2 and account_fest = 1 and account_rfid = 0";
                break;
            case 3:
                $sql = "SELECT a.id,a.account_rfid as hasil,account_gw_token,account_gw_refresh,account_gw_time FROM wooz_account_sosmed as a
            WHERE a.account_rfid like '%" . $keyword . "%' and a.account_status = 2 and account_fest = 1 and account_rfid = 0";
                break;
            default:
                $sql = "SELECT a.id,a.account_displayname as hasil,account_gw_token,account_gw_refresh,account_gw_time FROM wooz_account_sosmed as a
            WHERE a.account_displayname like '%" . $keyword . "%' and a.account_status = 2 and account_fest = 1 and account_rfid = 0";
                break;
        }
        $query = $this->db->query($sql);
        $dataquery = $query->result();

        if (!empty($dataquery)) {
            $data['response'] = 'true'; //Set response
            $data['message'] = array(); //Create array
            foreach ($dataquery as $row) {
                $data['message'][] = array(
                    'value' => $row->hasil,
                    'id' => $row->id,
                    'company' => $row->account_gw_token,
                    'employee' => $row->account_gw_refresh,
                    'department' => $row->account_gw_time
                );  //Add a row to array
            }
        }
        if ('IS_AJAX') {
            echo json_encode($data); //echo json string if ajax request
        } else {
            $this->load->view('dashboard/show', $data); //Load html view of search results
        }
    }

}
