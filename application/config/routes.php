<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['landing'] = 'landing/home';

#$apps_url = explode('.', $_SERVER['SERVER_NAME']);
#if(count($apps_url)== 3 && $apps_url[0]!='www') {
$apps_url = explode('/', $_SERVER['PATH_INFO']);
if(count($apps_url)>= 2 && $apps_url[1] =='landing') {
	$route['default_controller'] = "landing/landing";
	
	$route['landing/home'] = 'landing/landing/home';
	$route['landing/follow'] = 'landing/landing/follow';
	$route['landing/logout'] = 'landing/landing/logout';
	$route['landing/step2'] = 'landing/stepsecond';
	$route['landing/step3'] = 'landing/stepthird';
	$route['landing/step4'] = 'landing/stepfour';        
	$route['landing/step4s'] = 'landing/stepfour/ceklikefollow';
        $route['landing/step5'] = 'landing/stepshare';
        $route['landing/stepshare'] = 'landing/stepshare/share';
        $route['landing/stepnoshare'] = 'landing/stepshare/noshare';
	$route['landing/thankyou'] = 'landing/thankyou'; 
	$route['landing/cekemail'] = 'landing/stepsecond/cekemail';
	$route['landing/emailfound'] = 'landing/stepsecond/emailfound';
	$route['landing/mergeaccount'] = 'landing/stepsecond/mergeaccount';
	$route['landing/mergeaccount/(.*)'] = 'landing/stepsecond/mergeaccount';
	$route['landing/verify'] = 'landing/landing/verify';
	$route['landing/verify/(.*)'] = 'landing/landing/verify';
	$route['landing/accountfound/(.*)/(.*)'] = 'landing/landing/accountfound';
    
	$route['landing'] = 'landing/landing';
	$route['home'] = 'landing/landing/home';
	$route['logout1'] = 'landing/landing/logout';
	$route['step2'] = 'landing/stepsecond';
	$route['step3'] = 'landing/stepthird';
	$route['step4'] = 'landing/stepfour';
	$route['thankyou'] = 'landing/thankyou'; 
	$route['cekemail'] = 'landing/stepsecond/cekemail';
	$route['emailfound'] = 'landing/stepsecond/emailfound';
	$route['mergeaccount'] = 'landing/stepsecond/mergeaccount';
	$route['mergeaccount/(.*)'] = 'landing/stepsecond/mergeaccount';
	$route['verify'] = 'landing/landing/verify';
	$route['verify/(.*)'] = 'landing/landing/verify';
	$route['accountfound/(.*)/(.*)'] = 'landing/landing/accountfound';
}
if(count($apps_url)>= 2 && $apps_url[1] =='landingnew') {
	$route['default_controller'] = "landingnew/landing";
	
	$route['landingnew/home'] = 'landingnew/landing/home';
	$route['landingnew/follow'] = 'landingnew/landing/follow';
	$route['landingnew/logout'] = 'landingnew/landing/logout';
	$route['landingnew/step2'] = 'landingnew/stepsecond';
	$route['landingnew/step3'] = 'landingnew/stepthird';
	$route['landingnew/step4'] = 'landingnew/stepfour';        
	$route['landingnew/step4s'] = 'landingnew/stepfour/ceklikefollow';
        $route['landingnew/step5'] = 'landingnew/stepshare';
        $route['landingnew/stepshare'] = 'landingnew/stepshare/share';
        $route['landingnew/stepnoshare'] = 'landingnew/stepshare/noshare';
	$route['landingnew/thankyou'] = 'landingnew/thankyou'; 
	$route['landingnew/cekemail'] = 'landingnew/stepsecond/cekemail';
	$route['landingnew/emailfound'] = 'landingnew/stepsecond/emailfound';
	$route['landingnew/mergeaccount'] = 'landingnew/stepsecond/mergeaccount';
	$route['landingnew/mergeaccount/(.*)'] = 'landingnew/stepsecond/mergeaccount';
	$route['landingnew/verify'] = 'landingnew/landing/verify';
	$route['landingnew/verify/(.*)'] = 'landingnew/landing/verify';
	$route['landingnew/accountfound/(.*)/(.*)'] = 'landingnew/landing/accountfound';
}

$route['404_override'] = '';

$route['landing'] = 'landing/home';

/* static route */
$route['about'] = 'home/about';
$route['privacy'] = 'home/privacy';
$route['termsofservice'] = 'home/tos';

/* auth route */
$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';
$route['signup'] = 'auth/join';
$route['signupnext'] = 'auth/joinnew';
$route['signupskip'] = 'auth/joinskip';
$route['signupskip/(.*)'] = 'auth/joinskip';
$route['signupdone'] = 'auth/joindone';
$route['cek'] = 'auth/cek';
$route['ceksignup'] = 'auth/signup';
$route['cekdone/(.*)'] = 'auth/signupdone';

/* misc route */
$route['profile'] = 'woozer/profile';
$route['editprofile'] = 'woozer/editprofile';
$route['woozer/card'] = 'woozer/card';
$route['woozer/addcard'] = 'woozer/addcard';
$route['woozer/editcard'] = 'woozer/editcard';
$route['woozer/editcard/(.*)'] = 'woozer/editcard';
$route['woozer/delcard'] = 'woozer/delcard';
$route['woozer/delcard/(.*)'] = 'woozer/delcard';
$route['woozer/(.*)'] = 'woozer';
$route['woozpot'] = 'woozer/spot';
$route['woozpot/(.*)'] = 'woozer/spot';
$route['digitalk/traps/(.*)'] = "digitalk/traps/";

/* End of file routes.php */
/* Location: ./application/config/routes.php */