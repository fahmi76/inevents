<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

// $url = 'http://inevents.wooz.in//';
$url = 'http://localhost/inevents/';
//hayria
$config['photo_upload_folder'] = 'photos/';
$config['facebook_url'] = $url . 'home/facebook';
$config['twitter_url'] = $url . 'home/twit';
$config['assets_url'] = $url . 'assets';
$config['uploads_url'] = $url . 'uploads';
$config['url'] = $url;
$config['landing_url'] = $url . 'landing';

/* End of file config.php */
/* Location: ./application/config/site_options.php */
