<?php
function xdebug($param=array()){
    echo '<pre>'.print_r($param,true).'</pre>';
}


function get_text($row){
	if(isset($row->message) && $row->message){
		return xml_convert($row->message);
	}elseif(isset($row->description) && $row->description){
		return xml_convert($row->description);
	}elseif(isset($row->caption) && $row->caption){
		return xml_convert($row->caption);
	}else{
		return '...';
	}
}