<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<?php class MY_Model extends CI_Model {

protected $table_name = "table_name";
protected $primary_key = "primary_key";

 
function __construct(){
	// Call the Model constructor
    parent::__construct();
}


function _query_cache_browse($keyword){
	/*	
	if($keyword){
		$this->db->like("id_portfolio", $keyword);
		$this->db->or_like("name", $keyword);
		$this->db->or_like("description", $keyword);
		$this->db->or_like("category", $keyword);
	}
	
	$this->db->select("portfolio.*");
	$this->db->from($this->table_name);
	*/
}


function _query_order_browse(){
	//$this->db->order_by("name", "ASC");
}


/**
 * Get Paged rows
 *
 * @param string $keyword
 * @param int page
 * @return array result
 */
function get_browse($keyword = null, $page = 1){
	
	$result['rows'] = null;
	$result['rowcount'] = 0;
	$result['curpage'] = $page;
	$result['maxpage'] = $page;
	
	//START CACHE
	$this->db->start_cache();
	
	$this->_query_cache_browse($keyword);
	
	//STOP CACHE
	$this->db->stop_cache();
  	
  	//log_message('error', $query->num_rows().'---'.$this->db->last_query());
  	$query = $this->db->get();
	$result['rowcount'] = $query->num_rows();
		
	$limit = $this->config->item('results_per_page');
	if($result['rowcount']>0){
		$result['maxpage'] = intval(ceil($result['rowcount']/$limit));
	}else{
		$result['maxpage'] = 0;
	}
	if($page > $result['maxpage']){
		$result['curpage'] = $result['maxpage'];
	}
	if($result['curpage']<1){
		$result['curpage'] = 1;
	}
	$this->db->limit($limit, $limit * ($result['curpage'] - 1));
	$this->_query_order_browse();
	
	$query = $this->db->get();
	
	if ($query->num_rows() > 0){
		$result['rows'] = $query->result_array();	
	}
	
	$this->db->flush_cache();
	
	return $result;
}


/**
 * Add new row
 *
 * @param array $arrin
 * @return int id
 */
function add($arrin){
	$this->db->insert($this->table_name, $arrin);
  	return $this->db->insert_id();
}


/**
 * Update Table
 *
 * @param array $data
 */
function update($data){   
    $this->db->where($this->primary_key, $data[$this->primary_key]);
    $this->db->update($this->table_name, $data); 
}


/**
 * Delete row(s)
 *
 * @param int $iddel
 */
function delete($iddel){
    if ($id != NULL) {
        $this->db->where($this->primary_key, $iddel);
        $this->db->delete($this->table_name);
    }
}


/**
 * Get data by primary key
 *
 * @param int $primary_key
 * @return array $result
 */
function get_by_id($primary_key){
	$result = null;
	$this->db->where($this->primary_key, $primary_key);
	$query1 = $this->db->get($this->table_name);
	if ($query1->num_rows() > 0){
		$result = $query1->row_array();
	}	
	return $result;
}



/**
 * Get data by specific column
 *
 * @param int $column_name
 * @return array $result
 */
function get_by_column($colarr = null, $just_one = false){
	$result = null;
	if ($colarr) {
		foreach($colarr as $colname => $colval){
			$this->db->where($colname, $colval);
		}		
		$query1 = $this->db->get($this->table_name);
		if ($query1->num_rows() > 0){
			if($just_one){
				$result = $query1->row_array();
			}else{
				$result = $query1->result_array();
			}				
		}	
	}
	return $result;
}



/**
 * Get next id
 *
 * @return int $result
 */
function get_next_id(){
	$result = 1;
	$this->db->select_max($this->primary_key);
	
	$query1 = $this->db->get($this->table_name);
	if ($query1->num_rows() > 0){
		$row = $query1->row_array(); 
		$result = $row[$this->primary_key] + 1;
	}	
	return $result;
}


}
?>