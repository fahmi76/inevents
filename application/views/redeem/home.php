<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title><?php if(isset($title)&&$title!='') echo $title . ' - '; echo $main_title; ?></title>
        <meta name="description" content="wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity" />


        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/leaderboard/styleredeem.css"/>      
    </head>

    <body>

	<div id="container">	
		<div class="mainlogo">
		</div>
		
		<div class="content">
			<?php echo $TContent; ?>
		</div>
	</div>
	</body>
</html>
