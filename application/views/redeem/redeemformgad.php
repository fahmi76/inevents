<?php if (isset($nama) && $nama != ''): ?>
    <div class="intro">
        <div class="hai-intro">
            Hai <?php echo $nama; ?>,
            <p>Lo sudah melakukan <span class="number"><?php echo $total; ?></span> tap-in</p>
        </div>

        <div class="btnback">
            <a href="<?php echo site_url('redeemniaga/home'); ?>" class="btn-redm btn-active" style="margin:0 0 5px 0;">&lt;&lt; Kembali</a>
        </div>
        <div class="clear"></div>

    </div>
    <div class="spotlist">
        <table class="tabelspot">
            <tr>
                <td class="righttable <?php if (in_array(414, $places)) : ?> activelist <?php endif; ?>">Photo Booth A (finish line)</td>
            </tr>

            <tr>
                <td class="righttable <?php if (in_array(415, $places)) : ?> activelist <?php endif; ?>">Photo Booth B (finish line)</td>
            </tr>

            <tr>
                <td class="righttable <?php if (in_array(416, $places)) : ?> activelist <?php endif; ?>">Photo Booth C (CSR)</td>
            </tr>
            <tr>
                <td class="righttable <?php if (in_array(419, $places)) : ?> activelist <?php endif; ?>">Photo Booth - Niaga</td>
            </tr>
            <tr>
                <td class="righttable <?php if (in_array(417, $places)) : ?> activelist <?php endif; ?>">Check In @ Moment To Go (CSR)</td>
            </tr>
            <tr>
                <td class="righttable <?php if (in_array(418, $places)) : ?> activelist <?php endif; ?>">Check In & Redeem @ CSR Booth</td>
            </tr>
        </table>

    </div>
    <div class="respond-redm">
        <?php if ($redeem == 1) : ?> 
            <div class="btn-redm">Tas</div>
        <?php else: ?>
            <?php if ($total >= 3) : ?> 
                <a href="<?php echo site_url('redeemniaga/isredeem/1/' . $userid); ?>" class="btn-redm btn-active">Tas</a>
            <?php else: ?>
                <div class="btn-redm">Tas</div>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($redeem == 1) : ?> 
            <div class="btn-redm">Tumbler</div>
        <?php else: ?>
            <?php if ($total >= 5) : ?> 
                <a href="<?php echo site_url('redeemniaga/isredeem/2/' . $userid); ?>" class="btn-redm btn-active">Tumbler</a>
            <?php else: ?>
                <div class="btn-redm">Tumbler</div>
            <?php endif; ?>
        <?php endif; ?>
    </div>

<?php endif; ?>
<?php if (isset($error) && $error != ''): ?>
    <h2 class="center">
        <font size="6px" color='blue'><?php echo $error; ?></font> </br>    
    </h2>  
    <meta http-equiv="refresh" content="2;URL=<?php echo site_url('redeemniaga'); ?>" />
<?php endif; ?>