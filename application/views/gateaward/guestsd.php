<script type="text/javascript">
    $(document).ready(function () {
        $('#name').focus();
    });
</script>
<script type="text/javascript">
    $(function() {
        //update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>gateaward/curr_check/<?php echo $customs; ?>/<?php echo $gate; ?>", function(data) {
            $("p#results").empty();

            $.each(data.json, function() {
                $("p#results").append("\
                        \n\
                                Currently Checked In <b><span>: \n\
                                    " + this['total'] + "\
                                \n\
                        </span></b>");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
    function ShowHideDiv(param) {
        var dvPassport = document.getElementById("dvPassport");
        if(param == 'yes'){
            $('#name').focus();
            dvPassport.style.display = "block";
        }else{
            dvPassport.style.display = "none";
        }
    }
</script>
<div class="text-center">
    <h2 class="tap-wristband"><p id="tap-code"><span>GUEST:</span></p></h2>

        <?php if (validation_errors()): ?>
        <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif;?>
    <form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">

        <div class="form-group">
            <br>
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-warning" onclick="ShowHideDiv('yes')">
                    <input type="radio" name="guest" id="chkYes" value="1" > YES
                </label>
                <label class="btn btn-warning" onclick="ShowHideDiv('no')">
                    <input type="radio" name="guest" id="chkNo" value="2"> NO
                </label>
                <label class="btn btn-warning" onclick="ShowHideDiv('yes')">
                    <input type="radio" name="guest" id="chkYes" value="3"> LATER
                </label>
            </div>
        </div>

        <div class="form-group" id="dvPassport" style="display: none">
            <label for="InputEmail">Guest Name</label><br>
            <input id="name" type="text" autocomplete="off" name="name" value="" autofocus />
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        </div>
    </form>
</div>
<!-- old -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#name').focus();
    });
</script>
<script type="text/javascript">
    $(function() {
        //update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>gateaward/curr_check/<?php echo $customs; ?>/<?php echo $gate; ?>", function(data) {
            $("p#results").empty();

            $.each(data.json, function() {
                $("p#results").append("\
                        \n\
                                Currently Checked In <b><span>: \n\
                                    " + this['total'] + "\
                                \n\
                        </span></b>");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
</script>
<div class="text-center">
    <h2 class="tap-wristband"><p id="tap-code"><span>GUEST:</span></p></h2>
<hr>
<div>

            <button type="button" id="yes" class="btn btn-primary btn-block btn-xlg">YES</button>
        <br >

        <button type="button" id="no" class="btn btn-primary btn-block btn-xlg">NO</button>
        <br >
        <button type="button" id="later" class="btn btn-primary btn-block btn-xlg">LATER</button>
        <br >
</div>
<hr>
</div>

<script type="text/javascript">
    $(function() {
        $('#yes').click(function() {
            window.location = '<?php echo base_url(); ?>gateaward/gateschoose/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>/<?php echo $account_id; ?>/1'
                    });
        $('#no').click(function() {
            window.location = '<?php echo base_url(); ?>gateaward/gateschoose/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>/<?php echo $account_id; ?>/2'
                    });
        $('#later').click(function() {
            window.location = '<?php echo base_url(); ?>gateaward/gateschoose/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>/<?php echo $account_id; ?>/3'
                    });
    });
</script>