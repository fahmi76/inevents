<script src="<?php echo $assets_url; ?>/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo $assets_url; ?>/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $assets_url; ?>/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo $assets_url; ?>/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />
<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>gateaward/checknameawalnew/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id);
                        $("#InputCompany").val(ui.item.company);
                        $("#InputDepartment").val(ui.item.department);
                        $("#InputEmpl").val(ui.item.employee);
                    }
        });
    });
</script>
<?php if ($check == 1): ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#rfid').focus();
    });
</script>
<?php else: ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#InputName').focus();
    });
</script>
<?php endif;?>
<script type="text/javascript">
    $(function() {
        update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>gateaward/curr_check/<?php echo $customs; ?>/<?php echo $gate; ?>", function(data) {
            $("p#results").empty();

            $.each(data.json, function() {
                $("p#results").append("\
                        \n\
                    Currently Checked In <b><span>: " + this['total'] + "</span></b>\
                        \n\
                        ");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function later(param,param2,param3){
        //alert(param);
        top.location.href="<?php echo site_url('gateaward/checklater'); ?>/"+param+"/"+param2+"/"+param3;
    }
</script>
<div class="text-center">
    <?php if ($check == 1): ?>
    <h2 class="tap-wristband"><p class="notify"><b><span>Check-In</span></b></p></h2>
    <h2 class="tap-wristband"><p class="notify" id="results"></p></h2>
    <?php else: ?>
    <h2 class="tap-wristband"><p class="notify"><b><span>Check-Out</span></b></p></h2>
    <h2 class="tap-wristband"><p class="notify" id="results"></p></h2>
    <?php endif;?>
    <p style="font-style:italic;font-size:18px;"><?php echo $places_name; ?></p>
    <table style="margin: 0 auto;">
        <tr>
            <?php if ($check == 1): ?>
            <th>
                <h2 class="tap-wristband text-center"><p id="tap-code"><span>Fill your name</span></p></h2>
                <form id="input_submit" action="<?php echo site_url('gateaward/getname/' . $customs . '/' . $check . '/' . $gate); ?>" method="post">
                    <div class="input-form">
                        <p>
                            <input id="InputName" type="text" autocomplete="off" name="name" value="" autofocus /><br>
                            <input id="InputCompany" type="text" autocomplete="off" name="Company" value="" placeholder="Company" autofocus disabled/><br>
                            <input id="InputDepartment" type="text" autocomplete="off" name="Department" placeholder="Department" value="" autofocus disabled/><br>
                            <input id="InputEmpl" type="text" autocomplete="off" name="Employee" placeholder="Employee Class" value="" autofocus disabled/>
                            <input id="InputId" type="hidden" autocomplete="off" name="id" value="" autofocus />
                        </p>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Check Name</button>
                    </div>
                </form>
            </th>
            <?php else: ?>
            <th>
                <h2 class="tap-wristband text-center"><p id="tap-code"><span>Tap Your Card</span></p></h2>
                <form id="input_submit" action="<?php echo current_url(); ?>" method="post">
                    <div class="input-form">
                        <p>
                            <input type="text" autocomplete="off" name="serial" value="" autofocus />
                        </p>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Check RFID</button>
                    </div>
                </form>
            </th>
            <?php endif;?>
        </tr>
    </table>
    <div id="message_ajax" style="margin-top: 15px;">

            <a href="<?php echo site_url('gateaward/checklater/' . $customs . '/' . $check . '/' . $gate); ?>">
            <button type="submit" class="btn btn-info">Later</button>
            </a>
    </div>
</div>