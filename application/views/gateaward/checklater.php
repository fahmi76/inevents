
    <script src="<?php echo $assets_url; ?>/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo $assets_url; ?>/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />
    <script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>gateaward/checkname/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id);
                        $("#Inputcheck").val(ui.item.check);
                    }
        });
    });
</script>
<?php if (validation_errors()): ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', ''); ?>
    </p>
<?php endif;?>
<br />
<form method="post" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Fill your name</label>
        <input type="text" class="form-control input-lg" name="name" id="InputName" value=""  placeholder="name">
    </div>
    <input type="hidden" class="form-control input-lg" name="id" id="InputId" value="">
    <input type="hidden" class="form-control input-lg" name="check" id="Inputcheck" value="">

    <div class="text-center">
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        <a href="<?php echo site_url('gateaward/home/' . $customs . '/' . $check . '/' . $gate) ?>">
            <button type="button" class="btn btn-default btn-xlg">Go Back</button>
        </a>
    </div>
</form>