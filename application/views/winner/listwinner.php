<style type="text/css">
.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #997689; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; }.datagrid table td, .datagrid table th { padding: 3px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #999999), color-stop(1, #2B557F) );background:-moz-linear-gradient( center top, #999999 5%, #2B557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#999999', endColorstr='#2B557F');background-color:#999999; color:#FFFFFF; font-size: 14px; font-weight: bold; border-left: 1px solid #A8A8A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #000000; border-left: 2px solid #E1EEF4;font-size: 14px;border-bottom: 2px solid #E1EEF4;font-weight: normal; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }
</style>
<h2>List <?php echo $cat->nama; ?></h2>
<?php if ($cat->data_status == 1): ?>
<a class="btn btn-danger" href="<?php echo base_url(); ?>winner/lock/<?php echo $cat->id; ?>/2">
    <i class="icon-lock"></i> Lock
</a>
<?php else: ?>
<a class="btn btn-success" href="<?php echo base_url(); ?>winner/lock/<?php echo $cat->id; ?>/1">
    <i class="icon-unlock"></i> Unlock
</a>
<?php endif;?>
<?php if ($cat->toogle == 0): ?>
    <a class="btn btn-warning" href="<?php echo base_url(); ?>winner/cektoogle/<?php echo $cat->id; ?>/1">
        <i class="halflings-icon white edit"></i> check all user
    </a>
<?php else: ?>
    <a class="btn btn-primary" href="<?php echo base_url(); ?>winner/cektoogle/<?php echo $cat->id; ?>/0">
        <i class="halflings-icon white edit"></i> uncheck all user
    </a>
<?php endif;?>
<hr>
<!--
<div class="text-center">
<h2>List Presenter</h2>
    <div class="datagrid">
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Table Numbers</th>
                    <th>Option</th>
                    <th>Status</th>
                    <th>Datetime</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($presenter): ?>
                    <?php foreach ($presenter as $row): ?>
                <tr>
                    <?php $getstatus = $this->winner_m->get_statususer($row->id);?>
                    <td><?php echo $row->account_displayname; ?></td>
                    <td><?php echo $row->content; ?></td>
                    <td><?php echo $row->presenter_status; ?></td> <!--
                    <td><span class=" text-muted label label-default">presenter</span></td>
                    <?php if ($getstatus): ?>
                        <?php if ($getstatus->log_check_out == 2): ?>
                            <?php if ($getstatus->log_status_check == 1): ?>
                            <td><small class=" text-muted label label-warning">temporary check out</small></td>
                            <?php else: ?>
                            <td><small class=" text-muted label label-danger">permanent check out</small></td>
                            <?php endif;?>
                        <?php else: ?>
                            <?php if ($cat->data_status == 2): ?>
                                <?php if ($getstatus->log_stamps <= $cat->datelock): ?>
                                    <td><small class=" text-muted label label-success">checked in</small></td>
                                <?php else: ?>
                                    <td><small class=" text-muted label label-info">Arrived after lock</small></td>
                                <?php endif;?>
                            <?php else: ?>
                            <td><small class=" text-muted label label-success">checked in</small></td>
                            <?php endif;?>
                        <?php endif;?>
                        <td><?php echo $getstatus->log_stamps; ?></td>
                    <?php else: ?>
                        <td><small class=" text-muted label label-primary">none</small></td>
                        <td>0000-00-00 00:00:00</td>
                    <?php endif;?>
                </tr>
                    <?php endforeach;?>
                <?php else: ?>
                <tr>
                    <td>No Data</td>
                </tr>
                <?php endif;?>
            </tbody>
        </table>
    </div>
    <hr>
</div>
-->
<div class="text-center">
<h2>List Winner - Checkin</h2>
    <div class="datagrid">
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Table Numbers</th>
                    <th>Status</th>
                    <th>Datetime</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($listcheckin): ?>
                    <?php foreach ($listcheckin as $row): ?>
                        <?php $getstatus = $this->winner_m->get_statususer($row->id);?>
                        <?php if ($cat->toogle == 0): ?>
                            <tr>
                                <?php if ($getstatus): ?>
                                <td><?php echo $row->account_displayname; ?></td>
                                <td><?php echo $row->content; ?></td>
                                <?php if ($getstatus->log_check_out == 2): ?>
                                    <?php if ($getstatus->log_status_check == 1): ?>
                                    <td><small class=" text-muted label label-warning">temporary check out</small></td>
                                    <?php else: ?>
                                    <td><small class=" text-muted label label-danger">permanent check out</small></td>
                                    <?php endif;?>
                                <?php else: ?>
                                    <?php if ($cat->data_status == 2): ?>
                                        <?php if ($getstatus->log_stamps <= $cat->datelock): ?>
                                            <td><small class=" text-muted label label-success">checked in</small></td>
                                        <?php else: ?>
                                            <td><small class=" text-muted label label-info">Arrived after lock</small></td>
                                        <?php endif;?>
                                    <?php else: ?>
                                    <td><small class=" text-muted label label-success">checked in</small></td>
                                    <?php endif;?>
                                <?php endif;?>
                                <td><?php echo $getstatus->log_stamps; ?></td>
                            <?php endif;?>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <td><?php echo $row->account_displayname; ?></td>
                                <td><?php echo $row->content; ?></td>
                                <?php if ($getstatus): ?>
                                <?php if ($getstatus->log_check_out == 2): ?>
                                    <?php if ($getstatus->log_status_check == 1): ?>
                                    <td><small class=" text-muted label label-warning">temporary check out</small></td>
                                    <?php else: ?>
                                    <td><small class=" text-muted label label-danger">permanent check out</small></td>
                                    <?php endif;?>
                                <?php else: ?>
                                    <?php if ($cat->data_status == 2): ?>
                                        <?php if ($getstatus->log_stamps <= $cat->datelock): ?>
                                            <td><small class=" text-muted label label-success">checked in</small></td>
                                        <?php else: ?>
                                            <td><small class=" text-muted label label-info">Arrived after lock</small></td>
                                        <?php endif;?>
                                    <?php else: ?>
                                    <td><small class=" text-muted label label-success">checked in</small></td>
                                    <?php endif;?>
                                <?php endif;?>
                                <td><?php echo $getstatus->log_stamps; ?></td>
                            <?php else: ?>
                                <td><small class=" text-muted label label-primary">none</small></td>
                                <td>0000-00-00 00:00:00</td>
                            <?php endif;?>
                            </tr>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php else: ?>
                <tr>
                    <td>No Data</td>
                </tr>
                <?php endif;?>
            </tbody>
        </table>
    </div>
    <hr>
    <h2>List Winner - Not Checkin</h2>
    <div class="datagrid">
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Table Numbers</th>
                    <th>Status</th>
                    <th>Datetime</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($listnotcheckin): ?>
                    <?php foreach ($listnotcheckin as $row): ?>
                        <?php if ($cat->toogle == 0): ?>
                            <tr>
                                <td><?php echo $row->account_displayname; ?></td>
                                <td><?php echo $row->content; ?></td>
                                <td><small class=" text-muted label label-danger">not checked in</small></td>
                                <td>0000-00-00 00:00:00</td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <td><?php echo $row->account_displayname; ?></td>
                                <td><?php echo $row->content; ?></td>
                                <td><small class=" text-muted label label-danger">not checked in</small></td>
                                <td>0000-00-00 00:00:00</td>
                            </tr>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php else: ?>
                <tr>
                    <td>No Data</td>
                </tr>
                <?php endif;?>
            </tbody>
        </table>
    </div>
    <hr>
    <button type="button" id="back" class="btn btn-primary btn-xlg">Back</button>

</div>
<script type="text/javascript">
    $(function() {
        $('#back').click(function() {
            window.location = '<?php echo site_url('winner'); ?>'
        });

        <?php foreach ($list as $row): ?>
        $('#win-<?php echo $row->id; ?>').click(function() {
            window.location = '<?php echo site_url('winner/setwin/' . $row->id . '/' . $row->win); ?>'
        });
        <?php endforeach;?>
    });
</script>