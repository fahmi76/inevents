<!DOCTYPE html>
<html>
    <head>
        <title><?php if (isset($title) && $title != '') echo $title . ' - '; echo $main_title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/magnum/redeem/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/magnum/redeem/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/magnum/redeem/css/main-style.css">

        <!-- Custom colors based on company or product color scheme-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/magnum/redeem/css/custom-style.css">
        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.1.min.js"></script> 

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
          <![endif]-->
        <script type="text/javascript">
            urlhome = "<?php echo base_url(); ?>assets/magnum/redeem/";
            base_url = "<?php echo base_url();?>";
            main_url = "<?php echo site_url();?>";
            spot_id = "<?php echo $spot_id; ?>";
        </script>
        <?php if($background != ''): ?>
        <STYLE TYPE="text/css">
            body {
                background-image: url(<?php echo base_url(); ?><?php echo $background; ?>);
            }
        </STYLE>
        <?php else: ?>
        <STYLE TYPE="text/css">
            body {
                background-image: url(<?php echo $assets_url; ?>/landing/images/bg.jpg);
            }
        </STYLE>
        <?php endif; ?>  
<STYLE TYPE="text/css">
                body {
					Background-size: 100% 100%;	
					background-position: center;
				}
            </STYLE>		
    </head>
    <body>
        <div id="fb-root"></div>
        <div id="wrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 center-block">
                        <!-- Product logo -->
                        <h1 class="logo text-center">
                            <?php if($logo != ''): ?>
                                <img src="<?php echo base_url(); ?><?php echo $logo; ?>" alt="<?php echo $main_title; ?>">
                            <?php endif; ?>
						</h1>

                        <!-- Product tagline. Add class: show/hide -->
                        <div class="header text-center hide"><img src="<?php echo base_url(); ?>assets/magnum/redeem/images/header.png" alt="Welcome to the Pleasureable Magnum VVIP Experience"></div>

                        <div class="box">
                            <?php echo $TContent; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="footer" class="row text-center">
            <img src="<?php echo base_url(); ?>assets/magnum/redeem/images/woozin.png" alt="Powered by Wooz.in"> 
            <span><small>&copy;2013 wooz.in (Patent Pending). All rights reserved.</small></span>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>assets/magnum/redeem/js/jquery-2.0.3.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/magnum/redeem/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/magnum/redeem/js/screen.js"></script>
    </body>
</html>