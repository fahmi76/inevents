<h2>Category : <?php echo $list->nama; ?></h2>
<hr>    
<div class="text-center">
    <button type="button" id="win" class="btn btn-primary btn-xlg"><?php echo $list->account_displayname; ?></button>
    <hr>
    <button type="button" id="back" class="btn btn-primary btn-xlg">Finish</button>
    
</div>
<script type="text/javascript">
    $(function() {
        $('#back').click(function() {
            window.location = '<?php echo site_url('winner/done/'.$list->win); ?>'
        });
    });
</script>