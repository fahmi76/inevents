<script type="text/javascript">
    $(function() {
        update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>winner/showjson", function(data) {
            $("p#cat").empty();
            $("div#results").empty();

            $.each(data.json, function() {
                $("div#results").append("\
                        \n\
                         <button type='button' class='btn btn-primary btn-xlg'>"+ this['name'] +"</button>");
                $("p#cat").append("\
                        \n\
                         <h2>Category : "+ this['category'] +"</h2>");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
</script>
<p id="cat"></p>
<hr>    
<div class="text-center" id="results">
    
</div>
