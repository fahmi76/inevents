<h2>List Category</h2>
<hr>    
<div class="text-center">
    <?php $x=0; foreach($gate as $row): ?>
    <button type="button" id="win-<?php echo $row->id; ?>" class="btn btn-primary btn-xlg"><?php echo $row->account_displayname; ?></button>
    <?php if($x%3 == 0): ?>
        <br><br>
    <?php endif; ?>
    <?php $x++; endforeach; ?>
</div>
<script type="text/javascript">
    $(function() {
        <?php foreach($gate as $row): ?>
        $('#win-<?php echo $row->id; ?>').click(function() {
            window.location = '<?php echo site_url('winner/listcat/'. $row->id); ?>'
        });
        <?php endforeach; ?>
    });
</script>