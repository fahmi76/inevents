<script type="text/javascript">
    $(document).ready(function() {
        $("#txtrfid").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<?php if (validation_errors()) : ?>
    <p class="error">Whoops ! Something wrong
    </p>
<?php endif; ?>
    <div style="text-align: center;">
<h3>Hello <?php echo $datalist->account_displayname; ?>,</h3>
<p class="error"><font size=4px>Activate your Card now!<br />Place the card onto the reader</font></p>
<div class="icon-arrowdown"></div>
<form method="post" action="<?php echo current_url() . '/?url=' . $customs . '&acc=' . $id . '&from=' . $from . '&places=' . $spot_id; ?>" id="registration">
   <ul class="form-register">
        <li><?php echo form_error('serial'); ?>
            <input name="serial" id="txtrfid" type="text" /></li>
        <li><input type="submit" value="ACTIVATE" /></li>
        <li>
            <?php if (isset($datalist->account_token) && $datalist->account_token != ''): ?>
                <a href="<?php echo $this->facebook->getLogoutUrl(array('landing/logout?url=' . $customs. '&places=' . $spot_id)); ?>">Logout</a>
            <?php else: ?>
                <a href="<?php echo site_url('landing/logout?url=' . $customs. '&places=' . $spot_id); ?>">Logout</a>
            <?php endif; ?>
        </li>
    </ul>
</form>
</div>