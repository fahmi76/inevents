<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title><?php if(isset($title)&&$title!='') echo $title . ' - '; echo $main_title; ?></title>
        <meta name="description" content="Wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity" />


        <script src="http://assets.wooz.local/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="http://assets.wooz.local/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" media="all" href="http://assets.wooz.local/customs/style.css"/>

        <link rel="stylesheet" href="http://assets.wooz.local/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
        <link rel="stylesheet" href="http://assets.wooz.local/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />        
		<style type="text/css">
			<?php
				if(isset($css)&&$css!='') { 
					echo $css; 
                                        ?>
                        .sigPad {
	width: 275px;
	margin: 0 auto;
}
.signed {
	width: 275px;
	margin: 0 auto;
}
.sigPad #submit {
	float: left;
	margin-top: 15px;
}
.sigPad #clear {
	float: right;
	margin-top: 15px;
}
.sigPad .sig.sigWrapper .pad {
	border: 1px dotted #999;	
}
.error { padding: 5px; background-color:#C00; color: #fff; width: 275px; }
.pad {
	cursor: url("http://wooz.local/assets/signature/pen.png") 8 8, crosshair;
}
                        <?php
				} else {
			?>
			body {
				background: #000;
				color: #fff;
			}
			a {
				color:#656565; 
			}
                        
			<?php } ?>
		</style>
    </head>

    <body>

	<div id="container">

		<div id="header">
			<div id="section-top">
				<a href="<?php echo current_url(); ?>">
				wooz.in | join. connect. woozin'
				</a>
			</div>
		</div>
		<div id="main-box">
			<?php echo $TContent;?>
		</div>
		<div id="section-side">
		</div>
		<div id="footer">
			<a href="http://wooz.in" id="smalllogo">
				<img src="http://wooz.local/assets/customs/smalllogo.png" alt="wooz.in" />
			</a>
			<a href="<?php echo current_url(); ?>" class="home">Home</a> |
			<a href="http://wooz.in/about">About</a> |
			<a href="http://wooz.in/blog">Blog</a> |
			<a href="http://wooz.in/privacy">Privacy Policy</a> |
			<a href="http://wooz.in/termsofservice">Terms Of Service</a>
			<br />
			Wooz.in is a web based application using RFID Owned by BIRA.<br />
			&copy; 2012 wooz.in (Patent Pending). All rights reserved.
		</div>
		<div class="clear"></div>
	</div>
    <div id="fb-root"></div>
	<?php
	$app_id = $this->config->item('facebook_application_id');
	?>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId: '<?php echo $app_id; ?>',
          cookie: true,
          xfbml: true,
          oauth: true
        });
        FB.Event.subscribe('auth.login', function(response) {
          window.location.reload();
        });
        FB.Event.subscribe('auth.logout', function(response) {
          window.location.href = "<?=site_url('logout')?>";
        });
      };
      (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
          '//connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
      }());
    </script>
	</body>
</html>
