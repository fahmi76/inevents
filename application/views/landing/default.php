<?php if(isset($retry) && $retry === true) { ?>
	<p class="error">Please Try Again <?php if(isset($status) && $status != '') echo 'due to '.str_replace('_', ' ', $status) ?> </p>
<?php } ?>
	<h2 class="center">
		click below to start
	</h2>
	<?php if($customs == 'echelon2013'): ?>
		<p class="notify">By registering, I agree to provide my Name and Email address to the event organizers.</p>
	<?php else: ?>
		<p class="notify">Dengan mendaftar saya bersedia memberikan Nama & Email saya kepada pihak penyelenggara acara</p>
	<?php endif; ?>
	<?php if($type_registration == 1 || $type_registration == 3): ?>
        <p class="center">
            <?php $redirect = base_url().'home/facebook/landing/'.$customs.'/0/'.$spot_id;?>
            <a class="fb-connect" href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('logout1'), 'redirect_uri' => $redirect, 'scope' => $fb_perms)); ?>">
				<img src="http://wooz.in/assets/customs/fblogo.png" alt="connect with your facebook account" style="width: 100px; height: 100px;" />
            </a>
			<a class="tw-connect" href="<?php echo base_url(); ?>home/twit/landing/<?php echo $customs; ?>/0/<?php echo $spot_id; ?>">
				<img src="http://wooz.in/assets/customs/twitterlogo.png" alt="connect with your twitter account" style="width: 100px; height: 100px;margin-left: 50px;"/>
            </a>
		</p>
        <?php endif; ?>

		<?php if($customs == 'echelon2013'): ?>
		<p class="center">
			<a class="email-connect" href="<?php echo base_url();?>step2?url=<?php echo $customs; ?>&from=email&places=<?php echo $spot_id; ?>">
				Using E-mail
            </a>
		</p>
		<?php endif; ?>
        <?php if($type_registration == 2 || $type_registration == 3): ?>
 	<h2 class="center">
		or 
	</h2>       
	<p class="center">
            <a class="tw-connect" href="<?php echo base_url(); ?>landing/<?php echo $customs; ?>">
				<img src="http://wooz.in/uploads/apps/fritz/twitterlogos.png" alt="connect with your twitter account" style="width: 100px; height: 100px;"/>
            </a>
	</p>  
        <?php endif; ?>