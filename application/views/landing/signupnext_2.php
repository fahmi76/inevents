<?php
$datenow = date('Y');
$date = $datenow - 13;
?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#txtemail").focus();
		$("#sdate").datepicker({
				dateFormat: 'yy-mm-dd',
				maxDate: new Date(<?php echo $date; ?>, 11,31),
				changeMonth: true,
				changeYear: true,
				yearRange: 'c-60:c',
				beforeShow: function (input, inst) {
					var offset = $(input).offset();
					var height = $(input).height();
					window.setTimeout(function () {
						inst.dpDiv.css({ top: (offset.top + height + 4) + 'px', left: offset.left + 'px' })
					}, 1);
				}
            });
            $("#registration").submit(function() {
                    $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
            });
	});
</script>

<h2>Step 2.</h2>
<?php if(validation_errors()) : ?>
	<p class="error">Whoops ! Something wrong
    <?php echo validation_errors('<br />&gt; ', ''); ?>
	</p>
<?php endif; ?>
	<form method="post" action="<?php echo current_url().'/?url='.$customs.'&places='.$spot_id.'&from='.$from; ?>" id="registration">
 <p>
	Full Name<br />
	<input type="text" id="txtemail"  name="fullname" value="<?php echo set_value('fullname'); ?>" />
</p>
<p>
	Email<br />
	<input type="text" name="email" value="<?php echo set_value('email'); ?>"/>

</p> 
<p >Gender<br />
	<label style="margin-right: 15px;"><input type="radio" name="gender" value="1" style="width: 10px;"/> Male</label>
	<label style="margin-right: 15px;"><input type="radio" name="gender" value="2" style="width: 10px;"/> Female</label>
</p>
<p >birthdate<br />
	<input type="text" name="sdate" id="sdate" value="<?php echo set_value('sdate'); ?>" />
</p>
<p>

	Tap your wirstband number<br />
	<input name="serial" id="txtrfid" type="text" /><br />
</p> 
  
<p class="action">
	<input type="submit" name="submit" value="submit" class="button" />
	<?php $token = $this->facebook->getAccessToken(); ?>
	<a class="cancel button" href="<?php echo $this->facebook->getLogoutUrl(array('next' => site_url('logout?url='.$customs), 'access_token' => $token)); ?>">Cancel</a>
                <!-- <div id="clear">
			<button name="clear" type="clear" class="clearButton">Clear Signature</button>
		  </div> -->
		  <div style="clear:both;"></div>
</p>

		</form>
