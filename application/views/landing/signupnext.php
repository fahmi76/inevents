<script type="text/javascript">
    $(document).ready(function() {
        $("#email").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<?php if ($from == 'fb'): ?>
    <script type="text/javascript">
        var windowSizeArray = ["width=800,height=600",
            "width=800,height=400,scrollbars=yes"];

        $(document).ready(function() {
            $('.newWindow').click(function(event) {

                var url = $(this).attr("href");
                var windowName = "popUp";//$(this).attr("name");
                var windowSize = windowSizeArray[$(this).attr("rel")];
                winRef = new Object();
                winRef = window.open(url, windowName, windowSize);

                event.preventDefault();
                //setTimeout('winRef.close()',15000);

            });
        });
    </script>
<?php endif; ?>
<?php if ($from == 'tw'): ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var urlrfidhome = "<?php echo site_url('landing/follow?url=' . $customs); ?>";
            var accountid = "<?php echo $id; ?>";
            $("#summary").hide();
            $("#id101").click(function() {
                $.ajax({
                    url: urlrfidhome,
                    type: "POST",
                    data: {
                        id: accountid,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.nama == "yes")
                        {
                            $("#summary").show();
                            $("#summarys").hide();
                        }
                        else
                        {
                            $("#summarys").show();
                            $("#summary").hide();
                            return false;
                        }
                    }
                });

            });

        });
    </script>
<?php endif; ?>
<h2>Step 2.</h2>    
<div style="overflow:auto;height: 280px; ">
    <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
            <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
    <?php if ($from == 'fb'): ?>
        <?php if (isset($fb_page) && $fb_page != '') : ?>
            <div>
               <div class="fb-like-box" data-href="<?php echo $fb_page; ?>" data-colorscheme="light" data-show-faces="false" data-header="true" data-stream="false" data-show-border="true"></div>
            </div>	
        <?php endif; ?>		
    <?php endif; ?>
    <?php if ($from == 'tw'): ?>
        <?php if ($followtw == 1): ?>
            <div id="summaryss" style="width: 400px;"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" style="width: 76px;" alt=""/>@<?php echo $tw_page; ?></div>
    <?php else: ?>
        <div id="summarys" style="width: 400px;"><a id="id101" href="#" >
                <img src="http://wooz.in/assets/customs/Twitter_Follow.png" style="width: 76px;" alt=""/></a></div>
        <div id="summary" style="width: 400px;"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" style="width: 76px;" alt=""/>@<?php echo $tw_page; ?></div>
    
    <?php endif; ?>		
<?php endif; ?>
<form method="post" action="<?php echo current_url() . '/?url=' . $customs . '&from=' . $from . '&acc=' . $id . '&places=' . $spot_id; ?>" id="registration">
    <p>
        Full Name<br />
        <?php
        $name = set_value('fullname');
        if ($name) {
            $names = $name;
        } elseif(!$info) {
            $names = '';
        } else {
            $names = $info->account_displayname;
        }
        ?>
        <input type="text"  id="email" name="fullname" value="<?php echo $names; ?>" />
    </p>
	<p>
        <?php
        $fbname = set_value('sosmed');
        if ($from == 'fb') { ?>
			Facebook account name<br /><?php
            $sosmednames = $info->account_fb_name;
        } else {?>
			Twitter account name<br /><?php
            $sosmednames = '@'.$info->account_tw_username;
        }
        ?>
        <input type="text"  id="sosmed" name="sosmed" value="<?php echo $sosmednames; ?>" disabled />
    </p>
    <p>
        Email<br />
        <?php
        $email = set_value('email');
        if ($email) {
            $emails = $email;
        } elseif(!$info) {
            $emails = '';
        } else {
            $emails = $info->account_email;
        }
        ?>
        <input type="text" name="email" value="<?php echo $emails; ?>"/>
    </p>  
    <p >
        Gender<br />
        <?php
        $gender = set_value('gender');
        if ($gender) {
            $genders = $gender;
        } elseif(!$info) {
            $genders = '';
        } else {
            $genders = $info->account_gender;
        }
        ?>
        <label><input type="radio" name="gender" value="male" <?php if (stripos($genders, "male") !== false) : ?>checked<?php endif; ?> style="width: 20px;" /> Male</label>
        <label><input type="radio" name="gender" value="female" <?php if (stripos($genders, "female") !== false) : ?>checked<?php endif; ?> style="width: 20px;" /> Female</label>		
    </p>	
    <p >
        birthdate<br />
        <?php
        if($info){
            $birthdate = explode('-', $info->account_birthdate);
        }else{
            $birthdate[2] = 00;
            $birthdate[0] = 0000;
            $birthdate[1] = 0;
        }

        $tgl = set_value('tgl');
        if ($tgl) {
            $tgls = $tgl;
        }elseif($birthdate[2] == 00){
            $tgls = '';
        } else {
            $tgls = $birthdate[2];
        }
        $thn = set_value('thn');
        if ($thn) {
            $thns = $thn;
        }elseif($birthdate[0] == 0000){
            $thns = '';
        }else {
            $thns = $birthdate[0];
        }
        $bln = set_value('bln');
        if ($bln) {
            $blns = $bln;
        } else {
            $blns = $birthdate[1];
        }
        ?>
        <input type="text" name="tgl" value="<?php echo $tgls; ?>" style="width: 30px;" placeholder="Tgl" />
        -
        <select name="bln">
            <option value="0">--Bulan--</option>
            <option value="1" <?php if ($blns == 1) : ?>selected="selected"<?php endif; ?>>Januari</option>
            <option value="2" <?php if ($blns == 2) : ?>selected="selected"<?php endif; ?>>Februari</option>
            <option value="3" <?php if ($blns == 3) : ?>selected="selected"<?php endif; ?>>Maret</option>
            <option value="4" <?php if ($blns == 4) : ?>selected="selected"<?php endif; ?>>April</option>
            <option value="5" <?php if ($blns == 5) : ?>selected="selected"<?php endif; ?>>Mei</option>
            <option value="6" <?php if ($blns == 6) : ?>selected="selected"<?php endif; ?>>Juni</option>
            <option value="7" <?php if ($blns == 7) : ?>selected="selected"<?php endif; ?>>Juli</option>
            <option value="8" <?php if ($blns == 8) : ?>selected="selected"<?php endif; ?>>Agustus</option>
            <option value="9" <?php if ($blns == 9) : ?>selected="selected"<?php endif; ?>>September</option>
            <option value="10" <?php if ($blns == 10) : ?>selected="selected"<?php endif; ?>>Oktober</option>
            <option value="11" <?php if ($blns == 11) : ?>selected="selected"<?php endif; ?>>November</option>
            <option value="12" <?php if ($blns == 12) : ?>selected="selected"<?php endif; ?>>Desember</option>
        </select>
        -
        <input type="text" name="thn" value="<?php echo $thns; ?>" style="width: 60px;" placeholder="Tahun" />
    </p>    
</div>
<p class="action">
    <input type="submit" name="submit" value="submit" class="button" />
    <?php if (isset($token) && $token != ''): ?>
        <a class="cancel button" href="<?php echo $this->facebook->getLogoutUrl(array('next' => site_url('landing/logout?url=' . $customs. '&places=' . $spot_id), 'access_token' => $token)); ?>">Cancel</a>
    <?php else: ?>
        <a class="cancel button" href="<?php echo base_url('landing/logout?url=' . $customs. '&places=' . $spot_id); ?>">Cancel</a>
    <?php endif; ?>
    <!-- 
<a class="cancel button" href="<?php #echo $this->facebook->getLogoutUrl(array('next' => site_url('logout'), 'access_token' => $token));   ?>">Cancel</a> <div id="clear">
            <button name="clear" type="clear" class="clearButton">Clear Signature</button>
      </div> -->
<div style="clear:both;"></div>
</p>

</form>

