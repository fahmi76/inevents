<html>
    <head>
        <title>Photo Booth | <?php echo $title; ?></title>
        <link href="<?php echo base_url() . "assets/css/phototagging.css"; ?>" type="text/css" rel="stylesheet" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery-1.4.4.min.js"></script>
        <?php
        if(isset($_POST['rfid'])){
            if($error == 1){
                echo '<script type="text/javascript">';
                echo "var errormsg = ".$error.";";
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo "var errormsg ='';";
                echo '</script>';
            }
        }
        ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#rfid").focus();                

                $("#input").submit(function(){
                    var urlinput = $(this).attr('action');
                    var rfid = $("#rfid").val();

                    $.ajax({
                        type: "post",
                        dataType: "html",
                        cache: false,
                        url: urlinput,
                        data: 'rfid='+rfid,
                        beforeSend: function(xmlHttpRequest){
                            $("#loading").show();
                        },
                        complete: function(xmlHttpRequest, status){
                            $("#loading").hide();
                            $("#form").hide();
                        },
                        success: function (response) {
                            $("#container").html(response);
                            if(rfid!="" && errormsg==0){
                                $("#flash").fadeIn(1000).fadeTo("slow");
                            } else {
                                setInterval(function(){
                                    window.location.reload();
                                },2000);
                            }
                        }
                    });
                    return false;
                });

                $("#imgfirst").css("display","none").fadeIn(1000).fadeTo("slow");

                $("#tagme").click(function(){
                    var urltagme = $(this).attr('href');
                    $("#container").load(urltagme,function(){
                        $("#showrfid").fadeIn(1000).fadeTo("slow");
                    });
                    return false;
                });
                
                $("#finish").click(function(){
                    var urlfinish = $(this).attr('href');
                    $("#photo").hide();
                    $("#tag-welc2").hide();
                    $("#button").hide();
                    $("#loadtxt").fadeIn(1000).fadeTo("slow");

                    $("#thank").load(urlfinish,function(){
                        $("#thank").fadeIn(1000).fadeTo("slow");
                        $("#loadtxt").hide();
                        
                        setInterval(function(){
                            window.location.href='<?php echo base_url(); ?>index.php/photo';
                        },1000);
                        
                    });
                    
                    return false;
                });
            });
        </script>
    </head>
    <body>
        <div id="container">
            <div id="loadtxt" style="margin: auto; display: none;">
                Saving Photo...<br />
                Please waiting for a moment.
            </div>
            <div id="errormsg">
                <?php
                if(isset($_POST['rfid'])){
                    if(validation_errors () != ""){
                        echo validation_errors('<p class="error">Error! ', '</p>');
                    }
                }
                ?>
            </div>
            <?php if ($this->session->userdata('step') == 0): ?>            
            <div id="form">                
                Please Tap Your<br />Card / Bracelet<br />To Start<br />
                <form id="input" action="<?php echo current_url(); ?>" method="post">
                    <input id="rfid" type="text" name="rfid" value="" />
                    <input type="submit" name="submit" value="submit" />
                </form>                
            </div>
            
            <div id="flash" style="display:none;">
                <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' width="100%" height="100%" type="application/x-shockwave-flash" data="<?php echo base_url(); ?>/assets/photobooth_idbyte.swf">
                    <param name='movie' value="<?php echo base_url(); ?>/assets/photobooth_idbyte.swf">
                    <param name='quality' value="high" />
                    <param name='bgcolor' value='#FFFFFF' />
                    <param name='loop' value="true" />
                    <param name="wmode" value="transparent" />
                    <embed src="<?php echo base_url(); ?>/assets/photobooth_idbyte.swf" quality='high' bgcolor='#FFFFFF' width="100%" height="100%" loop="true" type='application/x-shockwave-flash' wmode="transparent" pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
                    </embed>
                </OBJECT>
            </div>
            <?php endif; ?>

            <div id="loading" style="padding: 2px; margin: 10px 0 0 0; text-align: center; display:none;">
                <img src="<?php echo base_url(); ?>assets/images/ajax-point.gif" alt="" border="0" />
            </div>

            <?php if ($this->session->userdata('step') == 1): ?>
            <div id="photo">
                <img id="tagsource" src="<?php echo base_url();?>uploads/photobooth/<?php echo $this->session->userdata('img_temp'); ?>" alt="" />
            </div>
            
            <div id="tag-welc2">
                Photo Uploaded, What Do You Want To Do Next?
            </div>
            
            <div id="button">
                <div id="done">
                    <a id="finish" href="<?php echo base_url();?>photo/publish">Finish</a>
                </div>
                <div id="tag">
                    <a id="tagme" href="<?php echo base_url();?>photo/rfidtagfirst">Tag Photo</a>
                </div>
                <div style="clear:both"></div>
            </div>
            
            <div id="thank"></div>
            <?php endif; ?>

            <?php if($this->session->userdata('step') == 3) : ?>
            <script type="text/javascript">
                $(document).ready(function(){                    
                    $("#inputtag").submit(function(){
                        var urlinputtag = $(this).attr('action');
                        var rfidtag = $("#rfidtag").val();                        
                        
                        $.ajax({
                            type: "post",
                            dataType: "html",
                            cache: false,
                            url: urlinputtag,
                            data: 'rfidtag='+rfidtag,
                            beforeSend: function(xmlHttpRequest){
                                $("#loading").show();
                            },
                            complete: function(xmlHttpRequest, status){
                                $("#loading").hide();
                                $("#showrfid").hide();
                                $("#imgfirst").hide();
                                $("#tagsource").show();
                            },
                            success: function (response) {
                                $("#rfidtagresult").html(response);
                            }
                        });
                        return false;
                    });
                });
            </script>            
            <div id="photo">
                <img id="tagsource" src="<?php echo base_url();?>uploads/photobooth/<?php echo $this->session->userdata('img_temp'); ?>" alt="" />
            </div>
            
            <div id="showrfid">
                <div id="form2">
                    Please Tap Your<br />Card / Bracelet
                    <form action="<?php echo base_url(); ?>photo/rfidtag" id="inputtag" method="post">
                        <input id="rfidtag" type="text" name="rfidtag" />
                        <input type="submit" name="submit" value="submit" />
                    </form>
                </div>
            </div>

            <div id="rfidtagresult"></div>            
            <?php endif; ?>   
        </div>
    </body>
</html>