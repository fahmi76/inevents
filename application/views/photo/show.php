<div id="ref2">
<a class="refresh" href="<?php echo current_url();?>">refresh</a>
<a class="refresh" href="<?php echo site_url('photo/cancel');?>">cancel</a>
<a class="refresh" href="<?php echo site_url('photo/publish');?>">finish</a>
</div>
<div id="photo">
	<img id="tagsource" src="<?php echo base_url();?>uploads/photobooth/<?php echo $img_temp; ?>" alt="" />
</div>
<div id="loading"><img src="<?php echo base_url();?>assets/images/ajax-loader.gif" alt="" width="220" height="19" /></div>

<div id="showtag">    

    <?php if(isset($hasil) && $hasil == 1) : ?>
    <div id="tag-welc2">
        Your Photo has been saved, but we can't uploaded to your facebook account due to expired session, please reconnect your facebook account with wooz.in so we can upload your photo automatically.
    </div>

    <?php elseif(isset($hasil) && $hasil == 2) : ?>
    <div id="tag-welc2">
        Your Photo has been saved, but we can't uploaded to your facebook account due to changed password notice, please reconnect your facebook account with wooz.in so we can upload your photo automatically.
    </div>

    <?php elseif(isset($hasil) && $hasil == 3) : ?>
    <div id="tag-welc2">
        Your Photo has been saved, but we can't uploaded to your facebook account due to unauthorized notice, please reconnect your facebook account with wooz.in so we can upload your photo automatically.
    </div>

    <?php elseif(isset($hasil) && $hasil == 4) : ?>
    <div id="tag-welc2">
        Your Photo has been saved, but we can't uploaded to your facebook account due to invalid session, please reconnect your facebook account with wooz.in so we can upload your photo automatically.
    </div>

    <?php elseif(isset($hasil) && $hasil == 5) : ?>
    <div id="tag-welc2">
        Your Photo has been saved, but we can't uploaded to your facebook account due to connection time, please retry to take a photo.
    </div>

	<?php elseif($this->session->userdata('status_rfid') == 0) : ?>
    <div id="tag-welc2">
        Photo uploaded. What do you want to do now?
    </div>
    <div id="button">
        <div id="done">
            <a id="finish" href="<?php echo site_url('photo/publish');?>">Finish</a> 
        </div>
        <div id="tag">
            <a id="tagme" href="<?php echo site_url('photo/rfidtag');?>">Tag photo</a>
        </div>
        <div style="clear:both"></div>
    </div>

	<?php else : ?>

	<?php $row = $this->db->get_where('account',array('id' => $name))->row(); ?>

	<?php if($img_status == 1 && $people == 1) :?>

	<div id="tag-welc2">
        Hello <?php echo $row->account_displayname; ?>, 
        You're already tagged in this photo. What next?
    </div>
    <div id="button">
        <div id="done">
            <a id="finish" href="<?php echo site_url('photo/publish');?>">Finish</a> 
        </div>
        <div id="tag">
            <a id="tagme" href="<?php echo site_url('photo/rfidtag');?>">Tag photo</a>
        </div>
        <div style="clear:both"></div>
    </div>

	<?php else: ?>
     <script type="text/javascript">
		$(document).ready(function(){
			$("#button").hide();
		});
		</script>
    <div id="tag-welc">
		Hello <?php echo $row->account_displayname; ?><br />
		Which one are you?
	</div>
    <?php endif; ?>
    
    <div id="tag-list">
		<?php
        if($dataphoto!='') :
        foreach($dataphoto as $detail) :
        $user = $this->db->get_where('account',array('id' => $detail->account_id))->row();
        ?>
        <div id="hotspot-<?php echo $detail->tagCounter; ?>" class="hotspot" style="left:<?php echo ($detail->targetX + 190); ?>px; top:<?php echo ($detail->targetY + 60); ?>px;"><span><?php echo $user->account_displayname; ?></span></div>
        <?php
		endforeach;
		endif;
		?>
    </div>
	<?php endif; ?>
</div>
<div id="showrfid">
	<div id="form2">
		<form id="inputtag" method="post">
			<input id="rfid" type="text" name="rfid" value="" />
			<input  type="submit" name="submit" value="submit" />
		</form>
		<p class="error"></p>
		Please tap your<br />wristband on the reader
	</div>
</div>