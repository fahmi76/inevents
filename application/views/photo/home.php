<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Photo Booth | <?php echo $title; ?></title>
        <link href="<?php echo base_url()."assets/css/phototagging.css";?>" type="text/css" rel="stylesheet" />
        <script src="<?php echo base_url()."assets/javascripts/jquery-1.5.1.min.js";?>" type="text/javascript"></script>
        <?php if(!empty($js)) :?>
        <script src="<?php echo base_url()."assets/javascripts/".$js;?>" type="text/javascript"></script>
        <?php endif; ?>
        <?php if(!empty($js2)) :?>
        <script src="<?php echo base_url()."assets/javascripts/".$js2;?>" type="text/javascript"></script>
        <?php endif; ?>
        <script type="text/javascript">
            site = "<?php echo site_url('photo/phototagjax');?>";
<?php if(!empty($name)) : ?>
            name = "<?php echo $name;?>";
<?php endif; ?>
            urlrfid = "<?php echo site_url('photo/rfidtag');?>";
            urltag = "<?php echo site_url('photo/phototag');?>";
        </script>
    </head>
    <body>
        <div id="container">
            <?php echo $body;?>
        </div>
    </body>
</html>