<!DOCTYPE html>
<html>
    <head>
        <title><?php if (isset($title) && $title != '') echo $title . ' - '; echo $main_title; ?></title>
        <meta name="description" content="wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity" />
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">      
        <?php if(isset($fbdatapage)): ?>        
            <!-- for Facebook -->          
            <meta property="fb:app_id" content="147429305270482" />
            <meta property="og:title" content="<?php echo $fbdatapage['title']; ?>" />
            <meta property="og:type" content="article" />
            <meta property="og:image" content="<?php echo $fbdatapage['image']; ?>" />
            <meta property="og:url" content="<?php echo $fbdatapage['url']; ?>" />
            <meta property="og:description" content="<?php echo $fbdatapage['desc']; ?>" />
        <?php endif; ?>
        <!-- Bootstrap -->
        <link href="<?php echo $assets_url; ?>/landing/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $assets_url; ?>/landing/fontawesome/css/font-awesome.min.css">
        
        <link rel="stylesheet" type="text/css" href="<?php echo $assets_url; ?>/landing/css/main-style.css">

        <script src="<?php echo $assets_url; ?>/landing/js/jquery-2.0.3.min.js"></script>

        <!-- Custom colors based on company or product color scheme-->
        <link rel="stylesheet" type="text/css" href="<?php echo $assets_url; ?>/landing/css/custom-style.css">

        <script type="text/javascript">
            urlhome = "<?php echo $assets_url; ?>/landing/";
        </script>
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <?php if ($background != ''): ?>
            <STYLE TYPE="text/css">
                body {
                    background-image: url(<?php echo base_url(); ?><?php echo $background; ?>);
                }

                <?php
                if (isset($css) && $css != '') {
                    echo $css;
                }
                ?>
            </STYLE>
        <?php else: ?>
            <STYLE TYPE="text/css">
                body {
                    background-image: url(<?php echo $assets_url; ?>/landing/images/bg.jpg);
                }
            </STYLE>
        <?php endif; ?>
    </head>
    <body>
        <div id="fb-root"></div>
        <div id="wrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 center-block">
                        <!-- Product logo -->
                        <h1 class="logo text-center">   
                            <?php if ($logo != ''): ?>
                                <img src="<?php echo base_url(); ?><?php echo $logo; ?>" alt="<?php echo $main_title; ?>">
                            <?php endif; ?>
                        </h1>
                        <!-- Product tagline. Add class: show/hide -->
                        <div class="header text-center hide"><img src="<?php echo $assets_url; ?>/landing/images/header.png" alt="Welcome to the Pleasureable Magnum VVIP Experience"></div>

                        <div class="box">
                            <?php echo $TContent; ?>
                        </div>
                    </div>  
                </div>
            </div>

        </div>
        <?php
        $app_id = $this->config->item('facebook_application_id');
        ?>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=<?php echo $app_id; ?>";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo $assets_url; ?>/landing/js/jquery-2.0.3.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo $assets_url; ?>/landing/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo $assets_url; ?>/landing/js/screen.js"></script>

    </body>
</html>