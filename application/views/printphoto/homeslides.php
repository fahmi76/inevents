<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Photo Gallery</title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
                <meta name="description" content="Wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity on Johnnie Walker Jet Black Party 2011 Singapore" />
       		<link rel="shortcut icon" href="../favicon.ico"> 
                <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/slide/demo.css";?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/slide/styleslide.css";?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/slide/elastislide.css";?>" />
        
		<script type="text/javascript" src="<?php echo base_url()."assets/javascripts/jquery-1.7.1.min.js";?>"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/slide/jquery.timers-1.0.0.js";?>"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/slide/jquery.tmpl.min.js"?>"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/slide/jquery.easing.1.3.js"?>"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/slide/jquery.elastislide.js"?>"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/slide/gallery.js"?>"></script>
		<style type="text/css">
			body {
				background: none repeat scroll 0 0 #000000;
				color: #101010;
			}
			a {
				color:#000;
				text-decoration: underline; 
			}
			a:hover {
				color:#555; 
				text-decoration: none;
			}
			#container {
				border: none;
				margin: 0 0;
				background: #FFF  repeat-y left top;
			}
			#section-top1 {
				text-indent:0;
                                text-align: center;
                                padding-left: 440px;
                                position: fixed;
			}
			#section-side {
				text-indent:0;
			}
			#main-box {
				background: #FFF  0 0 no-repeat;
				text-shadow: 0px 1px 2px rgba(255, 255, 255, 1);
			}
			#main-box .text-box {
				background: rgba(255, 255, 255, 0.7);
				padding: 10px;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
				border-radius: 5px;
			}
			#footer {
				color: #fff;
				text-shadow: none;
			}
			#footer a {
				color: #fff;
				text-shadow: none;
			}
			#txt_footer {
				font-family: Helvetica, Arial, sans-serif;
				font-size: 12px;
				color: #FFF;	
			}
			.error { color: #fff; text-shadow: none; background: rgba(255, 0, 0, 0.5); border: 1px dashed #fff; }
.publish1 {
	-moz-box-shadow:inset 0px 1px 0px 0px #918891;
	-webkit-box-shadow:inset 0px 1px 0px 0px #918891;
	box-shadow:inset 0px 1px 0px 0px #918891;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #c2bcc2), color-stop(1, #827b82) );
	background:-moz-linear-gradient( center top, #c2bcc2 5%, #827b82 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#c2bcc2', endColorstr='#827b82');
	background-color:#c2bcc2;
	-moz-border-radius:8px;
	-webkit-border-radius:8px;
	border-radius:8px;
	border:1px solid #7d7d7d;
	display:inline-block;
	color:#000;
	font-family:Courier New;
	font-size:25px;
	font-weight:bold;
	padding: 20px 100px;
	text-decoration:none;
	text-shadow:1px 1px 0px #787478;
        margin-top: 50px;
}.publish1:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #827b82), color-stop(1, #c2bcc2) );
	background:-moz-linear-gradient( center top, #827b82 5%, #c2bcc2 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#827b82', endColorstr='#c2bcc2');
	background-color:#827b82;
}.publish1:active {
	position:relative;
	top:1px;
}         
                        
		</style>                
		<noscript>
			<style>
				.es-carousel ul{
					display:block;
				}
			</style>
		</noscript>
                
		<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper">
				{{if itemsCount > 1}}
					<div class="rg-image-nav">
						<a href="#" class="rg-image-nav-prev">Previous Image</a>
						<a href="#" class="rg-image-nav-next">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-image"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
		</script>
           
    </head>
    <body class="refresh">

<?php echo $TContent;?>
    </body>
</html>