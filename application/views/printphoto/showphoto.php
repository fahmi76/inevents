<script type="text/javascript">
    var name = '<?php echo $name; ?>';
    var urltag = '<?php echo base_url(); ?>index.php/photo/phototag';
</script>
<script src="<?php echo base_url() . "assets/javascripts/phototagging.js"; ?>" type="text/javascript"></script>

<?php
if (validation_errors ()) {
    echo validation_errors('<p class="error">Error! ', '</p>');
    echo '<script type="text/javascript">';
    echo 'setInterval(function(){window.location.reload();},2000);';
    echo '</script>';
} else {
?>
<div id="loadings" style="padding: 2px; margin: 10px 0 0 0; text-align: center; display:none;">
    <img src="<?php echo base_url(); ?>assets/images/ajax-point.gif" alt="" border="0" />
</div>
<div id="tag-welc">
    Hello <?php echo $name; ?><br />
    Which One Are You?
</div>
<div id="tagresult"></div>
<?php
}
?>