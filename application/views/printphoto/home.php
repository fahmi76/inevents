<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Photo Booth | <?php echo $title; ?></title>
        <link rel="stylesheet" media="all" href="style.css"/>
        <link href="<?php echo base_url()."assets/css/phototagging.css";?>" type="text/css" rel="stylesheet" />
        <script src="<?php echo base_url()."assets/javascripts/jquery-1.5.1.min.js";?>" type="text/javascript"></script>
        <?php if(!empty($js)) :?>
        <script src="<?php echo base_url()."assets/javascripts/".$js;?>" type="text/javascript"></script>
        <?php endif; ?>
        <?php if(!empty($js2)) :?>
        <script src="<?php echo base_url()."assets/javascripts/".$js2;?>" type="text/javascript"></script>
        <?php endif; ?>
        <?php if(!empty($js3)) :?>
        <script src="<?php echo $js3; ?>" type="text/javascript"></script>
        <?php endif; ?>
        <script type="text/javascript">
            site = "<?php echo site_url('photo/phototagjax');?>";
<?php if(!empty($name)) : ?>
            name = "<?php echo $name;?>";
<?php endif; ?>
            urlrfid = "<?php echo site_url('photo/rfidtag');?>";
            urltag = "<?php echo site_url('photo/phototag');?>";
        </script>
		<style type="text/css">
			body {
				background: #f0f1f5 url('bg_repeat.jpg') repeat-y left top;
				color: #000;
			}
			a {
				color:#000;
				text-decoration: underline; 
			}
			a:hover {
				color:#555; 
				text-decoration: none;
			}
			#container {
				border: none;
				margin: 0 0;
				background: #f0f1f5 url('bg_repeat.jpg') repeat-y left top;
			}
			#section-top1 {
				text-indent:0;
			}
			#section-side {
				text-indent:0;
			}
			#main-box {
				background: transparent url(main_content_bg.jpg) 0 0 no-repeat;
				text-shadow: 0px 1px 2px rgba(255, 255, 255, 1);
			}
			#main-box .text-box {
				background: rgba(255, 255, 255, 0.7);
				padding: 10px;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
				border-radius: 5px;
			}
			#footer {
				color: #fff;
				text-shadow: none;
			}
			#footer a {
				color: #fff;
				text-shadow: none;
			}
			#txt_footer {
				font-family: Helvetica, Arial, sans-serif;
				font-size: 12px;
				color: #FFF;	
			}
			.error { color: #fff; text-shadow: none; background: rgba(255, 0, 0, 0.5); border: 1px dashed #fff; }      
		</style>          
    </head>
    <body>
        <div id="container">
            <?php echo $body;?>
        </div>
    </body>
</html>

