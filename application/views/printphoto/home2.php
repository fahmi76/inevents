<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8"/>
        <title>Home - Wooz.in</title>
        <meta name="description" content="Wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity on Johnnie Walker Jet Black Party 2011 Singapore" />
        <link rel="stylesheet" media="all" href="<?php echo base_url()."uploads/apps/mercedesbenz/stylephoto.css";?>"/>
        <script src="<?php echo base_url()."assets/javascripts/jquery-1.7.1.min.js";?>" type="text/javascript"></script>
        
		<style type="text/css">
			body {
				background: #f0f1f5 url('http://wooz.local/uploads/apps/mercedesbenz/bg_repeat.jpg') repeat-y left top;
				color: #000;
			}
			a {
				color:#000;
				text-decoration: underline; 
			}
			a:hover {
				color:#555; 
				text-decoration: none;
			}
			#container {
				border: none;
				margin: 0 0;
				background: #f0f1f5 url('http://wooz.local/uploads/apps/mercedesbenz/bg_repeat.jpg') repeat-y left top;
			}
			#section-top {
				text-indent:0;
			}
			#section-side {
				text-indent:0;
			}
			#main-box {
				background: transparent url() 0 0 no-repeat;
				text-shadow: 0px 1px 2px rgba(255, 255, 255, 1);
			}
			#main-box .text-box {
				background: rgba(255, 255, 255, 0.7);
				padding: 10px;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
				border-radius: 5px;
				overflow: hidden;
			}
			#footer {
				color: #fff;
				text-shadow: none;
			}
			#footer a {
				color: #fff;
				text-shadow: none;
			}
			#txt_footer {
				font-family: Helvetica, Arial, sans-serif;
				font-size: 12px;
				color: #FFF;	
			}
			.error { color: #000; text-shadow: none; background: rgba(51, 51, 51, 0.5); border: 1px dashed #fff; font-family: Helvetica, Arial, sans-serif;	font-size: 30px;text-align: center;}
		</style>
    </head>

    <body>

	<div id="container">

		<div id="header">
			<div id="section-top">
				<img src="http://wooz.local/uploads/apps/mercedesbenz/header_bg.jpg" alt="Mercedes-Benz" width="900" height="200" />
			</div>
		</div>
		<div id="main-box">
        	<div class="text-box">
                <?php echo $body;?>
            </div>    
        	<div id="footer">
		</div>
		</div>

		<div class="clear"></div>
        
        
	</div>
</body>
</html>