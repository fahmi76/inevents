<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<title>Galleriffic | Thumbnail rollover effects and slideshow crossfades</title>
		<link rel="stylesheet" href="<?php echo base_url()."assets/css/basic.css";?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo base_url()."assets/css/galleriffic-2.css";?>" type="text/css" />
		<script type="text/javascript" src="<?php echo base_url()."assets/js/jquery-1.3.2.js";?>"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/js/jquery.galleriffic.js";?>"></script>
		<script type="text/javascript" src="<?php echo base_url()."assets/js/jquery.opacityrollover.js";?>"></script>
		<!-- We only want the thunbnails to display when javascript is disabled -->
		<script type="text/javascript">
			document.write('<style>.noscript { display: none; }</style>');
		</script>
                <style type="text/css">
                .lbl_sold {
                        background: url('http://wooz.local/uploads/photoframe/mercedesbenz.png') top left no-repeat;
                        width: 640px;
                        height: 480px;
                        position: absolute;
                        z-index: 9999;
                        top: 31px; left: 4px;

                }
                </style>
	</head>
	<body>
		<div id="page">
			<div id="container">
				<h1><a href="index.html">Galleri foto</a></h1>
				<h2>Silahkan pilih photo anda</h2>

				<!-- Start Advanced Gallery Html Containers -->
				<div id="gallery" class="content">
					<div id="controls" class="controls"></div>
					<div class="slideshow-container">
						<div id="loading" class="loader"></div>
						<div id="slideshow" class="slideshow">
                                                    <div class="lbl_sold">
                                                    </div>
                                                </div>
					</div>
					<div id="caption" class="caption-container"></div>
				</div>
				<div id="thumbs" class="navigation">
					<ul class="thumbs noscript">
                                                <?php $y = 1; 
                                                foreach($body as $row): ?>
						<li>
							<a class="thumb" name="leaf" href="<?php echo base_url(); ?>uploads/photobooth/<?php echo $row; ?>" title="Title #0">
								<img src="<?php echo base_url(); ?>resizer/avatar.php?w=75&img=<?php echo base_url();?>uploads/photobooth/<?php echo $row; ?>" alt="Title #0" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="<?php echo base_url();?>printphoto/check/<?php echo $row; ?>">Publish</a>
								</div>
                                                                <!--
								<div class="image-title">Title #0</div>
								<div class="image-desc">Description</div>
                                                                -->
							</div>
						</li>
                                                <?php endforeach; ?>
					</ul>
				</div>
				<div style="clear: both;"></div>
			</div>
		</div>
		<div id="footer"></div>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '300px', 'float' : 'left'});
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     2500,
					numThumbs:                 15,
					preloadAhead:              10,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            7,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Slideshow',
					pauseLinkText:             'Pause Slideshow',
					prevLinkText:              '&lsaquo; Previous Photo',
					nextLinkText:              'Next Photo &rsaquo;',
					nextPageLinkText:          'Next &rsaquo;',
					prevPageLinkText:          '&lsaquo; Prev',
					enableHistory:             false,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});
			});
		</script>
	</body>
</html>