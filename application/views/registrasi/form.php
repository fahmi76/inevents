<script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>registrasi/search/1?url=<?php echo $customs; ?>",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                    }
        });
    });

    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputEmail").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>registrasi/search/2?url=<?php echo $customs; ?>",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                    }
        });
    });

    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputTelp").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>registrasi/search/3?url=<?php echo $customs; ?>",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                    }
        });
    });

</script>
<?php if (validation_errors()) : ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', ''); ?>
    </p>
<?php endif; ?> 
<br />
<form method="post" action="<?php echo current_url(); ?>?url=<?php echo $customs; ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Fill your name</label>       
        <input type="text" class="form-control input-lg" name="name" id="InputName" value=""  placeholder="name">
    </div>
    <div class="form-group">
        <label for="InputName">or Fill your email</label>       
        <input type="text" class="form-control input-lg" name="email" id="InputEmail" value=""  placeholder="email@email.com">
    </div>
    <div class="form-group">
        <label for="InputName">or Fill your phone</label>       
        <input type="text" class="form-control input-lg" name="telp" id="InputTelp" value=""  placeholder="82323232">
    </div>

    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        <a href="<?php echo site_url('landing/home?url=tresemmesby') ?>">
            <button type="button" class="btn btn-default btn-xlg">New Register</button>
        </a>
    </div>
</form>