<h2>Hi, <?php echo $data->account_displayname; ?></h2>
<hr>
<div class="text-center">
    <p>Share text on social media: </p>
    <p style="font-size: 30px;"><strong><?php echo $places->places_cstatus_tw; ?></strong></p>	
    <br />
    <!--
    -->
    <?php if ($data->account_fbid): ?>
        <?php
        $redirect3 = base_url() . 'home/facebook/tresemmeshare/' . $customs . '/' . $acc_id . '/' . $spot_id . '/' . $acc__new_id;
        $redirect = "https://www.facebook.com/dialog/oauth?client_id=" . $this->config->item('facebook_application_id') . "&redirect_uri=" . $redirect3 . "&scope=publish_stream,publish_actions";
        ?>
        <a class="reg-done" href="<?php echo $redirect; ?>">
        <?php else: ?>
            <a class="reg-done" href="<?php echo site_url('registrasi/share/'.$acc_id.'/'.$spot_id.'/'.$acc__new_id.'?share=1&url='.$customs); ?>">
            <?php endif; ?>
            <button type="button" class="btn btn-primary btn-xlg">Share</button>
        </a>
        <a class="reg-done" href="<?php echo site_url('registrasi/share/'.$acc_id.'/'.$spot_id.'/'.$acc__new_id.'?share=0&url='.$customs); ?>">
            <button type="button" class="btn btn-primary btn-xlg">No, thanks</button>
        </a>
</div>