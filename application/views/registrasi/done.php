<h2>Thank you</h2>
<hr>
<div class="text-center">
    <p>The registration process has been completed.</p>
    <br>
    <button type="button" class="btn btn-primary btn-xlg">Finish</button>
</div>

<?php if ($tokenfb): ?>
    <meta http-equiv="refresh" content="2;URL=<?php echo $this->facebook->getLogoutUrl(array('next' => site_url('registrasi/logout/'.$acc_id.'/' . $spot_id.'/'.$acc__new_id.'?url='.$customs), 'access_token' => $tokenfb)); ?>" />
<?php else: ?>
    <meta http-equiv="refresh" content="2;URL=<?php echo site_url('registrasi/logout/'.$acc_id.'/' . $spot_id.'/'.$acc__new_id.'?url='.$customs); ?>" />
<?php endif; ?>
