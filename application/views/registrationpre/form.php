<script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>registrationpre/search/1",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id); 
                        $("#Inputcompany").val(ui.item.company); 
                        $("#Inputemployee").val(ui.item.employee); 
                        $("#Inputdepartment").val(ui.item.department);  
                    }
        });
    });

    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputEmail").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>registrationpre/search/2",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                    }
        });
    });
    
</script>
<?php if (validation_errors()) : ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', ''); ?>
    </p>
<?php endif; ?> 
<br />
<form method="post" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Fill your name</label>       
        <input type="text" class="form-control input-lg" name="name" id="InputName" value=""  placeholder="name">
    </div>
    <input type="hidden" class="form-control input-lg" name="id" id="InputId" value=""  placeholder="">

    <div class="form-group">
        <label for="InputName">Company</label>       
        <input type="text" class="form-control input-lg" name="company" id="Inputcompany" value=""  placeholder="company" disabled="disabled">
    </div>
    <div class="form-group">
        <label for="InputName">Employee Class</label>       
        <input type="text" class="form-control input-lg" name="employee" id="Inputemployee" value=""  placeholder="employee" disabled="disabled">
    </div>
    <div class="form-group">
        <label for="InputName">Department</label>       
        <input type="text" class="form-control input-lg" name="department" id="Inputdepartment" value=""  placeholder="department" disabled="disabled">
    </div>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
    </div>
</form>