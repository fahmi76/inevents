<h2>Hi, <?php echo $info->account_displayname; ?></h2>
<hr>
<div class="text-center">
    <?php if ($info->account_fbid == '' && $info->account_token == ''): ?>
        <p>Also share your activity through your Facebook account?</p>  
        <a class="fb-connect" href="<?php echo $login; ?>">
            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
    <?php else: ?>
        <div class="form-group">
            <label for="InputName">Facebook Account Name</label> 
            <?php
            $sosmednames = $info->account_fb_name;
            ?>
            <input type="text" class="form-control input-lg" id="sosmed" name="sosmed" value="<?php echo $sosmednames; ?>" disabled />
        </div>
    <?php endif; ?>      
    <?php if ($info->account_tw_token == '' && $info->account_tw_secret == ''): ?>
        <p>Also share your activity through your Twitter account?</p>
        <a class="tw-connect" href="<?php echo base_url(); ?>registrationpre/twitter/<?php echo $id ?>">
            <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
    <?php endif; ?>   
    <p>or finish right here.</p> 
    <a class="reg-done" href="<?php echo site_url('registrationpre/share/'.$id); ?>">
        <button type="button" class="btn btn-primary btn-xlg">Done</button>
    </a>
</div>