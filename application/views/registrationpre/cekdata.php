<script type="text/javascript">
    $(document).ready(function () {
        $("#InputName").focus();
        $("#registration").submit(function () {
            $('.action').html('<img src="http://woozin.tgrid.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<div class="panel panel-info">
    <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
            <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>

</div>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Full Name</label>        
        <?php
        $name = set_value('fullname');
        if ($name) {
            $names = $name;
        } elseif (!$info) {
            $names = '';
        } else {
            $names = $info->account_displayname;
        }
        ?>
        <input type="text" class="form-control input-lg" name="fullname" id="InputName" autocomplete="off" value="<?php echo $names; ?>" placeholder="your name">
    </div>
    <div class="form-group">
        <label for="InputEmail">Email Address</label>
        <?php
        $email = set_value('email');
        if ($email) {
            $emails = $email;
        } elseif (!$info) {
            $emails = '';
        } else {
            $emails = $info->account_email;
        }
        ?>
        <input type="email" class="form-control input-lg" name="email" id="InputEmail" autocomplete="off" value="<?php echo $emails; ?>" placeholder="youremail@youremaildomain.com">
    </div>
    <div class="form-group">
        <label for="InputPhotoType">Photo Type</label>
        <br>
        <?php
        $gender = set_value('gender');
        if ($gender) {
            $genders = $gender;
        } elseif (!$info) {
            $genders = '';
        } else {
            $genders = $info->account_avatar_type;
        }
        ?>
        <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-warning <?php if ($genders == "naughty") : ?>active<?php endif; ?>">
                <input type="radio" name="gender" id="InputGenderNaughty" value="naughty" <?php if ($genders == "naughty") : ?>checked="checked"<?php endif; ?>> Naughty
            </label>
            <label class="btn btn-warning <?php if ($genders == "nice") : ?>active<?php endif; ?>">
                <input type="radio" name="gender" id="InputGenderNice" value="nice" <?php if ($genders == "nice") : ?>checked="checked"<?php endif; ?>> Nice
            </label>
        </div>
    </div> 
    <div class="form-group">
        <label for="InputPhoto">Photo</label>    <br />       
        <?php if ($info->account_avatar): ?>
            <img src="<?php echo base_url(); ?>resizer/thumbimage.php?img=user/<?php echo $info->account_avatar; ?>" alt="" />
        <?php endif; ?><br /> 
        <input type="file" class="form-control input-lg" name="photo" id="InputEmail">
    </div>
    <div class="form-group">
        <label for="InputRFID">RFID</label>
        <?php
        $rfid = set_value('rfid');
        if ($rfid) {
            $rfids = $rfid;
        } elseif (!$info) {
            $rfids = '';
        } else {
            $rfids = $info->account_rfid;
        }
        ?>
        <input type="text" class="form-control input-lg" name="rfid" id="InputRFID" autocomplete="off" value="<?php echo $rfids; ?>" placeholder="RFID Number">
    </div>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        <?php if (isset($token) && $token != ''): ?>
            <a href="<?php echo $this->facebook->getLogoutUrl(array('next' => site_url('combirun/logout'), 'access_token' => $token)); ?>">
                <button type="button" class="btn btn-default btn-xlg">Cancel</button>
            </a>
        <?php else: ?>
            <a href="<?php echo base_url('landing/logout?url=' . $customs . '&places=' . $spot_id); ?>">
                <button type="button" class="btn btn-default btn-xlg">Cancel</button>
            </a>
        <?php endif; ?>
    </div>
</form>