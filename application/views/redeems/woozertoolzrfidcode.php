<script type="text/javascript">
		$(document).ready(function(){
			$('#rfidcode').focus();
		});
</script>
<?php
        if($msgcode != '' && $msgcode == 1) { ?>
			<script type="text/javascript">
					$(document).ready(function(){
						$('#lspro').hide();
					});
			</script>
  <?php  } ?>


<form action="<?php echo current_url(); ?>" method="post">
    <div id="mform">
        <h1><font size=6>User Information</font></h1>
        <p>Tap a Bracelet for saving RFID number.</p>
        <div class="garis"></div>
        <?php
        if (validation_errors ())
            echo validation_errors('<div id="merror">', '</div>');

        if($msgcode != '' && $msgcode == 1) {
            echo '<div id="merror">Woozer Information has been saved.</div>';
            echo "<meta http-equiv=\"refresh\" content=\"2;url=".site_url('tool/code')."\">";
        }
        ?>
        <div id="lspro">
            <ul style="list-style: none;">
                <li style="border-bottom: #ccc solid 1px;">
                    <div id="setpro">
                        <p><font size=6>
                            Full Name : <?php echo $name; ?><br />
                            Email : <?php echo $email; ?>
                            <?php if($location != ''): ?>
                            <br />School : <?php echo $location; ?>
                            <?php endif; ?>
							</font>
                        </p>
                    </div>
                </li>
				<?php if(isset($data) && $data == 1): ?>
                <li>
                    <p>
                        RFID<br />
                        <input id="rfidcode" type="text" name="rfidcode" class="txt" />
                    </p>
                    <p>
                        <input type="submit" value="save" class="sub" />
                    </p>
                    <p>
                        <input type="submit" value="cancel" class="sub" />
                    </p>
                </li>
				<?php else: ?>
                <li>
                    <p>
                        <font size=6>Your Bracelet RFID number is <font size=6 color='green'><?php if($rfid != '') echo $rfid; ?></font> </font><br /> 
                    </p>
                    <p>
                        <font size=6>Last Update on <font size=6 color='green'><?php if($lastupdate != '') echo $lastupdate; ?></font> </font><br /> 
                    </p>
                </li>
				<?php endif; ?>
            </ul>
        </div>
        <div id="lspro1">
            <ul style="list-style: none;">
                <li style="border-bottom: #ccc solid 1px;">				
                </li>
				    <p>
                        <a href="<?php echo base_url(); ?>tool/code"><font size=5 color='red'>Back to home</font></a>
                    </p>	
            </ul>
        </div>
    </div>
</form>