<form action="<?php echo current_url(); ?>" method="post">
    <div id="mform">
        <h1>Login</h1>
        <p>Type in your email and password and you are ready to go.</p>
        <div class="garis"></div>
        <?php
        if (validation_errors ())
            echo validation_errors('<div id="merror">', '</div>');
        ?>
        <p>
            Email<br />
            <input type="text" name="email" class="txt" />
        </p>
        <p>
            Password<br />
            <input type="password" name="pass" class="txt" />
        </p>
        <p>
            <input type="submit" value="login" class="sub" />
        </p>
    </div>
</form>