<div id="mform">
    <h1>User Management</h1>
    <p>
    </p>
    <div class="garis"></div>

    <a href="<?php echo site_url('tool');?>">[ back to main page ]</a>
    <div class="garisgrey"></div>
    <div id="reminfo">
        <div id="remnotif">
            <?php if(isset($_GET['removed']) && $_GET['removed'] == 1): ?>
            <p>You have successfully deleted the data.</p>
            <meta http-equiv="refresh" content="2;url=<?php echo site_url('');?>tool" />
            <?php else : ?>
            <p>Are you sure to delete this woozer account?</p>
            <p>
                <a href="<?php echo site_url('tool/remove/'.$dwoozer->id.'?removed=1');?>" id="woozeremoved" name="remove">yes</a>&nbsp;&nbsp;
                <a href="<?php echo site_url('tool');?>" name="cancel">cancel</a>
            </p>
            <?php endif; ?>
            <div class="garisgrey"></div>
        </div>

        <h2>Woozer Information</h2>
        <p>Full Name : <?php echo $dwoozer->account_displayname; ?></p>
        <p>Email : <?php echo $dwoozer->account_email; ?></p>
    </div>
    <div class="garisgrey"></div>
    <a href="<?php echo site_url('');?>tool">[ back to main page ]</a>
</div>