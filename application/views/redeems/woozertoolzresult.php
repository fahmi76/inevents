<div id="mform">
    <h1>User Management</h1>
    <p>
    </p>
    <div class="garis"></div>

    <a href="<?php echo site_url('redeem');?>">[ back to main page ]</a>
    <div class="garisgrey"></div>
    <h2>User Results</h2>

    <?php if(isset($result) && count($result)!=0): ?>
    <div id="searchresult">
        <ul>
                <?php foreach($result as $lsres): ?>
            <li>
                <p>Full Name : <?php echo $lsres->account_displayname; ?></p>
                <p>Email : <?php echo $lsres->account_email; ?></p>
                <p>Phone Number : <?php echo $lsres->account_phone; ?></p>
				<p>
                    <a id="woozedit" href="<?php echo site_url('redeem/edit/'.$lsres->id);?>" name="wedit">edit</a>
                </p>
            </li>
                <?php endforeach; ?>
        </ul>
    </div>
    <?php else: ?>
    <p>Data Not Found.</p>
    <?php endif; ?>
    <div class="garisgrey"></div>
	                            <?php if( isset($paging) ): ?>
                        <div id="paging">
                                    <?php echo $paging?>
                        </div>
                            <?php endif;?>
    <a href="<?php echo site_url('redeem');?>">[ back to main page ]</a>
</div>