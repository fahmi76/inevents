<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title><?php if(isset($title)&&$title!='') echo $title . ' - '; echo $main_title; ?></title>
        <meta name="description" content="Wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity" />

		<script src="http://mscampfire.wooz.in/assets/javascripts/jquery-1.7.1.min.js"></script> 
        <link rel="stylesheet" media="all" href="http://mscampfire.wooz.in/assets/customs/style.css"/>
		<style type="text/css">
			body {
				background: #000 url('http://mscampfire.wooz.in/assets/customs/background_01.jpg') top center no-repeat;
				color: #fff;
			}
			a {
				color:#656565; 
			}
		</style>
    </head>
    <body>
	<div id="container">

		<div id="header">
			<div id="section-top">
				<a href="<?php echo current_url(); ?>">
				wooz.in | join. connect. woozin'
				</a>
			</div>
		</div>
		<div id="main-box">
			<?php echo $wooztoolz; ?>
		</div>
		<div id="section-side">
			<ul>
			<li>Co-partner:</li>
			<li><img src="http://mscampfire.wooz.in/assets/images/logo_e27.png" alt="E27 - Web Innovation In Asia" /></li>
			<li>Premier Sponsor:</li>
			<li><img src="http://mscampfire.wooz.in/assets/images/logo_nokia.png" alt="nokia - connecting people" /></li>
			<li>Supporting Sponsor:</li>
			<li><img src="http://mscampfire.wooz.in/assets/images/logo_lenovo.jpg" alt="wooz.in" /></li>
			<li>Audience experience partner:</li>
			<li><img src="http://mscampfire.wooz.in/assets/images/logo_woozin.png" alt="wooz.in" /></li>
			</ul>
		</div>
		<div class="clear"></div>
		<div id="footer">
			<a href="http://mscampfire.wooz.in" id="smalllogo">
				<img src="http://mscampfire.wooz.in/assets/customs/smalllogo.png" alt="wooz.in" />
			</a>
			<a href="http://mscampfire.wooz.in/" class="home">Home</a> |
			<a href="http://wooz.in/about">About</a> |
			<a href="http://wooz.in/blog">Blog</a> |
			<a href="http://wooz.in/privacy">Privacy Policy</a> |
			<a href="http://wooz.in/termsofservice">Terms Of Service</a>
			<br />
			Wooz.in is a web based application using RFID Owned by BIRA.<br />
			&copy; 2012 wooz.in (Patent Pending). All rights reserved.
		</div>
		<div class="clear"></div>
	</div>
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId: '<?php echo $this->config->item("facebook_application_id"); ?>',
          cookie: true,
          xfbml: true,
          oauth: true
        });
        FB.Event.subscribe('auth.login', function(response) {
          window.location.reload();
        });
        FB.Event.subscribe('auth.logout', function(response) {
          window.location.href = "<?=site_url('logout')?>";
        });
      };
      (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
          '//connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
      }());
    </script>
	</body>
</html>
