<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title><?php if (isset($title) && $title != '') echo $title; ?> | Wooz.in</title>
        <meta name="description" content="Wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity. " />

		<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico">
		<script type="text/javascript" src="http://use.typekit.com/gbq4aea.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        <!--[if IE]>
                <style type="text/css">
				.timer { display: none !important; }
				div.caption { background:transparent; filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000,endColorstr=#99000000);zoom: 1; }
			</style>
		<![endif]-->
        <!--[if lt IE 9]>
                        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <?php if (isset($page) && $page == 'home') : ?>
        <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/css/main.css"/>
        <?php else : ?>
        <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/css/main2.css"/>
        <?php endif; ?>

        <meta name="viewport" content="initial-scale=1.0, width=device-width, maximum-scale=1.0"/>
        <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/css/popup.css"/>
        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.5.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/javascripts/jquery.datePicker.js"></script>
        <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/css/datePicker.css"/>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-19434220-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </head>
    <body>
    <header>
        <div class="maindiv">
            <div id="topmenu">
                <ul class="home">
                    <li class="Muse30015"><a href="<?php echo base_url(); ?>" class="<?php if ($core == 'home') echo 'topmenuselected'; else echo 'topmenuhref'; ?>">home</a></li>
                    <li class="Muse30015"><a href="<?php echo site_url('about'); ?>" class="<?php if ($core == 'about') echo 'topmenuselected'; else echo 'topmenuhref'; ?>">about</a></li>
                    <li class="Muse30015"><a href="<?php echo base_url(); ?>/blog" class="topmenuhref">blog</a></li>
                    <li class="Muse30015"><a href="<?php echo site_url('woozer'); ?>" class="<?php if ($core == 'woozer') echo 'topmenuselected'; else echo 'topmenuhref'; ?>">woozer</a></li>
                    <li class="Muse30015" <?php if($user==""): ?>style="border-right:0;"<?php endif;?>><a href="<?php echo site_url('woozpot'); ?>" class="<?php if ($core == 'woozpot') echo 'topmenuselected'; else echo 'topmenuhref'; ?>">woozpot</a></li>
                    <?php if ($user): ?>
                    <li class="Muse30015"><a href="<?php echo site_url('profile'); ?>" class="<?php if ($core == 'profile') echo 'topmenuselected'; else echo 'topmenuhref'; ?>">profile</a></li>
                    <li class="Muse30015" style="border-right:0;"><a href="<?php echo $this->facebook->getLogoutUrl(array('next' => site_url('logout'),'redirect_uri' => site_url('logout'))); ?>" onclick="FB.logout();" class="topmenuhref">logout</a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div id="idlogo">
                <a href="<?php echo base_url(); ?>">
                    wooz.in | join. connect. woozin'
                </a>
            </div>
            <div id="hometagline">
                <h1 class="tk-museo">wooz.in.</h1>
                <p class="tk-museo">join. connect. woozin'</p>
            </div>
            <div id="homeright">
                <?php if ($user == ""): ?>
                <div id="btnloc">
                    <a href="<?php echo site_url('login'); ?>" title="Please Sign In">
                        <img src="<?php echo base_url(); ?>assets/images/btnlogin.png" alt="Login" width="71" height="68" border="0">
                    </a> Or
                    <a href="<?php echo site_url('signup'); ?>" class="topmenuhref">sign up</a>
                </div>
                <?php endif; ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </header>

    <?php echo $TContent; ?>

    <footer>
        <div class="maindiv">
            <div id="footer" class="arial12">
                <a href="<?php echo site_url(); ?>" id="smalllogo">
                    <img src="<?php echo base_url(); ?>assets/images/smalllogo.png" alt="wooz.in" />
                </a>
                <a href="<?php echo site_url(); ?>">Home</a> |
                <a href="<?php echo site_url('about'); ?>">About</a> |
                <a href="<?php echo site_url('blog'); ?>">Blog</a> |
                <a href="<?php echo site_url('privacy'); ?>">Privacy Policy</a> |
                <a href="<?php echo site_url('termsofservice'); ?>">Terms Of Service</a>
                <br />
                <span class="Muse30015">&copy; 2010 wooz.in (Patent Pending). All rights reserved. Wooz.in is a web based application using RFID Owned by BIRA.</span>
            </div>
        </div>
    </footer>
</body>
</html>