<h2><?php echo $data->account_displayname; ?></h2>
<hr>    
<div class="text-center">
    <div class="well stripe">
        <span>Redeem point: </span>
        <span class="redeem-point"><?php echo $mypoint; ?></span>
    </div
    <br>
    <button type="button" id="redeem" class="btn btn-primary btn-xlg">Redeem Promo</button>
    <button type="button" id="transaction" class="btn btn-primary btn-xlg">Transaction</button>
    <hr>
    <button type="button" id="selesai" class="btn btn-default btn-xlg">Done</button>
</div>
<script type="text/javascript">
    $(function() {
        $('#redeem').click(function() {
            window.location = '<?php echo site_url('magnum/redeem/promo/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>'
        });
        $('#transaction').click(function() {
            window.location = '<?php echo site_url('magnum/redeem/transaksi/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>'
        });
        $('#selesai').click(function() {
            window.location = '<?php echo site_url('magnum/redeem/finish/' . $loc); ?>'
        });
    });
</script>