<?php if($datastatus == 1): ?>
	<?php if($check == 1): ?>
		<h2 class="text-center" style="color: green;font-weight: bold;font-size: 250%;">ALLOWED TO ENTER</h2>
	<?php else: ?>
		<h2 class="text-center" style="color: green;font-weight: bold;font-size: 250%;">ALLOWED TO EXIT</h2>
	<?php endif; ?>
	<?php if($already == 0): ?>
		<meta http-equiv="refresh" content="2;URL=<?php echo site_url('gate/done/'.$check.'/'.$gate.'/'.$datauser['id']); ?>" />
	<?php else: ?>
		<meta http-equiv="refresh" content="2;URL=<?php echo site_url('gate/home/'.$check.'/'.$gate); ?>" />
	<?php endif; ?>	
<?php else: ?>
	<h2 class="text-center" style="color: red;font-weight: bold;font-size: 32px;">NOT ALLOWED IN THIS AREA</h2>
	<?php if($already == 0): ?>
		<meta http-equiv="refresh" content="2;URL=<?php echo site_url('gate/failed/'.$check.'/'.$gate.'/'.$datauser['id']); ?>" />
	<?php else: ?>
		<meta http-equiv="refresh" content="2;URL=<?php echo site_url('gate/home/'.$check.'/'.$gate); ?>" />
	<?php endif; ?>	
<?php endif; ?>

<hr>
<div class="text-center">
    <h2><b><?php echo $datauser['name']; ?></b></h2>
    <!--
    <?php if (isset($datauser['avatar']) && $datauser['avatar'] != ''): ?>
        <img src="<?php echo base_url(); ?>resizer/thumbimage.php?img=usermerchant/<?php echo $datauser['avatar']; ?>" alt="" />
    <?php else: ?>
        <img src="<?php echo base_url(); ?>resizer/thumbimage.php?img=user/default.png" alt="" />
    <?php endif; ?>
    -->
    <h3><?php echo 'Seat Number : <b>'.$datauser['seat'].'</b>'; ?></h3>   
    <h3><?php echo $datauser['visitor']; ?></h3>   
    <hr>      
</div>