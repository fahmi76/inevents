<script type="text/javascript">
    $(document).ready(function () {
        $('#rfid').focus();
    });
</script>
<script type="text/javascript">
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
</script>
<div class="text-center">
    <h2 class="tap-wristband"><p id="tap-code"><span>Tap Your Card Here</span></p></h2>
    <h2 class="tap-wristband">
        <p class="notify">
            <b>
                <span>
                    <?php if($check == 1){
                        echo 'Check-In';
                    }else{
                        echo 'Check-Out';
                    } ?>
                </span>
            </b>
        </p>
    </h2>

    <p style="font-style:italic;font-size:18px;"><?php echo $places_name; ?></p>
    
    <form id="input_submit" method="post">
        <div class="input-form">
            <p>
                <input id="rfid" type="text" onblur="displayResult(this);" autocomplete="off" name="serial" value="" autofocus />
            </p>
            <input type="submit" name="submit" value="submit" />
        </div>
    </form>

    <div id="message_ajax"></div>
</div> 