<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
</div>
<!-- /Title Page -->

<form name="addnews" enctype="multipart/form-data" method="post" action="<?php echo current_url() ?>">
    <?php if(validation_errors()) : ?>
    <div class="statusupdateError">
        <div class="statusupdateicon">
            <img src="<?php echo base_url()?>assets/images/admin/warningError.png" alt="Error" />
        </div>
        <div class="statusupdatedesc">
            <ul>
                    <?php echo validation_errors('<li>', '</li>'); ?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <?php endif; ?>

    <div style="padding-left:0px; padding-bottom:5px;">
        <div id="tabs">
            <div style="float:left; width:200px; padding-left: 10px;">
                <h4 class="titletable"><?php echo $title; ?></h4>
            </div>
        </div>
        <div class="clear"></div>

        <div class="tab_bdr"></div>
        <div class="panel" id="panel3" style="display: block;">
            <!-- form panel -->
            <div class="formpanel">

                <!-- large input -->
                <p>
                    <strong>Username</strong><br/>
                    <?php echo $datalist->account_username; ?>
                    <input name="id_user" type="hidden" value="<?php echo $datalist->id; ?>" />
                </p>
                <!-- /large input -->

                <!-- large input -->
                <p>
                    <strong>Display Name</strong><br/>
                    <?php echo $datalist->account_displayname; ?>
                </p>
                <!-- /large input -->

                <!-- listmenu -->
                <p>
                    <strong>User Group</strong><br/>
                    <select id="selectgroupuser" name="id_group" class="listmenus">
                        <option class="optgroupuser" value="9">Administrator</option>
                        <option class="optgroupuser" value="3">Operator</option>
                        <option class="optgroupuser" value="2">Multi Card Holder</option>
                        <option class="optgroupuser" value="1">Rakyat Jelata</option>
                    </select>
                </p>
                <!-- /listmenu-->

                <p id="checkgroupuser">
                    <strong>Places Permission</strong><br/>
                    <label class="largeinput">
                        <?php
                        $this->db->select('');
                        $this->db->where('places_parent', 0);
                        $this->db->from('places');
                        $this->db->order_by('id','ASC');
                        $select = $this->db->get()->result();
                        foreach ($select as $row) {
                            echo '<input type="checkbox" name="spots[]" value="' . $row->id . '" />&nbsp;&nbsp;' . $row->places_name . '<br />';                            
                        }
                        ?>
                    </label>
                </p>

                <!-- button submit -->
                <p>
                    <input type="image" src="<?php echo base_url(); ?>assets/images/admin/button-submit.gif" name="submit"/>
                </p>
                <!-- /button submit -->

            </div>
            <!-- /form panel -->

        </div>
        <div class="clear"></div>
        <div id="tabsbottom"></div>

    </div>

    <!-- /form window -->

</form>

