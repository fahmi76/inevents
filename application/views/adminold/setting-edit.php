<div style="padding: 10px;">
    <?php if (validation_errors ()): ?>
    <div class="statusupdateError">
        <div class="statusupdateicon">
            <img src="<?php echo base_url() ?>assets/images/admin/warningError.png" alt="Error" />
        </div>
        <div class="statusupdatedesc">
            <ul>
                <?php echo validation_errors('<li>', '</li>'); ?>
            </ul>
        </div>
        <div class="clear"></div>
        <meta http-equiv="refresh" content="1;URL=<?php echo site_url(); ?>/admin/setting">
    </div>
    <?php else : ?>    
    <div class="statusupdateOK">
        <div class="statusupdateicon"><img src="<?php echo base_url()?>assets/images/admin/warningOK.png" alt="Error" /></div>
        <div class="statusupdatedesc">Data Berhasil Disimpan.</div>
        <div class="clear"></div>
    </div>
    <meta http-equiv="refresh" content="1;URL=<?php echo site_url(); ?>/admin/setting">
    <?php endif; ?>
</div>