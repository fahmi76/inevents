<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Wooz.in - Management System</title>
        <link rel="icon" type="image/ico" href="favicon.ico" />

        <script language="JavaScript" type="text/javascript">
            site_url = '<?php echo base_url() ?>';
        </script>

        <!-- main css -->
        <link href="<?php echo base_url(); ?>assets/css/admin.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/modal.css" rel="stylesheet" type="text/css" />
        <!-- /main css -->

        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">
            var url = "<?php echo base_url(); ?>index.php/admin/settingedit";
        </script>
        <script src="<?php echo base_url(); ?>assets/javascripts/modal.window.js" type="text/javascript" charset="utf-8"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />

        <script type="text/javascript">
            function openFileBrowser(id){
                fileBrowserlink = "<?php echo base_url(); ?>assets/javascripts/pdw_file_browser/index.php?editor=standalone&returnID=" + id;
                window.open(fileBrowserlink,'pdwfilebrowser', 'width=1000,height=650,scrollbars=no,toolbar=no,location=no');
            }

			$(document).ready(function(){
                $("p#checkgroupuser").hide();
                $("select#selectgroupuser").change(function(){
                    var optgrpuser = $(".optgroupuser:selected").val();
                    if(optgrpuser == 3){
                        $("p#checkgroupuser").show();
                    }
                    return false;
                });
            });
        </script>        
    </head>

    <body>
        <!-- header -->
        <div id="header">            
            <?php if ($admin != "") : ?>
            <div class="menustatus">
                <h5>Logged in as
                    <a href="<?php echo site_url(); ?>admin/useredit">
                            <?php
                            $user_data = $this->db->get_where('account', array('id' => $admin))->row();
                            echo '<strong>' . $user_data->account_displayname . '</strong>';
                            ?>
                    </a>
			| <a href="<?php echo site_url(); ?>">View Website</a> 
                                                    			| <a href="<?php echo site_url(); ?>admin/logout">Logout</a>
                </h5>
            </div>
            <?php endif; ?>
            <div class="clear"></div>
        </div>
        <!-- /header -->

        <!-- middle -->
        <div id="middle">
            <?php if ($admin != "") : ?>
            <!-- menu -->
            <div class="middletop"><?php if ($user_data->account_group == '9') : ?>
                <h4 class="menu"><a href="<?php echo site_url() ?>admin">Dashboard</a></h4>
                    <?php #if ($user_data->account_group == '9') : ?>
                <h4 class="menu"><a href="<?php echo site_url() ?>admin/user">User</a></h4>
                    <?php endif; ?>
                <!--
                <h4 class="menu"><a href="<?php echo site_url() ?>/admin/booth">Stations</a></h4>
                -->
				<?php if($admin == 1): ?>
                <h4 class="menu"><a href="<?php echo site_url() ?>admin/places">Events / Places</a></h4>
                <h4 class="menu"><a href="<?php echo site_url() ?>admin/pin">Pins / Rewards</a></h4>
				<?php endif; ?>
                <h4 class="menu"><a href="<?php echo site_url() ?>admin/insight">Insights</a></h4>
                    <?php #if ($user_data->account_group == '9') : ?>
                <h4 class="menu"><a href="<?php echo site_url() ?>admin/banner">Banner</a></h4>
                <h4 class="menu"><a href="<?php echo site_url() ?>admin/userstat">Stat</a></h4>
                <h4 class="menu"><a name="setting" href="<?php echo site_url() ?>admin/setting">Setting</a></h4>
                    <?php #endif; ?>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
            <!-- /menu -->
            <?php endif; ?>

            <!-- line spacer -->
            <div class="middlebottom">
                <div class="footerspacer">&nbsp;</div>
            </div>
            <!-- /line spacer -->

            <!-- middleCONTENT -->
            <div class="middlecontent">
                <!-- Side Right -->
                <div class="sideright">
                    <?php echo $content; ?>
                </div>
                <!-- /Side Right -->
            </div>
            <!-- middleCONTENT -->

            <!-- line spacer -->
            <div class="middlebottom">
                <div class="footerspacer">&nbsp;</div>
            </div>
            <!-- /line spacer -->

            <!-- middleBOTTOM -->
            <div class="middlebottom">
                <div class="middlefooter">
                    <div class="middlefooterleft">&copy; 2010. <a href="<?php echo site_url(); ?>">Wooz.in</a></div>
                    <?php if ($admin != "") {
                        ?>
                    <div class="middlefooterright">Go Back to <a href="<?php echo site_url() ?>admin">Dashboard</a> | <a href="<?php echo site_url() ?>/admin/logout">Log Out</a></div>
                        <?php } ?>
                    <div class="clear"></div>
                </div>
            </div>
            <!-- /middleBOTTOM -->
        </div>
        <!-- /middle -->

    </body>
</html>
