
<SCRIPT language="JavaScript">
    <!--
    function confirmDelete(delUrl)
    {
        if (confirm("Are you sure you want to delete?")) {
            document.location = delUrl;
        }
    }
    function confirmBlock(delUrl)
    {
        if (confirm("Are you sure you want to block this user?")) {
            document.location = delUrl;
        }
    }
    function confirmUnBlock(delUrl)
    {
        if (confirm("Are you sure you want to unblock this user?")) {
            document.location = delUrl;
        }
    }
    //-->
</SCRIPT>
<script language="JavaScript">
    function resubmit()
    {
        document.filter.action="<?php echo current_url() ?>";
        document.filter.submit();
    }
</script>

<script type="text/javascript">
$(function() {
    $("#startdate").datepicker();
    $("#duedate").datepicker();
});
</script>
<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
    <!--
        <h4><a href="<?php echo site_url()?>/admin/useradd">Add new user</a></h4>
    -->
</div>
<!-- /Title Page -->

<div style="padding-left:0px; padding-bottom:5px;">
    <div id="tabs">
        <div style="float:left; width:200px; padding-left: 10px;">
            <h4 class="titletable"><?php echo $title; ?></h4>
        </div>
    </div>
    <div class="clear"></div>

    <div class="tab_bdr"></div>

    <div class="tablecontentmiddle">
        <form name="filter" action="<?php echo site_url();?>/admin/userjoindate" method="post">
            <div style="float:left; margin-left: 10px; line-height: 36px;">
            	<strong>Join Date : </strong>
                <input type="text" style="width:80px;" name="startdate" id="startdate" />
                <input type="text" style="width:80px;" name="duedate" id="duedate" />
                <input type="submit" value="search" />
            </div>
            <div style="float:right; margin-right: 10px; line-height: 36px;">
                User Count : <?php echo $total; ?>
            </div>
            <div class="clear"></div>
        </form>
    </div>

    <?php if(count($joindate)) : ?>
    <!-- table content middle -->
    <div class="tablecontentmiddle">
        <table class="tables">
            <thead>
                <tr>
                    <th>
                        <strong>No.</strong>
                    </th>
                    <th>
                        <strong>Month</strong>
                    </th>
                    <th>
                        <strong>Display Name</strong>
                    </th>
                    <th>
                        <strong>Join Date</strong>
                    </th>
                </tr>
            </thead>

            <tbody>
                <?php $no=1; foreach($joindate as $jdate): ?>
                <tr class="evenrow">
                    <td class="firstcol"><?php echo $no++; ?>.</td>
                    <td class="dateitem"><?php echo date('F Y',strtotime($jdate->account_joindate)); ?></td>
                    <td class="dataitem"><?php echo ucwords(strtolower($jdate->account_displayname)); ?></td>
                    <td class="dateitem"><?php echo date('d F Y',strtotime($jdate->account_joindate)); ?></td>
                </tr>
            </tbody>
                <?php endforeach; ?>
        </table>
    </div>
    <!-- table content middle -->
    <?php else : ?>
    <div class="panel notfound" id="panel3" style="display: block;">
        <h4>Data Not Found</h4>
    </div>
    <?php endif; ?>

    <div id="tabsbottom"></div>
</div>

<?php if(isset($tPaging) ): ?>
<div class="pages">
        <?php echo $tPaging?>
    <div class="clear"></div>
</div>
<?php endif;?>