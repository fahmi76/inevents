<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
</div>
<!-- /Title Page -->

<form name="addnews" enctype="multipart/form-data" method="post" action="<?php echo current_url() ?>">
    <?php if(validation_errors()) : ?>
    <div class="statusupdateError">
        <div class="statusupdateicon">
            <img src="<?php echo base_url()?>assets/images/admin/warningError.png" alt="Error" />
        </div>
        <div class="statusupdatedesc">
            <ul>
                    <?php echo validation_errors('<li>', '</li>'); ?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <?php endif; ?>
    <!-- form window -->

    <div style="padding-left:0px; padding-bottom:20px;">

        <div id="tabs">
            <div style="float:left; width:270px; padding:7px 0 0 13px;">
                <h4><?php echo $title; ?></h4>
            </div>
        </div>
        <div class="clear"></div>

        <div class="tab_bdr"></div>
        <div class="panel" id="panel3" style="display: block;">
            <!-- form panel -->
            <div class="formpanel">

                <!-- large input -->
                <p>
                    <strong>Pin Name</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="name" value="<?php if($datalist->badge_name!='') echo $datalist->badge_name; ?>" />
                    </label>
                </p>
                <!-- /large input -->

                <!-- listmenu -->
                <p>
                    <strong>Pin Type</strong><br/>
                    <select name="type" class="listmenus">
                        <option value="1" <?php if($datalist->badge_type == 1) echo 'selected="selected"'; ?>>All Spot Trip</option>
                        <option value="2" <?php if($datalist->badge_type == 2) echo 'selected="selected"'; ?>>Spot Visit Count</option>
                        <option value="3" <?php if($datalist->badge_type == 3) echo 'selected="selected"'; ?>>Selected Spot Visit</option>
                    </select>
                </p>
                <!-- /listmenu-->

                <!-- listmenu -->
                <p>
                    <strong>Pin Spot</strong><br/>
                    <select name="places" class="listmenus">
                        <option value="0" >Wooz.in Badge</option>
                        <?php
                        $this->db->select('');
                        $this->db->where('places_parent', 0);
                        $this->db->where('places_status !=', 0);
                        $this->db->from('places');
                        $select = $this->db->get()->result();
                        foreach($select as $row) {
                            echo '<option value="'.$row->id.'" ';
                            if($datalist->places_id == $row->id) echo 'selected="selected"';
                            echo ' >'.$row->places_name.'</option>';
                        }
                        ?>
                    </select>
                </p>
                <!-- /listmenu-->

                <!-- large input -->
                <p>
                    <strong>Minimum Spot Visit Count</strong> (only used for Pin type 2)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="value" value="<?php if($datalist->badge_value!='') echo $datalist->badge_value; ?>" />
                    </label>
                </p>
                <!-- /large input -->

                <!-- listmenu -->
                <p>
                    <strong>Visit This Spot</strong> (only used for Pin type 3)<br/>
                    <!--  <label class="largeinput"> -->
                        <?php
                        if($datalist->badge_spot!='') {
                           $spot = json_decode($datalist->badge_spot);
                        } else {
                            $spot = array();
                        }
                        
                        $this->db->select('');
                        $this->db->where('places_parent', 0);
                        $this->db->where('places_status !=', 0);
                        $this->db->from('places');
                        $select = $this->db->get()->result();
                       
                        foreach($select as $row) {
                            echo '<input type="checkbox" name="spots[]" value="'.$row->id.'"';
                            if($datalist->badge_type=='2') {
                                if(count($spot)==0&&in_array($row->id)) echo 'checked="checked"';
                            } elseif($datalist->badge_type=='1') {
                                if(count($spot)==0) echo 'checked="checked"';
                            }
                            else {
                                if(count($spot)!=0&&in_array($row->id, $spot)) echo 'checked="checked"';
                            }
                            echo '/>&nbsp;&nbsp;'.$row->places_name.'<br />';
						
                            
                            $this->db->select('');
                            $this->db->where('places_parent', $row->id);
                            $this->db->where('places_status !=', 0);
                            $this->db->from('places');
                            $select = $this->db->get()->result();
                            foreach($select as $row) {
                                
				echo '&nbsp;&nbsp;&nbsp;<input type="checkbox" name="spots[]" value="' . $row->id . '"';
				if($datalist->badge_type=='2') {
                                    if(count($spot)==0&&in_array($row->id)) echo 'checked="checked"';
                                } elseif($datalist->badge_type=='1') {
                                    if(count($spot)==0) echo 'checked="checked"';
                                } else {
                                    if(count($spot)!=0&&in_array($row->id, $spot)) echo 'checked="checked"';
                                }							
                            echo '/>&nbsp;&nbsp;'.$row->places_name.'<br />';
                            }
                        }
                        ?>
                   <!-- </label> -->
                </p>
                <!-- /listmenu-->

                <!-- textarea -->
                <p>
                    <strong>Pin Description</strong><br/>
                    <label class="textarea">
                        <textarea cols="80" rows="10" id="isi" name="isi"><?php if($datalist->badge_desc!='') echo $datalist->badge_desc; ?></textarea>
                    </label>
                </p>
                <!-- /textarea -->

                <!-- large input -->
                <p>
                    <strong>Pin Icon</strong><br/>
                    <label class="largeinput">
                        <input type="file" style="width:78%"  name="icon" />
                    </label>
                </p>
                <!-- /large input -->

                <!-- button submit -->
                <p>
                    <input type="image" src="<?php echo base_url(); ?>assets/images/admin/button-submit.gif" name="submit"/>
                </p>
                <!-- /button submit -->

            </div>
            <!-- /form panel -->

        </div>
        <div class="clear"></div>
        <div id="tabsbottom"></div>

    </div>

    <!-- /form window -->

</form>
