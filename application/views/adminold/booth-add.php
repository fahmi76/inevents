<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
</div>
<!-- /Title Page -->

<form name="addnews" enctype="multipart/form-data" method="post" action="<?php echo current_url() ?>">
    <?php if(validation_errors()) : ?>
    <div class="statusupdateError">
        <div class="statusupdateicon">
            <img src="<?php echo base_url()?>assets/images/admin/warningError.png" alt="Error" />
        </div>
        <div class="statusupdatedesc">
            <ul>
                    <?php echo validation_errors('<li>', '</li>'); ?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <?php endif; ?>
    <!-- form window -->

    <div style="padding-left:0px; padding-bottom:20px;">

        <div id="tabs">
            <div style="float:left; width:270px; padding:7px 0 0 13px;">
                <h4><?php echo $title; ?></h4>
            </div>
        </div>
        <div class="clear"></div>

        <div class="tab_bdr"></div>
        <div class="panel" id="panel3" style="display: block;">
            <!-- form panel -->
            <div class="formpanel">

                <!-- large input -->
                <p>
                    <strong>Booth Name</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="name" />
                    </label>
                </p>
                <!-- /large input -->

                <!-- large input -->
                <p>
                    <strong>Booth Serial</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="serial" />
                    </label>
                </p>
                <!-- /large input -->

                <!-- button submit -->
                <p>
                    <input type="image" src="<?php echo base_url(); ?>assets/images/admin/button-submit.gif" name="submit"/>
                </p>
                <!-- /button submit -->

            </div>
            <!-- /form panel -->

        </div>
        <div class="clear"></div>
        <div id="tabsbottom"></div>

    </div>

    <!-- /form window -->

</form>
