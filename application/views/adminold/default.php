<!-- Title Page -->
<div class="titlepage">
    <h3>Welcome</h3>
</div>
<!-- /Title Page -->

<div class="tablecontenttop">
    <h4 class="titletable">Statistik</h4>
</div>

<!-- table content middle -->
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>No.</strong>
                </th>
                <th>
                    <strong>Menu</strong>
                </th>
                <th>
                    <strong>Jumlah Data</strong>
                </th>
            </tr>
        </thead>

        <tbody>
            <tr class="oddrow">
                <td class="firstcol">
                    			1.
                </td>
                <td class="dataitem">
                    <a href="<?php echo site_url()?>/admin/user">User</a>
                </td>
                <td class="dataitem">
                    <a href="<?php echo site_url()?>/admin/user">
                        <?php
                        $this->db->select('');
                        $this->db->where('account_status !=', '0');
                        $this->db->from('account');
                        echo $this->db->count_all_results();
                        ?>
                    </a>
                </td>
            </tr>            
            <tr class="oddrow">
                <td class="firstcol">
                    			3.
                </td>
                <td class="dataitem">
                    <a href="<?php echo site_url()?>/admin/places">Events / Places</a>
                </td>
                <td class="dataitem">
                    <a href="<?php echo site_url()?>/admin/places">
                        <?php
                        $this->db->select('');
                        $this->db->where('places_parent', 0);
                        $this->db->where('places_status !=', 0);
                        $this->db->from('places');
                        echo $this->db->count_all_results();
                        ?>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!-- table content middle -->        

<div class="tablecontentbottom">&nbsp;</div> 

