
<SCRIPT language="JavaScript">
    <!--
    function confirmDelete(delUrl)
    {
        if (confirm("Are you sure you want to delete")) {
            document.location = delUrl;
        }
    }
    //-->
</SCRIPT>
<script language="JavaScript">
    function resubmit()
    {
        document.filter.action="<?php echo current_url() ?>";
        document.filter.submit();
    }
</script>
<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
    <h4><a href="<?php echo site_url('admin/boothadd')?>">Add new <?php echo $modules; ?></a></h4>
</div>
<!-- /Title Page -->

<div id="tabs">
    <div class="tabtitletable">
        <h4 class="titletable"><?php echo $title; ?></h4>
    </div>
    <div class="clear"></div>
</div>

<div class="tab_bdr"></div>

<div class="tablecontentmiddle">			
    <form name="filter" action="<?php echo current_url();?>" method="post">
        <div style="float:left; margin-left: 10px; line-height: 36px;">
            <input type="text" name="find" value="<?php if($find!='0') echo $find; ?>" />
            <input type="submit" value="search" />
        </div>

        <div style="float:right; margin-right: 10px; line-height: 36px;">
            <strong>Filter : </strong>
            <select name="order" onchange="resubmit()">
                <option value="3" <?php if($order=='3') echo "selected=\"selected\""; ?>>Title (A-Z)</option>
                <option value="2" <?php if($order=='2') echo "selected=\"selected\""; ?>>Title (Z-A)</option>
                <option value="1" <?php if($order=='1') echo "selected=\"selected\""; ?>>Date (Newest First)</option>
                <option value="4" <?php if($order=='4') echo "selected=\"selected\""; ?>>Date (Oldest First)</option>
            </select>
        </div>
        <div class="clear"></div>
    </form>
</div>

<?php if( count($datalist) ) : ?>
<!-- table content middle -->
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>No.</strong>
                </th>
                <th>
                    <strong>Name</strong>
                </th>
                <th>
                    <strong>Serial</strong>
                </th>
                <th>
                    <strong>Usage</strong>
                </th>
                <th>
                    <strong>Action</strong>
                </th>
            </tr>
        </thead>

        <tbody>

                <?php
                $no = 1;
                foreach($datalist as $row):
                    ?>
            <tr class="oddrow">
                <td class="firstcol">
                            <?php echo $no++; ?>
                </td>

                <td class="dataitem">
                            <?php echo $row->booth_name; ?>
                </td>

                <td class="edititem">
                            <?php echo $row->booth_serial; ?>
                </td>

                <td class="edititem">
                            <?php
                            $places = $this->db->get_where('places', array('booth_id' => $row->id))->row();
                            if($places)
                                echo $places->places_name;
                            else
                                echo 'not used';
                            ?>
                </td>

                <td class="edititem" >
                    <div class="iconedit">
                        <a href="<?php echo site_url(); ?>/admin/boothedit/<?php echo $row->id;?>">edit</a>
                    </div>
                    <div class="icondelete">
                        <a href="javascript:confirmDelete('<?php echo site_url(); ?>/admin/boothdelete/<?php echo $row->id;?>')">delete</a>
                    </div>
                </td>
            </tr>

                <?php endforeach;?>
        </tbody>
    </table>
</div>
<!-- table content middle -->        
<?php else : ?>
<div class="panel notfound" id="panel3" style="display: block;">
    <h4>Data Not Found</h4>
</div>
<?php endif; ?>

<div id="tabsbottom"></div>

<?php if( isset($tPaging) ): ?>
<div class="pages"> 
        <?php echo $tPaging?>
    <div class="clear"></div>
</div>
<?php endif;?>
