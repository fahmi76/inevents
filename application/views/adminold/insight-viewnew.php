<?php if($excel == 1): ?>
<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=statistik-event.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php endif; ?>
<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
	<div align="right"><a href="<?php echo base_url(); ?>admin/insightview/<?php echo $location; ?>/1">export to excel</a>  |  <a href="<?php echo base_url(); ?>admin/insightviewstat/<?php echo $location; ?>">view statistik this event</a> </div>
</div>
<!-- /Title Page -->
<div style="padding-left:0px; padding-bottom:5px;">
    <div id="tabs">
        <div style="float:left; width:200px; padding-left: 10px;">
            <h4><?php echo $title; ?></h4>
        </div>
    </div>
    <div class="clear"></div>

    <div class="tab_bdr"></div>
    <div class="panel" id="panel3" style="display: block;">
        <!-- form panel -->
        <div class="formpanel">

            <!-- large input -->
            <h3 class="ins">Events / Places : <?php echo $datalist->places_name; ?></h3>
            <p>
                <?php if ($datalist->places_desc != '')
                    echo $datalist->places_desc; ?>
            </p>
            <!-- /large input -->

            <?php
                /* total */
                $gen = array('male' => 0, 'female' => 0, 'unk' => 0);
                $age = array('40plus' => 0, '30-40' => 0, '26-30' => 0, '21-25' => 0, 'under' => 0, 'unk' => 0);
                $int = array();
                $snd = array();
                $crs = array();
                $c = 0;
                $d = 0;
                $e = 0;
				$y = 0;
				$z = 0;

                $xx = '';
                $yy = '';

                $data_inter = array();
				$data_ctgry = array();
                $like_inter = array();
				$like_ctgry = array();
                /* per data interest user processing */
                    /* interest */
					if (isset($datainterest) && count($datainterest) != 0 ) {
                        foreach ($datainterest as $interest) {
							if(isset($interest->facebook_page_name) && $interest->facebook_page_name != "")
                            $data_inter[] = strip_tags(trim($interest->facebook_page_name));
							if(isset($interest->category_name) && $interest->category_name != "")
                            $data_ctgry[] = strip_tags(trim($interest->category_name));
							$y++;
                        }
                    }

                    /* likes */
					if (isset($datalike) && count($datalike) != 0 ) {
                        foreach ($datalike as $like) {
							if(isset($like->facebook_page_name) && $like->facebook_page_name != "")
                            $like_inter[] = strip_tags(trim($like->facebook_page_name));
							if(isset($like->category_name) && $like->category_name != "")
                            $like_ctgry[] = strip_tags(trim($like->category_name));
							$z++;
                        }
                    }

                foreach($user as $about){
					$twfriend[] = $about->landing_tw_friends;
					$fbfriend[] = $about->landing_fb_friends;
                    if (isset($about->account_gender)) {
                        if ($about->account_gender == 'male')
                            $gen['male']++;
                        else
                            $gen['female']++;
                    }else {
						$gen['unk']++;
					}
                    if (isset($about->account_birthdate)) {
						if ($this->fungsi->age($about->account_birthdate) > 39)
							$age['40plus']++;
						elseif ($this->fungsi->age($about->account_birthdate) > 30)
							$age['30-40']++;
						elseif ($this->fungsi->age($about->account_birthdate) > 25)
							$age['26-30']++;
						elseif ($this->fungsi->age($about->account_birthdate) > 20)
							$age['21-25']++;
						elseif ($this->fungsi->age($about->account_birthdate) < 21)
							$age['under']++;
                    } else {
                        $age['unk']++;
					}                    
                }

                $config = array(
                    'min_font' => 12,
                    'max_font' => 45,
                    'html_start' => '',
                    'html_end' => '',
                    'shuffle' => FALSE,
                    'class' => 'tag1',
                    'match_Class' => 'bold',
                );

				$tag1 = $this->fungsi->insight_break($data_inter);
                $inter_tag = $this->taggly->cloud($tag1, $config);

				$tag2 = $this->fungsi->insight_break($data_ctgry);
                $inter_cat = $this->taggly->cloud($tag2, $config);

				$tag3 = $this->fungsi->insight_break($like_inter);
				$like1 = array();
				foreach ($tag3 as $tag){
					if($tag[0] >= 5)
						$like1[] = $tag;
				}
                $likes_tag = $this->taggly->cloud($like1, $config);

				$tag4 = $this->fungsi->insight_break($like_ctgry);
                $likes_cat = $this->taggly->cloud($tag4, $config);

			?>

                <h3 class="ins">Statistics (Update Status & Photo)</h3>
                <p>
                    Total registration at venue : <?php echo $totaluser;?>
				<br />Total who tap at venue : <?php echo $actiontotal['user']; #echo $useraktif; ?>
                <br />Total all tap in : <?php echo $actiontotal['all'];?>
                <br />Total all Facebook update : <?php echo $actiontotal['fb'];?>
                <br />Total all Twitter update : <?php echo $actiontotal['tw'];?>
				<?php if($rfid != 0): ?>
				<br />Total RFID tag event : <?php echo $rfid;?>
				<br />Presentase RFID tag event : <?php $total = ($useraktif/$rfid)*100; echo $total.'%';?>
				<br />Presentase Photo tag event : <?php $total = ($photos['user']/$rfid)*100; echo $total.'%';?>
				<?php endif; ?>
                </p>

                <h3 class="ins">Update Status</h3>
                <p>
					Total who tap update status at venue : <?php if(isset($action['user'])) echo $action['user'];?>
					<br />Total Facebook update : <?php echo $action['fb'];?>
					<br />Total Twitter update : <?php echo $action['tw'];?>
                </p>

                <h3 class="ins">Photo Booth</h3>
                <p>
					Total photo taken : <?php echo $photos['photo'];?>
                <br />Total who initialize photo taking : <?php echo $photos['user'];?>
					<br />Total Facebook update : <?php echo $action1['fb'];?>
					<br />Total Twitter update : <?php echo $action1['tw'];?>
                </p>
				<?php if($location == 247 || $location == 230 || $location == 381): ?>
				<h3 class="ins">Redeem</h3>
				<p>
				<?php
						$this->db->select('DISTINCT(redeem) AS thedate, count(distinct(account_id)) as total');
						$this->db->where_in('places_id', $family );
						$this->db->where_in('redeem', array("1","2") );
						$this->db->from('log');
						$this->db->group_by("thedate"); 
						$this->db->order_by("thedate", "asc");
						$total = $this->db->get()->result();
						$y = 1;	
						foreach ($total as $user) {
				?>
					Total Redeem <?php echo $y;?> Kali : <?php echo $user->total; ?><br />
				<?php $y++; } ?>
                </p>
				<?php endif; ?>
                <h3 class="ins">Badge</h3>
                <p>
					<?php foreach($badge as $row): ?>
					<?php  
						#$famil = array("193","194","195");
						$this->db->select('count( wooz_log.account_id ) AS total, badge_name');
						$this->db->join('badge', 'badge.id = log.badge_id', 'right');
						$this->db->where('log.badge_id', $row->badge_id);
						$this->db->where_in('log.places_id', $family );
						$this->db->order_by('log.id', 'desc');
						$this->db->where('log.log_status', 1);
						$this->db->from('log');			
						$total1 = $this->db->get()->row();
						echo "Total user get badges : ". $total1->total ." in ". $total1->badge_name;
						echo '</br>';
					?>
					<?php endforeach; ?>
                </p>

                <h3 class="ins">Gender</h3>
                <p>
					Male : <?php echo $gen['male'];?>
                <br />Female : <?php echo $gen['female'];?>
				 <br />Unknown : <?php echo $gen['unk'];?>
                </p>

				<h3 class="ins">Age Range</h3>
                <p>
					Age 40 ++ : <?php echo $age['40plus'];?>
                <br />Age 31 - 40 : <?php echo $age['30-40'];?>
                <br />Age 26 - 30 : <?php echo $age['26-30'];?>
                <br />Age 21 - 25 : <?php echo $age['21-25'];?>
                <br />Under 21 : <?php echo $age['under'];?>
                <br />Unknown : <?php echo $age['unk'];?>
                </p>

                <h3 class="ins">Friends</h3>
                <p>
					updates via Facebook reached <?php echo array_sum($fbfriend);?> friends
				<br />updates via Twitter reached <?php echo array_sum($twfriend);?> friends
                </p>

                <h3 class="ins">Like and Follow</h3>
                <p>
					like fanpage : <?php echo count($fblike);?> users
				<br />like twitter : <?php echo count($twfollow);?> users
                </p>

                <h3 class="ins">URL link photobooth</h3>
                <p>
				<a href="http://wooz.in/backups/woozin-photobooth-<?php echo $datalist->places_landing ?>.zip">http://wooz.in/backups/woozin-photobooth-<?php echo $datalist->places_landing ?>.zip</a>
				</p>
				<!--
				<h3 class="ins">Interest (by Name, taken from <?php echo $totaluser; ?> user)</h3>
				-->
				<h3 class="ins">Interest (by Name)</h3>
                <p class="tags">
                <?php echo $inter_tag; ?>
				</p>
				<!--
				<h3 class="ins">Likes (by Name, taken from <?php echo $totaluser; ?> user)</h3>
				-->
				<h3 class="ins">Likes (by Name)</h3>
                <p class="tags">
                <?php echo $likes_tag; ?>
				</p>
				<!--
				<h3 class="ins">Interest (by Category, taken from <?php echo $totaluser; ?> user)</h3>
				-->
				<h3 class="ins">Interest (by Category)</h3>
                <p class="tags">
                <?php echo $inter_cat; ?>
				</p>
				<!--
				<h3 class="ins">Likes (by Category, taken from <?php echo $totaluser; ?> user)</h3>
				-->
				<h3 class="ins">Likes (by Category)</h3>
                <p class="tags">
                <?php echo $likes_cat; ?>
				</p>

        </div>
        <!-- /form panel -->

    </div>
    <div class="clear"></div>
    <div id="tabsbottom"></div>

</div>

<!-- /form window -->

