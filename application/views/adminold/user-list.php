
<SCRIPT language="JavaScript">
    <!--
    function confirmDelete(delUrl)
    {
        if (confirm("Are you sure you want to delete?")) {
            document.location = delUrl;
        }
    }
    function confirmBlock(delUrl)
    {
        if (confirm("Are you sure you want to block this user?")) {
            document.location = delUrl;
        }
    }
    function confirmUnBlock(delUrl)
    {
        if (confirm("Are you sure you want to unblock this user?")) {
            document.location = delUrl;
        }
    }
    //-->
</SCRIPT>
<script language="JavaScript">
    function resubmit()
    {
        document.filter.action="<?php echo current_url() ?>";
        document.filter.submit();
    }
</script>

<script type="text/javascript">
    $(function() {
        $("#startdate").datepicker();
        $("#duedate").datepicker();
    });
</script>
<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
    <!--
        <h4><a href="<?php echo site_url() ?>/admin/useradd">Add new user</a></h4>
    -->
</div>
<!-- /Title Page -->

<div style="padding-left:0px; padding-bottom:5px;">
    <div id="tabs">
        <div style="float:left; width:200px; padding-left: 10px;">
            <h4 class="titletable"><?php echo $title; ?></h4>
        </div>
    </div>
    <div class="clear"></div>

    <div class="tab_bdr"></div>

    <div class="tablecontentmiddle">
        <form name="filter" action="<?php echo current_url(); ?>" method="post">
            <div style="float:left; margin-left: 10px; line-height: 36px;">
                <input type="text" name="find" value="<?php if ($find != '0')
    echo $find; ?>" />
                <input type="submit" value="search" />
            </div>
            <div style="float:right; margin-right: 10px; line-height: 36px;">
                <strong>Filter : </strong>
                <select name="order" onchange="resubmit()">
                    <option value="3" <?php if ($order == '3')
                           echo "selected=\"selected\""; ?>>Display Name (A-Z)</option>
                    <option value="2" <?php if ($order == '2')
                                echo "selected=\"selected\""; ?>>Display Name (Z-A)</option>
                    <option value="1" <?php if ($order == '1')
                                echo "selected=\"selected\""; ?>>Date (Newest First)</option>
                    <option value="4" <?php if ($order == '4')
                                echo "selected=\"selected\""; ?>>Date (Oldest First)</option>
                    <option value="5" <?php if ($order == '5')
                                echo "selected=\"selected\""; ?>>Spot (Most-Lowest)</option>
                    <option value="6" <?php if ($order == '6')
                                echo "selected=\"selected\""; ?>>Spot (Lowest-Most)</option>
                    <option value="7" <?php if ($order == '7')
                                echo "selected=\"selected\""; ?>>Pin (Most-Lowest)</option>
                    <option value="8" <?php if ($order == '8')
                                echo "selected=\"selected\""; ?>>Pin (Lowest-Most)</option>
                </select>
            </div>
            <div class="clear"></div>
        </form>
    </div>

    <?php if (count($datalist)) : ?>
                                <!-- table content middle -->
                                <div class="tablecontentmiddle">
                                    <table class="tables">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <strong>No.</strong>
                                                </th>
                                                <th>
                                                    <strong>Display Name</strong>
                                                </th>
                                                <th>
                                                    <strong>Spot Visited</strong>
                                                </th>
                                                <th>
                                                    <strong>Pin Earned</strong>
                                                </th>
                                                <th>
                                                    <strong>User Level</strong>
                                                </th>
                                                <th>
                                                    <strong>Action</strong>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>

                <?php
                                $no = 1;
                                foreach ($datalist as $row):
                ?>
                                    <tr class="oddrow">
                                        <td class="firstcol">
                        <?php echo $no++; ?>
                                </td>

                                <td class="dataitem">
                        <?php echo $row->account_displayname ?><br />
                                </td>

                                <td class="dateitem">
                        <?php
                                    $this->db->select('');
                                    $this->db->where('log.account_id', $row->id);
                                    $this->db->where('log.log_status', 1);
                                    $this->db->where('log.log_type', 1);
                                    $this->db->join('places', 'places.id = log.places_id', 'left');
                                    $this->db->order_by('log.id', 'desc');
                                    $this->db->from('log');
                                    $getspot = $this->db->get();
                                    $numspot = $getspot->num_rows();
                                    echo $numspot;                                    
                        ?>
                                </td>

                                <td class="dateitem">
                        <?php
                                    $this->db->select('');
                                    $this->db->where('log.account_id', $row->id);
                                    $this->db->where('log.log_status', 1);
                                    $this->db->where('log.log_type', 2);
                                    $this->db->join('account', 'account.id = log.account_id', 'left');
                                    $this->db->join('places', 'places.id = log.places_id', 'left');
                                    $this->db->join('badge', 'badge.id = log.badge_id', 'left');
                                    $this->db->order_by('log.id', 'desc');
                                    $this->db->from('log');
                                    $getpin = $this->db->get();
                                    $numpin = $getpin->num_rows();
                                    echo $numpin;
                        ?>
                                </td>

                                <td class="dateitem">
                        <?php
                                    if ($row->account_group == 9){
                                        echo 'Administrator';
                                    } elseif ($row->account_group == 2){
                                        echo 'Multi Card Holder';
                                    } elseif ($row->account_group == 3){
                                        echo 'Operator';
                                    } else {
                                        echo 'Rakyat Jelata';
                                    }
                        ?>
                                </td>

                                <td class="edititem" >
                        <?php if ($admin != $row->id) : ?>
                        <?php if ($row->account_group != 9) : ?>
                                            <div class="icongroup">
                                                <a href="<?php echo site_url(); ?>/admin/usergroup/<?php echo $row->id; ?>">group</a>
                                            </div>
                        <?php if ($row->account_group != 0) : ?>
                                                <div class="iconblock">
                                                    <a href="javascript:confirmBlock('<?php echo site_url(); ?>/admin/userblock/<?php echo $row->id; ?>')">block</a>
                                                </div>
                        <?php else : ?>
                                                    <div class="iconunblock">
                                                        <a href="javascript:confirmUnBlock('<?php echo site_url(); ?>/admin/userunblock/<?php echo $row->id; ?>')">unblock</a>
                                                    </div>
                        <?php endif; ?>
                        <?php endif; ?>
                                                    <div class="icondelete">
                                                        <a href="javascript:confirmDelete('<?php echo site_url(); ?>/admin/userdelete/<?php echo $row->id; ?>')">delete</a>
                                                    </div>
                        <?php else : ?>
                                                                                                                		This Is You!
                        <?php endif; ?>
                                                    </td>
                                                </tr>
                <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- table content middle -->
    <?php else : ?>
                                                            <div class="panel notfound" id="panel3" style="display: block;">
                                                                <h4>Data Not Found</h4>
                                                            </div>
    <?php endif; ?>

                                                            <div id="tabsbottom"></div>
                                                        </div>

<?php if (isset($tPaging)): ?>
                                                                <div class="pages">
    <?php echo $tPaging ?>
                                                                <div class="clear"></div>
                                                            </div>
<?php endif; ?>
