<script src="<?php echo $assets_url; ?>/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo $assets_url; ?>/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $assets_url; ?>/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo $assets_url; ?>/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />
<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>rsvp/checknameawalnew/<?php echo $customs; ?>",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id);
                        $("#InputOrganization").val(ui.item.organization);
                        $("#InputEmail").val(ui.item.email);
                    }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://woozin.tgrid.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<div class="panel panel-info">
    <?php if (validation_errors()): ?>
        <p class="error">Whoops ! Something wrong
            <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif;?>

</div>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">
        <div class="form-group">
            <label for="InputName">Full Name</label>
            <input type="text" class="form-control input-lg" name="name" id="InputName" autocomplete="off" value="" placeholder="Your Name">
        </div>
        <input id="InputId" type="hidden" autocomplete="off" name="id" value="" autofocus />
        <div class="form-group">
            <label for="InputName">Business Unit</label>
            <input type="text" class="form-control input-lg" name="staff" id="InputOrganization" autocomplete="off" value="" placeholder="Business Unit" disabled>
        </div>
        <div class="form-group">
            <label for="InputName">Email Address</label>
            <input type="text" class="form-control input-lg" name="email" id="InputEmail" autocomplete="off" value="" placeholder="Email Address" disabled>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        </div>
</form>