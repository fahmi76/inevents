<script type="text/javascript">
$(document).ready(function() {
    $("#InputRFID").focus();
    $("#registration").submit(function() {
        $('.action').html('<img src="http://woozin.tgrid.in/assets/customs/loader.gif" /> Please Wait...');
    });
});
</script>
<div class="panel panel-info">
    <?php if (validation_errors()): ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
    </p>
    <?php endif;?>
</div>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Full Name</label>
        <input type="text" class="form-control input-lg" name="name" id="InputName" autocomplete="off" value="<?php echo $hasil->account_displayname; ?>" placeholder="Your Name">
    </div>
    <div class="form-group">
        <label for="InputName">Email</label>
        <input type="text" class="form-control input-lg" name="email" id="InputName" autocomplete="off" value="<?php echo $email; ?>" placeholder="Email">
    </div>
    <div class="form-group">
        <label class="InputName">Business Unit</label>
        <input type="text" class="form-control input-lg" name="organization" id="InputName" autocomplete="off" value="<?php echo $organization; ?>" placeholder="Organization">
    </div>
    <div class="form-group">
        <label class="InputName">Staff Number</label>
        <input type="text" class="form-control input-lg" name="staffnumber" id="InputRFID" autocomplete="off" value="<?php echo $staff; ?>" placeholder="Staff Number">
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
    </div>
</form>
