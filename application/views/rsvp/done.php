<h2>Thank you</h2>
<hr>
<div class="text-center">
    <p>The registration process has been completed.</p>
    <br>
    <button type="button" class="btn btn-primary btn-xlg">Finish</button>
</div>

<?php if ($logoutfb == 1): ?>
<meta http-equiv="refresh" content="2;URL=<?php echo $logout; ?>" /> 
<?php else: ?>
<meta http-equiv="refresh" content="2;URL=<?php echo site_url('registration/logoutsosmed'); ?>" /> 
<?php endif; ?>