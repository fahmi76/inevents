<script type="text/javascript">
$(document).ready(function() {
    $("#InputRFID").focus();
    $("#registration").submit(function() {
        $('.action').html('<img src="http://woozin.tgrid.in/assets/customs/loader.gif" /> Please Wait...');
    });
});
</script>
<div class="panel panel-info">
    <?php if (validation_errors()): ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
    </p>
    <?php endif;?>
</div>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">

    <div class="form-group">
        <label class="InputName">Would you be attending?</label>
            <div class="controls">

            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-warning">
                    <input type="radio" name="rsvp" id="InputGenderMale" value="Yes"> Yes
                </label>
                <label class="btn btn-warning active">
                    <input type="radio" name="rsvp" id="InputGenderFemale" value="No" checked="checked"> No
                </label>
            </div>
            </div>
    <div class="text-center">
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
    </div>
</form>
