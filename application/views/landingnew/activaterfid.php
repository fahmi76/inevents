<script type="text/javascript">
    $(document).ready(function() {
        $("#rfidNumber").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
    //document.forms['input_submit'].style.visibility = "hidden";
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function autosubmit(original) {
        if (original.value.length === 10) {
            document.getElementById('registration').submit();
        }
    }
</script>
<h2>Hello, <?php echo $datalist->account_displayname; ?></h2>
<br>
<div class="panel panel-info">
    <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
            <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
</div>

<?php if ($spot_id == 453 || $spot_id == 258 || $spot_id == 454): ?>	
<div class="alert alert-danger">Type your registration code</div>
<?php else: ?>
<div class="alert alert-danger">Activate your <?php echo $card;?> now. Place Tap <?php echo $card;?> onto the reader</div>
<?php endif; ?>
<form method="post" role="form" action="<?php echo current_url() . '/?url=' . $customs . '&acc=' . $id . '&from=' . $from . '&places=' . $spot_id.'&likefollow='.$likefollow; ?>" id="registration">
    <?php if($spot_id == 446): ?>
		<div class="form-group">
			<div class="error">
				<div align="left" style="font-size:12px;width:480px;height:110px;overflow:scroll;text-align:left;overflow-x:hidden;">
				<b>Syarat dan ketentuan :</b>
				<ul>
					<li>Saya Bersedia mengikuti THE RUNWAY atas kesadaran saya sendiri. Foto & videonya yang terambil saat THE RUNWAY adalah milik TRESemme dan dapat digunakan untuk kepentingan publikasi TRESemme</li>
					<li>Saya berani tampil gorgeous dengan busana hitam putih pada saat THE RUNWAY. Hitam menandakan tidak takut terlihat rambut berketombe & Putih menandakan tidak takut terlihat rambut rontok pada saat catwalk.</li>
					<li>Untuk peserta yang mengikuti TRESemme Youtube Star :				
						<ol>
							<li>Pengumuman 10 finalis plg lambat 30 Mei 2014, validasi data akan dilakukan dalam 3 hari setelah pengumuman. Mereka yg tervalidasi akan mendapatkan hadiah berupa hampers.</li>
							<li>10 finalis berhak mempromosikan link videonya untuk mendapatkan like & view sebanyak2nya yg akan memperngaruhi penilaian dewan juri</li>
							<li>Dari 10 finalis akan dipilih 5 pemenang yg berhak mendapatkan kontrak senilai 5 juta dan fee setiap dipakai shooting</li>
							<li>Pengumuman 5 pemenang ini plg lambat 22 Juni 2014.</li>
						</ol>
					</li>
				</ul>			
				</div>
				<br /><input type="checkbox" name="tosfoto" value="1" checked="checked" /> <font size="1">Saya setuju dengan syarat dan ketentuan</font>
			</div>
		</div>
		<div class="form-group">
			<label for="InputBirth">Nomor Peserta</label>			
            <?php
            $number1 = set_value('number1');
            if ($number1) {
                $number1 = $number1;
            } else {
                $number1 = '';
            }
            ?>
			<div class="form-inline">
				<div class="form-group">
					<label class="sr-only" for="InputMonth">No</label>
					<select class="form-control input-lg" name="number1" id="InputMonth">
						<option value="">-No-</option>
						<option value="A" <?php if ($number1 == 'A') : ?>selected="selected"<?php endif; ?>>A</option>
						<option value="B" <?php if ($number1 == 'B') : ?>selected="selected"<?php endif; ?>>B</option>
						<option value="C" <?php if ($number1 == 'C') : ?>selected="selected"<?php endif; ?>>C</option>
					</select> 
				</div> - 
				<div class="form-group" style="width: 70px;">
					<label class="sr-only" for="InputYear">Nomer Peserta</label>
					<input type="text" class="form-control input-lg" id="rfidNumber" name="number" placeholder="0000" value="<?php echo set_value('number'); ?>" maxlength="4" autocomplete="off">
				</div>                   
			</div>
		</div> 
		<div class="form-group">
			<label for="rfidNumber">Code Pembelian</label>		
			<input type="text" name="code" class="form-control input-lg" value="<?php echo set_value('code'); ?>" placeholder="Your Code" autocomplete="off">
		</div>
		<div class="form-group">
			<label for="rfidNumber"><?php echo $card;?> Number</label>
			<input type="text" name="serial" onKeyUp="autosubmit(this);" class="form-control input-lg" id="rfidNumber" placeholder="Your RFID number" autocomplete="off">
		</div>
	<?php else: ?>
		<div class="form-group">
			<label for="rfidNumber"  class="sr-only">RFID <?php echo $card;?> Number</label>
			<input type="text" name="serial" onblur="displayResult(this);" onKeyUp="autosubmit(this);" class="form-control input-lg" id="rfidNumber" placeholder="Your RFID number" autocomplete="off">
		</div>
	<?php endif; ?>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Activate</button>
    </div>
</form>