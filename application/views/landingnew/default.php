<?php if (isset($retry) && $retry === true) { ?>
    <p class="error">Please Try Again <?php if (isset($status) && $status != '') echo 'due to ' . str_replace('_', ' ', $status) ?> </p>
<?php } ?>
<div class="text-center">
    <h3>Click Below to Register</h3>
    <?php $redirect = base_url() . 'home/facebook/landing/' . $customs . '/0/' . $spot_id; ?> 
    <a class="fb-connect" href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('logout'), 'redirect_uri' => $redirect, 'scope' => $fb_perms)); ?>">
        <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
    </a>
    <a class="tw-connect" href="<?php echo base_url(); ?>home/twit/landing/<?php echo $customs; ?>/0/<?php echo $spot_id; ?>">
        <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
    </a>
    <?php $spotss = array(466,468,470,472,474,476,478); if(in_array($spot_id, $spotss)): ?>	
    <br />
	<a href="<?php echo base_url(); ?>landing/step2?url=<?php echo $customs; ?>&acc=0&from=email&places=<?php echo $spot_id; ?>">
	.
	</a>
    <?php endif; ?>
    <div class="alert alert-warning small">By registering, I agree to provide my Name and Email address to the event organizers.</div>
	
</div>