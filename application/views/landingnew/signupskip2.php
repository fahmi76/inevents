<?php if ($from == 'fb'): ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var urlrfidhome = "<?php echo site_url('landing/landing/follow?url=' . $customs); ?>";
            var accountid = "<?php echo $id; ?>";

            $("#summary").hide();
            $("#id101").click(function () {
                $.ajax({
                    url: urlrfidhome,
                    type: "POST",
                    data: {
                        id: accountid,
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.nama == "yes")
                        {
                            $("#summary").show();
                            $("#summarys").hide();
                        }
                        else
                        {
                            $("#summarys").show();
                            $("#summary").hide();
                            return false;
                        }
                    }
                });

            });

        });
    </script>
<?php endif; ?>

<?php if ($from == 'tw'): ?>
    <script type="text/javascript">
        var windowSizeArray = ["width=800,height=600",
            "width=800,height=400,scrollbars=yes"];

        $(document).ready(function () {
            $('.newWindow').click(function (event) {

                var url = $(this).attr("href");
                var windowName = "popUp";//$(this).attr("name");
                var windowSize = windowSizeArray[$(this).attr("rel")];
                winRef = new Object();
                winRef = window.open(url, windowName, windowSize);

                event.preventDefault();
                setTimeout('winRef.close()', 10000);

            });
        });
    </script>
<?php endif; ?>
<h2>Hi, <?php echo $user_name; ?></h2>
<hr>
<div class="text-center">
    <?php if ($from == 'tw'): ?>
        <?php if ($fb_page): ?>
            <p>Want to get more info about this event?</p>
            <div class="twitter-follow">
                <?php if ($likefollow2 == 0): ?>					
					<?php if($spot_id == 1): ?>
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Facebarservices&amp;width&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:62px;" allowTransparency="true"></iframe>
                    <?php else: ?>
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcarnivalcityultrasportslounge&amp;width&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=504876302925193" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:62px;" allowTransparency="true"></iframe>
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fheineken&amp;width&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=580751025342598" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:62px;" allowTransparency="true"></iframe>
					<?php endif; ?>
					<!--
					<a href="<?php echo $fb_page; ?>" rel="0" class="newWindow" >Like Our Facebook Page</a>
                    <div class="fb-like-box" data-href="<?php echo $fb_page; ?>" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false">
                    </div> -->
                <?php else: ?>
                    <a href="#" style="color:green;">You Already Like Our Facebook Page</a>
                <?php endif; ?>
            </div>
            <br>
            <p>or finish right here.</p> 
        <?php else: ?>
            <p>finish right here.</p> 
        <?php endif; ?>
    <?php else: ?>		
        <?php if ($tw_page): ?>
            <p>Want to get more info about this event?</p>
            <div class="twitter-follow">
                <?php if ($likefollow2 == 1): ?>
                    <p><div id="summaryss"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" alt=""/>@<?php echo $tw_page; ?></div></p>
                <?php else: ?>
                    <p>	<div id="summarys"><a id="id101" href="#" >
                            <img src="http://wooz.in/assets/customs/Twitter_Follow.png" alt=""/></a> @<?php echo $tw_page; ?></div>
                    <div id="summary"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" alt=""/>@<?php echo $tw_page; ?></div>
                    </p>
                <?php endif; ?>
            </div>
            <br>
            <p>or finish right here.</p> 
        <?php else: ?>
            <p>finish right here.</p> 
        <?php endif; ?>		
    <?php endif; ?>
    <a class="reg-done" href="<?php echo site_url('landing/step4s?url=' . $url . '&places=' . $spot_id . '&from=' . $from . '&acc=' . $id . '&likefollow=' . $likefollow); ?>">
        <button type="button" class="btn btn-primary btn-xlg">Done</button>
    </a>
</div>