<h2>Hi, <?php echo $user_name; ?></h2>
<hr>
<div class="text-center">
    <?php if ($from == 'fb'): ?>
        <p>Also share your activity through your Twitter account?</p>
        <a class="tw-connect" href="<?php echo base_url(); ?>home/twit/landingtw2/<?php echo $customs ?>/<?php echo $id ?>/<?php echo $spot_id; ?>/<?php echo $likefollow; ?>">
            <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
    <?php elseif ($from == 'tw'): ?>
        <p>Also share your activity through your Facebook account?</p>	
        <?php  $redirect = base_url().'home/facebook/landingfb2/'.$customs.'/'.$id.'/'.$spot_id.'/'.$likefollow; ?>
        <a class="fb-connect" href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('logout'), 'redirect_uri' => $redirect, 'scope' => $fb_perms)); ?>">
            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
	<?php else: ?>
        <p>Also share your activity through your Twitter account?</p>
        <a class="tw-connect" href="<?php echo base_url(); ?>home/twit/landingtw2/<?php echo $customs ?>/<?php echo $id ?>/<?php echo $spot_id; ?>/<?php echo $likefollow; ?>">
            <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
		<p>or also share your activity through your Facebook account?</p>	
        <?php  $redirect = base_url().'home/facebook/landingfb2/'.$customs.'/'.$id.'/'.$spot_id.'/'.$likefollow; ?>
        <a class="fb-connect" href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('logout'), 'redirect_uri' => $redirect, 'scope' => $fb_perms)); ?>">
            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
    <?php endif; ?>   
    <p>or finish right here.</p> 
    <a class="reg-done" href="<?php echo site_url('landing/step5?url=' . $url . '&places=' . $spot_id . '&from=' . $from . '&acc=' . $id.'&likefollow='.$likefollow); ?>">
        <button type="button" class="btn btn-primary btn-xlg">Done</button>
    </a>
</div>