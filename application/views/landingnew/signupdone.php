<h2>Thank you</h2>
<hr>
<div class="text-center">
    <p>The registration process has been completed.</p>
    <br>
    <button type="button" class="btn btn-primary btn-xlg">Finish</button>
</div>
    <?php if($token): ?>
    <meta http-equiv="refresh" content="2;URL=<?php echo $this->facebook->getLogoutUrl(array('next' => site_url('landing/logout?url=' . $url . '&places=' . $spot_id), 'access_token' => $token)); ?>" />
    <?php else: ?>
    <meta http-equiv="refresh" content="2;URL=<?php echo site_url('landing/logout?url=' . $url . '&places=' . $spot_id); ?>" />
    <?php endif; ?>
<?php #endif; ?>
