
<div class="text-center">
    <h2 class="tap-wristband"><p id="tap-code"><span>GUEST:</span></p></h2>
<hr>
<div>
        <button type="button" id="yes" class="btn btn-primary btn-block btn-xlg" style="height:100px;font-size:40px;">YES</button>
        <br >
        <button type="button" id="no" class="btn btn-primary btn-block btn-xlg" style="height:100px;font-size:40px;">NO</button>
        <?php if($check == 1): ?>
        <br >
        <button type="button" id="later" class="btn btn-primary btn-block btn-xlg" style="height:100px;font-size:40px;">LATER</button>
        <br >
        <?php endif; ?>
</div>
<hr>
</div> 

<script type="text/javascript">
    $(function() {
        $('#yes').click(function() {
            window.location = '<?php echo base_url(); ?>gates/gateschoose/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>/<?php echo $account_id; ?>/1'
                    });
        $('#no').click(function() {
            window.location = '<?php echo base_url(); ?>gates/gateschoose/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>/<?php echo $account_id; ?>/2'
                    });
        $('#later').click(function() {
            window.location = '<?php echo base_url(); ?>gates/gateschoose/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>/<?php echo $account_id; ?>/3'
                    });
    });
</script>