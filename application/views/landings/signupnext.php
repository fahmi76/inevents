<script type="text/javascript">
    $(document).ready(function() {
        $("#InputEmail").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>

<?php if ($from == 'fb'): ?>
    <script type="text/javascript">
        var windowSizeArray = ["width=800,height=600",
            "width=800,height=400,scrollbars=yes"];

        $(document).ready(function() {
            $('.newWindow').click(function(event) {

                var url = $(this).attr("href");
                var windowName = "popUp";//$(this).attr("name");
                var windowSize = windowSizeArray[$(this).attr("rel")];
                winRef = new Object();
                winRef = window.open(url, windowName, windowSize);

                event.preventDefault();
                //setTimeout('winRef.close()',15000);

            });
        });
    </script>
<?php endif; ?>
<?php if ($from == 'tw'): ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var urlrfidhome = "<?php echo site_url('landings/follow/' . $customs); ?>";
            var accountid = "<?php echo $info->id; ?>";
            $("#summary").hide();
            $("#id101").click(function() {
                $.ajax({
                    url: urlrfidhome,
                    type: "POST",
                    data: {
                        id: accountid,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.nama == "yes")
                        {
                            $("#summary").show();
                            $("#summarys").hide();
                        }
                        else
                        {
                            $("#summarys").show();
                            $("#summary").hide();
                            return false;
                        }
                    }
                });

            });

        });
    </script>
<?php endif; ?>
<div class="panel panel-info">
        <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
    <?php if ($from == 'fb'): ?>
        <?php if ($fb_page): ?>
            <div class="panel-heading">"Like" <?php echo $tw_page; ?> Facebook Page</div>
            <div class="panel-body">
                <?php if ($likefollow == 0): ?>
                    <div class="fb-page" data-href="https://www.facebook.com/TRESemmeIndonesia" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/TRESemmeIndonesia"><a href="https://www.facebook.com/TRESemmeIndonesia">TRESemmé</a></blockquote></div></div>
                    <!--				
					<a href="<?php echo $fb_page; ?>" rel="0" class="newWindow" >Like Our Facebook Page</a>--> 
                <?php else: ?>
                    <a href="#" style="color:green;">You Already Like Our Facebook Page</a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($from == 'tw'): ?>	
        <?php if ($tw_page): ?>
            <div class="panel-heading">"Follow" <?php echo $tw_page; ?> Twitter Page</div>
            <div class="panel-body">
                <?php if ($likefollow == 1): ?>
                    <div id="summaryss" style="text-align: center;" style="width: 400px;"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" style="width: 76px;" alt=""/>@<?php echo $tw_page; ?></div>
                <?php else: ?>
                    <div id="summarys" style="text-align: center;" style="width: 400px;"><a id="id101" href="#" >
                            <img src="http://wooz.in/assets/customs/Twitter_Follow.png" style="width: 76px;" alt=""/></a>@<?php echo $tw_page; ?></div>
                    <div id="summary" style="text-align: center;" style="width: 400px;"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" style="width: 76px;" alt=""/>@<?php echo $tw_page; ?></div>
                <?php endif; ?>	
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">
    <div style="height:400px;overflow:scroll;overflow-x:hidden;">
        <div class="form-group">
            <label for="InputName">Full Name</label>        
            <?php
            $name = set_value('fullname');
            if ($name) {
                $names = $name;
            } elseif (!$info) {
                $names = '';
            } else {
                $names = $info->account_displayname;
            }
            ?>
            <input type="text" class="form-control input-lg" name="fullname" id="InputName" autocomplete="off" value="<?php echo $names; ?>" placeholder="your name">
        </div>
            <?php if ($from == 'fb' || $from == 'tw'): ?>
            <div class="form-group">
                <?php
                $fbname = set_value('sosmed');
                if ($from == 'fb') {
                    ?>
                    <label for="InputName">Facebook Account Name</label> 
                    <?php
                    $sosmednames = $info->account_fb_name;
                } else {
                    ?>
                    <label for="InputName">Twitter Account Name</label>
                    <?php
                    $sosmednames = '@' . $info->account_tw_username;
                }
                ?>
                <input type="text" class="form-control input-lg" id="sosmed" name="sosmed" value="<?php echo $sosmednames; ?>" disabled />
            </div>
        <?php endif; ?>

        <div class="form-group">
            <label for="InputEmail">Email Address</label>
            <?php
            $email = set_value('email');
            if ($email) {
                $emails = $email;
            } elseif (!$info) {
                $emails = '';
            } else {
                $emails = $info->account_email;
            }
            ?>
            <input type="email" class="form-control input-lg" name="email" id="InputEmail" autocomplete="off" value="<?php echo $emails; ?>" placeholder="youremail@youremaildomain.com">
        </div>
        <div class="form-group">
            <label for="InputGender">Gender</label>
            <br>
            <?php
            $gender = set_value('gender');
            if ($gender) {
                $genders = $gender;
            } elseif (!$info) {
                $genders = '';
            } else {
                $genders = $info->account_gender;
            }
            ?>
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-warning <?php if ($genders == "male") : ?>active<?php endif; ?>">
                    <input type="radio" name="gender" id="InputGenderMale" value="male" <?php if ($genders == "male") : ?>checked="checked"<?php endif; ?>> Male
                </label>
                <label class="btn btn-warning <?php if ($genders == "female") : ?>active<?php endif; ?>">
                    <input type="radio" name="gender" id="InputGenderFemale" value="female" <?php if ($genders == "female") : ?>checked="checked"<?php endif; ?>> Female
                </label>
            </div>
        </div>  
        <div class="form-group">
            <label for="InputBirth">Birthdate</label>

            <?php
            if ($info) {
                $birthdate = explode('-', $info->account_birthdate);
            } else {
                $birthdate[2] = 00;
                $birthdate[0] = 0000;
                $birthdate[1] = 0;
            }

            $tgl = set_value('tgl');
            if ($tgl) {
                $tgls = $tgl;
            } elseif ($birthdate[2] == 00) {
                $tgls = '';
            } else {
                $tgls = $birthdate[2];
            }
            $thn = set_value('thn');
            if ($thn) {
                $thns = $thn;
            } elseif ($birthdate[0] == 0000) {
                $thns = '';
            } else {
                $thns = $birthdate[0];
            }
            $bln = set_value('bln');
            if ($bln) {
                $blns = $bln;
            } else {
                $blns = $birthdate[1];
            }
            ?>
            <div class="form-inline">
                <div class="form-group" style="width: 65px">
                    <label class="sr-only" for="InputDate">Date</label>
                    <input type="text" class="form-control input-lg" id="InputDate" name="tgl" autocomplete="off" value="<?php echo $tgls; ?>" placeholder="Tgl">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="InputMonth">Month</label>
                    <select class="form-control input-lg" name="bln" id="InputMonth">
                        <option value="0">--Month--</option>
                        <option value="1" <?php if ($blns == 1) : ?>selected="selected"<?php endif; ?>>January</option>
                        <option value="2" <?php if ($blns == 2) : ?>selected="selected"<?php endif; ?>>February</option>
                        <option value="3" <?php if ($blns == 3) : ?>selected="selected"<?php endif; ?>>March</option>
                        <option value="4" <?php if ($blns == 4) : ?>selected="selected"<?php endif; ?>>April</option>
                        <option value="5" <?php if ($blns == 5) : ?>selected="selected"<?php endif; ?>>May</option>
                        <option value="6" <?php if ($blns == 6) : ?>selected="selected"<?php endif; ?>>June</option>
                        <option value="7" <?php if ($blns == 7) : ?>selected="selected"<?php endif; ?>>July</option>
                        <option value="8" <?php if ($blns == 8) : ?>selected="selected"<?php endif; ?>>August</option>
                        <option value="9" <?php if ($blns == 9) : ?>selected="selected"<?php endif; ?>>September</option>
                        <option value="10" <?php if ($blns == 10) : ?>selected="selected"<?php endif; ?>>October</option>
                        <option value="11" <?php if ($blns == 11) : ?>selected="selected"<?php endif; ?>>November</option>
                        <option value="12" <?php if ($blns == 12) : ?>selected="selected"<?php endif; ?>>December</option>
                    </select> 
                </div> 
                <div class="form-group" style="width: 100px">
                    <label class="sr-only" for="InputYear">Year</label>
                    <input type="text" class="form-control input-lg" id="InputYear" name="thn" autocomplete="off" value="<?php echo $thns; ?>" placeholder="Tahun">
                </div>                   
            </div>
            <div class="form-group">
                <?php
                $telp = set_value('telp');
                if ($telp) {
                    $telps = $telp;
                } elseif (!$info) {
                    $telps = '';
                } else {
                    if($info->account_phone == 0){
                        $telps = '';
                    }else{
                        $telps = $info->account_phone;
                    }
                }
                ?>
                <label for="InputName">Phone Number</label>       
                <input type="text" class="form-control input-lg" name="telp" id="InputTelp" value="<?php echo $telps; ?>"  placeholder="087532423">
            </div>	
        <div class="text-center">            
            <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
            <?php if ($from == 'fb'): ?>
                <a href="<?php echo $logout; ?>">
                    <button type="button" class="btn btn-default btn-xlg">Cancel</button>
                </a>
            <?php else: ?>
                <a href="<?php echo base_url('landing/logout?url=' . $customs . '&places=' . $spot_id); ?>">
                    <button type="button" class="btn btn-default btn-xlg">Cancel</button>
                </a>
            <?php endif; ?>
        </div>
    </div>
</form>