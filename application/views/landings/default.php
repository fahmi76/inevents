<?php if (isset($retry) && $retry === true) { ?>
    <p class="error">Please Try Again <?php if (isset($status) && $status != '') echo 'due to ' . str_replace('_', ' ', $status) ?> </p>
<?php } ?>
<div class="text-center">
    <h3>Click Below to Register</h3>
    <?php if(in_array('1', $regis_type)): ?>	
	<a class="fb-connect" href="<?php echo $login; ?>">
            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
	</a>
    <?php endif; ?>
    <?php if(in_array('1', $regis_type)): ?>	
    <a class="tw-connect" href="<?php echo base_url(); ?>home/twitter/landings/home/<?php echo $customs; ?>/0/0/tw">
        <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
    </a>
    <?php endif; ?>
    <?php if(in_array('4', $regis_type)): ?>	
	<br />
	<a href="<?php echo base_url(); ?>landings/step2?url=<?php echo $customs; ?>&acc=0&from=email&places=<?php echo $spot_id; ?>">
	.
	</a>
    <?php endif; ?>
    <div class="alert alert-warning small">By registering, I agree to provide my Name and Email address to the event organizers.</div>
    
</div>