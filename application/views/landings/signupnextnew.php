<script type="text/javascript">
    $(document).ready(function() {
        $("#rfidNumber").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
    //document.forms['input_submit'].style.visibility = "hidden";
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function autosubmit(original) {
        if (original.value.length === 10) {
            document.getElementById('registration').submit();
        }
    }
</script>

<?php if ($from == 'fb'): ?>
    <script type="text/javascript">
        var windowSizeArray = ["width=800,height=600",
            "width=800,height=400,scrollbars=yes"];

        $(document).ready(function() {
            $('.newWindow').click(function(event) {

                var url = $(this).attr("href");
                var windowName = "popUp";//$(this).attr("name");
                var windowSize = windowSizeArray[$(this).attr("rel")];
                winRef = new Object();
                winRef = window.open(url, windowName, windowSize);

                event.preventDefault();
                //setTimeout('winRef.close()',15000);

            });
        });
    </script>
<?php endif; ?>
<?php if ($from == 'tw'): ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var urlrfidhome = "<?php echo site_url('landings/follow/' . $customs); ?>";
            var accountid = "<?php echo $info->id; ?>";
            $("#summary").hide();
            $("#id101").click(function() {
                $.ajax({
                    url: urlrfidhome,
                    type: "POST",
                    data: {
                        id: accountid,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.nama == "yes")
                        {
                            $("#summary").show();
                            $("#summarys").hide();
                        }
                        else
                        {
                            $("#summarys").show();
                            $("#summary").hide();
                            return false;
                        }
                    }
                });

            });

        });
    </script>
<?php endif; ?>
<div class="panel panel-info">
        <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
    <?php if ($from == 'fb'): ?>
        <?php if ($fb_page): ?>
            <div class="panel-heading">"Like" <?php echo $tw_page; ?> Facebook Page</div>
            <div class="panel-body">
                    <div class="fb-page" data-href="<?php echo $fb_page; ?>" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $fb_page; ?>"><a href="<?php echo $fb_page; ?>">Like</a></blockquote></div></div>
                    <!--				
					<a href="<?php echo $fb_page; ?>" rel="0" class="newWindow" >Like Our Facebook Page</a>--> 
                
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($from == 'tw'): ?>	
        <?php if ($tw_page): ?>
            <div class="panel-heading">"Follow" <?php echo $tw_page; ?> Twitter Page</div>
            <div class="panel-body">
                <?php if ($likefollow == 1): ?>
                    <div id="summaryss" style="text-align: center;" style="width: 400px;"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" style="width: 76px;" alt=""/>@<?php echo $tw_page; ?></div>
                <?php else: ?>
                    <div id="summarys" style="text-align: center;" style="width: 400px;"><a id="id101" href="#" >
                            <img src="http://wooz.in/assets/customs/Twitter_Follow.png" style="width: 76px;" alt=""/></a>@<?php echo $tw_page; ?></div>
                    <div id="summary" style="text-align: center;" style="width: 400px;"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" style="width: 76px;" alt=""/>@<?php echo $tw_page; ?></div>
                <?php endif; ?>	
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">
        <div class="form-group">
            <label for="InputName">Full Name</label>        
            <?php
            $name = set_value('fullname');
            if ($name) {
                $names = $name;
            } elseif (!$info) {
                $names = '';
            } else {
                $names = $info->account_displayname;
            }
            ?>
            <input type="text" class="form-control input-lg" name="fullname" id="InputName" autocomplete="off" value="<?php echo $names; ?>" placeholder="your name">
        </div>
            <?php if ($from == 'fb' || $from == 'tw'): ?>
            <div class="form-group">
                <?php
                $fbname = set_value('sosmed');
                if ($from == 'fb') {
                    ?>
                    <label for="InputName">Facebook Account Name</label> 
                    <?php
                    $sosmednames = $info->account_fb_name;
                } else {
                    ?>
                    <label for="InputName">Twitter Account Name</label>
                    <?php
                    $sosmednames = '@' . $info->account_tw_username;
                }
                ?>
                <input type="text" class="form-control input-lg" id="sosmed" name="sosmed" value="<?php echo $sosmednames; ?>" disabled />
            </div>
        <?php endif; ?>

        <div class="form-group">
            <label for="rfidNumber">RFID Number</label>   
            <input type="text" name="serial" onblur="displayResult(this);" class="form-control input-lg" id="rfidNumber" placeholder="Your RFID number" autocomplete="off">
        </div>
        <div class="text-center">            
            <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
            <?php if ($from == 'fb'): ?>
                <a href="<?php echo $logout; ?>">
                    <button type="button" class="btn btn-default btn-xlg">Cancel</button>
                </a>
            <?php else: ?>
                <a href="<?php echo base_url('landing/logout?url=' . $customs . '&places=' . $spot_id); ?>">
                    <button type="button" class="btn btn-default btn-xlg">Cancel</button>
                </a>
            <?php endif; ?>
        </div>
</form>