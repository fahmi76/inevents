<h2>Hi, <?php echo $info->account_displayname; ?></h2>
<hr>
<div class="text-center">
    <?php if ($from == 'fb'): ?>
        <p>Also share your activity through your Twitter account?</p>
        <a class="tw-connect" href="<?php echo base_url(); ?>home/twitter/landings/step4/<?php echo $customs; ?>/<?php echo $info->id; ?>/0/fb">
         <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
    <?php elseif ($from == 'tw'): ?>
		<a class="fb-connect" href="<?php echo $login; ?>">
	            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
		</a>
	<?php else: ?>
        <p>Also share your activity through your Twitter account?</p>
        <a class="tw-connect" href="<?php echo base_url(); ?>home/twit/landingnewtw2/<?php echo $customs ?>/<?php echo $id ?>/<?php echo $spot_id; ?>/<?php echo $likefollow; ?>">
            <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
		<p>or also share your activity through your Facebook account?</p>	
        <a class="fb-connect" href="<?php echo $login; ?>">
            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
    <?php endif; ?>   
    <p>or finish right here.</p> 
	
    <a class="reg-done" href="<?php echo site_url('landings/share/'.$places.'/'.$info->id.'/'.$landingid.'/'.$from.'/2'); ?>">
    <!--
    <a class="reg-done" href="<?php echo site_url('landings/step6/'.$places.'/'.$info->id.'/'.$landingid.'/'.$from); ?>"> -->
        <button type="button" class="btn btn-primary btn-xlg">Done</button>
    </a>
</div>