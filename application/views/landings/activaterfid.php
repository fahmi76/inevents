<script type="text/javascript">
    $(document).ready(function() {
        $("#rfidNumber").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
    //document.forms['input_submit'].style.visibility = "hidden";
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function autosubmit(original) {
        if (original.value.length === 10) {
            document.getElementById('registration').submit();
        }
    }
</script>
<h2>Hello, <?php echo $info->account_displayname; ?></h2>
<br>
<div class="panel panel-info">
    <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
            <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
</div>

<div class="alert alert-danger">Fill your RFID now.</div>

<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">

		<div class="form-group">
			<label for="rfidNumber"  class="sr-only">RFID Number</label>
			<input type="text" name="serial" class="form-control input-lg" id="rfidNumber" placeholder="Your RFID number" autocomplete="off">
		</div>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Activate</button>
    </div>
</form>