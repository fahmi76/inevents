<?php if ($from == 'fb'): ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var urlrfidhome = "<?php echo site_url('landings/follow/' . $customs); ?>";
            var accountid = "<?php echo $info->id; ?>";

            $("#summary").hide();
            $("#id101").click(function() {
                $.ajax({
                    url: urlrfidhome,
                    type: "POST",
                    data: {
                        id: accountid,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.nama == "yes")
                        {
                            $("#summary").show();
                            $("#summarys").hide();
                        }
                        else
                        {
                            $("#summarys").show();
                            $("#summary").hide();
                            return false;
                        }
                    }
                });

            });

        });
    </script>
<?php endif; ?>

<?php if ($from == 'tw'): ?>
    <script type="text/javascript">
        var windowSizeArray = ["width=800,height=600",
            "width=800,height=400,scrollbars=yes"];

        $(document).ready(function() {
            $('.newWindow').click(function(event) {

                var url = $(this).attr("href");
                var windowName = "popUp";//$(this).attr("name");
                var windowSize = windowSizeArray[$(this).attr("rel")];
                winRef = new Object();
                winRef = window.open(url, windowName, windowSize);

                event.preventDefault();
                setTimeout('winRef.close()', 10000);

            });
        });
    </script>
<?php endif; ?>
<h2>Hi, <?php echo $info->account_displayname; ?></h2>
<hr>
<div class="text-center">
        <?php if ($from == 'tw'): ?>
			<?php if($fb_page): ?>
                <p>Want to get more info about this event?</p>
                <div class="twitter-follow">
				<?php if ($likefollow == 0): ?>
                    <div class="fb-page" data-href="<?php echo $fb_page; ?>" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $fb_page; ?>"><a href="<?php echo $fb_page; ?>">Like</a></blockquote></div></div>
                    
                    <!--                
                    <a href="<?php echo $fb_page; ?>" rel="0" class="newWindow" >Like Our Facebook Page</a>--> 
				<?php else: ?>
					<a href="#" style="color:green;">You Already Like Our Facebook Page</a>
				<?php endif; ?>
                </div>
                <br>
                <p>or finish right here.</p> 
	       <?php else: ?>
            <p>finish right here.</p> 
			<?php endif; ?>
        <?php else: ?>
			<?php if($tw_page): ?>
                <p>Want to get more info about this event?</p>
                <div class="twitter-follow">
				<?php if ($likefollow == 1): ?>
					<p><div id="summaryss"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" alt=""/>@<?php echo $tw_page; ?></div></p>
				<?php else: ?>
					<p>	<div id="summarys"><a id="id101" href="#" >
						<img src="http://wooz.in/assets/customs/Twitter_Follow.png" alt=""/></a> @<?php echo $tw_page; ?></div>
					<div id="summary"><img src="http://wooz.in/assets/customs/Twitter_Follow_BW.png" alt=""/>@<?php echo $tw_page; ?></div>
					</p>
				<?php endif; ?>
            </div>
            <br>
            <p>or finish right here.</p> 
	<?php else: ?>
    <p>finish right here.</p> 
            <?php endif; ?>
        <?php endif; ?>
    
    <a class="reg-done" href="<?php echo site_url('landings/share/'.$places.'/'.$info->id.'/'.$landingid.'/'.$from.'/2'); ?>">
    <!--
    <a class="reg-done" href="<?php echo site_url('landings/step6/'.$places.'/'.$info->id.'/'.$landingid.'/'.$from); ?>"> -->
        <button type="button" class="btn btn-primary btn-xlg">Done</button>
    </a>
</div>