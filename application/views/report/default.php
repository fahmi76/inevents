<?php
		$places = $this->db->get_where('places', array('places_landing' => $event))->row();
?>
<!-- Title Page -->
<div class="titlepage">
    <h3>Welcome Event Statistik</h3>
</div>
<!-- /Title Page -->

<div class="tablecontenttop">
    <h4 class="titletable">Statistik on <?php echo $places->places_name; ?></h4>
</div>

<!-- table content middle -->
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>Report</strong>
                </th>
<?php
		$startTime = strtotime($places->places_startdate); 
		$endTime = strtotime($places->places_duedate); 
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i); // 2010-05-01, 2010-05-02, etc
?>
                <th>
                    <strong>Day <?php echo $y; ?></strong>
                </th>
<?php
			$y++;
		}
?>
                <th>
                    <strong>Total</strong>
                </th>
            </tr>
        </thead>
		<!--
        <tbody>
		
			<tr class="oddrow">
			<td class="firstcol">Registration</td>
<?php
		//registrasi
		$startTime = strtotime($places->places_startdate); 
		$endTime = strtotime($places->places_duedate); 
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i);
			$this->db->select('count(distinct(wooz_account.id)) as total');
			$this->db->join('landing', 'landing.account_id = account.id', 'left');
			$this->db->where("landing.landing_register_form", $places->id);
			$this->db->where("landing.landing_joindate like", '%'.$thisDate.'%');
			$this->db->from('account');
			$total = $this->db->get()->row();
			?>
			<td class="dataitem"><?php echo $total->total; ?></td>
<?php
			$y++;
		}

		$this->db->select('count(wooz_account.id) as total');
        $this->db->join('landing', 'landing.account_id = account.id', 'left');
        $this->db->where("landing.landing_register_form", $places->id);
		$this->db->where("landing.landing_joindate between '" . $places->places_startdate . "' and '" . $places->places_duedate . "'");
        $this->db->from('account');
        $clear = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $clear->total; ?></td>
			</tr> 
		 </tbody>
        <tbody>
		-->
			<tr class="oddrow">
			<td class="firstcol">Photo</td>
<?php
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i); 
			$this->db->select('count(id) as total');
			$this->db->where("photos.places_id", $places->id);
			$this->db->where("photos.time_upload like", '%'.$thisDate.'%');
			$this->db->from('photos');
			$total = $this->db->get()->row();
			?>
			<td class="dataitem"><?php echo $total->total; ?></td>
<?php
			$y++;
		}

		$this->db->select('count(id) as total');
		$this->db->where("photos.places_id", $places->id);
		$this->db->where("photos.time_upload >", $places->places_startdate. ' 00:00:00');
		$this->db->where("photos.time_upload <=", $places->places_duedate. ' 23:59:00');
		$this->db->from('photos');
		$total = $this->db->get()->row();
		
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
			</tr> 
		 </tbody>
<?php if($event == 'yamaha'): ?>
        <tbody>
		
			<tr class="oddrow">
			<td class="firstcol">AR Photo Using fan page Yamaha</td>
<?php
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i); 
			$this->db->select('count(id) as total');
			$this->db->where("photos.places_id", 229);
		$this->db->where("photos.account_id", 28176);
			$this->db->where("photos.time_upload like", '%'.$thisDate.'%');
			$this->db->from('photos');
			$total = $this->db->get()->row();
			?>
			<td class="dataitem"><?php echo $total->total; ?></td>
<?php
			$y++;
		}

		$this->db->select('count(id) as total');
		$this->db->where("photos.places_id", 229);
		$this->db->where("photos.account_id", 28176);
		$this->db->where("photos.time_upload >", $places->places_startdate. ' 00:00:00');
		$this->db->where("photos.time_upload <=", $places->places_duedate. ' 23:59:00');
		$this->db->from('photos');
		$total = $this->db->get()->row();
		
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
			</tr> 
		 </tbody>
        <tbody>
		
			<tr class="oddrow">
			<td class="firstcol">AR Photo Not Using fan page Yamaha</td>
<?php
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i); 
			$this->db->select('count(id) as total');
			$this->db->where("photos.places_id", 229);
		$this->db->where("photos.account_id !=", 28176);
			$this->db->where("photos.time_upload like", '%'.$thisDate.'%');
			$this->db->from('photos');
			$total = $this->db->get()->row();
			?>
			<td class="dataitem"><?php echo $total->total; ?></td>
<?php
			$y++;
		}

		$this->db->select('count(id) as total');
		$this->db->where("photos.places_id", 229);
		$this->db->where("photos.account_id !=", 28176);
		$this->db->where("photos.time_upload >", $places->places_startdate. ' 00:00:00');
		$this->db->where("photos.time_upload <=", $places->places_duedate. ' 23:59:00');
		$this->db->from('photos');
		$total = $this->db->get()->row();
		
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
			</tr> 
		 </tbody>
<?php endif; ?>
        <tbody>

<?php
		//update status
        $this->db->select('');
        $this->db->where('places_parent', $places->id);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $family = $this->db->get()->result();
		#$family = $this->convert->family($places->id);

		if(count($family) >= 1){
			foreach($family as $id){
				$places1 = $this->db->get_where('places', array('id' => $id->id))->row();
?>
        <tbody>
		
			<tr class="oddrow">
			<td class="firstcol">Update status di <?php echo $places1->places_name; ?></td>
<?php
				$startTime = strtotime($places->places_startdate); 
				$endTime = strtotime($places->places_duedate); 

				$y = 1;
				for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
					$thisDate = date('Y-m-d', $i); 
					$this->db->select('count(id) as total');
					$this->db->where("log.places_id", $places1->id);
					$this->db->where("log.log_stamps like", '%'.$thisDate.'%');
					$this->db->from('log');
					$total = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
<?php
					$y++;
				}

				$this->db->select('count(id) as total');
				$this->db->where("log.places_id", $places1->id);
				$this->db->where("log.log_stamps >", $places1->places_startdate. ' 00:00:00');
				$this->db->where("log.log_stamps <=", $places1->places_duedate. ' 23:59:00');
				$this->db->from('log');
				$total = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
			</tr> 
		 </tbody>
        <tbody>
<?php
			}
		}
?>

    </table>

<?php
		//Top Ten photo
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i); 
			$this->db->select(' wooz_account.account_email, wooz_account.account_displayname, wooz_account.account_fbid, count( wooz_photos.`account_id` ) AS total');
			$this->db->join('account', 'account.id = photos.account_id', 'left');
			$this->db->where("photos.time_upload like", '%'.$thisDate.'%');
			$this->db->where("photos.places_id", $places->id);
			$this->db->from('photos');
			$this->db->having('total > 1'); 
			$this->db->order_by("total", "desc");
			$this->db->group_by('account.account_displayname');
			$this->db->limit(10);
			$total = $this->db->get()->result();
			if(!empty($total)){
				echo '<table border="1">';
				echo '</br>';
				echo '<font size=4>Top Ten People Photo Day '.$y. '</font>';
				echo '<table border="1">';
				echo '<tr>';
				echo '<th>No</th>';
				echo '<th>Nama</th>';
				echo '<th>Email</th>';
				echo '<th>Link Facebook</th>';
				echo '<th>Total</th>';
				echo '</tr>';
				$x = 1;
				foreach ($total as $row){
					echo '<th>'.$x.'</th>';
					echo '<th>'.$row->account_displayname.'</th>';
					echo '<th>'.$row->account_email.'</th>';
					echo '<th><a href="https://www.facebook.com/profile.php?id='.$row->account_fbid.'"> https://www.facebook.com/profile.php?id='.$row->account_fbid.'</a></th>';
					echo '<th>'.$row->total.'</th></tr>';
					$x++;
				}
			}
			echo '</table>';
			$y++;
		}
		
		$this->db->select(' wooz_account.account_email, wooz_account.account_displayname, wooz_account.account_fbid, count( wooz_photos.`account_id` ) AS total');
		$this->db->join('account', 'account.id = photos.account_id', 'left');
		$this->db->where("photos.places_id", $places->id);
		$this->db->where("photos.time_upload >", $places->places_startdate. ' 00:00:00');
		$this->db->where("photos.time_upload <=", $places->places_duedate. ' 23:59:00');
		$this->db->from('photos');
		$this->db->having('total > 1'); 
		$this->db->order_by("total", "desc");
		$this->db->group_by('account.account_displayname');
		$this->db->limit(10);
		$total = $this->db->get()->result();
		if(!empty($total)){
			echo '<table border="1">';
			echo '</br>';
			echo '<font size=4>Top Ten People Photo</font>';
			echo '<tr>';
			echo '<th>No</th>';
			echo '<th>Nama</th>';
			echo '<th>Email</th>';
			echo '<th>Link Facebook</th>';
			echo '<th>Total</th>';
			echo '</tr>';
			$x = 1;
			foreach ($total as $row){
				echo '<th>'.$x.'</th>';
				echo '<th>'.$row->account_displayname.'</th>';
				echo '<th>'.$row->account_email.'</th>';
				echo '<th><a href="https://www.facebook.com/profile.php?id='.$row->account_fbid.'">https://www.facebook.com/profile.php?id='.$row->account_fbid.'</a></th>';
				echo '<th>'.$row->total.'</th></tr>';
				$x++;
			}
		}
		echo '</table>';

		//Top Ten Update Status

		if(count($family) >= 1){
			foreach($family as $id){
				$places1 = $this->db->get_where('places', array('id' => $id->id))->row();

				$startTime = strtotime($places->places_startdate); 
				$endTime = strtotime($places->places_duedate); 

				$y = 1;
				for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
					$thisDate = date('Y-m-d', $i); 
					$this->db->select('wooz_account.account_email, wooz_account.account_displayname, wooz_account.account_fbid, count( wooz_log.`account_id` ) AS total');
					$this->db->join('account', 'account.id = log.account_id', 'left');
					$this->db->where("log.log_stamps like", '%'.$thisDate.'%');
					$this->db->where("log.places_id", $places1->id);
					$this->db->from('log');
					$this->db->having('total > 1'); 
					$this->db->order_by("total", "desc");
					$this->db->group_by('account.account_displayname');
					$this->db->limit(10);
					$total = $this->db->get()->result();
			
					if(!empty($total)){
						echo '<table border="1">';
						echo '</br>';
						echo '<font size=4>Top Ten People Update Status on '.$places1->places_name.' Day '.$y.'</font>';
						echo '<table border="1">';
						echo '<tr>';
						echo '<th>No</th>';
						echo '<th>Nama</th>';
						echo '<th>Email</th>';
						echo '<th>Link Facebook</th>';
						echo '<th>Total</th>';
						echo '</tr>';
						$x = 1;
						foreach ($total as $row){
							echo '<th>'.$x.'</th>';
							echo '<th>'.$row->account_displayname.'</th>';
							echo '<th>'.$row->account_email.'</th>';
							echo '<th><a href="https://www.facebook.com/profile.php?id='.$row->account_fbid.'">https://www.facebook.com/profile.php?id='.$row->account_fbid.'</a></th>';
							echo '<th>'.$row->total.'</th></tr>';
							$x++;
						}
					}
					echo '</table>';
					$y++;
				}

				$this->db->select('wooz_account.account_email, wooz_account.account_displayname, wooz_account.account_fbid, count( wooz_log.`account_id` ) AS total');
				$this->db->join('account', 'account.id = log.account_id', 'left');
				$this->db->where("log.places_id", $places1->id);
				$this->db->from('log');
				$this->db->having('total > 1'); 
				$this->db->order_by("total", "desc");
				$this->db->group_by('account.account_displayname');
				$this->db->limit(10);
				$total = $this->db->get()->result();
				if(!empty($total)){
					echo '<table border="1">';
					echo '</br>';
					echo '<font size=4>Top Ten People Update Status on '.$places1->places_name.'</font>';
					echo '<table border="1">';
					echo '<tr>';
					echo '<th>No</th>';
					echo '<th>Nama</th>';
					echo '<th>Email</th>';
					echo '<th>Link Facebook</th>';
					echo '<th>Total</th>';
					echo '</tr>';
					$x = 1;
					foreach ($total as $row){
						echo '<th>'.$x.'</th>';
						echo '<th>'.$row->account_displayname.'</th>';
						echo '<th>'.$row->account_email.'</th>';
						echo '<th><a href="https://www.facebook.com/profile.php?id='.$row->account_fbid.'">https://www.facebook.com/profile.php?id='.$row->account_fbid.'</a></th>';
						echo '<th>'.$row->total.'</th></tr>';
						$x++;
					}
				}
				echo '</table>';

			}
		}

	
?>

</div>
<!-- table content middle -->        

<div class="tablecontentbottom">&nbsp;</div> 

