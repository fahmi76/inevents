<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title><?php if(isset($title)&&$title!='') echo $title; ?></title>
        <meta name="description" content="Wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity" />

		<script src="http://wooz.in/assets/javascripts/jquery-1.7.1.min.js"></script> 
        <link rel="stylesheet" media="all" href="http://wooz.in/assets/customs/style1.css"/>
		<style type="text/css">
			body {
				background: #FFF;
				color: #000;
			}
			a {
				color:#656565; 
			}

		</style>
    </head>

    <body>

	<div id="container">

		<div id="header">
		</div>
		<div id="main-box">
			<?php echo $TContent;?>
		</div>
		
		<div id="footer">
			<a href="http://wooz.in" id="smalllogo">
				<img src="http://wooz.in/assets/customs/smalllogo.png" alt="wooz.in" />
			</a>
			<a href="<?php echo current_url(); ?>" class="home">Home</a> |
			<a href="http://wooz.in/about">About</a> |
			<a href="http://wooz.in/blog">Blog</a> |
			<a href="http://wooz.in/privacy">Privacy Policy</a> |
			<a href="http://wooz.in/termsofservice">Terms Of Service</a>
			<br />
			Wooz.in is a web based application using RFID Owned by BIRA.<br />
			&copy; 2012 wooz.in (Patent Pending). All rights reserved.
		</div>
		<div class="clear"></div>
	</div>

	</body>
</html>
