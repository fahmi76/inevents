<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8"/>
        <title>Home - Wooz.in</title>
        <meta name="description" content="Wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity on Johnnie Walker Jet Black Party 2011 Singapore" />
        <link rel="stylesheet" media="all" href="http://wooz.in/assets/customs/stylegad.css"/>
		<script src="http://wooz.in/assets/javascripts/jquery-1.7.1.min.js"></script> 
        
		<style type="text/css">
			body {
				background: #000;
				color: #000;
			}
			a {
				color:#000;
				text-decoration: underline; 
			}
			a:hover {
				color:#555; 
				text-decoration: none;
			}
			#container {
				background: url(http://wooz.in/uploads/apps/gad2012/landing-JKT.jpg);
				width: 1024px;
				margin-top: 10px;
				background-repeat: no-repeat;
				background-position: right;
				position:relative;
				border: none;
			} 
			#section-top {
				text-indent:0;
			}
			#section-side {
				text-indent:0;
			}
			#main-box {
				text-shadow: 0px 1px 2px rgba(255, 255, 255, 1);
			}
			#main-box .text-box {
				background: rgba(46, 46, 46, 0.7);
				padding: 10px;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
				border-radius: 5px;
			}
			#footer {
				width:100%;
				margin-top: 20px;
				color: #000;
				text-shadow: none;
			}
			#footer a {
				color: #000;
				text-shadow: none;
			}
			#txt_footer {
				font-family: Helvetica, Arial, sans-serif;
				font-size: 12px;
				color: #000;	
			}
			.error { color: #fff; text-shadow: none; background: rgba(255, 0, 0, 0.5); border: 1px dashed #fff; }

		</style>
    </head>

    <body>

	<div id="container">
		<div id="logo" style=" width: 350px;">
    	<img src="http://wooz.in/uploads/apps/gad2012/logo1.png" height="203" width="344" alt="">
      <div id="main-box">
        <div class="text-box">
			<?php echo $TContent;?>
        </div>
        </div>   
		</div>
		
        <div id="footer">  
			<div style="padding-top: 150px;">
				<div class="footer-logo">
					<img src="http://wooz.in/uploads/apps/gad2012/guinness.png" height="32" width="155" alt="#">
				</div>
				<a href="#" id="smalllogo">
					<img src="http://wooz.in/uploads/apps/gad2012/smalllogo.png" alt="wooz.in" />
				</a>
				<a href="index.html" class="home"><span>Home</span></a> |
				<a href="about.html"><span>About</span></a> |
				<a href="#"><span>Blog</span></a> |
				<a href="privacy.html"><span>Privacy Policy</span></a> |
				<a href="terms.html"><span>Terms Of Service</span></a>
				<br />
				<span id="txt_footer">
				Wooz.in is a web based application using RFID Owned by BIRA.<br />
				&copy; 2010 wooz.in (Patent Pending). All rights reserved.</span>
			</div>
		</div>
    </div>
	</div>
</body>
</html>