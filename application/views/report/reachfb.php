<?php
		$places = $this->db->get_where('places', array('places_landing' => $event))->row();
?>
<!-- Title Page -->
<div class="titlepage">
    <h3>Welcome Event Statistik Facebook Reach</h3>
</div>
<!-- /Title Page -->

<div class="tablecontenttop">
    <h4 class="titletable">Statistik on <?php echo $places->places_name; ?></h4>
</div>

<!-- table content middle -->
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>Report</strong>
                </th>
                <th>
                    <strong>Total</strong>
                </th>

                <th>
                    <strong>FB Reach</strong>
                </th>
                <th>
                    <strong>TW Reach</strong>
                </th>
            </tr>
        </thead>

<?php
		//update status
        $this->db->select('');
        $this->db->where('places_parent', $places->id);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $family = $this->db->get()->result();
		#$family = $this->convert->family($places->id);

		if(count($family) >= 1){
			foreach($family as $id){
				$places1 = $this->db->get_where('places', array('id' => $id->id))->row();
?>
        <tbody>
		
			<tr class="oddrow">
			<td class="firstcol">Update status di <?php echo $places1->places_name; ?></td>
<?php

				$this->db->select('count(id) as total');
				$this->db->where("log.places_id", $places1->id);
				$this->db->from('log');
				$total = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
<?php
			$this->db->select('distinct(wooz_landing.account_id),landing.landing_fb_friends , landing.landing_tw_friends');
			$this->db->join('log', 'log.account_id = landing.account_id', 'left');
			$this->db->where("log.places_id", $places1->id);
			$this->db->where("landing.landing_register_form", $places1->places_parent);
			$this->db->from('landing');
			$totalreach = $this->db->get()->result();

			foreach($totalreach as $about){	
					$twfriend[] = $about->landing_tw_friends;
					$fbfriend[] = $about->landing_fb_friends;
			}
?>
			<td class="dataitem"><?php echo array_sum($fbfriend); ?></td>
			<td class="dataitem"><?php echo array_sum($twfriend); ?></td>

			</tr> 
		 </tbody>
        <tbody>
<?php
			}
		}
?>

    </table>

</div>
<!-- table content middle -->        

<div class="tablecontentbottom">&nbsp;</div> 

