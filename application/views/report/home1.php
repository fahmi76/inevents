<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Report System</title>
        <link rel="icon" type="image/ico" href="favicon.ico" />

        <script language="JavaScript" type="text/javascript">
            site_url = '<?php echo base_url() ?>';
        </script>

        <!-- main css -->
        <link href="<?php echo base_url(); ?>assets/css/admin.css" rel="stylesheet" type="text/css" />
        <!-- /main css -->

        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />     
    </head>

    <body>
        <!-- header -->
        <div id="header">            
            <div class="clear"></div>
        </div>
        <!-- /header -->

        <!-- middle -->
        <div id="middle">
            <!-- menu -->
            <div class="middletop">
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
            <!-- /menu -->

            <!-- line spacer -->
            <div class="middlebottom">
                <div class="footerspacer">&nbsp;</div>
            </div>
            <!-- /line spacer -->

            <!-- middleCONTENT -->
            <div class="middlecontent">
                <!-- Side Right -->
                <div class="sideright">
                    <?php echo $content; ?>
                </div>
                <!-- /Side Right -->
            </div>
            <!-- middleCONTENT -->

            <!-- line spacer -->
            <div class="middlebottom">
                <div class="footerspacer">&nbsp;</div>
            </div>
            <!-- /line spacer -->

            <!-- middleBOTTOM -->
            <div class="middlebottom">
                <div class="middlefooter">
                    <div class="middlefooterleft">&copy; 2012. <a href="<?php echo site_url(); ?>">Wooz.in</a></div>
                    <div class="clear"></div>
                </div>
            </div>
            <!-- /middleBOTTOM -->
        </div>
        <!-- /middle -->

    </body>
</html>
