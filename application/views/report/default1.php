<?php
		$places = $this->db->get_where('places', array('places_landing' => $event))->row();
?>
<!-- Title Page -->
<div class="titlepage">
    <h3>Welcome Event Statistik</h3>
</div>
<!-- /Title Page -->

<div class="tablecontenttop">
    <h4 class="titletable">Statistik on <?php echo $places->places_name; ?></h4>
</div>

<!-- table content middle -->
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>Report</strong>
                </th>
<?php
		$startTime = strtotime($places->places_startdate); 
		$endTime = strtotime($places->places_duedate); 
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i); // 2010-05-01, 2010-05-02, etc
?>
                <th>
                    <strong>Day <?php echo $y; ?></strong>
                </th>
<?php
			$y++;
		}
?>
                <th>
                    <strong>Total</strong>
                </th>
            </tr>
        </thead>

<?php
		//update status
        $this->db->select('');
        $this->db->where('places_parent', $places->id);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $family = $this->db->get()->result();
		#$family = $this->convert->family($places->id);

		if(count($family) >= 1){
			foreach($family as $id){
				$places1 = $this->db->get_where('places', array('id' => $id->id))->row();
?>
        <tbody>
		
			<tr class="oddrow">
			<td class="firstcol">Update status di <?php echo $places1->places_name; ?></td>
<?php
				$startTime = strtotime($places->places_startdate); 
				$endTime = strtotime($places->places_duedate); 

				$y = 1;
				for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
					$thisDate = date('Y-m-d', $i); 
					$this->db->select('count(distinct(account_id)) as total');
					$this->db->where("log.places_id", $places1->id);
					$this->db->where("log.log_stamps like", '%'.$thisDate.'%');
					$this->db->from('log');
					$total = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
<?php
					$y++;
				}

				$this->db->select('count(distinct(account_id)) as total');
				$this->db->where("log.places_id", $places1->id);
				$this->db->where("log.log_stamps >", $places1->places_startdate. ' 00:00:00');
				$this->db->where("log.log_stamps <=", $places1->places_duedate. ' 23:59:00');
				$this->db->from('log');
				$total = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
			</tr> 
		 </tbody>
        <tbody>
<?php
			}
		}
?>

    </table>