<?php if(isset($nama) && $nama != ''): ?>
<h2 class="center">
 <font size="6px" color='blue'>Hai <?php echo $nama; ?></font> </br>    
</h2>  
<?php endif; ?>
<?php if(isset($error) && $error != ''): ?>
<h2 class="center">
 <font size="6px" color='blue'><?php echo $error; ?></font> </br>    
</h2>  
<?php endif; ?>

<?php if(isset($update) && $update != ''): ?>
    <div id="updatestatus">
    <h2 class="center">
        <font size="4px">Tap Update Status</font>
    </h2>
        <p class="center">
        <?php foreach($update as $row) : ?>
            <?php $places = $this->db->get_where('places', array('id' => $row->places_id))->row(); ?>
            <?php echo $places->places_name; ?>
            </br>
        <?php endforeach; ?>
           <font size="4px" color='green'> Total = <?php echo count($update).' titik update status'; ?> </font>
        </p>    
    </div>
<?php endif; ?>

<?php if(isset($photo) && $photo != ''): ?>
<div id="updatephoto">
    <h2 class="center">
            <font size="4px">Tap Update Photo </font>
    </h2>
    <p class="center">
        <?php foreach($photo as $row) : ?>
            <?php $places = $this->db->get_where('places', array('id' => $row->places_id))->row(); ?>
            <?php echo $places->places_name; ?>
            </br>
        <?php endforeach; ?>
<font size="4px" color='red'> Total = <?php echo count($photo).' titik update photo'; ?> </font>
    </p>      
</div>
<?php endif; ?>
<div id="redeem"><h2 class="center">
        <!--
    <?php #if(isset($update) && $update != ''): ?>  
        Total semua titik = <?php #$a=count($update); echo $a.' titik' ?></h2>
    <?php #endif; ?>
    <?php #if(isset($photo) && $photo != ''): ?>
        Total semua titik = <?php #$a=count($photo); echo $a.' titik' ?></h2>
    <?php #endif; ?>
        -->
    <?php if((isset($update) && $update != '') && (isset($photo) && $photo != '')): ?>
        Total semua titik = <?php $a=count($update)+count($photo); echo $a.' titik' ?></h2>
    <?php endif; ?>
    <div id="redeemleft">
        <?php if(isset($merchandise)): ?>
        <?php if($merchandise == 2): ?>
        <h2 class="center">
                Selamat 
                Anda berhak atas merchandise</br>
                Mau di redeem?</br>
                <a href="<?php echo site_url('event/isredeem/merchandise/'.$userid); ?>">
                    Ya
                </a>       
        </h2>
                <?php elseif($merchandise == 1): ?>
                <h2 class="center">

                Anda sudah menukarkan merchandise </br>    
        </h2>
        <?php else: ?>
        <h2 class="center">
        Untuk hadiah Merchandise anda belum memenuhi syarat</br>    
        </h2>
        <?php endif; ?>   
        <?php endif; ?>   
    </div>
    <div id="redeemright">
     <?php if(isset($eskrim1)): ?>
     <?php if(isset($eskrim1) && $eskrim1 == 2): ?>
        <h2 class="center">
                Selamat 
                Anda berhak atas ice cream 1 buah</br>
                Mau di redeem?</br>
                <a href="<?php echo site_url('event/isredeem/eskrim1/'.$userid); ?>">
                    Ya
                </a>      
        </h2>
                <?php elseif(isset($eskrim1) && $eskrim1 == 1): ?>
                <h2 class="center">

                Anda sudah menukarkan 1 buah ice cream </br>    
        </h2>
        <?php else: ?>
        <h2 class="center">
        Untuk hadiah 1 buah ice cream anda belum memenuhi syarat</br>    
        </h2>
        <?php endif; ?>  
        <?php endif; ?>  
    </div>
    <?php if(isset($eskrim2)): ?>
    <?php if(isset($eskrim2) && $eskrim2 == 2): ?>
    <h2 class="center">
            Selamat 
            Anda berhak atas ice cream 2 buah</br>
            Mau di redeem?</br>
            <a href="<?php echo site_url('event/isredeem/eskrim2/'.$userid); ?>">
                Ya
            </a>       
    </h2>
            <?php elseif(isset($eskrim2) &&  $eskrim2 == 1): ?>
            <h2 class="center">

            Anda sudah menukarkan 2 buah ice cream </br>    
    </h2>
    <?php else: ?>
    <h2 class="center">
    Untuk hadiah 2 buah ice cream anda belum memenuhi syarat</br>    
    </h2>
    <?php endif; ?>
    <?php endif; ?>
</div>


<div id="ref">
<a href="<?php echo site_url('event/formredeem');?>"><font size="7px" color='red'>finish</font></a>
</div>