<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=useryamaha.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<?php
		$places = $this->db->get_where('places', array('places_landing' => 'yamaha'))->row();
?>
<!-- Title Page -->
<div class="titlepage">
    <h3>Welcome Event Statistik</h3>
</div>
<!-- /Title Page -->

<div class="tablecontenttop">
    <h4 class="titletable">Statistik on <?php echo $places->places_name; ?></h4>
</div>

<!-- table content middle -->
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>Nama</strong>
                </th>
                <th>
                    <strong>Email</strong>
                </th>
                <th>
                    <strong/>Account facebook</strong>
                </th>
<?php
		$startTime = strtotime($places->places_startdate); 
		$endTime = strtotime($places->places_duedate); 
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i); // 2010-05-01, 2010-05-02, etc
?>
                <th>
                    <strong>Day <?php echo $y; ?></strong>
                </th>
<?php
			$y++;
		}
?>
                <th>
                    <strong>Total</strong>
                </th>
            </tr>
        </thead>
<?php
		$this->db->select('distinct(account_id),wooz_account.account_displayname,wooz_account.account_email,wooz_account.account_fbid');
		$this->db->join('account', 'account.id = log.account_id', 'left');
        $this->db->where_in('places_id', array("224","225","226","227","228","229","238","239","240","241","242","243"));
		$this->db->order_by("account_id", "desc"); 
        $this->db->from('log');
        $event = $this->db->get()->result();
?>
	<?php foreach ($event as $row): ?>
	<tbody>		
		<tr class="oddrow">
			<td class="firstcol"><?php echo $row->account_displayname; ?></td>
			<td class="dataitem"><?php echo $row->account_email; ?></td>
			<td class="dataitem"><a href="https://www.facebook.com/profile.php?id=<?php echo $row->account_fbid; ?>">https://www.facebook.com/profile.php?id=<?php echo $row->account_fbid; ?></a>
			<?php
			$startTime = strtotime($places->places_startdate); 
			$endTime = strtotime($places->places_duedate); 
			$y = 1;
			$total = 0;
			for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
				$thisDate = date('Y-m-d', $i);
				$this->db->select('distinct(places_id),log_date');
				$this->db->where_in('places_id', array("224","225","226","227","228","229","238","239","240","241","242","243"));
				$this->db->where('account_id',$row->account_id);
				$this->db->where('log_date like','%'.$thisDate.'%');
				$this->db->from('log');
				$event1 = $this->db->get()->result();		
				$total = $total + count($event1);
			?>
			<td class="dataitem"><?php echo count($event1); ?></td>
			<?php
			}		
			?>
			<td class="dataitem"><?php echo $total; ?></td>
		</tr> 
	</tbody>
	<?php endforeach; ?>