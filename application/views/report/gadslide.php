<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class='no-js' lang='en'>
	<!--<![endif]-->
	<head>
		<meta charset='utf-8' />
		<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
		<title>Slide show GAD 2012</title>
		
		<link rel="stylesheet" href="http://wooz.in/gallery/lib/css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="http://wooz.in/gallery/lib/css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
		
		<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		
		<!--[if IE 6]>
			<style type="text/css" media="screen">
				/*I don't feel like messing with pngs for this browser... sorry*/
				#gradient {display:none;}
			</style>
		<![endif]-->
	</head>
	<body>
		
		<div id="maximage">
			<?php foreach($event as $row): ?>
			<img src="http://wooz.in/uploads/photobooth/<?php echo $row->photo_upload; ?>" width="1280" height="760" />
			<?php endforeach; ?>
		</div>
		<!--
		<div id="maximage1">
			<?php foreach($events as $rows): ?>
			<img src="http://wooz.in/uploads/photobooth/<?php echo $rows->photo_upload; ?>" width="1280" height="760" />
			<?php endforeach; ?>
		</div>
		-->
		<script src='http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js'></script>
		<script src="http://wooz.in/gallery/lib/js/jquery.cycle.all.js" type="text/javascript" charset="utf-8"></script>
		<script src="http://wooz.in/gallery/lib/js/jquery.maximage.js" type="text/javascript" charset="utf-8"></script>
		
		<script type="text/javascript" charset="utf-8">
			$(function(){
				// Trigger maximage
				jQuery('#maximage').maximage();
				jQuery('#maximage1').maximage();
			});
		</script>
		
	<meta http-equiv="refresh" content="900;URL=<?php echo site_url('event/smirnoffslide'); ?>">
  </body>
</html>
