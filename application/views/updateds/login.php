<article>

    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <?php if(validation_errors()) : ?>
                    <span class="Muse70030">Whoops ! Something wrong with your login</span>
                    <br>
                        <?php echo validation_errors('<span class="Muse30018reg">', '</span>'); ?>
                    <?php else : ?>
                    <span class="Muse70030">Login</span>
                    <br>
                    <span class="Muse30018reg">Type in your email and password and you are ready to go</span>
                    <?php endif; ?>
                </div>
                <div class="formbox">
                    <form action="<?php echo current_url(); ?>" method="post">
                        <ul class="navlist">
                            <li class="listyle arial17">
                                Email :
                            </li>
                            <li>
                                <input name="maillogin" id="txtemail" type="text" class="inputtype arial17" >
                            </li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial17">
                                Password :
                            </li>
                            <li>
                                <input name="passlogin" id="txtpassword" type="password" class="inputtype arial17" >
                            </li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial17">
                                &nbsp;
                            </li>
                            <li>
                                <a href="<?php echo site_url('home/forgotpassword'); ?>" class="arial12">Forgot password</a>
                            </li>
                            <!--
                                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" name="checkbox" id="checkbox">
                                                            <a href="#" class="arial12 greystyle">Remember me</a>
                                                        </li>
                            -->
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial17">
                                &nbsp;
                            </li>
                            <li>
                                <input type="image" src="<?php echo base_url(); ?>assets/images/btnsubmit.png">
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="formfooter arial15">
                    <span class="greystyle">Don’t have wooz account?</span>
                    <a href="<?php echo site_url('home/join'); ?>" class="redstyle">Sign up here >></a>
                </div>
            </div>

        </div>
    </section>

</article>