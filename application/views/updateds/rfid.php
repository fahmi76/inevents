<script type="text/javascript">
		$(document).ready(function(){
			$('#rfid').focus();                        
			$('#loading').hide();
            $('#input_submit').show();

			$('#input_submit').bind('submit', function() {
				$('#loading').show();
                $('#input_submit').show();
				$.ajax({
					url: main_url + "updateds/home",
					type: "POST",
					data: {
						card : $('#rfid').val(),
						booth : $('#serial').val()
					},
					dataType: "json",
					success: function(data) {
                     if(data.status == "true"){
						$('#rfid').focus();      
						$('#message_ajax').show();
                        $('#loading').hide();
						$('#input_submit').show();
						$("#message_ajax").html("<div id='successMessage' class='successMessage'><span>" + data.message + "</span> Now you're in <font color='green'>" + data.message2 + " </font></div>");
						setTimeout(function(){
							$('#message_ajax').hide();
							$('#rfid').focus();      
						}, 2000); 
					 }
                     else{
						$('#rfid').focus();      
						$('#message_ajax').show();
						$('#loading').hide();
						$('#input_submit').show();
						$("#message_ajax").html("<div class='successMessage'>" + data.message + "</br>Failed due to <font color='red'>" + data.message2 + " </font></div>");
						setTimeout(function(){
							$('#message_ajax').hide();
							$('#rfid').focus();      
						}, 2000); 
					 }

					}
				});
				$('#rfid').val('');
                $('#serial').val();
				return false;
			});
		});
</script>
<?php if($customs == 'heineken'): ?>
<div class="text-box" style="margin-left: 200px; margin-right: -200px;height: 380px;">
				<div style="text-align: left; width: 50%; float: left;">
				<h2 class="tap-wristband"><p class="notify"><span><?php echo $places_name; ?></span></p></h2>
				<h2 class="tap-wristband"><p id="tap-code"><span>Tap your wristband here</span></p></h2>
                <p style="text-align:center">
                
				<div id="loading"><img src="<?php echo base_url();?>assets/images/ajax-loader.gif" alt="loading..." /></br>Please Wait</div>
				<form id="input_submit" method="post">
					<div class="input-form">
						<p>
							<input id="rfid" type="password" name="serial" value="" /><br />
							<input id="serial" type="hidden" name="fullname" value="<?php echo $loc; ?>" />
						</p>
						<input type="submit" name="submit" value="submit" />
					</div>
				</form>

				<div id="message_ajax"></div>
				<p style="font-style:italic;font-size:16px;"><?php echo $places_text; ?></p>
				<p>
                    <a href=#" >
                        <img src="http://wooz.in/uploads/apps/fritz/facebook-logo.png" alt="connect with your facebook account" style="width: 50px; height: 50px;" />
                    </a>
					<a href="#" >
                        <img src="http://wooz.in/uploads/apps/fritz/twitterlogos.png" alt="connect with your facebook account" style="width: 50px; height: 50px;" />
                    </a>
                </p>
                </p>
				</div>
				<div style="text-align: right; width: 50%; float: left;">
				<img src="http://wooz.in/uploads/apps/fritz/booth.png" style="text-align:right;height: 350px;" />
				</div>
            </div> 
<?php else: ?>
<div class="text-box">
<h2 class="tap-wristband"><p class="notify"><span><?php echo $places_name; ?></span></p></h2>
<h2 class="tap-wristband"><p id="tap-code"><span>Tap your wristband here</span></p></h2>

<p style="font-style:italic;font-size:18px;"><?php echo $places_text; ?></p>
<!--
	<section id="intro"> 
        <div id="bigsign" class="maindiv" style="margin-top: -55px; padding-right: 40px;"> 
                <div class="signbox">
                	<h1 id="tap-code" class="tap-card"><span>Tap your wristband here</span></h1>
<p><img alt="wooz.in" src="<?php echo base_url().'assets/images/smalllogo.png';?>"></p>
</div>
</div>
</section>

-->

<div id="loading"><img src="<?php echo base_url();?>assets/images/ajax-loader.gif" alt="loading..." /></br>Please Wait</div>
<form id="input_submit" method="post">
    <div class="input-form">
        <p>
            <input id="rfid" type="password" name="serial" value="" /><br />
            <input id="serial" type="hidden" name="fullname" value="<?php echo $loc; ?>" />
        </p>
        <input type="submit" name="submit" value="submit" />
    </div>
</form>

<div id="message_ajax"></div>
</div> 
<?php endif; ?>