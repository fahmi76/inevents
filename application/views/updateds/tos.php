<script type="text/javascript">
    $(document).ready(function(){
        $('#english').hide();
        $('#bahasa a').click(function(){
            $('#english').show();
            $('#bahasa').hide();

            $('#english a').click(function(){
                $('#english').hide();
                $('#bahasa').show();
            });
        });
    });
</script>
<article>

    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Terms Of Service</span><br>
                    <span class="Muse30018reg">Wooz.in - Join. Connect. Woozin'</span>
                </div>
                <div class="formbox">
<div id="bahasa">
<p>Bahasa | <a href="#">English</a></p>
<p>Dengan menggunakan Wooz.in service, Anda menyetujui Terms and Condition (Terms of Service) kami.� Wooz.in memiliki hak untuk melakukan penambahan dan perubahan pada Term of Service secara berkala tanpa pemberitahuan terlebih dahulu. Fitur baru yang nantinya akan dikembangkan juga berada dibawah Terms of Service yang sama.</p>

<p>Pelanggaran yang terjadi pada Terms of Service akan berakibat pada pe-nonaktifan akun anda. Wooz.in juga tidak bertanggung jawab pada isi yang diposting dalam Wooz.in dan bahwa Anda mungkin akan terpapar pada isi yang dibuat oleh para pengguna lain. Anda setuju untuk menggunakan jasa Wooz.in atas resiko yang akan Anda tanggung sendiri.</p>

<h3>Terms of Service:</h3>
<p>
1. Anda harus berusia minimal diatas 13 tahun untuk menggunakan jasa Wooz.in<br />
2. Ada harus memasukkan data yang valid<br /> 
3. Login Anda hanya dapat dipergunakan unuk single login.<br /> 
4. Bots atau metode otomatis tidak diperkenankan<br /> 
5. Anda bertanggung jawab untuk menjaga keamanan akun dan password Anda. Wooz.in tidak bertanggung jawab atas kerugian atau hilangnya akun Anda dan akibat yang mengikutinya.<br /> 
6. Anda bertanggung jawab untuk semua isi yang Anda buat ketika Anda menggunakan Wooz.in<br /> 
7. Anda tidak diperkenankan menggunakan Wooz.in untuk kegiatan yang melanggar hukum atau SARA.<br /> 
8. Penggunaan service kami atas keinginan Anda sendiri. <br />
9. Anda setuju untuk tidak melakukan reproduksi, duplikasi, membuat kopi service yang dimiliki Wooz.in tanpa ijin tertulis dari Wooz.in 	<br />
10. Anda tidak diperkenankan untuk upload, posting atau menyebarkan info yang bersifat spam, SARA dan isi yang melanggar hukum  <br />
11. Wooz.in tidak menjamin bahwa: <br />
(i) servos yang diberikan sell aman dan error free<br /> 
(ii) kualitas produk, servis, info yang Anda dapat dari servis sesuai dengan harapan Anda 
</p>

<h3>Modifikasi </h3>
<p>
1. Wooz.in berhak untuk melakukan modifikasi atau menghilangkan service yang ada/dimiliki. <br />
2. Wooz.in tidak bertanggung jawab pada perubahan ataupun modifikasi yang terjadi pada pihak ketiga.</p> 

<h3>Pembatalan</h3>
<p>
1. Anda bertanggung jawab untuk secara benar membatalkan akun Anda. untuk melakukan pembatalan akun ikuti petunjuk yang sudah diberikan.<br />
2. Semua isi akun Anda akan langsung di-delete ketika Anda melakukan pembatalan akun. Informasi ini tidak dapat didapatkan kembali ketika Anda melakukan pembatalan akun.<br />
3. Wooz in tidak bertanggung jawab pada kerugian atau kehilangan akibat dibatalkannya akun Anda.</p>

<h3>Copyright dan Kepemilikan Isi</h3>
<p>
1. Wooz.in tidak memiliki copyright atas isi material yang diposting dalam service Wooz.in. Material yang Anda upload merupakan kepemilikan Anda. Namun Anda telah menyetujui untuk membagi isi yang Anda miliki kepada orang lain ketika melakukan pendaftaran service.<br />
2. Wooz.in tidak melakukan proses pre-screen terhadap isi. Namun Wooz.in berhak untuk tidak menampilkan isi yang dianggap bertentangan dengan Terms of Service
</p>
</div>
<div id="english">
<p><a href="#">Bahasa</a> | English</p>
<p>By using wooz.in service ....</p>
</div>
                </div>
            </div>
        </div>
    </section>

</article>
