    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Sign up</span><br />
                    <span class="Muse30018reg">1. Please Connect With Your Facebook Account</span>
                </div>
                <div class="formbox" >
                    <ul class="navlist">
                        <li>
                            By Registering Using Your Facebook Account, You Allow Wooz.In To Automatically Update Your Status
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <ul class="navlist">
                        <li> 
                            <a href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('home/facebook'), 'next' => site_url('home/facebook'), 'scope' => $this->config->item('facebook_perms'))); ?>">
                                <img alt="connect with facebook" src="<?php echo base_url().'assets/images/icon/connect-facebook.gif';?>" />
                            </a>
                        </li>
                        <li class="clear"></li>
                    </ul>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>

            </div>
        </div>
    </section>