<?php
$token = $this->facebook->getAccessToken();
$logoutUrl = $this->facebook->getLogoutUrl(array('next' => site_url('logout'), 'access_token' => $token));
?>
<h2 class="center">Terima kasih!</h2>
<p class="reg-done center">Proses registrasi telah selesai</p>
<meta http-equiv="refresh" content="2;URL=<?php echo $logoutUrl; ?>" />
