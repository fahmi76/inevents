<script type="text/javascript">
		$(document).ready(function(){
			$('#rfid').focus();                        
			$('#loading').hide();
            $('#input_submit').show();

			$('#input_submit').bind('submit', function() {
				$('#loading').show();
                $('#input_submit').show();
				$.ajax({
					url: main_url + "updateds/home",
					type: "POST",
					data: {
						card : $('#rfid').val(),
						booth : $('#serial').val()
					},
					dataType: "json",
					success: function(data) {
                     if(data.status == "true"){
                        $('#loading').hide();
						$('#input_submit').show();
						$("#message_ajax").html("<div class='successMessage'>" + data.message + " Sekarang Anda berada di <font color='green'>" + data.message2 + " </font></div>");
						//setTimeout('location.reload()', 500);
					 }
                     else{
						$('#loading').hide();
						$('#input_submit').show();
						$("#message_ajax").html("<div class='successMessage'>" + data.message + "</br>Failed due to <font color='red'>" + data.message2 + " </font></div>");
                        //setTimeout('location.reload()', 500);
					 }

					}
				});
				$('#rfid').val('');
                $('#serial').val();
				return false;
			});
		});
</script>
    <section id="intro"> 
        <div id="bigsign" class="maindiv" style="margin-top: -55px; padding-right: 40px;"> 
                <div class="signbox">

<p><h2 style="width: 242px; margin-top: 50px;">Tap your wristband </br> here</h2></p>
<p><img alt="wooz.in" src="<?php echo base_url().'assets/images/smalllogo.png';?>"></p>


</div>
</div>
</section>

<div id="loading"><img src="<?php echo base_url();?>assets/images/ajax-loader.gif" alt="loading..." /></br>Please Wait</div>
<form id="input_submit" method="post">
    <div class="input-form">
        <p>
            <input id="rfid" type="text" name="serial" value="" /><br />
            <input id="serial" type="hidden" name="fullname" value="<?php echo $loc; ?>" />
        </p>
        <input type="submit" name="submit" value="submit" />
    </div>
</form>

<div id="message_ajax"></div>
