<div id="navigation">
	<a class="top_flash" href="<?php echo current_url();?>">refresh</a>
	<a class="top_flash" href="<?php echo site_url('photobooth/cancel');?>">cancel</a>
	<div class="clear"></div>
</div>
<div id="webcam">
</div>
<div id="canvas_set">
    <div class="frame_overlay">
        <img class="overlay" src="<?php echo $frame_normal; ?>" />
    </div>
    <img id="preview" width="1000" height="750" />
</div>
<div id="status"></div>
<div id="choice">
	<div id="notes">Hi! <?php echo $name;?>, What do you want to do next?</div>
	<div id="choices">
		<a class="link" id="retry" href="#">Retry</a>
		<a class="link" id="cancel" href="<?php echo site_url('photobooth/cancel');?>">Cancel</a>
		<a class="link" id="save" href="#">Upload</a>
		<div class="clear"></div>
	</div>
</div>
