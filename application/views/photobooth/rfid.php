<script type="text/javascript">
	$(document).ready(function(){
		$("#rfid").focus();
	});
</script>
<div id="navigation">
	<a class="top" href="<?php echo current_url();?>">refresh</a>
	<a class="top" href="<?php echo site_url('photobooth/cancel');?>">cancel</a>
	<a class="top" href="<?php echo site_url('photobooth/thankyou');?>">finish</a>
	<div class="clear"></a>
</div>
<div id="photo">
	<img id="tagsource" class="overlay1" src="<?php echo base_url();?>uploads/photobooth/<?php echo $img_temp; ?>" alt="" />
</div>
<div id="form2">
	<form id="inputtag" method="post">
		<input id="rfid" type="text" name="rfid" value="" />
		<input  type="submit" name="submit" value="submit" />
	</form>
<?php
	if(validation_errors()) {
		echo validation_errors('<p class="error">Error! ', '</p>');
		echo 'Please try again.';
	} else {
	    echo 'Please tap your<br />wristband on the reader';
	}
?>
</div>