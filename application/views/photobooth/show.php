<div id="navigation">
	<a class="top" href="<?php echo current_url();?>?place=<?php echo $customs;?>">refresh</a>
	<a class="top" href="<?php echo site_url('photobooth/cancel?place='.$customs);?>">cancel</a>
	<a class="top" href="<?php echo site_url('photobooth/thankyou?place='.$customs);?>">finish</a>
	<div class="clear"></div>
</div>
<div id="photo">
	<img id="tagsource" class="overlay1" src="<?php echo base_url();?>uploads/photobooth/<?php echo $img_temp; ?>" alt="" />
</div>
<div id="status"></div>
<div id="choice">
	<?php if(isset($hasil) && $hasil == 6): ?>
		<div id="notes"><font size=12>Hi! <?php echo $name;?>. You're Already In This Photo,</font></br> What's next?</div>
		<div id="choices">
			<a class="link" id="finish" href="<?php echo site_url('photobooth/thankyou?place='.$customs);?>">Finish Here</a>
			<a class="link" id='tagbutton' onclick='check()' href="#">Tag photo</a>
			<!--
			<a class="link" id="save" href="<?php echo site_url('photobooth/phototag?place='.$customs);?>" style = "height: 85px; width: 500px;">Tag photo</a>
			-->
			<div class="clear"></div>
		</div>
    <?php elseif(isset($hasil) && $hasil == 1) : ?>
    <div id="notes">
        Your Photo has been saved, but we can't uploaded to your facebook account due to expired session, please reconnect your facebook account with wooz.in so we can upload your photo automatically.
    </div>

    <?php elseif(isset($hasil) && $hasil == 2) : ?>
    <div id="notes">
        Your Photo has been saved, but we can't uploaded to your facebook account due to changed password notice, please reconnect your facebook account with wooz.in so we can upload your photo automatically.
    </div>

    <?php elseif(isset($hasil) && $hasil == 3) : ?>
    <div id="notes">
        Your Photo has been saved, but we can't uploaded to your facebook account due to unauthorized notice, please reconnect your facebook account with wooz.in so we can upload your photo automatically.
    </div>

    <?php elseif(isset($hasil) && $hasil == 4) : ?>
    <div id="notes">
        Your Photo has been saved, but we can't uploaded to your facebook account due to invalid session, please reconnect your facebook account with wooz.in so we can upload your photo automatically.
    </div>

    <?php elseif(isset($hasil) && $hasil == 5) : ?>
    <div id="notes">
        Your Photo has been saved, but we can't uploaded to your facebook account due to connection time, please retry to take a photo.
    </div>
	<?php else: ?>
	<!--
		<div id="notes"><font size=12>Hi! <?php echo $name;?>. Photo Uploaded,</font></br>  What do you want to do next?</div>
		-->
		<div id="notes"><font size=12>Hi! <?php echo $name;?>,</br>Your Photo has been Uploaded</font></div>

		<div id="choices">
			<a class="link" id="finish" href="<?php echo site_url('photobooth/thankyou?place='.$customs);?>">Finish Here</a>
			<a class="link" id='tagbutton' onclick='check()' href="#">Tag photo</a>
			<!--
		<div id="choices" style="margin-left: 330px;">
			<a class="link" id="finish" href="<?php echo site_url('photobooth/thankyou');?>">Finish Here</a>
			<a class="link" id='tagbutton' onclick='check()' href="#">Tag photo</a>
			<a class="link" id='tagbutton' href="<?php echo site_url('photobooth/rfidtag');?>">Tag photo</a>
			
			<a class="link" id="save" href="<?php echo site_url('photobooth/phototag');?>" style = "height: 85px; width: 500px;">Tag photo</a>
			-->
			<div class="clear"></div>
		</div>
	<?php endif; ?>
</div>
<div id="message_ajax">
<!--
<div id="target"></div><div id="input"><label for="name">Please tap your wristband</label><input type="text" id="name"><button style="background-color: transparent;border: 0" type="submit"></button></div>
-->
<div id="target"></div><div id="input"><label for="name">Please tap your <?php echo $card; ?></label><input type="text" id="name"><button style="background-color: transparent;border: 0" type="submit"></button></div>
</div>
<div id="message">
<!--
<div class='message success'>Your Wristband is Not Registered.<br size=2px />Please Try Again.</div>
-->
<div class='message success'>Your Card is Not Registered.<br size=2px />Please Try Again.</div>
</div>
<div id="message1">
</div>
<div id="message2">
</div>