<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Photo Booth Setup</title>
        <link href="<?php echo base_url() . "assets/css/photobooth.css"; ?>" type="text/css" rel="stylesheet" />
        <script src="<?php echo base_url() . "assets/javascripts/jquery-1.7.1.min.js"; ?>" type="text/javascript"></script>
        <script src="<?php echo base_url() . "assets/javascripts/jquery.webcam.js"; ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var pos = 0, ctx = null, saveCB, image = [];
                w = 640;
                h = 480;
                y = 4;    
                function camera() {
                    $("#webcam").webcam({
                        width: 1000,
                        height: 750,
                        mode: "callback",
                        swffile: "../assets/photobooth/photobooth_640.swf",
                        quality: 100,
                        onTick: function(remain) {
                            if (0 == remain) {
                                $("#status").html("Cheese!");
                            } else if ( remain < 7 ) {
                                $("#status").html("Hi, " + name + "! Prepare your pose<br />" + remain + " seconds remaining...");
                            } else {
                                $("#status").html("Hi, " + name + "! Prepare your pose");
                            }
                        },
                        onCapture: function () {
                            webcam.save();
                            $("#status").hide();
                            $("#choice").show();
                        },
                        onLoad: function () {
                            webcam.capture(10);
                        }
                    });
                }
                camera();
            });

        </script>
    </head>
    <body>
        <div id="wrap">
            <div id="webcam">
            </div>
        </div>
    </body>
</html>