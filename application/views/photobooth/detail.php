<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Detail</title>
<link href="<?php echo base_url()."assets/css/phototagging.css"; ?>" type="text/css" rel="stylesheet" />
<script src="<?php echo base_url()."assets/js/jquery.js";?>" type="text/javascript"></script>
<script type="text/javascript">
	base_url = "<?php echo base_url(); ?>";
</script>
<?php if(!empty($js)) :?>
<script src="<?php echo base_url()."assets/js/".$js;?>" type="text/javascript"></script>
<?php endif; ?>
</head>
<body>
<img src="<?php echo base_url();?>uploads/photobooth/<?php echo $list->photo_upload; ?>" border="0" alt="" />
<br />
<?php foreach($det as $detail) :?>
<div id="hotspot-<?php echo $detail->tagCounter; ?>" class="hotspot" style="left:<?php echo $detail->targetX; ?>px; top:<?php echo $detail->targetY; ?>px;"><span><?php echo $detail->displayname; ?></span></div>
<p id="hotspot-item-<?php echo $detail->tagCounter; ?>"><?php echo $detail->tagCounter; ?>.&nbsp;<span class="remove" onmouseover="showTag(<?php echo $detail->tagCounter; ?>)" onmouseout="hideTag(<?php echo $detail->tagCounter; ?>)"><?php echo $detail->displayname; ?></span></p>
<?php endforeach; ?>
</body>
</html>