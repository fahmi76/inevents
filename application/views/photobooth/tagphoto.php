<script type="text/javascript">
    $(document).ready(function(){
        $("#tagme").click(function(){
            var urltagme = $(this).attr('href');
            $("#container").load(urltagme,function(){
                $("#showrfid").fadeIn(1000).fadeTo("slow");
            });
            return false;
        });
        $("#tagfinish").click(function(){
            var urlfinishtag = $(this).attr('href');
            $("#tag-welc2").hide();
            $("#button").hide();
            $("#photo").hide();
            $("#thanktag").load(urlfinishtag,function(){
                $(this).fadeIn(1000).fadeTo("slow");
                setInterval(function(){
                    window.location.href='<?php echo base_url(); ?>index.php/photo';
                },2000);
            });
            return false;
        });
    });
</script>
<div id="tag-welc2">
    Hello <?php echo $this->session->userdata('name'); ?>,
    You Already In This Photo, Whats next?
</div>
<div id="button">
    <div id="done">
        <a id="tagfinish" href="<?php echo base_url(); ?>index.php/photo/publish">Finish</a>
    </div>
    <div id="tag">
        <a id="tagme" href="<?php echo base_url(); ?>index.php/photo/rfidtagfirst">Tag Photo</a>
    </div>
    <div style="clear:both"></div>
</div>
<div id="tag-list">
    <?php
    foreach ($dataphoto as $dp) {
        $user = $this->db->get_where('account', array('id' => $dp->account_id))->row();
    ?>
        <div onmouseover="showTag(<?php echo $dp->tagCounter; ?>)" onmouseout="hideTag(<?php echo $dp->tagCounter; ?>)" id="hotspot-<?php echo $dp->tagCounter; ?>" class="hotspot" style="left:<?php echo $dp->targetX + 190; ?>px; top:<?php echo $dp->targetY + 60; ?>px;">
            <span><?php echo $user->account_displayname; ?></span>
        </div>
    <?php
    }
    ?>
</div>
<div id="thanktag"></div>