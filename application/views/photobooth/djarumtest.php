<?php

class djarumtest extends CI_controller {

    protected $tpl;
	protected $album_name = "Test 2013";
    
    function djarumtest() {
        parent::__construct();
        $this->tpl['TContent'] = null;
        $this->load->database();
        $this->load->library('curl');
        $this->load->library('fungsi');
		$this->load->library('tweet');
        //$this->load->library('model');
        include(APPPATH . 'third_party/tmhOAuth.php');
        include(APPPATH . 'third_party/tmhUtilities.php');
        
		$setting = $this->db->get_where('setting', array('id' => 1))->row();

        $app_id = $this->config->item('facebook_application_id');
        $secret_key = $this->config->item('facebook_secret_key');
        $api_key = $this->config->item('facebook_api_key');
        $fb_perms = $this->config->item('facebook_perms');
        $this->tpl['fb_perms'] = $fb_perms;

        $this->load->library('facebook', array(
            'appId' => $app_id,
            'secret' => $secret_key,
            'cookie' => true,
        ));
    }    
    
    #function index(){
	function user(){
        if (isset($_REQUEST['no_rfid'])&&$_REQUEST['no_rfid']!='') {
            $cserial = $_REQUEST['no_rfid'];
            if ($cserial) {
                $user = $this->db->get_where('account', array('account_rfid' => $cserial, 'account_status' => 1))->row();
                if ($user) {
                    $data = array(
                            'status' => 1,
                            'message' => 1
                    );
                    echo json_encode($data);
                } else {
                    $this->db->select('wooz_account.account_displayname');
                    $this->db->join('account', 'account.id = landing.account_id', 'left');
                    #$this->db->where('places_id', 1);
                    $this->db->where('landing.landing_register_form', 287);
                    $this->db->where("landing.landing_rfid", $cserial);
                    $this->db->from('landing');
                    $acc = $this->db->get()->row();                
                    if($acc){
                        $data = array(
                                'status' => 1,
                                'message' => $acc->account_displayname
                        );
                        echo json_encode($data);                    
                    }else{
                        $data = array(
                                'status' => 0,
                                'message' => 'no'
                        );
                        echo json_encode($data);
                    }
                }
            } else {
                $data = array(
                        'status' => 0,
                        'message' => 'no'
                );
                echo json_encode($data);
            }
        }        
    }
	
	#function user(){
	function index(){
        if (isset($_REQUEST['no_rfid'])&&$_REQUEST['no_rfid']!='') {
            $cserial = $_REQUEST['no_rfid'];
			$user = $this->cekuser($cserial);
			if($user){
				$syarat = $this->cekredeem($user->id);
				xdebug($syarat);die;
				if($syarat){
					$data = array(
							'status' => 1,
                            'message' => 1
						   );
					echo json_encode($data); 
				}else{
					$data = array(
						'status' => 0,
                        'message' => 3
					);
					echo json_encode($data);
				}
			}else{
				$data = array(
					'status' => 0,
                    'message' => 2
				);
				echo json_encode($data);
			}
		}else{
			$data = array(
					'status' => 0,
                    'message' => 2
            );
            echo json_encode($data);		
		}
	
	}

	function cekredeem($id){
		#$id = 14334;
		$date = date('Y-m-d');
		$now = strtotime($date) + 86400;
		$akhir = date('Y-m-d',$now);
		$this->db->where('places_id', 291);
		$this->db->where("account_id", $id);
		$this->db->where("time_upload >=", $date.' 02:00:00'); //open
		$this->db->where("time_upload <=", $akhir.' 01:30:00'); //close
		$this->db->where("status",1);
		$this->db->from('photos');
		$user = $this->db->get()->row();
		if($user){
			return false;
		}else{
			return true;
		}
	}
	
	function upload(){
		$upload_directory='/home/woozin/public_html/mobile/upload/';
		if (isset($_POST['upload'])) {  
			if (!empty($_FILES['photo_path'])) { 
					//check for image submitted
					if ($_FILES['photo_path']['error'] > 0) { 
					// check for error re file
					$data = array('status' => 'no');
					echo json_encode($data);
				} else {
					//move temp file to our server      
					move_uploaded_file($_FILES['photo_path']['tmp_name'], 
					$upload_directory . $_FILES['photo_path']['name']);
					$upload = $this->uploads($_POST['no_rfid'],$_POST['type'],$_FILES['photo_path']['name']);
					if($upload){
						$data = array('status' => 'yes');
						echo json_encode($data);
					}else{
						$data = array('status' => 'no');
						echo json_encode($data);
					}
				}
			} else {
					$data = array('status' => 'no');
					echo json_encode($data);
					// exit script
			}
		}		
	}

	function uploads($no_rfid,$type,$file_name){
		$user = $this->cekuser($no_rfid);
		#$venue = $this->db->get_where('places', array('id' => 1))->row(); 
		$venue = $this->db->get_where('places', array('id' => 291))->row(); 
		$this->mergephoto($file_name,$venue->places_frame);
		if ($user) {
			if ($user->account_tw_token && $user->account_tw_secret) {
                $consumer_key = $this->config->item('twiiter_consumer_key');
                $consumer_key_secret = $this->config->item('twiiter_consumer_secret');

                $token = new tmhOAuth(array(
                            'consumer_key' => $consumer_key,
                            'consumer_secret' => $consumer_key_secret,
                            'user_token' => $user->account_tw_token,
                            'user_secret' => $user->account_tw_secret,
                        ));

                $image = FCPATH . 'mobile/upload/' . $file_name;
                $text = $venue->places_tw_caption;
                $code = $token->request(
                    'POST', 'https://upload.twitter.com/1/statuses/update_with_media.json', array(
                            'media[]' => "@{$image};type=image/jpeg;filename={$image}",
                            'status' => $text,
                            ), true, // use auth
                    true  // multipart
                );
                $resp = json_decode($token->response['response'], true);
				#xdebug($resp);die;
				$hasil = 1;
                if ($code == 200) {
                    $new_tid = $resp['id_str'];
                }                
            }else{
				$hasil = 0;
				$new_tid = 0;
			}
            if($user->account_fbid && $user->account_token){
                try{
                    $url = "https://graph.facebook.com/".$user->account_fbid."/albums?access_token=" . $user->account_token;
                    $info = json_decode($this->curl->simple_get($url));
                    $info = $info->data;
                    $up_to = false;
                    $albumname = $venue->places_album;
                    foreach ($info as $alb) {
                        #if ($alb->name == $venue->places_name) {
                        if ($alb->name == $albumname) {
                            $up_to = $alb->id;
                        }
                    }

                    if ($up_to) {
                        $album_id = $up_to;
                    } else {
                        try{
                            $options['name'] = $albumname;
                            $options['access_token'] = $user->account_token;
                            $url = $this->facebook->api("/$user->account_fbid/albums", 'post', $options);
                            $hasil = json_encode($url);
                            $album = json_decode($hasil);
                            $album_id = $album->id;
                        }catch (FacebookApiException $e){
                            $hasil = 0;
                        }  
                    }
                    try{
                        if ($venue->places_fb_caption != '') {
                            $photo = $venue->places_fb_caption;
                        }else{ 
                            $photo = $user->account_displayname." test";
                        }
                        /* upload photo */
                        $this->facebook->setFileUploadSupport(true);

                        $attachement = array(
                            'access_token' => $user->account_token,
                            'name' => $photo,
                            'caption' => 'uploaded foto',
                            'source' => '@' . FCPATH . 'mobile/upload/' . $file_name
                        );

                        $upload = $this->facebook->api($album_id . '/photos', 'post', $attachement);
                        $fbid = $upload['id'];
                        $hasil = 1;                            
                    }catch (FacebookApiException $e){
                        $hasil = 0;
                    }  
                }catch (FacebookApiException $e){
                    $hasil = 0;
                }                   
            }
            if(isset($hasil) && $hasil == 1){
				if(isset($new_tid) && $new_tid != 0)
					$tid = $new_tid;
				else
					$tid = 0;
				if (isset($fbid) && $fbid != '0') 
					$fid = $fbid;
				else
					$fid = 0;

				$input = array(
                    'account_id' => $user->id,
                    'places_id' => 291,
                    'photo_upload' => addslashes($file_name),
                    'code_photo' => 'p' . $this->fungsi->recreate3($this->fungsi->acak(time())) . '-' . $user->id,
                    'pid' => $fid,
                    'twit_id' => $tid,
                    'status' => 1	
                );
				$id = $this->db->insert('photos', $input);
				
				$db['account_id'] = $user->id;
                $db['places_id'] = 291;
                $db['log_fb'] = $input['pid'];
                $db['log_tw'] = $input['twit_id'];
                $db['log_type'] = 6;
                $db['log_hash'] = $input['code_photo'];
                $db['log_date'] = date('Y-m-d');
                $db['log_time'] = date('h:i:s');
                $db['log_status'] = 1;
                $upd = $this->db->insert('log', $db);
                $new = $this->db->insert_id();
				
				/*
				//upload for fan page 
						try{
							$album_name = $venue->places_album; //set album 
							$photo = $user->account_displayname . ' ' . $venue->places_fb_caption;
							$fanpage_id = '229857937061758';	
							$fanpage_token = 'AAACGFg9ewNIBAKb8qccKP8LxXNi8dGSXZBKEnEokjBqPs3KiWkjZAacBtZCM2BX4IptnsYMZCDsVzzZAuqGplxq5pdi3Sn2jIxYqR44kAIAN7mAjZBbVlr';

							$args = array('access_token' => $fanpage_token);
							$url1 = $this->facebook->api($fanpage_id .'/albums', 'get', $args);
							$info1 = $url1['data'];
							$up_to1 = false;
							
							foreach($info1 as $alb1) {
								if($alb1['name'] == $album_name) {
									$up_to1 = $alb1['id'];
								}
							}
							if($up_to1) {
								$album_id1 = $up_to1;
							}else {
								try{
									$args = array('name' => $album_name);
									$args['access_token'] = $fanpage_token;
									$url1 = $this->facebook->api($fanpage_id .'/albums', 'post', $args);
									$album_id1 = $url1['id'];
								}catch(FacebookApiException $e){
									error_log($e);
								}
							}
								// upload photo
								$this->facebook->setFileUploadSupport(true);
								$attachement1 = array(							
									'access_token'=> $fanpage_token,
									'caption' => 'uploaded foto',
									'name' => $photo,
									'no_story' => 1,
									'source' => '@' . FCPATH . 'mobile/upload/' . $file_name,
									'place' => $venue->places_facebook
								);
								#print_r($attachement);
								$upload1 = $this->facebook->api($album_id1.'/photos','POST',$attachement1);				
						} catch (FacebookApiException $e){
							error_log($e);
						}
						//end
						*/
                return true;                  
            }else{
				return false;
            }                   
        }else{
            return false;
        }
	}

	function cekuser($no_rfid){
		$date = date('Y-m-d');
		$now = strtotime($date) + 86400;
		$akhir = date('Y-m-d',$now);
		/* $user = $this->db->get_where('account', array('account_rfid' => $no_rfid, 'account_status' => 1))->row();
        if ($user) {
			return $user;
        } else { */
			$this->db->select('wooz_account.*');
			$this->db->join('account', 'account.id = landing.account_id', 'left');
			$this->db->where('landing.landing_register_form', 287);
			#$this->db->like("landing.landing_rfid", $no_rfid);
			$this->db->where("landing.landing_rfid", $no_rfid);
			#$this->db->where("landing.landing_joindate >=", $date.' 12:00:00');
			#$this->db->where("landing.landing_joindate <=", $akhir.' 01:30:00');
			$this->db->from('landing');
			$acc = $this->db->get()->row();
            if($acc){
				return $acc;
			}else{
				return false;
            }
		#}
	}

	function mergephoto($file_path,$frame){
		#$file = FCPATH . 'mobile/upload/' . $file_name;
		#$file = FCPATH . 'mobile/upload/0323758059_98749571.jpg';
		$file = FCPATH . 'mobile/upload/'.$file_path;
		#xdebug($file);
		#xdebug($frame);die;
        $dest  = imagecreatefrompng('/home/woozin/public_html'.$frame);
        $src = imagecreatefromjpeg( FCPATH . 'mobile/upload/'.$file_path);
        $w = 640;
        $h = 480;
        /* photo setup */
        $targetImage = imagecreatetruecolor($w, $h);
        imagecolorallocate($targetImage, 0, 0, 0);
        $default = imagecreatetruecolor($w, $h);
        imagecopyresampled($default, $src, 0, 0, 0, 0, $w, $h, $w, $h);
        imagecopyresampled($targetImage, $default, 0, 0, 0, 0, $w, $h, $w, $h);
        imagecopyresampled($targetImage, $dest, 0, 0, 0, 0, $w, $h, $w, $h);

        imagejpeg($targetImage, $file, 100);
        imagedestroy($default);
        imagedestroy($targetImage);
        imagedestroy($dest);
        imagedestroy($src);

	}
	
	function totalregis(){
		$sql = "SELECT count(distinct(account_id)) as total FROM wooz_landing WHERE landing_register_form = 287";
		$hasil = $this->db->query($sql);
		$data = $hasil->result();
		echo 'total : '.$data[0]->total.'<br />';
		
		$sql1 = "SELECT DISTINCT(DATE_FORMAT( landing_joindate,'%d %M %Y')) AS thedate, count(distinct(account_id)) as total FROM `wooz_landing` where landing_register_form = 287 group by thedate order by thedate";
		$hasil1 = $this->db->query($sql1);
		$data1 = $hasil1->result();
		foreach($data1 as $row){
			echo $row->thedate .' total: '.$row->total.'<br />';
		}
	}
}