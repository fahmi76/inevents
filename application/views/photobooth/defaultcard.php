<script type="text/javascript">
function displayResult(obj)
{
  var inp1=obj.value;
  var inp2=inp1.length;
  if(inp2==0)
  {
    setTimeout(function(){obj.focus()}, 10);
  }
}
</script>

<div id="navigation">
	<a class="top_flash" href="<?php echo current_url().'?place='.$customs;?>">refresh</a>
	<!--
	<a class="top_flash" href="<?php echo site_url('photobooth/cancel?place='.$customs);?>">cancel</a>
	-->
	<div class="clear"></div>
</div>

<div id="webcam">
    <div class="frame_overlay">
        <img class="overlay" src="<?php echo $frame_normal; ?>" />
    </div>
</div>
<div id="canvas_set">
    <div class="frame_overlay">
        <img class="overlay" src="<?php echo $frame_normal; ?>" />
    </div>
    <img id="preview" width="1000" height="750" />
</div>
<div id="status"></div>
<div id="choice">
	<div id="notes"></div>
	<div id="choices">
		<a class="link" id="retry" href="#">Retry</a>
		<a class="link" id="cancel" href="<?php echo site_url('photobooth/cancel?place='.$customs);?>">Cancel</a>
		<a class="link" id="save" href="#">Upload</a>
		<div class="clear"></div>
	</div>
</div>
<div id="target"></div>
<?php if($customs == 'bukbermeter'): ?>
<div id="input"><label for="name"><strong>Ganteng/Cantik Meter</strong></label><input type="text" id="name"><button style="background-color: transparent;border: 0"  type="submit"></button></div>
<?php else: ?>
<div id="input">
	<label for="name">
		Tap <?php echo $card; ?>, take photo, and publish <br>to <strong>Social Media Account</strong>
	</label>
	<input type="text" id="name" onblur="displayResult(this);">
	<button style="background-color: transparent;border: 0"  type="submit"></button>
</div>
<?php endif; ?>
</div>
<div id="message1">
</div>