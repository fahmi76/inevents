<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Photo Booth | <?php echo $title; ?></title>
        <!--[if IE]><script src="/static/excanvas.js"></script><![endif]-->
        <link href="<?php echo base_url() . "assets/css/photobooth.css"; ?>" type="text/css" rel="stylesheet" />
        <script src="<?php echo base_url() . "assets/javascripts/jquery-1.7.1.min.js"; ?>" type="text/javascript"></script>
        <?php if (isset($page) && $page === 'flash'): ?>
            <script src="<?php echo base_url() . "assets/javascripts/jquery.webcam.js"; ?>" type="text/javascript"></script>
            <script src="<?php echo base_url() . "assets/photobooth/webcam.js"; ?>" type="text/javascript"></script>
        <?php endif; ?>
        <?php if (isset($page) && $page === 'home1'): ?>
            <script src="<?php echo base_url() . "assets/javascripts/jquery.webcam.js"; ?>" type="text/javascript"></script>
            <script src="<?php echo base_url() . "assets/photobooth/webcam.js"; ?>" type="text/javascript"></script>
        <?php endif; ?>
        <?php if (!empty($js)) : ?>
            <script src="<?php echo base_url() . "assets/javascripts/" . $js; ?>" type="text/javascript"></script>
            <script type="text/javascript">
                urlrfidtag = "<?php echo site_url('photobooth/rfidtag/'.$photosid.'/'.$userid.'?place=' . $customs); ?>";
                urltag = "<?php echo site_url('photobooth/phototag/'.$photosid.'/'.$userid.'?place=' . $customs); ?>";
                urltagdone = "<?php echo site_url('photobooth/phototag/'.$photosid.'/'.$userid.'/6?place=' . $customs); ?>";
            </script>
        <?php endif; ?>
        <?php if (!empty($js2)) : ?>
            <script src="<?php echo base_url() . "assets/javascripts/" . $js2; ?>" type="text/javascript"></script>
        <?php endif; ?>
        <?php if (!empty($js3)) : ?>
            <script src="<?php echo $js3; ?>" type="text/javascript"></script>
        <?php endif; ?>

        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <script type="text/javascript">
            <?php if (isset($page) && $page === 'flash'): ?>
                events = '<?php echo $customs; ?>';
                frame_big = '<?php echo $frame_normal; ?>';
                frame_normal = '<?php echo $frame_normal; ?>';
                page = "<?php echo $page; ?>";
            <?php endif; ?>
            <?php if (isset($name) && $name != '') { ?>
                name = "<?php echo $name; ?>";
            <?php } ?>
            card = "<?php echo $card; ?>";
            base_url = "<?php echo base_url(); ?>";
            main_url = "<?php echo site_url(); ?>";
            ptbh_url = "<?php echo site_url('photobooth?place=' . $customs); ?>";
            this_url = "<?php echo current_url(); ?>";

            urlrfidhome = "<?php echo site_url('photobooth/check_rfid?place=' . $customs); ?>";
            urlrfid = "<?php echo site_url('photobooth/rfidtag?place=' . $customs); ?>";
            urlplaces = "<?php echo $customs; ?>";
            urlplaces_id = "<?php echo $customs_id; ?>";
        </script>
    </head>
    <body>
        <div id="wrap">
            <?php echo $body; ?>
            <!--
            <div id="refresh">
                    <a class="refresh" href="<?php echo current_url(); ?>">refresh</a>
            </div>
            -->
        </div>
    </body>
</html>