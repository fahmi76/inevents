<img src="<?php echo base_url();?>uploads/photobooth/<?php echo $img_temp; ?>" alt="" />
<div id="loading"><img src="<?php echo base_url();?>assets/images/ajax-loader.gif" alt="" width="50" height="50" /></div>
<?php if($img_status == 1) :?>
<script type="text/javascript">
$(document).ready(function(){
    $("#tag-list").show();
    $("#tag-other").show();
    $("#tag-welc").hide();
    $("#tag-welc2").show();
});
</script>
<?php else: ?>
<script type="text/javascript">
$(document).ready(function(){
    $("#tag-list").show();
    $("#tag-other").hide();
    $("#tag-welc").show();
    $("#tag-welc2").hide();
});
</script>
<?php endif; ?>

<?php if($people == 1) :?>
<script type="text/javascript">
$(document).ready(function(){
    $("#tag-welc2").show();
});
</script>
<?php else: ?>
<script type="text/javascript">
$(document).ready(function(){
    $("#tag-welc2").hide();
});
</script>
<?php endif; ?>

<?php
$row = $this->db->get_where('account',array('id' => $name))->row();
?>
<div id="tag-welc">
	Hello <?php echo $row->account_displayname; ?><br />
	Which one are you?
</div>

<div id="tag-welc2">
	Hello <?php echo $row->account_displayname; ?>, 
	You�re already tagged in this photo. What next?
</div>

<div id="tag-list">
<?php
if(count($dataphoto)!=0) :
foreach($dataphoto as $detail) :
$user = $this->db->get_where('account',array('id' => $detail->account_id))->row();
?>
<div id="hotspot-<?php echo $detail->tagCounter; ?>" class="hotspot" style="left:<?php echo ($detail->targetX + 190); ?>px; top:<?php echo ($detail->targetY + 60); ?>px;"><span><?php echo $user->account_displayname; ?></span></div>
<?php endforeach; endif; ?>
</div>

<div id="tag-other">
	<div id="done">
		<a href="<?php echo site_url('photo/endphoto');?>">Done</a> 
	</div>
	<div id="tag">
		<a href="<?php echo site_url('photo/rfidtag');?>">Tag</a>
	</div>
	<div style="clear:both"></div>
</div>
<div id="showrfid"></div>
