<div class="text-box">
    <h2 class="center">
        Choose Your Application
    </h2>
    <h2 class="center">
        Registration Pre-event Link :
    </h2>
    <p class="center">
        <a href="<?php echo base_url(); ?>landings/home/guardianpre">
            Registrasi Pre-event
        </a>
    </p> 
    <h2 class="center">
        Search by Name Pre-event Link :
    </h2>
    <p class="center">
        <a href="<?php echo base_url(); ?>registrationpre">
            Search by Name Pre-event
        </a>
    </p> 
    <h2 class="center">
        Registration Link :
    </h2>
    <p class="center">
        <a href="<?php echo base_url(); ?>landings/home/guardian">
            Registrasi
        </a>
    </p>  
    <h2 class="center">
        Re-Registration Link :
    </h2>
    <p class="center">
        <a href="<?php echo base_url(); ?>registration">
            Re-Registrasi
        </a>
    </p>  
    <h2 class="center">
        Activated Social Media Link :
    </h2>
    <p class="center">
        <a href="<?php echo base_url(); ?>registration/activated">
            Activated Social Media
        </a>
    </p>  
    <h2 class="center">
        Gate Checkin Link :
    </h2>
    <?php 
        $this->db->from('gate')->where('id >', 13);
        $gate = $this->db->get()->result();
    ?>

    <?php foreach($gate as $row): ?>
        <p class="center">
            <a href="<?php echo base_url(); ?>gates/home/1/<?php echo $row->id?>">
                <?php echo $row->nama; ?>
            </a>
        </p>
    <?php endforeach; ?>
    <h2 class="center">
        Gate Checkout Link :
    </h2>
    <?php foreach($gate as $row): ?>
        <p class="center">
            <a href="<?php echo base_url(); ?>gates/home/2/<?php echo $row->id?>">
                <?php echo $row->nama; ?>
            </a>
        </p>
    <?php endforeach; ?>

</div>