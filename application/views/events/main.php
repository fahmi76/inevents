<div class="text-box">
    <h2 class="center">
        Choose Your Application
    </h2>

    <h2 class="center">
        Registration Pre-event Link :
    </h2>
    <p class="center">
        <a href="<?php echo base_url(); ?>regis/preregis/<?php echo $places->places_landing; ?>" target="_blank">
            Registrasi Pre-event
        </a>
    </p>
    <h2 class="center">
        Registration Link :
    </h2>
    <p class="center">
        <?php if ($places_id == 15): ?>
        <a href="<?php echo base_url(); ?>landings/home/<?php echo $places->places_landing; ?>" target="_blank">
        <?php else: ?>
        <a href="<?php echo base_url(); ?>regis/home/<?php echo $places->places_landing; ?>" target="_blank">
        <?php endif;?>
            Registrasi
        </a>
    </p>
    <?php if ($places_id == 34): ?>
    <h2 class="center">
        Re-Registration Link :
    </h2>
    <p class="center">
        <a href="<?php echo base_url(); ?>regis/reregis/<?php echo $places->places_landing; ?>" target="_blank">
            Re-Registrasi
        </a>
    </p>
    <?php endif;?>

    <?php if ($places_id == 42): ?>
    <h2 class="center">
        RSVP Link :
    </h2>
    <p class="center">
        <a href="<?php echo base_url(); ?>rsvp/home/<?php echo $places->places_landing; ?>" target="_blank">
            RSVP
        </a>
    </p>
    <?php endif;?>
    <?php if ($places_id == 34): ?>
    <h2 class="center">
        Update Station Link :
    </h2>
    <?php
$this->db->from('places')->where('places_parent', $places_id)->where('places_type != ', 4);
$gate = $this->db->get()->result();
?>

    <?php foreach ($gate as $row): ?>
        <p class="center">
            <a href="<?php echo base_url(); ?>updatestatus/home/<?php echo $places->places_landing; ?>/<?php echo $row->places_nicename ?>" target="_blank">
                <?php echo $row->places_name; ?>
            </a>
        </p>
    <?php endforeach;?>

    <h2 class="center">
        Report Station Link :
    </h2>
    <?php
$this->db->from('places')->where('places_parent', $places_id)->where('places_type != ', 4);
$gate = $this->db->get()->result();
?>

    <?php foreach ($gate as $row): ?>
        <p class="center">
            <a href="<?php echo base_url(); ?>report/booth/<?php echo $places->places_landing; ?>/<?php echo $row->places_nicename ?>" target="_blank">
                <?php echo $row->places_name; ?>
            </a>
        </p>
    <?php endforeach;?>
<!--
    <h2 class="center">
        Photobooth Link :
    </h2>
    <?php
$this->db->from('places')->where('places_parent', $places_id)->where('places_type', 4);
$gate = $this->db->get()->result();
?>

    <?php foreach ($gate as $row): ?>
        <p class="center">
            <a href="<?php echo base_url(); ?>photobooth?place=<?php echo $places->places_landing; ?>" target="_blank">
                <?php echo $row->places_name; ?>
            </a>
        </p>
    <?php endforeach;?>
    -->
    <?php endif;?>
    <h2 class="center">
        Gate Checkin Link :
    </h2>
    <?php
$this->db->from('gate')->where('places_id', $places_id);
$gate = $this->db->get()->result();
?>

    <?php foreach ($gate as $row): ?>
        <p class="center">
            <?php if ($places_id == 37): ?>
            <a href="<?php echo base_url(); ?>gateaward/home/<?php echo $places->places_landing; ?>/1/<?php echo $row->id ?>" target="_blank">
        <?php else: ?>
            <a href="<?php echo base_url(); ?>gates/home/<?php echo $places->places_landing; ?>/1/<?php echo $row->id ?>" target="_blank">
        <?php endif;?>
                <?php echo $row->nama; ?>
            </a>
        </p>
    <?php endforeach;?>
    <h2 class="center">
        Gate Checkout Link :
    </h2>
    <?php foreach ($gate as $row): ?>
        <p class="center">
            <a href="<?php echo base_url(); ?>gates/home/<?php echo $places->places_landing; ?>/2/<?php echo $row->id ?>" target="_blank">
                <?php echo $row->nama; ?>
            </a>
        </p>
    <?php endforeach;?>
        <?php if ($places_id != 34 && $places_id != 37): ?>
        <h2 class="center">
            Winner Link :
        </h2>
        <p class="center">
            <a href="<?php echo base_url(); ?>winner" target="_blank">
                List
            </a>
        </p>

        <p class="center">
            <a href="<?php echo base_url(); ?>winner/listmc" target="_blank">
                List by MC
            </a>
        </p>
        <p class="center">
            <a href="<?php echo base_url(); ?>winner/show" target="_blank">
                Show winner (screen 2)
            </a>
        </p>
        <?php endif;?>

</div>