<script type="text/javascript">
    $(function() {
        $("#sdate").datepicker();
        $("#edate").datepicker();
    });
</script>

<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
</div>
<form name="addnews" enctype="multipart/form-data" method="post" action="<?php echo current_url() ?>">
    <?php if (validation_errors()) : ?>
        <div class="statusupdateError">
            <div class="statusupdateicon">
                <img src="<?php echo base_url() ?>assets/images/admin/warningError.png" alt="Error" />
            </div>
            <div class="statusupdatedesc">
                <ul>
                    <?php echo validation_errors('<li>', '</li>'); ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
    <div style="padding-left:0px; padding-bottom:20px;">

        <div id="tabs">
            <div style="float:left; width:270px; padding:7px 0 0 13px;">
                <h4><?php echo $title; ?></h4>
            </div>
        </div>
        <div class="clear"></div>
        <div class="tab_bdr"></div>
        <div class="panel" id="panel3" style="display: block;">
            <div class="formpanel">
                <p>
                    <strong>Nama Barang</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="name" value="<?php if ($datalist->nama_barang != '')
        echo $datalist->nama_barang; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Total Barang Awal</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="totalawal" value="<?php if ($datalist->total_barang_awal != '')
        echo $datalist->total_barang_awal; ?>"  />
                    </label>
                </p>
                <p>
                    <strong>Total Barang Akhir</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="totalakhir" value="<?php if ($datalist->total_barang_akhir != '')
        echo $datalist->total_barang_akhir; ?>"  />
                    </label>
                </p>
                <p>
                    <strong>Tanggal Active</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="date" id="sdate" value="<?php if ($datalist->date_active != '')
                                   echo date("m/d/Y", strtotime($datalist->date_active)); ?>" />
                    </label>
                </p>
                <p>
                    <input type="image" src="<?php echo base_url(); ?>assets/images/admin/button-submit.gif" name="submit"/>
                </p>
            </div>
        </div>
        <div class="clear"></div>
        <div id="tabsbottom"></div>
    </div>
</form>
