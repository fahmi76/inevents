<?php if ($excel == 1): ?>
    <?php
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=user-menshealth-category-$categorynama.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    ?>
<?php endif; ?>
<!-- Title Page -->
<div class="titlepage">
    <h3>User</h3>
</div>
<!-- /Title Page -->

<div class="tablecontenttop">

    <h4 class="titletable">History User</h4></div>


<!-- table content middle -->
<div class="tablecontentmiddle">
    
<a href="<?php echo site_url('chevroletadmin/userdata/' . $barang . '/' . $tgl. '?excel=1'); ?>">eksport excel</a> 
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>No.</strong>
                </th>
                <th>
                    <strong>Nama</strong>
                </th>
                <th>
                    <strong>Email</strong>
                </th>
                <th>
                    <strong>Redeem Barang</strong>
                </th>
                <th>
                    <strong>Tanggal Ambil</strong>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            $x = 1;
            foreach ($data as $row):
                ?>
                <tr class="oddrow">
                    <td class="firstcol">
                        <?php echo $x; ?>
                    </td>
                    <td class="dataitem">
                        <?php echo $row->account_displayname; ?>
                    </td>
                    <td class="dataitem">
                        <?php echo $row->account_email; ?>
                    </td>
                    <td class="dataitem">
                        <?php echo $row->redeem_id; ?>
                    </td>
                    <td class="dataitem">
                        <?php echo $row->date_add; ?>
                    </td>
                </tr>  
                <?php
                $x++;
            endforeach;
            ?>  
        </tbody>
    </table>

</div>
<!-- table content middle   -->           
<div class="tablecontentbottom">&nbsp;</div> 

