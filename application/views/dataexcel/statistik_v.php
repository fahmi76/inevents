<!-- Title Page -->
<div class="titlepage">
    <h3>Statistik Urbanathlon</h3>

</div>
<!-- /Title Page -->
<div style="padding-left:0px; padding-bottom:5px;">
    <div id="tabs">
        <div style="float:left; width:200px; padding-left: 10px;">
            <h4>Statistik Urbanathlon</h4>
        </div>
    </div>
    <div class="clear"></div>

    <div class="tab_bdr"></div>
    <div class="panel" id="panel3" style="display: block;">
        <!-- form panel -->
        <div class="formpanel">

            <!-- large input -->
            
            <h3 class="ins">Waktu Pengambilan RFID dan Race Number</h3>
            <p>
                <?php foreach ($dateuser as $row): ?>
                    <?php echo $row->thedate; ?> : <?php echo $row->total; ?><br />
                <?php endforeach; ?>
            </p>

            <h3 class="ins">Total user yang menukarkan race number</h3>
            <p>
               Total penukaran race : <?php echo $totaluserracepack->total; ?> user<br />
               Total jumlah user : <?php echo $totaluser->total; ?> user
            </p>

            <h3 class="ins">Pengguna Sosial Media</h3>
            <p>
                Facebook : <?php echo $sosmed->fb; ?><br />
                Twitter : <?php echo $sosmed->tw; ?><br />
                Facebook dan Twitter : <?php echo $sosmed->fbtw; ?>
            </p>

            <h3 class="ins">Start dan Finish</h3>
            <p>
                Start : <?php echo $startfinish->start; ?><br />
                Finish : <?php echo $startfinish->finish; ?>
            </p>

        </div>
        <!-- /form panel -->

    </div>
    <div class="clear"></div>
    <div id="tabsbottom"></div>

</div>

<!-- /form window -->

