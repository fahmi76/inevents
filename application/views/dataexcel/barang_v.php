<!-- Title Page -->
<div class="titlepage">
    <h3>Barang</h3>
    <h4><a href="<?php echo site_url('chevroletadmin/barangadd') ?>">Add new Barang</a></h4>
</div>
<!-- /Title Page -->

<div id="tabs">
    <div class="tabtitletable">
        <h4 class="titletable">List Barang</h4>
    </div>
    <div class="clear"></div>
</div>

<div class="tab_bdr"></div>

<div class="tablecontentmiddle">		
</div>

<?php if (count($data)) : ?>
<!-- table content middle -->
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>No.</strong>
                </th>
                <th>
                    <strong>Nama Barang</strong>
                </th>
                <th>
                    <strong>Total Barang Awal</strong>
                </th>
                <th>
                    <strong>Total Barang Akhir</strong>
                </th>
                <th>
                    <strong>Tanggal Berlaku</strong>
                </th>
                <th>
                    <strong>Action</strong>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
                $no = 1;
                foreach ($data as $row):
            ?>
            <tr class="oddrow">
                <td class="firstcol"><?php echo $no; ?></td>
                <td class="dataitem">
                    <?php echo $row->nama_barang; ?>
                </td>

                <td class="edititem">
                    <?php echo $row->total_barang_awal; ?>
                </td>
                <td class="edititem">
                    <?php echo $row->total_barang_akhir; ?>
                </td>

                <td class="dateitem">
                    <?php echo $this->fungsi->dates($row->date_active); ?>
                </td>

                <td class="edititem">
                    <div class="iconedit">
                        <a href="<?php echo site_url(); ?>chevroletadmin/barangedit/<?php echo $row->id; ?>">edit</a>
                    </div>
                </td>
           </tr>

           <?php           
                $this->db->select('');
                $this->db->where('places_parent', $row->id);
                $this->db->where('places_status !=', '0');
                $this->db->from('places');
                $select = $this->db->get()->result();
                $np = 1;
                foreach ($select as $row) :
            ?>

            <tr class="oddrow">
                <td class="firstcol"><?php echo $no . '.' . $np; ?> )</td>
                <td class="edititem">
                    <?php if ($row->places_avatar) : ?>
                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar; ?>" alt="" />
                    <?php endif; ?>
                </td>
                <td class="dataitem">
                    <?php echo $row->places_name; ?><br />
                    <?php echo $row->places_address; ?>
                </td>
                <td class="edititem"><?php echo $row->id; ?></td>
                <td class="dateitem">
                    <?php echo $this->fungsi->dates($row->places_date); ?>
                </td>
                <td class="edititem">
                    <div class="iconedit">
                        <a href="<?php echo site_url(); ?>/admin/placesedit/<?php echo $row->id; ?>">edit</a>
                    </div>
                    <?php if ($userop->account_group == 9): ?>
                    <div class="icondelete">
                        <a href="javascript:confirmDelete('<?php echo site_url(); ?>/admin/placesdelete/<?php echo $row->id; ?>')">delete</a>
                    </div>
                    <?php endif; ?>
                </td>
            </tr>

            <?php $np++; endforeach; $no++; endforeach; ?>
        </tbody>
    </table>
</div>
<!-- table content middle -->
<?php else : ?>
<div class="panel notfound" id="panel3" style="display: block;">
    <h4>Data Not Found</h4>
</div>
<?php endif; ?>
<div id="tabsbottom"></div>
    <?php if (isset($tPaging)): ?>
<div class="pages">
    <?php echo $tPaging ?>
    <div class="clear"></div>
</div>
<?php endif; ?>