<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Wooz.in - Management System</title>
        <link rel="icon" type="image/ico" href="favicon.ico" />

        <script language="JavaScript" type="text/javascript">
            site_url = '<?php echo base_url() ?>';
        </script>

        <!-- main css -->
        <link href="<?php echo base_url(); ?>assets/css/admin.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/modal.css" rel="stylesheet" type="text/css" />
        <!-- /main css -->

        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php echo base_url(); ?>assets/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">
            var url = "<?php echo base_url(); ?>index.php/admin/settingedit";
        </script>
        <script src="<?php echo base_url(); ?>assets/javascripts/modal.window.js" type="text/javascript" charset="utf-8"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />

        <script type="text/javascript">
            function openFileBrowser(id){
                fileBrowserlink = "<?php echo base_url(); ?>assets/javascripts/pdw_file_browser/index.php?editor=standalone&returnID=" + id;
                window.open(fileBrowserlink,'pdwfilebrowser', 'width=1000,height=650,scrollbars=no,toolbar=no,location=no');
            }

            $(document).ready(function(){
                $("p#checkgroupuser").hide();
                $("select#selectgroupuser").change(function(){
                    var optgrpuser = $(".optgroupuser:selected").val();
                    if(optgrpuser == 3){
                        $("p#checkgroupuser").show();
                    }
                    return false;
                });
            });
        </script>        
    </head>

    <body>
        <!-- header -->
        <div id="header">            
            <?php if (isset($admin) && $admin != "") : ?>
                <div class="menustatus">
                    <h5>Logged in as Administrator
                        | <a href="<?php echo site_url(); ?>niaga/logout">Logout</a>
                    </h5>
                </div>
            <?php endif; ?>
            <div class="clear"></div>
        </div>
        <!-- /header -->

        <!-- middle -->
        <div id="middle">
            <?php if (isset($admin) && $admin != "") : ?>
                <!-- menu -->
                <div class="middletop">
                    <h4 class="menu"><a href="<?php echo site_url() ?>niaga">Dashboard</a></h4>
                    <h4 class="menu"><a href="<?php echo site_url() ?>niaga/user">User Registrasi</a></h4>
                    <!--
                    <h4 class="menu"><a href="<?php echo site_url() ?>niaga/statistik?pseudo=<?php echo time(); ?>">Statistik</a></h4>
                    <h4 class="menu"><a href="<?php echo site_url() ?>niaga/barang?pseudo=<?php echo time(); ?>">Barang</a></h4>
                    -->
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
                <!-- /menu -->
            <?php endif; ?>

            <!-- line spacer -->
            <div class="middlebottom">
                <div class="footerspacer">&nbsp;</div>
            </div>
            <!-- /line spacer -->

            <!-- middleCONTENT -->
            <div class="middlecontent">
                <!-- Side Right -->
                <div class="sideright">
                    <?php echo $content; ?>
                </div>
                <!-- /Side Right -->
            </div>
            <!-- middleCONTENT -->

            <!-- line spacer -->
            <div class="middlebottom">
                <div class="footerspacer">&nbsp;</div>
            </div>
            <!-- /line spacer -->

            <!-- middleBOTTOM -->
            <div class="middlebottom">
                <div class="middlefooter">
                    <div class="middlefooterleft">&copy; 2010. <a href="<?php echo site_url(); ?>">Wooz.in</a></div>
                    <div class="clear"></div>
                </div>
            </div>
            <!-- /middleBOTTOM -->
        </div>
        <!-- /middle -->

    </body>
</html>
