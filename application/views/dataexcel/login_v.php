<form action="<?php echo current_url(); ?>" method="post" name="login">
    <div class="formpanel">
        <?php if (validation_errors()) : ?>
            <div class="signupError marginbottom10">Terdapat Kesalahan
                <ul class="ulwarning">
                    <?php echo validation_errors('<li>', '</li>'); ?>
                </ul>
            </div>
        <?php endif; ?>
        <?php if ($hasil == 1) : ?>
            <div class="signupError marginbottom10">Terdapat Kesalahan
                <ul class="ulwarning">
                    Username atau password salah
                </ul>
            </div>
        <?php endif; ?>
        <!-- small input -->
        <p>
            <strong>Username</strong><br/>
            <label class="smallinput">
                <input type="text" style="width:30%" name="userlogin"/>
            </label>
        </p>
        <!-- small input -->

        <!-- small input -->
        <p>
            <strong>Password</strong><br/>
            <label class="smallinput">
                <input type="password" style="width:30%" name="passlogin"/>
            </label>
        </p>
        <!-- small input -->
        <p>
            <input type="image" src="<?php echo base_url() ?>assets/images/admin/button-submit.gif" style="margin:20px 0 10px 0" name="submit" />
        </p>

    </div>
    <!-- /form panel -->
</form>
