<?php if ($excel == 1): ?>
	<?php $date = date('Y-m-d');?>
    <?php
    header("Content-type: application/x-msdownload");
    header("Content-Disposition: attachment; filename=list-user-cimbniaga-$date.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    ?>
<?php endif; ?>
<div>

    <h4>List User</h4></div>


<!-- table content middle -->
<div>
    <p>
		Total registrasi = <?php echo $total; ?> user
	</p>
	<table>
        <thead>
            <tr>
                <th>
                    <strong>No.</strong>
                </th>
                <th>
                    <strong>Nama</strong>
                </th>
                <th>
                    <strong>Email</strong>
                </th>
                <th>
                    <strong>Alamat</strong>
                </th>
                <th>
                    <strong>Kota</strong>
                </th>
                <th>
                    <strong>Propinsi</strong>
                </th>
                <th>
                    <strong>Nomor SIM</strong>
                </th>
                <th>
                    <strong>Agama</strong>
                </th>
                <th>
                    <strong>Nomor Telp</strong>
                </th>
                <th>
                    <strong>Tempat Lahir</strong>
                </th>
                <th>
                    <strong>Tgl Lahir</strong>
                </th>
                <th>
                    <strong>Jenis Kelamin</strong>
                </th>
                <th>
                    <strong>Golongan Darah</strong>
                </th>
                <th>
                    <strong>Nama Club</strong>
                </th>
                <th>
                    <strong>Kota Club</strong>
                </th>
                <th>
                    <strong>Merk Motor</strong>
                </th>
                <th>
                    <strong>Tipe Motor</strong>
                </th>
                <th>
                    <strong>Nomor Rangka</strong>
                </th>
                <th>
                    <strong>Facebook</strong>
                </th>
                <th>
                    <strong>Twitter</strong>
                </th>
                <th>
                    <strong>Tanggal Registrasi</strong>
                </th>
                <th>
                    <strong>Registrasi Ulang</strong>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php 
            $x = 1;
            foreach ($data as $row):
                ?>
				<?php				
					if($row->account_fb_id != 0){
						$fb = 'ya';
					}else{
						$fb = 'tidak';
					}
					if($row->account_tw_id != 0){
						$tw = 'ya';
					}else{
						$tw = 'tidak';
					}
					if($row->account_rfid != 0){
						$rfid = 'ya';
					}else{
						$rfid = 'tidak';
					}
					#$date =explode(" ",$row->account_joindate);
				?>
                <tr>
                    <td>
                        <?php echo $x; ?>
                    </td>
                    <td>
                        <?php echo $row->account_displayname; ?>
                    </td>
                    <td>
                        <?php echo $row->account_email; ?>
                    </td>
                    <td>
                        <?php echo $row->account_address; ?>
                    </td>
                    <td>
                        <?php echo $row->account_kota; ?>
                    </td>
                    <td>
                        <?php echo $row->account_propinsi; ?>
                    </td>
                    <td>
                        '<?php echo $row->account_sim; ?>'
                    </td>
                    <td>
                        <?php echo $row->account_religion; ?>
                    </td>
                    <td>
                        <?php echo $row->account_phone; ?>
                    </td>
                    <td>
                        <?php echo $row->account_birthdate_places; ?>
                    </td>
                    <td>
                        <?php echo $row->account_birthdate; ?>
                    </td>
                    <td>
                        <?php echo $row->account_gender; ?>
                    </td>
                    <td>
                        <?php echo $row->account_golongan_darah; ?>
                    </td>
                    <td>
                        <?php echo $row->account_nama_club; ?>
                    </td>
                    <td>
                        <?php echo $row->account_kota_club; ?>
                    </td>
                    <td>
						<?php if($row->account_merk_motor != 17):?>
							<?php echo $this->dataexcel_m->getmotor($row->account_merk_motor); ?>
						<?php else: ?>
							<?php echo $row->account_merk_motor_lain; ?>
						<?php endif; ?>
                    </td>
                    <td>
                        <?php echo $row->account_tipe_motor; ?>
                    </td>
                    <td>
                        <?php echo $row->account_nomor_rangka; ?>
                    </td>
                    <td>
                        <?php echo $fb; ?>
                    </td>
                    <td>
                        <?php echo $tw; ?>
                    </td>
                    <td>
                        <?php echo $row->account_joindate; ?>
                    </td>
                    <td>
                        <?php echo $rfid;?>
                    </td>
                </tr>  
                <?php
                $x++;
            endforeach;
            ?>  
        </tbody>
    </table>

</div>

