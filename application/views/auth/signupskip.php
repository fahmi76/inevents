
<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Sign up</span><br />
                    <span class="Muse30018reg">3. Connect With Your Social Network</span>
                </div>
                <div class="formbox" >
                    <?php if($pole == 1) : ?>
                    <ul class="navlist">
                        <li>
                            By Connecting To Foursquare, You Allow Wooz.In To Automatically Update Your Status
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <ul class="navlist">
                        <li>
                            <a href="<?php echo site_url('home/foursquare'); ?>">
                                <img src="<?php echo base_url(); ?>assets/images/icon/connect-foursquare.png" alt="connect with foursquare" />
                            </a> Or You Can <a href="<?php echo site_url('signupskip/2') ?>" class="redstyle"> Skip This Step</a>
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php else : ?>
                    <ul class="navlist">
                        <li>
                            By Connecting To Twitter You Allow Wooz.In To Automatically Update Your Status
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <ul class="navlist">
                        <li>
                            <a href="<?php echo site_url('home/twit/signup'); ?>">
                                <img alt="connect with twitter" src="<?php echo base_url().'assets/images/icon/connect-twitter.png';?>" />
                            </a> Or You Can <a href="<?php echo site_url('signupdone') ?>" class="redstyle"> Skip This Step</a>
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php endif; ?>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>

            </div>
        </div>
    </section>
</article>