
<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Signup</span><br />
                </div>
                <div class="formbox" >
                    <?php if($pole == 1) : ?>
                    <ul class="navlist">
                        <li>
                            No account
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <ul class="navlist">
                        <li>
                            <a href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('home/facebook'), 'next' => site_url('home/facebook'), 'req_perms' => $fb_perms));?>" class="redstyle"> Ya </a>
                            Or <a href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('home/facebook'), 'redirect_uri' => site_url('home/facebook/done'), 'scope' => $fb_perms)); ?>" class="redstyle"> Tidak </a>
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php else : ?>
                    <ul class="navlist">
                        <li>
                            By Connecting To Twitter You Allow Wooz.In To Automatically Update Your Status
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <ul class="navlist">
                        <li>
                            <a href="<?php echo site_url('signupdone') ?>" class="redstyle"> Skip This Step</a>
                            Or You Can <a href="<?php echo site_url('signupdone') ?>" class="redstyle"> Skip This Step</a>
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php endif; ?>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>

            </div>
        </div>
    </section>
</article>