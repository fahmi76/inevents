
<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">CEK</span><br />
                </div>
                <div class="formbox" >
                    <?php if($pole == 'step1') : ?>
                    <ul class="navlist">
                        <li>
                            Account Facebook sudah terdaftar atas nama <?php echo $datalist->account_displayname; ?>
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <ul class="navlist">
                        <li>
                         <?php if ($datalist->account_avatar) : ?>
                        <img src="<?php echo base_url(); ?>resizer/avatar.php?w=120&img=<?php echo $datalist->account_avatar; ?>" alt="" />
                        <?php else: ?>
                        <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=120&img=user/default.png" alt="" />
                        <?php endif; ?>
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <ul class="navlist">
                        <li>
                            Apakah ini account anda?
                        </li>
                        <li class="clear"></li>
                    </ul> 
                    <ul class="navlist">
                        <li>
                            <?php if($datalist->account_passwd != '' ): ?>
                            <a href="<?php echo site_url('found/'.$datalist->id); ?>" class="redstyle"> Ya </a>
                            <?php else: ?>
                            <a href="<?php echo site_url('cekdone/step3'); ?>" class="redstyle"> Ya </a>
                            <?php endif; ?>
                            Or <a href="<?php #$token = $this->facebook->getAccessToken();
            $logoutUrl = $this->facebook->getLogoutUrl(array('next' => site_url('logout'), 'access_token' => $datalist->account_fb_token)); echo $logoutUrl; ?>" class="redstyle"> Tidak </a>
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php elseif($pole == 'step2') : ?>
                    <ul class="navlist">
                        <li>
                            Kita akan verifikasi ke email
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php elseif($pole == 'step3') : ?>
                    <ul class="navlist">
                        <li>
                            Kita akan verifikasi ke email
                            <meta http-equiv="refresh" content="2;URL=<?php echo site_url('cekdone/step3'); ?>">
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php else : ?>
                    <ul class="navlist">
                        <li>
                            Silahkan periksa email anda di <?php echo $datalist->account_email; ?>
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php endif; ?>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>

            </div>
        </div>
    </section>
</article>