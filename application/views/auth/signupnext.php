<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <?php if(validation_errors()) : ?>
                    <span class="Muse70030">Whoops ! Something wrong with your Sign Up Process</span>
                    <br>
                        <?php echo validation_errors('<span class="Muse30018reg">', '</span>'); ?>

                    <?php else : ?>
                    <span class="Muse70030">Sign up</span>
                    <br>
                    <span class="Muse30018reg">2. Few things to fill then voila you are ready to use your wooz.in</span>
                    <?php endif; ?>
                </div>
                <div class="formbox" >
                    <form action="<?php echo current_url(); ?>" method="post">                        
                        <ul class="navlist">
                            <li class="listyle arial17">
                                RFID Serial :
                            </li>
                            <li>
                                <input name="serial" id="txtpassword" type="text" class="inputtype arial17" />
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial17">
                                &nbsp;
                            </li>
                            <li>
                                <input type="image" src="<?php echo base_url(); ?>assets/images/create.gif">
                            </li>
                            <li class="clear"></li>
                        </ul>
                    </form>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>

            </div>
        </div>
    </section>
</article>