<h2 class="desc">Hi, <?php echo $user->account_displayname; ?></h2>
<?php if($from == 'tw' && $datalist->account_fbid && $datalist->account_token): ?>
<p>Maaf Akun Facebook atas nama <?php echo $datalist->account_displayname; ?> sudah terdaftar, Jadi tidak bisa digunakan.</p>
<p>	
     <?php if ($datalist->account_avatar) : ?>
        <img src="<?php echo $uploads_url; ?>/resizer/avatar.php?w=120&img=<?php echo $datalist->account_avatar; ?>" alt="" />
    <?php else: ?>
        <img src="<?php echo $uploads_url; ?>/resizer/thumb2.php?w=120&img=user/default.png" alt="" />
    <?php endif; ?>   
</p>
<?php  ?>
<?php endif; ?>
<?php if($from == 'fb' && $datalist->account_tw_token && $datalist->account_tw_secret): ?>
<p>Maaf Akun Twitter atas nama <?php echo $datalist->account_tw_username; ?> sudah terdaftar, Jadi tidak bisa digunakan.</p>
<p>	
    <?php if ($datalist->account_avatar) : ?>
        <img src="<?php echo $uploads_url; ?>/resizer/avatar.php?w=120&img=<?php echo $datalist->account_avatar; ?>" alt="" />
    <?php else: ?>
        <img src="<?php echo $uploads_url; ?>/resizer/thumb2.php?w=120&img=user/default.png" alt="" />
    <?php endif; ?>
</p>
<?php endif; ?>
Mau mendaftar menggunakan account lain atau selesai disini.
<p>
        <?php if($from == 'fb'): ?>
	<a class="reg-done" href="<?php echo base_url(); ?>home/twit/cekrfid/<?php echo $customs ?>/<?php echo $user->id ?>/<?php echo $spot_id; ?>">
		Ganti akun twitter lain
	</a>
        <?php else: ?>
        <?php  $redirect = base_url().'home/facebook/cekrfid/'.$customs.'/'.$user->id.'/'.$spot_id; ?>
        <?php $token = $this->facebook->getAccessToken(); ?>
	<a class="cancel button" href="<?php echo $this->facebook->getLogoutUrl(array('next' => $redirect, 'access_token' => $token)); ?>">
		Ganti akun facebook lain
	</a>
        <?php endif; ?>
</p>
<p>
	<a class="reg-done" href="<?php echo site_url('cekrfid/home/'.$user->id.'?url='.$url); ?>">
		Back
	</a>
</p>