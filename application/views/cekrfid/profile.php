
<div class="alert alert-warning small">Profile User</div>
<div class="panel panel-info">
</div>
<div class="form-group">
    <label for="InputName">Nama : </label>
    <label for="InputName"><?php echo $info->account_displayname; ?> </label>        
</div>
<div class="form-group">
    <label for="InputName">Email : </label>
    <label for="InputName"><?php echo $info->account_email; ?> </label>        
</div>
<?php if ($info->account_fbid && $info->account_token && $cek_fb == 1): ?>
    <div class="form-group">
        <label for="InputName">Facebook name : </label>
        <label for="InputName"><?php echo $info->account_fb_name; ?> </label>        
    </div>
<?php else: ?>
    <div class="form-group">
        <label for="InputName">Connect with Facebook? </label><br />
        <?php  $redirect = base_url().'home/facebook/cekrfid/'.$customs.'/'.$info->id.'/'.$spot_id; ?>
        <a class="fb-connect" href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('logout'), 'redirect_uri' => $redirect, 'scope' => $fb_perms)); ?>">
            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
        </a>       
    </div>
<?php endif; ?>
<?php if ($info->account_tw_token && $info->account_tw_secret): ?>
    <div class="form-group">
        <label for="InputName">Twitter name : </label>
        <label for="InputName"><?php echo $info->account_tw_username; ?> </label>        
    </div>
<?php else: ?>
    <div class="form-group">
        <label for="InputName">Connect with Twitter? </label><br />
        <a class="tw-connect" href="<?php echo base_url(); ?>home/twit/cekrfid/<?php echo $customs ?>/<?php echo $info->id ?>/<?php echo $spot_id; ?>">
            <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
        </a>     
    </div>
<?php endif; ?>


<form method="post" role="form" action="<?php echo current_url().'?url='.$customs; ?>" id="registration">    
    <div class="form-group">
        <label for="rfidNumber">Your Wristband</label>
            <div class="form-group">
                <input type="text" name="left" class="form-control input-lg" id="rfidNumber" placeholder="000" value="<?php echo $info->account_rfid; ?>" autocomplete="off">

            </div>
        </div>
    </div>           
	<div class="text-center">   
        <button type="submit" class="btn btn-primary btn-xlg">Change RFID</button>         
    </div>
</form>        
	<div class="text-center"><br />
    <a href ="<?php echo base_url(); ?>cekrfid?url=<?php echo $customs ?>"><button type="submit" class="btn btn-primary btn-xlg">Finish</button></a>
	</div>