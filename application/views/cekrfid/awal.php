<script type="text/javascript">
    $(document).ready(function() {
        $("#rfidNumber").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
    //document.forms['input_submit'].style.visibility = "hidden";
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function autosubmit(original) {
        if (original.value.length === 10) {
            document.getElementById('registration').submit();
        }
    }
</script>
<br>
<?php if($error == 0): ?>
<div class="panel panel-info">
    <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
            <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
</div>
<div class="alert alert-danger">Check your <?php echo $card; ?> now. Place Tap <?php echo $card; ?> onto the reader</div>
<form method="post" role="form" action="<?php echo current_url() . '/?url=' . $customs; ?>" id="registration">
    <div class="form-group">
        <label for="rfidNumber" class="sr-only">RFID Number</label>
        <input type="text" name="serial" onblur="displayResult(this);" onKeyUp="autosubmit(this);" class="form-control input-lg" id="rfidNumber" placeholder="Your RFID number" autocomplete="off">
    </div>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
    </div>
</form>
<?php else: ?>
    <h2>Sorry</h2>
    <hr>
    <div class="text-center">
        <p>Your <?php echo $card; ?> is not registered, please registered again</p>
        <br>
    </div>
    <meta http-equiv="refresh" content="3;URL=<?php echo site_url('cekrfid?url=' . $url); ?>" />
<?php endif; ?>
