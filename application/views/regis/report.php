<style type="text/css">
	.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 2px solid #006699; -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px; }.datagrid table td, .datagrid table th { padding: 6px 20px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #006699), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #006699 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#006699', endColorstr='#00557F');background-color:#006699; color:#FFFFFF; font-size: 16px; font-weight: bold; border-left: 2px solid #0070A8; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00496B; border-left: 2px solid #E1EEF4;font-size: 13px;border-bottom: 3px solid #E1EEF4;font-weight: normal; }.datagrid table tbody .alt td { background: #E1EEF4; color: #00496B; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }
</style>
<h2>Report</h2>
<hr>
<div class="text-center">
	<div class="datagrid">
		<table>
			<thead><tr><th>Date</th><th>#RSVP</th><th># WITH GUEST</th><th># SOUTH SHUTTLE</th><th># TAKE THE ELEVATOR HOME</th></tr></thead>
			<tbody>
				<?php $x = 1; foreach($data as $row): ?>
				<?php if($x%2 == 0): ?>
					<tr class="alt">
				<?php else: ?>
					<tr>
				<?php endif; ?>
					<td><?php echo $row['date']; ?></td>
					<td><?php echo $row['rsvp']; ?></td>
					<td><?php echo $row['guest']; ?></td>
					<td><?php echo $row['shuttle']; ?></td>
					<td><?php echo $row['elevator']; ?></td>
				</tr>
				<?php $x++; endforeach; ?>
			</tbody>
		</table>
	</div>
<br>
    <a href="<?php echo base_url(); ?>report/mandrill" onclick="alert('Send Email!')"><button type="button" class="btn btn-primary btn-xlg">Send Email</button></a>
</div>