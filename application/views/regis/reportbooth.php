<link href="<?php echo base_url('assets/datatables/datatables/css/dataTables.bootstrap.css') ?>" rel="stylesheet">
<div id="content" class="span10">
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List Booth</h2>

            </div>
            <div class="box-content">

        <table id="table" class="table table-striped table-bordered bootstrap-datatable datatable" >
            <thead>
                <tr>
                    <th>Username</th>
                    <?php foreach ($tabel as $rowtabel): ?>
                    <th><?php echo $rowtabel->field_name; ?></th>
                    <?php endforeach;?>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
            </tbody>

            <tfoot>
                <tr>
                    <th>Username</th>
                    <?php foreach ($tabel as $rowtabel): ?>
                    <th><?php echo $rowtabel->field_name; ?></th>
                    <?php endforeach;?>
                    <th>Time</th>
                </tr>
            </tfoot>
        </table>
            </div>
        </div><!--/span-->

    </div><!--/row-->


<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/datatables/datatables/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables/js/dataTables.bootstrap.js') ?>"></script>


<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('report/ajax_list/' . $gate . '/' . $check . '/' . $status . '/' . $id) ?>",
            "type": "POST"
        },
        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
        "scrollX": true

    });

});

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}

</script>