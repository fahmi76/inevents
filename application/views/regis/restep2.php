<script type="text/javascript">
$(document).ready(function() {
    $("#InputRFID").focus();
    $("#registration").submit(function() {
        $('.action').html('<img src="http://woozin.tgrid.in/assets/customs/loader.gif" /> Please Wait...');
    });
});
</script>
<div class="panel panel-info">
    <?php if (validation_errors()): ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
    </p>
    <?php endif;?>
</div>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Full Name</label>
        <input type="text" class="form-control input-lg" name="name" id="InputName" autocomplete="off" value="<?php echo $hasil->account_displayname; ?>" placeholder="Your Name" disabled>
    </div>
    <div class="form-group">
        <label for="InputName">Email</label>
        <input type="text" class="form-control input-lg" name="email" id="InputName" autocomplete="off" value="<?php echo $email; ?>" placeholder="Email">
    </div>
    <div class="form-group">
        <label class="InputName">Organization</label>
        <input type="text" class="form-control input-lg" name="organization" id="InputName" autocomplete="off" value="<?php echo $organization; ?>" placeholder="Organization">
    </div>
    <div class="form-group">
        <label class="InputName">Business Tel. No.</label>
        <input type="text" class="form-control input-lg" name="business-tel-no" id="InputName" autocomplete="off" value="<?php echo $btn; ?>" placeholder="Business Tel. No.">
    </div>
    <div class="form-group">
        <label class="InputName">Mobile Tel. No.</label>
        <input type="text" class="form-control input-lg" name="mobile-tel-no" id="InputName" autocomplete="off" value="<?php echo $mtn; ?>" placeholder="Mobile Tel. No.">
    </div>
    <div class="form-group">
        <label class="InputName">Products & Services</label>
        <input type="text" class="form-control input-lg" name="products-services" id="InputName" autocomplete="off" value="<?php echo $product; ?>" placeholder="Products & Services">
    </div>
    <div class="form-group">
        <label class="InputName">RFID</label>
        <input type="text" class="form-control input-lg" name="rfid" id="InputRFID" autocomplete="off" value="<?php echo $rfid; ?>" placeholder="RFID">
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
    </div>
</form>
