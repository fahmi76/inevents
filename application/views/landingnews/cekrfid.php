<script type="text/javascript">
    $(document).ready(function() {
        $("#rfidNumber").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
    //document.forms['input_submit'].style.visibility = "hidden";
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function autosubmit(original) {
        if (original.value.length === 10) {
            document.getElementById('registration').submit();
        }
    }
</script>
<br>
<?php if($error == 0): ?>
<div class="panel panel-info">
    <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
            <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
</div>
<div class="alert alert-danger">Check your CIMB Niaga Gear now. Place Tap CIMB Niaga Gear onto the reader</div>
<form method="post" role="form" action="<?php echo current_url() . '/?url=' . $customs.'&places='.$spot_id; ?>" id="registration">
    <div class="form-group">
        <label for="rfidNumber"  class="sr-only">RFID Number</label>
        <input type="text" name="serial" onblur="displayResult(this);" onKeyUp="autosubmit(this);" class="form-control input-lg" id="rfidNumber" placeholder="Your RFID number" autocomplete="off">
    </div>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Activate</button>
    </div>
</form>
<?php elseif($error == 1):?>
    <h2>Hello, <?php echo $name; ?></h2>
    <hr>
    <div class="text-center">
        <p>This is your CIMB Niaga Gear?</p>
        <br>
        <a href="<?php echo site_url('landing/steprfid/cekrfid?url=' . $url.'&accid='.$acc_id.'&places='.$spot_id.'&final=1'); ?>">
			<button type="submit" class="btn btn-primary btn-xlg">Yes
			</button>
		<a href="<?php echo site_url('landing/steprfid/changerfid?url=' . $url.'&places='.$spot_id.'&final=2'); ?>">
			<button type="submit" class="btn btn-primary btn-xlg">No</button>
		</a>
    </div>
<?php elseif($error == 3):?>
    <h2>Hello, <?php echo $name; ?></h2>
    <hr>
    <div class="text-center">
        <p>Please keep your CIMB Niaga Gear and Thank You</p>
        <br>
    </div>
    <meta http-equiv="refresh" content="4;URL=<?php echo site_url('landing/home?url=' . $url.'&places='.$spot_id); ?>" />	
<?php else: ?>
    <h2>Sorry</h2>
    <hr>
    <div class="text-center">
        <p>Your CIMB Niaga Gear is not registered, please registered again</p>
        <br>
    </div>
    <meta http-equiv="refresh" content="3;URL=<?php echo site_url('landing/home?url=' . $url.'&places='.$spot_id); ?>" />
<?php endif; ?>
