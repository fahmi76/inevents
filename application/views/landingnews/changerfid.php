<script type="text/javascript">
    $(document).ready(function() {
        $("#rfidNumber").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
    //document.forms['input_submit'].style.visibility = "hidden";
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function autosubmit(original) {
        if (original.value.length === 10) {
            document.getElementById('registration').submit();
        }
    }
</script>
<br>
<?php if($final == 2): ?>
	<div class="panel panel-info">
	<?php if($error == 2): ?>
		<p class="error">Whoops ! Something wrong<br />
			Please try again, email not registered
		</p>
	<?php endif; ?>
		<?php if (validation_errors()) : ?>
			<p class="error">Whoops ! Something wrong
				<?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
			</p>
		<?php endif; ?>
	</div>
	<div class="alert alert-danger">Fill your email</div>
	<form method="post" role="form" action="<?php echo current_url() . '/?url=' . $customs.'&places='.$spot_id.'&final='.$final; ?>" id="registration">
		<div class="form-group">
			<label for="rfidNumber"  class="sr-only">Email</label>
			<input type="text" name="email" class="form-control input-lg" id="rfidNumber" placeholder="Your Emai" autocomplete="off">
		</div>
		<div class="text-center">            
			<button type="submit" class="btn btn-primary btn-xlg">Submit</button>
		</div>
	</form>
<?php else:?>
	<?php if($error == 3): ?>
		<h2>Hello, <?php echo $name; ?></h2>
		<div class="alert alert-danger">CIMB Niaga Gear successfully update<br />Please wait</div>
		<meta http-equiv="refresh" content="4;URL=<?php echo site_url('landing/steprfid/cekrfid?url=' . $url.'&places='.$spot_id); ?>" />	
	<?php else: ?>
		<div class="panel panel-info">
			<?php if (validation_errors()) : ?>
				<p class="error">Whoops ! Something wrong
					<?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
				</p>
			<?php endif; ?>
		</div>
		<h2>Hello, <?php echo $name; ?></h2>
		<div class="alert alert-danger">Tap your CIMB Niaga Gear now. Place Tap CIMB Niaga Gear onto the reader</div>
		<form method="post" role="form" action="<?php echo current_url() . '/?url=' . $customs.'&places='.$spot_id.'&id='.$acc_id.'&final='.$final; ?>" id="registration">
			<div class="form-group">
				<label for="rfidNumber"  class="sr-only">RFID Number</label>
				<input type="text" name="serial" onblur="displayResult(this);" onKeyUp="autosubmit(this);" class="form-control input-lg" id="rfidNumber" placeholder="Your RFID number" autocomplete="off">
			</div>
			<div class="text-center">            
				<button type="submit" class="btn btn-primary btn-xlg">Activate</button>
			</div>
		</form>	
	<?php endif; ?>
<?php endif; ?>
