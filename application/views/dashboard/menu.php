<div class="nav-collapse sidebar-nav">
    <ul class="nav nav-tabs nav-stacked main-menu">
        <li><a href="<?php echo base_url() . 'dashboard'; ?>"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>
        <li><a href="<?php echo base_url() . 'dashboard/home/report'; ?>"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Report</span></a></li>
        <?php if ($admin_group == 9 || $admin_group == 10): ?>
        <li><a href="<?php echo base_url() . 'dashboard/admin'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> Admin</span></a></li>
        <?php endif;?>
    </ul>
</div>