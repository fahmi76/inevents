<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('dashboard/header'); ?>

    </head>

    <body>
        <!-- start: Header -->
        <?php $this->load->view('dashboard/navigasi'); ?>
        <!-- start: Header -->

        <div class="container-fluid-full">
            <div class="row-fluid">

                <!-- start: Main Menu -->
                <div id="sidebar-left" class="span2">
                    <?php $this->load->view('dashboard/menu'); ?>
                </div>
                <!-- end: Main Menu -->

                <noscript>
                <div class="alert alert-block span10">
                    <h4 class="alert-heading">Warning!</h4>
                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                </div>
                </noscript>

                <!-- start: Content -->
                <?php echo $content; ?>
                <!-- end: Content -->
            </div><!--/#content.span10-->
        </div><!--/fluid-row-->


        <div class="clearfix"></div>

        <footer>

        </footer>

        <!-- start: JavaScript-->
        <?php echo $this->load->view('dashboard/javascript'); ?>
        <!-- end: JavaScript-->

    </body>
</html>
