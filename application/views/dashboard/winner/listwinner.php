<link href="<?php echo base_url('assets/datatables/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
<script type="text/javascript">
    function back(){
        top.location.href="<?=site_url('dashboard/event');?>";
    }
</script>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>
                    <?php if($pages == 1): ?>
                        Winner Category
                    <?php else: ?>
                        Presenters Category
                    <?php endif; ?></h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/winner/home/download/'.$id.'/'.$pages; ?>"><i class="halflings-icon download"></i></a>
                    <?php if($pages == 1): ?>
                    <a href="<?php echo base_url() . 'dashboard/winner/add'; ?>"><i class="halflings-icon plus"></i></a>
                    <?php else: ?>
                    <a href="<?php echo base_url() . 'dashboard/winner/add/presenter'; ?>"><i class="halflings-icon plus"></i></a>
                    <?php endif; ?>
                </div>

            </div>
            <div class="box-content">

        <table id="table" class="table table-striped table-bordered bootstrap-datatable datatable" >
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <?php if($pages == 2): ?>
                    <th>Number</th>
                    <?php endif; ?>
                    <th>Status</th>
                    <th>Datetime</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>

            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <?php if($pages == 2): ?>
                    <th>Number</th>
                    <?php endif; ?>
                    <th>Status</th>
                    <th>Datetime</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>

                <p>
                    <button class="btn btn-large" onclick="back()"><i class="halflings-icon white arrow-left"></i> Back</button>
                </p>         
            </div>
        </div><!--/span-->

    </div><!--/row-->


<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/datatables/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables/js/dataTables.bootstrap.js')?>"></script>


<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        <?php if($pages == 1): ?>
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('dashboard/winner/home/ajax_list/'.$places_id.'/'.$id)?>",
            "type": "POST"
        },
        <?php else: ?>
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('dashboard/winner/home/ajax_list_presenter/'.$places_id.'/'.$id)?>",
            "type": "POST"
        },
        <?php endif; ?>
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });
    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});


function edit_person(id)
{
    top.location.href="<?=site_url('dashboard/winner/edit/main');?>/"+id;
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function delete_person(id)
{
    top.location.href="<?=site_url('dashboard/winner/home/delete');?>/"+id;
}

</script>