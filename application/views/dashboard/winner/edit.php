<script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />   

<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>dashboard/winner/home/search/11",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id);  
                    }
        });
    });
</script>
<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Edit User</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit User</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal"  onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">                        
                            <label class="control-label" for="focusedInput">Name</label>
                            <?php
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } elseif (!$datauser) {
                                $names = '';
                            } else {
                                $names = $datauser->account_displayname;
                            }
                            $id = set_value('id');
                            if ($id) {
                                $ids = $id;
                            } elseif (!$datauser) {
                                $ids = '';
                            } else {
                                $ids = $datauser->account_id;
                            } 
                            ?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <input class="input-xlarge focused" name="id" autocomplete="off" id="InputId" type="hidden" value="<?php echo $ids; ?>">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">
                            <label class="control-label">Category</label>
                            <div class="controls">
                                <?php foreach($gate as $row): ?>
                                <label class="radio inline">
                                    <input type="radio" name="gate" id="inlineCheckbox1" value="<?php echo $row->id; ?>" <?php if($row->id == $datauser->winner_id): ?>checked<?php endif; ?>> 
                                        <?php echo $row->nama; ?>
                                </label><br>
                            <?php endforeach ; ?>
                            <?php echo form_error('gate', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     