<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Winner Category</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/winner/add/home'; ?>"><i class="halflings-icon plus"></i></a>
                </div>

            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->nama ?></td>
                                <td class="center">
                                    <a class="btn btn-info" href="<?php echo base_url(); ?>dashboard/winner/home/mainlist/<?php echo $id; ?>/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white edit"></i> List Winner
                                    </a>
                                    <?php if($row->data_status == 1): ?>
                                    <a class="btn btn-warning" href="<?php echo base_url(); ?>dashboard/winner/home/lock/<?php echo $id; ?>/<?php echo $row->id; ?>/2">
                                        <i class="icon-lock"></i> Lock 
                                    </a>
                                    <?php else: ?>
                                    <a class="btn btn-primary" href="<?php echo base_url(); ?>dashboard/winner/home/lock/<?php echo $id; ?>/<?php echo $row->id; ?>/1">
                                        <i class="icon-unlock"></i> Unlock 
                                    </a>
                                    <?php endif; ?>
                                    <?php if($row->toogle == 0): ?>
                                        <a class="btn btn" href="<?php echo base_url(); ?>dashboard/winner/home/cektoogle/<?php echo $id; ?>/<?php echo $row->id; ?>/1">
                                            <i class="halflings-icon white edit"></i> check all user
                                        </a>
                                    <?php else: ?>
                                        <a class="btn btn-primary" href="<?php echo base_url(); ?>dashboard/winner/home/cektoogle/<?php echo $id; ?>/<?php echo $row->id; ?>/0">
                                            <i class="halflings-icon white edit"></i> uncheck all user
                                        </a>
                                    <?php endif; ?>
                                    <a class="btn btn-success" href="<?php echo base_url(); ?>dashboard/winner/add/home/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white edit"></i> Edit
                                    </a>
                                    <a class="btn btn-danger" href="<?php echo base_url(); ?>dashboard/winner/home/deletecat/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white trash"></i> Delete
                                    </a> 
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->