<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=".$title.".xls");
 
?>
<html>
<head>
    <style>
        body {
            font-family: Arial;
        }
        table {
            border-collapse: collapse;
        }
        th {
            background-color: #cccccc;
        }
        th, td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
    <?php if(isset($name) && $name): ?>
        <h2><?php echo $name->nama; ?></h2>
    <?php endif; ?>
    <table>
        <thead>
        <tr>
            <th>Username</th>
            <th>Table Number</th>
            <th>Status</th>
            <th>Time</th>
        </tr>
        </thead>
        <tbody>
			<?php foreach($user as $row): ?>
                <?php $getstatus = $this->winner_m->get_statususer($row->account_id); ?>
                <?php if($getstatus && ($getstatus->log_stamps <= date('Y-m-d H:i:s'))): ?>
            <tr>
                <td><?php echo $row->account_displayname ?></td>
                <?php $datas = $this->insight_m->ambil_content_data($row->account_id,$places_id,13); ?> 
                <?php if($datas): ?>
                    <td class="center"><?php echo $datas->content; ?></td>
                <?php else: ?>
                    <td class="center"></td>
                <?php endif; ?>
                    <?php if($getstatus->log_check_out == 2): ?>
                        <?php if($getstatus->log_status_check == 1): ?>
                        <td><small class=" text-muted label label-warning">temporary check out</small></td>
                        <?php else: ?>
                        <td><small class=" text-muted label label-important">permanent check out</small></td>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if($row->data_status == 2): ?>
                            <?php if($getstatus->log_stamps <= $row->datelock): ?>
                                <td><small class=" text-muted label label-success">checked in</small></td>
                            <?php else: ?>
                                <td><small class=" text-muted label label-info">Arrived after lock</small></td>
                            <?php endif; ?>
                        <?php else: ?>
                        <td><small class=" text-muted label label-success">checked in</small></td>
                        <?php endif; ?>
                    <?php endif; ?>
                <td><?php echo $getstatus->log_stamps; ?></td>
            </tr>
                <?php endif; ?>
			<?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>