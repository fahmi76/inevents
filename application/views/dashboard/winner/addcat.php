<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add Winner Category</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Winner Category</h2>
            </div>
            <div class="box-content">
                <form class="form-horizontal" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">          
                            <?php
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } elseif($datalist){
                                $names = $datalist->nama;
                            } else {
                                $names = '';
                            }
                            $id = set_value('id');
                            if ($id) {
                                $ids = $id;
                            } elseif($datalist){
                                $ids = $datalist->id;
                            } else {
                                $ids = '';
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="InputName" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <input class="input-xlarge focused" name="id" autocomplete="off" id="InputId" type="hidden" value="<?php echo $ids; ?>">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  

                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard/winner'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     