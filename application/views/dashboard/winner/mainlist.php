<script type="text/javascript">
    function back(){
        top.location.href="<?=site_url('dashboard/event');?>";
    }
</script>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>
                    <?php if($pages == 1): ?>
                        Winner Category
                    <?php else: ?>
                        Presenters Category
                    <?php endif; ?></h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/winner/home/download/'.$places_id.'/'.$pages; ?>"><i class="halflings-icon download"></i></a>
                    <?php if($pages == 1): ?>
                    <a href="<?php echo base_url() . 'dashboard/winner/add'; ?>"><i class="halflings-icon plus"></i></a>
                    <?php else: ?>
                    <a href="<?php echo base_url() . 'dashboard/winner/add/presenter'; ?>"><i class="halflings-icon plus"></i></a>
                    <?php endif; ?>
                </div>

            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <?php if($pages == 2): ?>
                            <th>Number</th>
                            <?php endif; ?>
                            <th>Status</th>
                            <th>Datetime</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <?php if($pages == 2): ?>
                                <td><?php echo $row->presenter_status; ?></td>
                                <?php endif; ?>
                                <?php if($pages == 1): ?>
                                <?php $getstatus = $this->winner_m->get_statususer($row->account_id); ?>
                                <?php else: ?>
                                <?php $getstatus = $this->winner_m->get_statususer($row->account_id); ?>
                                <?php endif; ?>
                                <?php if($getstatus): ?>
                                    <?php if($getstatus->log_check_out == 2): ?>
                                        <?php if($getstatus->log_status_check == 1): ?>
                                        <td><small class=" text-muted label label-warning">temporary check out</small></td>
                                        <?php else: ?>
                                        <td><small class=" text-muted label label-important">permanent check out</small></td>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if($row->data_status == 2): ?>
                                            <?php if($getstatus->log_stamps <= $row->datelock): ?>
                                                <td><small class=" text-muted label label-success">checked in</small></td>
                                            <?php else: ?>
                                                <td><small class=" text-muted label label-info">Arrived after lock</small></td>
                                            <?php endif; ?>
                                        <?php else: ?>
                                        <td><small class=" text-muted label label-success">checked in</small></td>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <td><?php echo $getstatus->log_stamps; ?></td>
                                <?php else: ?>
                                    <td><small class=" text-muted label label-primary">none</small></td>
                                    <td>0000-00-00 00:00:00</td>
                                <?php endif; ?>
                                <td class="center">
                                <?php if($pages == 1): ?>
                                    <a class="btn btn-info" href="<?php echo base_url(); ?>dashboard/winner/edit/main/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="<?php echo base_url(); ?>dashboard/winner/home/delete/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                <?php else: ?>
                                    <a class="btn btn-info" href="<?php echo base_url(); ?>dashboard/winner/edit/presenter/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="<?php echo base_url(); ?>dashboard/winner/home/deletepresenter/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                <?php endif; ?>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>    
                <p>
                    <button class="btn btn-large" onclick="back()"><i class="halflings-icon white arrow-left"></i> Back</button>
                </p>         
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->