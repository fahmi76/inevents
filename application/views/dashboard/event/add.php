<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add Event</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Event</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">          
                            <?php
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } else {
                                $names = '';
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Event Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
                        <div class="control-group">
                            <?php
                            $sdate = date('m/d/y');
                            ?>   
                            <label class="control-label" for="date01">Start Date</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge datepicker" name="sdate" id="date01" value="<?php echo $sdate?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <?php
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } else {
                                $names = '';
                            }
                            ?>   
                            <label class="control-label" for="date02">End Date</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge datepicker" name="edate" id="date02" value="<?php echo $sdate?>">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="fileInput">Background Landing Page Event</label>
                            <div class="controls">
                                <input class="input-file uniform_on" id="fileInput" name="file" type="file">
                            </div>
                        </div>          
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard/gate'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     