<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List Gate <?php echo $check; ?> <?php if($status == 1){echo 'Check-in';}elseif($status == 2){echo 'Check-out';}else{echo 'Arrive';}?></h2>
            
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/insight/home/delete/'.$from; ?>"><i class="halflings-icon trash"></i></a>
                    <a href="<?php echo base_url() . 'dashboard/insight/home/download/'.$from; ?>"><i class="halflings-icon download"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="example">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Company</th>
                            <th>Empl Class</th>
                            <th>Departement</th>
                            <!--
                            <th>Access</th>
                            -->
                            <th>Status</th>
                            <th>Time</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname ?></td>
                                <td><?php echo $row->account_gw_token ?></td>
                                <td><?php echo $row->account_gw_refresh ?></td>
                                <td><?php echo $row->account_gw_time ?></td>
                                <!--
                                <td class="center"><?php echo $row->nama; ?></td>
                                -->
                                <td>Registrasi</td>
                                <td><?php echo $row->time_upload ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->