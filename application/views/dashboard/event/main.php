<script type="text/javascript">
    function edit(param){
        top.location.href="<?=site_url('dashboard/event/edit/main/');?>/"+param;
    }
    function gate(param){
        top.location.href="<?=site_url('dashboard/gate/home/main/');?>/"+param;
    }
    function access(param){
        top.location.href="<?=site_url('dashboard/visitor/home/main/');?>/"+param;
    }
    function user(param){
        top.location.href="<?=site_url('dashboard/user/home/main/');?>/"+param;
    }
    function winner(param){
        top.location.href="<?=site_url('dashboard/winner/home/main/');?>/"+param;
    }
    function presenter(param){
        top.location.href="<?=site_url('dashboard/winner/home/presenter/');?>/"+param;
    }
    function report(param){
        top.location.href="<?=site_url('dashboard/insight/home/main/');?>/"+param;
    }
    function link(param){
        var url = "<?php echo base_url(); ?>event/main/"+param;
        window.open(url);
    }
    function delete_event(param){
        var r = confirm("Do you want clear this data?");
        if (r == true){
            $.ajax({
              url: '<?=site_url('dashboard/user/home/deleteall');?>',
              type: 'POST',
              dataType: "json",
              data: 'places_id='+param,
              success: function(data) {
                //document.getElementById("status_team_"+id_div).innerHTML = data;
                location.reload();
                //console.log(data);
              },
                error: function(e) {
              }
            });
        }
    }
    function delete_events(param){
        var r = confirm("Do you want clear this checkin/checkout data?");
        if (r == true){
            $.ajax({
              url: '<?=site_url('dashboard/user/home/deletecheck');?>',
              type: 'POST',
              dataType: "json",
              data: 'places_id='+param,
              success: function(data) {
                //document.getElementById("status_team_"+id_div).innerHTML = data;
                location.reload();
                //console.log(data);
              },
                error: function(e) {
              }
            });
        }
    }
</script>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Places</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/event/add'; ?>"><i class="halflings-icon plus"></i></a>
                </div>

            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->places_name ?></td>
                                <td><?php echo $row->places_startdate ?></td>
                                <td><?php echo $row->places_duedate ?></td>
                                <td class="center">
                                    <?php

$a = strtotime($row->places_duedate);
$b = time();
?>
                                <?php if ($row->id == 13 || $row->id == 22 || $row->id == 29 || $row->id == 33 || $row->id == 38): ?>

                                <?php if ($row->id == 38): ?>
                                    <script type="text/javascript">
                                        function settingquis(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/home/survey/');?>/"+param;
                                        }
                                        function linkquis(param){
                                            var url = "<?php echo base_url(); ?>event/quisioner/"+param;
                                            window.open(url);
                                        }
                                        function reportquis(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/insight/main/');?>/"+param;
                                        }
                                        function reportchart(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/insight/grafikall/');?>/"+param;
                                        }
                                        function delete_quisioner(param){
                                            var r = confirm("Do you want clear this data?");
                                            if (r == true){
                                                $.ajax({
                                                  url: '<?=site_url('dashboard/quisioner/home/delete_quisonerdata');?>',
                                                  type: 'POST',
                                                  dataType: "json",
                                                  data: 'places_id='+param,
                                                  success: function(data) {
                                                    //document.getElementById("status_team_"+id_div).innerHTML = data;
                                                    location.reload();
                                                    //console.log(data);
                                                  },
                                                    error: function(e) {
                                                  }
                                                });
                                            }
                                        }
                                    </script>
                                    <p>
                                    <?php if ($a > $b): ?>
                                    <button class="btn btn-small btn-success" onclick="edit('<?php echo $row->id; ?>');"><i class="halflings-icon white edit"></i> Edit</button>
                                    <button class="btn btn-small btn-warning" onclick="settingquis('<?php echo $row->id; ?>');"><i class="halflings-icon white list-alt"></i> Setting Survey</button>
                                    <button class="btn btn-small btn-inverse" onclick="linkquis('<?php echo $row->id; ?>');"><i class="halflings-icon white share"></i> Link Survey</button> </p><p>
                                    <?php endif;?>
                                    <button class="btn btn-small btn-primary" onclick="reportquis('<?php echo $row->id; ?>');"><i class="halflings-icon white file"></i> Report Survey</button>
                                    <button class="btn btn-small btn-danger" onclick="delete_quisioner('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Clear Survey Answer</button>
                                <?php else: ?>
                                    <script type="text/javascript">
                                        function quisioner(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/home/main/');?>/"+param;
                                        }
                                        function settingquis(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/home/setting/');?>/"+param;
                                        }
                                        function linkquis(param){
                                            var url = "<?php echo base_url(); ?>event/quisioner/"+param;
                                            window.open(url);
                                        }
                                        function reportquis(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/insight/main/');?>/"+param;
                                        }
                                        function reportchart(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/insight/grafikall/');?>/"+param;
                                        }
                                        function delete_quisioner(param){
                                            var r = confirm("Do you want clear this data?");
                                            if (r == true){
                                                $.ajax({
                                                  url: '<?=site_url('dashboard/quisioner/home/delete_quisonerdata');?>',
                                                  type: 'POST',
                                                  dataType: "json",
                                                  data: 'places_id='+param,
                                                  success: function(data) {
                                                    //document.getElementById("status_team_"+id_div).innerHTML = data;
                                                    location.reload();
                                                    //console.log(data);
                                                  },
                                                    error: function(e) {
                                                  }
                                                });
                                            }
                                        }
                                    </script>
                                    <p>
                                    <?php if ($a > $b): ?>
                                    <button class="btn btn-small btn-success" onclick="edit('<?php echo $row->id; ?>');"><i class="halflings-icon white edit"></i> Edit</button>
                                    <button class="btn btn-small btn-danger" onclick="delete_quisioner('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Clear Quisioner Answer</button>
                                    <button class="btn btn-small btn-primary" onclick="quisioner('<?php echo $row->id; ?>');"><i class="halflings-icon white list-alt"></i> Question</button>
                                    <button class="btn btn-small btn-warning" onclick="settingquis('<?php echo $row->id; ?>');"><i class="halflings-icon white list-alt"></i> Setting</button>  </p><p>
                                    <button class="btn btn-small btn-inverse" onclick="linkquis('<?php echo $row->id; ?>');"><i class="halflings-icon white share"></i> Link Event</button>
                                    <?php endif;?>
                                    <button class="btn btn-small btn-primary" onclick="reportquis('<?php echo $row->id; ?>');"><i class="halflings-icon white file"></i> Report</button>
                                    <!--
                                    <button class="btn btn-small btn-success" onclick="reportchart('<?php echo $row->id; ?>');"><i class="halflings-icon white file"></i> Chart</button> -->

                                    </p>
                                    <!--
                                <p>
                                    <?php if ($a > $b): ?>
                                    <button class="btn btn-small btn-primary" onclick="gate('<?php echo $row->id; ?>');"><i class="halflings-icon white list-alt"></i> Gate</button>
                                    <button class="btn btn-small" onclick="access('<?php echo $row->id; ?>');"><i class="halflings-icon white star"></i> Access Privileges</button>
                                    <button class="btn btn-small btn-warning" onclick="user('<?php echo $row->id; ?>');"><i class="halflings-icon white user"></i> User</button>
                                </p>
                                <p>
                                    <button class="btn btn-small btn-inverse" onclick="link('<?php echo $row->id; ?>');"><i class="halflings-icon white share"></i> Link Event</button>
                                </p><p>
                                    <button class="btn btn-small btn-danger" onclick="delete_event('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Clear Checkin</button>
                                    <button class="btn btn-small btn-primary" onclick="report('<?php echo $row->id; ?>');"><i class="halflings-icon white file"></i> Report</button>

                                </p>
                                <?php endif;?>
                                -->
                                <?php endif;?>
                                <?php else: ?>
                                <p>
                                    <?php if ($a > $b): ?>
                                    <button class="btn btn-small btn-success" onclick="edit('<?php echo $row->id; ?>');"><i class="halflings-icon white edit"></i> Edit</button>
                                    <?php if ($row->id == 15): ?>
                                    <script type="text/javascript">
                                        function station(param){
                                            top.location.href="<?=site_url('dashboard/station/home/main/');?>/"+param;
                                        }
                                        function photobooth(param){
                                            top.location.href="<?=site_url('dashboard/station/photobooth/main');?>/"+param;
                                        }
                                        function delete_station(param){
                                            var r = confirm("Do you want clear this data?");
                                            if (r == true){
                                                $.ajax({
                                                  url: '<?=site_url('dashboard/station/home/delete');?>',
                                                  type: 'POST',
                                                  dataType: "json",
                                                  data: 'places_id='+param,
                                                  success: function(data) {
                                                    //document.getElementById("status_team_"+id_div).innerHTML = data;
                                                    location.reload();
                                                    //console.log(data);
                                                  },
                                                    error: function(e) {
                                                  }
                                                });
                                            }
                                        }
                                        function delete_photo(param){
                                            var r = confirm("Do you want clear this data?");
                                            if (r == true){
                                                $.ajax({
                                                  url: '<?=site_url('dashboard/station/photobooth/delete');?>',
                                                  type: 'POST',
                                                  dataType: "json",
                                                  data: 'places_id='+param,
                                                  success: function(data) {
                                                    //document.getElementById("status_team_"+id_div).innerHTML = data;
                                                    location.reload();
                                                    //console.log(data);
                                                  },
                                                    error: function(e) {
                                                  }
                                                });
                                            }
                                        }
                                        function delete_event(param){
                                            var r = confirm("Do you want clear this data?");
                                            if (r == true){
                                                $.ajax({
                                                  url: '<?=site_url('dashboard/station/home/deleteall');?>',
                                                  type: 'POST',
                                                  dataType: "json",
                                                  data: 'places_id='+param,
                                                  success: function(data) {
                                                    //document.getElementById("status_team_"+id_div).innerHTML = data;
                                                    location.reload();
                                                    //console.log(data);
                                                  },
                                                    error: function(e) {
                                                  }
                                                });
                                            }
                                        }
                                        function reportsosmed(param){
                                            top.location.href="<?=site_url('dashboard/insight/sosmed/main/');?>/"+param;
                                        }
                                    </script>
                                    <button class="btn btn-small btn-primary" onclick="station('<?php echo $row->id; ?>');"><i class="halflings-icon white list-alt"></i> Update Station</button>
                                    <button class="btn btn-small" onclick="photobooth('<?php echo $row->id; ?>');"><i class="halflings-icon white star"></i> Photobooth</button>
                                    <?php else: ?>
                                    <button class="btn btn-small btn-primary" onclick="gate('<?php echo $row->id; ?>');"><i class="halflings-icon white list-alt"></i> Gate</button>
                                    <button class="btn btn-small" onclick="access('<?php echo $row->id; ?>');"><i class="halflings-icon white star"></i> Access Privileges</button>
                                    <?php if ($row->id == 34): ?>
                                    <script type="text/javascript">
                                        function station(param){
                                            top.location.href="<?=site_url('dashboard/station/home/main/');?>/"+param;
                                        }
                                    </script>
                                    <button class="btn btn-small btn-inverse" onclick="station('<?php echo $row->id; ?>');"><i class="halflings-icon white share"></i> Booth</button>
                                    <?php endif;?>
                                    <?php endif;?>
                                    <button class="btn btn-small btn-warning" onclick="user('<?php echo $row->id; ?>');"><i class="halflings-icon white user"></i> User</button>
                                </p><p>
                                    <?php if ($row->id == 11 || $row->id == 31): ?>
                                    <button class="btn btn-small btn-warning" onclick="winner('<?php echo $row->id; ?>');"><i class="halflings-icon white user"></i> Winner</button>
                                    <button class="btn btn-small btn-success" onclick="presenter('<?php echo $row->id; ?>');"><i class="halflings-icon white edit"></i> Presenter</button>
                                    <?php elseif ($row->id == 32): ?>
                                    <button class="btn btn-small btn-warning" onclick="winner('<?php echo $row->id; ?>');"><i class="halflings-icon white user"></i> Winner</button>
                                    <?php else: ?>
                                        <!--
                                    <script type="text/javascript">
                                        function email(param){
                                            top.location.href="<?=site_url('dashboard/gate/email/main/');?>/"+param;
                                        }
                                    </script>
                                    <button class="btn btn-small btn-warning" onclick="email('<?php echo $row->id; ?>');"><i class="halflings-icon white envelope"></i> Set Email</button>  -->
                                    <?php endif;?>
                                <?php endif;?>
                                    <?php if ($row->id == 28): ?>
                                        <script type="text/javascript">

                                            function reportsollam(param){
                                                top.location.href="<?=site_url('dashboard/insight/sollam/main/');?>/"+param;
                                            }
                                        </script>
                                        <button class="btn btn-small btn-primary" onclick="reportsollam('<?php echo $row->id; ?>');"><i class="halflings-icon white file"></i> Report</button>
                                    <?php else: ?>
                                    <button class="btn btn-small btn-primary" onclick="report('<?php echo $row->id; ?>');"><i class="halflings-icon white file"></i> Report</button>
                                    <?php endif;?>
                                    <?php if ($a > $b): ?>
                                    <button class="btn btn-small btn-inverse" onclick="link('<?php echo $row->id; ?>');"><i class="halflings-icon white share"></i> Link Event</button>
                                </p><p>
                                    <button class="btn btn-small btn-danger" onclick="delete_event('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Clear All Data</button>
                                        <?php if ($row->id == 15): ?>
                                        <button class="btn btn-small btn-danger" onclick="delete_station('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Clear Update Station Data</button>
                                        <button class="btn btn-small btn-danger" onclick="delete_photo('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Clear Photobooth Data</button>
                                        <?php else: ?>
                                        <button class="btn btn-small btn-danger" onclick="delete_events('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Clear Checkin/Checkout Data</button>
                                        <?php endif;?>
                                    <?php endif;?>
                                </p>
                                <?php endif;?>
                                <?php if ($row->id == 37): ?>
                                    <script type="text/javascript">
                                        function settingquis(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/home/survey/');?>/"+param;
                                        }
                                        function linkquis(param){
                                            var url = "<?php echo base_url(); ?>event/quisioner/"+param;
                                            window.open(url);
                                        }
                                        function reportquis(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/insight/main/');?>/"+param;
                                        }
                                        function reportchart(param){
                                            top.location.href="<?=site_url('dashboard/quisioner/insight/grafikall/');?>/"+param;
                                        }
                                        function delete_quisioner(param){
                                            var r = confirm("Do you want clear this data?");
                                            if (r == true){
                                                $.ajax({
                                                  url: '<?=site_url('dashboard/quisioner/home/delete_quisonerdata');?>',
                                                  type: 'POST',
                                                  dataType: "json",
                                                  data: 'places_id='+param,
                                                  success: function(data) {
                                                    //document.getElementById("status_team_"+id_div).innerHTML = data;
                                                    location.reload();
                                                    //console.log(data);
                                                  },
                                                    error: function(e) {
                                                  }
                                                });
                                            }
                                        }
                                    </script>
                                    <p>
                                    <?php if ($a > $b): ?>
                                    <button class="btn btn-small btn-warning" onclick="settingquis('<?php echo $row->id; ?>');"><i class="halflings-icon white list-alt"></i> Setting Survey</button>
                                    <button class="btn btn-small btn-inverse" onclick="linkquis('<?php echo $row->id; ?>');"><i class="halflings-icon white share"></i> Link Survey</button> </p><p>
                                    <?php endif;?>
                                    <button class="btn btn-small btn-primary" onclick="reportquis('<?php echo $row->id; ?>');"><i class="halflings-icon white file"></i> Report Survey</button>
                                    <button class="btn btn-small btn-danger" onclick="delete_quisioner('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Clear Survey Answer</button>
                                <?php endif;?>
                                </td>
                            </tr>

                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->