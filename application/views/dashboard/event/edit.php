<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURLs(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blahs').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Edit Event</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Event</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
                        <div class="control-group <?php if (form_error('name')): ?> error<?php endif;?>">
                            <?php
$name = set_value('name');
if ($name) {
	$names = $name;
} elseif (!$datauser) {
	$names = '';
} else {
	$names = $datauser->places_name;
}
?>
                            <label class="control-label" for="focusedInput">Event Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <?php
$sdate = date('m/d/y');
$sdates = set_value('sdate');
if ($sdates) {
	$sdates1 = $sdates;
} elseif (!$datauser) {
	$sdates1 = $sdate;
} else {
	$sdates1 = date("m/d/y", strtotime($datauser->places_startdate));
}
?>
                            <label class="control-label" for="date01">Start Date</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge datepicker" name="sdate" id="date01" value="<?php echo $sdates1; ?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <?php
$edates = set_value('edate');
if ($edates) {
	$edates1 = $edates;
} elseif (!$datauser) {
	$edates1 = $sdate;
} else {
	$edates1 = date("m/d/y", strtotime($datauser->places_duedate));
}
?>
                            <label class="control-label" for="date02">End Date</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge datepicker" name="edate" id="date02" value="<?php echo $edates1 ?>">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="fileInput">Background Landing Page Event</label>
                            <div class="controls">
                                <?php if ($datauser->places_backgroud): ?>
                                <img id="blah" src="<?php echo base_url(); ?><?php echo $datauser->places_backgroud; ?>" style="width: 200px;">
                                <?php else: ?>
                                <img id="blah" src="http://placehold.it/200x200" alt="your image" style="width: 200px;" />
                                <?php endif;?>
                                <input class="input-file uniform_on" onchange="readURL(this);" id="fileInput" name="file" type="file">
                            </div>
                        </div>
                        <?php if ($places_id == 34): ?>
                            <fieldset>
                                <div class="control-group <?php if (validation_errors()): ?> error<?php endif;?>">
                                    <label class="control-label" for="focusedInput">Sender Name</label>
                                    <?php
$email = set_value('sender_name');
if ($email) {
	$emails = $email;
} elseif (!$datauser) {
	$emails = '';
} else {
	$emails = $datauser->places_email;
}
?>
                                    <div class="controls">
                                        <input class="input-xlarge focused" name="sender_name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $emails; ?>" placeholder="Sender Name">
                                        <?php echo form_error('sender_name', '<span class="help-inline">', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group <?php if (validation_errors()): ?> error<?php endif;?>">
                                    <label class="control-label" for="focusedInput">Subject</label>
                                    <?php
$subject = set_value('subject');
if ($subject) {
	$subjects = $subject;
} elseif (!$datauser) {
	$subjects = '';
} else {
	$subjects = $datauser->places_subject_email;
}
?>
                                    <div class="controls">
                                        <input class="input-xlarge focused" name="subject" autocomplete="off" id="focusedInput" type="text" value="<?php echo $subjects; ?>" placeholder="Subject">
                                        <?php echo form_error('subject', '<span class="help-inline">', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="control-group <?php if (validation_errors()): ?> error<?php endif;?>">
                                    <label class="control-label" for="focusedInput">Content</label>
                                    <?php
$content = set_value('content');
if ($content) {
	$contents = $content;
} elseif (!$datauser) {
	$contents = '';
} else {
	$contents = $datauser->places_custom_email;
}
?>
                                    <div class="controls">
                                        <textarea name="content" class="cleditor" id="textarea2" rows="5">
                                            <?php echo $contents; ?>
                                        </textarea>
                                        <?php echo form_error('content', '<span class="help-inline">', '</span>'); ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <?php
$attachemail = json_decode($datauser->places_email_attach);
?>
<?php foreach ($attachemail as $row): ?>
<div class="controls"><?php echo $row; ?> <a href="<?php echo base_url(); ?>dashboard/event/edit/deletefile/34/<?php echo $row; ?>" class="btn btn-danger">Delete</a></div><br></label>

<?php endforeach;?>
                                    <label class="control-label">File Attachment (single or multiple file, max: 10MB)</label>
                                    <div class="controls">
                                        <input type="file" name="attach[]" id="filesToUpload" onChange="makeFileList();" multiple>
                                    </div>
                                    <div id="showsize"></div>
                                </div>
                                <ul id="fileList"><li>No Files Selected</li></ul>

                                <script type="text/javascript">
                                    function makeFileList() {
                                        var input = document.getElementById("filesToUpload");
                                        var ul = document.getElementById("fileList");
                                        while (ul.hasChildNodes()) {
                                            ul.removeChild(ul.firstChild);
                                        }
                                        var size = 0;
                                        for (var i = 0; i < input.files.length; i++) {
                                            var li = document.createElement("li");
                                            size += input.files[i].size;
                                            li.innerHTML = input.files[i].name + ' - ('+formatBytes(input.files[i].size)+')';
                                            ul.appendChild(li);
                                            console.log(formatBytes(size));
                                            document.getElementById("showsize").innerHTML= "Total File Size : "+formatBytes(size);
                                        }
                                        if(size > 10000000){
                                            alert('limit file 10 MB');
                                            var input = $("#filesToUpload");
                                            input.replaceWith(input.val('').clone(true));
                                            while (ul.hasChildNodes()) {
                                                ul.removeChild(ul.firstChild);
                                            }
                                            var li = document.createElement("li");
                                            li.innerHTML = 'No Files Selected';
                                            ul.appendChild(li);
                                            document.getElementById("showsize").innerHTML= "";
                                        }
                                        if(!ul.hasChildNodes()) {
                                            var li = document.createElement("li");
                                            li.innerHTML = 'No Files Selected';
                                            ul.appendChild(li);
                                        }
                                    }
                                    function formatBytes(bytes,decimals) {
                                       if(bytes == 0) return '0 Bytes';
                                       var k = 1000,
                                           dm = decimals || 2,
                                           sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
                                           i = Math.floor(Math.log(bytes) / Math.log(k));
                                       return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
                                    }
                                </script>
                            </fieldset>
                        <?php endif;?>
                        <?php if ($places_id == 15): ?>
                                <input type="hidden" class="input-xlarge datepicker" name="id" value="<?php echo $places_id ?>">
                            <div class="control-group <?php if (form_error('image')): ?> error<?php endif;?>">
                                <label class="control-label" for="fileInput">Thumbnail</label>
                                <div class="controls">
                                    <?php if ($datauser->places_avatar != ''): ?>
                                    <img id="blahs" src="<?php echo base_url(); ?><?php echo $datauser->places_avatar; ?>" alt="your image" style="width: 200px;" />
                                    <?php else: ?>
                                    <img id="blahs" src="http://placehold.it/200x200" alt="your image" style="width: 200px;" />
                                    <?php endif;?>
                                    <input class="input-file uniform_on" onchange="readURLs(this);" id="fileInput" name="image" type="file">
                                    <?php echo form_error('image', '<span class="help-inline">', '</span>'); ?>
                                </div>
                            </div>
                            <div class="control-group <?php if (form_error('share')): ?> error<?php endif;?>">
                                <?php
$share = set_value('share');
if ($share) {
	$shares = $share;
} elseif (!$datauser) {
	$shares = '';
} else {
	$shares = $datauser->places_type;
}
?>
                                <label class="control-label" for="focusedInput">Share</label>
                                <div class="controls">
                                    <label class="radio">
                                        <input type="radio" name="share" id="optionsRadios1" value="1" <?php echo set_radio('share', '1', TRUE); ?>>
                                        Share Text
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="share" id="optionsRadios1" value="2" <?php echo set_radio('share', '2'); ?>>
                                        Share Image
                                    </label>
                                    <?php echo form_error('share', '<span class="help-inline">', '</span>'); ?>
                                </div>
                            </div>
                            <div class="control-group <?php if (form_error('facebook')): ?> error<?php endif;?>">
                                <?php
$facebook = set_value('facebook');
if ($facebook) {
	$facebooks = $facebook;
} elseif (!$datauser) {
	$facebooks = '';
} else {
	$facebooks = $datauser->places_cstatus_fb;
}
?>
                                <label class="control-label" for="focusedInput">Facebook</label>
                                <div class="controls">
                                    <input class="input-xlarge focused" name="facebook" autocomplete="off" id="focusedInput" type="text" value="<?php echo $facebooks; ?>" placeholder="Facebook Message">
                                    <?php echo form_error('facebook', '<span class="help-inline">', '</span>'); ?>
                                </div>
                            </div>
                            <div class="control-group <?php if (form_error('twitter')): ?> error<?php endif;?>">
                                <?php
$twitter = set_value('twitter');
if ($twitter) {
	$twitters = $twitter;
} elseif (!$datauser) {
	$twitters = '';
} else {
	$twitters = $datauser->places_cstatus_tw;
}
?>
                                <label class="control-label" for="focusedInput">Twitter</label>
                                <div class="controls">
                                    <input class="input-xlarge focused" name="twitter" autocomplete="off" id="focusedInput" type="text" value="<?php echo $twitters; ?>" placeholder="Twitter Message">
                                    <?php echo form_error('twitter', '<span class="help-inline">', '</span>'); ?>
                                </div>
                            </div>
                        <?php endif;?>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->