<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Edit Photobooth</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Photobooth</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/gate'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal"  onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset> 
                        <div class="control-group <?php if (form_error('name')) : ?> error<?php endif; ?>">          
                            <?php
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } elseif (!$datauser) {
                                $names = '';
                            } else {
                                $names = $datauser->places_name;
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
                        <div class="control-group <?php if (form_error('image')) : ?> error<?php endif; ?>"> 
                            <label class="control-label" for="fileInput">Frame</label>
                            <div class="controls">
                                <?php if($datauser->places_frame != ''): ?>
                                <img id="blah" src="<?php echo base_url(); ?><?php echo $datauser->places_frame; ?>" alt="your image" style="width: 200px;" />
                                <?php else: ?>
                                <img id="blah" src="http://placehold.it/200x200" alt="your image" style="width: 200px;"/>
                                <?php endif;  ?>
                                <input class="input-file uniform_on" onchange="readURL(this);" id="fileInput" name="image" type="file">
                                <?php echo form_error('image', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div> 
                        <div class="control-group <?php if (form_error('album')) : ?> error<?php endif; ?>">          
                            <?php
                            $album = set_value('album');
                            if ($album) {
                                $albums = $album;
                            } elseif (!$datauser) {
                                $albums = '';
                            } else {
                                $albums = $datauser->places_album;
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Facebook Album</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="album" autocomplete="off" id="focusedInput" type="text" value="<?php echo $albums; ?>" placeholder="Facebook Album">
                                <?php echo form_error('album', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div> 
                        <div class="control-group <?php if (form_error('facebook')) : ?> error<?php endif; ?>">          
                            <?php
                            $facebook = set_value('facebook');
                            if ($facebook) {
                                $facebooks = $facebook;
                            } elseif (!$datauser) {
                                $facebooks = '';
                            } else {
                                $facebooks = $datauser->places_cstatus_fb;
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Facebook</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="facebook" autocomplete="off" id="focusedInput" type="text" value="<?php echo $facebooks; ?>" placeholder="Facebook Message">
                                <?php echo form_error('facebook', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
                        <div class="control-group <?php if (form_error('twitter')) : ?> error<?php endif; ?>">          
                            <?php
                            $twitter = set_value('twitter');
                            if ($twitter) {
                                $twitters = $twitter;
                            } elseif (!$datauser) {
                                $twitters = '';
                            } else {
                                $twitters = $datauser->places_cstatus_tw;
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Twitter</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="twitter" autocomplete="off" id="focusedInput" type="text" value="<?php echo $twitters; ?>" placeholder="Twitter Message">
                                <?php echo form_error('twitter', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div> 
                        
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard/station/photobooth/main/'.$places_id; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     