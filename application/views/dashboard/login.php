<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('dashboard/header'); ?>

        <style type="text/css">
            body { background: url(<?php echo base_url(); ?>assets/dashboard/img/bg-login.jpg) !important; }
        </style>



    </head>

    <body>
        <div class="container-fluid-full">
            <div class="row-fluid">

                <div class="row-fluid">
                    <div class="login-box">
                        <div class="icons">
                            <a href="wooz.in"><i class="halflings-icon home"></i></a>
                        </div>
                        
                        <?php if (validation_errors()) : ?>
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Oh snap!</strong> <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
                        </div>
                        <?php endif; ?>
                        <h2>Login to your account</h2>
                        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
                            <fieldset>

                                <div class="input-prepend" title="Username">
                                    <span class="add-on"><i class="halflings-icon user"></i></span>
                                    <input class="input-large span10" name="email_login" id="username" type="text" autocomplete="off" placeholder="type email"/>
                                </div>
                                <div class="clearfix"></div>

                                <div class="input-prepend" title="Password">
                                    <span class="add-on"><i class="halflings-icon lock"></i></span>
                                    <input class="input-large span10" name="password_login" id="password" type="password" placeholder="type password"/>
                                </div>
                                <div class="clearfix"></div>

                                <div class="button-login">	
                                    <button type="submit" class="btn btn-primary">Login</button>
                                </div>
                                <div class="clearfix"></div>
                        </form>
                        <hr>
                        <h3>Forgot Password?</h3>
                        <p>
                            No problem, <a href="<?php echo base_url() . 'dashboard' ?>">click here</a> to get a new password.
                        </p>	
                    </div><!--/span-->
                </div><!--/row-->


            </div><!--/.fluid-container-->

        </div><!--/fluid-row-->

        <!-- start: JavaScript-->
        <?php echo $this->load->view('dashboard/javascript'); ?>
        <!-- end: JavaScript-->

    </body>
</html>
