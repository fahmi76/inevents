<script type="text/javascript">
    function edit(param,param2){
        top.location.href="<?=site_url('dashboard/visitor/edit/main/');?>/"+param+"/"+param2;
    }
    function back(){
        top.location.href="<?=site_url('dashboard/event');?>";
    }
    function delete_event(param){
        var r = confirm("Do you want delete this data?");
        if (r == true){
            $.ajax({
              url: '<?=site_url('dashboard/visitor/home/delete_places/');?>',
              type: 'POST',
              dataType: "json",
              data: 'places_id='+param,
              success: function(data) {
                //document.getElementById("status_team_"+id_div).innerHTML = data;
                location.reload();
                //console.log(data);
              },
                error: function(e) {
              }
            });         
        }       
    }
</script>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url() . 'dashboard/event'; ?>">Event</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Access Privileges Event <?php echo $places->places_name;?></h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/visitor/add/main/'.$places_id; ?>"><i class="halflings-icon plus"></i></a>
                </div>

            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Gate</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->nama ?></td>
                                <td>
                                    <?php 
                                        $gate = json_decode($row->gate_id); 
                                        $i = 0;
                                        foreach($gate as $data_gate){
                                            $list = $this->visitor_m->get_gate($data_gate);
                                            echo $list->nama;
                                            $i++;
                                            if($i < count($gate)){
                                                echo ',';
                                            } 
                                        }
                                    ?>
                                </td>
                                <td class="center">    
                                    <button class="btn btn-small btn-success" onclick="edit('<?php echo $row->id; ?>','<?php echo $places_id; ?>');"><i class="halflings-icon white edit"></i> Edit</button> 
                                    <button class="btn btn-small btn-danger" onclick="delete_event('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Delete</button> 
                               
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>       
                <p>
                    <button class="btn btn-large" onclick="back()"><i class="halflings-icon white arrow-left"></i> Back</button>
                </p>        
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->