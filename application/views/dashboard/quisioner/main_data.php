
<table class="table table-striped table-bordered bootstrap-datatable datatable">
    <thead>
        <tr>
            <th>No</th>
            <th></th>
            <th>Name Quisioner</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php $x = 1;foreach ($data as $row): ?>
            <tr>
                <td><?php echo $x; ?></td>
                <td></td>
                <td><?php echo $row->question ?>
                </td>
                <td class="center">
                    <button class="btn btn-small btn-primary" onclick="add_person('<?php echo $row->id; ?>','<?php echo $places_id; ?>');"><i class="halflings-icon white edit"></i> Answer</button>
                    <button class="btn btn-small btn-success" onclick="edit_question('<?php echo $row->id; ?>','<?php echo $places_id; ?>');"><i class="halflings-icon white edit"></i> Edit</button>
                    <button class="btn btn-small btn-danger" onclick="delete_event('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Delete</button>

                </td>
            </tr>
            <?php $answer = $this->quisioner_m->list_answer($row->id);?>
            <?php $y = 1;if ($answer): ?>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Answer</th>
                    <th></th>
                </tr>
                <?php foreach ($answer as $rowanswer): ?>
                <tr>
                    <td></td>
                    <td><?php echo $x . '-' . $y; ?></td>
                    <td><?php echo $rowanswer->answer; ?> <?php if ($rowanswer->data_status == 1) {echo '(Correct Answer)';} else {echo '(Wrong Answer)';}?></td>
                    <td class="center">
                        <button class="btn btn-small btn-success" onclick="edit_person('<?php echo $rowanswer->id; ?>','<?php echo $places_id; ?>');"><i class="halflings-icon white edit"></i> Edit</button>
                        <button class="btn btn-small btn-danger" onclick="delete_person('<?php echo $rowanswer->id; ?>')"><i class="icon-remove icon-white"></i> Delete</button>

                    </td>
                </tr>
                <?php $y++;endforeach;?>
            <?php endif;?>
        <?php $x++;endforeach;?>
    </tbody>
</table>