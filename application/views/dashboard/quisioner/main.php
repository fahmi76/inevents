<script type="text/javascript">
    $(document).ready(function () {
        load_table();
    });

    function load_table(){
        $('#result').load("<?php echo site_url('dashboard/quisioner/home/load_data/' . $places_id . '/' . $survey->id) ?>");
    }
    function edit(param,param2){
        top.location.href="<?=site_url('dashboard/quisioner/edit/main/');?>/"+param+"/"+param2;
    }

    function add_question(places_id){
        save_method = 'add';
        $('#form1')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form1').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Question'); // Set Title to Bootstrap modal title
        $('[name="id"]').val(places_id);
    }

    function edit_question(id)
    {
        save_method = 'update';
        $('#form1')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('dashboard/quisioner/home/edit_question/') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {

                $('[name="id"]').val(data.id);
                $('[name="name"]').val(data.name);
                $('#modal_form1').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function save_question()
    {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('dashboard/quisioner/home/question_add/') ?>";
        } else {
            url = "<?php echo site_url('dashboard/quisioner/home/question_update/') ?>";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form1').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form1').modal('hide');
                    load_table();
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable

            }
        });
    }

    function add_person(id)
    {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Answer'); // Set Title to Bootstrap modal title
        $('[name="id"]').val(id);
    }

    function edit_person(id)
    {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('dashboard/quisioner/home/edit_answer/') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {

                $('[name="id"]').val(data.id);
                $('[name="answer"]').val(data.answer);
                if(data.data_status == 1){
                    $( "#statuscorrect").prop('checked', true);
                }
                else
                {
                    $( "#statuscorrect").prop('checked', false);
                }
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Answer'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function save()
    {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('dashboard/quisioner/home/answer_add/') ?>";
        } else {
            url = "<?php echo site_url('dashboard/quisioner/home/answer_update/') ?>";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    load_table();
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable

            }
        });
    }

    function back(){
        top.location.href="<?=site_url('dashboard/quisioner/home/survey/' . $places_id);?>";
    }
    function delete_event(param){
        var r = confirm("Do you want delete this data?");
        if (r == true){
            $.ajax({
              url: '<?=site_url('dashboard/quisioner/home/delete_places/');?>',
              type: 'POST',
              dataType: "json",
              data: 'places_id='+param,
              success: function(data) {
                //document.getElementById("status_team_"+id_div).innerHTML = data;
                load_table();
                //console.log(data);
              },
                error: function(e) {
              }
            });
        }
    }
    function delete_person(param){
        var r = confirm("Do you want delete this data?");
        if (r == true){
            $.ajax({
              url: '<?=site_url('dashboard/quisioner/home/delete_answer/');?>',
              type: 'POST',
              dataType: "json",
              data: 'places_id='+param,
              success: function(data) {
                //document.getElementById("status_team_"+id_div).innerHTML = data;
                load_table();
                //console.log(data);
              },
                error: function(e) {
              }
            });
        }
    }
</script>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url() . 'dashboard/event'; ?>">Event</a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Quisioner "<?php echo $survey->title; ?>" Event <?php echo $places->places_name; ?></h2>
                <div class="box-icon">
                </div>

            </div>
            <div class="box-content">
                <button class="btn btn-small btn-primary" onclick="add_question('<?php echo $places_id; ?>');"><i class="halflings-icon plus"></i> Question</button> <!--
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th></th>
                            <th>Name Quisioner</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $x = 1;foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $x; ?></td>
                                <td></td>
                                <td><?php echo $row->question ?>
                                </td>
                                <td class="center">
                                    <button class="btn btn-small btn-primary" onclick="add_person('<?php echo $row->id; ?>','<?php echo $places_id; ?>');"><i class="halflings-icon white edit"></i> Answer</button>
                                    <button class="btn btn-small btn-success" onclick="edit('<?php echo $row->id; ?>','<?php echo $places_id; ?>');"><i class="halflings-icon white edit"></i> Edit</button>
                                    <button class="btn btn-small btn-danger" onclick="delete_event('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Delete</button>

                                </td>
                            </tr>
                            <?php $answer = $this->quisioner_m->list_answer($row->id);?>
                            <?php $y = 1;if ($answer): ?>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Answer</th>
                                    <th></th>
                                </tr>
                                <?php foreach ($answer as $rowanswer): ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $x . '-' . $y; ?></td>
                                    <td><?php echo $rowanswer->answer; ?></td>
                                    <td class="center">
                                        <button class="btn btn-small btn-success" onclick="edit('<?php echo $row->id; ?>','<?php echo $places_id; ?>');"><i class="halflings-icon white edit"></i> Edit</button>
                                        <button class="btn btn-small btn-danger" onclick="delete_event('<?php echo $row->id; ?>')"><i class="icon-remove icon-white"></i> Delete</button>

                                    </td>
                                </tr>
                                <?php $y++;endforeach;?>
                            <?php endif;?>
                        <?php $x++;endforeach;?>
                    </tbody>
                </table>  -->
                <div id="result"></div>
                <p>
                    <button class="btn btn-large" onclick="back()"><i class="halflings-icon white arrow-left"></i> Back</button>
                </p>
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/js/bootstrap.min.js"></script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Answer Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                            <label class="control-label col-md-3">Answer</label><br>
                        <div class="form-group">
                            <div class="col-md-9">
                                <input name="answer" placeholder="Answer" class="form-control" type="text" style="width: 456px;">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="checkbox">
                          <label><input type="checkbox" id="statuscorrect" name="status" value="1">Set to Correct Answer</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Question Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form1" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-9">
                                <label class="control-label col-md-3">Question</label>
                                <input name="survey" type="hidden" value="<?php echo $survey->id; ?>">
                                <input name="name" placeholder="Question" class="form-control" type="text" style="width: 456px;">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_question()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->