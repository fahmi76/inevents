<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List All Data</h2>

                <div class="box-icon">
                <!--
                    <a href="<?php echo base_url() . 'dashboard/quisioner/insight/download/' . $id . '/' . $places_id; ?>"><i class="halflings-icon download"></i></a>
                    -->
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="example">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Location</th>
                            <th>Question</th>
                            <th>Answer</th>
                            <th>Time</th>
                            <th>Set Winner</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname; ?></td>
                                <td><?php echo $row->account_email; ?></td>
                                <td><?php echo $row->account_phone; ?></td>
                                <td><?php echo $row->account_location; ?></td>
                                <td><?php echo $row->question; ?></td>
                                <td><?php echo $row->answer; ?></td>
                                <td><?php echo $row->date_add; ?></td>
                                <td>
                                    <?php if ($row->account_status == 1): ?>
                                    <a href="<?php echo base_url(); ?>dashboard/quisioner/insight/userwin/<?php echo $status; ?>/<?php echo $id; ?>/<?php echo $places_id; ?>/<?php echo $row->id; ?>" class="btn btn-primary">Set Winner</a>
                                    <?php else: ?>
                                        Already Winner
                                    <?php endif;?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->