<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Edit Setting</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Setting</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/quisioner/home/main/' . $places_id; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal"  onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
                        <div class="control-group <?php if (validation_errors()): ?> error<?php endif;?>">
                            <label class="control-label" for="focusedInput">Title Survey</label>
                            <?php
$first = set_value('first');
if ($first) {
	$firsts = $first;
} elseif (!$datauser) {
	$firsts = '';
} else {
	$firsts = $datauser->title;
}
?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="first" autocomplete="off" id="focusedInput" type="text" value="<?php echo $firsts; ?>" placeholder="Button">
                                <?php echo form_error('first', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="control-group <?php if (validation_errors()): ?> error<?php endif;?>">
                            <label class="control-label" for="focusedInput">Title Second Page</label>
                            <?php
$second = set_value('second');
if ($second) {
	$seconds = $second;
} elseif (!$datauser) {
	$seconds = '';
} else {
	$seconds = $datauser->question_order;
}
?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="second" autocomplete="off" id="focusedInput" type="text" value="<?php echo $seconds; ?>" placeholder="Button">
                                <?php echo form_error('second', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="control-group <?php if (validation_errors()): ?> error<?php endif;?>">
                            <label class="control-label" for="focusedInput">Title Finish Page</label>
                            <?php
$third = set_value('third');
if ($third) {
	$thirds = $third;
} elseif (!$datauser) {
	$thirds = '';
} else {
	$thirds = $datauser->question_finish;
}
?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="third" autocomplete="off" id="focusedInput" type="text" value="<?php echo $thirds; ?>" placeholder="Button">
                                <?php echo form_error('third', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="control-group <?php if (validation_errors()): ?> error<?php endif;?>">
                            <label class="control-label" for="focusedInput">Message Finish Page</label>
                            <?php
$fourth = set_value('fourth');
if ($fourth) {
	$fourths = $fourth;
} elseif (!$datauser) {
	$fourths = '';
} else {
	$fourths = $datauser->question_message;
}
?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="fourth" autocomplete="off" id="focusedInput" type="text" value="<?php echo $fourths; ?>" placeholder="Button">
                                <?php echo form_error('fourth', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="control-group <?php if (validation_errors()): ?> error<?php endif;?>">
                            <label class="control-label" for="focusedInput">Type Questioner</label>
                            <?php
$five = set_value('five');
if ($five) {
	$fives = $five;
} elseif (!$datauser) {
	$fives = 0;
} else {
	$fives = $datauser->status;
}
?>
                            <div class="controls">
                                <label class="radio-inline"><input type="radio" name="five" value="0" <?php if ($fives == 0): ?> checked <?php endif;?> >&nbsp; Grouping</label>
                                <label class="radio-inline"><input type="radio" name="five" value="1" <?php if ($fives == 1): ?> checked <?php endif;?>>&nbsp; Stand Alone</label>
                                <?php echo form_error('five', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard/quisioner/home/survey/' . $places_id; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->