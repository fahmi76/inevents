<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
if($question){
    $titles = 'list-'.str_replace(' ', '_', $question->question);   
}else{
    $titles = 'list-all';
}
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=".$titles.".xls");
 
?>
<html>
<head>
    <style>
        body {
            font-family: Arial;
        }
        table {
            border-collapse: collapse;
        }
        th {
            background-color: #cccccc;
        }
        th, td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
    <?php if($question): ?>
    <h2>List Answer for '<?php echo $question->question; ?>'</h2>
    <?php else: ?>
    <h2>List All</h2>
    <?php endif; ?>
    <table>
        <thead>
            <tr>
                <th>Question</th>
                <th>Answer</th>
                <th>Time</th>
            </tr>
        </thead>   
        <tbody>
            <?php foreach ($data as $row): ?>
                <tr>
                    <td><?php echo $row->question; ?></td>
                    <td><?php echo $row->answer; ?></td>
                    <td><?php echo $row->date_add; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>