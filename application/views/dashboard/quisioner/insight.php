<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>

    <div class="row-fluid">
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $count_all->total_account; ?></div>
            <div class="title">List All</div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/quisioner/insight/listall/<?php echo $places_id; ?>"> read full report</a>
            </div>
        </div>
        <?php $x = 1;foreach ($checkin as $row): ?>
        <div class="span3 statbox blue" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $row['total']->total_account; ?></div>
            <div class="title"><?php
$y = substr($row['nama'], 0, 20);
echo $y;?></div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/quisioner/insight/listall/<?php echo $places_id; ?>/<?php echo $row['id']; ?>"> read full report</a>
            </div>
        </div>
    	<?php $x++;endforeach;?>
    </div>
    <?php if ($places_id == 38): ?>
    <div class="row-fluid">
        <?php $x = 1;foreach ($checkin as $row): ?>
        <div class="span3 statbox green" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $this->quisioner_m->count_user_answer($row['id'], 1)->total; ?></div>
            <div class="title"><?php
$y = substr($row['nama'], 0, 20);
echo $y;?></div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/quisioner/insight/user/1/<?php echo $places_id; ?>/<?php echo $row['id']; ?>"> read User Correct</a>
            </div>
        </div>
    	<?php $x++;endforeach;?>
    </div>
    <div class="row-fluid">
        <?php $x = 1;foreach ($checkin as $row): ?>
        <div class="span3 statbox orange" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $this->quisioner_m->count_user_answer($row['id'], 2)->total; ?></div>
            <div class="title"><?php
$y = substr($row['nama'], 0, 20);
echo $y;?></div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/quisioner/insight/user/2/<?php echo $places_id; ?>/<?php echo $row['id']; ?>"> read All User Answer</a>
            </div>
        </div>
    	<?php $x++;endforeach;?>
    </div>
    <?php endif;?>
    <?php $x = 1;foreach ($grafikcheckin as $rowgrafikin): ?>
	<script type="text/javascript">
    $(document).ready(function() {
		var data<?php echo $x; ?> = [
			<?php foreach ($rowgrafikin['data'] as $row): ?>
				{ label: "<?php echo $row['date']; ?> (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach;?>
		];
		if($("#piechart<?php echo $x; ?>").length)
		{
			$.plot($("#piechart<?php echo $x; ?>"), data<?php echo $x; ?>,
			{
				series: {
					pie: {
				      show: true,
				      radius: 150,
				      label: {
				        show: true,
				        radius: 0.5, // place in middle of pie slice
				        formatter: function(label, series){
				          var number = series.data[0][1]; // this is the y value of the first point
				          return ('&nbsp;<b>' + number + '</b> users'); // custom format
				        }
				      }
				    }
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});

			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechart<?php echo $x; ?>").bind("plothover", pieHover);
		}
	});
	</script>

		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span><?php echo $rowgrafikin['nama']; ?></h2>
			</div>
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/quisioner/insight/downloadgraph/<?php echo $x; ?>" id="myForm<?php echo $x; ?>">
				<input type="hidden" name="img_val<?php echo $x; ?>" id="img_val<?php echo $x; ?>" value="" />
			</form>
				<div id="target<?php echo $x; ?>">
				<h2><span class="break"></span><?php echo $rowgrafikin['nama']; ?></h2>
			<div class="box-content">
				<div id="piechart<?php echo $x; ?>" style="height:300px"></div>
			</div>
				</div>
				<input type="submit" value="Download" onclick="capture();" />
			<script type="text/javascript">
				function capture() {
					html2canvas( $('#target<?php echo $x; ?>'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_val<?php echo $x; ?>').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myForm<?php echo $x; ?>").submit();
						}
					});
				}
			</script>
		</div>
	<?php $x++;endforeach;?>

</div>

<!--/.fluid-container-->

<!-- end: Content -->