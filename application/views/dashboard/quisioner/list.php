<div id="content" class="span10">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>">
                <?php echo $page; ?>
            </a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">
                <?php echo $title; ?>
            </a>
        </li>
    </ul>
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List Quisioner Setting</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/quisioner/home/add/' . $places_id; ?>"><i class="halflings-icon plus"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="example">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $row): ?>
                        <tr>
                            <td>
                                <?php echo $row->title ?>
                            </td>
                            <td class="center">
                                <a class="btn btn-success" href="<?php echo base_url(); ?>dashboard/quisioner/home/main/<?php echo $places_id; ?>/<?php echo $row->id; ?>">
                                    <i class="halflings-icon white question-sign"></i> Quisioner
                                </a>
                                <a class="btn btn-info" href="<?php echo base_url(); ?>dashboard/quisioner/home/setting/<?php echo $places_id; ?>/<?php echo $row->id; ?>">
                                    <i class="halflings-icon white edit"></i> Edit
                                </a>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>dashboard/quisioner/home/delete/<?php echo $places_id; ?>/<?php echo $row->id; ?>">
                                    <i class="halflings-icon white trash"></i> Delete
                                </a>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->
    <p>
        <button class="btn btn-large" onclick="back()"><i class="halflings-icon white arrow-left"></i> Back</button>
    </p>
</div>
<!--/.fluid-container-->
<script type="text/javascript">

    function back(){
        top.location.href="<?=site_url('dashboard');?>";
    }
</script>