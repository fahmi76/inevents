<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=".$title.".xls");
 
?>

<table>
    <thead>
        <tr>
            <th>Username</th>
            <?php foreach($tabel as $rowtabel): ?>
            <th><?php echo $rowtabel->field_name; ?></th>
            <?php endforeach; ?>
        </tr>
    </thead>   
    <tbody>
        <?php foreach ($data as $row): ?>
            <tr>
                <td><?php echo $row->account_displayname ?></td>
                <?php foreach($tabel as $rowtabel): ?>
                <?php $datas = $this->user_m->ambil_content_data($row->id,$places_id,$rowtabel->id); ?> 
                <?php if($datas): ?>
                    <td class="center"><?php echo $datas->content; ?></td>
                <?php else: ?>
                    <td class="center"></td>
                <?php endif; ?> 
                <?php endforeach; ?>
            </tr>

        <?php endforeach; ?>
    </tbody>
</table>            