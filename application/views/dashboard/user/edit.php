<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Edit User</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit User</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal"  onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>

                        <div class="control-group <?php if (form_error('name')) : ?> error<?php endif; ?>">          
                            <?php 
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } elseif (!$datauser) {
                                $names = '';
                            } else {
                                $names = $datauser->account_displayname;
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>   
                        <?php foreach($form as $rowform): ?>
                            <?php switch ($rowform->field_type) {
                                case 'radio': ?>      
                                <?php $datafield = $this->user_m->ambil_content_data($datauser->id,$places_id,$rowform->id);
                                $name = set_value($rowform->field_nicename);
                                if ($name) {
                                    $names = $name;
                                } elseif (!$datafield) {
                                    $names = '';
                                } else {
                                    $names = $datafield->content;
                                }
                                ?>                  
                                <div class="control-group <?php if (form_error('visitor')) : ?> error<?php endif; ?>">
                                    <label class="control-label"><?php echo $rowform->field_name; ?></label>
                                    <div class="controls">
                                        <?php $fieldrow = json_decode($rowform->field_add_data); foreach($fieldrow as $row): ?>
                                            <label class="radio">
                                                <input type="radio" name="<?php echo $rowform->field_nicename; ?>" id="optionsRadios1" value="<?php echo $row; ?>" <?php if ($names == $row) : ?>checked="checked"<?php endif; ?>>
                                                <?php echo $row; ?>
                                            </label>
                                            <div style="clear:both"></div>
                                        <?php endforeach ; ?>
                                        <?php echo form_error($rowform->field_nicename, '<span class="help-inline">', '</span>'); ?>
                                    </div>
                                </div> 
                                <?php
                                    break;
                                case 'file': ?>
                                    <div class="control-group"> 
                                        <?php $datafield = $this->user_m->ambil_content_data($datauser->id,$places_id,$rowform->id);
                                            $name = set_value($rowform->field_nicename);
                                            if ($name) {
                                                $names = $name;
                                            } elseif (!$datafield) {
                                                $names = '';
                                            } else {
                                                $names = $datafield->content;
                                            }
                                        ?>      
                                        <label class="control-label" for="fileInput"><?php echo $rowform->field_name; ?></label>
                                        <div class="controls">
                                            <?php if($names != ''): ?>
                                            <img src="<?php echo base_url().'uploads/user/'.$names; ?>" style="width: 100px;">
                                            <?php endif; ?>
                                            <input class="input-file uniform_on" name="<?php echo $rowform->field_nicename; ?>" id="fileInput" type="file">
                                        </div>
                                    </div>  
                                <?php
                                    break;
                                default: ?>
                                <div class="control-group">     
                                <?php $datafield = $this->user_m->ambil_content_data($datauser->id,$places_id,$rowform->id);
                                    $name = set_value($rowform->field_nicename);
                                    if ($name) {
                                        $names = $name;
                                    } elseif (!$datafield) {
                                        $names = '';
                                    } else {
                                        $names = $datafield->content;
                                    }
                                ?>                                     
                                    <label class="control-label" for="focusedInput"><?php echo $rowform->field_name; ?></label>
                                    <div class="controls">
                                        <input class="input-xlarge focused" name="<?php echo $rowform->field_nicename; ?>" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="<?php echo $rowform->field_name; ?>">
                                        <?php echo form_error($rowform->field_nicename, '<span class="help-inline">', '</span>'); ?>
                                    </div>
                                </div>  
                                <?php
                                    break;
                            }?>
                        <?php endforeach; ?>    
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     