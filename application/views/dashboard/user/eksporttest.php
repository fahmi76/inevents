<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add User</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Import File</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user/home/main/'.$places_id; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo base_url(); ?>dashboard/user/testupload/data/0">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="fileInput">Import Excel</label>
                            <div class="controls">
                                <input class="input-file uniform_on" name="userfile" id="fileInput" type="file">
                            </div>
                        </div>    			
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard/user/home/main/'.$places_id; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   
                <div class="control-group <?php if (form_error('name')) : ?> error<?php endif; ?>">   
                    <label class="control-label" for="focusedInput">Example file</label>
                    <div class="controls">
                        <a href="<?php echo base_url() . 'dashboard/user/eksport/download/'.$places_id;  ?>">
                            <input type="button" class="btn btn-success" value="Download File" />
                            <!--
                            <button class="btn">Cancel</button> -->
                        </a>
                    </div>
                </div>  
            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     