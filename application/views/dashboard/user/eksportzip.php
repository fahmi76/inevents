<script language="javascript" type="text/javascript">
    window.onload = function () {
        var fileUpload = document.getElementById("fileupload");
        fileUpload.onchange = function () {
            if (typeof (FileReader) != "undefined") {
                //Initiate the FileReader object.
                var reader = new FileReader();
                //Read the contents of Image File.
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();
     
                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;
                           
                    //Validate the File Height and Width.
                    image.onload = function () {

                        var dvPreview = document.getElementById("dvPreview");
                        dvPreview.innerHTML = "";
                        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                        for (var i = 0; i < fileUpload.files.length; i++) {
                            var file = fileUpload.files[i];
                            if (regex.test(file.name.toLowerCase())) {
                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    var img = document.createElement("IMG");
                                    img.height = "100";
                                    img.width = "100";
                                    img.src = e.target.result;
                                    dvPreview.appendChild(img);
                                }
                                reader.readAsDataURL(file);
                            } else {
                                alert(file.name + " is not a valid image file.");
                                dvPreview.innerHTML = "";
                                return false;
                            }
                        }

                    };
     
                }
            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        }
    };

    function change(param){
        top.location.href="<?php echo site_url('dashboard/slider/status/');?>/"+param;
    }
</script>
<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add User</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Import File</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user/home/main/'.$places_id; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="alert alert-info">
                    <h1>Upload your photos</h1>
                </div>
                <form class="form-horizontal" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo base_url(); ?>dashboard/user/eksport/datazip/<?php echo $places_id; ?>">
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="fileInput">file photos</label>
                            <div class="controls">
                            <input type="file" multiple="multiple" class="validate[required]" name="file[]" id="fileupload" placeholder="pilih file maximum(1000 X 405)"/>
                            </div>
                        </div>    	    
                        <hr />
                        <b>Live Preview</b>
                        <br />
                        <div id="dvPreview">
                        </div>		
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard/user/home/main/'.$places_id; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     