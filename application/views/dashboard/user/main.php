<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
                <div class="box-icon"> <!--
                    <a href="<?php echo base_url() . 'dashboard/user/home/deletemain'.$places_id; ?>"><i class="halflings-icon trash"></i></a>
                    -->
                    <a href="<?php echo base_url() . 'dashboard/user/eksport/main/'.$places_id; ?>"><i class="halflings-icon upload"></i></a>
                    <a href="<?php echo base_url() . 'dashboard/user/add/main/'.$places_id; ?>"><i class="halflings-icon plus"></i></a>
                </div>

            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <?php if($places_id != 27): ?>
                            <th>Username</th>
                            <?php else: ?>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <?php endif; ?>
                            <?php foreach($tabel as $rowtabel): ?>
                            <th><?php echo $rowtabel->field_name; ?></th>
                            <?php endforeach; ?>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <?php if($places_id != 27): ?>
                                <td><?php echo $row->account_displayname ?></td>
                                <?php else: ?>
                                <td><?php echo $row->account_firstname; ?></td>
                                <td><?php echo $row->account_lastname; ?></td>
                                <?php endif; ?>
                                <?php foreach($tabel as $rowtabel): ?>
                                <?php $datas = $this->user_m->ambil_content_data($row->id,$places_id,$rowtabel->id); ?> 
                                <?php if($datas): ?>
                                    <?php if($datas->field_type == 'file'): ?>
                                    <td class="center"><img src="<?php echo base_url(); ?>uploads/profile/<?php echo $datas->content; ?>" style="width: 120px;"/></td>
                                    <?php else: ?>
                                    <td class="center"><?php echo $datas->content; ?></td>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <td class="center"></td>
                                <?php endif; ?> 
                                <?php endforeach; ?>
                                <td class="center">
                                    <a class="btn btn-info" href="<?php echo base_url(); ?>dashboard/user/edit/main/<?php echo $row->id; ?>/<?php echo $places_id; ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="<?php echo base_url(); ?>dashboard/user/home/delete/<?php echo $row->id; ?>/<?php echo $places_id; ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->