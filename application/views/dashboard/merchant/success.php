<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add User</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2>Thank you</h2>
            </div>
            <div class="box-content">
                <div class="text-center">
                    <p>process has been completed.</p>
                    <meta http-equiv="refresh" content="2;URL=<?php echo site_url('dashboard/merchant'); ?>" />
                    <br>
                </div>
            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     
