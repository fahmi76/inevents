<script type="text/javascript">
    function back(){
        top.location.href="<?=site_url('dashboard/insight/sollam/main/'.$places_id);?>";
    }
</script>
<style type="text/css">
.search-table-outter {border:2px solid red; overflow-x:scroll;}
.search-table{table-layout: fixed; margin:40px auto 0px auto;   }
.search-table, td, th{border-collapse:collapse; border:1px solid #777;}
th{padding:20px 7px; font-size:15px; color:#444; background:#66C2E0;}
td{padding:5px 10px; height:35px;}
</style>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List Registrasi</h2>
            </div>
            <div class="box-content">
            <div class="search-table-outter wrapper">
                <table class="search-table inner">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Age</th>
                            <th>Work</th>
                            <th>Contact</th>
                            <th>Interest</th>
                            <th>Experience</th>
                            <th>Recomended</th>
                            <th>Comment</th>
                            <th>Facebook</th>
                            <th>Twitter</th>
                            <th>Time</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname ?></td>
                                <td><?php echo $row->account_email ?></td>
                                <td><?php echo $row->account_age ?></td>
                                <td><?php echo $row->account_work ?></td>
                                <td><?php echo $row->account_phone ?></td>
                                <td><?php echo $row->account_interest ?></td>
                                <td><?php echo $row->account_experience ?></td>
                                <td><?php echo $row->account_recomended ?></td>
                                <td><?php echo $row->account_comment ?></td>
                                <td><?php if($row->account_fbid != ''){ echo 'http://www.facebook.com/'.$row->account_fbid; } else{echo '';} ?></td>
                                <td><?php if($row->account_tw_username != ''){ echo '@'.$row->account_tw_username; } else{echo '';} ?></td>
                                <td><?php echo $row->account_joindate ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>   
            </div>         
            </div>
            <p>
                <button class="btn btn-large" onclick="back()"><i class="halflings-icon white arrow-left"></i> Back</button>
            </p>    
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->