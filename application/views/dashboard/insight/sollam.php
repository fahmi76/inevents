<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>

    <div class="row-fluid">
        <div class="span3 statbox red" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo count($data); ?></div>
            <div class="title">Total registration</div>
        	<div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/1/0"> read full report</a>
            </div>
        </div>
        <?php 
        	$fb = 0;
        	$tw = 0;
        	$age1 = 0;
        	$age2 = 0;
        	$age3 = 0;
        	$age4 = 0;
        	$age5 = 0;
        	$age6 = 0;
        	$exp1 = 0;
        	$exp2 = 0;
        	$exp3 = 0;
        	$exp4 = 0;
        	$exp5 = 0;
        	$rec1 = 0;
        	$rec2 = 0;
        	foreach ($data as $row) {
        		if($row->account_fbid != ""){
        			$fb++;
        		}
        		if($row->account_tw_username != ""){
        			$tw++;
        		}
        		if($row->account_recomended == "Yes"){
        			$rec1++;
        		}else{
        			$rec2++;
        		}
        		switch ($row->account_age) {
        			case 'Under 18':
        				$age1++;
        				break;
        			case '18 - 25':
        				$age2++;
        				break;
        			case '26 - 35':
        				$age3++;
        				break;
        			case '36 - 45':
        				$age4++;
        				break;
        			case '46 - 60':
        				$age5++;
        				break;
        			default:
        				$age6++;
        				break;
        		}
        		switch ($row->account_experience) {
        			case 'Poor':
        				$exp1++;
        				break;
        			case 'Fair':
        				$exp2++;
        				break;
        			case 'Good':
        				$exp3++;
        				break;
        			case 'Great':
        				$exp4++;
        				break;
        			default:
        				$exp5++;
        				break;
        		}
        	}

        ?>
        <div class="span3 statbox blue" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $fb; ?></div>
            <div class="title">Total using FB</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/2/1"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox blue" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $tw; ?></div>
            <div class="title">Total using TW</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/2/2"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $rec1; ?></div>
            <div class="title">Total Recommended Yes</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/3/1"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $rec2; ?></div>
            <div class="title">Total Recommended No</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/3/2"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $age1; ?></div>
            <div class="title">Total Under 18</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/4/1"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $age2; ?></div>
            <div class="title">Total 18 - 25</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/4/2"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $age3; ?></div>
            <div class="title">Total 26 - 35</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/4/3"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $age4; ?></div>
            <div class="title">Total 36 - 45</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/4/4"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $age5; ?></div>
            <div class="title">Total 46 - 60</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/4/5"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $age6; ?></div>
            <div class="title">Total Over 60</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/4/6"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox black" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $exp1; ?></div>
            <div class="title">Total experience Poor</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/5/1"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox black" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $exp2; ?></div>
            <div class="title">Total experience Fair</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/5/2"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox black" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $exp3; ?></div>
            <div class="title">Total experience Good</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/5/3"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox black" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $exp4; ?></div>
            <div class="title">Total experience Great</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/5/4"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox black" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $exp5; ?></div>
            <div class="title">Total experience Exceptional</div>
            <div class="footer">
            <a href="<?php echo base_url(); ?>dashboard/insight/sollam/mainlist/<?php echo $places_id; ?>/5/5"> read full report</a>
            </div>
        </div>
    </div>	


</div>

<!--/.fluid-container-->

<!-- end: Content -->