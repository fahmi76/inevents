<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>

    <div class="row-fluid">
        <div class="span3 statbox red" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $count_regis->total_account; ?></div>
            <div class="title">List Registration</div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/sosmed/regis/<?php echo $places_id; ?>"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $count_all; ?></div>
            <div class="title">List User All Station</div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/sosmed/listall/<?php echo $places_id; ?>"> read full report</a>
            </div>
        </div>
        <?php $x=1; foreach($station as $row): ?>
        <div class="span3 statbox blue" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $row['total']->total_account; ?></div>
            <div class="title">User Tap at station <?php echo $row['nama']; ?></div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/sosmed/checkin/<?php echo $row['id']; ?>/<?php echo $places_id; ?>"> read full report</a>
            </div>
        </div>
    	<?php $x++; endforeach; ?>
        <?php $y = $x - 1; ?>
    </div>	
<script type="text/javascript">
    $(document).ready(function() {	
		var dataregis = [
			<?php foreach($grafikregis['data'] as $row): ?>
				{ label: "<?php echo $row['date']; ?> hours (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach; ?>
		];
		if($("#piechartregis").length)
		{
			$.plot($("#piechartregis"), dataregis,
			{
				series: {
						pie: {
								show: true
						}
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});
			
			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechartregis").bind("plothover", pieHover);
		}
	});
	</script>

		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Registration</h2>
			</div>		
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/insight/sosmed/downloadgraph/0" id="myFormregis">
				<input type="hidden" name="img_valregis" id="img_valregis" value="" />
			</form>
				<div id="targetregis">
				<h2><span class="break"></span>Registration</h2>
			<div class="box-content">
				<div id="piechartregis" style="height:300px"></div>
			</div>	
				</div>
				<input type="submit" value="Download" onclick="capture();" />
			<script type="text/javascript">
				function capture() {
					html2canvas( $('#targetregis'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_valregis').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myFormregis").submit();
						}
					});
				}
			</script>
		</div>


    <?php $x=1; foreach($grafikcheckin as $rowgrafikin): ?>
	<script type="text/javascript">
    $(document).ready(function() {	
		var data<?php echo $x; ?> = [
			<?php foreach($rowgrafikin['data'] as $row): ?>
				{ label: "<?php echo $row['date']; ?> hours (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach; ?>
		];
		if($("#piechart<?php echo $x; ?>").length)
		{
			$.plot($("#piechart<?php echo $x; ?>"), data<?php echo $x; ?>,
			{
				series: {
						pie: {
								show: true
						}
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});
			
			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechart<?php echo $x; ?>").bind("plothover", pieHover);
		}
	});
	</script>

		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Check-in <?php echo $rowgrafikin['nama']; ?></h2>
			</div>		
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/insight/sosmed/downloadgraph/<?php echo $x; ?>" id="myForm<?php echo $x; ?>">
				<input type="hidden" name="img_val<?php echo $x; ?>" id="img_val<?php echo $x; ?>" value="" />
			</form>
				<div id="target<?php echo $x; ?>">
				<h2><span class="break"></span>Check-in <?php echo $rowgrafikin['nama']; ?></h2>
			<div class="box-content">
				<div id="piechart<?php echo $x; ?>" style="height:300px"></div>
			</div>	
				</div>
				<input type="submit" value="Download" onclick="capture();" />
			<script type="text/javascript">
				function capture() {
					html2canvas( $('#target<?php echo $x; ?>'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_val<?php echo $x; ?>').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myForm<?php echo $x; ?>").submit();
						}
					});
				}
			</script>
		</div>
	<?php $x++; endforeach; ?>
	<div class="box span12">
		<div class="box-header">
			<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Insight Social Media</h2><br><br>
		</div>	
		<?php 
			$fb_login = 0;
			$tw_login = 0;
			$fb_like = 0;
			$tw_follow = 0;
			$fb_friend = 0;
			$tw_friend = 0;
			if (isset($datasosmed) && count($datasosmed) != 0 ) {
				foreach ($datasosmed as $rows) {
					if($rows->account_fbid){
						$fb_login++;
					}
					if($rows->account_twid){
						$tw_login++;
					}
					if($rows->landing_fb_friends){
						$fb_friend += $rows->landing_fb_friends;
					}
					if($rows->landing_tw_friends){
						$tw_friend += $rows->landing_tw_friends;
					}
					if($rows->landing_fb_like == 1 || $rows->landing_fb_like_event == 1){
						$fb_like++;
					}
					if($rows->landing_tw_follow == 1 || $rows->landing_tw_follow_event == 1){
						$tw_follow++;
					}
				}
			}
		?>
		<div class="box-content">
			<a href="<?php echo base_url(); ?>dashboard/insight/sosmed/download/4/15/<?php echo $places_id; ?>"> Download</a>
			<p>Total Likes facebook fan pages : <?php echo $fb_like; ?></p>
			<p>Total Registrasi using facebook : <?php echo $fb_login; ?></p>
			<p>Total Registrasi using twitter : <?php echo $tw_login; ?></p>
			<p>Total facebook reach : 312751</p>
			<p>Total twitter reach : <?php echo $tw_friend; ?></p>
			<p>Total Update status on facebook : <?php echo $datalogfb; ?></p>
			<p>Total Update status on twitter : <?php echo $datalogtw; ?></p>
			<p>Total Photo on facebook : <?php echo $dataphotofb; ?></p>
			<p>Total Photo on twitter : <?php echo $dataphototw; ?></p>
		</div>	
		<?php
			$like_inter = array();
			$like_ctgry = array();
			/* likes */
			if (isset($datalike) && count($datalike) != 0 ) {
				foreach ($datalike as $like) {
				    if(isset($like->facebook_page_name) && $like->facebook_page_name != "")
				        $like_inter[] = strip_tags(trim($like->facebook_page_name));
				    if(isset($like->category_name) && $like->category_name != "")
				        $like_ctgry[] = strip_tags(trim($like->category_name));
				}
			}            

			$config = array(
				'min_font' => 12,
				'max_font' => 45,
				'html_start' => '',
				'html_end' => '',
				'shuffle' => FALSE,
				'class' => 'tag1',
				'match_Class' => 'bold',
			);

			if(!empty($like_inter)){
				$tag3 = $this->fungsi->insight_break($like_inter);
				$likes_tag = $this->taggly->cloud($tag3, $config);
			}
			if(!empty($like_ctgry)){
				$tag4 = $this->fungsi->insight_break($like_ctgry);
				$likes_cat = $this->taggly->cloud($tag4, $config); 
			}
		?>
	<div class="col-lg-4">
		<div class="box-header">
			<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Likes (By Name)</h2>
		</div>	
		<div class="box-content">
			<p>
				<?php if(isset($likes_tag)): ?>
	                <?php echo $likes_tag; ?>
	            <?php else: ?>
	                Not Data Likes(by Name)
	            <?php endif; ?>
			</p>
		</div>	
	</div>
	<div class="col-lg-4">
		<div class="box-header">
			<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Likes (By Category)</h2>
		</div>	
		<div class="box-content">
            <?php if(isset($likes_cat)): ?>
                <?php echo $likes_cat; ?>
            <?php else: ?>
                Not Data Likes(by Category)
            <?php endif; ?>
		</div>	
	</div>
	</div>

</div>

<!--/.fluid-container-->

<!-- end: Content -->