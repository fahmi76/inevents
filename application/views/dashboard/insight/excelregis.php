<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=".$titles.".xls");
 
?>
<html>
<head>
    <style>
        body {
            font-family: Arial;
        }
        table {
            border-collapse: collapse;
        }
        th {
            background-color: #cccccc;
        }
        th, td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
    <table>
        <thead>
        <tr>
            <th>Username</th>
            <?php foreach($tabel as $rowtabel): ?>
            <th><?php echo $rowtabel->field_name; ?></th>
            <?php endforeach; ?>
            <th>Status</th>
            <th>Time</th>
        </tr>
        </thead>
        <tbody>
			<?php foreach($data as $row): ?>
            <tr>
                <td><?php echo $row->account_displayname ?></td>
                <?php foreach($tabel as $rowtabel): ?>
                <?php $datas = $this->insight_m->ambil_content_data($row->account_id,$places_id,$rowtabel->id); ?> 
                <?php if($datas): ?>
                    <?php if($datas->field_type == 'file'): ?>
                    <td class="center"><img src="<?php echo base_url(); ?>uploads/user/<?php echo $datas->content; ?>" style="width: 120px;"/></td>
                    <?php else: ?>
                    <td class="center"><?php echo $datas->content; ?></td>
                    <?php endif; ?>
                <?php else: ?>
                    <td class="center"></td>
                <?php endif; ?> 
                <?php endforeach; ?>
                <?php if($from == 1): ?>
                <td>Pre-registration</td>
                <?php elseif($from == 2): ?>
                <td>New Registration</td>
                <?php else: ?>
                <td>Re-registration</td>
                <?php endif; ?>
                <td><?php echo $row->time_upload ?></td>
            </tr>
			<?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>