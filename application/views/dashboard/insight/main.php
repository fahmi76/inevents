<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>

    <div class="row-fluid">
        <?php if ($places_id == 21): ?>
	        <div class="span3 statbox red" onTablet="span6" onDesktop="span3">
	            <div class="number"><?php echo $count_pre; ?></div>
	            <div class="title">Total pre-registration</div>
            	<div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/listregis/<?php echo $places_id; ?>/1"> read full report</a>
                </div>
	        </div>
	        <div class="span3 statbox red" onTablet="span6" onDesktop="span3">
	            <div class="number"><?php echo $count_new; ?></div>
	            <div class="title">Total New registration on the spot</div>
                <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/listregis/<?php echo $places_id; ?>/2"> read full report</a>
                </div>
	        </div>
	        <div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
	            <div class="number"><?php echo $count_re; ?></div>
	            <div class="title">Total Re-registration</div>
                <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/listregis/<?php echo $places_id; ?>/3"> read full report</a>
                </div>
	        </div>
        <?php endif;?>
        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $count_all->total_account; ?></div>
            <div class="title">List All Checkin/Checkout</div>
            <div class="footer">
            	<?php if ($places_id == 11): ?>
                <a href="<?php echo base_url(); ?>dashboard/insight/home/mainlist/<?php echo $places_id; ?>/3/1/<?php echo $places_id; ?>"> read full report</a>
	            <?php else: ?>
                <a href="<?php echo base_url(); ?>dashboard/insight/home/listall/<?php echo $places_id; ?>"> read full report</a>
	            <?php endif;?>
            </div>
        </div>
    	<?php if ($places_id == 20): ?>
        <div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $count_guest; ?></div>
            <div class="title">Total Guest Adult</div>
        </div>
        <div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $count_guest_child; ?></div>
            <div class="title">Total Guest Child</div>
        </div>
    	<?php endif;?>
        <?php $x = 1;foreach ($checkin as $row): ?>
        <div class="span3 statbox blue" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $row['total']->total_account; ?></div>
            <div class="title">User Tap at Checkin <?php echo $row['nama']; ?></div>
            <div class="footer">
            	<?php if ($places_id == 11): ?>
                <a href="<?php echo base_url(); ?>dashboard/insight/home/mainlist/<?php echo $row['id']; ?>/1/1/<?php echo $places_id; ?>"> read full report</a>
	            <?php else: ?>
                <a href="<?php echo base_url(); ?>dashboard/insight/home/checkin/<?php echo $row['id']; ?>/1/1/<?php echo $places_id; ?>"> read full report</a>
	            <?php endif;?>
            </div>
        </div>
        <?php if ($places_id == 21): ?>
        <div class="span3 statbox black" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $row['email']; ?></div>
            <div class="title">User Email Sent at Checkin <?php echo $row['nama']; ?></div>
            <div class="footer">
            <!--
                <a href="<?php echo base_url(); ?>dashboard/insight/home/checkin/<?php echo $row['id']; ?>/1/1/<?php echo $places_id; ?>"> read full report</a>
                -->
            </div>
        </div>
    	<?php endif;?>
    	<?php $x++;endforeach;?>
        <?php $y = $x - 1;foreach ($checkout as $rows): ?>
        <div class="span3 statbox green" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $rows['total']->total_account; ?></div>
            <div class="title">User Tap at Checkout <?php echo $rows['nama']; ?></div>
            <div class="footer">
            	<?php if ($places_id == 11): ?>
                <a href="<?php echo base_url(); ?>dashboard/insight/home/mainlist/<?php echo $row['id']; ?>/2/1/<?php echo $places_id; ?>"> read full report</a>
	            <?php else: ?>
                <a href="<?php echo base_url(); ?>dashboard/insight/home/checkin/<?php echo $row['id']; ?>/2/1/<?php echo $places_id; ?>"> read full report</a>
	            <?php endif;?>
            </div>
        </div>

    	<?php endforeach;?>
    </div>
    <?php if ($places_id == 37): ?>
		<div class="row-fluid">
		<h3>Report Team</h3>
		<table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Team</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($team as $rowteam): ?>
                    <tr>
                        <td><?php echo $rowteam->content ?></td>
                        <td><?php echo $rowteam->total; ?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    	</div>
    <?php endif;?>
    <?php $x = 1;foreach ($grafikcheckin as $rowgrafikin): ?>
	<script type="text/javascript">
    $(document).ready(function() {
		var data<?php echo $x; ?> = [
			<?php foreach ($rowgrafikin['data'] as $row): ?>
				{ label: "<?php echo $row['date']; ?> hours (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach;?>
		];
		if($("#piechart<?php echo $x; ?>").length)
		{
			$.plot($("#piechart<?php echo $x; ?>"), data<?php echo $x; ?>,
			{
				series: {
						pie: {
								show: true
						}
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});

			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechart<?php echo $x; ?>").bind("plothover", pieHover);
		}
	});
	</script>

		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Check-in <?php echo $rowgrafikin['nama']; ?></h2>
			</div>
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/insight/home/downloadgraph/<?php echo $x; ?>" id="myForm<?php echo $x; ?>">
				<input type="hidden" name="img_val<?php echo $x; ?>" id="img_val<?php echo $x; ?>" value="" />
			</form>
				<div id="target<?php echo $x; ?>">
				<h2><span class="break"></span>Check-in <?php echo $rowgrafikin['nama']; ?></h2>
			<div class="box-content">
				<div id="piechart<?php echo $x; ?>" style="height:300px"></div>
			</div>
				</div>
				<input type="submit" value="Download" onclick="capture();" />
			<script type="text/javascript">
				function capture() {
					html2canvas( $('#target<?php echo $x; ?>'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_val<?php echo $x; ?>').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myForm<?php echo $x; ?>").submit();
						}
					});
				}
			</script>
		</div>
	<?php $x++;endforeach;?>

    <?php $x = 21;foreach ($grafikcheckout as $rowgrafikout): ?>
	<script type="text/javascript">
    $(document).ready(function() {
		var data<?php echo $x; ?> = [
			<?php foreach ($rowgrafikout['data'] as $row): ?>
				{ label: "<?php echo $row['date']; ?> hours (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach;?>
		];
		if($("#piechart<?php echo $x; ?>").length)
		{
			$.plot($("#piechart<?php echo $x; ?>"), data<?php echo $x; ?>,
			{
				series: {
						pie: {
								show: true
						}
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});

			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechart<?php echo $x; ?>").bind("plothover", pieHover);
		}
	});
	</script>

		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Check-out <?php echo $rowgrafikout['nama']; ?></h2>
			</div>
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/insight/home/downloadgraph/<?php echo $x; ?>" id="myForm<?php echo $x; ?>">
				<input type="hidden" name="img_val<?php echo $x; ?>" id="img_val<?php echo $x; ?>" value="" />
			</form>
				<div id="target<?php echo $x; ?>">
				<h2><span class="break"></span>Check-out <?php echo $rowgrafikout['nama']; ?></h2>
			<div class="box-content">
				<div id="piechart<?php echo $x; ?>" style="height:300px"></div>
			</div>
				</div>
				<input type="submit" value="Download" onclick="capture();" />
			<script type="text/javascript">
				function capture() {
					html2canvas( $('#target<?php echo $x; ?>'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_val<?php echo $x; ?>').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myForm<?php echo $x; ?>").submit();
						}
					});
				}
			</script>
		</div>
	<?php $x++;endforeach;?>

</div>

<!--/.fluid-container-->

<!-- end: Content -->