<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=" . $title . ".xls");

?>
<html>
<head>
    <style>
        body {
            font-family: Arial;
        }
        table {
            border-collapse: collapse;
        }
        th {
            background-color: #cccccc;
        }
        th, td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
    <table>
        <thead>
        <tr>
            <th>Username</th>
            <?php foreach ($tabel as $rowtabel): ?>
            <th><?php echo $rowtabel->field_name; ?></th>
            <?php endforeach;?>
            <?php if ($places_id == 10): ?>
            <th>Guest From</th>
            <?php endif;?>
            <th>Gate</th>
            <th>Status</th>
            <th>Time Check-in</th>
            <th>Time Check-out</th>
        </tr>
        </thead>
        <tbody>
			<?php foreach ($user as $row): ?>
            <tr>
                <td><?php echo $row->account_displayname ?></td>
                <?php foreach ($tabel as $rowtabel): ?>
                <?php $datas = $this->insight_m->ambil_content_data($row->account_id, $places_id, $rowtabel->id);?>
                <?php if ($datas): ?>
                    <?php if ($datas->field_type == 'file'): ?>
                    <td class="center"><img src="<?php echo base_url(); ?>uploads/user/<?php echo $datas->content; ?>" style="width: 120px;"/></td>
                    <?php else: ?>
                    <td class="center"><?php echo $datas->content; ?></td>
                    <?php endif;?>
                <?php else: ?>
                    <td class="center"></td>
                <?php endif;?>
                <?php endforeach;?>
                <?php if ($places_id == 10): ?>
                    <?php if ($row->log_fb_places == 0): ?>
                    <td>NO</td>
                    <?php elseif ($row->log_fb_places == 1): ?>
                    <td>YES</td>
                    <?php else: ?>
                    <td>LATER</td>
                    <?php endif;?>
                <?php endif;?>
                <td class="center"><?php echo $this->insight_m->get_gate($row->log_gate); ?></td>
                <!--
                <td class="center"><?php echo $row->nama; ?></td>
                -->
                <?php if ($id == 1 || $id == 2 || $id == 3): ?>
                <?php if ($row->log_check_out == 2): ?>
                <td>Check-out</td>
                <?php else: ?>
                <td>Currently Check-in</td>
                <?php endif;?>
                <?php else: ?>
                <td><?php echo $status; ?></td>
                <?php endif;?>
                <td><?php echo $row->time_upload ?></td>
                <td><?php echo $row->log_date . ' ' . $row->log_time; ?></td>
            </tr>
			<?php endforeach;?>
        </tbody>
    </table>
</body>
</html>