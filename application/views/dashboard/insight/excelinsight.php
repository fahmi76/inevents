<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=".$title.".xls");
 
?>
<body style="width: 600px;">

<div class="box-header">
			<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Insight Social Media</h2>
		</div>	
		<?php 
			$fb_login = 0;
			$tw_login = 0;
			$fb_like = 0;
			$tw_follow = 0;
			$fb_friend = 0;
			$tw_friend = 0;
			if (isset($datasosmed) && count($datasosmed) != 0 ) {
				foreach ($datasosmed as $rows) {
					if($rows->account_fbid){
						$fb_login++;
					}
					if($rows->account_twid){
						$tw_login++;
					}
					if($rows->landing_fb_friends){
						$fb_friend += $rows->landing_fb_friends;
					}
					if($rows->landing_tw_friends){
						$tw_friend += $rows->landing_tw_friends;
					}
					if($rows->landing_fb_like == 1 || $rows->landing_fb_like_event == 1){
						$fb_like++;
					}
					if($rows->landing_tw_follow == 1 || $rows->landing_tw_follow_event == 1){
						$tw_follow++;
					}
				}
			}
		?>
		<div class="box-content">
			<p>Total Likes facebook fan pages : <?php echo $fb_like; ?></p>
			<p>Total Registrasi using facebook : <?php echo $fb_login; ?></p>
			<p>Total Registrasi using twitter : <?php echo $tw_login; ?></p>
			<p>Total facebook reach : 312751</p>
			<p>Total twitter reach : <?php echo $tw_friend; ?></p>
			<p>Total Update status on facebook : <?php echo $datalogfb; ?></p>
			<p>Total Update status on twitter : <?php echo $datalogtw; ?></p>
			<p>Total Photo on facebook : <?php echo $dataphotofb; ?></p>
			<p>Total Photo on twitter : <?php echo $dataphototw; ?></p>
		</div>	
		<?php
			$like_inter = array();
			$like_ctgry = array();
			/* likes */
			if (isset($datalike) && count($datalike) != 0 ) {
				foreach ($datalike as $like) {
				    if(isset($like->facebook_page_name) && $like->facebook_page_name != "")
				        $like_inter[] = strip_tags(trim($like->facebook_page_name));
				    if(isset($like->category_name) && $like->category_name != "")
				        $like_ctgry[] = strip_tags(trim($like->category_name));
				}
			}            

			$config = array(
				'min_font' => 12,
				'max_font' => 45,
				'html_start' => '',
				'html_end' => '',
				'shuffle' => FALSE,
				'class' => 'tag1',
				'match_Class' => 'bold',
			);

			if(!empty($like_inter)){
				$tag3 = $this->fungsi->insight_break($like_inter);
				$likes_tag = $this->taggly->cloud($tag3, $config);
			}
			if(!empty($like_ctgry)){
				$tag4 = $this->fungsi->insight_break($like_ctgry);
				$likes_cat = $this->taggly->cloud($tag4, $config); 
			}
		?>
	<div class="col-lg-4">
		<div class="box-header">
			<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Likes (By Name)</h2>
		</div>	
		<div class="box-content">
			<p>
				<?php if(isset($likes_tag)): ?>
	                <?php echo $likes_tag; ?>
	            <?php else: ?>
	                Not Data Likes(by Name)
	            <?php endif; ?>
			</p>
		</div>	
	</div>
	<div class="col-lg-4">
		<div class="box-header">
			<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Likes (By Category)</h2>
		</div>	
		<div class="box-content">
            <?php if(isset($likes_cat)): ?>
                <?php echo $likes_cat; ?>
            <?php else: ?>
                Not Data Likes(by Category)
            <?php endif; ?>
		</div>	
	</div>	
</body>