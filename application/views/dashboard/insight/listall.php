<script type="text/javascript">
    function back(){
        top.location.href="<?=site_url('dashboard/insight/sosmed/main/'.$places_id);?>";
    }
    function delete_event(param){
        var r = confirm("Do you want delete this data?");
        if (r == true){
            $.ajax({
              url: '<?=site_url('dashboard/gate/home/delete_places/');?>',
              type: 'POST',
              dataType: "json",
              data: 'places_id='+param,
              success: function(data) {
                //document.getElementById("status_team_"+id_div).innerHTML = data;
                location.reload();
                //console.log(data);
              },
                error: function(e) {
              }
            });         
        }       
    }
</script>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List User</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/insight/sosmed/download/2/'.$places_id.'/'.$places_id; ?>"><i class="halflings-icon download"></i></a>
                </div>
                <!--
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/insight/sosmed/delete/'.$from.'/1/'.$places_id; ?>"><i class="halflings-icon trash"></i></a>
                    <a href="<?php echo base_url() . 'dashboard/insight/sosmed/download/'.$from.'/1/'.$places_id; ?>"><i class="halflings-icon download"></i></a>
                </div>
                -->
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="example">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Facebook</th>
                            <th>Twitter</th>
                            <th>Booth 1</th>
                            <th>Booth 2</th>
                            <th>Booth 3</th>
                            <th>Photo</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname ?></td>
                                <td><?php if($row->account_fbid != ''){ echo 'http://www.facebook.com/'.$row->account_fbid; } else{echo '';} ?></td>
                                <td><?php if($row->account_tw_username != ''){ echo '@'.$row->account_tw_username; } else{echo '';} ?></td>
                                <td><?php echo $row->time_booth1; ?></td>
                                <td><?php echo $row->time_booth2; ?></td>
                                <td><?php echo $row->time_booth3; ?></td>
                                <td><?php echo $row->time_boothp; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
            <p>
                <button class="btn btn-large" onclick="back()"><i class="halflings-icon white arrow-left"></i> Back</button>
            </p>    
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->