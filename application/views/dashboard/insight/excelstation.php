<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=".$title.".xls");
 
?>
<html>
<head>
    <style>
        body {
            font-family: Arial;
        }
        table {
            border-collapse: collapse;
        }
        th {
            background-color: #cccccc;
        }
        th, td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
    <p><?php echo $status; ?></p>
    <?php if($id != 2): ?>
    <table>
        <thead>
        <tr>
            <th>Username</th>
            <th>Facebook</th>
            <th>Twitter</th>
            <th>Time</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $row): ?>
                <tr>
                    <td><?php echo $row->account_displayname ?></td>
                    <td><?php if($row->account_fbid != ''){ echo 'http://www.facebook.com/'.$row->account_fbid; } else{echo '';} ?></td>
                    <td><?php if($row->account_tw_username != ''){ echo '@'.$row->account_tw_username; } else{echo '';} ?></td>
                    <td><?php echo $row->time_upload ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else: ?>
    <table>
        <thead>
            <tr>
                <th>Username</th>
                <th>Facebook</th>
                <th>Twitter</th>
                <th>Booth 1</th>
                <th>Booth 2</th>
                <th>Booth 3</th>
                <th>Photo</th>
            </tr>
        </thead>   
        <tbody>
            <?php foreach ($data as $row): ?>
                <tr>
                    <td><?php echo $row->account_displayname ?></td>
                    <td><?php if($row->account_fbid != ''){ echo 'http://www.facebook.com/'.$row->account_fbid; } else{echo '';} ?></td>
                    <td><?php if($row->account_tw_username != ''){ echo '@'.$row->account_tw_username; } else{echo '';} ?></td>
                    <td><?php echo $row->time_booth1; ?></td>
                    <td><?php echo $row->time_booth2; ?></td>
                    <td><?php echo $row->time_booth3; ?></td>
                    <td><?php echo $row->time_boothp; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</body>
</html>