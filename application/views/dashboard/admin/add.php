<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add Admin</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Admin</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/admin'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
			<div class="control-group <?php if (form_error('name')) : ?> error<?php endif; ?>">                        
                            <label class="control-label" for="focusedInput">Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" placeholder="Name" value="<?php echo set_value('name'); ?>">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
			<div class="control-group <?php if (form_error('email')) : ?> error<?php endif; ?>"> 
                            <label class="control-label" for="focusedInput">Email</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="email" autocomplete="off" id="focusedInput" type="text" placeholder="Email@email.com" value="<?php echo set_value('email'); ?>">
                                <?php echo form_error('email', '<span class="help-inline">', '</span>'); ?>
                           </div>
                        </div>    
			<div class="control-group <?php if (form_error('password')) : ?> error<?php endif; ?>"> 
                            <label class="control-label" for="focusedInput">Password</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="password" autocomplete="off" id="focusedInput" type="text" placeholder="Password">
                                <?php echo form_error('password', '<span class="help-inline">', '</span>'); ?>
                           </div>
                        </div>    
                        <!--
                        <div class="control-group">
                            <label class="control-label">Role</label>
                            <div class="controls">
                                <label class="checkbox inline">
                                    <input type="radio" name="role" id="inlineCheckbox1" value="9"> Super Admin
                                </label>
                                <label class="checkbox inline">
                                    <input type="radio" name="role" id="inlineCheckbox2" value="8"> Admin
                                </label>
                            </div>
                        </div>   
                        -->    
                        <div class="control-group <?php if (form_error('event')) : ?> error<?php endif; ?>">
                            <label class="control-label">Event</label>
                            <div class="controls">
                                <?php foreach($list_event as $row): ?>
                                <label class="checkbox inline">
                                    <input type="checkbox" name="event[]" id="inlineCheckbox1" value="<?php echo $row->id; ?>">  <?php echo $row->places_name; ?>
                                </label>
                                <?php endforeach; ?>
                            </div>
                            <?php echo form_error('event', '<span class="help-inline">', '</span>'); ?>
                        </div>                   
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard/admin'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     