<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List <?php if($from == 1){echo 'Pre-registration';}elseif($from == 2){echo 'New Registration';}else{echo 'Re-registration';}?></h2>
            
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/insight/home/listregis/'.$places_id.'/'.$from.'?download=1'; ?>"><i class="halflings-icon download"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="example">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Time</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname ?> 
                                </td>
                                <td><?php echo $row->time_upload ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->