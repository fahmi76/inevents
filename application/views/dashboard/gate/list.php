<style type="text/css">
#example {
    overflow-x: scroll;
    max-width: 100%;
    display: block;
    white-space: nowrap;
}
</style>
<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List Gate <?php echo $check; ?> <?php if ($from == 1) {echo 'Check-in';} elseif ($from == 2) {echo 'Check-out';} else {echo 'Arrive';}?></h2>

                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/insight/home/delete/' . $from . '/' . $gate . '/' . $places_id; ?>"><i class="halflings-icon trash"></i></a>
                    <a href="<?php echo base_url() . 'dashboard/insight/home/download/' . $from . '/' . $gate . '/' . $places_id; ?>"><i class="halflings-icon download"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="example">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <?php foreach ($tabel as $rowtabel): ?>
                            <th><?php echo $rowtabel->field_name; ?></th>
                            <?php endforeach;?>
                            <th>Status</th>
                            <th>Time</th>
                            <?php if ($from == 2): ?>
                            <th>Duration</th>
                            <?php endif;?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname ?>
                                    <?php if ($row->log_fb_places == 1): ?>
                                        (GUEST)
                                    <?php endif;?>
                                    <?php if ($row->log_fb_places == 3): ?>
                                        (GUEST)
                                    <?php endif;?>
                                </td>
                                <?php foreach ($tabel as $rowtabel): ?>
                                <?php $datas = $this->insight_m->ambil_content_data($row->account_id, $places_id, $rowtabel->id);?>
                                <?php if ($datas): ?>
                                    <?php if ($datas->field_type == 'file'): ?>
                                    <td class="center"><img src="<?php echo base_url(); ?>uploads/user/<?php echo $datas->content; ?>" style="width: 120px;"/></td>
                                    <?php else: ?>
                                    <td class="center"><?php echo $datas->content; ?></td>
                                    <?php endif;?>
                                <?php else: ?>
                                    <td class="center"></td>
                                <?php endif;?>
                                <?php endforeach;?>
                                <!--
                                <td class="center"><?php echo $row->nama; ?></td>
                                <?php if ($places_id == 11): ?>
                                    <?php if ($row->log_fb_places == 0): ?>
                                    <td>NO</td>
                                    <?php elseif ($row->log_fb_places == 1): ?>
                                    <td>YES</td>
                                    <?php else: ?>
                                    <td>LATER</td>
                                    <?php endif;?>
                                <?php endif;?>
                                -->
                                <td class="center">
                                    <?php if ($row->log_check_out == 2): ?>
                                    <span class="label label-error">Check-out</span>
                                    <?php else: ?>
                                        <?php if ($row->log_fb_places == 3): ?>
                                            <span class="label label-danger">did not already check in a guest</span>
                                        <?php else: ?>
                                            <span class="label label-success">Currently Check-in</span>
                                        <?php endif;?>
                                    <?php endif;?>
                                </td>
                                <?php if ($from == 2): ?>
                                <?php $duration = $this->insight_m->get_last_checkout($row->log_date, $row->account_id, $gate);?>
                                <td><?php echo $duration['checkout'] ?></td>
                                <td><?php echo $duration['duration'] ?></td>
                                <?php else: ?>
                                <td><?php echo $this->insight_m->get_first_checkin($row->log_date, $row->account_id, $gate); ?></td>
                                <?php endif;?>
                            </tr>

                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->