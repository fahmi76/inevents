<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add Gate</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Gate Event <?php echo $places->places_name; ?></h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">          
                            <?php
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } else {
                                $names = '';
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div> 
                        <?php if($places_id == 21): ?>
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">                        
                            <label class="control-label" for="focusedInput">Sender Name</label>
                            <?php
                            $nameemail = set_value('nameemail');
                            if ($nameemail) {
                                $nameemails = $nameemail;
                            } else {
                                $nameemails = '';
                            }
                            ?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="nameemail" autocomplete="off" id="focusedInput" type="text" value="<?php echo $nameemails; ?>" placeholder="Name">
                                <?php echo form_error('nameemail', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">                        
                            <label class="control-label" for="focusedInput">Subject</label>
                            <?php
                            $subject = set_value('subject');
                            if ($subject) {
                                $subjects = $subject;
                            } else {
                                $subjects = '';
                            }
                            ?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="subject" autocomplete="off" id="focusedInput" type="text" value="<?php echo $subjects; ?>" placeholder="Subject">
                                <?php echo form_error('subject', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">                        
                            <label class="control-label" for="focusedInput">Content</label>
                            <?php
                            $content = set_value('content');
                            if ($content) {
                                $contents = $content;
                            } else {
                                $contents = '';
                            }
                            ?>
                            <div class="controls">
                                <textarea name="content">
                                    <?php echo $contents; ?>
                                </textarea>
                                <?php echo form_error('content', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  

                        <div class="control-group">
                            <label class="control-label" for="fileInput">Attachment</label>
                            <div class="controls">
                                <input class="input-file uniform_on" onchange="readURL(this);" id="fileInput" name="file" type="file">
                            </div>
                        </div>  
                        <?php endif; ?>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard/gate/home/main/'.$places_id; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     