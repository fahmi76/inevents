<script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<?php if (validation_errors()) : ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', ''); ?>
    </p>
<?php endif; ?> 
<br />
<form method="post" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Tap your RFID</label>       
        <input type="text" class="form-control input-lg" name="rfid" id="InputName" value=""  placeholder="RFID" autocomplete='off'>
    </div>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
    </div>
</form>