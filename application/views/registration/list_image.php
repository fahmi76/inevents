<?php if (isset($retry) && $retry === true) { ?>
    <p class="error">Please Try Again <?php if (isset($status) && $status != '') echo 'due to ' . str_replace('_', ' ', $status) ?> </p>
<?php } ?>
<div class="text-center">
    <h3>Click Below to Register</h3>
    <?php $redirect = base_url() . 'registration/login/facebook/0/0/step1'; ?> 
    <a class="fb-connect" href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('logout'), 'redirect_uri' => $redirect, 'scope' => $fb_perms)); ?>">
        <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
    </a>
    <a class="tw-connect" href="<?php echo base_url(); ?>registration/login/twitter/0/0/step1">
        <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
    </a><?php if ($spot_id == 258 || $spot_id == 454 || $spot_id == 453 || $spot_id == 480): ?>
        <div class="alert alert-warning small">By registering, I agree to provide my Name and Email address to the event organizers.</div>
    <?php else: ?>
        <div class="alert alert-warning small">Dengan mendaftar saya bersedia memberikan Nama dan Email saya kepada pihak penyelenggara acara.</div>
    <?php endif; ?>
</div>