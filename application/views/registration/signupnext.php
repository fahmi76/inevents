<script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://woozin.tgrid.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<?php if ($spot_id == 413): ?>
    <div class="alert alert-warning small">Mohon isi data kamu secara benar karena akan ada undian dengan hadiah Rekening Ponsel senilai masing-masing Rp 500.000 untuk 10 orang pemenang.</div>
<?php endif; ?>
<div class="panel panel-info">
    <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
            <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>

</div>
<?php if($from == 'fb'): ?>
    <div class="panel-heading">"Like" Facebook Page</div>
    <div class="panel-body">
        <iframe 
            src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fjoin.connect.woozin&amp;
                    width&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=false&amp;
                    show_border=true" 
            scrolling="no" 
            frameborder="0" 
            style="border:none; overflow:hidden; height:62px;" allowTransparency="true">
               
        </iframe>
    </div>
    
<?php else: ?>
    <div class="panel-heading">"Follow" Twitter Page</div>
    <div class="panel-body">
        <iframe
            src="//platform.twitter.com/widgets/follow_button.html?screen_name=wooz_in&show_count=false"
            style="width: 300px; height: 20px;"
            allowtransparency="true"
            frameborder="0"
            scrolling="no">
        </iframe>
    </div>
<?php endif; ?>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">
        <div class="form-group">
            <label for="InputName">Full Name</label>        
            <?php
            $name = set_value('fullname');
            if ($name) {
                $names = $name;
            } else {
                $names = '';
            }
            ?>
            <input type="text" class="form-control input-lg" name="fullname" id="InputName" autocomplete="off" value="<?php echo $names; ?>" placeholder="your name">
        </div>
        <?php if ($from == 'fb' || $from == 'tw'): ?>
            <div class="form-group">
                <?php
                $fbname = set_value('sosmed');
                if ($from == 'fb') {
                    ?>
                    <label for="InputName">Facebook Account Name</label> 
                    <?php
                    $sosmednames = $info->account_fb_name;
                } else {
                    ?>
                    <label for="InputName">Twitter Account Name</label>
                    <?php
                    $sosmednames = '@' . $info->account_tw_username;
                }
                ?>
                <input type="text" class="form-control input-lg" id="sosmed" name="sosmed" value="<?php echo $sosmednames; ?>" disabled />
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label for="InputEmail">Email Address</label>
            <?php
            $email = set_value('email');
            if ($email) {
                $emails = $email;
            } else {
                $emails = '';
            }
            ?>
            <input type="email" class="form-control input-lg" name="email" id="InputEmail" autocomplete="off" value="<?php echo $emails; ?>" placeholder="youremail@youremaildomain.com">
        </div>
        <div class="text-center">            
            <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
            <?php if (isset($token) && $token != ''): ?>
                <a href="<?php echo $this->facebook->getLogoutUrl(array('next' => site_url('combirun/logout'), 'access_token' => $token)); ?>">
                    <button type="button" class="btn btn-default btn-xlg">Cancel</button>
                </a>
            <?php else: ?>
                <a href="<?php echo base_url('landing/logout?url=' . $customs . '&places=' . $spot_id); ?>">
                    <button type="button" class="btn btn-default btn-xlg">Cancel</button>
                </a>
            <?php endif; ?>
        </div>
</form>