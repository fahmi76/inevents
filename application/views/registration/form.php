<script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>registration/search/1",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id); 
                        $("#Inputcompany").val(ui.item.company); 
                        $("#Inputemployee").val(ui.item.employee); 
                        $("#Inputdepartment").val(ui.item.department);  
                    }
        });
    });

    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputEmail").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>registration/search/2",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                    }
        });
    });
    
</script>
<script type="text/javascript">
    $(function() {
        update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>gates/curr_check", function(data) {
            $("p#results").empty();

            $.each(data.json, function() {
                $("p#results").append("\
                        \n\
                                Currently Checked In <b><span>: \n\
                                    " + this['total'] + "\
                                \n\
                        </span></b>");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
</script>
    <div class="col-md-6">

    </div>
    <div class="col-md-6 text-right">

    </div>
    <table class="table">
  <tr>
    <th><?php if (validation_errors()) : ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', ''); ?>
    </p>
<?php endif; ?> 
<form method="post" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Fill your name</label>       
        <input type="text" class="form-control input-lg" name="name" id="InputName" value=""  placeholder="name">
    </div>
    <input type="hidden" class="form-control input-lg" name="id" id="InputId" value=""  placeholder="">
        <input type="hidden" class="form-control input-lg" name="tap" id="InputId" value="2">

    <div class="form-group">
        <label for="InputName">Company</label>       
        <input type="text" class="form-control input-lg" name="company" id="Inputcompany" value=""  placeholder="company" disabled="disabled">
    </div>
    <div class="form-group">
        <label for="InputName">Employee Class</label>       
        <input type="text" class="form-control input-lg" name="employee" id="Inputemployee" value=""  placeholder="employee" disabled="disabled">
    </div>
    <div class="form-group">
        <label for="InputName">Department</label>       
        <input type="text" class="form-control input-lg" name="department" id="Inputdepartment" value=""  placeholder="department" disabled="disabled">
    </div>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
    </div>
</form>
</th>
    <th style="width:45%;">
    <h2 class="tap-wristband"><p id="tap-code"><span>Tap Your Card Here</span></p></h2>
    <h2 class="tap-wristband"><p class="notify"><b><span>Check-In</span></b></p></h2>
    <h2 class="tap-wristband"><p class="notify" id="results"></p></h2>
<form method="post" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <input type="hidden" class="form-control input-lg" name="tap" id="InputId" value="1">
        <input type="text" class="form-control input-lg" name="rfid" id="RFIDinput" autocomplete="off" value=""  placeholder="Tap Your RFID">
    </div>
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
    </div>
</form></th>
  </tr>
        <td>
</td>
        <td class="text-align: center;"></td>
    </table>