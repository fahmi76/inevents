<script type="text/javascript">
    $(document).ready(function() {
        $("#rfidNumber").focus();
    });
</script>

        <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
<form method="post" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">name</label>       
        <input type="text" class="form-control input-lg" name="name" id="InputName" value="<?php echo $nama; ?>" disabled="disabled">
    </div>
    <!--
        <div class="form-group">
            <label for="InputEmail">Email Address</label>
            <?php
            $email = set_value('email');
            if ($email) {
                $emails = $email;
            } elseif (!$info) {
                $emails = '';
            } else {
                $emails = $info->account_email;
            }
            ?>
            <input type="email" class="form-control input-lg" name="email" id="InputEmail" autocomplete="off" value="<?php echo $emails; ?>" placeholder="youremail@youremaildomain.com">
        </div>
        <div class="form-group">
            <label for="InputGender">Gender</label>
            <br>
            <?php
            $gender = set_value('gender');
            if ($gender) {
                $genders = $gender;
            } elseif (!$info) {
                $genders = '';
            } else {
                $genders = $info->account_gender;
            }
            ?>
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-warning <?php if ($genders == "male") : ?>active<?php endif; ?>">
                    <input type="radio" name="gender" id="InputGenderMale" value="male" <?php if ($genders == "male") : ?>checked="checked"<?php endif; ?>> Male
                </label>
                <label class="btn btn-warning <?php if ($genders == "female") : ?>active<?php endif; ?>">
                    <input type="radio" name="gender" id="InputGenderFemale" value="female" <?php if ($genders == "female") : ?>checked="checked"<?php endif; ?>> Female
                </label>
            </div>
        </div>  
        <div class="form-group">
            <label for="InputBirth">Birthdate</label>

            <?php
            if ($info) {
                $birthdate = explode('-', $info->account_birthdate);
            } else {
                $birthdate[2] = 00;
                $birthdate[0] = 0000;
                $birthdate[1] = 0;
            }

            $tgl = set_value('tgl');
            if ($tgl) {
                $tgls = $tgl;
            } elseif ($birthdate[2] == 00) {
                $tgls = '';
            } else {
                $tgls = $birthdate[2];
            }
            $thn = set_value('thn');
            if ($thn) {
                $thns = $thn;
            } elseif ($birthdate[0] == 0000) {
                $thns = '';
            } else {
                $thns = $birthdate[0];
            }
            $bln = set_value('bln');
            if ($bln) {
                $blns = $bln;
            } else {
                $blns = $birthdate[1];
            }
            ?>
            <div class="form-inline">
                <div class="form-group" style="width: 65px">
                    <label class="sr-only" for="InputDate">Date</label>
                    <input type="text" class="form-control input-lg" id="InputDate" name="tgl" autocomplete="off" value="<?php echo $tgls; ?>" placeholder="Tgl">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="InputMonth">Month</label>
                    <select class="form-control input-lg" name="bln" id="InputMonth">
                        <option value="0">--Month--</option>
                        <option value="1" <?php if ($blns == 1) : ?>selected="selected"<?php endif; ?>>January</option>
                        <option value="2" <?php if ($blns == 2) : ?>selected="selected"<?php endif; ?>>February</option>
                        <option value="3" <?php if ($blns == 3) : ?>selected="selected"<?php endif; ?>>March</option>
                        <option value="4" <?php if ($blns == 4) : ?>selected="selected"<?php endif; ?>>April</option>
                        <option value="5" <?php if ($blns == 5) : ?>selected="selected"<?php endif; ?>>May</option>
                        <option value="6" <?php if ($blns == 6) : ?>selected="selected"<?php endif; ?>>June</option>
                        <option value="7" <?php if ($blns == 7) : ?>selected="selected"<?php endif; ?>>July</option>
                        <option value="8" <?php if ($blns == 8) : ?>selected="selected"<?php endif; ?>>August</option>
                        <option value="9" <?php if ($blns == 9) : ?>selected="selected"<?php endif; ?>>September</option>
                        <option value="10" <?php if ($blns == 10) : ?>selected="selected"<?php endif; ?>>October</option>
                        <option value="11" <?php if ($blns == 11) : ?>selected="selected"<?php endif; ?>>November</option>
                        <option value="12" <?php if ($blns == 12) : ?>selected="selected"<?php endif; ?>>December</option>
                    </select> 
                </div> 
                <div class="form-group" style="width: 100px">
                    <label class="sr-only" for="InputYear">Year</label>
                    <input type="text" class="form-control input-lg" id="InputYear" name="thn" autocomplete="off" value="<?php echo $thns; ?>" placeholder="Tahun">
                </div>                   
            </div>
    <div class="form-group">
        <label for="InputName">Company</label>        
        <?php
        $company = set_value('company');
        if ($company) {
            $companys = $company;
        } elseif (!$info) {
            $companys = '';
        } else {
            $companys = $info->account_gw_token;
        }
        ?>
        <input type="text" class="form-control input-lg" name="company" id="InputName" autocomplete="off" value="<?php echo $companys; ?>" placeholder="your company">
    </div>    
    <div class="form-group">
        <label for="InputName">Employee Class</label>        
        <?php
        $employee = set_value('employee');
        if ($employee) {
            $employees = $employee;
        } elseif (!$info) {
            $employees = '';
        } else {
            $employees = $info->account_gw_token;
        }
        ?>
        <input type="text" class="form-control input-lg" name="employee" id="InputName" autocomplete="off" value="<?php echo $employees; ?>" placeholder="Employee Class">
    </div>    
    <div class="form-group">
        <label for="InputName">Department</label>        
        <?php
        $department = set_value('department');
        if ($department) {
            $departments = $department;
        } elseif (!$info) {
            $departments = '';
        } else {
            $departments = $info->account_gw_time;
        }
        ?>
        <input type="text" class="form-control input-lg" name="department" id="InputName" autocomplete="off" value="<?php echo $departments; ?>" placeholder="your department">
    </div>    
    -->
    <div class="form-group">
        <label for="InputName">RFID</label>    
        <input type="text" class="form-control input-lg" name="rfid" id="rfidNumber" autocomplete="off" value="" placeholder="RFID">
    </div>    
    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        <a href="<?php echo base_url('registration/done/'.$id); ?>">
            <button type="button" class="btn btn-default btn-xlg">Cancel</button>
        </a>
    </div>
</form>