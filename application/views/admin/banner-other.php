<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th><strong>No.</strong></th>
                <th><strong>Image</strong></th>
                <th><strong>Title</strong></th>
                <th><strong>Category</strong></th>
            </tr>
        </thead>
        <?php if($num != 0) : ?>
        <tbody>
       		<?php $no = 1; foreach($databanner as $dbanner) : ?>
           	<tr class="oddrow">
                <td class="firstcol"><?php echo $no++; ?>.</td>
                <td class="dataitem">
                	<img src="<?php echo base_url();?>uploads/banner/<?php echo $dbanner->banner_image; ?>" width="264" alt="" />
                </td>
                <td class="edititem" style="width: 200px;"><?php echo stripslashes($dbanner->banner_title); ?></td>
                <td class="edititem"><?php echo stripslashes($dbanner->banner_cat); ?></td>
            </tr>
            <?php endforeach; else: ?>
            <tr class="oddrow">
                <td class="firstcol">1.</td>
                <td class="dataitem">No Data</td>
                <td class="edititem">No Data</td>
                <td class="edititem">No Data</td>
            </tr>
        	<?php endif; ?>   
        </tbody>
    </table>
</div>