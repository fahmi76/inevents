<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-migrate-1.0.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-ui-1.10.0.custom.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.ui.touch-punch.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/modernizr.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.cookie.js"></script>

<script src='<?php echo base_url(); ?>assets/dashboard/js/fullcalendar.min.js'></script>

<script src='<?php echo base_url(); ?>assets/dashboard/js/jquery.dataTables.min.js'></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/excanvas.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.flot.stack.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.flot.resize.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.chosen.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.cleditor.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.noty.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.elfinder.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.raty.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.iphone.toggle.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.uploadify-3.1.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.gritter.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.imagesloaded.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.masonry.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.knob.modified.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery.sparkline.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/counter.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/retina.js"></script>

<script src="<?php echo base_url(); ?>assets/dashboard/js/custom.js"></script>
<script>
$(document).ready(function() {

  var oTable = $('#example').dataTable();

  // Sort immediately with column 2 (at position 1 in the array (base 0). More could be sorted with additional array elements
  oTable.fnSort( [ [1,'asc'] ] );

  // And to sort another column descending (at position 2 in the array (base 0).
  oTable.fnSort( [ [2,'desc'] ] );
} );

</script>