<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=statistik-event-spot.xls");
 
?>
<html>
<head>
    <style>
        body {
            font-family: Arial;
        }
        table {
            border-collapse: collapse;
        }
        th {
            background-color: #cccccc;
        }
        th, td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
<?php
		$places = $this->db->get_where('places', array('id' => $event))->row();
?>
<div class="tablecontenttop">
    <h4 class="titletable">Statistik on <?php echo $places->places_name; ?></h4>
</div>

    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>Report</strong>
                </th>
<?php
		$startTime = strtotime($places->places_startdate); 
		$endTime = strtotime($places->places_duedate) - 86400; 
		$y = 1;
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i); // 2010-05-01, 2010-05-02, etc
?>
                <th>
                    <strong>Day <?php echo $y; ?></strong>
                </th>
<?php
			$y++;
		}
?>
                <th>
                    <strong>Total</strong>
                </th>
            </tr>
        </thead>

        <tbody>
		
			<tr class="oddrow">
			<td class="firstcol">Registration</td>
<?php
		//registrasi
		$y = 0;
		
		$this->db->select('DISTINCT(date(landing_joindate)) AS thedate, count(distinct(account_id)) as total');
        $this->db->where("landing_register_form", $places->id);
        $this->db->where_not_in("account_id", array("12209","1230","28171","26653","14334","24797","28175"));
		$this->db->where("landing_joindate between '$places->places_startdate' and '$places->places_duedate' ");
        $this->db->from('landing');
		$this->db->group_by("thedate"); 
		$this->db->order_by("thedate", "asc");
        $total = $this->db->get()->result();

		foreach ($total as $user) {
			?>
			<td class="dataitem"><?php echo $user->total; ?></td>
<?php
		}

		$this->db->select('count(distinct(account_id)) as total');
        $this->db->where("landing_register_form", $places->id);
		$this->db->where("landing_joindate between '$places->places_startdate' and '$places->places_duedate' ");
        $this->db->where_not_in("account_id", array("12209","1230","28171","26653","14334","24797","28175"));
        $this->db->from('landing');
        $clear = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $clear->total; ?></td>
			</tr> 
		 </tbody>

        <tbody>

<?php
		//update status
        $this->db->select('');
        $this->db->where('places_parent', $places->id);
        $this->db->where('places_status', 1);
        $this->db->order_by("id", "desc");
        $this->db->from('places');
        $family = $this->db->get()->result();
		#$family = $this->convert->family($places->id);

		if(count($family) >= 1){
			foreach($family as $id){
				$places1 = $this->db->get_where('places', array('id' => $id->id))->row();
?>
        <tbody>
		
			<tr class="oddrow">
			<td class="firstcol">Update status di <?php echo $places1->places_name; ?></td>
<?php
				$y = 1;
				for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
					$thisDate = date('Y-m-d', $i); 
					$this->db->select('count(id) as total');
					$this->db->where_not_in("account_id", array("12209","1230","28171","26653","14334","24797","28175"));
					$this->db->where("log.places_id", $places1->id);
					$this->db->where("log.log_stamps like", '%'.$thisDate.'%');
					$this->db->from('log');
					$total = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
<?php
					$y++;
				}

				$this->db->select('count(id) as total');
				$this->db->where_not_in("account_id", array("12209","1230","28171","26653","14334","24797","28175"));
				$this->db->where("log.places_id", $places1->id);
				$this->db->where("log.log_stamps >", $places1->places_startdate. ' 00:00:00');
				$this->db->where("log.log_stamps <=", $places1->places_duedate. ' 23:59:00');
				$this->db->from('log');
				$total = $this->db->get()->row();
?>
			<td class="dataitem"><?php echo $total->total; ?></td>
			</tr> 
		 </tbody>
        <tbody>
<?php
			}
		}
?>

    </table>

<?php

	//Top Ten Update Status

		if(count($family) >= 1){
			foreach($family as $id){
				$places1 = $this->db->get_where('places', array('id' => $id->id))->row();

				$startTime = strtotime($places->places_startdate); 
				$endTime = strtotime($places->places_duedate); 

				$y = 1;
				for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
					$thisDate = date('Y-m-d', $i); 
					$this->db->select('wooz_account.account_email, wooz_account.account_displayname, wooz_account.account_fbid, count( wooz_log.`account_id` ) AS total');
					$this->db->join('account', 'account.id = log.account_id', 'left');
					#$this->db->where_not_in("account_id", array("12209","1230","28171","26653","14334","24797","28175"));
					$this->db->where("log.log_stamps like", '%'.$thisDate.'%');
					$this->db->where("log.places_id", $places1->id);
					$this->db->from('log');
					#$this->db->having('total > 1'); 
					$this->db->order_by("total", "desc");
					$this->db->group_by('account.account_displayname');
					#$this->db->limit(30);
					$total = $this->db->get()->result();
			
					if(!empty($total)){
						echo '</br>';
						echo '<font size=4>Top Ten People Update Status on '.$places1->places_name.' Day '.$y.' - '.$thisDate.'</font>';
						echo '<table border="1">';
						echo '<tr>';
						echo '<th>No</th>';
						echo '<th>Nama</th>';
						echo '<th>Email</th>';
						echo '<th>Link Facebook</th>';
						echo '<th>Total</th>';
						echo '</tr>';
						$x = 1;
						foreach ($total as $row){
							echo '<td>'.$x.'</td>';
							echo '<td>'.$row->account_displayname.'</td>';
							echo '<td>'.$row->account_email.'</td>';
							echo '<td><a href="https://www.facebook.com/profile.php?id='.$row->account_fbid.'">https://www.facebook.com/profile.php?id='.$row->account_fbid.'</a></td>';
							echo '<td>'.$row->total.'</td></tr>';
							$x++;
						}
					}
					echo '</table>';
					$y++;
				}

				$this->db->select('wooz_account.account_email, wooz_account.account_displayname, wooz_account.account_fbid, count( wooz_log.`account_id` ) AS total');
				$this->db->join('account', 'account.id = log.account_id', 'left');
				$this->db->where_not_in("account_id", array("12209","1230","28171","26653","14334","24797","28175"));
				$this->db->where("log.places_id", $places1->id);
				$this->db->from('log');
				$this->db->having('total > 1'); 
				$this->db->order_by("total", "desc");
				$this->db->group_by('account.account_displayname');
				#$this->db->limit(30);
				$total = $this->db->get()->result();
				if(!empty($total)){
					echo '</br>';
					echo '<font size=4>Top Ten People Update Status on '.$places1->places_name.'</font>';
					echo '<table border="1">';
					echo '<tr>';
					echo '<th>No</th>';
					echo '<th>Nama</th>';
					echo '<th>Email</th>';
					echo '<th>Link Facebook</th>';
					echo '<th>Total</th>';
					echo '</tr>';
					$x = 1;
					foreach ($total as $row){
						echo '<td>'.$x.'</td>';
						echo '<td>'.$row->account_displayname.'</td>';
						echo '<td>'.$row->account_email.'</td>';
						echo '<td><a href="https://www.facebook.com/profile.php?id='.$row->account_fbid.'">https://www.facebook.com/profile.php?id='.$row->account_fbid.'</a></td>';
						echo '<td>'.$row->total.'</td></tr>';
						$x++;
					}
				}
				echo '</table>';

			}
		}

	
?>

</body>
</html>