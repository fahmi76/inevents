<div class="nav-collapse sidebar-nav">
    <ul class="nav nav-tabs nav-stacked main-menu">
        <li><a href="<?php echo base_url() . 'dashboard'; ?>"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
        <li><a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> User</span></a></li>
        <li><a href="<?php echo base_url() . 'dashboard/visitor'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> Visitor</span></a></li>
        <li><a href="<?php echo base_url() . 'dashboard/gate'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> Gate</span></a></li>
        <li><a href="<?php echo base_url() . 'dashboard/winner'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> Winner</span></a></li>
        <!--
        <li><a href="<?php echo base_url() . 'dashboard/seat'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> Seat</span></a></li>
        <li><a href="<?php echo base_url() . 'dashboard/merchant'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> Merchant</span></a></li>
        li><a href="<?php echo base_url() . 'dashboard/visitor/rfid'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> RFID Visitor</span></a></li>
        -->
		<li><a href="<?php echo base_url() . 'dashboard/insight'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> Insight</span></a></li>
        
		<?php if($admin_group == 9 || $admin_group == 10): ?>
        <li><a href="<?php echo base_url() . 'dashboard/admin'; ?>"><i class="icon-user"></i><span class="hidden-tablet"> Admin</span></a></li>
        <?php endif; ?>
		<li>
			<a class="dropmenu" href="#"><i class="icon-list-alt"></i><span class="hidden-tablet"> List Gate</span></a>
			<ul>
				<li><a class="submenu" href="<?php echo base_url() . 'dashboard/gate/vendor'; ?>"><i class="icon-file-alt"></i><span class="hidden-tablet"> Staff Success</span></a></li>
				<li><a class="submenu" href="<?php echo base_url() . 'dashboard/gate/vendor/failed'; ?>"><i class="icon-file-alt"></i><span class="hidden-tablet"> Staff Failed</span></a></li>
				<li><a class="submenu" href="<?php echo base_url() . 'dashboard/gate/backstage'; ?>"><i class="icon-star"></i><span class="hidden-tablet"> Backstage Success</span></a></li>
				<li><a class="submenu" href="<?php echo base_url() . 'dashboard/gate/backstage/failed'; ?>"><i class="icon-star"></i><span class="hidden-tablet"> Backstage Failed</span></a></li>
			</ul>	
		</li>
    </ul>
</div>