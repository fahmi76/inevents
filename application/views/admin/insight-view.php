<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
	<div align="right"><a href="<?php echo base_url(); ?>admin/insightviewstat/<?php echo $location; ?>">view statistik this event</a> </div>
</div>
<!-- /Title Page -->
<div style="padding-left:0px; padding-bottom:5px;">
    <div id="tabs">
        <div style="float:left; width:200px; padding-left: 10px;">
            <h4><?php echo $title; ?></h4>
        </div>
    </div>
    <div class="clear"></div>

    <div class="tab_bdr"></div>
    <div class="panel" id="panel3" style="display: block;">
        <!-- form panel -->
        <div class="formpanel">

            <!-- large input -->
            <h3 class="ins">Events / Places : <?php echo $datalist->places_name; ?></h3>
            <p>
                <?php if ($datalist->places_desc != '')
                    echo $datalist->places_desc; ?>
            </p>
            <!-- /large input -->

            <?php
                /* total */
                $gen = array('male' => 0, 'female' => 0, 'unk' => 0);
                $age = array('40plus' => 0, '30-40' => 0, '26-30' => 0, '21-25' => 0, 'under' => 0, 'unk' => 0);
                $int = array();
                $snd = array();
                $crs = array();
                $c = 0;
                $d = 0;
                $e = 0;
				$y = 0;
				$z = 0;

                $xx = '';
                $yy = '';

                $data_inter = array();
				$data_ctgry = array();

                /* per data user processing */
                foreach ($data as $row) {
                    #$about = json_decode($row->fb_about);
                    $inter = json_decode($row->fb_interest, true);
                    $likes = json_decode($row->fb_likes, true);

                    $friend[] = $row->fb_friends;

                    /* interest */
					if (isset($inter['data']) && count($inter['data']) != 0 ) {
                        foreach ($inter['data'] as $interest) {
							if(isset($interest['name']) && $interest['name'] != "")
                            $data_inter[] = strip_tags(trim($interest['name']));
							if(isset($interest['category']) && $interest['category'] != "")
                            $data_ctgry[] = strip_tags(trim($interest['category']));
                        }
						/*
                        $yy = implode(' ', $data_inter);
                        $xx .= ' ' . $yy;
						*/
						$y++;
                    }

                    /* likes */
					if (isset($likes['data']) && count($likes['data']) != 0 ) {
                        foreach ($likes['data'] as $like) {
							if(isset($like['name']) && $like['name'] != "")
                            $like_inter[] = strip_tags(trim($like['name']));
							if(isset($like['category']) && $like['category'] != "")
                            $like_ctgry[] = strip_tags(trim($like['category']));
                        }
						/*
                        $yy = implode(' ', $like_inter);
                        $xx .= ' ' . $yy;
						*/
						$z++;
                    }

					/* gender 
                    if (isset($about->gender)) {
                        if ($about->gender == 'male')
                            $gen['male']++;
                        else
                            $gen['female']++;
                    }*/

                    /* age 
                    if (isset($about->birthday)) {
						$a = explode('/', $about->birthday);
						$a = $a[2] . '-' . $a[0] . '-' . $a[1];
						if ($this->fungsi->age($a) > 39)
							$age['40plus']++;
						elseif ($this->fungsi->age($a) > 30)
							$age['30-40']++;
						elseif ($this->fungsi->age($a) > 25)
							$age['26-30']++;
						elseif ($this->fungsi->age($a) > 20)
							$age['21-25']++;
						elseif ($this->fungsi->age($a) < 21)
							$age['under']++;
                    } else {
                        $age['unk']++;
					}*/
                }

                foreach($user as $about){
					$twfriend[] = $about->landing_tw_friends;
                    if (isset($about->account_gender)) {
                        if ($about->account_gender == 'male')
                            $gen['male']++;
                        else
                            $gen['female']++;
                    }else {
						$gen['unk']++;
					}
                    if (isset($about->account_birthdate)) {
						if ($this->fungsi->age($about->account_birthdate) > 39)
							$age['40plus']++;
						elseif ($this->fungsi->age($about->account_birthdate) > 30)
							$age['30-40']++;
						elseif ($this->fungsi->age($about->account_birthdate) > 25)
							$age['26-30']++;
						elseif ($this->fungsi->age($about->account_birthdate) > 20)
							$age['21-25']++;
						elseif ($this->fungsi->age($about->account_birthdate) < 21)
							$age['under']++;
                    } else {
                        $age['unk']++;
					}                    
                }

                $config = array(
                    'min_font' => 12,
                    'max_font' => 45,
                    'html_start' => '',
                    'html_end' => '',
                    'shuffle' => FALSE,
                    'class' => 'tag1',
                    'match_Class' => 'bold',
                );

				$tag1 = $this->fungsi->insight_break($data_inter);
                $inter_tag = $this->taggly->cloud($tag1, $config);
				
				$tag2 = $this->fungsi->insight_break($data_ctgry);
                $inter_cat = $this->taggly->cloud($tag2, $config);

				$tag3 = $this->fungsi->insight_break($like_inter);
                $likes_tag = $this->taggly->cloud($tag3, $config);

				$tag4 = $this->fungsi->insight_break($like_ctgry);
                $likes_cat = $this->taggly->cloud($tag4, $config);

			?>

                <h3 class="ins">Statistics</h3>
                <p>
                    Total registration at venue : <?php echo $totaluser;?>
				<br />Total who tap at venue : <?php echo count($data);?>
                <br />Total tap in : <?php echo $action['all'];?>
                <br />Total Facebook update : <?php echo $action['fb'];?>
                <br />Total Twitter update : <?php echo $action['tw'];?>
                <br />Total Foursquare update : <?php echo $action['fs'];?>
				<?php if($rfid != 0): ?>
				<br />Total RFID tag event : <?php echo $rfid;?>
				<br />Presentase RFID tag event : <?php $total = ($totaluser/$rfid)*100; echo $total.'%';?>
				<br />Presentase Photo tag event : <?php $total = (count($data)/$totaluser)*100; echo $total.'%';?>
				<?php endif; ?>
                </p>

                <h3 class="ins">Photo Booth</h3>
                <p>
					Total photo taken : <?php echo $photos['photo'];?>
                <br />Total who initialize photo taking : <?php echo $photos['user'];?>
                </p>

                <h3 class="ins">Gender</h3>
                <p>
					Male : <?php echo $gen['male'];?>
                <br />Female : <?php echo $gen['female'];?>
				 <br />Unknown : <?php echo $gen['unk'];?>
                </p>

				<h3 class="ins">Age Range</h3>
                <p>
					Age 40 ++ : <?php echo $age['40plus'];?>
                <br />Age 31 - 40 : <?php echo $age['30-40'];?>
                <br />Age 26 - 30 : <?php echo $age['26-30'];?>
                <br />Age 21 - 25 : <?php echo $age['21-25'];?>
                <br />Under 21 : <?php echo $age['under'];?>
                <br />Unknown : <?php echo $age['unk'];?>
                </p>

                <h3 class="ins">Friends</h3>
                <p>
					updates via Facebook reached <?php echo array_sum($friend);?> friends
				<br />updates via Twitter reached <?php echo array_sum($twfriend);?> friends
                </p>

                <h3 class="ins">URL link photobooth</h3>
                <p>
				<a href="http://wooz.in/backups/woozin-photobooth-<?php echo $datalist->places_landing ?>.zip">http://wooz.in/backups/woozin-photobooth-<?php echo $datalist->places_landing ?>.zip</a>
				</p>

				<h3 class="ins">Interest (by Name, taken from <?php echo $y; ?> user)</h3>
                <p class="tags">
                <?php echo $inter_tag; ?>
				</p>

				<h3 class="ins">Likes (by Name, taken from <?php echo $z; ?> user)</h3>
                <p class="tags">
                <?php echo $likes_tag; ?>
				</p>

				<h3 class="ins">Interest (by Category, taken from <?php echo $y; ?> user)</h3>
                <p class="tags">
                <?php echo $inter_cat; ?>
				</p>

				<h3 class="ins">Likes (by Category, taken from <?php echo $z; ?> user)</h3>
                <p class="tags">
                <?php echo $likes_cat; ?>
				</p>

        </div>
        <!-- /form panel -->

    </div>
    <div class="clear"></div>
    <div id="tabsbottom"></div>

</div>

<!-- /form window -->

