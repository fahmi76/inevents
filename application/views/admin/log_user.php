<!-- Title Page -->
<div class="titlepage">
    <h3>User</h3>
</div>
<!-- /Title Page -->

<div class="tablecontenttop">

    <h4 class="titletable">User activity</h4></div>


<!-- table content middle -->
<div class="tablecontentmiddle">
    
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>No.</strong>
                </th>
                <th>
                    <strong>Nama</strong>
                </th>
                <th>
                    <strong>Email</strong>
                </th>
                <th>
                    <strong>Facebook</strong>
                </th>
                <th>
                    <strong>Twitter</strong>
            </tr>
        </thead>

        <tbody>
            <?php
            $x = 1;
            foreach ($data as $row):
                ?>
                <tr class="oddrow">
                    <td class="firstcol">
                        <?php echo $x; ?>
                    </td>
                    <td class="dataitem">
                        <?php echo $row->account_displayname; ?>
                    </td>
                    <td class="dataitem">
                        <?php echo $row->account_email; ?>
                    </td>
                    <td class="dataitem">
						<?php if($row->account_fbid): ?>
                        <?php echo 'https://www.facebook.com/'.$row->account_fbid; ?>
						<?php endif; ?>
                    </td>
                    <td class="dataitem">
						<?php if($row->account_tw_username): ?>
                        <?php echo '@'.$row->account_tw_username; ?>
						<?php endif; ?>
                    </td>
                </tr>  
                <?php
                $x++;
            endforeach;
            ?>  
        </tbody>
    </table>

</div>
<!-- table content middle   -->           
<div class="tablecontentbottom">&nbsp;</div> 

