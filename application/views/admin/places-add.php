<?php
echo initialize_tinymce();
?>
<script type="text/javascript">
    $(function() {
        $("#sdate").datepicker();
        $("#edate").datepicker();
    });
</script>

<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
</div>

<form name="addnews" enctype="multipart/form-data" method="post" action="<?php echo current_url() ?>">
    <?php if (validation_errors()) : ?>
        <div class="statusupdateError">
            <div class="statusupdateicon">
                <img src="<?php echo base_url() ?>assets/images/admin/warningError.png" alt="Error" />
            </div>
            <div class="statusupdatedesc">
                <ul>
                    <?php echo validation_errors('<li>', '</li>'); ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    <?php endif; ?>

    <div style="padding-left:0px; padding-bottom:20px;">

        <div id="tabs">
            <div style="float:left; width:270px; padding:7px 0 0 13px;">
                <h4><?php echo $title; ?></h4>
            </div>
        </div>
        <div class="clear"></div>

        <div class="tab_bdr"></div>
        <div class="panel" id="panel3" style="display: block;">
            <div class="formpanel">
                <p>
                    <strong>Spot Name</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="name" />
                    </label>
                </p>
                <p>
                    <strong>Email Address</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="emailaddress" />
                    </label>
                </p>
                <p>
                    <strong>Subject Email</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="subjectemail" />
                    </label>
                </p>
                <p>
                    <strong>Email Registration</strong><br/>
                    <label class="largeinput">
                        <textarea class="myTextEditor" cols="80" rows="10" name="isiemail"></textarea>
                    </label>
                </p>
                <p>                                
                    <strong>Spot Parent</strong><br/>
                    <select name="parent" class="listmenus">
                        <?php
                        $user_data = $this->db->get_where('account', array('id' => $admin))->row();
                        if ($user_data->account_group == '9') :
                            ?>
                            <option value="0" >Main Spot</option>
                        <?php endif; ?>
                        <?php
                        $this->db->select('');
                        $this->db->where('places_parent', 0);
                        if($admin == '10862')
                            $this->db->where('places_status =', 2);
                        else
                            $this->db->where('places_status !=', 0);
                        $this->db->from('places');
                        $select = $this->db->get()->result();
                        foreach ($select as $row) {
                            echo '<option value="' . $row->id . '" >' . $row->places_name . '</option>';
                        }
                        ?>
                    </select>
                </p>
                <p>
                    <strong>Spot Type</strong><br/>
                    <?php if ($admin == '10862'): ?>
                    <select name="type" class="listmenus">
                        <option value="1">Event</option>
                    </select>
                    <?php else: ?>
                    <select name="type" class="listmenus">
                        <option value="">No Type</option>
                        <option value="1">Event</option>
                        <option value="2">Place</option>
                        <option value="3">Private</option>
                        <option value="4">Photo Share</option>
                        <option value="5">Like Booth</option>
                        <option value="6">Photo Booth</option>
                    </select>
                    <?php endif; ?>
                </p>
                <p>
                    <strong>Spot Address</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="address" />
                    </label>
                </p>
                <p>
                    <strong>Start Date</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="sdate" id="sdate" />
                    </label>
                </p>
                <p>
                    <strong>End Date</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="edate" id="edate" />
                    </label>
                </p>
                <p>
                    <strong>Spot Description</strong><br/>
                    <label class="textarea">
                        <textarea cols="80" rows="10" name="isi"></textarea>
                    </label>
                </p>
                <p>
                    <strong>Custom Status Facebook</strong> (200 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="cstatus_fb" maxlength="900" />
                    </label>
                </p>
                <p>
                    <strong>Custom Status Twitter</strong> (140 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="cstatus_tw" maxlength="140" />
                    </label>
                </p>
                <p>
                    <strong>Custom Twitter Direct Message</strong> (100 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="cstatus_dm" maxlength="100" />
                    </label>
                </p>
                <p>
                    <strong>Spot Icon</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" id="icon" name="icon" onclick="openFileBrowser('icon');" />
                    </label>
                </p>
                <p>
                    <strong>Spot Facebook Places ID</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="facebook" />
                    </label>
                </p>
                <p>
                    <strong>Spot Foursquare Category ID</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="foursquare" />
                    </label>
                </p>
                <p>
                    <strong>Spot Latitude</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="latitude" />
                    </label>
                </p>
                <p>
                    <strong>Spot Longitude</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="longitude" />
                    </label>
                </p>
                <p>
                    <strong>Spot Landing Page URL</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="landing" />
                    </label>
                </p>
                <p>
                    <strong>Facebook Page URL to like</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="facebook_page" />
                    </label>
                </p>
                <p>
                    <strong>Twitter Username to follow</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="twitter_username" />
                    </label>
                </p>
                <p>
                    <strong>Photobooth Frame URL</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" id="frame" name="frame" onclick="openFileBrowser('frame');" />
                    </label>
                </p>
               <p>
                    <strong>Photobooth Album Facebook</strong> (200 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="fb_album" maxlength="200" />
                    </label>
                </p>
                <p>
                    <strong>Photobooth Custom Status Facebook</strong> (200 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="photo_cstatus_fb" maxlength="200" />
                    </label>
                </p>
                <p>
                    <strong>Photobooth Custom Status Twitter</strong> (110 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="photo_cstatus_tw" maxlength="110" />
                    </label>
                </p>
                <p>
                    <strong>Spot Landing Page Custom CSS</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <textarea cols="80" rows="15" name="css"></textarea>
                    </label>
                </p>
                <p>
                    <input type="image" src="<?php echo base_url(); ?>assets/images/admin/button-submit.gif" name="submit"/>
                </p>
            </div>
        </div>
        <div class="clear"></div>
        <div id="tabsbottom"></div>
    </div>
</form>
