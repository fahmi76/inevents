<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Show</a></li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span9">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon list"></i><span class="break"></span>User</h2>
            </div>
            <div class="box-content">
                <dl>
                    <dt>Organization</dt>
                    <dd><span class="break">name : </span><b><?php echo $data->account_displayname; ?></b></dd>
                    <dd><span class="break">email : </span><b><?php echo $data->account_email; ?></b></dd>
                    <dd><span class="break">Password : </span><b><?php echo $this->fungsi->encrypt_decrypt('decrypt',$data->account_passwd); ?></b></dd>
                </dl>           
                <a href="<?php echo base_url() . 'dashboard/merchant'; ?>">
                    <input type="button" class="btn" value="Back" />
                    <!--
                    <button class="btn">Cancel</button> -->
                </a> 
            </div>
        </div>
    </div>
</div>