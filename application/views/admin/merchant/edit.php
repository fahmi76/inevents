<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Edit User</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit User</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal"  onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">                        
                            <label class="control-label" for="focusedInput">Name</label>
                            <?php
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } elseif (!$datauser) {
                                $names = '';
                            } else {
                                $names = $datauser->account_displayname;
                            }
                            ?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>"> 
                            <label class="control-label" for="focusedInput">Email</label>
                            <?php
                            $email = set_value('email');
                            if ($email) {
                                $emails = $email;
                            } elseif (!$datauser) {
                                $emails = '';
                            } else {
                                $emails = $datauser->account_email;
                            }
                            ?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="email" autocomplete="off" value="<?php echo $emails; ?>" id="focusedInput" type="text" placeholder="Email@email.com">
                                <?php echo form_error('email', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>      
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">                        
                            <label class="control-label" for="focusedInput">Password</label>
                            <?php
                            $rfid = set_value('password');
                            if ($rfid) {
                                $rfids = $rfid;
                            } elseif (!$datauser) {
                                $rfids = '';
                            } else {
                                $rfids = $this->fungsi->encrypt_decrypt('decrypt',$datauser->account_passwd);
                            }
                            ?>
                            <div class="controls">
                                <input class="input-xlarge focused" name="password" autocomplete="off" id="focusedInput" type="text" value="<?php echo $rfids; ?>" placeholder="password">
                                <?php echo form_error('password', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  			
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     