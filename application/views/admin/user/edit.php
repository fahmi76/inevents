<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Edit User</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit User</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <?php if (validation_errors()) : ?>
                    <p class="error">Whoops ! Something wrong
                        <?php echo validation_errors('<br />&gt; ', ''); ?>
                    </p>
                <?php endif; ?> 
                <form class="form-horizontal"  onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>

                        <div class="control-group <?php if (form_error('name')) : ?> error<?php endif; ?>">          
                            <?php
                            $name = set_value('name');
                            if ($name) {
                                $names = $name;
                            } elseif (!$datauser) {
                                $names = '';
                            } else {
                                $names = $datauser->account_displayname;
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="Name">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>    
                        <div class="control-group <?php if (form_error('visitor')) : ?> error<?php endif; ?>">
                            <label class="control-label">Visitor Access</label>
                            <div class="controls">
                                <?php
                                $visitor1 = set_value('visitor');
                                if ($visitor1) {
                                    $visitors = $visitor1;
                                } elseif (!$datauser) {
                                    $visitors = '';
                                } else {
                                    $visitors = $datauser->visitor_id;
                                } 
                                ?>
                                <?php foreach($visitor as $row): ?>
                                    <label class="radio">
                                        <input type="radio" name="visitor" id="optionsRadios1" value="<?php echo $row->id; ?>" <?php if ($visitors == $row->id) : ?>checked=""<?php endif; ?>>
                                        <?php echo $row->nama; ?>
                                    </label>
                                    <div style="clear:both"></div>
                                <?php endforeach ; ?>
                                <?php echo form_error('visitor', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div> 
                        <div class="control-group <?php if (form_error('seat')) : ?> error<?php endif; ?>">                 
                            <label class="control-label" for="focusedInput">Seat</label>
                            <?php
                            $seat = set_value('seat');
                            if ($seat) {
                                $seats = $seat;
                            } elseif (!$datauser) {
                                $seats = '';
                            } else {
                                $seats = $datauser->seat;
                            }
                            ?>  
                            <div class="controls">
                                <input class="input-xlarge focused" name="seat" autocomplete="off" id="focusedInput" type="text" value="<?php echo $seats; ?>" placeholder="Seat">
                                <?php echo form_error('seat', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>   
                        <div class="control-group">                        
                            <label class="control-label" for="focusedInput">RFID</label>
                            <?php
                            $rfid = set_value('rfid');
                            if ($rfid) {
                                $rfids = $rfid;
                            } elseif (!$datauser) {
                                $rfids = '';
                            } else {
                                $rfids = $datauser->account_rfid;
                            }
                            ?>  
                            <div class="controls">
                                <input class="input-xlarge focused" name="rfid" autocomplete="off" id="focusedInput" type="text" value="<?php echo $rfids; ?>" placeholder="rfid">
                                <?php echo form_error('rfid', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>   			
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     