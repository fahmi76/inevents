<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user/home/deleteall'; ?>"><i class="halflings-icon trash"></i></a>
                    <a href="<?php echo base_url() . 'dashboard/user/eksport'; ?>"><i class="halflings-icon upload"></i></a>
                    <a href="<?php echo base_url() . 'dashboard/user/add'; ?>"><i class="halflings-icon plus"></i></a>
                </div>

            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Access</th>
                            <th>Seat</th>
                            <th>RFID</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname ?></td>
                                <td class="center"><?php echo $row->nama; ?></td>
                                <td class="center"><?php echo $row->seat; ?></td>
                                <td class="center"><?php echo $row->account_rfid; ?></td>
                                <td class="center"><?php if($row->account_status == 3){echo 'Arrived - Checked In';}elseif($row->account_status == 4){echo 'Arrived - Checked Out';}else{echo 'registration';} ?></td>
                                <td class="center">
                                    <a class="btn btn-success" href="<?php echo base_url(); ?>dashboard/user/home/show/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white zoom-in"></i>  
                                    </a>
                                    <a class="btn btn-info" href="<?php echo base_url(); ?>dashboard/user/edit/main/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="<?php echo base_url(); ?>dashboard/user/home/delete/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->