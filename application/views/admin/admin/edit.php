<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Edit User</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit User</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/user'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
			<div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">                        
                            <label class="control-label" for="focusedInput">Name</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="name" autocomplete="off" id="focusedInput" type="text" value="<?php echo $datauser->account_displayname; ?>" placeholder="Name">
                                <?php echo form_error('name', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
			<div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>"> 
                            <label class="control-label" for="focusedInput">Email</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="email" autocomplete="off" value="<?php echo $datauser->account_email; ?>" id="focusedInput" type="text" placeholder="Email@email.com">
                                <?php echo form_error('email', '<span class="help-inline">', '</span>'); ?>
                           </div>
                        </div>      
			<div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>"> 
                            <label class="control-label" for="focusedInput">Password</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="password" autocomplete="off" id="focusedInput" type="text" placeholder="Password">
                                <?php echo form_error('password', '<span class="help-inline">', '</span>'); ?>
                           </div>
                        </div>    
                        <div class="control-group">
                            <label class="control-label">Role</label>
                            <div class="controls">
                                <label class="checkbox inline">
                                    <input type="checkbox" name="role" id="inlineCheckbox1" value="9"> Super Admin
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" name="role" id="inlineCheckbox2" value="8"> Admin
                                </label>
                            </div>
                        </div>    
                        <div class="control-group">
                            <label class="control-label">Role</label>
                            <div class="controls">
                                <label class="checkbox inline">
                                    <input type="checkbox" name="status" id="inlineCheckbox1" value="1"> Active
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" name="status" id="inlineCheckbox2" value="2"> Banned
                                </label>
                            </div>
                        </div>       			
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     