<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add RFID</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Add RFID</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/visitor/rfid'; ?>"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data" method="post" role="form" action="<?php echo current_url(); ?>">
                    <fieldset>
                        <div class="control-group <?php if (validation_errors()) : ?> error<?php endif; ?>">          
                            <?php
                            $name = set_value('rfid');
                            if ($name) {
                                $names = $name;
                            } else {
                                $names = '';
                            }
                            ?>                  
                            <label class="control-label" for="focusedInput">RFID</label>
                            <div class="controls">
                                <input class="input-xlarge focused" name="rfid" autocomplete="off" id="focusedInput" type="text" value="<?php echo $names; ?>" placeholder="RFID">
                                <?php echo form_error('rfid', '<span class="help-inline">', '</span>'); ?>
                            </div>
                        </div>  
                        <div class="control-group">
                            <label class="control-label">Visitor Category</label>
                            <div class="controls">
                                <?php foreach($visitor as $row): ?>
                                    <label class="radio">
                                        <input type="radio" name="visitor" id="optionsRadios1" value="<?php echo $row->id; ?>" checked="">
                                        <?php echo $row->nama; ?>
                                    </label>
                                    <div style="clear:both"></div>
                                <?php endforeach ; ?>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <a href="<?php echo base_url() . 'dashboard'; ?>">
                                <input type="button" class="btn" value="Cancel" />
                                <!--
                                <button class="btn">Cancel</button> -->
                            </a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->

<!-- end: Content -->                     