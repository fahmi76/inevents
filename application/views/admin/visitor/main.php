<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Visitor Category</h2>
                <div class="box-icon">
                    <a href="<?php echo base_url() . 'dashboard/visitor/add'; ?>"><i class="halflings-icon plus"></i></a>
                </div>

            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Gate</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->nama ?></td>
                                <td>
                                    <?php 
                                        $gate = json_decode($row->gate_id); 
                                        $i = 0;
                                        foreach($gate as $data_gate){
                                            $list = $this->visitor_m->get_gate($data_gate);
                                            echo $list->nama;
                                            $i++;
                                            if($i < count($gate)){
                                                echo ',';
                                            } 
                                        }
                                    ?>
                                </td>
                                <td class="center">
                                    <a class="btn btn-info" href="<?php echo base_url(); ?>dashboard/visitor/edit/main/<?php echo $row->id; ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->