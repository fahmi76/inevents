<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List Gate <?php echo $check; ?> <?php if($status == 1){echo 'Check-in';}elseif($status == 2){echo 'Check-out';}else{echo 'Arrive';}?></h2>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable" id="example">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <!--
                            <th>Access</th>
                            -->
                            <th>Status</th>
                            <th>Time</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname ?></td>
                                <!--
                                <td class="center"><?php echo $row->nama; ?></td>
                                -->
                                <td class="center">
                                    <?php if($row->log_check == 2): ?>
                                    <span class="label label-error">Check-out</span>
                                    <?php else: ?>
                                    <span class="label label-success">Check-in</span>
                                    <?php endif; ?>
                                </td>
                                <td><?php echo $row->log_stamps ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->
