<div class="statusupdateOK">
    <div class="statusupdateicon"><img src="<?php echo base_url()?>assets/images/admin/warningError.png" alt="Error" /></div>
    <div class="statusupdatedesc"><?php echo $error; ?></div>
    <div class="clear"></div>
</div>
<meta http-equiv="refresh" content="1;URL=<?php echo site_url('admin/'.$parent); ?>">
