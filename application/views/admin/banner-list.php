
<script type="text/javascript">
    <!--
    function confirmDelete(delUrl)
    {
        if (confirm("Are you sure you want to delete")) {
            document.location = delUrl;
        }
    }
	
	function confirmActive(actUrl)
    {
        if (confirm("Are you sure you want to active this banner?")) {
            document.location = actUrl;
        }
    }
    //-->
</script>
<script type="text/javascript">
    function resubmit()
    {
        document.filter.action="<?php echo current_url() ?>";
        document.filter.submit();
    }
</script>

<!-- tabs -->
<script language="javascript" type="text/javascript">
	var panels = new Array('panel1', 'panel2', 'panel3');
	var tabs = new Array('tab1', 'tab2', 'tab3');
	function displayPanel(nval){
		for(i = 0; i < panels.length; i++){
			document.getElementById(panels[i]).style.display = (nval-1 == i) ? 'block':'none';
			document.getElementById(tabs[i]).className=(nval-1 == i) ? 'tab_sel':'tab';
		}
	}
</script>
<!-- /tabs -->

<script type="text/javascript">
$(document).ready(function(){
	$('table').show(function(){ $(this).fadeIn(); });
	
	$("#tab2").click(function(){
		var urlevent = '<?php echo site_url(); ?>/admin/bannerevent';
		$('table').hide();
		$("#panel2").load(urlevent+" div.tablecontentmiddle",function(){
			$(this).ajaxStart(function(){
				$('table').hide();
			});
			
			$(this).ajaxStop(function(){
				$('table').show(function(){ $(this).fadeIn(); });
			});
		});
		$("div.pages").hide();
		
	});
	
	$("#tab1").click(function(){
		var urlother = '<?php echo site_url(); ?>/admin/bannerother';
		$('table').hide();
		$("#panel1").load(urlother+" div.tablecontentmiddle",function(){
			$(this).ajaxStart(function(){
				$('table').hide();
			});
			
			$(this).ajaxStop(function(){
				$('table').show(function(){ $(this).fadeIn(); });
			});
		});
		$(".pages").hide();
	});
	
	$("#tab3").click(function(){
		var urlcur = '<?php echo site_url(); ?>/admin/banner';
		$('table').hide();
		$("#panel3").load(urlcur+" div.tablecontentmiddle",function(){
			$(this).ajaxStart(function(){
				$('table').hide();
			});
			
			$(this).ajaxStop(function(){
				$('table').show(function(){ $(this).fadeIn('slow'); });
			});			
		});
		$(".pages").show();
	});
});
</script>
<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
    <h4><a href="<?php echo site_url('admin/banneradd')?>">Add new <?php echo $modules; ?></a></h4>
</div>
<!-- /Title Page -->

<div id="tabs">
    <div class="tabtitletable">
        <h4 class="titletable"><?php echo $title; ?></h4>
    </div>
    <div id="tab1" class="tab" style="margin-left: 5px;" onclick="javascript: displayPanel('1');" align="center">Side Banner</div>
    <div id="tab2" class="tab" style="margin-left: 5px;" onclick="javascript: displayPanel('2');" align="center">Event Banner</div>
    <div id="tab3" class="tab_sel" style="margin-left: 5px;" onclick="javascript: displayPanel('3');" align="center">All Banner</div>
	<div class="clear"></div>
</div>

<div class="tab_bdr"></div>

<div class="panel" id="panel3" style="display: block;">
<!-- table content middle -->
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th><strong>No.</strong></th>
                <th><strong>Image</strong></th>
                <th><strong>Title</strong></th>
                <th><strong>Category</strong></th>
                <th><strong>Action</strong></th>
            </tr>
        </thead>
        <?php if($num != 0) : ?>
        <tbody>
            <?php $no = 1; foreach($databanner as $dbanner) : ?>
			<tr class="oddrow">
                <td class="firstcol"><?php echo $no++; ?>.</td>
                <td class="dataitem">
                	<img src="<?php echo base_url();?>uploads/banner/<?php echo $dbanner->banner_image; ?>" width="264" alt="" />
                    <?php if($dbanner->banner_status == 1) : ?>
                    	<p>Status : <strong>Active</strong></p>
                    <?php else : ?>
                    	<p>Status : <strong>Not Active</strong></p>
                    <?php endif; ?>
                </td>
                <td class="edititem" style="width: 200px;"><?php echo stripslashes($dbanner->banner_title); ?></td>
                <td class="edititem"><?php echo stripslashes($dbanner->banner_cat); ?></td>
                <td class="edititem">
                    <div class="iconedit">
                        <a href="<?php echo site_url(); ?>/admin/banneredit/<?php echo $dbanner->id; ?>">edit</a>
                    </div>
                    <div class="icondelete">
                        <a href="javascript:confirmDelete('<?php echo site_url(); ?>/admin/bannerdel/<?php echo $dbanner->id;?>')">delete</a>
                    </div>
                    <div class="iconedit">
                        <a href="javascript:confirmActive('<?php echo site_url(); ?>/admin/banneractive/<?php echo $dbanner->id;?>')">activate</a>
                    </div>
                </td>
            </tr>
            <?php endforeach; else: ?>
        	<tr class="oddrow">
                <td class="firstcol">1.</td>
                <td class="dataitem">No Data</td>
                <td class="edititem" style="width: 150px;">No Data</td>
                <td class="edititem">No Data</td>
                <td class="edititem">
                    <div class="iconedit">
                        <a href="#">No Data</a>
                    </div>
                </td>
            </tr>
        	<?php endif; ?>
        </tbody>
    </table>
</div>
<!-- table content middle -->        
</div>
<div class="panel" id="panel2" style="display: none;"></div>
<div class="panel" id="panel1" style="display: none;"></div>
<div class="clear"></div>
<div id="tabsbottom"></div>


<?php if(isset($tPaging)): ?>
<div class="pages"> 
        <?php echo $tPaging;?>
    <div class="clear"></div>
</div>
<?php endif; ?>
