<script type="text/javascript">
    <!--
    function confirmDelete(delUrl)
    {
        if (confirm("Are you sure you want to delete")) {
            document.location = delUrl;
        }
    }
    //-->

    function resubmit()
    {
        document.filter.action="<?php echo current_url() ?>";
        document.filter.submit();
    }

</script>
<!-- Title Page -->
<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
    <h4><a href="#dialog" name="modal">Modify site information</a></h4>
</div>
<!-- /Title Page -->

<div id="tabs">
    <div class="tabtitletable">
        <h4 class="titletable"><?php echo $title; ?></h4>        
    </div>
    <div class="clear"></div>
</div>

<div class="tab_bdr"></div>
<?php if(count($setting) != 0) : ?>
<div class="tablecontentmiddle">
    <table class="tables">
        <thead>
            <tr>
                <th>
                    <strong>Label</strong>
                </th>
                <th>
                    <strong>Description</strong>
                </th>
            </tr>
        </thead>

        <tbody>
            <tr class="oddrow">
                <td class="dataitem" style="width: 30%;">Site Name</td>
                <td class="dataitem" style="width: 70%;"><?php echo $setting->site_name; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Site Description</td>
                <td class="dataitem"><?php echo $setting->site_description; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Site URL</td>
                <td class="dataitem"><?php echo $setting->site_url; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Mobile Site URL</td>
                <td class="dataitem"><?php echo $setting->site_mobile_url; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Default Status</td>
                <td class="dataitem"><?php echo $setting->site_status; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Facebook Application ID</td>
                <td class="dataitem"><?php echo $setting->facebook_id; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Facebook API Key</td>
                <td class="dataitem"><?php echo $setting->facebook_key; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Facebook API Secret Key</td>
                <td class="dataitem"><?php echo $setting->facebook_secret; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem" style="width: 30%;">Facebook Permissions</td>
                <td class="dataitem" style="width: 70%;"><?php echo $setting->facebook_perms; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Facebook Callback URL</td>
                <td class="dataitem"><?php echo $setting->facebook_callback; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Twitter API Key</td>
                <td class="dataitem"><?php echo $setting->twitter_key; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Twitter API Secret Key</td>
                <td class="dataitem"><?php echo $setting->twitter_secret; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Twitter Callback URL</td>
                <td class="dataitem"><?php echo $setting->twitter_callback; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Foursquare API Key</td>
                <td class="dataitem"><?php echo $setting->foursquare_key; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Foursquare API Secret Key</td>
                <td class="dataitem"><?php echo $setting->foursquare_secret; ?></td>
            </tr>
            <tr class="oddrow">
                <td class="dataitem">Foursquare Callback URL</td>
                <td class="dataitem"><?php echo $setting->foursquare_callback; ?></td>
            </tr>
        </tbody>
    </table>    
</div>
<?php else : ?>
<div class="panel notfound" id="panel3" style="display: block;">
    <h4>Data Not Found</h4>
</div>
<?php endif; ?>
<div id="boxes">
    <div id="dialog" class="window">
        <div style="padding: 2px 5px; text-align: right;">
            <a href="#" class="close">Close it</a>
        </div>
        <div id="dialogtitle">
            <h3><?php echo $modules; ?></h3>
        </div>
        <span style="padding: 5px;">
            <a href="#" id="linkbasic">basic setting</a> . <a href="#" id="linkfb">facebook setting</a> . <a href="#" id="linktw">twitter setting</a> . <a href="#" id="linkfs">foursquare setting</a>
        </span>
        <div id="formresult"></div>
        <form id="modalform" action="<?php base_url(); ?>index.php/admin/settingedit" method="post">            
            <div id="sitebasic">
                <p>
                    <strong>Site Name</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="site_name" name="site_name" <?php if (count($setting)!=0 && $setting->site_name != '') echo "value=\"" . $setting->site_name . "\""; else echo ""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Site Description</strong><br/>
                    <label class="textarea">
                        <textarea cols="66" rows="4" id="site_description" name="site_description" style="text-align: left;">
                            <?php if (count($setting)!=0 && $setting->site_description != '') echo trim($setting->site_description); ?>
                        </textarea>
                    </label>
                </p>
                <p>
                    <strong>Site URL</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="site_url" name="site_url" <?php if (count($setting)!=0 && $setting->site_url != '') echo "value=\"" . $setting->site_url . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Mobile Site URL</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="site_mobile_url" name="site_mobile_url" <?php if (count($setting)!=0 && $setting->site_mobile_url != '') echo "value=\"" . $setting->site_url . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Default Status</strong><br/>
                    <label class="textarea">
                        <textarea cols="66" rows="4" id="site_status" name="site_status">
                            <?php if (count($setting)!=0 && $setting->site_status != '') echo trim($setting->site_status); ?>
                        </textarea>
                    </label>
                </p>                 
            </div>
            <div id="sitefb">
                <p>
                    <strong>Facebook Application ID</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="facebook_id" name="facebook_id" <?php if (count($setting)!=0 && $setting->facebook_id != '') echo "value=\"" . $setting->facebook_id . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Facebook API Key</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="facebook_key" name="facebook_key" <?php if (count($setting)!=0 && $setting->facebook_key != '') echo "value=\"" . $setting->facebook_key . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Facebook API Secret Key</strong><br/>
                    <label class="largeinput">
                    <input type="text" style="width:90%;" id="facebook_secret" name="facebook_secret" <?php if (count($setting)!=0 && $setting->facebook_secret != '') echo "value=\"" . $setting->facebook_secret . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Facebook Permissions</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="facebook_perms" name="facebook_perms" <?php if (count($setting)!=0 && $setting->facebook_perms != '') echo "value=\"" . $setting->facebook_perms . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Facebook Callback URL</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="facebook_callback" name="facebook_callback" <?php if (count($setting)!=0 && $setting->facebook_callback != '') echo "value=\"" . $setting->facebook_callback . "\""; ?> />
                    </label>
                </p>
            </div>
            <div id="sitetw">
                <p>
                    <strong>Twitter API Key</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="twitter_key" name="twitter_key" <?php if (count($setting)!=0 && $setting->twitter_key != '') echo "value=\"" . $setting->twitter_key . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Twitter API Secret Key</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="twitter_secret" name="twitter_secret" <?php if (count($setting)!=0 && $setting->twitter_secret != '') echo "value=\"" . $setting->twitter_secret . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Twitter Callback URL</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="twitter_callback" name="twitter_callback" <?php if (count($setting)!=0 && $setting->twitter_callback != '') echo "value=\"" . $setting->twitter_callback . "\""; ?> />
                    </label>
                </p>
            </div>
            <div id="sitefs">
                <p>
                    <strong>Foursquare API Key</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="foursquare_key" name="foursquare_key" <?php if (count($setting)!=0 && $setting->foursquare_key != '') echo "value=\"" . $setting->foursquare_key . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Foursquare API Secret Key</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="foursquare_secret" name="foursquare_secret" <?php if (count($setting)!=0 && $setting->foursquare_secret != '') echo "value=\"" . $setting->foursquare_secret . "\""; ?> />
                    </label>
                </p>
                <p>
                    <strong>Foursquare Callback URL</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:90%;" id="foursquare_callback" name="foursquare_callback" <?php if (count($setting)!=0 && $setting->foursquare_callback != '') echo "value=\"" . $setting->foursquare_callback . "\""; ?> />
                    </label>
                </p>
            </div>
            <p>
                <input type="image" src="<?php echo base_url(); ?>assets/images/admin/button-submit.gif" name="submit" id="modalsubmit" />
            </p>
        </form>        
    </div>
    <div id="mask"></div>
</div>

