<!-- start: Content -->
<div id="content" class="span10">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>

    <div class="row-fluid">

        <div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $preregis->total; ?></div>
            <div class="title">User Pre-Registration</div>
        </div>
        <div class="span3 statbox black" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $arrive_event->total_account; ?></div>
            <div class="title">User Arrive at Event</div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/arrive"> read full report</a>
            </div>
        </div>
        <?php $x=1; foreach($checkin as $row): ?>
        <div class="span3 statbox blue" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $row['total']->total_account; ?></div>
            <div class="title">User Tap at Checkin <?php echo $row['nama']; ?></div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/checkin/<?php echo $row['id']; ?>/1/1"> read full report</a>
            </div>
        </div>
    	<?php $x++; endforeach; ?>
        <?php $y = $x - 1; foreach($checkout as $rows): ?>
        <div class="span3 statbox green" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $rows['total']->total_account; ?></div>
            <div class="title">User Tap at Checkout <?php echo $rows['nama']; ?></div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/checkin/<?php echo $rows['id']; ?>/1/1"> read full report</a>
            </div>
        </div>

    	<?php endforeach; ?>
    	<!--
        <div class="span3 statbox green" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $checkoutstaff->total_account; ?></div>
            <div class="title">User Tap at Checkout Staff</div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/checkin/11/2/1"> read full report</a>
            </div>
        </div>
		-->
    </div>			

	<!--
    <div class="row-fluid">
        <div class="span3 statbox pink" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $checkinbackstage->total_account; ?></div>
            <div class="title">User Tap at Checkin Backstage</div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/checkin/10/1/1"> read full report</a>
            </div>
        </div>
        <div class="span3 statbox blue noMargin" onTablet="span6" onDesktop="span3">
            <div class="number"><?php echo $checkoutbackstage->total_account; ?></div>
            <div class="title">User Tap at Checkout Backstage</div>
            <div class="footer">
                <a href="<?php echo base_url(); ?>dashboard/insight/home/checkin/10/2/1"> read full report</a>
            </div>
        </div>

    </div>	
	<script type="text/javascript">
    $(document).ready(function() {	
		var data = [
			<?php foreach($checkinstaffgrafik as $row): ?>
				{ label: "<?php echo $row['date']; ?> hours (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach; ?>
		];
		
		if($("#piechart").length)
		{
			$.plot($("#piechart"), data,
			{
				series: {
						pie: {
								show: true
						}
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});
			
			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechart").bind("plothover", pieHover);
		}
	
		var data1 = [
			<?php foreach($checkoutstaffgrafik as $row): ?>
				{ label: "<?php echo $row['date']; ?> hours (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach; ?>
		];
		
		if($("#piechart1").length)
		{
			$.plot($("#piechart1"), data1,
			{
				series: {
						pie: {
								show: true
						}
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});
			
			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechart1").bind("plothover", pieHover);
		}
		var data2 = [
			<?php foreach($checkinbackstagegrafik as $row): ?>
				{ label: "<?php echo $row['date']; ?> hours (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach; ?>
		];
		
		if($("#piechart2").length)
		{
			$.plot($("#piechart2"), data2,
			{
				series: {
						pie: {
								show: true
						}
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});
			
			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechart2").bind("plothover", pieHover);
		}
		var data3 = [
			<?php foreach($checkoutbackstagegrafik as $row): ?>
				{ label: "<?php echo $row['date']; ?> hours (<?php echo $row['total']; ?> users)",  data: <?php echo $row['total']; ?>},
			<?php endforeach; ?>
		];
		
		if($("#piechart3").length)
		{
			$.plot($("#piechart3"), data3,
			{
				series: {
						pie: {
								show: true
						}
				},
				grid: {
						hoverable: true,
						clickable: true
				},
				legend: {
					show: true
				},
				colors: ["#FA5833", "#2FABE9", "#FABB3D", "#78CD51"]
			});
			
			function pieHover(event, pos, obj)
			{
				if (!obj)
						return;
				percent = parseFloat(obj.series.percent).toFixed(2);
				$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
			}
			$("#piechart3").bind("plothover", pieHover);
		}
	
	});
	</script>
	<div class="row-fluid sortable">
		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Check-in Staff</h2>
			</div>		
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/insight/home/download/1" id="myForm">
				<input type="hidden" name="img_val" id="img_val" value="" />
			</form>
				<div id="target">
				<h2><span class="break"></span>Check-in Staff</h2>
			<div class="box-content">
				<div id="piechart" style="height:300px"></div>
			</div>	
				</div>
				<input type="submit" value="Download" onclick="capture();" />
			<script type="text/javascript">
				function capture() {
					html2canvas( $('#target'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_val').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myForm").submit();
						}
					});
				}
			</script>
		</div>
		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Check-out Staff</h2>
			</div>
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/insight/home/download/2" id="myForm1">
				<input type="hidden" name="img_val1" id="img_val1" value="" />
			</form>
				<div id="target1">
				<h2><span class="break"></span>Check-out Staff</h2>
			<div class="box-content">
				<div id="piechart1" style="height:300px"></div>
			</div>
				</div>
				<input type="submit" value="Download" onclick="capture1();" />
			<script type="text/javascript">
				function capture1() {
					html2canvas( $('#target1'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_val1').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myForm1").submit();
						}
					});
				}
			</script>
		</div>
	</div>
	<div class="row-fluid sortable">
		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Check-in Backstage</h2>				
			</div>
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/insight/home/download/3" id="myForm2">
				<input type="hidden" name="img_val2" id="img_val2" value="" />
			</form>
				<div id="target2">
				<h2><span class="break"></span>Check-in Backstage</h2>
			<div class="box-content">
				<div id="piechart2" style="height:300px"></div>
			</div>
				</div>
				<input type="submit" value="Download" onclick="capture2();" />
			<script type="text/javascript">
				function capture2() {
					html2canvas( $('#target2'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_val2').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myForm2").submit();
						}
					});
				}
			</script>
		</div>
		<div class="box span6">
			<div class="box-header">
				<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Check-out Backstage</h2>
			</div>
			<form method="POST" enctype="multipart/form-data" action="<?php echo base_url(); ?>dashboard/insight/home/download/4" id="myForm3">
				<input type="hidden" name="img_val3" id="img_val3" value="" />
			</form>
				<div id="target3">
				<h2><span class="break"></span>Check-out Backstage</h2>
			<div class="box-content">
				<div id="piechart3" style="height:300px"></div>
			</div>
				</div>
				<input type="submit" value="Download" onclick="capture3();" />
			<script type="text/javascript">
				function capture3() {
					html2canvas( $('#target3'),{
						onrendered: function (canvas) {
							//Set hidden field's value to image data (base-64 string)
							$('#img_val3').val(canvas.toDataURL("image/png"));
							//Submit the form manually
							document.getElementById("myForm3").submit();
						}
					});
				}
			</script>
		</div>
	</div>
</div>
-->
<!--/.fluid-container-->

<!-- end: Content -->