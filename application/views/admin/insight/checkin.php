<div id="content" class="span10">

    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url() . 'dashboard'; ?>"><?php echo $page; ?></a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#"><?php echo $title; ?></a></li>
    </ul>


    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>List user using all three station</h2>

            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Photo</th>
                            <th>Email</th>
                            <th>Facebook</th>
                            <th>Twitter</th>
                            <th>Total</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr>
                                <td><?php echo $row->account_displayname ?></td>
                                <td class="center">
                                    <?php if($row->account_avatar): ?>
                                    
                    <img src="<?php echo base_url(); ?>resizer/thumb.php?img=user/<?php echo $row->account_avatar; ?>" alt="" />
                                    <?php else: ?>
                                        EMPTY
                                    <?php endif; ?>
                                </td>
                                <td class="center"><?php echo $row->account_email; ?></td>
                                <td class="center">
                                    <?php if($row->account_fbid): ?>
                                        http://www.facebook.com/<?php echo $row->account_fbid; ?>
                                    <?php endif; ?>
                                </td>
                                <td class="center">
                                    <?php if($row->account_tw_username): ?>
                                        @<?php echo $row->account_tw_username; ?>
                                    <?php endif; ?>
                                </td>
                                <td class="center"><?php echo $row->total; ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->