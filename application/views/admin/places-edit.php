<script type="text/javascript">
    $(function() {
        $("#sdate").datepicker();
        $("#edate").datepicker();
    });
</script>
<?php
echo initialize_tinymce();
?>

<div class="titlepage">
    <h3><?php echo $modules; ?></h3>
</div>
<form name="addnews" enctype="multipart/form-data" method="post" action="<?php echo current_url() ?>">
    <?php if (validation_errors()) : ?>
        <div class="statusupdateError">
            <div class="statusupdateicon">
                <img src="<?php echo base_url() ?>assets/images/admin/warningError.png" alt="Error" />
            </div>
            <div class="statusupdatedesc">
                <ul>
                    <?php echo validation_errors('<li>', '</li>'); ?>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
    <div style="padding-left:0px; padding-bottom:20px;">

        <div id="tabs">
            <div style="float:left; width:270px; padding:7px 0 0 13px;">
                <h4><?php echo $title; ?></h4>
            </div>
        </div>
        <div class="clear"></div>
        <div class="tab_bdr"></div>
        <div class="panel" id="panel3" style="display: block;">
            <div class="formpanel">
                <p>
                    <strong>Spot Name</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="name" value="<?php if ($datalist->places_name != '')
        echo $datalist->places_name; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Email Address</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="emailaddress" value="<?php if ($datalist->places_email != '')
        echo $datalist->places_email; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Subject Email</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="subjectemail" value="<?php if ($datalist->places_subject_email != '')
        echo $datalist->places_subject_email; ?>"/>
                    </label>
                </p>
                <p>
                    <strong>Email Registration</strong><br/>
                    <label class="largeinput">
                        <textarea class="myTextEditor" cols="80" rows="10" name="isiemail"><?php if ($datalist->places_custom_email != '')
                                echo $datalist->places_custom_email; ?></textarea>
                    </label>
                </p>
                <p>
                    <strong>Spot Parent</strong><br/>
                    <select name="parent" class="listmenus">
                        <?php
                        $user_data = $this->db->get_where('account', array('id' => $admin))->row();
                        if ($user_data->account_group == '9') :
                            ?>
                            <option value="0" >Main Spot</option>
                        <?php endif; ?>
                        <?php
                        $this->db->select('');
                        $this->db->where('places_parent', 0);
                        $this->db->where('places_status !=', 0);
                        $this->db->from('places');
                        $select = $this->db->get()->result();
                        foreach ($select as $row) {
                            echo '<option value="' . $row->id . '" ';
                            if ($datalist->places_parent == $row->id)
                                echo 'selected="selected"';
                            echo ' >' . $row->places_name . '</option>';
                        }
                        ?>
                    </select>
                </p>
                <p>
                    <strong>Spot Type</strong><br/>
                    <select name="type" class="listmenus">
                        <option value="">No Type</option>
                        <?php
                        echo '<option value="1" ';
                        if ($datalist->places_type == 1) {
                            echo 'selected="selected"';
                        }
                        echo ' >Event</option>';
                        echo '<option value="2" ';
                        if ($datalist->places_type == 2) {
                            echo 'selected="selected"';
                        }
                        echo ' >Place</option>';
                        echo '<option value="3" ';
                        if ($datalist->places_type == 3) {
                            echo 'selected="selected"';
                        }
                        echo ' >Private</option>';
                        echo '<option value="4" ';
                        if ($datalist->places_type == 4) {
                            echo 'selected="selected"';
                        }
                        echo ' >Photo Share</option>';
                        echo '<option value="5" ';
                        if ($datalist->places_type == 5) {
                            echo 'selected="selected"';
                        }
                        echo ' >Like Booth</option>';
						echo '<option value="6" ';
                        if ($datalist->places_type == 6) {
                            echo 'selected="selected"';
                        }
                        echo ' >Photo Booth</option>';
                        ?>
                    </select>
                </p>
                <p>
                    <strong>Spot Address</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="address" value="<?php if ($datalist->places_address != '')
                            echo $datalist->places_address; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Start Date</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="sdate" id="sdate" value="<?php if ($datalist->places_startdate != '')
                                   echo date("m/d/Y", strtotime($datalist->places_startdate)); ?>" />
                    </label>
                </p>
                <p>
                    <strong>End Date</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="edate" id="edate" value="<?php if ($datalist->places_duedate != '')
                                   echo date("m/d/Y", strtotime($datalist->places_duedate)); ?>" />
                    </label>
                </p>
                <p>
                    <strong>Spot Description</strong><br/>
                    <label class="textarea">
                        <textarea cols="80" rows="10" id="isi" name="isi"><?php if ($datalist->places_desc != '')
                                   echo $datalist->places_desc; ?></textarea>
                    </label>
                </p>
                <p>
                    <strong>Custom Status Facebook</strong> (200 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="cstatus_fb" maxlength="900" value="<?php if ($datalist->places_cstatus_fb != '')
                                echo $datalist->places_cstatus_fb; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Custom Status Twitter</strong> (140 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="cstatus_tw" maxlength="140" value="<?php if ($datalist->places_cstatus_tw != '')
                                echo $datalist->places_cstatus_tw; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Custom Twitter Direct Message</strong> (100 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="cstatus_dm" maxlength="100" value="<?php if ($datalist->places_dm != '')
                                echo $datalist->places_dm; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Spot Icon</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" id="icon" name="icon" onclick="openFileBrowser('icon');" value="<?php if ($datalist->places_avatar != '')
                                echo $datalist->places_avatar; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Spot Facebook Places ID</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="facebook" value="<?php if ($datalist->places_facebook != '')
                                echo $datalist->places_facebook; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Spot Foursquare ID</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="foursquare" value="<?php if ($datalist->places_foursquare != '')
                                echo $datalist->places_foursquare; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Spot Latitude</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="latitude" value="<?php if ($datalist->places_latitude != '')
                                echo $datalist->places_latitude; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Spot Longitude</strong><br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="longitude" value="<?php if ($datalist->places_longitude != '')
                                echo $datalist->places_longitude; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Spot Landing Page URL</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="landing" value="<?php if ($datalist->places_landing != '0')
                                echo $datalist->places_landing; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Facebook Page URL to like</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="facebook_page" value="<?php if ($datalist->places_fbid != '0')
                                echo $datalist->places_fbid; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Twitter Username to follow</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" name="twitter_username" value="<?php if ($datalist->places_twitter != '0')
                                echo $datalist->places_twitter; ?>" />
                    </label>
                </p>
                <p>
                    <strong>Photobooth Frame URL</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%" id="frame" name="frame" onclick="openFileBrowser('frame');" value="<?php if ($datalist->places_frame != '0')
                                echo $datalist->places_frame; ?>" />
                    </label>
                </p>
               <p>
                    <strong>Photobooth Album Facebook</strong>(main spot only)(200 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="fb_album" maxlength="200" value="<?php if ($datalist->places_album != '')
                                echo $datalist->places_album; ?>"/>
                    </label>
                </p>
                <p>
                    <strong>Photobooth Custom Status Facebook</strong>(main spot only)(200 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="photo_cstatus_fb" maxlength="440" value="<?php if ($datalist->places_fb_caption != '')
                                echo $datalist->places_fb_caption; ?>"/>
                    </label>
                </p>
                <p>
                    <strong>Photobooth Custom Status Twitter</strong>(main spot only)(110 char at max)<br/>
                    <label class="largeinput">
                        <input type="text" style="width:78%"  name="photo_cstatus_tw" maxlength="110" value="<?php if ($datalist->places_tw_caption != '')
                                echo $datalist->places_tw_caption; ?>"/>
                    </label>
                </p>
                <p>
                    <strong>Spot Landing Page Custom CSS</strong> (main spot only)<br/>
                    <label class="largeinput">
                        <textarea cols="80" rows="15" name="css"><?php if ($datalist->places_css != '')
                                echo $datalist->places_css; ?></textarea>
                    </label>
                </p>
                <p>
                    <input type="image" src="<?php echo base_url(); ?>assets/images/admin/button-submit.gif" name="submit"/>
                </p>
            </div>
        </div>
        <div class="clear"></div>
        <div id="tabsbottom"></div>
    </div>
</form>
