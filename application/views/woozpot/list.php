<article>

    <section id="higlight">
        <div class="maindiv">
            <div id="left">
                <div id="innerleft">
                    <div class="titleplace"><span class="Muse30023">Woozpot</span><br>All spot <span class="redstyle">wooz.in</span> is at, have fun, happy woozin’</div>
                    <div>
                        <?php if( count($datalist) ) : ?>
                        <ul>
                                <?php foreach($datalist as $row) : ?>
                            <li class="clearmargin">
                                <div class="licontent">
                                    <div class="picplace">
                                        <a href="<?php echo site_url('woozpot/'.$row->places_nicename);?>" class="topmenuselected">
                                                    <?php if($row->places_avatar) : ?>
                                            <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar?>" alt="" class="foto">
                                                    <?php else: ?>
                                            <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                                    <?php endif; ?>
                                        </a>
                                    </div>
                                    <div class="contplace descmember arial14">
                                        <span class="arial19 redstyle">
                                            <a href="<?php echo site_url('woozpot/'.$row->places_nicename);?>" class="topmenuselected">
                                                        <?php echo $row->places_name;?>
                                            </a>
                                        </span><br />
                                                <?php echo $row->places_desc; ?>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                                <?php endforeach; ?>

                        </ul>

                            <?php if( isset($paging) ): ?>
                        <div id="paging">
                                    <?php echo $paging?>
                        </div>
                            <?php endif;?>
                        <?php else : ?>

                        <h3 class="error Muse30015">No Data Found</h3>

                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div id="right">
                <div class="titleplace">
                    <span class="Muse30023">Recent Activity</span><br>
                    <span class="Muse30018reg">Member <span class="redstyle">wooz.in</span> history on woozin’ spots</span>
                </div>
                <div>
                    <?php if( count($recent) ) : ?>
                    <ul>
                            <?php foreach($recent as $row) : ?>
                        <li class="clearmargin">
                            <div class="licontent">
                                <div class="picplace">
                                    <a href="<?php echo site_url('woozer/'.$row->account_username);?>" class="topmenuselected" title="<?php echo $row->account_displayname; ?>">
                                                <?php if($row->account_avatar) : ?>
                                        <img src="<?php echo base_url(); ?>resizer/avatar.php?w=50&img=<?php echo $row->account_avatar?>" alt="<?php echo $row->account_displayname; ?>" />
                                                <?php else: ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=user/default.png" alt="<?php echo $row->account_displayname; ?>" />
                                                <?php endif; ?>
                                    </a>
                                </div>
                                <div class="contplace2 descmember">
                                    <span class="titlemember"><?php echo $row->account_displayname; ?></span><br />
                                            <?php if($row->log_type == '2' && $row->badge_id != '0') : ?>
                                    earned the &quot;<span class="titlemember"><a href="<?php echo site_url('woozer/'.$row->account_username.'/pin/'.$row->log_hash); ?>"><?php echo $row->badge_name; ?></a></span>&quot; pin @
                                            <?php else: ?>
                                    wooz in @
                                            <?php endif; ?>
                                    <span class="titlemember"><a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>"><?php echo $row->places_name; ?></a></span>
                                </div>
                                <div class="iconplace" align="center">
                                            <?php if($row->badge_avatar) : ?>
                                    <img src="<?php echo base_url(); ?>resizer/thumb.php?img=badge/<?php echo $row->badge_avatar?>" alt="" />
                                            <?php elseif($row->places_avatar) : ?>
                                    <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar?>" alt="" />
                                            <?php else: ?>
                                    <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                            <?php endif; ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </li>
                            <?php endforeach; ?>

                    </ul>
                    <?php else : ?>

                    <h5>Data Not Found</h5>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

</article>