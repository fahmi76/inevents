<article>

    <section id="higlight">
        <div class="maindiv">
            <div id="left">
                <div id="innerleft">
                    <div class="titleplacenoborder Muse30023">Woozpot</div>
                    <div class="boxspot">
                        <div id="picspotplace">
                            <?php if($datalist->places_avatar) : ?>
                            <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $datalist->places_avatar; ?>" alt="" />
                            <?php else: ?>
                            <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=90&img=icon/default.png" alt="" />
                            <?php endif; ?>
                        </div>
                        <div id="spottext">
                            <span class="Muse30023 redstylemuda"><?php echo $datalist->places_name; ?></span><br />
                            <?php if($datalist->places_parent!='0') : ?>
                                <?php
                                $main = $this->db->get_where('places',array('id' => $datalist->places_parent, 'places_status' => 1))->row();
                                ?>
                            <span class="Muse30018reg">One Of
                                <a class="redstylemuda" href="<?php echo site_url('woozpot/'.$main->places_nicename);?>">
                                        <?php echo $main->places_name;?>
                                </a>
                                Wooz.in Stations </span>
                            <br />
                            <?php endif; ?>
                            <span class="descmember">
                                <?php if($datalist->places_desc!='') : ?>
                                    <?php echo $datalist->places_desc; ?>
                                <?php endif; ?>
                            </span><br />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <?php if($datalist->places_parent=='0') : ?>
                        <?php
                        $acc = $this->db->get_where('account',array('id' => $user, 'account_status' => 1))->row();
                        if($acc && $acc->account_group == 9){
                            $main = $this->db->get_where('places',array('places_parent' => $datalist->id, 'places_status' => 1))->result();
                        } else {
                            $main = $this->db->get_where('places',array('places_parent' => $datalist->id, 'places_status' => 1, 'places_type !=' => 3))->result();
                        }
                        ?>
                        <?php if( count($main) ) : ?>
                    <div id="children">
                        <div class="title Muse30023">
                                    <?php echo $datalist->places_name; ?> Stations :
                        </div>
                        <ul id="childs">
                                    <?php foreach($main as $row) : ?>
                            <li class="childs">
                                <div class="pic">
                                    <a href="<?php echo site_url('woozpot/'.$row->places_nicename);?>" class="topmenuselected">
                                                    <?php if($row->places_avatar) : ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar; ?>" alt="" class="foto">
                                                    <?php else: ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                                    <?php endif; ?>
                                    </a>
                                </div>
                                <div class="desc arial14">
                                    <span class="arial19 redstyle">
                                        <a href="<?php echo site_url('woozpot/'.$row->places_nicename);?>" class="topmenuselected">
                                                        <?php echo $row->places_name;?>
                                        </a>
                                    </span>
                                    <!--
                                    <br />
                                    <?php //echo $row->places_desc; ?><br />
                                    -->
                                </div>
                                <div class="clear"></div>
                                <div class="child">
                                    <h4 class="Muse30018reg">Who have been here :</h4>
                                                <?php $ls_log = $this->convert->child_data($row->id); ?>
                                                <?php if( count($ls_log) ) : ?>
                                    <ul>
                                                        <?php foreach($ls_log as $raw) : ?>
                                        <li class="childrens">
                                            <a href="<?php echo site_url('woozer/'.$raw->account_username);?>" class="topmenuselected" title="<?php echo $raw->account_displayname; ?>">
                                                                    <?php if($raw->account_avatar) : ?>
                                                <img src="<?php echo base_url(); ?>resizer/avatar.php?w=50&img=<?php echo $raw->account_avatar; ?>" alt="<?php echo $raw->account_displayname; ?>" class="foto">
                                                                    <?php else: ?>
                                                <img src="<?php echo base_url(); ?>resizer/thumb.php?img=user/default.png" alt="<?php echo $raw->account_displayname; ?>" />
                                                                    <?php endif; ?>
                                            </a>
                                        </li>
                                                        <?php endforeach; ?>
                                        <li class="clear"></li>
                                    </ul>
                                                <?php else : ?>

                                    <h3 class="error Muse30015">No Data Found</h3>

                                                <?php endif; ?>
                                </div>
                                <div class="all">
                                    <a href="<?php echo site_url('woozpot/'.$row->places_nicename);?>" class="topmenuselected">
                                        View All
                                    </a>
                                </div>
                            </li>
                                    <?php endforeach; ?>
                            <li class="clear"></li>
                        </ul>
                    </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <div class="titleplacenoborder">
                        who have been here
                    </div>
                    <div class="clearmargin">
                        <?php if( count($history) ) : ?>
                        <ul>
                                <?php foreach($history as $row) : ?>
                            <li class="pic">
                                <a href="<?php echo site_url('woozer/'.$row->account_username);?>" class="topmenuselected" title="<?php echo $row->account_displayname; ?>">
                                            <?php if($row->account_avatar) : ?>
                                    <img src="<?php echo base_url(); ?>resizer/avatar.php?w=50&img=<?php echo $row->account_avatar; ?>" alt="<?php echo $row->account_displayname; ?>" />
                                            <?php else: ?>
                                    <img src="<?php echo base_url(); ?>resizer/thumb.php?img=user/default.png" alt="<?php echo $row->account_displayname; ?>" />
                                            <?php endif; ?>
                                </a>
                            </li>
                                <?php endforeach; ?>
                            <li class="clear"></li>
                        </ul>
                        <?php else : ?>

                        <h3 class="error Muse30015">No Data Found</h3>

                        <?php endif; ?>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div id="right">
                <div class="titleplace">
                    <span class="Muse30023">Recent Activity</span><br>
                    <span class="Muse30018reg">Member <span class="redstyle">wooz.in</span> history on woozin’ spots</span>
                </div>
                <div>
                    <?php if( count($recent) ) : ?>
                    <ul>
                            <?php foreach($recent as $row) : ?>
                        <li class="clearmargin">
                            <div class="licontent">
                                <div class="picplace">
                                    <a href="<?php echo site_url('woozer/'.$row->account_username);?>" class="topmenuselected" title="<?php echo $row->account_displayname; ?>">
                                                <?php if($row->account_avatar) : ?>
                                        <img src="<?php echo base_url(); ?>resizer/avatar.php?w=50&img=<?php echo $row->account_avatar?>" alt="<?php echo $row->account_displayname; ?>" />
                                                <?php else: ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=user/default.png" alt="<?php echo $row->account_displayname; ?>" />
                                                <?php endif; ?>
                                    </a>
                                </div>
                                <div class="contplace2 descmember">
                                    <span class="titlemember"><?php echo $row->account_displayname; ?></span><br />
                                            <?php if($row->log_type == '2' && $row->badge_id != '0') : ?>
                                    earned the &quot;<span class="titlemember"><a href="<?php echo site_url('woozer/'.$row->account_username.'/pin/'.$row->log_hash); ?>"><?php echo $row->badge_name; ?></a></span>&quot; pin @
                                            <?php else: ?>
                                    wooz in @
                                            <?php endif; ?>
                                    <span class="titlemember"><a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>"><?php echo $row->places_name; ?></a></span>
                                </div>
                                <div class="iconplace" align="center">
                                            <?php if($row->badge_avatar) : ?>
                                    <img src="<?php echo base_url(); ?>resizer/thumb.php?img=badge/<?php echo $row->badge_avatar?>" alt="" />
                                            <?php elseif($row->places_avatar) : ?>
                                    <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar?>" alt="" />
                                            <?php else: ?>
                                    <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                            <?php endif; ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </li>
                            <?php endforeach; ?>

                    </ul>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </section>

</article>