<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Wooz.in Personal Page</span>
                    <br>
                    <span class="Muse30018reg">Hi,
                        <a href="<?php echo site_url('woozer/'.$user->account_username);?>">
                            <span class="redstylemuda"><?php echo $user->account_displayname; ?></span>
                        </a>,
                        welcome. This is your wooz.in page
                    </span>
                </div>
                <div class="formbox">
                    <div class="formbox3" >
                        <span class="Muse30023">RFID Card</span><br>
                        <span class="arial17reg">Are you sure to remove RFID card <strong><?php echo $cards->card_number; ?></strong>?</span>
                    </div>
                    <ul class="navlist">
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/woozer/delcard/<?php echo $card; ?>?act=delete" class="redstylemuda">DELETE</a>&nbsp;&nbsp;
                            <a href="<?php echo site_url('woozer/card'); ?>" class="redstylemuda">CANCEL</a>
                        </li>
                        <li class="clear"></li>
                   </ul>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>
            </div>
        </div>
    </section>
</article>
