<div id="mform">
    <h1>User Management</h1>
    <p>
        Hi, <span style="color:#666; font-weight:bold;"><?php echo $name; ?></span>.
        If you've done, click <a name="logout" href="<?php echo site_url('tool/logout'); ?>">Logout</a> to exit from this page.
    </p>

    <div class="garis"></div>

    <h2>Woozer Search :</h2>
    <p>You can search woozers, then edit it or delete it.</p>

    <div class="garisgrey"></div>

    <form id="frmsrc" action="<?php echo site_url('tool/search'); ?>" method="post">
        <div id="sname">
            <p>By Name<br /><input id="iname" type="text" class="txt" name="sname" /></p>
            <p><input type="submit" value="search" class="sub" /></p>
        </div>
        <div class="garisgrey"></div>
        <div id="semail">
            <p>By Email<br /><input id="iemail" type="text" class="txt" name="semail" /></p>
            <p><input type="submit" value="search" class="sub" /></p>
        </div>
        <div class="garisgrey"></div>
        <div id="srfid">
            <p>By RFID<br /><input id="irfid" type="text" class="txt" name="srfid" /></p>
            <p><input type="submit" value="search" class="sub" /></p>
        </div>
        <div id="srfid1">
            <p>By RFID Event<br /><input id="irfid1" type="text" class="txt" name="srfid1" /></p>
            <p><input type="submit" value="search" class="sub" /></p>
        </div>
    </form>
</div>