<form action="<?php echo current_url(); ?>" method="post">
    <div id="mform">
        <h1>Registration Code</h1>
        <p>Enter a registration code in the fields below.</p>
        <div class="garis"></div>
        <?php
        if (validation_errors ())
            echo validation_errors('<div id="merror">', '</div>');
        ?>
        <p>
            Registration Code<br />
            <input type="text" name="code" class="txt" />
        </p>
        <p>
            <input type="submit" value="submit" class="sub" />
        </p>
    </div>
</form>