<div id="mform">
    <h1>User Management</h1>
    <p>
        Hi, <span style="color:#666; font-weight:bold;"><?php echo $name; ?></span>.
        If you've done, click <a name="logout" href="<?php echo site_url('tool/logout');?>">Logout</a> to exit from this page.
    </p>
    <div class="garis"></div>

    <a href="<?php echo site_url('tool');?>">[ back to main page ]</a>
    <div class="garisgrey"></div>
    <h2>User Results</h2>

    <?php if(isset($result) && count($result)!=0): ?>
    <div id="searchresult">
        <ul>
                <?php foreach($result as $lsres): ?>
            <li>
                <p>Full Name : <?php echo $lsres->account_displayname; ?></p>
                <p>Email : <?php echo $lsres->account_email; ?></p>
				<?php if(isset($result1) && $result1 = 'landing'): ?>
				<p>Places Event : <?php echo $lsres->places_name; ?> </p>
                <p>
                    <a id="woozedit" href="<?php echo site_url('tool/editlanding/'.$lsres->id);?>" name="wedit">edit</a>
                            <?php if($woozer->account_group == 9): ?>
                    &nbsp;&nbsp;&nbsp;<a id="woozdel" href="<?php echo site_url('tool/removelanding/'.$lsres->id);?>" name="wdel">delete</a>
                            <?php endif; ?>
                </p>
				<?php else: ?>
                <p>
                    <a id="woozedit" href="<?php echo site_url('tool/edit/'.$lsres->id);?>" name="wedit">edit</a>
                            <?php if($woozer->account_group == 9): ?>
                    &nbsp;&nbsp;&nbsp;<a id="woozdel" href="<?php echo site_url('tool/remove/'.$lsres->id);?>" name="wdel">delete</a>
                            <?php endif; ?>
                </p>
				<?php endif; ?>
            </li>
                <?php endforeach; ?>
        </ul>
    </div>
    <?php else: ?>
    <p>Data Not Found.</p>
    <?php endif; ?>
    <div class="garisgrey"></div>
    <a href="<?php echo site_url('tool');?>">[ back to main page ]</a>
</div>