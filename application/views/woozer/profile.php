<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Wooz.in Personal Page</span>
                    <br>
                    <span class="Muse30018reg">Hi,
                        <a href="<?php echo site_url('woozer/'.$user->account_username);?>">
                            <span class="redstylemuda"><?php echo $user->account_displayname; ?></span>
                        </a>,
                        welcome. This is your wooz.in page
                    </span>
                </div>
                <div class="formbox">
                    <div class="formbox3" >
                        <?php if($data == false) : ?>
                            <?php if(validation_errors()) : ?>
                        <span class="Muse70030">Whoops ! Something wrong with your Profile</span>
                        <br>
                                <?php echo validation_errors('<span class="arial17">', '</span>'); ?>
                            <?php else : ?>
                        <span class="Muse30023">Edit Profile</span><br>
                        <span class="arial17reg">Please type in your RFID card ID on the form below. ID number is printed on the card or bracelet</span>
                            <?php endif; ?>
                        <?php else : ?>
                        <span class="Muse30023">Edit Profile</span><br>
                        <span class="arial17reg">Thank You! Your Profile Have been Updated</span>
                        <?php endif; ?>
                    </div>
                    <form enctype="multipart/form-data" action="<?php echo current_url(); ?>" method="post">
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                Display name :
                            </li>
                            <li>
                                <input name="fullname" id="txtemail" type="text" class="inputtype arial17reg" value="<?php if($user->account_displayname!="") echo $user->account_displayname;?>" />
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                RFID Card Number :
                            </li>
                            <li>
                                <input name="rfid" id="txtemail" type="text" class="inputtype arial17reg"  value="<?php if($user->account_rfid!="0") echo $user->account_rfid;?>" />
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                Birthday :
                            </li>
                            <li>
                                <input name="birthday" id="txtemail" type="text" class="inputtype arial17reg" value="<?php if($user->account_birthdate!="") echo $user->account_birthdate;?>" />
                                exp: 2000-12-31
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                URL :
                            </li>
                            <li>
                                <input name="url" id="txtemail" type="text" class="inputtype arial17reg" value="<?php if($user->account_url!="") echo $user->account_url;?>" />
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                Location :
                            </li>
                            <li>
                                <input name="location" id="txtemail" type="text" class="inputtype arial17reg"  value="<?php if($user->account_location!="") echo $user->account_location;?>" />
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                Profile :
                            </li>
                            <li>
                                <textarea name="profile" id="txtprofile" rows="8"  class="inputtypex arial17reg"><?php if($user->account_profile!="") echo $user->account_profile;?></textarea>
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                Avatar :
                            </li>
                            <li>
                                <input name="avatar" id="txtemail" type="file" class="inputtype arial17reg" >
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                New password :
                            </li>
                            <li>
                                <input name="password1" id="txtemail" type="password" class="inputtype arial17reg" >
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                Re-Type Password :
                            </li>
                            <li>
                                <input name="password2" id="txtemail" type="password" class="inputtype arial17reg" >
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                &nbsp;
                            </li>
                            <li>
                                <input type="image" src="<?php echo base_url(); ?>assets/images/save.gif">
                            </li>
                            <li class="clear"></li>
                        </ul>
                    </form>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>
            </div>
        </div>
    </section>

</article>
