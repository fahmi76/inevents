<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Wooz.in Personal Page</span>
                    <br>
                    <span class="Muse30018reg">Hi,
                        <a href="<?php echo site_url('woozer/'.$user->account_username);?>">
                            <span class="redstylemuda"><?php echo $user->account_displayname; ?></span>
                        </a>,
                        welcome. This is your wooz.in page
                    </span>
                </div>
                <div class="formbox">
                    <div class="formbox3" >                        
                        <span class="Muse30023">RFID Card</span><br>
                        <span class="arial17reg">List of your RFID card. <a href="<?php echo site_url('woozer/addcard');?>">Add New RFID card.</a></span>
                    </div>
                        <?php if($card->num_rows() != 0) :
                        $no = 1;
                        foreach($card->result() as $cards) :
                    ?>
                    <ul class="navlist">
                        <li class="listyle arial15 greystyle">
                            <?php echo $no++; ?>.&nbsp;<?php echo $cards->card_number; ?>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/woozer/editcard/<?php echo $cards->id; ?>" class="redstylemuda">edit</a> -
                            <a href="<?php echo base_url(); ?>index.php/woozer/delcard/<?php echo $cards->id; ?>" class="redstylemuda">delete</a>
                        </li>
                        <li class="clear"></li>
                   </ul>
                        <?php endforeach; ?>
                    <?php else : ?>
                    <ul class="navlist">
                        <li class="listyle arial15 greystyle">
                            Error Data Found.
                        </li>
                        <li class="clear"></li>
                    </ul>
                    <?php endif; ?>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>
            </div>
        </div>
    </section>

</article>
