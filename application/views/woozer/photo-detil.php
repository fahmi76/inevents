<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Photo History</span>
                </div>
                <div class="formbox" >
                    <div id="picpersonalplace">
                        <?php if($data->places_avatar) : ?>
                        <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=120&img=badge/<?php echo $data->places_avatar; ?>" alt="" />
                        <?php else: ?>
                        <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=120&img=icon/default.png" alt="" />
                        <?php endif; ?>
                    </div>
                    <div id="spottext3" class="Muse70030">
                        <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=640&img=photobooth/<?php echo $data->photo_upload; ?>" alt="" />
                        <a href="<?php echo site_url('woozer/'.$data->account_username);?>" class="topmenuselected" title="<?php echo $data->account_displayname; ?>">
                            <?php echo $data->account_displayname;?>
                        </a><br />
                        Woohooo!  I've just took photo on<br />
                        wooz.in @ <a href="<?php echo site_url('woozpot/'.$data->places_nicename); ?>">
                            <?php echo $data->places_name; ?>
                        </a><br /> 
                        by <?php echo $this->fungsi->dates($data->time_upload); ?>
                        <br />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>

</article>
