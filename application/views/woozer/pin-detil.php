<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Pin Earned</span>
                </div>
                <div class="formbox" >
                    <div id="picpersonalplace">
                        <?php if($data->badge_avatar) : ?>
                        <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=120&img=badge/<?php echo $data->badge_avatar; ?>" alt="" />
                        <?php else: ?>
                        <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=120&img=icon/default.png" alt="" />
                        <?php endif; ?>
                    </div>
                    <div id="spottext3" class="Muse70030">
                        <a href="<?php echo site_url('woozer/'.$datalist->account_username);?>" class="topmenuselected" title="<?php echo $datalist->account_displayname; ?>">
                            <?php echo $datalist->account_displayname;?>
                        </a><br />
                        Woohooo!  I've just unlock the &quot;<?php echo $data->badge_name; ?>&quot; badge on<br />
                        wooz.in @ <a href="<?php echo site_url('woozpot/'.$data->places_nicename); ?>">
                            <?php echo $data->places_name; ?>
                        </a><br /> 
                        by <?php echo $this->fungsi->dates($data->log_stamps); ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>

</article>
