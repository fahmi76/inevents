<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Wooz.in Personal Page</span>
                    <br>
                    <span class="Muse30018reg">Hi,
                        <a href="<?php echo site_url('woozer/'.$user->account_username);?>">
                            <span class="redstylemuda"><?php echo $user->account_displayname; ?></span>
                        </a>,
                        welcome. This is your wooz.in page
                    </span>
                </div>
                <div class="formbox">
                    <div class="formbox3" >
                        <?php if(validation_errors()) : ?>
                            <span class="Muse70030">Whoops ! Something wrong with your Profile</span>
                            <br>
                            <?php echo validation_errors('<span class="arial17">', '</span>'); ?>
                        <?php else : ?>
                            <span class="Muse30023">RFID Card</span><br>
                            <span class="arial17reg">Edit RFID card.</span>
                        <?php endif; ?>
                    </div>
                    <form action="<?php echo current_url(); ?>" method="post">
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                RFID Card Number :
                            </li>
                            <li>
                                <input name="rfid" id="txtemail" type="text" class="inputtype arial17reg" value="<?php if($card->card_number!="") echo $card->card_number;?>" />
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle arial15 greystyle">
                                &nbsp;
                            </li>
                            <li>
                                <input type="image" src="<?php echo base_url(); ?>assets/images/save.gif">
                            </li>
                            <li class="clear"></li>
                        </ul>
                    </form>
                </div>
                <div class="formfooter arial15">
                    &nbsp;
                </div>
            </div>
        </div>
    </section>

</article>