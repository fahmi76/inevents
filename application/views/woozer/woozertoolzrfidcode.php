<form action="<?php echo current_url(); ?>" method="post">
    <div id="mform">
        <h1>User Information</h1>
        <p>Tap a Card/Bracelet for saving RFID number.</p>
        <div class="garis"></div>
        <?php
        if (validation_errors ())
            echo validation_errors('<div id="merror">', '</div>');

        if($msgcode != '' && $msgcode == 1) {
            echo '<div id="merror">Woozer Information has been saved.</div>';
            echo "<meta http-equiv=\"refresh\" content=\"2;url=".site_url('tool/code')."\">";
        }
        ?>
        <div id="lspro">
            <ul style="list-style: none;">
                <li style="border-bottom: #ccc solid 1px;">
                    <div id="setpro">
                        <?php if($avatar==''): ?>
                        <img src="<?php echo base_url();?>assets/images/wb.jpg" width="150" />
                        <?php else :
                            if($chkavatar==1): ?>
                        <img src="<?php echo $avatar;?>" width="150" />
                            <?php else : ?>
                        <img src="<?php echo base_url();?>uploads/user/<?php echo $avatar;?>" width="150" />
                            <?php endif;
                        endif; ?>
                        <br style="clear: both;" />
                    </div>
                    <div id="setpro">
                        <p>
                            Full Name : <?php echo $name; ?><br />
                            Email : <?php echo $email; ?>
                            <?php if($location != ''): ?>
                            <br />Location : <?php echo $location; ?>
                            <?php endif; ?>
                        </p>
                    </div>
                </li>
                <li>
                    <p>
                        RFID<br />
                        <input type="text" name="rfidcode" class="txt" <?php if($rfid != '') echo 'value="'.$rfid.'"'; ?> />
                    </p>
                    <p>
                        <input type="submit" value="save" class="sub" />
                    </p>
                </li>
            </ul>
        </div>        
    </div>
</form>