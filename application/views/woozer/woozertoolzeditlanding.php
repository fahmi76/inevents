<div id="mform">
    <h1>User Management</h1>
    <p>
        Hi, <span style="color:#666; font-weight:bold;"><?php echo $name; ?></span>.
        If you've done, click <a name="logout" href="<?php echo site_url('tool/logout');?>">Logout</a> to exit from this page.
    </p>
    <div class="garis"></div>

    <a href="<?php echo site_url('tool');?>">[ back to main page ]</a>
    <div class="garisgrey"></div>
    <div id="editresult">
        <?php
        if (validation_errors ()) {
            echo validation_errors('<div id="merror">', '</div>');
        } else {
            if ($notice != "") {
                echo '<div style="margin: 0; padding: 2px 0 2px 6px; border: #ccc solid 1px;">';
                echo $notice;
                echo '</div>';
                echo '<meta http-equiv="refresh" content="2;url='.site_url('tool').'" />';
            }
        }
        ?>
    </div>
    <form action="<?php echo current_url(); ?>" method="post">
        <h2>User Edit</h2>
        <p>You can modify user informations. Please be careful and do it wisely.</p>
        <div class="garisgrey"></div>
        <p>
            Email<br />
            <input type="text" name="email" class="txt" <?php if ($ewoozer->account_email != '')
                       echo 'value="' . $ewoozer->account_email . '"'; ?> />
        </p>
        <div class="garisgrey"></div>
        <p>
            Full Name<br />
            <input type="text" name="ename" class="txt" <?php if ($ewoozer->account_displayname != '')
                       echo 'value="' . $ewoozer->account_displayname . '"'; ?> />
        </p>
        <p>
            RFID Card Number<br />
            <input id="erfid" type="text" name="erfid" class="txt" <?php if ($ewoozer->landing_rfid != '')
                       echo 'value="' . $ewoozer->landing_rfid . '"'; ?> />
        </p>
        <p>
            Location Event<br />
            <input type="text" name="ename" class="txt" <?php if ($ewoozer->places_name != '')
                       echo 'value="' . $ewoozer->places_name . '"'; ?> />
        </p>
        <div class="garisgrey"></div>        
        <p>
            <input type="submit" id="woozsubmit" value="save" class="sub" />
        </p>
    </form>
    <div class="garisgrey"></div>
    <a href="<?php echo site_url('tool');?>">[ back to main page ]</a>
</div>