<article>

    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Wooz.in Personal Page</span>
                    <br>
                    <span class="Muse30018reg">Hi,
                        <span class="redstylemuda">
                            <?php echo $datalist->account_displayname; ?>
                        </span>,
                        welcome. This is your wooz.in page
                    </span>
                    <?php if ($token === 0): ?>
                    <br>
                    <span class="Muse30018reg">
                        <span class="redstylemuda">Attention! Please Re-Connect Your Account Facebook.
                            <a href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('home/facebook'), 'redirect_uri' => site_url('home/facebook/profile'), 'scope' => $this->config->item('facebook_perms'))); ?>">
                                <img src="<?php echo base_url(); ?>assets/images/icon/connect-facebook.gif" alt="connect with facebook" />
                            </a>
                        </span>
                    </span>
                    <?php endif; ?>
                </div>
                <div class="formbox" >
                    <div id="picpersonalplace">
                        <?php if ($datalist->account_avatar) : ?>
                        <img src="<?php echo base_url(); ?>resizer/avatar.php?w=120&img=<?php echo $datalist->account_avatar; ?>" alt="" />
                        <?php else: ?>
                        <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=120&img=user/default.png" alt="" />
                        <?php endif; ?>
                    </div>
                    <div id="spottext2">
                        <ul class="navlist">
                            <li class="listyle2 descmember"><?php echo $datalist->account_displayname; ?></li>
                            <li class="clear"></li>
                        </ul>

                        <?php if ($datalist->account_location != '') : ?>
                        <ul class="navlist">
                            <li class="listyle2 descmember"><?php echo $datalist->account_location; ?></li>
                            <li class="clear"></li>
                        </ul>
                        <?php endif; ?>

                        <?php if ($datalist->account_profile != '') : ?>
                        <ul class="navlist">
                            <li class="listyle2 descmember"><?php echo $datalist->account_profile; ?></li>
                            <li class="clear"></li>
                        </ul>
                        <?php endif; ?>

                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                <?php
                                $result = $this->convert->history($datalist->id, 1);
                                if ($result != '0') :
                                    ?>
                                <a class="redstyle" href="<?php echo site_url('woozer/' . $datalist->account_username); ?>/history">
                                        <?php echo $result; ?>
                                </a>
                                <?php else : echo '0';
                                endif; ?>
                                Woozed Spot
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                <?php
                                $result = $this->convert->history($datalist->id, 2);
                                if ($result != '0') :
                                    ?>
                                <a class="redstyle" href="<?php echo site_url('woozer/' . $datalist->account_username); ?>/pin">
                                        <?php echo $result; ?>
                                </a>
                                <?php else : echo '0';
                                endif; ?>
                                Pin Earned
                            </li>
                            <li class="clear"></li>
                        </ul>

                        <?php if ($datalist->account_fbid != '' || $datalist->account_tw_token != '') : ?>
                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                    <?php if ($datalist->account_fbid != '') : ?>
                                <a href="http://www.facebook.com/profile.php?id=<?php echo $datalist->account_fbid; ?>">
                                    <img src="<?php echo base_url(); ?>assets/images/icon/mini-facebook.png" alt="facebook profile" />
                                </a>
                                    <?php endif; ?>
                                    <?php if ($datalist->account_tw_token != '') : ?>
                                <a href="http://twitter.com/<?php echo $datalist->account_username; ?>">
                                    <img src="<?php echo base_url(); ?>assets/images/icon/mini-twitter.png" alt="twitter profile" />
                                </a>
                                    <?php endif; ?>
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <?php endif; ?>

                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                <a href="<?php echo site_url('editprofile'); ?>">
                                    <img src="<?php echo base_url(); ?>assets/images/btnedit.gif" alt="edit" />
                                </a>
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div class="maindiv">
            <div id="left2">
                <div id="innerleft2">
                    <div class="titleplace">
                        <span class="Muse30023">Connection Info</span><br />
                        <span class="Muse30018reg">You can start exploring wooz.in after finishing a few setting below</span>
                    </div>
                    <div>
                        <ul>
                            <li class="clearmargin">
                                <div class="licontent2">
                                    <div class="picplacep">
                                        <?php if ($datalist->account_rfid == '0') : ?>
                                        <img src="<?php echo base_url(); ?>assets/images/icon/grey-rfid.png" alt="" />
                                        <?php else : ?>
                                        <img src="<?php echo base_url(); ?>assets/images/icon/color-rfid.png" alt="" />
                                        <?php endif; ?>
                                    </div>
                                    <div class="cont5place descmember">
                                        <?php if ($datalist->account_rfid == "0") : ?>
                                        <span class="arial15reg">You haven’t register any RFID number !</span><br>
                                        <a href="<?php echo site_url('home/rfid'); ?>" class="redstylemuda">Register now >></a>
                                        <?php else : ?>
                                        <span class="arial15reg">your RFID number is : <?php echo $datalist->account_rfid; ?></span><br>
                                        <a href="<?php echo site_url('home/rfid'); ?>" class="redstylemuda">Click Here >></a> to Edit
                                        <?php endif; ?>
                                        <?php
                                        $this->db->select('');
                                        $this->db->from('card');
                                        $this->db->where('account_id',$datalist->id);
                                        $this->db->where('card_status',1);
                                        $numcard = $this->db->get()->num_rows();
                                        if($numcard != 0):
                                        ?>
                                        <br /><a href="<?php echo base_url().'index.php/woozer/card'; ?>" class="redstylemuda">List of RFID card</a>
                                        <?php else : ?>
                                        <br /><a href="<?php echo base_url().'index.php/woozer/addcard'; ?>" class="redstylemuda">Add new RFID</a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                            <li class="clearmargin">
                                <div class="licontent2">
                                    <div class="picplacep">
                                        <?php if ($datalist->account_fbid == NULL) : ?>
                                        <img src="<?php echo base_url(); ?>assets/images/icon/grey-facebook.png" alt="" />
                                        <?php else : ?>
                                        <img src="<?php echo base_url(); ?>assets/images/icon/color-facebook.png" alt="" />
                                        <?php endif; ?>
                                    </div>
                                    <div class="cont5place descmember">
                                        <?php if ($datalist->account_fbid == NULL) : ?>
                                        <span class="arial15reg">Share your wooz.in activity through your Facebook account</span><br>
                                        <a href="<?php echo $this->facebook->getLoginUrl(array('cancel_url' => site_url('home/facebook'), 'redirect_uri' => site_url('home/facebook/profile'), 'scope' => $this->config->item('facebook_perms'))); ?>">
                                            <img src="<?php echo base_url(); ?>assets/images/icon/connect-facebook.gif" alt="connect with facebook" />
                                        </a>
                                        <?php else : ?>
                                        <span class="arial15reg">You've been connected with your Facebook Account</span><br>
                                        <a href="<?php echo site_url('home/revoke/fb'); ?>" class="redstylemuda">Click Here >></a> To Revoke Access.
                                        <?php endif; ?>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                            <li class="clearmargin">
                                <div class="licontent2">
                                    <div class="picplacep">
                                        <?php if ($datalist->account_tw_token == NULL) : ?>
                                        <img src="<?php echo base_url(); ?>assets/images/icon/grey-twitter.png" alt="" />
                                        <?php else : ?>
                                        <img src="<?php echo base_url(); ?>assets/images/icon/color-twitter.png" alt="" />
                                        <?php endif; ?>
                                    </div>
                                    <div class="cont5place descmember">
                                        <?php if ($datalist->account_tw_token == NULL) : ?>
                                        <span class="arial15reg">Share your wooz.in activity through your Twitter account</span><br>
                                        <a href="<?php echo site_url('home/twit'); ?>">
                                            <img src="<?php echo base_url(); ?>assets/images/icon/connect-twitter.png" alt="connect with twitter" />
                                        </a>
                                        <?php else : ?>
                                        <span class="arial15reg">you've been connected with your Twitter Account</span><br>
                                        <a href="<?php echo site_url('home/revoke/tw'); ?>" class="redstylemuda">Click Here >></a> To Revoke Access.
                                        <?php endif; ?>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                            <li class="clearmargin">
                                <div class="licontent2">
                                    <div class="picplacep">
                                        <?php if ($datalist->account_fs_token == NULL) : ?>
                                        <img src="<?php echo base_url(); ?>assets/images/icon/grey-foursquare.png" alt="" />
                                        <?php else : ?>
                                        <img src="<?php echo base_url(); ?>assets/images/icon/color-foursquare.png" alt="" />
                                        <?php endif; ?>
                                    </div>
                                    <div class="cont5place descmember">
                                        <?php if ($datalist->account_fs_token == NULL) : ?>
                                        <span class="arial15reg">Share your wooz.in activity through your Foursquare account</span><br>
                                        <a href="<?php echo site_url('home/foursquare'); ?>">
                                            <img src="<?php echo base_url(); ?>assets/images/icon/connect-foursquare.png" alt="connect with foursquare" />
                                        </a>
                                        <?php else : ?>
                                        <span class="arial15reg">you've been connected with your Foursquare Account</span><br>
                                        <a href="<?php echo site_url('home/revoke/fs'); ?>" class="redstylemuda">Click Here >></a> To Revoke Access.
                                        <?php endif; ?>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="right2">
                <div id="innerright2">
                    <div class="titleplace">
                        <span class="Muse30023">Recent Activity</span><br>
                        <span class="Muse30018reg">Your <span class="redstyle">wooz.in</span> history on woozin’ spot</span>
                    </div>
                    <div>
                        <?php if (count($history)) : ?>
                        <ul>
                                <?php foreach ($history as $row) :?>
                            <li class="clearmargin">
                                <div class="licontent">
                                    <div class="picplace">
                                        <a href="<?php echo site_url('woozer/' . $row->account_username . '/pin/' . $row->log_hash); ?>">
                                                    <?php if ($row->badge_avatar) : ?>
                                            <img src="<?php echo base_url(); ?>resizer/thumb.php?img=badge/<?php echo $row->badge_avatar ?>" alt="" />
                                                    <?php elseif ($row->places_avatar) : ?>
                                            <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar ?>" alt="" />
                                                    <?php else: ?>
                                            <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                                    <?php endif; ?>
                                        </a>
                                    </div>
                                    <div class="cont4place descmember">
                                        - <?php echo $this->fungsi->dates($row->log_stamps); ?><br />
                                                <?php if ($row->log_type == '2' && $row->badge_id != '0') : ?>
                                        earned the &quot;<span class="titlemember"><a href="<?php echo site_url('woozer/' . $row->account_username . '/pin/' . $row->log_hash); ?>"><?php echo $row->badge_name; ?></a></span>&quot; pin @
                                                <?php else: ?>
                                        wooz in @
                                                <?php endif; ?>
                                        <span class="titlemember"><a href="<?php echo site_url('woozpot/' . $row->places_nicename); ?>"><?php echo $row->places_name; ?></a></span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                                <?php endforeach; ?>
                        </ul>
                        <?php else : ?>
                        <h3 class="error Muse30015">No Data Found</h3>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </section>

</article>
