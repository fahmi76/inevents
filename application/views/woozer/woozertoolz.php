<html>
    <head>
        <title><?php echo $title; ?></title>
        <link href="<?php echo base_url(); ?>assets/css/mobile.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="mheader">
            <a href="<?php echo site_url('tool');?>">
                <div id="mheaderimg"></div>
                <div id="mheadertitle">
                    <strong style="color:#CC0000;">wooz.in.</strong><br />
                    <strong style="color:#666;">join. connect. woozin'</strong>
                </div>
            </a>
            <br style="clear:both;" />
        </div>
        <div id="mmain">
            <?php echo $wooztoolz; ?>
        </div>
        <div id="mfooter">
            <p>&copy;2010-2011 wooz.in. All rights reserved.</p>
            <p>Wooz.in is a web based application using RFID Owned by BIRA. Wooz.in (Patent Pending)</p>
        </div>
    </body>
</html>