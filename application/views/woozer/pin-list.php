
<article>

    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030"><?php echo $datalist->account_displayname;?></span>
                    <br>
                    <span class="Muse30018reg">
                        <span class="redstylemuda">
                            Wooz.in
                        </span> Public Profile
                    </span>
                </div>
                <div class="formbox" >
                    <div id="picpersonalplace">
                        <a href="<?php echo site_url('woozer/'.$datalist->account_username);?>" class="topmenuselected" title="<?php echo $datalist->account_displayname; ?>">
                            <?php if($datalist->account_avatar) : ?>
                            <img src="<?php echo base_url(); ?>resizer/avatar.php?w=120&img=<?php echo $datalist->account_avatar; ?>" alt="" />
                            <?php else: ?>
                            <img src="<?php echo base_url(); ?>resizer/thumb2.php?w=120&img=user/default.png" alt="" />
                            <?php endif; ?>
                        </a>
                    </div>
                    <div id="spottext2">
                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                <a href="<?php echo site_url('woozer/'.$datalist->account_username);?>" class="topmenuselected" title="<?php echo $datalist->account_displayname; ?>">
                                    <?php echo $datalist->account_displayname;?>
                                </a>
                            </li>
                            <li class="clear"></li>
                        </ul>

                        <?php if($user) : ?>
                            <?php if($datalist->account_location!='') : ?>
                        <ul class="navlist">
                            <li class="listyle2 descmember"><?php echo $datalist->account_location; ?></li>
                            <li class="clear"></li>
                        </ul>
                            <?php endif; ?>

                            <?php if($datalist->account_profile!='') : ?>
                        <ul class="navlist">
                            <li class="listyle2 descmember"><?php echo $datalist->account_profile; ?></li>
                            <li class="clear"></li>
                        </ul>
                            <?php endif; ?>
                        <?php endif; ?>

                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                <?php
                                $result = $this->convert->history($datalist->id, 1);
                                if($result != '0') :
                                    ?>
                                <a class="redstyle" href="<?php echo site_url('woozer/'.$datalist->account_username);?>/history">
                                        <?php echo $result; ?>
                                </a>
                                <?php else : echo '0';
                                endif; ?>
                                Woozed Spot
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                <?php
                                $result = $this->convert->history($datalist->id, 2);
                                if($result != '0') :
                                    ?>
                                <a class="redstyle" href="<?php echo site_url('woozer/'.$datalist->account_username);?>/pin">
                                        <?php echo $result; ?>
                                </a>
                                <?php else : echo '0';
                                endif; ?>
                                Pin Earned
                            </li>
                            <li class="clear"></li>
                        </ul>

                        <?php if($datalist->account_fbid!=''||$datalist->account_tw_token!='') : ?>
                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                    <?php if($datalist->account_fbid!='') : ?>
                                <a href="http://www.facebook.com/profile.php?id=<?php echo $datalist->account_fbid; ?>">
                                    <img src="<?php echo base_url(); ?>assets/images/icon/mini-facebook.png" alt="facebook profile" />
                                </a>
                                    <?php endif; ?>
                                    <?php if($datalist->account_tw_token!='') : ?>
                                <a href="http://twitter.com/<?php echo $datalist->account_username; ?>">
                                    <img src="<?php echo base_url(); ?>assets/images/icon/mini-twitter.png" alt="twitter profile" />
                                </a>
                                    <?php endif; ?>
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <?php endif; ?>

                        <?php if(!$user) : ?>
                        <ul class="navlist">
                            <li class="listyle2 descmember">
                                <a href="<?php echo site_url('signup'); ?>">
                                    <img src="<?php echo base_url(); ?>assets/images/create.gif" alt="edit" />
                                </a>
                            </li>
                            <li class="clear"></li>
                        </ul>
                        <?php endif; ?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div class="maindiv">
            <div id="left">
                <div id="innerleft">
                    <div class="titleplace">
                        <span class="Muse30023">Wooz.in Pin History</span><br>
                        <span class="Muse30018reg"><?php echo $datalist->account_displayname;?> <span class="redstyle">wooz.in</span> pin history on woozin’ spots</span>
                    </div>
                    <div>
                        <?php if( count($data) ) : ?>
                        <ul>
                                <?php foreach($data as $row) : ?>
                            <li class="clearmargin">
                                <div class="licontent">
                                    <div class="picplace">
                                        <a href="<?php echo site_url('woozer/'.$datalist->account_username.'/pin/'.$row->log_hash); ?>">
                                                    <?php if($row->badge_avatar) : ?>
                                            <img src="<?php echo base_url(); ?>resizer/thumb.php?img=badge/<?php echo $row->badge_avatar?>" alt="" />
                                                    <?php else: ?>
                                            <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                                    <?php endif; ?>
                                        </a>
                                    </div>
                                    <div class="contplace descmember arial14">
                                        <span class="titlemember">
                                            <a href="<?php echo site_url('woozer/'.$datalist->account_username.'/pin/'.$row->log_hash); ?>">
                                                        <?php echo $row->badge_name; ?>
                                            </a>
                                        </span> - <?php echo $this->fungsi->dates($row->log_stamps); ?><br />
                                        unlocked @
                                        <span class="titlemember">
                                            <a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>">
                                                        <?php echo $row->places_name; ?>
                                            </a>
                                        </span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                                <?php endforeach; ?>

                        </ul>

                            <?php if( isset($paging) ): ?>
                        <div align="right" class="arial14 redstyle" style="margin:6px 20px 0;" >
                                    <?php echo $paging?>
                        </div>
                            <?php endif;?>
                        <?php else : ?>

                        <h3 class="error Muse30015">No Data Found</h3>

                        <?php endif; ?>

                    </div>
                </div>

            </div>
            <div class="clear"></div>
        </div>
    </section>

</article>