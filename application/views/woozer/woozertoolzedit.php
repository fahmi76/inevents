<div id="mform">
    <h1>User Management</h1>
    <p>
        Hi, <span style="color:#666; font-weight:bold;"><?php echo $name; ?></span>.
        If you've done, click <a name="logout" href="<?php echo site_url('tool/logout');?>">Logout</a> to exit from this page.
    </p>
    <div class="garis"></div>

    <a href="<?php echo site_url('tool');?>">[ back to main page ]</a>
    <div class="garisgrey"></div>
    <div id="editresult">
        <?php
        if (validation_errors ()) {
            echo validation_errors('<div id="merror">', '</div>');
        } else {
            if ($notice != "") {
                echo '<div style="margin: 0; padding: 2px 0 2px 6px; border: #ccc solid 1px;">';
                echo $notice;
                echo '</div>';
                echo '<meta http-equiv="refresh" content="2;url='.site_url('tool').'" />';
            }
        }
        ?>
    </div>
    <form action="<?php echo current_url(); ?>" method="post">
        <h2>User Edit</h2>
        <p>You can modify user informations. Please be careful and do it wisely.</p>
        <div class="garisgrey"></div>
        <p>
            Email<br />
            <input type="text" name="email" class="txt" <?php if ($ewoozer->account_email != '')
                       echo 'value="' . $ewoozer->account_email . '"'; ?> />
        </p>
        <p>
            New Password<br />
            <input type="password" name="npass" class="txt" />
        </p>
        <p>
            Re-type Password<br />
            <input type="password" name="rpass" class="txt" />
        </p>
        <div class="garisgrey"></div>
        <p>
            Full Name<br />
            <input type="text" name="ename" class="txt" <?php if ($ewoozer->account_displayname != '')
                       echo 'value="' . $ewoozer->account_displayname . '"'; ?> />
        </p>
        <p>
            RFID Card Number<br />
            <input id="erfid" type="text" name="erfid" class="txt" <?php if ($ewoozer->account_rfid != '')
                       echo 'value="' . $ewoozer->account_rfid . '"'; ?> />
        </p>
        <p>
            Birthdate<br />
            <input id="ebirth" type="text" name="ebirth" class="txt" <?php if ($ewoozer->account_birthdate != '')
                       echo 'value="' . $ewoozer->account_birthdate . '"'; ?> />
            <br /><span style="font-size:10px;">(ex. 1974-11-05)</span>
        </p>
        <p>
            User Group<br/>
            <select class="txtselect" name="id_group">
                <option class="txtopt" value="<?php echo $ewoozer->account_group; ?>">
                    <?php
                    if($ewoozer->account_group == 9) {
                        echo "Administrator";
                    } elseif($ewoozer->account_group == 3) {
                        echo "Operator";
                    } elseif($ewoozer->account_group == 2) {
                        echo "Multi Card Holder";
                    } else {
                        echo "Rakyat Jelata";
                    }

                    $group = $ewoozer->account_group;
                    $lsgroup = array(9 => 'Administrator', 3 => 'Operator', 2 => 'Multi Card Holder', 1 => 'Rakyat Jelata');
                    ?>
                </option>
                <?php
                foreach($lsgroup as $key => $val) {
                    if($ewoozer->account_group != $key) {
                        echo '<option class="txtopt" value="'.$key.'">'.$val.'</option>';
                    }
                }
                ?>
            </select>
        </p>
        <div class="garisgrey"></div>
        <p>
            URL<br />
            <input id="eurl" type="text" name="eurl" class="txt" <?php if ($ewoozer->account_url != '')
                       echo 'value="' . $ewoozer->account_url . '"'; ?> />
        </p>
        <p>
            Location<br />
            <input id="elocation" type="text" name="elocation" class="txt" <?php if ($ewoozer->account_location != '')
                       echo 'value="' . $ewoozer->account_location . '"'; ?> />
        </p>        
        <p>
            Avatar<br />
            <input type="file" name="avatar" class="txtf" />
        </p>
        <p>
            Profile<br />
            <textarea name="profil" class="txta">
                <?php if ($ewoozer->account_profile != '') echo $ewoozer->account_profile; ?>
            </textarea>
        </p>
        <div class="garisgrey"></div>
        <p>
            Facebook UID<br />
            <input type="text" name="fbuid" class="txtx" <?php if ($ewoozer->account_fbid != '')
                       echo 'value="' . $ewoozer->account_fbid . '"'; ?> />
        </p>
        <p>
            Facebook Access Token<br />
            <input type="text" name="fbtoken" class="txtx" <?php if ($ewoozer->account_token != '')
                       echo 'value="' . $ewoozer->account_token . '"'; ?> />
        </p>
        <p>
            Twitter Access Token<br />
            <input type="text" name="twtoken" class="txtx" <?php if ($ewoozer->account_tw_token != '')
                       echo 'value="' . $ewoozer->account_tw_token . '"'; ?> />
        </p>
        <p>
            Twitter Secret<br />
            <input type="text" name="twsecret" class="txtx" <?php if ($ewoozer->account_tw_secret != '')
                       echo 'value="' . $ewoozer->account_tw_secret . '"'; ?> />
        </p>
        <p>
            Foursquare Token<br />
            <input type="text" name="fstoken" class="txtx" <?php if ($ewoozer->account_fs_token != '')
                       echo 'value="' . $ewoozer->account_fs_token . '"'; ?> />
        </p>        
        <div class="garisgrey"></div>        
        <p>
            <input type="submit" id="woozsubmit" value="save" class="sub" />
        </p>
    </form>
    <div class="garisgrey"></div>
    <a href="<?php echo site_url('tool');?>">[ back to main page ]</a>
</div>