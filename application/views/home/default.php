<article>
    <section id="intro">
        <div id="bigbanner" class="maindiv">
            <div id="bannerhome1">
                <div id="howto">
                    <h3 class="Muse30023">How To :</h3>
                    <ol class="Muse30015">
                        <li>connect to http://wooz.in</li>
                        <li>sign up or login</li>
                        <li>type your RFID number in</li>
                        <li>connect with your favorite social media channel</li>
                        <li>share your wooz to everyone</li>
                    </ol>
                </div>
                <div id="bigsign">
                    <a href="<?php echo site_url('signup'); ?>">Sign Up</a>
                </div>
            </div>
            <div id="bannerhome2">
                <object width="100%" height="100%" >
                    <param name="allowfullscreen" value="true" />
                    <param name="allowscriptaccess" value="always" />
                    <param name="movie" value="http://www.facebook.com/v/491493138058" />
                    <embed src="http://www.facebook.com/v/491493138058" type="application/x-shockwave-flash"
                           allowscriptaccess="always" allowfullscreen="true" width="100%" height="100%">
                    </embed>
                </object>
            </div>
            <div class="clear"></div>
        </div>
    </section>

    <section id="higlight">
        <div class="maindiv sparxup">
			<a href="http://www.sparxup.com/winners">
				<img src="<?php echo base_url(); ?>assets/images/Woozin_as-the-best-useoftechnology_728x90.jpg" alt="Wooz.in is the winner of SparX Up Awards 2011 for the Best Use of Technology Category" />
			</a>
        </div>
        <div class="maindiv">
            <div id="left">
                <div class="titleplace">Recent activity</div>

                <div class="boxhiglight">
                    <?php if( count($recent) ) : ?>
                    <ul>
                            <?php foreach($recent as $row) : ?>
                        <li class="clearmargin">
                            <div class="licontent">
                                <div class="picplace">
                                    <a href="<?php echo site_url('woozer/'.$row->account_username); ?>" title="<?php echo $row->account_displayname; ?>">
                                                <?php if($row->account_avatar) : ?>
                                        <img src="<?php echo base_url(); ?>resizer/avatar.php?w=50&img=<?php echo $row->account_avatar?>" alt="<?php echo $row->account_displayname; ?>" />
                                                <?php else: ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=user/default.png" alt="<?php echo $row->account_displayname; ?>" />
                                                <?php endif; ?>
                                    </a>
                                </div>
                                <div class="contplace descmember">
                                    <a class="redstyle" href="<?php echo site_url('woozer/'.$row->account_username); ?>"><?php echo $row->account_displayname; ?></a><br />
                                            <?php if($row->log_type == '2' && $row->badge_id != '0') : ?>
                                    earned the &quot;<span class="titlemember"><a href="<?php echo site_url('woozer/'.$row->account_username.'/pin/'.$row->log_hash); ?>"><?php echo $row->badge_name; ?></a></span>&quot; pin @
                                            <?php else: ?>
                                    wooz in @ 
                                            <?php endif; ?>
                                    <span class="titlemember"><a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>"><?php echo $row->places_name; ?></a></span>
                                </div>
                                <div class="iconplace" align="center">
                                            <?php if($row->badge_avatar) : ?>
                                    <a href="<?php echo site_url('woozer/'.$row->account_username.'/pin/'.$row->log_hash); ?>">
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=badge/<?php echo $row->badge_avatar?>" alt="" />
                                    </a>
                                            <?php elseif($row->places_avatar) : ?>
                                    <a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>">
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar?>" alt="" />
                                    </a>
                                            <?php else: ?>
                                    <a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>">
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                    </a>
                                            <?php endif; ?>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </li>
                            <?php endforeach; ?>
                    </ul>
                    <?php else : ?>
                    <ul>
                        <li class="clearmargin">
                            <div class="licontent">
                                <div class="picplace"></div>
                                <div class="contplace descmember">Data is Empty</div>
                                <div class="iconplace" align="center"></div>
                                <div class="clear"></div>
                            </div>
                        </li>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>

            <div id="middle">
                <div class="titleplace">Woozpot</div>
                <div class="boxhiglight">

                    <?php if( count($spots) ) : ?>
                    <ul>
                            <?php foreach($spots as $row) : ?>
                        <li class="clearmargin">
                            <div class="licontent">
                                <div class="cont2place descmember">
                                    <span class="titlemember">
                                        <a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>"><?php echo $row->places_name; ?></a>
                                    </span><br />
                                            <?php echo $this->fungsi->excerpt( $row->places_desc, 30);?>
                                </div>
                                <div class="iconplace" align="center">
                                    <a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>">
                                                <?php if($row->places_avatar) : ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar?>" width="49" height="49" />
                                                <?php else: ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                                <?php endif; ?>
                                    </a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </li>
                            <?php endforeach; ?>
                    </ul>
                    <?php else : ?>
                    <ul>
                        <li class="clearmargin">
                            <div class="licontent">
                                <div class="picplace"></div>
                                <div class="contplace descmember">Data is Empty</div>
                                <div class="iconplace" align="center"></div>
                                <div class="clear"></div>
                            </div>
                        </li>
                    </ul>
                    <?php endif; ?>

                </div>

                <div class="titleplace2">Wooziest</div>
                <div class="boxhiglight">

                    <?php if( count($active) ) : ?>
                    <ul>
                            <?php foreach($active as $row) : ?>
                        <li class="clearmargin">
                            <div class="licontent">

                                <div class="cont2place descmember">
                                    <span class="titlemember">
                                        <a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>"><?php echo $row->places_name; ?></a>
                                    </span><br />
                                            <?php echo $this->fungsi->excerpt( $row->places_desc, 30);?>
                                </div>
                                <div class="iconplace" align="center">
                                    <a href="<?php echo site_url('woozpot/'.$row->places_nicename); ?>">
                                                <?php if($row->places_avatar) : ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/<?php echo $row->places_avatar;?>" width="49" height="49" />
                                                <?php else: ?>
                                        <img src="<?php echo base_url(); ?>resizer/thumb.php?img=icon/default.png" alt="" />
                                                <?php endif; ?>
                                    </a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </li>
                            <?php endforeach; ?>
                    </ul>
                    <?php else : ?>
                    <ul>
                        <li class="clearmargin">
                            <div class="licontent">
                                <div class="picplace"></div>
                                <div class="contplace descmember">Data is Empty</div>
                                <div class="iconplace" align="center"></div>
                                <div class="clear"></div>
                            </div>
                        </li>
                    </ul>
                    <?php endif; ?>

                </div>
            </div>
            <div id="right">
                <div class="titleplace">Others</div>
                <div class="boxhighlight4">
                    <?php if($csbanner != 0): ?>
                    <ul>
                            <?php foreach($sidebanner as $sbanner) : ?>
                        <li>
							<a href="<?php echo $sbanner->banner_url; ?>" title="<?php echo $sbanner->banner_title; ?>">
								<img src="<?php echo base_url(); ?>uploads/banner/<?php echo $sbanner->banner_image; ?>" width="264" height="144">
							</a>
						</li>
                            <?php endforeach; ?>
                    </ul>
                    <?php else: ?>
                    <ul>
                        <li><img src="<?php echo base_url(); ?>uploads/banner/other.png" width="264" height="144"></li>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </section>
</article>
