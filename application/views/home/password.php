<article>

    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <?php if(validation_errors()) : ?>
                    <span class="Muse70030">Whoops ! Something wrong with your Input</span>
                    <br>
                        <?php echo validation_errors('<span class="Muse30018reg">', '</span>'); ?>
                    <?php else : ?>
                    <span class="Muse70030">Forgot your password?</span>
                    <br>
                    <span class="Muse30018reg">Wooz.in will send password reset instructions to the email address associated with your account.</span>
                    <?php endif; ?>
                </div>
                <div class="formfooter arial15">
                    <form action="<?php echo current_url(); ?>" method="post">
                        <div class="navlist arial17">
                            Please type your email address
                        </div>
                        <div class="navlist">
                            <input name="email" id="txtemail" type="text" class="inputtype arial17" >
                        </div>
                        <div class="navlist">
                            <input type="image" src="<?php echo base_url(); ?>assets/images/send.gif" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

</article>