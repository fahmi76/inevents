<html>
    <head>
        <title><?php echo $title; ?></title>
        <link href="<?php echo base_url(); ?>assets/css/pagesfb.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascripts/jquery-1.4.4.min.js"></script>
    </head>
    <body id="dbody">
        <div id="fb-root"></div>
        <script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
        <script type="text/javascript">
            FB.init({
                appId: '<?php echo $app_id; ?>',
                status  : true,
                cookie  : true,
                xfbml   : true
            });

            $(document).ready(function() {            
                $("#maincontent").hide();
                $('a[name=home]').click(function(){                    
                    $("#maincontent").hide();
                    $("#formrfid").hide();
                    $("#profile").show();
                    $("#event").fadeIn(1000).fadeTo("slow");
                });

                $('a[name=about]').click(function(){
                    $("#formrfid").hide();
                    $("#privacy").hide();
                    $("#profile").hide();
                    $("#term").hide();
                    $("#maincontent").slideDown(function(){
                        $("#about").fadeIn(1000).fadeTo("slow");                        
                    });
                });

                $('a[name=privacy]').click(function(){
                    $("#formrfid").hide();
                    $("#about").hide();
                    $("#profile").hide();
                    $("#term").hide();
                    $("#maincontent").slideDown(function(){
                        $("#privacy").fadeIn(1000).fadeTo("slow");                        
                    });
                });

                $('a[name=term]').click(function(){
                    $("#formrfid").hide();
                    $("#about").hide();
                    $("#profile").hide();
                    $("#privacy").hide();
                    $("#maincontent").slideDown(function(){
                        $("#term").fadeIn(1000).fadeTo("slow");                        
                    });
                });
            });
        </script>
        <?php $status = $this->session->userdata('status'); ?>
        <div id="top">
            <div id="logo"></div>
            <div id="titletop">
                <span class="woozin">wooz.in.</span><br />
                <span class="join">join. connect. woozin'</span>
            </div>
            <br class="clear" />
        </div>

        <?php if($status != 3): ?>
        <div id="menu">
            <a href="#" name="home">home</a>
            | <a href="#" name="about">about</a>
            | <a href="#" name="privacy">privacy policy</a>
            | <a href="#" name="term">term of service</a>
        </div>
        <?php endif; ?>

        <?php if($status == 1): ?>
        <div id="error">
            Please press Like button to connect with us!
        </div>
        <?php endif; ?>

        <?php if($status == 2): ?>
        <div id="error">
            <a href="<?php echo site_url('pagesfb/auth'); ?>"><strong>Sign Up Now</strong></a>
        </div>        
        <?php endif; ?>

        <div id="maincontent" style="display:none;">
            <div id="about" style="display:none;">
                <strong><h1>Apaan sih wooz.in?</h1></strong>
                <p>
                    wooz.in adalah web apps yang digunakan untuk men-share keberadaan dan komentar kamu check-in ditempat-tempat yang kamu sering kunjungi dengan menggunakan alat RFID. Share kamu akan langsung di bisa dibaca oleh teman-teman kamu di Facebook, Twitter, Foursquare dan Gowalla!
                </p>
            </div>
            <div id="privacy" style="display:none; height: 400px; overflow: auto;">
                <strong><h1>Privacy Policy</h1></strong>
                <p>
                    Kami mengumpulkan alamat surreal dare mereka yang mendaftarkan diri ke wooz.in, menyimpan informasi yang diberikan sepengetahuan pengguna (seperti survey dan atau pada proses pendaftaran). Informasi yang kami kumpulkan diperuntukann untuk meningkatkan pelayanan. <br />
                    Wooz.in mungkin akan menggunakan email Anda yang terdaftar di social media lain untuk memberikan pelayanan maksimal ketika menggunakan jasa Wooz.in serta tidak akan memberikan atau memberikan informasi untuk kepentingan komersial. Kecuali dipergunakan untuk memberikan service yang Anda butuhkan dalam Wooz.in, dan kami mendapatkan ijin penggunaan atau bila terjadi situasi berikut:
                </p>
                <ol style="font-size:11px; margin: 2px auto;">
                    <li>Jika dipelukan untuk melakukan penyidikan, menghindari atau mengambil tindakan terhadap kegiatan ilegal, penipuan atau situasi yang berpotensi ancaman terhadap keamanan seseorang, melanggar jasa yang diberikan atau bila dibutuhkan secara hukum.</li>
                    <li>Kami akan mentransfer data mengenai pengguna bila Wooz.in diakuisisi atau bergabung dengan perusahaan lain. Bila hal ini terjadi, Wooz.in akan memberikan informasi pada Anda.</li>
                </ol>
                <p>
                    <strong>Pengumpulan dan Penggunaan Informasi</strong>
                </p>
                <ol style="font-size:11px; margin: 2px auto;">
                    <li>Ketika Anda mendaftarkan diri ke Wooz.in, kami akan meminta informasi seperti nama dan alamat email.</li>
                    <li>Wooz.in menggunakan informasi yang diterima untuk keperluan penyediaan jasa (peningkatan service) Wooz.in</li>
                </ol>

                <p>
                    <strong style="margin: 2px auto;">Perubahan</strong><br />
                    Wooz.in secara berkala akan melakukan perubahan terhadap Kebijakan. Kami akan memberi tahukan apabila terjadi perubahan besar yang terkait dengan informasi pribadi pengguna melalui email yang sudah didaftarkan pengguna.
                </p>                
                <p>
                    <strong style="margin: 2px auto;">Disclaimer</strong><br />
                    Wooz. in tidak ber-afiliasi dengan social media lain (Facebook, twitter, foursquare, gowalla dan lainnya) dan isi serta pemiliknya. Bila Anda melakukan sign in menggunakan layanan kami, Wooz in tidak akan menyimpan password atau informasi personal lain yang terkait dengan social media lain (Facebook, twitter, foursquare, gowalla dan lainnya).
                </p>
            </div>
            <div id="term" style="display:none; height: 400px; overflow: auto;">
                <strong><h1>Term of Service</h1></strong>
                <p>
                    Dengan menggunakan Wooz.in service, Anda menyetujui Terms and Condition (Terms of Service) kami. Wooz.in memiliki hak untuk melakukan penambahan dan perubahan pada Term of Service secara berkala tanpa pemberitahuan terlebih dahulu. Fitur baru yang nantinya akan dikembangkan juga berada dibawah Terms of Service yang sama.<br />
                    Pelanggaran yang terjadi pada Terms of Service akan berakibat pada pe-nonaktifan akun anda. Wooz.in juga tidak bertanggung jawab pada isi yang diposting dalam Wooz.in dan bahwa Anda mungkin akan terpapar pada isi yang dibuat oleh para pengguna lain. Anda setuju untuk menggunakan jasa Wooz.in atas resiko yang akan Anda tanggung sendiri.
                </p>
                <p>&nbsp;</p>
                <h2>Terms of Service:</h2>
                <ol style="font-size:11px; margin: 2px auto;">
                    <li>Anda harus berusia minimal diatas 13 tahun untuk menggunakan jasa Wooz.in.</li>
                    <li>Ada harus memasukkan data yang valid.</li>
                    <li>Login Anda hanya dapat dipergunakan unuk single login.</li>
                    <li>Bots atau metode otomatis tidak diperkenankan.</li>
                    <li>Anda bertanggung jawab untuk menjaga keamanan akun dan password Anda. Wooz.in tidak bertanggung jawab atas kerugian atau hilangnya akun Anda dan akibat yang mengikutinya.</li>
                    <li>Anda bertanggung jawab untuk semua isi yang Anda buat ketika Anda menggunakan Wooz.in.</li>
                    <li>Anda tidak diperkenankan menggunakan Wooz.in untuk kegiatan yang melanggar hukum atau SARA.</li>
                    <li>Penggunaan service kami atas keinginan Anda sendiri.</li>
                    <li>Anda setuju untuk tidak melakukan reproduksi, duplikasi, membuat kopi service yang dimiliki Wooz.in tanpa ijin tertulis dari Wooz.in.</li>
                    <li>Anda tidak diperkenankan untuk upload, posting atau menyebarkan info yang bersifat spam, SARA dan isi yang melanggar hukum.</li>
                    <li>
                        Wooz.in tidak menjamin bahwa:
                        <ul>
                            <li>Servis yang diberikan sell aman dan error free.</li>
                            <li>Kualitas produk, servis, info yang Anda dapat dari servis sesuai dengan harapan Anda.</li>
                        </ul>
                    </li>
                </ol>
                <p>
                    <strong>Modifikasi</strong>
                </p>
                <ol style="font-size:11px; margin: 2px auto;">
                    <li>Wooz.in berhak untuk melakukan modifikasi atau menghilangkan service yang ada/dimiliki.</li>
                    <li>Wooz.in tidak bertanggung jawab pada perubahan ataupun modifikasi yang terjadi pada pihak ketiga.</li>
                </ol>
                <p>
                    <strong>Pembatalan</strong>
                </p>
                <ol style="font-size:11px; margin: 2px auto;">
                    <li>Anda bertanggung jawab untuk secara benar membatalkan akun Anda. untuk melakukan pembatalan akun ikuti petunjuk yang sudah diberikan.</li>
                    <li>Semua isi akun Anda akan langsung di-delete ketika Anda melakukan pembatalan akun. Informasi ini tidak dapat didapatkan kembali ketika Anda melakukan pembatalan akun.</li>
                    <li>Wooz in tidak bertanggung jawab pada kerugian atau kehilangan akibat dibatalkannya akun Anda.</li>
                </ol>
                <p>
                    <strong>Pembatalan</strong>
                </p>
                <ol style="font-size:11px; margin: 2px auto;">
                    <li>Wooz.in tidak memiliki copyright atas isi material yang diposting dalam service Wooz.in. Material yang Anda upload merupakan kepemilikan Anda. Namun Anda telah menyetujui untuk membagi isi yang Anda miliki kepada orang lain ketika melakukan pendaftaran service.</li>
                    <li>Wooz.in tidak melakukan proses pre-screen terhadap isi. Namun Wooz.in berhak untuk tidak menampilkan isi yang dianggap bertentangan dengan Terms of Service.</li>
                </ol>
            </div>
        </div>

        <?php if($status == ""): ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('a[name=edittherfid]').click(function(){
                    $("#edittitle").hide();
                    $("#formrfidedit").slideDown(1000).fadeIn(1000).fadeTo("slow");

                    $("#editsubmit").click(function(){
                        var urledit = $("#editrfid").attr('action');
                        var baseurl = '<?php echo site_url('pagesfb/goback'); ?>';

                        $("#loadload").show();
                        $.post(urledit, {
                            erfid : $("#erfid").val(),
                            ajax: true
                        }, function(data){
                            $("#rfideditresult").fadeIn(1000).fadeTo("slow");
                            $("#rfideditresult").load(urledit,function(){
                                if($("#erfid").val() != ""){
                                    $("#rfideditresult").html(data);
                                    $("#loadload").hide();
                                    setTimeout(function(){
                                        history.go(0);
                                    },1000);
                                    return true;
                                } else {
                                    $("#loadload").hide();
                                    $("#rfideditresult").html(data);

                                }
                            });
                        });
                        return false;
                    });
                });
            });
        </script>
        <div id="profile" style="text-align: center;">
            <strong><h1>Congratulations, you have become a woozer. </h1></strong>
            <h2>your registration code : <?php echo $user->id; ?>-<?php echo date("Ymd",strtotime($user->account_joindate)); ?>-<?php echo $pageid; ?></h2>
            <p>
                <img src="<?php echo $info->picture; ?>" width="150" alt="" border="0" /><br />
                <strong><?php echo $info->name; ?></strong><br />
                (<?php echo $info->email; ?>)<br />
                Location : <?php echo $info->location->name; ?>
            </p>
            <div style="border-top: 1px solid #F2F2F2; border-bottom: 1px solid #F2F2F2; margin: 2px 0; padding: 2px 0;">
                <div id="rfideditresult"></div>
                <h3 id="edittitle"><a href="#" name="edittherfid">Edit your RFID</a></h3>
                <div id="formrfidedit" style="display:none;">
                    <h2>Your RFID :</h2>
                    <form id="editrfid" action="<?php echo site_url('pagesfb/edit'); ?><?php echo $user->id; ?>" method="post">
                        <p>
                            <input type="text" class="inputtxt" id="erfid" name="erfid" <?php echo 'value="'.$user->account_rfid.'"'; ?> /><br />
                            <input type="submit" class="inputsubmit" id="editsubmit" value="save" />
                        </p>
                    </form>
                </div>
                <div id="loadload" style="display:none;">
                    <img src="<?php echo base_url(); ?>assets/images/ipages-loader.gif" alt="" width="16" height="11" border="0" />
                </div>
            </div>
        </div>        
        <?php endif; ?>

        <?php if($status == 3): ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#maincontent").hide();
                $("#formrfid").fadeIn(1000).fadeTo("slow");
                $("#formresult").hide();
                $("#signubmit").click(function(){                   
                    var dataurl = $("#signform").attr('action');
                    $("#loading").fadeIn(1000).fadeTo("slow");
                    $.post(dataurl, {
                        rfid : $("#rfid").val()
                    }, function(data){
                        $("#formresult").fadeIn(1000).fadeTo("slow");
                        $("#formresult").load(dataurl,function(){                            
                            if($("#rfid").val() != ""){
                                $("#formrfid").hide();
                                $(this).html(data);                                
                                setTimeout(function(){
                                    window.location.href="<?php echo site_url('pagesfb/done'); ?>";
                                },1000);
                                return true;
                            } else {
                                $(this).html(data);
                                return false;
                            }
                        });
                    });
                    return false;
                });
            });
        </script>        
        <div id="formresult"></div>
        <div id="formrfid">
            <h1>Insert Your RFID :</h1>
            <form id="signform" action="<?php echo site_url('pagesfb/rfid'); ?>" method="post">
                    <?php
                    if($this->session->userdata('accrfid') != '') {
                        $valrfid = 'value="'.$this->session->userdata('accrfid').'"';
                    } else {
                        $valrfid = '';
                    }
                    ?>
                <p>
                    <input type="text" class="inputtxt" id="rfid" name="rfid" <?php echo $valrfid; ?> /><br />
                    <input type="submit" class="inputsubmit" id="signubmit" value="save" />
                </p>
            </form>
        </div>        
        <?php endif; ?>

        <?php if($status == 5): ?>
        <div id="presign">
            You have registered, but you have not activation in this application. Press the signup button to start the activation.<br />
            &rtrif; <a href="<?php echo site_url('pagesfb/setstatus'); ?>"><strong>Sign Up Now</strong></a><br />
        </div>
        <?php endif; ?>

        <?php if($status == 4): ?>
        <div id="error">
            <script type="text/javascript">
                $(document).ready(function() {
                    setTimeout(function(){
                        history.go(0);
                    },1000);
                });
            </script>
            Thanks for join with us.
        </div>
        <?php endif; ?>

        <div id="loading">
            <img src="<?php echo base_url(); ?>assets/images/ipages-loader.gif" alt="" width="16" height="11" border="0" />
        </div>

        <div id="bottom">
            &copy;2010-2011 wooz.in. All rights reserved.<br />
            Wooz.in is a web based application using RFID Owned by BIRA. Wooz.in (Patent Pending)
        </div>
    </body>
</html>