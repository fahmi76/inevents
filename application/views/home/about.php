<script type="text/javascript">
    $(document).ready(function(){
        $('#english').hide();
        $('#bahasa a').click(function(){
            $('#english').show();
            $('#bahasa').hide();

            $('#english a').click(function(){
                $('#english').hide();
                $('#bahasa').show();
            });
        });
    });
</script>
<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">About</span><br>
                    <span class="Muse30018reg">Wooz.in - Join. Connect. Woozin'</span>
                </div>
                <div class="formbox">
                    <div id="bahasa">
                        <p>Bahasa | <a href="#eng">English</a></p>
                        <h3>Apaan sih wooz.in?</h3>
                        <p>wooz.in adalah web apps yang digunakan untuk men-share keberadaan dan komentar kamu check-in ditempat-tempat yang kamu sering kunjungi dengan menggunakan alat RFID. Share kamu akan langsung di bisa dibaca oleh teman-teman kamu di Facebook, Twitter, dan Foursquare!</p>
						<p>Kalau mau tanya lebih lanjut soal wooz.in, kirim email ke : <br />
							<h3><font color = 'red'>contact@wooz.in</font></h3>
						</p>
						<p>Atau telepon ke : <br />
							<h3><font color = 'red'>+6221-72787478 ext 111</font></h3>
						</p>
                    </div>
                    <div id="english">
                        <p><a href="#ind">Bahasa</a> | English</p>
						<h3>What&rsquo;s up with wooz.in?</h3>
						<p>Online and offline world is now borderless. Wooz.in is an application that enables you to share your existence, comment or do a check-in in places that you frequently visit, by using radio frequency devices. Your share can be automatically updated to your friend in facebook, twitter, Foursquare and Gowalla!</p>
						<p>We gather e-mail address from wooz.in registrant, store information that given by user consent (surveys, and/or on registration process). Any information that we gather is used to upgrade our services.</p>
						<p>To find out more about wooz.in, send an email to: <br />
							<h3><font color = 'red'>contact@wooz.in</font></h3>
						</p>
						<p>or call to: <br />
							<h3><font color = 'red'>+6221-72787478 ext 111</font></h3>
						</p>
                    </div>
					<p>And at SparX Up Awards 2011, Wooz.in is the winner for the Best Use of Technology Category<br />
						<a href="http://www.sparxup.com/winners">
							<img src="<?php echo base_url(); ?>assets/images/Woozin_as-the-best-useoftechnology_300x250.jpg" alt="Wooz.in is the winner of SparX Up Awards 2011 for the Best Use of Technology Category" />
						</a>
					</p>
                </div>
            </div>
        </div>
    </section>
</article>