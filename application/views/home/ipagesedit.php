<?php
if (validation_errors()) {
    echo validation_errors();
    echo "<script>var editerr = 1;</script>";
} else {
    echo "Your RFID has been successfully saved.";
    echo "<script>var editerr = 0;</script>";
}
?>