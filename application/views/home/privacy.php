<script type="text/javascript">
    $(document).ready(function(){
        $('#english').hide();
        $('#bahasa a').click(function(){
            $('#english').show();
            $('#bahasa').hide();

            $('#english a').click(function(){
                $('#english').hide();
                $('#bahasa').show();
            });
        });
    });
</script>
<article>
    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Privacy Policy</span><br>
                    <span class="Muse30018reg">Wooz.in - Join. Connect. Woozin'</span>
                </div>
                <div class="formbox">
                    <div id="bahasa">
                        <p>Bahasa | <a href="#eng">English</a></p>
                        <p>Kami mengumpulkan alamat surreal dare mereka yang mendaftarkan diri ke wooz.in, menyimpan informasi yang diberikan sepengetahuan pengguna (seperti survey dan atau pada proses pendaftaran). Informasi yang kami kumpulkan diperuntukann untuk meningkatkan pelayanan.</p>
                        <p>Wooz.in mungkin akan menggunakan email Anda yang terdaftar di social media lain untuk memberikan pelayanan maksimal ketika menggunakan jasa Wooz.in serta tidak akan memberikan atau memberikan informasi untuk kepentingan komersial. Kecuali dipergunakan untuk memberikan service yang Anda butuhkan dalam Wooz.in, dan kami mendapatkan ijin penggunaan atau bila terjadi situasi berikut:</p>
                        <ol>
                            <li>Jika dipelukan untuk melakukan penyidikan, menghindari atau mengambil tindakan terhadap kegiatan ilegal, penipuan atau situasi yang berpotensi ancaman terhadap keamanan seseorang, melanggar jasa yang diberikan atau bila dibutuhkan secara hukum.</li>
                            <li>Kami akan mentransfer data mengenai pengguna bila Wooz.in diakuisisi atau bergabung dengan perusahaan lain. Bila hal ini terjadi, Wooz.in akan memberikan informasi pada Anda.</li>
                        </ol>
                        <h3>Pengumpulan dan Penggunaan Informasi</h3>
                        <ol>
                            <li>Ketika Anda mendaftarkan diri ke Wooz.in, kami akan meminta informasi seperti nama dan alamat email.</li>
                            <li>Wooz.in menggunakan informasi yang diterima untuk keperluan penyediaan jasa (peningkatan service) Wooz.in</li>
                        </ol>
                        <h3>Perubahan</h3>
                        <p>
                            Wooz.in secara berkala akan melakukan perubahan terhadap Kebijakan. Kami akan memberi tahukan apabila terjadi perubahan besar yang terkait dengan informasi pribadi pengguna melalui email yang sudah didaftarkan pengguna.
                        </p>
                        <h3>Disclaimer</h3>
                        <p>Wooz. in tidak ber-afiliasi dengan social media lain (Facebook, twitter, foursquare, gowalla dan lainnya) dan isi serta pemiliknya. Bila Anda melakukan sign in menggunakan layanan kami, Wooz in tidak akan menyimpan password atau informasi personal lain yang terkait dengan social media lain (Facebook, twitter, foursquare, gowalla dan lainnya).</p>
                    </div>
                    <div id="english">
                        <p><a href="#ind">Bahasa</a> | English</p>
						<p>Wooz.in may use your email, which registered to other social media to provide optimal service while you&rsquo;re using our service. Wooz.in will not give or provide any information to commercial interest, unless to enhance your overall experience in using Wooz.in and you granted the permission to use, or if the following situation occurs.</p>
						<ol start="1">
						<li>If needed for investigation, to prevent or take action against illegal activities, fraud, or certain situations that potentially threatening to a person security, violate the provided services or when required by law.</li>
						<li>We will transfer Wooz.in user data, when Wooz.in is acquired by or merged with another company. When this happens, Wooz.in will provide information for you. </li>
						</ol>
						<h3>Information Gathered and Used</h3>
						<ol start="1">
						<li>When you enroll into Wooz.in, we may ask for information such as name and email address.</li>
						<li>Wooz.in use of information received for the purpose of providing services (service improvement) of wooz.in</li>
						</ol>
						<h3>Changes</h3>
						<p>Wooz.in will periodically make changes to the policy. We will inform the major changes related with personal information via email to user who registered to the service.</p>
						<h3>Disclaimer</h3>
						<p>Wooz.in have no affiliation with other social media (Facebook, Twitter, Foursquare, Gowalla and other social media services), as well as content and the owner. When you do sign in to social media using our service, Wooz.in will not store passwords or other personal info related with other social media (Facebook, Twitter, Foursquare, Gowalla and other social media services).</p>                    </div>
                </div>
            </div>
        </div>
    </section>
</article>