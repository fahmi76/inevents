<script type="text/javascript">
    $(document).ready(function(){
        $('#english').hide();
        $('#bahasa a').click(function(){
            $('#english').show();
            $('#bahasa').hide();

            $('#english a').click(function(){
                $('#english').hide();
                $('#bahasa').show();
            });
        });
    });
</script>
<article>

    <section id="higlight">
        <div class="maindiv">
            <div id="middleone">
                <div class="signbox">
                    <span class="Muse70030">Terms Of Service</span><br>
                    <span class="Muse30018reg">Wooz.in - Join. Connect. Woozin'</span>
                </div>
                <div class="formbox">
                    <div id="bahasa">
                        <p>Bahasa | <a href="#">English</a></p>
                        <p>Dengan menggunakan Wooz.in service, Anda menyetujui Terms and Condition (Terms of Service) kami. Wooz.in memiliki hak untuk melakukan penambahan dan perubahan pada Term of Service secara berkala tanpa pemberitahuan terlebih dahulu. Fitur baru yang nantinya akan dikembangkan juga berada dibawah Terms of Service yang sama.</p>

                        <p>Pelanggaran yang terjadi pada Terms of Service akan berakibat pada pe-nonaktifan akun anda. Wooz.in juga tidak bertanggung jawab pada isi yang diposting dalam Wooz.in dan bahwa Anda mungkin akan terpapar pada isi yang dibuat oleh para pengguna lain. Anda setuju untuk menggunakan jasa Wooz.in atas resiko yang akan Anda tanggung sendiri.</p>

                        <h3>Terms of Service:</h3>
                        <p>
                            1. Anda harus berusia minimal diatas 13 tahun untuk menggunakan jasa Wooz.in<br />
                            2. Ada harus memasukkan data yang valid<br />
                            3. Login Anda hanya dapat dipergunakan unuk single login.<br />
                            4. Bots atau metode otomatis tidak diperkenankan<br />
                            5. Anda bertanggung jawab untuk menjaga keamanan akun dan password Anda. Wooz.in tidak bertanggung jawab atas kerugian atau hilangnya akun Anda dan akibat yang mengikutinya.<br />
                            6. Anda bertanggung jawab untuk semua isi yang Anda buat ketika Anda menggunakan Wooz.in<br />
                            7. Anda tidak diperkenankan menggunakan Wooz.in untuk kegiatan yang melanggar hukum atau SARA.<br />
                            8. Penggunaan service kami atas keinginan Anda sendiri. <br />
                            9. Anda setuju untuk tidak melakukan reproduksi, duplikasi, membuat kopi service yang dimiliki Wooz.in tanpa ijin tertulis dari Wooz.in 	<br />
                            10. Anda tidak diperkenankan untuk upload, posting atau menyebarkan info yang bersifat spam, SARA dan isi yang melanggar hukum  <br />
                            11. Wooz.in tidak menjamin bahwa: <br />
                            (i) servos yang diberikan sell aman dan error free<br />
                            (ii) kualitas produk, servis, info yang Anda dapat dari servis sesuai dengan harapan Anda
                        </p>

                        <h3>Modifikasi </h3>
                        <p>
                            1. Wooz.in berhak untuk melakukan modifikasi atau menghilangkan service yang ada/dimiliki. <br />
                            2. Wooz.in tidak bertanggung jawab pada perubahan ataupun modifikasi yang terjadi pada pihak ketiga.</p>

                        <h3>Pembatalan</h3>
                        <p>
                            1. Anda bertanggung jawab untuk secara benar membatalkan akun Anda. untuk melakukan pembatalan akun ikuti petunjuk yang sudah diberikan.<br />
                            2. Semua isi akun Anda akan langsung di-delete ketika Anda melakukan pembatalan akun. Informasi ini tidak dapat didapatkan kembali ketika Anda melakukan pembatalan akun.<br />
                            3. Wooz in tidak bertanggung jawab pada kerugian atau kehilangan akibat dibatalkannya akun Anda.</p>

                        <h3>Copyright dan Kepemilikan Isi</h3>
                        <p>
                            1. Wooz.in tidak memiliki copyright atas isi material yang diposting dalam service Wooz.in. Material yang Anda upload merupakan kepemilikan Anda. Namun Anda telah menyetujui untuk membagi isi yang Anda miliki kepada orang lain ketika melakukan pendaftaran service.<br />
                            2. Wooz.in tidak melakukan proses pre-screen terhadap isi. Namun Wooz.in berhak untuk tidak menampilkan isi yang dianggap bertentangan dengan Terms of Service
                        </p>
                    </div>
                    <div id="english">
                        <p><a href="#">Bahasa</a> | English</p>
						<p>By using Wooz.in service, you accept our Terms and Conditions along with our Terms of Service. Wooz.in have the right to make additions and changes to the Term and Conditions or Term of Service without prior notice.</p>
						<p>New feature that will be developed also under the same Terms and Conditions and Term of Service. Violations that occur on the Terms and Condition and Terms of Services will result in a non-active account. Wooz.in will not be responsible for the content posted on Wooz.in and that you may be exposed to content created by other user. You agree to use the services of Wooz.in with your own risk. </p>
						<h3>Terms of Service:</h3>
						<ol>
						<li>You must be at least 13 years old and above to use Wooz.in service</li>
						<li>The data entered must be valid</li>
						<li>Your login may only be used under one single login</li>
						<li>Bots or automated methods are not allowed</li>
						<li>You are responsible for maintaining the security of your Wooz.in account and password. Wooz.in are not responsible for the damages or loss of your account and the consequences that follows</li>
						<li>You are responsible for all content that you created when using Wooz.in</li>
						<li>You are not allowed to use Wooz.in for any unlawful activity or SARA related</li>
						<li>You use our service on your own free will</li>
						<li>You agree not to reproduce, duplicate, make copied service of Wooz.in without written permission from Wooz.in</li>
						<li>You may not upload, post or disseminate information that is spam in nature, SARA and content that is unlawful</li>
						<li>Wooz.in does not warrant that:<ol style="list-style-type: lower-roman;">
						<li>the service is secure and error free</li>
						<li>product quality, service, and info you obtained from the service is meeting your expectation</li>
						</ol></li>
						</ol>
						<h3>Modification</h3>
						<ol>
						<li>Wooz.in reserve the right to modify or eliminate existing or owned service</li>
						<li>Wooz.in is not responsible for the alterations or modifications that occurs on the third party</li>
						</ol>
						<h3>Cancelation</h3>
						<ol>
						<li>You are responsible for properly canceling your account, to cancel your account, please follow the instruction that already been given.</li>
						<li>All content of your account will immediately be deleted when you cancel your account. This information cannot be obtained again when you cancel your account.</li>
						<li>Wooz.in is not responsible for the loss created due to the cancelation of your account.</li>
						</ol>
						<h3>Copyright and Content Ownership</h3>
						<ol>
						<li>Wooz.in doesn&rsquo;t own the copyright of content from the material posted on Wooz.in service. Materials that you upload are of your ownership. But you have agreed to share your content to others, when you register to the service.</li>
						<li>Wooz.in do not pre-screen the content, however Wooz.in reserves the right not to display the content of which are considered will violate the Term of Service.</li>
						</ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

</article>
