<h2><?php echo $data->account_displayname; ?></h2>
<hr>	
<div class="text-center">
    <div class="well stripe">
        <span>Redeem point: </span>
        <span class="redeem-point"><?php echo $mypoint; ?></span>
    </div
    <br>
    <button type="button" id="Bottle" class="btn btn-primary btn-xlg">Bottle</button>
    <br><br>
    <button type="button" id="Jumbo" class="btn btn-primary btn-xlg">Jumbo Pint</button>
	<br><br>
    <button type="button" id="Tower" class="btn btn-primary btn-xlg">Tower</button>
    <hr>
	
<button type="button" id="back" class="btn btn-default btn-lg btn-xlg"><i class="fa fa-chevron-left">&nbsp;</i>Back</button>

</div>
<script type="text/javascript">
    $(function() {
        $('#Bottle').click(function() {
            window.location = '<?php echo site_url('guinness/redeem/purchase/1/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>'
        });
        $('#Jumbo').click(function() {
            window.location = '<?php echo site_url('guinness/redeem/purchase/2/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>'
        });
        $('#Tower').click(function() {
            window.location = '<?php echo site_url('guinness/redeem/purchase/3/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>'
        });
        $('#back').click(function() {
            window.location = '<?php echo base_url(); ?>guinness/redeem/found/<?php echo $loc; ?>/<?php echo $data->id; ?>/<?php echo $data->account_username; ?>'
        });
    });
</script>