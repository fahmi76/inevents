<h2 class="center">
    Silahkan Pilih Registrasi atau redeem
</h2><br />
<p class="center">
	<button type="button" id="registrasi" class="btn btn-primary btn-xlg">Registrasi</button>
    <br /><br />
    <button type="button" id="redeem" class="btn btn-primary btn-xlg">Redeem dan check point</button>
</p>

<script type="text/javascript">
    $(function() {
        $('#redeem').click(function() {
            window.location = '<?php echo site_url('guinness/redeem/form?loc=1'); ?>'
        });
        $('#registrasi').click(function() {
            window.location = '<?php echo site_url('landing/home?url=guinness'); ?>'
        });
    });
</script>