<h2><?php echo $data->account_displayname; ?></h2>
<hr>
<span>Redeem point: </span>
<span style="font-size: 120%"><strong><?php echo $mypoint; ?></strong></span>
<br><br>
<div>
    <?php if($redeem != 2): ?>
        Silahkan Pilih Promo
    <?php else: ?>
        Silahkan Pilih Discount
    <?php endif; ?>
    <?php foreach($promo as $row):?>
        <a href="<?php echo site_url('guinness/redeem/takeredeem/'.$redeem.'/'.$row->id.'/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>">
            <button type="button" class="btn btn-primary btn-block btn-xlg"><?php echo $row->promo; ?> (<?php echo $row->point; ?> points)</button>
        </a><br >
    <?php endforeach; ?>
</div>
<hr>
<button type="button" id="back" class="btn btn-default btn-lg btn-xlg"><i class="fa fa-chevron-left">&nbsp;</i>Back</button>


<script type="text/javascript">
    $(function() {
        $('#back').click(function() {
            window.location = '<?php echo base_url(); ?>guinness/redeem/found/<?php echo $loc; ?>/<?php echo $data->id; ?>/<?php echo $data->account_username; ?>'
                    });
                });
</script>