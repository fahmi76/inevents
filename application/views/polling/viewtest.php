<link rel="stylesheet" type="text/css" href="<?php echo $assets_url; ?>/grafik/css/jquery.jqplot.css" />
<script src="<?php echo $assets_url; ?>/grafik/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $assets_url; ?>/grafik/js/jquery.jqplot.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $assets_url; ?>/grafik/js/plugins/jqplot.pieRenderer.js"></script>

<script class="code" type="text/javascript">
	var $j = jQuery.noConflict();
	$j(document).ready(function(){
	// kita masukkan jumlah total ditas kemari
		plot1 = $j.jqplot('pie', [[['REAL MADRID',<?php echo $data->pollingA; ?>],['ATLETICO MADRID', <?php echo $data->pollingB; ?>]]], {
				gridPadding: {top:0, bottom:38, left:0, right:0},
		      seriesDefaults:{renderer:$j.jqplot.PieRenderer, trendline:{show:false}, rendererOptions: { padding: 20, showDataLabels: true}},
		                  legend:{
		                      show:true, 
		                      placement: 'outside', 
		                      rendererOptions: {
		                          numberRows: 1
		                      }, 
		                      location:'s',
		                      marginTop: '15px'
		                  }       
		    });
	});
</script>
<div id="pie" style="margin-top:20px; margin-left:20px; width:400px; height:400px;"></div>