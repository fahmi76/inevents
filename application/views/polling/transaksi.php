<script type="text/javascript">
    $(document).ready(function() {
        $('#InputTransaction').focus();
    });
</script>
<script language="javascript">

    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + '.' +
                    num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + ' ' + num + ',' + cents);
    }
</script>
<h2><?php echo $data->account_displayname; ?></h2>
<hr>
<span>Redeem point: </span>
<span style="font-size: 120%"><strong><?php echo $mypoint; ?></strong></span>
<br><br>
<?php
if (validation_errors()) {
    echo validation_errors('<p class="error">Error! ', '</p>');
}
?> 
<form role="form" method="post" action="<?php echo current_url(); ?>">
    <div class="form-group">
        <label for="InputTransaction">Amount</label>
        <div class="input-group amount">
            <span class="input-group-addon">Rp</span>
            <input type="text" name="transaksi" class="form-control input-lg" id="InputTransaction" placeholder="100000" onkeyup="document.getElementById('format').innerHTML = formatCurrency(this.value);" value="<?php set_value('transaksi'); ?>" autocomplete="off">
            <span  id="format"></span>
        </div>  
    </div>
    <div class="form-group">
        <label for="InputTransaction">Kode Transaksi</label>
        <div class="input-group amount"><span class="input-group-addon">Kode</span> <br />
            <input type="text" name="kodetrans" class="form-control input-lg" value="" autocomplete="off"/><br />
        </div>  
    </div>
    <hr>
    <div class="text-center">
        <button type="button" id="back" class="btn btn-default btn-lg btn-xlg"><i class="fa fa-chevron-left">&nbsp;</i>Back</button>
        <button type="submit" id="next" class="btn btn-primary btn-xlg">Next</button>  
    </div>
</form>

<script type="text/javascript">
    $(function() {
        $('#back').click(function() {
            window.location = '<?php echo base_url(); ?>magnum/redeem/found/<?php echo $loc; ?>/<?php echo $data->id; ?>/<?php echo $data->account_username; ?>'
                    });
                });
</script>