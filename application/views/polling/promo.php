<h2><?php echo $data->account_displayname; ?></h2>
<hr>
<span>Redeem point: </span>
<span style="font-size: 120%"><strong><?php echo $mypoint; ?></strong></span>
<br><br>
<div>
    <?php if ($promo[0]->status == 1): ?>
        <button type="button" id="promo" class="btn btn-primary btn-block btn-xlg">Redeem Promo</button>  
    <?php else: ?>
        <button type="button" class="btn btn-primary btn-block btn-xlg">No Promo Available</button>
    <?php endif; ?>
	<?php if ($promo[1]->status == 1): ?>
        <button type="button" id="discount" class="btn btn-primary btn-block btn-xlg">Redeem Discount</button>  
    <?php else: ?>
        <button type="button" class="btn btn-primary btn-block btn-xlg">No Discount Available</button>
    <?php endif; ?>
    <?php if ($birthday == 1): ?>
        <button type="button" id="birthday" class="btn btn-primary btn-block btn-xlg">Birthday Surprise</button>
    <?php endif; ?>
</div>
<hr>
<button type="button" id="back" class="btn btn-default btn-lg btn-xlg"><i class="fa fa-chevron-left">&nbsp;</i>Back</button>


<script type="text/javascript">
    $(function() {
        $('#birthday').click(function() {
            window.location = '<?php echo site_url('guinness/redeem/redeems/3/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>'
        });
        $('#promo').click(function() {
            window.location = '<?php echo site_url('guinness/redeem/redeems/1/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>'
        });
        $('#discount').click(function() {
            window.location = '<?php echo site_url('guinness/redeem/redeems/2/' . $loc . '/' . $data->id . '/' . $data->account_username); ?>'
        });
        $('#back').click(function() {
            window.location = '<?php echo base_url(); ?>guinness/redeem/found/<?php echo $loc; ?>/<?php echo $data->id; ?>/<?php echo $data->account_username; ?>'
                    });
                });
</script>