<?php
session_start();
$_SESSION['photo_upload_time'] = microtime(true);

if(isset($GLOBALS["HTTP_RAW_POST_DATA"])){
	$jpg = $GLOBALS["HTTP_RAW_POST_DATA"];
	$img = $_GET["img"];
	$namafile = $_GET["namafile"];
	$filename = "/home/woozin/public_html/uploads/photobooth/" . $namafile . ".jpg";
	file_put_contents($filename, $jpg);

	$thumb = imagecreatetruecolor(640, 480);
	$source = imagecreatefromjpeg($filename);

	imagecopyresampled($thumb, $source, 0, 0, 0, 0, 648, 486, 648, 486);
	imagejpeg($thumb, $filename, 100);
} else{
	echo "Encoded JPEG information not received.";
}
?>