$(document).ready(function() {
    var pos = 0, ctx = null, saveCB, image = [];
    w = 1200;
    h = 1800;
    y = 4;

    if (page === 'flash') {
        function home() {
            $("#choice").hide();
            $("#webcam").webcam({
                width: 1200,
                height: 1800,
                mode: "callback",
                swffile: base_url + "assets/magnum/frame/jscam.swf",
                quality: 100
            });
            $("#name").focus();

            $("#name").keyup(function(e) {
                if (e.keyCode == 13) {
                    $.ajax({
                        url: urlrfidhome,
                        type: "POST",
                        data: {
                            rfid: $('#name').val(),
                        },
                        dataType: 'json',
                        success: function(data) {
                            if (data.nama == "ada")
                            {
                                $("#input").hide();
                                tagname = data.alamat;
                                userid = data.id;
                                camera();
                            }
                            else
                            {
                                $("#input").hide();
                                //$("#message1").html("<div class='notice'>Error Your Wristband is Not Registered.</br>Please Try Again</div>");
                                $("#message1").html("<div class='notice'>Error Your Card is Not Registered.</br>Please Try Again</div>");
                                setTimeout(window.location.replace(ptbh_url), 500);
                                return false;
                            }
                        }
                    });
                }
                return false;
            });

            $('button[type="submit"]').click(function() {
                submit();
                return false;
            });
        }

        function camera() {
            $("#canvas_set").hide();
            $("#status").show();
            $("#choice").hide();
            $("#webcam").webcam({
                width: 0,
                height: 0,
                mode: "callback",
                swffile: base_url + "assets/magnum/frame/jscam.swf",
                quality: 100,
                onTick: function(remain) {
                    if (0 == remain) {
                        $("#status").html("Cheese!");
                    } else if (remain < 6) {
                        $("#status").html("Hi, " + tagname + "! Prepare your pose<br />" + remain + " seconds remaining...");
                    } else {
                        $("#status").html("Hi, " + tagname + "! Prepare your pose");
                    }
                },
                onSave: saveCB,
                onCapture: function() {
                    webcam.save();
                    $("#status").hide();
                    $("#choice").show();
                    $("#notes").html("Hi, " + tagname + ", What do you want to do next?");
                    $("#choices").show();
                },
                onLoad: function() {
                    webcam.capture(10);
                }
            });
        }
        
        function camera1() {
            $("#canvas_set").hide();
            $("#status").show();
            $("#choice").hide();
            $("#webcam").html("<div class='frame_overlay'><img class='overlay' src=' " + frame_normal + "' /></div>");
            $("#webcam").webcam({
                width: 0,
                height: 0,
                mode: "callback",
                swffile: base_url + "assets/magnum/frame/jscam.swf",
                quality: 100,
                onTick: function(remain) {
                    if (0 == remain) {
                        $("#status").html("Cheese!");
                    } else if (remain < 6) {
                        $("#status").html("Hi, " + tagname + "! Prepare your pose<br />" + remain + " seconds remaining...");
                    } else {
                        $("#status").html("Hi, " + tagname + "! Prepare your pose");
                    }
                },
                onSave: saveCB,
                onCapture: function() {
                    webcam.save();
                    $("#status").hide();
                    $("#choice").show();
                    $("#notes").html("Hi, " + tagname + ", What do you want to do next?");
                    $("#choices").show();
                },
                onLoad: function() {
                    webcam.capture(10);
                }
            });
        }


        var canvas = document.createElement("canvas");
        canvas.setAttribute('width', w);
        canvas.setAttribute('height', h);
        if (canvas.toDataURL) {
            ctx = canvas.getContext("2d");
            image = ctx.getImageData(0, 0, w, h);
            saveCB = function(data) {
                var col = data.split(";");
                var img = image;
                for (var i = 0; i < w; i++) {
                    var tmp = parseInt(col[i]);
                    img.data[pos + 0] = (tmp >> 16) & 0xff;
                    img.data[pos + 1] = (tmp >> 8) & 0xff;
                    img.data[pos + 2] = tmp & 0xff;
                    img.data[pos + 3] = 0xff;
                    pos += y;
                }
                if (pos >= y * w * h) {
                    ctx.putImageData(img, 0, 0);
                    preview = canvas.toDataURL("image/png");
                    $("#preview").attr("src", preview);
                    $("#canvas_set").css("display", "block");
                    $("#webcam").html("");
                    $("#webcam").hide();
                    pos = 0;
                }
            };
        }
        /*
         } else {
         saveCB = function(data) {
         image.push(data);
         pos+= y * w;
         if (pos >= y * w * h) {
         $.post("http://localhost/cam/upload.php", {
         type: "pixel", 
         image: image.join('|')
         }, function(data) {
         $('#canvas').html('<img src="' + data + '" alt="" class="jadi" />');
         });
         pos = 0;
         }
         };
         }
         */
        $("#choice").hide();
        $("#canvas_set").hide();
        $("#status").hide();
        $("#target").show();
        home();

        $("#retry").click(function() {
            $("#canvas_set").hide();
            $("#input").hide();
            $("#webcam").show();
            //$("#status").html("&nbsp;");
            //$("#status").show();

            camera1();
            return false;
        });
    }

    //the starting call
    $(".rfidin").focus();

    $(".refresh").click(function() {
        $("#refresh").html('<img src="' + base_url + 'assets/photobooth/loader.gif" alt="" /> Please Wait...');
    });

    $("#save").click(function() {
        $("#status").html('Please wait while uploading...<br /><img src="' + base_url + 'assets/images/ajax-loader.gif" alt="" />');
        $("#choice").hide();
        $("#status").show();

        $.post(base_url + "assets/magnum/frame/upload.php", {
            type: "data",
            image: $("#preview").attr("src"),
            frame: frame_normal,
            path: events,
            userid: userid
        }, function(data) {
            if (data != '') {
                window.location.replace(main_url + "magnum/frame/results?place="+ urlplaces +"&photos_id=" + data +"&user_id=" + userid)
            } else {
                window.location.replace(this_url);
            }
        });
        return false;
    });

});

