$(document).ready(function(){
	$("#loading").hide();
	$(".error").hide();
	$("#tag-list").show();
	$("#showrfid").hide();

	$("#upload").click(function(){
		var tagurl = $(this).attr('href');
		$("#loading").show();
		$("#showtag").hide();	 
		$("#showrfid").show();
		$("#rfid").focus();
		$(".error").hide();
		$("#loading").hide();
		return false;
	});

	$("#tagme").click(function(){
		var tagurl = $(this).attr('href');
		$("#loading").show();
		$("#showtag").hide();	 
		$("#showrfid").show();
		$("#rfid").focus();
		$(".error").hide();
		$("#loading").hide();
		return false;
	});

    $("#inputtag").bind("submit", function(){
        var rfid_val = $("#rfid").val();
        $("#loading").show();
        $("#form2").hide();				
        $.ajax({
            type: "POST",
            cache: false,
            url: urlrfid,
            data: "rfid="+rfid_val,
            success: function(data) {
                if(data == 1){
                    $("#loading").hide();
                    $("#showtag").load(location.href+" #showtag>*", function(){
						$(this).show();
					});
                } else {
                    $("#loading").hide();
					$(".error").html('Error! '+data);
					$(".error").show();
					$("#form2").show();
					$("#rfid").val("");
                }
            }
        });
        return false;
    });

    $("#inputupload").bind("submit", function(){
        var rfid_val = $("#rfid").val();
        var file_val = $("#file_id").val();
        $("#loading").show();
        $("#form2").hide();				
        $.ajax({
            type: "POST",
            cache: false,
            url: urlupload,
            data: "rfid="+rfid_val+"&file="+file_val,
            success: function(data) {
                if(data == 1){
					window.location = 'http://wooz.in/underwater/phototag';
                } else {
                    $("#loading").hide();
					$(".error").html('Error! '+data);
					$(".error").show();
					$("#form2").show();
					$("#rfid").val("");
                }
            }
        });
        return false;
    });

});