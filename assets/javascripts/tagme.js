// Locales
var addtagtext = "Add Tag";
var tagprocess = "Tagging...";
var removelink = "Remove";

//Placed outside .ready for scoping
var targetX, targetY;
var tagCounter = 0;
var tagname;

function submitTag()
{
    tagValue = $("#tag-name").val();

    $("#tag-wrapper").append('<div id="hotspot-' + tagCounter + '" class="hotspot" style="left:' + targetX + 'px; top:' + targetY + 'px;"><span>' + tagValue + '</span></div>');

    tagCounter++;
    closeTagInput();

    // Changing cursor to default
    $(".overlay1").css({cursor: 'default'});

    // Saving via AJAX
    $.ajax({
        url: urltag,
        type: "POST",
        data: {
            xpos: targetX, ypos: targetY, tagvalue: tagCounter, status: 1
        },
        success: function(data) {
            setTimeout(window.location.replace(urltagdone), 500);
        }

    });
}

function finish()
{
    $("#showtag").hide();
    //$("#message_ajax").append('<div id="target"></div><div id="input"><label for="name">Please tap your Wristband</label><input style="background-color: transparent;border: 0" type="text" id="name"><button style="background-color: transparent;border: 0" type="submit"></button></div>');
    $("#message_ajax").append('<div id="target"></div><div id="input"><label for="name">Please tap your ' + card + '</label><input style="background-color: transparent;border: 0" type="text" id="name"><button style="background-color: transparent;border: 0" type="submit"></button></div>');

    //Give input focus
    $("#name").focus();

    $("#name").keyup(function(e) {
        if (e.keyCode == 13) {
            submit();
        }
    });

    $('button[type="submit"]').click(function() {
        submit();
    });


}

function retrieve()
{
    $("#tag-target").hide();
    for (i = 0; i < totaltags; i++)
    {
        $("#tag-wrapper").append('<div id="hotspot-' + tagCounter + '" class="hotspot" style="left:' + tags[i].posx + 'px; top:' + tags[i].posy + 'px;"><span>' + tags[i].tagvalue + '</span></div>');
        tagCounter++;
    }
}
function closeTagInput()
{
    $("#tag-target").fadeOut();
    $("#tag-input").fadeOut();
    $("#tag-name").val("");
    $("#tagbutton").val(addtagtext)
    $(".overlay1").css({cursor: 'default'});
    $(".overlay1").click(function(e) {
        $("#tag-target").css({display: 'none'});
        $("#tag-input").css({display: 'none'});
    });
}
function removeTag(i, j)
{
    $("#hotspot-item-" + i).fadeOut();
    $("#hotspot-" + i).fadeOut();

    if (j != 0)
    {
        // Deleting via AJAX
        $.post("delete.php", {tagid: j},
        function(data) {
            alert(data);
        }
        );
    }
}
function showTag(i)
{
    $("#hotspot-" + i).addClass("hotspothover");
}
function hideTag(i)
{
    $("#hotspot-" + i).removeClass("hotspothover");
}

function check()
{
    $("#tag-target").hide();
    $("#choice").hide();
    $("#message_ajax").show();
    $("#name").focus();

    $("#name").keyup(function(e) {
        if (e.keyCode == 13) {
            submit();
        }
        return false;
    });

    $('button[type="submit"]').click(function() {
        submit();
        return false;
    });


}

function submit() {
    $("#message_ajax").hide();
    $("#successMessage").hide();
    $("#status").html('Please wait...<br /><img src="http://wooz.in/assets/images/ajax-loader.gif" alt="" />');

    $.ajax({
        url: urlrfidtag,
        type: "POST",
        data: {
            rfid: $('#name').val(),
        },
        dataType: 'json',
        success: function(data) {
            if (data.nama == "ada")
            {
                $("#status").hide();
                tagname = data.alamat;
                $("#message2").html("<div class='notice'> Hello " + data.alamat + "</br><font size=8px>Which one are you?</font></div>");
                $("#message2").change(function() {
                    addtag();
                }).change();
            }
            else if (data.nama == "ulang")
            {
                $("#status").hide();
                $("#message").show();
                setTimeout(function() {
                    $("#message").hide();
                    $("#choice").show();
                }, 500);

                return false;
            } else if (data.nama == "sudah ada")
            {
                setTimeout(window.location.replace(urltagdone), 500);
            }
            else
            {
                $("#status").hide();
                $("#message").show();
                setTimeout(function() {
                    $("#message").hide();
                    $("#choice").show();
                }, 500);

                return false;
            }
        }
    });
}

function addtag()
{
    $(".overlay1").click(function(e) {

        $("#tag-input").css({display: 'block'});

        //Determine area within element that mouse was clicked
        mouseX = e.pageX - $("#tag-wrapper").offset().left;
        mouseY = e.pageY - $("#tag-wrapper").offset().top;

        //Get height and width of #tag-target
        targetWidth = $("#tag-target").outerWidth();
        targetHeight = $("#tag-target").outerHeight();

        //Determine position for #tag-target
        targetX = mouseX - targetWidth / 2;
        targetY = mouseY - targetHeight / 2;

        //Determine position for #tag-input
        inputX = mouseX + targetWidth / 2;
        inputY = mouseY - targetHeight / 2;

        //Animate if second click, else position and fade in for first click
        if ($("#tag-target").css("display") == "block")
        {
            $("#tag-target").animate({left: targetX, top: targetY}, 500);
            $("#tag-input").animate({left: inputX, top: inputY}, 500);
        } else {
            $("#tag-target").css({left: targetX, top: targetY}).fadeIn();
            $("#tag-input").css({left: inputX, top: inputY}).fadeIn();
        }

        //Give input focus
        $("#tag-name").focus();
    });

    $(".overlay1").css({cursor: 'crosshair'});
}


$(document).ready(function() {

    $("#message_ajax").hide();
    $("#message").hide();

    //Dynamically wrap image
    $(".overlay1").wrap('<div id="tag-wrapper"></div>');

    // Creating DIV for names of tags
    $("#tag-wrapper").after('<div id="tags-names"></div>');

    //Dynamically size wrapper div based on image dimensions
    $("#tag-wrapper").css({width: $(".overlay1").outerWidth(), height: $(".overlay1").outerHeight()});

    //Append #tag-target content and #tag-input content
    $("#tag-wrapper").append('<div id="tag-target"></div>');

    // Function to Retrieve Tags
    retrieve();

    $("#tag-wrapper").click(function() {
        //submitTag();
        tagValue = $("#tag-name").val();

        $("#tag-wrapper").append('<div id="hotspot-' + tagCounter + '" class="hotspot" style="left:' + targetX + 'px; top:' + targetY + 'px;"></div>');

        closeTagInput();

        // Changing cursor to default
        $(".overlay1").css({cursor: 'default'});

        // Saving via AJAX
        $.ajax({
            url: urltag,
            type: "POST",
            data: {
                xpos: targetX, ypos: targetY, tagvalue: tagCounter, status: 1
            },
            success: function(data) {
                setTimeout(window.location.replace(urltagdone), 100);
            }

        });
    });

});