//Placed outside .ready for scoping
var targetX, targetY;
var tagCounter = 0;

$(document).ready(function(){
    //Dynamically wrap image
    $("#tagsource").wrap('<div id="tag-wrapper"></div>');

    //Dynamically size wrapper div based on image dimensions
    $("#tag-wrapper").css({
        width: 640,
        height: 480
    });

    //Append #tag-target content and #tag-input content
    $("#tag-wrapper").append('<div id="tag-target"></div><input type="hidden" id="tag-name"" value="'+name+'" />');

    //$("#tag-wrapper").click(function(e){
    $("#tagsource").click(function(e){
        //Determine area within element that mouse was clicked
        mouseX = e.pageX - $("#tag-wrapper").offset().left;
        mouseY = e.pageY - $("#tag-wrapper").offset().top;

        //Get height and width of #tag-target
        targetWidth = $("#tag-target").outerWidth();
        targetHeight = $("#tag-target").outerHeight();

        //Determine position for #tag-target
        targetX = mouseX-targetWidth/2;
        targetY = mouseY-targetHeight/2;

        //Determine position for #tag-input
        inputX = mouseX+targetWidth/2;
        inputY = mouseY-targetHeight/2;

        //Animate if second click, else position and fade in for first click
        if($("#tag-target").css("display")=="block")
        {
            $("#tag-target").animate({
                left: targetX + 190,
                top: targetY + 60
            }, 500);
            $("#tag-input").animate({
                left: inputX + 190,
                top: inputY + 60
            }, 500);
        } else {
            $("#tag-target").css({
                left: targetX + 190,
                top: targetY + 60
            }).fadeIn();
            $("#tag-input").css({
                left: inputX + 190,
                top: inputY + 60
            }).fadeIn();
        }

        //Give input focus
        $("#tag-name").focus();
    });

    //If enter button is clicked within #tag-input
    $("#tag-name").keyup(function(e) {
        if(e.keyCode == 13) submitTag();
    });

    $("#tag-wrapper").click(function(){
		tagValue = $("#tag-name:hidden").val();

        $.ajax({
            type	: "POST",
            cache	: false,
            url		: site,
            data	: {
                "tarX" : targetX,
                "tarY" : targetY,
                "tagCounter" : tagCounter,
                "status" : 1
            },
            success: function(data) {
                if(data==1) {
					tagCounter++;
					$("#tag-target").fadeOut();
					$("#tagsource").unbind('click');
					$("#tag-wrapper").unbind('click');
					window.location.replace(urltag);
                } else {
                    return false;
                }
            }
        });
    });

});

/*
function submitTag()
{
    tagValue = $("#tag-name:hidden").val();
        $.ajax({
            type	: "POST",
            cache	: false,
            url		: site_url + 'home/loginpopup',
            data	: {
                "accname" : tagValue,
                "tarX" : targetX,
                "tarY" : targetY,
                "tagCounter" : tagCounter,
                "status" : 1,
            },
            success: function(data) {
                if(data==1) {
                    $("#login_form").dialog('close');
                    $(window.location).attr('href', $('#login_next').val() );
                    return true;
                } else {
                    $("#login_error").html(data);
                    $("#login_error").show();
                    return false;
                }
            }
        });
        return false;
	$.ajax({
        type: "POST",
        url: site,
        data: "accname="+tagValue+"&tarX="+targetX+"&tarY="+targetY+"&tagCounter="+tagCounter+"&status=1"
    });
	//Adds a new list item below image. Also adds events inline since they are dynamically created after page load
    //$("#tag-wrapper").after('<p id="hotspot-item-' + tagCounter + '">-. <span class="remove" onmouseover="showTag(' + tagCounter + ')" onmouseout="hideTag(' + tagCounter + ')">' + tagValue + '</span></p>');

    //Adds a new hotspot to image
    //$("#tag-wrapper").append('<div id="hotspot-' + tagCounter + '" class="hotspot" style="left:' + targetX + 'px; top:' + targetY + 'px;"><span>' + tagValue + '</span></div>');

    tagCounter++;
    $("#tag-target").fadeOut();
//window.location.reload();
//$("#tag-other").show('slow');
//$("#tag-list").show('slow');
//$("#tag-welc").hide('slow');
//closeTagInput();
}
*/

function closeTagInput() {
    $("#tag-target").fadeOut();
    $("#tag-input").fadeOut();
    $("#tag-name").val("");
}

function removeTag(i) {
    $("#hotspot-item-"+i).fadeOut();
    $("#hotspot-"+i).fadeOut();
}

function showTag(i) {
    $("#hotspot-"+i).addClass("hotspothover");
}

function hideTag(i) {
    $("#hotspot-"+i).removeClass("hotspothover");
}// JavaScript Document