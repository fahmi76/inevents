$(document).ready(function(){
	$("#loading").hide();
	$("#showrfid").hide();
	$("#tag-list").show();
	$("#tagme").click(function(){
		var tagurl = $(this).attr('href');
		$("#loading").show();
		$("#showtag").hide();

		$("#showrfid").show();
		$("#rfid").focus();
		$(".error").hide();
		$("#loading").hide();
		return false;
	});

    $("#inputtag").bind("submit", function(){
        var rfid_val = $("#rfid").val();
        $("#loading").show();
        $("#form2").hide();				
        $.ajax({
            type: "POST",
            cache: false,
            url: urlrfid,
            data: "rfid="+rfid_val,
            success: function(data) {
                if(data == 1){
                    $("#loading").hide();
                    $("#showtag").load(location.href+" #showtag>*", function(){
						$(this).show();
					});
                } else {
                    $("#loading").hide();
					$(".error").html('Error! '+data);
					$(".error").show();
					$("#form2").show();
					$("#rfid").val("");
                }
            }
        });
        return false;
    });

/*
		$("#showrfid").load(tagurl,function(){
			$("#loading").hide();
		});
*/
});