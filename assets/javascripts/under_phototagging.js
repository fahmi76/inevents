//Placed outside .ready for scoping
var targetX, targetY;
var tagCounter = 0;

$(document).ready(function(){
    //Dynamically wrap image
    $("#tagsource").wrap('<div id="tag-wrapper"></div>');

    //Dynamically size wrapper div based on image dimensions
    $("#tag-wrapper").css({
        width: 640,
        height: 480
    });

    //Append #tag-target content and #tag-input content
    $("#tag-wrapper").append('<div id="tag-target"></div><input type="hidden" id="tag-name"" value="'+name+'" />');

    //$("#tag-wrapper").click(function(e){
    $("#tagsource").click(function(e){
		widh = $(document).width();
		marg = (widh - 1000) / 2;
		test = (1000-640) / 2;
		post = marg + test;

        //Determine area within element that mouse was clicked
        mouseX = e.pageX - $("#tag-wrapper").offset().left;
        mouseY = e.pageY - $("#tag-wrapper").offset().top;

        //Get height and width of #tag-target
        targetWidth = $("#tag-target").outerWidth();
        targetHeight = $("#tag-target").outerHeight();

        //Determine position for #tag-target
        targetX = mouseX-targetWidth/2;
        targetY = mouseY-targetHeight/2;

        //Determine position for #tag-input
        inputX = mouseX+targetWidth/2;
        inputY = mouseY-targetHeight/2;

        //Animate if second click, else position and fade in for first click
        if($("#tag-target").css("display")=="block")
        {
            $("#tag-target").animate({
                left: targetX + post,
                top: targetY + 40
            }, 500);
            $("#tag-input").animate({
                left: inputX + post,
                top: inputY + 40
            }, 500);
        } else {
            $("#tag-target").css({
                left: targetX + post,
                top: targetY + 40
            }).fadeIn();
            $("#tag-input").css({
                left: inputX + post,
                top: inputY + 40
            }).fadeIn();
        }

        //Give input focus
        $("#tag-name").focus();
    });

    //If enter button is clicked within #tag-input
    $("#tag-name").keyup(function(e) {
        if(e.keyCode == 13) submitTag();
    });

    $("#tag-wrapper").click(function(){
		tagValue = $("#tag-name:hidden").val();

        $.ajax({
            type	: "POST",
            cache	: false,
            url		: site,
            data	: {
                "tarX" : targetX,
                "tarY" : targetY,
                "tagCounter" : tagCounter,
                "status" : 1
            },
            success: function(data) {
                if(data==1) {
					tagCounter++;
					$("#tag-target").fadeOut();
					$("#tagsource").unbind('click');
					$("#tag-wrapper").unbind('click');
					window.location.replace(urltag);
                } else {
                    return false;
                }
            }
        });
    });

});
function closeTagInput() {
    $("#tag-target").fadeOut();
    $("#tag-input").fadeOut();
    $("#tag-name").val("");
}

function removeTag(i) {
    $("#hotspot-item-"+i).fadeOut();
    $("#hotspot-"+i).fadeOut();
}

function showTag(i) {
    $("#hotspot-"+i).addClass("hotspothover");
}

function hideTag(i) {
    $("#hotspot-"+i).removeClass("hotspothover");
}