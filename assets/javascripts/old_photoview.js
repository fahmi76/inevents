//Placed outside .ready for scoping
var targetX, targetY;
var tagCounter = 0;

$(document).ready(function(){
	 //Dynamically wrap image
	$("img").wrap('<div id="tag-wrapper"></div>');
	
	 //Dynamically size wrapper div based on image dimensions
	$("#tag-wrapper").css({width: $("img").outerWidth(), height: $("img").outerHeight()});
	
	 

}); //$(document).ready
 
function submitTag()
 {
 tagValue = $("#tag-name").val();

 //Adds a new list item below image. Also adds events inline since they are dynamically created after page load
 $("#tag-wrapper").after('<p id="hotspot-item-' + tagCounter + '">' + tagValue + ' <span class="remove" onclick="removeTag(' + tagCounter + ')" onmouseover="showTag(' + tagCounter + ')" onmouseout="hideTag(' + tagCounter + ')">(Remove)</span></p>');

 //Adds a new hotspot to image
 $("#tag-wrapper").append('<div id="hotspot-' + tagCounter + '" class="hotspot" style="left:' + targetX + 'px; top:' + targetY + 'px;"><span>' + tagValue + '</span></div>');

 tagCounter++;
 $.ajax({
		type: "POST",  
		url: base_url+"photo/tagname",  
		data: "accname="+tagValue+"&tarX="+targetX+"&tarY="+targetY+"&tagCounter="+tagCounter
		});
		$("#ichank").show('slow');
 closeTagInput();
 }

 function closeTagInput()
 {
 $("#tag-target").fadeOut();
 $("#tag-input").fadeOut();
 $("#tag-name").val("");
 }

 function removeTag(i)
 {
 $("#hotspot-item-"+i).fadeOut();
 $("#hotspot-"+i).fadeOut();
 }

 function showTag(i)
 {
 $("#hotspot-"+i).addClass("hotspothover");
 }

 function hideTag(i)
 {
 $("#hotspot-"+i).removeClass("hotspothover");
 }// JavaScript Document