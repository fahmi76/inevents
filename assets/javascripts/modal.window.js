$(document).ready(function() {
    $('a[name=modal]').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('href');

        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        $('#mask').css({
            'width':maskWidth,
            'height':maskHeight
        });

        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow",0.8);

        var winH = $(window).height();
        var winW = $(window).width();

        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);

        $(id).fadeIn(2000);
    });

    $('.window .close').click(function (e) {
        e.preventDefault();

        $('#mask').hide();
        $('.window').hide();
    });

    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });

    $("#sitefb").hide();
    $("#sitetw").hide();
    $("#sitefs").hide();
    $("#sitegw").hide();

    $("#linkbasic").click(function(){
        $("#sitefb").hide();
        $("#sitetw").hide();
        $("#sitefs").hide();
        $("#sitegw").hide();
        $("#sitebasic").fadeIn(1000);
        $("#sitebasic").fadeTo("slow",0.8);
        return false;
    });

    $("#linkfb").click(function(){
        $("#sitebasic").hide();
        $("#sitetw").hide();
        $("#sitefs").hide();
        $("#sitegw").hide();
        $("#sitefb").fadeIn(1000);
        $("#sitefb").fadeTo("slow",0.8);
        return false;
    });

    $("#linktw").click(function(){
        $("#sitebasic").hide();
        $("#sitefb").hide();
        $("#sitefs").hide();
        $("#sitegw").hide();
        $("#sitetw").fadeIn(1000);
        $("#sitetw").fadeTo("slow",0.8);
        return false;
    });

    $("#linkfs").click(function(){
        $("#sitebasic").hide();
        $("#sitefb").hide();
        $("#sitetw").hide();
        $("#sitegw").hide();
        $("#sitefs").fadeIn(1000);
        $("#sitefs").fadeTo("slow",0.8);
        return false;
    });

    $("#linkgw").click(function(){
        $("#sitebasic").hide();
        $("#sitetw").hide();
        $("#sitefb").hide();
        $("#sitefs").hide();
        $("#sitegw").fadeIn(1000);
        $("#sitegw").fadeTo("slow",0.8);
        return false;
    });

    $('#modalsubmit').click(function(e){
        e.preventDefault();
        
        $.post(url, {
            site_name : $("#site_name").val(),
            site_description : $("#site_description").val(),
            site_url : $("#site_url").val(),
            site_mobile_url : $("#site_mobile_url").val(),
            site_status : $("#site_status").val(),
            facebook_id : $("#facebook_id").val(),
            facebook_key : $("#facebook_key").val(),
            facebook_secret : $("#facebook_secret").val(),
            facebook_perms : $("#facebook_perms").val(),
            facebook_callback : $("#facebook_callback").val(),
            twitter_key : $("#twitter_key").val(),
            twitter_secret : $("#twitter_secret").val(),
            twitter_callback : $("#twitter_callback").val(),
            foursquare_key : $("#foursquare_key").val(),
            foursquare_secret : $("#foursquare_secret").val(),
            foursquare_callback : $("#foursquare_callback").val(),
            gowalla_key : $("#gowalla_key").val(),
            gowalla_secret : $("#gowalla_secret").val(),
            gowalla_callback : $("#gowalla_callback").val()
        }, function(data) {
            $("#formresult").load(url,function(){
                $(this).html(data);
                if($("#site_name").val() != '' && $("#site_description").val() != '' && $("#site_url").val() != ''){
                    $("span").hide();
                    $("#modalform").hide();
                    
                    setTimeout(function(){
                        $('#mask').fadeOut('slow', function () {
                            $(this).remove();
                        });
                        $('.window').fadeOut('slow', function () {
                            $(this).remove();
                        });
                        window.location.reload();
                    }, 100);
                    
                } else {
                    return true;
                }
            });
        });        
    });
});