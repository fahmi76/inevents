// Locales
var addtagtext = "Add Tag";
var tagprocess = "Tagging...";
var removelink = "Remove";

//Placed outside .ready for scoping
var targetX, targetY;
var tagCounter = 0;

function submitTag()
{
	tagValue = $("#tag-name").val();	
	
	//Adds a new list item below image. Also adds events inline since they are dynamically created after page load
	$("#tags-names").append('<p id="hotspot-item-' + tagCounter + '" style="display: inline;">' + tagValue + '</p>&nbsp;');
	
	//Adds a new hotspot to image
	$("#tag-wrapper").append('<div id="hotspot-' + tagCounter + '" class="hotspot" style="left:' + targetX + 'px; top:' + targetY + 'px;"><span>' + tagValue + '</span></div>');
	
	tagCounter++;
	closeTagInput();

	// Changing cursor to default
	$(".photo_to_tag").css({cursor: 'default'});

	// Saving via AJAX
	$.ajax({
		url: "http://wooz.local/tag/save",
		type: "POST",
		data: {
			xpos: targetX, ypos: targetY, tagvalue: tagValue
		},
        success: function(data){
	    	alert(data);
	  	}
	});					
					
	//$.post("save.php",
	  //{ xpos: targetX, ypos: targetY, tagvalue: tagValue },
	  //function(data){
	    //alert(data);
	  //}
	//);
}
function retrieve()
{
	for (i=0;i<totaltags;i++)
	{
	//Adds a new list item below image. Also adds events inline since they are dynamically created after page load
	$("#tags-names").append('<p id="hotspot-item-' + tagCounter + '" style="display: inline;">' + tags[i].tagvalue + ' <span class="remove" onclick="removeTag(' + tagCounter + ',' + tags[i].tagid + ')" onmouseover="showTag(' + tagCounter + ')" onmouseout="hideTag(' + tagCounter + ')">('+removelink+'),</span></p>&nbsp;');
	
	//Adds a new hotspot to image
	$("#tag-wrapper").append('<div id="hotspot-' + tagCounter + '" class="hotspot" style="left:' + tags[i].posx + 'px; top:' + tags[i].posy + 'px;"><span>' + tags[i].tagvalue + '</span></div>');

	tagCounter++;
	}
}
function closeTagInput()
{
	$("#tag-target").fadeOut();
	$("#tag-input").fadeOut();
	$("#tag-name").val("");
	$("#tagbutton").val(addtagtext)
	$(".photo_to_tag").css({cursor: 'default'});
	$(".photo_to_tag").click(function(e){
		$("#tag-target").css({display: 'none'});
		$("#tag-input").css({display: 'none'});
		});
}
function removeTag(i,j)
{
	$("#hotspot-item-"+i).fadeOut();
	$("#hotspot-"+i).fadeOut();

	if (j != 0)
	{
	// Deleting via AJAX
	$.post("delete.php",
	  { tagid: j },
	  function(data){
	    alert(data);
	  }
	);
	}
}
function showTag(i)
{
	$("#hotspot-"+i).addClass("hotspothover");
}
function hideTag(i)
{
	$("#hotspot-"+i).removeClass("hotspothover");
}
function addtag()
{
	// Changing Button text
	$("#tagbutton").hide();

	$(".photo_to_tag").click(function(e){

		$("#tag-target").css({display: 'block'});

		//Determine area within element that mouse was clicked
		mouseX = e.pageX - $("#tag-wrapper").offset().left;
		mouseY = e.pageY - $("#tag-wrapper").offset().top;
		
		//Get height and width of #tag-target
		targetWidth = $("#tag-target").outerWidth();
		targetHeight = $("#tag-target").outerHeight();
		
		//Determine position for #tag-target
		targetX = mouseX-targetWidth/2;
		targetY = mouseY-targetHeight/2;
		
		//Determine position for #tag-input
		inputX = mouseX+targetWidth/2;
		inputY = mouseY-targetHeight/2;
		
		//Animate if second click, else position and fade in for first click
		if($("#tag-target").css("display")=="block")
		{
			$("#tag-target").animate({left: targetX, top: targetY}, 500);
		} else {
			$("#tag-target").css({left: targetX, top: targetY}).fadeIn();
		}
		
		//Give input focus
                $("#tag-input").css({display: 'block'});
		$("#tag-name").focus();	
	});

	$(".photo_to_tag").css({cursor: 'crosshair'});
}


$(document).ready(function(){

	//Dynamically wrap image
	$(".photo_to_tag").wrap('<div id="tag-wrapper"></div>');

	// Creating DIV for names of tags
	$("#tag-wrapper").after('<div id="tags-names"></div>');
	
	//Dynamically size wrapper div based on image dimensions
	$("#tag-wrapper").css({width: $(".photo_to_tag").outerWidth(), height: $(".photo_to_tag").outerHeight()});

	//Append #tag-target content and #tag-input content
	$("#tag-wrapper").append('<div id="tag-target"></div><div id="tag-input"><label for="tag-name">Person Name:</label><input type="text" id="tag-name"><button type="submit">Submit</button><button type="reset">Cancel</button></div>');

	// Function to Retrieve Tags
	retrieve();

	//$("#tag-wrapper").click(function(e){

	//If cancel button is clicked
	$('button[type="reset"]').click(function(){
		closeTagInput();
	});
	
	//If enter button is clicked within #tag-input
	$("#tag-name").keyup(function(e) {
		if(e.keyCode == 13) submitTag();
	});	
	
	//If submit button is clicked
	$('button[type="submit"]').click(function(){
		submitTag();
	});

});