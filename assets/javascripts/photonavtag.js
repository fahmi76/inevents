$(document).ready(function(){
	$("#loading").hide();
	$("#tag-list").show();
	$("#tag a").click(function(){
		var tagurl = $(this).attr('href');
		$('#rfid').focus();
		$("#loading").show();
		$("#showtag").hide();
		$("#showrfid").load(tagurl,function(){
			$("#loading").hide();
			$("#rfid").focus();
			$('form').submit(function(){
				var rfid_val = $("#rfid").val();
				$("#loading").show();
				$("#form2").hide();
				$.ajax({
					type: "POST",
					cache: false,
					url: urlrfid,
					data: "rfid="+rfid_val,
					success: function(data) {
						if(data == 0){							
							$("#showrfid").hide();						
							window.location.replace(urltag,function(){
								$("#loading").hide();
								$("#button").hide();
								//$("#photo").hide(function(){$(this).show('slow')});
								$("#showtag").show();
							});
						} else {
							$("#loading").hide();
							$("#showrfid").html(data);
						}
					}
				});
				return false;
			});
		});
		return false;
	});
});