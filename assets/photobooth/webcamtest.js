$(document).ready(function(){
    var pos = 0, ctx = null, saveCB, image = [];
    w = 640;
    h = 480;
    y = 4;
    
    if(page === 'flash' ) {
        function camera() {
            $("#choice").hide();
            $("#webcam").webcam({
                width: 1000,
                height: 750,
                mode: "callback",
                swffile: "../assets/photobooth/photobooth_640.swf",
                quality: 100,
                onTick: function(remain) {
                    if (0 == remain) {
                        $("#status").html("Cheese!");
                    } else if ( remain < 7 ) {
                        $("#status").html("Hi, " + name + "! Prepare your pose<br />" + remain + " seconds remaining...");
                    } else {
                        $("#status").html("Hi, " + name + "! Prepare your pose");
					}
                },
                onSave: saveCB,
                onCapture: function () {
                    webcam.save();
                    $("#status").hide();
					$("#choice").show();
                },
                onLoad: function () {
                    webcam.capture(10);
                }
            });
        }

        var canvas = document.createElement("canvas");
        canvas.setAttribute('width', w);
        canvas.setAttribute('height', h);
        if (canvas.toDataURL) {
            ctx = canvas.getContext("2d");
            image = ctx.getImageData(0, 0, w, h);
            saveCB = function(data) {
                var col = data.split(";");
                var img = image;
                for(var i = 0; i < w; i++) {
                    var tmp = parseInt(col[i]);
                    img.data[pos + 0] = (tmp >> 16) & 0xff;
                    img.data[pos + 1] = (tmp >> 8) & 0xff;
                    img.data[pos + 2] = tmp & 0xff;
                    img.data[pos + 3] = 0xff;
                    pos+= y;
                }
                if (pos >= y * w * h) {
                    ctx.putImageData(img, 0, 0);
                    preview = canvas.toDataURL("image/png");
                    $("#preview").attr("src", preview);
                    $("#canvas_set").css("display", "block");
		            $("#webcam").html("");
                    $("#webcam").hide();
                    pos = 0;
                }
            };
        }
        /*
    } else {
        saveCB = function(data) {
            image.push(data);
            pos+= y * w;
            if (pos >= y * w * h) {
                $.post("http://localhost/cam/upload.php", {
                    type: "pixel", 
                    image: image.join('|')
                }, function(data) {
                    $('#canvas').html('<img src="' + data + '" alt="" class="jadi" />');
                });
                pos = 0;
            }
        };
    }
    */
        $("#canvas_set").hide();
        camera();

        $("#retry").click(function() {
            $("#canvas_set").hide();
            $("#webcam").show();
            $("#status").html("&nbsp;");
            $("#status").show();
			
            camera();
            return false;
        });
    }

    //the starting call
    $(".rfidin").focus();

    $(".refresh").click(function() {
        $("#refresh").html('<img src="' + base_url + 'assets/photobooth/loader.gif" alt="" /> Please Wait...');
    });

    $("#save").click(function() {
		$("#status").html('Please wait while uploading...<br /><img src="' + base_url + 'assets/images/ajax-loader.gif" alt="" />');
		$("#choice").hide();
        $("#status").show();

        $.post( base_url + "assets/photobooth/upload.php", {
            type: "data", 
            image: $("#preview").attr("src"),
            frame: frame_normal,
			path: events
        }, function(data) {
			if(data != '') {
				window.location.replace(main_url + "testphotobooth/result?image=" + data)
			} else {
                window.location.replace(this_url);
			}
        });        
        return false;
    });

});

