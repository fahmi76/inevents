<?php
session_start();
$_SESSION['photo_upload_time'] = microtime(true);

if(isset($_POST['path'])&&$_POST['path']!='')
	$path = $_POST['path'] . '_';
else
	$path = 'woozin_';

$realname = $path . time() . '.jpg';
$filename = '/home/wooz_in/public_html/uploads/photobooth/' . $realname;
$w = 480;
$h = 640;
/* photo setup */
$targetImage = imagecreatetruecolor($w, $h);
imagecolorallocate($targetImage, 0, 0, 0);

$default = imagecreatetruecolor($w, $h);
if (isset($_POST['image']) && $_POST['image'] != '') {
    if ($_POST['type'] == "pixel") {
        foreach (explode("|", $_POST['image']) as $y => $csv) {
            foreach (explode(";", $csv) as $x => $color) {
                imagesetpixel($default, $x, $y, $color);
            }
        }
    } else {
        $photo = imagecreatefrompng($_POST['image']);
        imagecopyresampled($default, $photo, 0, 0, 0, 0, $w, $h, $w, $h);
    }
    /* put photo to target file */
    imagecopyresampled($targetImage, $default, 0, 0, 0, 0, $w, $h, $w, $h);

    if (isset($_POST['frame']) && $_POST['frame'] != '') {
        /* calling framing method */
        $srcImage = imagecreatefrompng('/home/wooz_in/public_html'.$_POST['frame']);
        /*
          imagealphablending( $targetImage, false );
          imagesavealpha( $targetImage, true );
         */
        imagecopyresampled($targetImage, $srcImage, 0, 0, 0, 0, $w, $h, $w, $h);
    }

	imagejpeg($targetImage, $filename, 100);
    imagedestroy($default);
    imagedestroy($srcImage);
    imagedestroy($targetImage);
    echo $realname;
}