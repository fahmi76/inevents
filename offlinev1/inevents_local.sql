-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 25, 2017 at 07:06 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inevents_local`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `landing_id` int(10) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `account_displayname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_description` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `areaofinterest` varchar(255) DEFAULT NULL,
  `account_rfid` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT '0',
  `account_joindate` datetime NOT NULL,
  `account_verify` int(5) DEFAULT '0',
  `account_status` tinyint(1) NOT NULL DEFAULT '2',
  `account_upload` int(5) NOT NULL DEFAULT '0',
  `account_lastupdate` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `account_log`
--

CREATE TABLE `account_log` (
  `id` bigint(40) NOT NULL,
  `account_id` int(20) NOT NULL,
  `places_id` int(20) NOT NULL,
  `badge_id` int(20) NOT NULL DEFAULT '0',
  `log_type` tinyint(1) NOT NULL DEFAULT '1',
  `log_date` date NOT NULL,
  `log_time` time NOT NULL,
  `log_hash` varchar(255) DEFAULT NULL,
  `log_status` tinyint(1) NOT NULL,
  `log_stamps` datetime NOT NULL,
  `redeem` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1467182251, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '::1', NULL, '$2y$08$9CsdWNVgIVbq/27Lej5tV.5vkurw737wpbxdNmdR0oXL37QdU.hHG', NULL, 'inevents@wooz.in', NULL, NULL, NULL, NULL, 1467182602, 1495731380, 1, 'inevents', 'inevets', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wooz_gate`
--

CREATE TABLE `wooz_gate` (
  `id` int(10) NOT NULL,
  `places_id` int(10) NOT NULL DEFAULT '0',
  `nama` varchar(50) DEFAULT NULL,
  `data_status` int(10) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `wooz_places`
--

CREATE TABLE `wooz_places` (
  `id` int(20) UNSIGNED NOT NULL,
  `places_name` varchar(255) DEFAULT NULL,
  `places_nicename` varchar(255) DEFAULT NULL,
  `places_address` text,
  `places_logo` varchar(255) DEFAULT NULL,
  `places_backgroud` varchar(255) DEFAULT NULL,
  `places_desc` text,
  `places_landing` varchar(255) NOT NULL DEFAULT '0',
  `places_status` tinyint(1) NOT NULL DEFAULT '1',
  `places_duedate` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `wooz_places`
--

INSERT INTO `wooz_places` (`id`, `places_name`, `places_nicename`, `places_address`, `places_logo`, `places_backgroud`, `places_desc`, `places_landing`, `places_status`, `places_duedate`) VALUES
(21, 'Fashion Focus', 'Fashion-Focus', 'Trinidad & Tobago', 'assets/images/pp-21.jpg', 'assets/images/pp-21.jpg', 'Fashion Focus', 'Fashion-Focus', 1, '2016-07-14'),
(33, 'RBL Survey', 'Fashion-Focus', 'Trinidad & Tobago', 'assets/images/pp-33.jpg', 'assets/images/pp-33.jpg', 'Fashion Focus', 'Fashion-Focus', 1, '2017-06-05');

-- --------------------------------------------------------

--
-- Table structure for table `wooz_quisioner`
--

CREATE TABLE `wooz_quisioner` (
  `id` int(10) NOT NULL,
  `places_id` int(10) NOT NULL DEFAULT '0',
  `question_id` varchar(50) NOT NULL DEFAULT '0',
  `answer_id` varchar(50) NOT NULL DEFAULT '0',
  `account_id` varchar(50) NOT NULL DEFAULT '0',
  `data_status` int(10) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wooz_quisioner`
--

INSERT INTO `wooz_quisioner` (`id`, `places_id`, `question_id`, `answer_id`, `account_id`, `data_status`, `date_add`) VALUES
(11, 13, '5', '5', '0', 1, '2016-04-25 11:10:53'),
(12, 13, '5', '6', '0', 1, '2016-04-25 11:11:00'),
(13, 13, '5', '6', '0', 1, '2016-04-25 11:11:07'),
(14, 13, '5', '9', '0', 1, '2016-04-25 11:11:13'),
(15, 13, '5', '5', '0', 1, '2016-04-25 11:20:18'),
(16, 13, '5', '7', '0', 1, '2016-04-25 11:20:30'),
(17, 13, '5', '5', '0', 1, '2016-04-25 11:22:17'),
(18, 13, '5', '6', '0', 1, '2016-04-26 21:23:47'),
(19, 13, '5', '5', '0', 1, '2016-04-26 21:25:03'),
(20, 13, '5', '5', '0', 1, '2016-04-26 22:41:35'),
(21, 13, '5', '6', '0', 1, '2016-04-26 22:42:13'),
(22, 13, '5', '7', '0', 1, '2016-04-26 22:43:24'),
(23, 13, '5', '6', '0', 1, '2016-04-26 22:47:32'),
(24, 13, '5', '6', '0', 1, '2016-04-28 14:44:47'),
(25, 13, '5', '7', '0', 1, '2016-05-01 23:46:11'),
(26, 13, '5', '5', '0', 1, '2016-05-02 09:30:44'),
(27, 13, '5', '5', '0', 1, '2016-05-02 09:32:41'),
(28, 13, '5', '5', '0', 1, '2016-05-02 09:32:48'),
(29, 13, '5', '6', '0', 1, '2016-05-02 09:32:55'),
(30, 13, '5', '9', '0', 1, '2016-05-02 09:34:36'),
(31, 13, '5', '5', '0', 1, '2016-05-02 09:35:33'),
(32, 13, '5', '7', '0', 1, '2016-05-02 09:37:46'),
(33, 13, '5', '6', '0', 1, '2016-05-02 11:22:25'),
(34, 13, '5', '6', '0', 1, '2016-05-02 11:25:41'),
(35, 13, '5', '5', '0', 1, '2016-05-02 13:40:12'),
(36, 13, '5', '5', '0', 1, '2016-05-02 13:43:04'),
(37, 13, '5', '7', '0', 1, '2016-05-02 13:43:10'),
(38, 13, '5', '5', '0', 1, '2016-05-02 13:44:56'),
(39, 13, '5', '6', '0', 1, '2016-05-02 13:45:03'),
(40, 13, '5', '7', '0', 1, '2016-05-02 13:45:09'),
(41, 13, '5', '9', '0', 1, '2016-05-02 13:45:14'),
(42, 13, '5', '7', '0', 1, '2016-05-02 14:05:35'),
(43, 13, '5', '5', '0', 1, '2016-05-02 14:06:00'),
(55, 22, '6', '10', 'owkuUyWiibN0nq6Mobgs', 1, '2016-08-17 11:09:08'),
(56, 22, '7', '12', 'owkuUyWiibN0nq6Mobgs', 1, '2016-08-17 11:09:11'),
(57, 22, '6', '11', 'VMF0KfUB91z0WuDa7L5N', 1, '2016-08-17 11:09:19'),
(58, 22, '7', '14', 'VMF0KfUB91z0WuDa7L5N', 1, '2016-08-17 11:09:21'),
(59, 22, '6', '10', 'Q5Pb9mRLhdhrXoRqawaO', 1, '2016-08-17 11:12:36'),
(60, 22, '7', '12', 'Q5Pb9mRLhdhrXoRqawaO', 1, '2016-08-17 11:12:37'),
(61, 22, '6', '11', 'AHqIRqedspfiu5uErlqI', 1, '2016-08-17 11:12:40'),
(62, 22, '7', '13', 'AHqIRqedspfiu5uErlqI', 1, '2016-08-17 11:12:40'),
(63, 22, '6', '11', 'zHax62Xgz75aPvSGV6Uo', 1, '2016-08-17 11:12:45'),
(64, 22, '7', '14', 'zHax62Xgz75aPvSGV6Uo', 1, '2016-08-17 11:12:46'),
(65, 22, '6', '10', 'waG0fbFHw5p6NADTDAac', 1, '2016-08-17 11:12:47'),
(66, 22, '7', '12', 'waG0fbFHw5p6NADTDAac', 1, '2016-08-17 11:12:48'),
(67, 22, '6', '11', 'IfmxLeeHl85RjMSyXxfu', 1, '2016-08-17 11:12:50'),
(68, 22, '7', '13', 'IfmxLeeHl85RjMSyXxfu', 1, '2016-08-17 11:12:51'),
(69, 22, '6', '10', 'MiwbLnEcLAdcsScCxQqd', 1, '2016-08-17 11:15:41'),
(70, 22, '7', '13', 'MiwbLnEcLAdcsScCxQqd', 1, '2016-08-17 11:15:43'),
(71, 22, '8', '16', 'MiwbLnEcLAdcsScCxQqd', 1, '2016-08-17 11:15:46'),
(72, 22, '6', '10', 'JVV35vOoVMsVsqPhBDDo', 1, '2016-08-17 11:20:44'),
(73, 22, '7', '12', 'JVV35vOoVMsVsqPhBDDo', 1, '2016-08-17 11:20:46'),
(74, 22, '8', '15', 'JVV35vOoVMsVsqPhBDDo', 1, '2016-08-17 11:20:47'),
(75, 22, '6', '10', 'C3QdKhMEfnP9GebprdKn', 1, '2016-08-17 11:20:48'),
(76, 22, '7', '12', 'C3QdKhMEfnP9GebprdKn', 1, '2016-08-17 11:20:49'),
(77, 22, '8', '15', 'C3QdKhMEfnP9GebprdKn', 1, '2016-08-17 11:20:49'),
(78, 22, '6', '10', 'RNS9SURrDqaN8foKTnrz', 1, '2016-08-18 09:28:50'),
(79, 22, '7', '13', 'RNS9SURrDqaN8foKTnrz', 1, '2016-08-18 09:28:51'),
(80, 22, '8', '17', 'RNS9SURrDqaN8foKTnrz', 1, '2016-08-18 09:28:52'),
(81, 29, '9', '18', 'pRWbsJkqdo7e49yKmiJj', 1, '2017-02-01 14:55:10'),
(82, 29, '10', '26', 'pRWbsJkqdo7e49yKmiJj', 1, '2017-02-01 14:55:12'),
(83, 29, '11', '30', 'pRWbsJkqdo7e49yKmiJj', 1, '2017-02-01 14:55:14'),
(84, 29, '12', '21', 'pRWbsJkqdo7e49yKmiJj', 1, '2017-02-01 14:55:15'),
(85, 29, '13', '33', 'pRWbsJkqdo7e49yKmiJj', 1, '2017-02-01 14:55:16'),
(86, 29, '9', '18', 'HjA2vq1RvRAfTZPbSAOW', 1, '2017-02-02 19:28:37'),
(87, 29, '10', '26', 'HjA2vq1RvRAfTZPbSAOW', 1, '2017-02-02 19:28:40'),
(88, 29, '11', '20', 'HjA2vq1RvRAfTZPbSAOW', 1, '2017-02-02 19:28:42'),
(89, 29, '12', '21', 'HjA2vq1RvRAfTZPbSAOW', 1, '2017-02-02 19:28:42'),
(90, 29, '13', '42', 'HjA2vq1RvRAfTZPbSAOW', 1, '2017-02-02 19:28:45'),
(91, 29, '14', '44', 'HjA2vq1RvRAfTZPbSAOW', 1, '2017-02-02 19:28:47'),
(92, 29, '9', '18', 'DTAuH4MLYvLCKkdS4C9n', 1, '2017-02-02 19:34:23'),
(93, 29, '10', '26', 'DTAuH4MLYvLCKkdS4C9n', 1, '2017-02-02 19:34:31'),
(94, 29, '11', '35', 'DTAuH4MLYvLCKkdS4C9n', 1, '2017-02-02 19:34:34'),
(95, 29, '12', '41', 'DTAuH4MLYvLCKkdS4C9n', 1, '2017-02-02 19:34:35'),
(96, 29, '13', '22', 'DTAuH4MLYvLCKkdS4C9n', 1, '2017-02-02 19:34:36'),
(97, 29, '14', '45', 'DTAuH4MLYvLCKkdS4C9n', 1, '2017-02-02 19:34:39'),
(98, 29, '9', '38', 'yE2IzCUAQ7VXUNHcwYXL', 1, '2017-02-02 19:34:55'),
(99, 29, '10', '39', 'yE2IzCUAQ7VXUNHcwYXL', 1, '2017-02-02 19:34:56'),
(100, 29, '11', '35', 'yE2IzCUAQ7VXUNHcwYXL', 1, '2017-02-02 19:34:56'),
(101, 29, '12', '24', 'yE2IzCUAQ7VXUNHcwYXL', 1, '2017-02-02 19:34:57'),
(102, 29, '13', '22', 'yE2IzCUAQ7VXUNHcwYXL', 1, '2017-02-02 19:34:58'),
(103, 29, '14', '45', 'yE2IzCUAQ7VXUNHcwYXL', 1, '2017-02-02 19:34:59');

-- --------------------------------------------------------

--
-- Table structure for table `wooz_quisioner_answer`
--

CREATE TABLE `wooz_quisioner_answer` (
  `id` int(10) NOT NULL,
  `server_id` int(10) NOT NULL DEFAULT '0',
  `question_id` int(10) NOT NULL DEFAULT '0',
  `answer` varchar(255) NOT NULL DEFAULT '0',
  `data_status` int(10) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wooz_quisioner_answer`
--

INSERT INTO `wooz_quisioner_answer` (`id`, `server_id`, `question_id`, `answer`, `data_status`, `date_add`) VALUES
(1, 0, 1, '1', 1, '2016-04-25 01:05:16'),
(2, 0, 1, '3', 1, '2016-04-25 01:05:41'),
(3, 0, 1, '1', 1, '2016-04-25 01:05:54'),
(4, 0, 1, '2', 1, '2016-04-25 01:06:08'),
(5, 0, 5, 'Within the Last Three Months', 1, '2016-04-25 08:08:06'),
(6, 0, 5, 'Within the Last Year', 1, '2016-04-25 08:09:12'),
(7, 0, 5, 'Within the Last Three Years', 1, '2016-04-25 08:09:32'),
(9, 0, 5, 'Too Long Ago to Remember', 1, '2016-04-25 08:15:58'),
(10, 0, 6, 'one time per day', 1, '2016-08-16 11:16:27'),
(11, 0, 6, 'two times per day', 1, '2016-08-16 11:16:34'),
(12, 0, 7, 'I Like it very much', 1, '2016-08-16 13:22:46'),
(13, 0, 7, 'I like it somewhat', 1, '2016-08-16 13:22:54'),
(14, 0, 7, 'I dont like it at all', 1, '2016-08-16 13:23:03'),
(15, 0, 8, 'Yes Kinda', 1, '2016-08-17 15:14:54'),
(16, 0, 8, 'Somewhat', 1, '2016-08-17 15:15:04'),
(17, 0, 8, 'Nope', 1, '2016-08-17 15:15:11'),
(18, 0, 9, 'Awesomely Excellent', 1, '2017-01-31 16:05:59'),
(19, 0, 10, 'Excellent', 1, '2017-01-31 16:06:03'),
(20, 0, 11, 'Excellent', 1, '2017-01-31 16:06:10'),
(21, 0, 12, 'Excellent', 1, '2017-01-31 16:06:15'),
(22, 0, 13, 'Excellent', 1, '2017-01-31 16:06:20'),
(23, 0, 13, 'Very Good', 1, '2017-01-31 16:06:32'),
(24, 0, 12, 'Very Good', 1, '2017-01-31 16:06:39'),
(25, 0, 11, 'Very Good', 1, '2017-01-31 16:06:46'),
(26, 0, 10, 'Very Good', 1, '2017-01-31 16:06:52'),
(27, 0, 9, 'Very Good', 1, '2017-01-31 16:07:00'),
(28, 0, 9, 'Okay', 1, '2017-01-31 16:07:12'),
(29, 0, 10, 'Okay', 1, '2017-01-31 16:07:20'),
(30, 0, 11, 'Okay', 1, '2017-01-31 16:07:26'),
(31, 0, 12, 'Okay', 1, '2017-01-31 16:07:33'),
(32, 0, 13, 'Okay', 1, '2017-01-31 16:07:39'),
(33, 0, 13, 'Below Average', 1, '2017-01-31 16:07:50'),
(34, 0, 12, 'Below Average', 1, '2017-01-31 16:07:56'),
(35, 0, 11, 'Below Average', 1, '2017-01-31 16:08:01'),
(36, 0, 10, 'Below Average', 1, '2017-01-31 16:08:08'),
(37, 0, 9, 'Below Average', 1, '2017-01-31 16:08:15'),
(38, 0, 9, 'Poor', 1, '2017-01-31 16:08:25'),
(39, 0, 10, 'Poor', 1, '2017-01-31 16:08:31'),
(40, 0, 11, 'Poor', 1, '2017-01-31 16:08:39'),
(41, 0, 12, 'Poor', 1, '2017-01-31 16:08:44'),
(42, 0, 13, 'Poor', 1, '2017-01-31 16:08:49'),
(43, 0, 14, 'Almost', 1, '2017-02-02 23:27:43'),
(44, 0, 14, 'Yes', 1, '2017-02-02 23:27:57'),
(45, 0, 14, 'Nope', 1, '2017-02-02 23:28:04'),
(46, 0, 15, 'Excellent', 1, '2017-05-24 08:03:33'),
(47, 0, 15, 'Very Good', 1, '2017-05-24 08:03:45'),
(48, 0, 15, 'Okay', 1, '2017-05-24 08:04:03'),
(49, 0, 15, 'Below Average', 1, '2017-05-24 08:04:13'),
(50, 0, 15, 'Poor', 1, '2017-05-24 08:04:19'),
(51, 0, 16, 'Excellent', 1, '2017-05-24 08:04:58'),
(52, 0, 19, 'Excellent', 1, '2017-05-24 08:05:03'),
(53, 0, 18, 'Excellent', 1, '2017-05-24 08:05:06'),
(54, 0, 18, 'Very Good', 1, '2017-05-24 08:05:09'),
(55, 0, 17, 'Excellent', 1, '2017-05-24 08:05:16'),
(56, 0, 16, 'Very Good', 1, '2017-05-24 08:05:27'),
(57, 0, 17, 'Very Good', 1, '2017-05-24 08:05:36'),
(58, 0, 19, 'Very Good', 1, '2017-05-24 08:05:45'),
(59, 0, 16, 'Okay', 1, '2017-05-24 08:05:53'),
(60, 0, 17, 'Okay', 1, '2017-05-24 08:05:57'),
(61, 0, 18, 'Okay', 1, '2017-05-24 08:06:02'),
(62, 0, 19, 'Okay', 1, '2017-05-24 08:06:06'),
(63, 0, 16, 'Below Average', 1, '2017-05-24 08:06:15'),
(64, 0, 17, 'Below Average', 1, '2017-05-24 08:06:21'),
(65, 0, 18, 'Below Average', 1, '2017-05-24 08:06:26'),
(66, 0, 19, 'Below Average', 1, '2017-05-24 08:06:30'),
(67, 0, 16, 'Poor', 1, '2017-05-24 08:06:37'),
(68, 0, 17, 'Poor', 1, '2017-05-24 08:06:42'),
(69, 0, 18, 'Poor', 1, '2017-05-24 08:06:46'),
(70, 0, 19, 'Poor', 1, '2017-05-24 08:06:50'),
(71, 46, 20, 'Excellent', 1, '2017-05-25 12:38:38'),
(72, 47, 20, 'Very Good', 1, '2017-05-25 12:38:38'),
(73, 48, 20, 'Okay', 1, '2017-05-25 12:38:38'),
(74, 49, 20, 'Below Average', 1, '2017-05-25 12:38:38'),
(75, 50, 20, 'Poor', 1, '2017-05-25 12:38:38'),
(76, 51, 21, 'Excellent', 1, '2017-05-25 12:38:38'),
(77, 56, 21, 'Very Good', 1, '2017-05-25 12:38:38'),
(78, 59, 21, 'Okay', 1, '2017-05-25 12:38:38'),
(79, 63, 21, 'Below Average', 1, '2017-05-25 12:38:38'),
(80, 67, 21, 'Poor', 1, '2017-05-25 12:38:38'),
(81, 55, 22, 'Excellent', 1, '2017-05-25 12:38:38'),
(82, 57, 22, 'Very Good', 1, '2017-05-25 12:38:38'),
(83, 60, 22, 'Okay', 1, '2017-05-25 12:38:38'),
(84, 64, 22, 'Below Average', 1, '2017-05-25 12:38:38'),
(85, 68, 22, 'Poor', 1, '2017-05-25 12:38:38'),
(86, 53, 23, 'Excellent', 1, '2017-05-25 12:38:38'),
(87, 54, 23, 'Very Good', 1, '2017-05-25 12:38:38'),
(88, 61, 23, 'Okay', 1, '2017-05-25 12:38:38'),
(89, 65, 23, 'Below Average', 1, '2017-05-25 12:38:38'),
(90, 69, 23, 'Poor', 1, '2017-05-25 12:38:38'),
(91, 52, 24, 'Excellent', 1, '2017-05-25 12:38:38'),
(92, 58, 24, 'Very Good', 1, '2017-05-25 12:38:38'),
(93, 62, 24, 'Okay', 1, '2017-05-25 12:38:38'),
(94, 66, 24, 'Below Average', 1, '2017-05-25 12:38:38'),
(95, 70, 24, 'Poor', 1, '2017-05-25 12:38:38');

-- --------------------------------------------------------

--
-- Table structure for table `wooz_quisioner_setting`
--

CREATE TABLE `wooz_quisioner_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `places_id` int(10) NOT NULL DEFAULT '0',
  `question` text NOT NULL,
  `question_task` varchar(255) NOT NULL,
  `question_tombol` varchar(255) DEFAULT NULL,
  `question_order` varchar(255) DEFAULT NULL,
  `question_finish` varchar(255) DEFAULT NULL,
  `question_message` varchar(255) DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wooz_quisioner_setting`
--

INSERT INTO `wooz_quisioner_setting` (`id`, `places_id`, `question`, `question_task`, `question_tombol`, `question_order`, `question_finish`, `question_message`, `status`) VALUES
(1, 13, 'When Last Have You Done a Preventative Check Up?', 'Press \'BEGIN\' to Participate', 'BEGIN', 'Please Select Your Answer', 'You\'re All Done!', 'THANK YOU FOR PARTICIPATING. LOOK OUT FOR NEXT WEEK\'S QUESTION.', 0),
(2, 22, 'When Last Have You Done a Preventative Check Up?', 'Press \'BEGIN\' to Participate', 'BEGIN', 'Please Select Your Answer', 'You\'re All Done!', 'THANK YOU FOR PARTICIPATING. LOOK OUT FOR NEXT WEEK\'S QUESTION.', 0),
(3, 29, 'When Last Have You Done a Preventative Check Up?', 'Press \'BEGIN\' to Participate', 'BEGIN', 'Please Select Your Answer', 'You\'re All Done!', 'THANK YOU FOR PARTICIPATING. LOOK OUT FOR NEXT WEEK\'S QUESTION.', 0),
(4, 33, 'When Last Have You Done a Preventative Check Up?', 'Press \'BEGIN\' to Participate', 'BEGIN', 'Please Select Your Answer', 'You\'re All Done!', 'THANK YOU FOR PARTICIPATING. LOOK OUT FOR NEXT WEEK\'S QUESTION.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wooz_quisioner_soal`
--

CREATE TABLE `wooz_quisioner_soal` (
  `id` int(10) NOT NULL,
  `places_id` int(10) NOT NULL DEFAULT '0',
  `server_id` int(10) NOT NULL DEFAULT '0',
  `question` varchar(255) NOT NULL DEFAULT '0',
  `data_status` int(10) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wooz_quisioner_soal`
--

INSERT INTO `wooz_quisioner_soal` (`id`, `places_id`, `server_id`, `question`, `data_status`, `date_add`) VALUES
(1, 6, 0, '1', 1, '2016-04-25 01:05:16'),
(2, 6, 0, '3', 1, '2016-04-25 01:05:41'),
(3, 6, 0, '1', 1, '2016-04-25 01:05:54'),
(4, 6, 0, '2', 1, '2016-04-25 01:06:08'),
(5, 13, 0, 'When Last Have You Done a Preventative Check Up?', 1, '2016-04-25 08:08:06'),
(6, 22, 0, 'Do you think this is good or bad?', 1, '2016-08-16 11:16:21'),
(7, 22, 0, 'How may times per day do you do this?', 1, '2016-08-16 13:22:30'),
(8, 22, 0, 'Do Like Like Gian?', 1, '2016-08-17 15:14:43'),
(9, 29, 0, 'How did you rate the shuttle service to the event. please tell us honestly?', 1, '2017-01-31 16:05:09'),
(10, 29, 0, 'How did you rate the shuttle service to the event? (1)', 1, '2017-01-31 16:05:20'),
(11, 29, 0, 'How did you rate the shuttle service to the event? (2)', 1, '2017-01-31 16:05:29'),
(12, 29, 0, 'How did you rate the shuttle service to the event? (3)', 1, '2017-01-31 16:05:36'),
(13, 29, 0, 'How did you rate the shuttle service to the event? (4)', 1, '2017-01-31 16:05:46'),
(14, 29, 0, 'Is it done yet? Just checking', 1, '2017-02-02 23:27:29'),
(20, 33, 15, 'How did you rate the shuttle service to the event?', 1, '2017-05-25 12:38:38'),
(21, 33, 16, 'How did you rate the shuttle service to the event? (1)', 1, '2017-05-25 12:38:38'),
(22, 33, 17, 'How did you rate the shuttle service to the event? (2)', 1, '2017-05-25 12:38:38'),
(23, 33, 18, 'How did you rate the shuttle service to the event? (3)', 1, '2017-05-25 12:38:38'),
(24, 33, 19, 'How did you rate the shuttle service to the event? (4)', 1, '2017-05-25 12:38:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_group` (`account_id`),
  ADD KEY `account_email` (`email`);

--
-- Indexes for table `account_log`
--
ALTER TABLE `account_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `places_id` (`places_id`),
  ADD KEY `badge_id` (`badge_id`),
  ADD KEY `log_type` (`log_type`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `wooz_gate`
--
ALTER TABLE `wooz_gate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wooz_places`
--
ALTER TABLE `wooz_places`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wooz_quisioner`
--
ALTER TABLE `wooz_quisioner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wooz_quisioner_answer`
--
ALTER TABLE `wooz_quisioner_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wooz_quisioner_setting`
--
ALTER TABLE `wooz_quisioner_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wooz_quisioner_soal`
--
ALTER TABLE `wooz_quisioner_soal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `account_log`
--
ALTER TABLE `account_log`
  MODIFY `id` bigint(40) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wooz_gate`
--
ALTER TABLE `wooz_gate`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wooz_places`
--
ALTER TABLE `wooz_places`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `wooz_quisioner`
--
ALTER TABLE `wooz_quisioner`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `wooz_quisioner_answer`
--
ALTER TABLE `wooz_quisioner_answer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `wooz_quisioner_setting`
--
ALTER TABLE `wooz_quisioner_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wooz_quisioner_soal`
--
ALTER TABLE `wooz_quisioner_soal`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
