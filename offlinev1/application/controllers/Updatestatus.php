<?php

class Updatestatus extends CI_controller {

    protected $tpl;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Port_of_Spain');

        $this->db->select('id,places_name,places_backgroud');
        $this->db->from('wooz_places');
        $this->db->where('id', '21');
        $customs = $this->db->get()->row();

        $this->tpl['spot_id'] = 1;
        $this->tpl['custom_model'] = 1;
        $this->tpl['customs'] = 'landing';
        $this->tpl['background'] = $customs->places_backgroud;
        $this->tpl['logo'] = '';
        $this->tpl['main_title'] = 'Registration';
        $this->tpl['css'] = '';
    }

    function home($id) {
        $sqluser = "select id,nama from wooz_gate where places_id = '21' and id = '".$id."'";
        $queryuser = $this->db->query($sqluser);
        $gate = $queryuser->row();
        $this->tpl['page'] = "home";
        $this->tpl['body_class'] = '';
        $this->tpl['title'] = "| DS - tap";

        $this->tpl['title'] = "Gate";
        $this->tpl['places_text'] = $gate->nama;
        $this->tpl['places_name'] = "Gate";
        $this->tpl['nice2'] = $id;
        $this->tpl['nice1'] = $id;

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('serial', 'Serial RFID', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $rfid = $this->input->post('serial', true);
            if($rfid){
                $sqluser = "select a.id FROM account a where a.account_rfid = '" . $rfid."'";
                $queryuser = $this->db->query($sqluser);
                $datauser = $queryuser->row();
                if($datauser){
                    $data = $this->cek_user_gate($datauser->id);
                    $this->tpl['datauser'] = $data['data'];
                    $this->tpl['datastatus'] = $data['status'];
                    $this->tpl['TContent'] = $this->load->view('updatestatus/found', $this->tpl, true);
                }else {
                    $this->tpl['TContent'] = $this->load->view('updatestatus/notfound', $this->tpl, true);
                }
            }else {
                $this->tpl['TContent'] = $this->load->view('updatestatus/notfound', $this->tpl, true);
            }
            //redirect('updatestatus/home?card=' . $rfid . '&places=' . $places);
        }else{
            $this->tpl['TContent'] = $this->load->view('updatestatus/rfid', $this->tpl, true);
        }

        $this->load->view('updatestatus/home', $this->tpl);
    }

    function getname($id) {
        $sqluser = "select id,nama from wooz_gate where places_id = '21' and id = '".$id."'";
        $queryuser = $this->db->query($sqluser);
        $gate = $queryuser->row();
        $this->tpl['page'] = "home";
        $this->tpl['body_class'] = '';
        $this->tpl['title'] = "| DS - tap";

        $this->tpl['title'] = "Gate";
        $this->tpl['places_text'] = $gate->nama;
        $this->tpl['places_name'] = "Gate";
        $this->tpl['nice2'] = $id;
        $this->tpl['nice1'] = $id;


        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim');
        $this->form_validation->set_rules('id', 'Name', 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            $id = $this->input->post('id', true);
            if($id){
                $sqluser = "select a.id FROM account a where a.id = '" . $id."'";
                $queryuser = $this->db->query($sqluser);
                $datauser = $queryuser->row();
                if($datauser){
                    $data = $this->cek_user_gate($datauser->id);
                    $this->tpl['datauser'] = $data['data'];
                    $this->tpl['datastatus'] = $data['status'];
                    $this->tpl['TContent'] = $this->load->view('updatestatus/found', $this->tpl, true);
                }else {
                    $this->tpl['TContent'] = $this->load->view('updatestatus/notfound', $this->tpl, true);
                }
            }else {
                $this->tpl['TContent'] = $this->load->view('updatestatus/notfound', $this->tpl, true);
            }
        } else {
            $this->tpl['TContent'] = $this->load->view('updatestatus/rfid', $this->tpl, true);
        }
        $this->load->view('updatestatus/home', $this->tpl);
    }

    function cek_user_gate($acc_id){
        $hasil = array(
                        'status' => 0,
                        'data' => array()
                    );
        $sql = "select a.id,a.account_displayname,a.branch "
                ."from account a where a.id =  '" . $acc_id . "'";
        $query = $this->db->query($sql);
        $data = $query->row();
        if($data){
            $sql1 = "select a.id from account_log a where a.account_id =  '" . $data->id . "' and a.places_id = 28";
            $query1 = $this->db->query($sql1);
            $data1 = $query1->row();
            if($data1){
                $cek_gate = 2;
            }else{
                $cek_gate = 1;
                $db['account_id'] = $data->id;
                $db['places_id'] = 28;
                $db['log_type'] = 1;
                $db['log_date'] = date('Y-m-d');
                $db['log_time'] = date('H:i:s');
                $db['log_hash'] = 'v' .time() . '-' . $data->id;
                $db['log_stamps'] = date('Y-m-d H:i:s');
                $db['log_status'] = 1;
                $upd = $this->db->insert('account_log', $db);
                $new = $this->db->insert_id(); 
            }
            $hasil = array(
                    'status' => $cek_gate,
                    'data' => array(
                            'id' => $data->id,
                            'name' => $data->account_displayname,
                            'branch' => $data->branch
                        )
                );
        }        
        return $hasil;
    }

    function checknameawal($gate){

        // process posted form data (the requested items like province)
        $keyword = $this->input->post('term');
        $data['response'] = 'false'; //Set default response
        if(strlen($keyword) >= 3){
            $sqluser = "select a.id,a.account_displayname as hasil FROM account a where a.account_displayname like '%" . $keyword . "%'";
            $query = $this->db->query($sqluser);
            $dataquery = $query->result();

            if (!empty($dataquery)) {
                $data['response'] = 'true'; //Set response
                $data['message'] = array(); //Create array
                foreach ($dataquery as $row) {
                    $data['message'][] = array(
                        'value' => $row->hasil,
                        'id' => $row->id
                    );  //Add a row to array
                }
            }
            if ('IS_AJAX') {
                echo json_encode($data); //echo json string if ajax request
            } else {
                echo json_encode($data); //echo json string if ajax request
            }
        }else{
            echo json_encode($data);
        }
    }


    function curr_check($gate){
        $sql = "select count(id) as total from account_log "
                ."where places_id = '".$gate."'"; 
        $query = $this->db->query($sql);
        $data = $query->row(); 
        $hasil = $data->total ;
        $json = array();
        array_push($json, array('total' => $hasil));
        echo json_encode(array("json" => $json));
    }
    
    function updates() {
        $cserial = $this->input->post('card', true);
        $bserial = $this->input->post('booth', true);

        if ($cserial && $bserial) {
            $venue = $this->db->get_where('wooz_gate', array('id' => $bserial))->row();
            if ($venue) {
                $sqluser = "select * FROM account where account_rfid = '" . $cserial."'";
                $queryuser = $this->db->query($sqluser);
                $user = $queryuser->row();

                if ($user) {
                        $this->db->where('account_id', $user->id);
                        $this->db->where('log_type', 1);
                        $this->db->where('log_status', 1);
                        $this->db->where('places_id', $venue->id);
                        $this->db->order_by('log_stamps', 'desc');
                        $this->db->limit(1, 0);
                        $this->db->from('account_log');
                        $last = $this->db->get()->row();

                        if ((isset($last->places_id)) && ($last->places_id == $venue->id)) {
                            $marker = 42;
                        } else {
                            $marker = 22;
                        }
                        if (isset($last->log_stamps))
                            $margin = time() - mysql_to_unix($last->log_stamps);
                        else
                            $margin = time();

                        if ($margin > $marker) {
                            $db['account_id'] = $user->id;
                            $db['places_id'] = $venue->id;
                            $db['log_type'] = 1;
                            $db['log_date'] = date('Y-m-d');
                            $db['log_time'] = date('H:i:s');
                            $db['log_hash'] = 'v' .time() . '-' . $user->id;
                            $db['log_stamps'] = date('Y-m-d H:i:s');
                            $db['log_status'] = 1;
                            $upd = $this->db->insert('account_log', $db);
                            $new = $this->db->insert_id();

                            $data['status'] = true;
                            $data['message']['name'] = $user->account_displayname;
                            $data['message']['places'] = $venue->nama;

                            $return_json = '{"status":"' . json_encode($data['status']) . '",';
                            $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                            $return_json = $return_json . '"message2":' . json_encode($data['message']['places']) . '}';

                            echo $return_json;
                        } else {
                            $data = array(
                                'status' => 0,
                                'message' => array(
                                    'name' => $user->account_displayname,
                                    'error' => 'time limit not passed'
                                )
                            );
                            $return_json = '{"status":"' . json_encode($data['status']) . '",';
                            $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                            $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

                            echo $return_json;
                        }
                    

                } else {
                    $data = array(
                        'status' => 0,
                        'message' => array(
                            'name' => 'Please try again',
                            'error' => "card serial is not connected with any member"
                        )
                    );
                    $return_json = '{"status":"' . json_encode($data['status']) . '",';
                    $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                    $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

                    echo $return_json;
                }
            } else {
                $data = array(
                    'status' => 0,
                    'message' => array(
                        'name' => 'Please try again',
                        'error' => 'device serial is not connected with any venue'
                    )
                );
                $return_json = '{"status":"' . json_encode($data['status']) . '",';
                $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
                $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

                echo $return_json;
            }
        } else {
            $data = array(
                'status' => 0,
                'message' => array(
                    'name' => 'Please try again',
                    'error' => 'card serial and  device serial not received'
                )
            );
            $return_json = '{"status":"' . json_encode($data['status']) . '",';
            $return_json = $return_json . '"message":' . json_encode($data['message']['name']) . ',';
            $return_json = $return_json . '"message2":' . json_encode($data['message']['error']) . '}';

            echo $return_json;
        }
    }
}
