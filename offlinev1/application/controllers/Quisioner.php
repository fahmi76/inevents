<?php

class Quisioner extends CI_controller {

	protected $tpl;

	function __construct() {
		parent::__construct();
		$this->tpl['TContent'] = null;
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
		$this->lang->load('form_validation', 'en');
		$this->load->helper('date');
		$this->tpl['assets_url'] = $this->config->item('assets_url');
		date_default_timezone_set('America/Port_of_Spain');

		#$custom_page = $this->input->get('place', true);
		$custom_page = 38;
		if ($custom_page != '') {
			$customs = $this->db->get_where('wooz_places', array('id' => $custom_page))->row();
			if ($customs) {
				$a = strtotime($customs->places_duedate);
				$b = time();
				if ($customs->places_duedate != '0000-00-00' && $a > $b) {
					$this->custom_id = $customs->id;

					$sql = "SELECT * FROM `wooz_quisioner_setting` where places_id = '" . $this->custom_id . "' order by id asc";
					$query = $this->db->query($sql);
					$this->tpl['template'] = $query->row();
					$this->custom_url = $customs->places_landing;
					$this->spot_id = $this->input->get('places', true);
					$this->tpl['spot_id'] = $this->input->get('place', true);
					#$this->custom_model = $customs->places_model;
					$this->tpl['customs'] = $customs->places_landing;
					$this->tpl['background'] = $customs->places_backgroud;
					$this->tpl['logo'] = '';
					$this->tpl['main_title'] = $customs->places_name;
					$this->tpl['type_registration'] = 1;
				} else {
					redirect('?status=' . $custom_page . '_custom_page_end_from_showing');
				}
			} else {
				redirect('?status=custom_page_not_registered');
			}
		} else {
			redirect('?status=custom_page_not_found');
		}
	}

	function index() {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$this->tpl['TContent'] = $this->load->view('quisioner/default', $this->tpl, true);
		$this->load->view('quisioner/home', $this->tpl);
	}

	function start($id, $survey) {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->tpl['places_id'] = $id;
		$this->tpl['survey'] = $survey;

		$this->tpl['TContent'] = $this->load->view('quisioner/default', $this->tpl, true);
		$this->load->view('quisioner/home', $this->tpl);

	}

	function home($id, $survey) {
		header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->tpl['page'] = "home";
		$this->tpl['places_id'] = $id;
		$this->tpl['survey'] = $survey;
		$this->tpl['body_class'] = 'home';
		$this->content['title'] = "| DS - tap";

		$sql = "SELECT id,question as nama FROM `wooz_quisioner_soal` where places_id = '" . $this->custom_id . "' and survey_id = '" . $survey . "' order by id asc";
		$query = $this->db->query($sql);
		$this->tpl['data'] = $query->result();
		$this->tpl['token'] = $this->generateRandomString($survey);
		$this->tpl['TContent'] = $this->load->view('quisioner/questionwizard', $this->tpl, true);
		$this->load->view('quisioner/home', $this->tpl);
	}

	function saveaccount($places_id, $survey, $account_id) {
		$db['account_email'] = $this->input->post('email');
		$db['account_displayname'] = $this->input->post('name');
		$db['account_location'] = $this->input->post('location');
		$db['account_rfid'] = $account_id;
		$db['account_phone'] = $this->input->post('contact');
		$db['account_status'] = 1;
		$db['places_id '] = $places_id;
		$db['survey'] = $survey;
		$upd = $this->db->insert('wooz_quisioner_account', $db);
		echo json_encode('ok');
	}

	function save() {
		$db['question_id'] = $this->input->post('qid');
		$db['answer_id'] = $this->input->post('aid');
		$db['account_id'] = $this->input->post('tid');
		$db['places_id'] = $this->custom_id;
		$db['data_status'] = 1;
		$db['date_add'] = date('Y-m-d H:i:s');

		$sql = "SELECT id FROM `wooz_quisioner` where places_id = '" . $this->custom_id . "' and question_id = '" . $db['question_id'] . "' and account_id = '" . $db['account_id'] . "' order by id asc";
		$query = $this->db->query($sql)->row();
		if ($query) {
			$this->db->where('id', $query->id);
			$this->db->update('wooz_quisioner', $db);
		} else {
			$upd = $this->db->insert('wooz_quisioner', $db);
		}
		echo json_encode('ok');
	}

	function delete($id, $token) {
		$this->db->delete('wooz_quisioner', array('places_id' => $id, 'account_id' => $token));
		echo json_encode('ok');
	}

	function done($check) {
		$db['answer_id'] = $check;
		$db['places_id'] = $this->custom_id;
		$db['data_status'] = 1;
		$db['date_add'] = date('Y-m-d H:i:s');
		$upd = $this->db->insert('wooz_quisioner', $db);

		redirect('quisioner/finish');
	}

	function finish($id, $survey) {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->tpl['places_id'] = $id;
		$this->tpl['survey'] = $survey;

		$this->tpl['TContent'] = $this->load->view('quisioner/finish', $this->tpl, true);
		$this->load->view('quisioner/home', $this->tpl);
	}

	function generateRandomString($survey) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$String = '';
		for ($i = 0; $i < 20; ++$i) {
			$String .= $characters[rand(0, $charactersLength - 1)];
		}
		return strtotime("now") . '-' . $String . '-' . $survey;
	}

}
