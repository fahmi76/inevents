<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('America/Port_of_Spain');
		$this->time = date('H');
		$this->url = 'http://inevents.wooz.in/';
		// $this->url = 'http://localhost/inevents/';
		$this->places_id = 38;
		$this->date = date('Y-m-d');
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('login', 'refresh');
		}
	}

	public function index() {
		$sqluser = "select id,places_name,places_backgroud,places_logo,places_duedate from wooz_places where id = '" . $this->places_id . "'";
		$queryuser = $this->db->query($sqluser);
		$this->tpl['places'] = $queryuser->row();
		if (!$this->tpl['places']) {
			$sqluser = "select id,places_name,places_backgroud,places_logo,places_duedate from wooz_places where id = '" . $this->tpl['places'] . "'";
			$queryuser = $this->db->query($sqluser);
			$this->tpl['places'] = $queryuser->row();

		}
		$sqlgate = "select id,nama from wooz_gate where places_id = '" . $this->places_id . "' and data_status = 1";
		$querygate = $this->db->query($sqlgate);
		$this->tpl['gate'] = $querygate->result();

		$this->tpl['data'] = 'data';
		$this->tpl['content'] = $this->load->view('dashboard/index', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);
	}

	public function listquestion() {
		$sqlgate = "select id,question,places_id from wooz_quisioner_soal where places_id = '" . $this->places_id . "'";
		$queryuser = $this->db->query($sqlgate);
		$this->tpl['data'] = $queryuser->result();

		$this->tpl['content'] = $this->load->view('dashboard/question', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);
	}

	public function sinkron() {
		$url = $this->url . 'apilocalv1/list_quisioner/' . $this->places_id;
		$getplaces = $this->curlget($url);
		$getplaces = json_decode($getplaces);
		$update = 1;
		$sqlgate1 = "select status_update from wooz_quisioner_soal where places_id = '" . $this->places_id . "' order by status_update desc";
		$querygate1 = $this->db->query($sqlgate1);
		$resultcheck1 = $querygate1->row();
		if ($resultcheck1) {
			$update = $resultcheck1->status_update + 1;
		}
		if ($getplaces) {
			if ($getplaces->status == 1) {
				if ($getplaces->data) {
					foreach ($getplaces->setting as $value) {
						$in_data['title'] = $value->title;
						$in_data['survey_id'] = $value->id;
						$in_data['places_id'] = $this->places_id;
						$in_data['question_task'] = "Press 'Start' button to begin";
						$in_data['question_tombol'] = "START";
						$in_data['question_order'] = $value->order;
						$in_data['question_finish'] = $value->finish;
						$in_data['question_message'] = $value->message;
						$in_data['status_update'] = $update;

						$sqlsetting = "select id from wooz_quisioner_setting where places_id = '" . $this->places_id . "' and survey_id = '" . $value->id . "'";
						$querysetting = $this->db->query($sqlsetting);
						$resultsetting = $querysetting->row();
						if ($resultsetting) {
							$this->db->where('survey_id', $value->id);
							$this->db->update('wooz_quisioner_setting', $in_data);
							$ques_id = $resultsetting->id;
						} else {
							$this->db->insert('wooz_quisioner_setting', $in_data);
							$ques_id = $this->db->insert_id();
						}
					}
					foreach ($getplaces->data as $row) {
						$in_question['server_id'] = $row->id;
						$in_question['question'] = $row->name;
						$in_question['survey_id'] = $row->survey;
						$in_question['places_id'] = $this->places_id;
						$in_question['date_add'] = date('Y-m-d H:i:s');
						$in_question['data_status'] = 1;
						$in_question['status_update'] = $update;

						$sqlgate = "select id from wooz_quisioner_soal where places_id = '" . $this->places_id . "' and server_id = '" . $row->id . "'";
						$querygate = $this->db->query($sqlgate);
						$resultcheck = $querygate->row();
						if ($resultcheck) {
							$this->db->where('id', $resultcheck->id);
							$this->db->update('wooz_quisioner_soal', $in_question);
							$ques_id = $resultcheck->id;
						} else {
							$this->db->insert('wooz_quisioner_soal', $in_question);
							$ques_id = $this->db->insert_id();
						}
						foreach ($row->answer as $anserrow) {

							$in_answer['server_id'] = $anserrow->id;
							$in_answer['answer'] = $anserrow->name;
							$in_answer['question_id'] = $ques_id;
							$in_answer['date_add'] = date('Y-m-d H:i:s');
							$in_answer['data_status'] = $anserrow->status;
							$in_answer['status_update'] = $update;

							$sqlgateanswer = "select id from wooz_quisioner_answer where question_id = '" . $ques_id . "' and server_id = '" . $anserrow->id . "'";
							$querygateanswer = $this->db->query($sqlgateanswer);
							$resultcheckanswer = $querygateanswer->row();
							if ($resultcheckanswer) {
								$this->db->where('id', $resultcheckanswer->id);
								$this->db->update('wooz_quisioner_answer', $in_answer);
							} else {
								$this->db->insert('wooz_quisioner_answer', $in_answer);
							}
						}
					}
				}

				$sqlgate21 = "select id from wooz_quisioner_setting where places_id = '" . $this->places_id . "' and status_update < '" . $update . "'";
				$querygate21 = $this->db->query($sqlgate21);
				$resultcheck21 = $querygate21->result();
				if ($resultcheck21) {
					foreach ($resultcheck21 as $rowdelete) {
						$this->db->delete('wooz_quisioner_setting', array('id' => $rowdelete->id));
					}
				}

				$sqlgate2 = "select id from wooz_quisioner_soal where places_id = '" . $this->places_id . "' and status_update < '" . $update . "'";
				$querygate2 = $this->db->query($sqlgate2);
				$resultcheck2 = $querygate2->result();
				if ($resultcheck2) {
					foreach ($resultcheck2 as $rowdelete) {
						$this->db->delete('wooz_quisioner_soal', array('id' => $rowdelete->id));
					}
				}
				$sqlgate3 = "SELECT a.id FROM `wooz_quisioner_answer` a inner join wooz_quisioner_soal b on b.id = a.question_id where b.places_id = '" . $this->places_id . "' and a.status_update < '" . $update . "'";
				$querygate3 = $this->db->query($sqlgate3);
				$resultcheck3 = $querygate3->result();
				if ($resultcheck3) {
					foreach ($resultcheck3 as $rowdelete) {
						$this->db->delete('wooz_quisioner_answer', array('id' => $rowdelete->id));
					}
				}
			}
			$return = 'ok';
		} else {
			$return = 'error';
		}
		echo json_encode($return);
	}

	public function report() {
		$sqlsurvey = "SELECT id,survey_id,title FROM wooz_quisioner_setting where places_id = '" . $this->places_id . "' order by survey_id asc";
		$querysurvey = $this->db->query($sqlsurvey);
		$surveydata = $querysurvey->result();

		foreach ($surveydata as $row) {
			$sqlquestion = "SELECT question_id,question,count(a.id) as total FROM `wooz_quisioner` a inner join wooz_quisioner_soal b on b.id = a.question_id inner join wooz_quisioner_account c on c.account_rfid = a.account_id where a.places_id = '" . $this->places_id . "' and b.survey_id = '" . $row->survey_id . "' group by question_id order by survey_id asc";
			$queryquestion = $this->db->query($sqlquestion);
			$questiondata = $queryquestion->result();
			$dataquest = array();
			foreach ($questiondata as $value) {
				$dataquest[] = array(
					'question_id' => $value->question_id,
					'question' => $value->question,
					'total' => $value->total,
				);
			}

			$datauser[] = array(
				'id' => $row->id,
				'title' => $row->title,
				'survey_id' => $row->survey_id,
				'question' => $dataquest,
			);
		}
		// $sqlquestion = "SELECT survey_id,question_id,question,count(a.id) as total FROM `wooz_quisioner` a inner join wooz_quisioner_soal b on b.id = a.question_id inner join wooz_quisioner_account c on c.account_rfid = a.account_id where a.places_id = '" . $this->places_id . "' group by question_id order by survey_id asc";
		// $queryquestion = $this->db->query($sqlquestion);
		// $questiondata = $queryquestion->result();
		$this->tpl['question'] = $datauser;

		$sqluser = "SELECT count(DISTINCT(a.account_id)) as total FROM `wooz_quisioner` a inner join wooz_quisioner_account b on b.account_rfid = a.account_id where a.places_id = '" . $this->places_id . "'";
		$queryuser = $this->db->query($sqluser);
		$this->tpl['data'] = $queryuser->row()->total;
		$this->tpl['content'] = $this->load->view('dashboard/questionreport', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);
	}
	private function get_list_answer($id) {
		$sqlcheck = "SELECT count(a.id) as total FROM `wooz_quisioner_answer` a inner join wooz_quisioner_soal b on b.id = a.question_id where b.survey_id = '" . $id . "' and a.data_status = 1";
		$queryquestion = $this->db->query($sqlcheck);
		$question = $queryquestion->row();
		return $question->total;
	}

	function userwin($id, $places_id = 0, $account_id) {

		$in_data['account_status'] = 2;

		$this->db->where('id', $account_id);
		$this->db->update('wooz_quisioner_account', $in_data);
		redirect('question/reportgroup/' . $id . '/' . $places_id);
	}

	public function reportgroup($id, $soal) {
		$this->tpl['id'] = $id;
		$this->tpl['soal'] = $soal;
		$this->tpl['total'] = 0;
		if ($id == 2) {
			$sql = "SELECT c.*,d.question,b.answer,b.data_status,a.date_add,count(a.id) as total FROM `wooz_quisioner` a inner join wooz_quisioner_answer b on b.id = a.answer_id inner join wooz_quisioner_account c on c.account_rfid = a.account_id inner join wooz_quisioner_soal d on d.id = a.question_id  ";
			$sql .= " WHERE d.survey_id = '" . $soal . "' group by a.account_id";
			$queryquestion = $this->db->query($sql);
			$this->tpl['question'] = $queryquestion->result();
		} else {
			$sql = "SELECT c.*,d.question,b.answer,b.data_status,a.date_add,count(a.id) as total FROM `wooz_quisioner` a inner join wooz_quisioner_answer b on b.id = a.answer_id inner join wooz_quisioner_account c on c.account_rfid = a.account_id inner join wooz_quisioner_soal d on d.id = a.question_id  ";
			$sql .= " WHERE d.survey_id = '" . $soal . "' and b.data_status = 1 group by a.account_id";
			$queryquestion = $this->db->query($sql);
			$this->tpl['question'] = $queryquestion->result();
			$this->tpl['total'] = $this->get_list_answer($soal);
		}

		$this->tpl['content'] = $this->load->view('dashboard/questionuser', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);

	}

	public function reportuser($id, $soal) {
		$sql = "SELECT c.*,d.question,b.answer,b.data_status,a.date_add FROM `wooz_quisioner` a inner join wooz_quisioner_answer b on b.id = a.answer_id inner join wooz_quisioner_account c on c.account_rfid = a.account_id inner join wooz_quisioner_soal d on d.id = a.question_id  ";
		if ($id == 1) {
			$sql .= " WHERE a.question_id = '" . $soal . "'";
		} else {
			$sql .= " WHERE a.answer_id = '" . $soal . "'";
		}
		$queryquestion = $this->db->query($sql);
		$this->tpl['question'] = $queryquestion->result();

		$this->tpl['content'] = $this->load->view('dashboard/questionlist', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);

	}

	public function ajax_delete() {
		$query = $this->db->query("Delete from wooz_quisioner where places_id = '" . $this->places_id . "'");

		echo json_encode(array("status" => TRUE));
	}

	public function upload() {
		$json = 'kosong';
		if ($this->is_connected()) {
			$sql = "SELECT a.account_id,b.server_id as question,c.server_id as answer,a.date_add,a.id FROM wooz_quisioner a inner join wooz_quisioner_soal b on b.id = a.question_id inner join wooz_quisioner_answer c on c.id = a.answer_id where a.places_id = '" . $this->places_id . "' and a.data_status = 1 order by a.id asc";
			$query = $this->db->query($sql);
			$result = $query->result();
			if ($result) {
				foreach ($result as $row) {
					$cekupload = $this->curlpost($row, $this->url . 'apilocalv1/uploadquisioner/' . $this->places_id);
					if ($cekupload != 0) {
						$statusupload['data_status'] = 2;
					} else {
						$statusupload['data_status'] = 3;
					}
					$this->db->where('id', $row->id);
					$this->db->update('wooz_quisioner', $statusupload);
					$json = 'done';
				}
			}
		}
		echo json_encode(array("json" => $json));
	}

	private function curlget($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		return $response;
	}

	private function curlpost($post, $url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		$response = curl_exec($ch);
		return $response;
	}

	function is_connected() {
		$connected = @fsockopen("inevents.wooz.in", 80);
		//website, port  (try 80 or 443)
		if ($connected) {
			$is_conn = true; //action when connected
			fclose($connected);
		} else {
			$is_conn = false; //action in connection failure
		}
		return $is_conn;
	}

}
