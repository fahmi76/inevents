<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Person extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('person_model', 'person');
		$this->places_id = 37;
		// $this->url = 'http://localhost/inevents/';
		$this->url = 'http://inevents.wooz.in/';
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('login', 'refresh');
		}
	}

	public function index() {
		$this->tpl['data'] = 'data';
		$this->tpl['content'] = $this->load->view('dashboard/person_view', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);
	}

	public function ajax_list() {
		$list = $this->person->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			if ($person->account_verify == 1 || $person->account_status == 1 || $person->account_status == 3) {
				$verify = 'Activated';
			} else {
				$verify = 'Not Activated';
			}
			if ($person->account_status == 1) {
				$status = 'On the spot';
			} else {
				$status = 'Re-registration';
			}
			$row = array();
			$row[] = $person->account_displayname;
			//$row[] = $person->email;
			$row[] = $person->branch;
			$row[] = $person->department;
			$row[] = $person->team;
			$row[] = $person->mealpreference;
			// $row[] = $person->account_rfid;
			//$row[] = $verify;
			//$row[] = $status;

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->person->count_all(),
			"recordsFiltered" => $this->person->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	public function sinkron() {
		ini_set('max_execution_time', 10000);
		$sqluser = "select landing_id from account where landing_id != 0 order by id desc";
		$queryuser = $this->db->query($sqluser);
		$user = $queryuser->row();
		if ($user) {
			$url = $this->url . 'apilocalv1/user/' . $this->places_id . '/' . $user->landing_id;
		} else {
			$url = $this->url . 'apilocalv1/user/' . $this->places_id;
		}
		// xdebug($url);
		$getplaces = $this->curlget($url);
		$getplaces = json_decode($getplaces);
		// xdebug($getplaces);die;
		if ($getplaces) {
			if ($getplaces->status == 1) {
				foreach ($getplaces->data as $row) {
					$db['account_id'] = $row->id;
					$db['landing_id'] = $row->landingid;
					$db['account_displayname'] = $row->nama;
					foreach ($row->other as $other) {

						if (isset($other->department)) {
							$db['department'] = $other->department;
						}

						if (isset($other->branch)) {
							$db['branch'] = $other->branch;
						}

						if (isset($other->team)) {
							$db['team'] = $other->team;
						}
						if (isset($other->mealpreference)) {
							$db['mealpreference'] = $other->mealpreference;
						}

						if (isset($row->account_rfid)) {
							$db['account_rfid'] = $row->account_rfid;
						} else {
							$db['account_rfid'] = '';
						}
					}
					$db['account_joindate'] = $row->date;
					$db['account_status'] = 2;

					$sqluser1 = "select id from account where landing_id = '" . $row->landingid . "' order by id desc";
					$queryuser1 = $this->db->query($sqluser1);
					$user1 = $queryuser1->row();
					if ($user1) {
						$this->db->where('id', $user1->id);
						$this->db->update('account', $db);
					} else {
						$this->db->insert('account', $db);
					}
				}

			}
			$return = 'ok';
		} else {
			$return = 'error';
		}
		echo json_encode($return);
	}

	public function ajax_delete() {
		$query = $this->db->query("TRUNCATE account");
		// $query = $this->db->query("TRUNCATE account_log");

		echo json_encode(array("status" => TRUE));
	}

	public function usernew() {
		$url = $this->url . 'apilocalv1/places/' . $this->places_id;
		$getplaces = $this->curlget($url);
		$getplaces = json_decode($getplaces);
		if ($getplaces) {

			$return = 'ok';
		} else {
			$return = 'error';
		}
		echo json_encode($return);
	}

	private function curlget($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		return $response;
	}

	private function getimg($url) {
		$headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/pjpeg';
		$headers[] = 'Connection: Keep-Alive';
		$headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
		$user_agent = 'php';
		$process = curl_init($url);
		curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($process, CURLOPT_HEADER, 0);
		curl_setopt($process, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
		$return = curl_exec($process);
		curl_close($process);
		return $return;
	}
}
