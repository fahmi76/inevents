<?php
class Apilocal extends CI_controller {

    protected $tpl;

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

	function index(){
        echo 'test';
	}

    function places(){
        $placesid = 21;
        $data = array(
                'status' => 0,
                'name' => 'venue ended'
            );

        $sqluser = "select id,places_name,places_backgroud,places_logo,places_duedate from wooz_places where id = '".$placesid."'";
        $queryuser = $this->db->query($sqluser);
        $user = $queryuser->row();
        if($user){
            $data = array(
                'status' => 1,
                'id' => $user->id,
                'name' => $user->places_name,
                'background' => $user->places_backgroud,
                'logo' => $user->places_logo,
                'edate' => $user->places_duedate
            );
        }
        echo json_encode($data);
    }

    function gate(){
        $placesid = 21;
        $data = array(
                'status' => 0,
                'data' => array()
            );
        $sqluser = "select id,nama from wooz_gate where places_id = '".$placesid."'";
        $queryuser = $this->db->query($sqluser);
        $user = $queryuser->result();
        if($user){
            $data = array(
                        'status' => 1
                    );
            foreach($user as $row){
                $data['data'][] = array(
                            'id' => $row->id,
                            'nama' => $row->nama
                        );
            }
        }
        echo json_encode($data);
    }

    function user(){
        $data = array(
                'status' => 0,
                'data' => array()
            );
        $placesid = 21;
        $sqluser = "select a.id,a.account_displayname FROM wooz_account a inner join wooz_landing b on b.account_id = a.id
                    where landing_register_form = '".$placesid."' and landing_from_regis = 2";
        $queryuser = $this->db->query($sqluser);
        $user = $queryuser->result();
        if($user){
            $data = array(
                        'status' => 1
                    );
            $placesdata = $this->list_tabel($placesid);
            foreach ($user as $row) {
                $dataother = array();
                foreach ($placesdata as $value) {
                    $dataother[] = array(
                            $value->field_name => $this->ambil_data_user($row->id,$value->id)
                        );
                }
                $data['data'][] = array(
                            'id' => $row->id,
                            'nama' => $row->account_displayname,
                            'other' => $dataother
                        );
            }
        }
        echo json_encode($data);
    }

    function list_tabel($id){
        $sql = "select id,field_name from wooz_form_regis where places_id = '".$id."' order by id asc";
        $query = $this->db->query($sql);
        $hasil = $query->result();
        return $hasil;
    }

    function ambil_data_user($acc_id,$form_id){
        $sql = "select a.account_id,a.content "
                ."from wooz_form_regis_detail a where a.form_regis_id = '".$form_id."' and account_id = '" . $acc_id . "'";
        $query = $this->db->query($sql);
        $data = $query->row();
        if($data){
            $hasil = $data->content;
        }else{
            $hasil = '';
        }
        return $hasil;
    }
}
?>
