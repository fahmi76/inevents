<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('checkin_model', 'person');
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('login', 'refresh');
		}
	}

	public function index() {
		$this->tpl['data'] = 'data';
		$this->tpl['content'] = $this->load->view('dashboard/checkin_view', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);
	}

	public function ajax_list() {
		$list = $this->person->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			if ($person->badge_id != 0) {
				$row[] = $person->account_displayname . " (GUEST)";
			} else {
				$row[] = $person->account_displayname;
			}
			$row[] = $person->branch;
			$row[] = $person->department;
			$row[] = $person->team;
			$row[] = $person->mealpreference;
			if ($person->log_check_out == 2) {
				$row[] = 'Check-out';
			} else {
				if ($person->log_fb_places != 5) {
					$row[] = 'Currently Check-in';
				} else {
					$row[] = 'did not already check in a guest';
				}
			}
			$row[] = $person->log_stamps;

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->person->count_all(),
			"recordsFiltered" => $this->person->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_delete() {
		$query = $this->db->query("TRUNCATE log_user_gate");

		echo json_encode(array("status" => TRUE));
	}
}
