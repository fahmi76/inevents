<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('America/Port_of_Spain');
		$this->time = date('H');
		$this->url = 'http://inevents.wooz.in/';
		// $this->url = 'http://localhost/inevents/';
		$this->places_id = 38;
		$this->date = date('Y-m-d');
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('login', 'refresh');
		}
	}

	public function index() {
		$sqluser = "select id,places_name,places_backgroud,places_logo,places_duedate from wooz_places where id = '" . $this->places_id . "'";
		$queryuser = $this->db->query($sqluser);
		$this->tpl['places'] = $queryuser->row();
		if (!$this->tpl['places']) {
			$sqluser = "select id,places_name,places_backgroud,places_logo,places_duedate from wooz_places where id = '" . $this->tpl['places'] . "'";
			$queryuser = $this->db->query($sqluser);
			$this->tpl['places'] = $queryuser->row();

		}
		$sqlgate = "select id,nama from wooz_gate where places_id = '" . $this->places_id . "' and data_status = 1";
		$querygate = $this->db->query($sqlgate);
		$this->tpl['gate'] = $querygate->result();

		$this->tpl['data'] = 'data';
		$this->tpl['content'] = $this->load->view('dashboard/index', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);
	}

	public function places() {
		$url = $this->url . 'apilocalv1/places/' . $this->places_id;
		$getplaces = $this->curlget($url);
		$getplaces = json_decode($getplaces);

		if ($getplaces) {
			if ($getplaces->status == 1) {
				$data['places_name'] = $getplaces->name;
				$images = null;
				$profile_picture = $getplaces->background;
				if ($profile_picture != '') {
					$profile_picture = $this->getimg($profile_picture);
					file_put_contents(FCPATH . 'assets/images/pp-' . $getplaces->id . '.jpg', $profile_picture);
					$images = 'assets/images/pp-' . $getplaces->id . '.jpg';
				}

				$data['places_backgroud'] = $images;
				$data['places_logo'] = $images;
				$data['places_duedate'] = $getplaces->edate;
				$this->db->where('id', $this->places_id);
				$this->db->update('wooz_places', $data);
			}
			$return = 'ok';
		} else {
			$return = 'error';
		}
		echo json_encode($return);
	}

	public function gate() {
		$sqlgate = "select id,nama from wooz_gate where places_id = '" . $this->places_id . "'";
		$querygate = $this->db->query($sqlgate);
		$gate = $querygate->result();
		if ($gate) {
			foreach ($gate as $row) {
				$url = $this->url . 'apilocalv1/gateid/' . $this->places_id . '/' . $row->id;
				$getplaces = $this->curlget($url);
				$getplaces = json_decode($getplaces);
				if ($getplaces->status == 0) {
					$data['data_status'] = 0;
					$this->db->where('id', $row->id);
					$this->db->update('wooz_gate', $data);
				}

			}

		}
		$url = $this->url . 'apilocalv1/gate/' . $this->places_id;
		$getplaces = $this->curlget($url);
		$getplaces = json_decode($getplaces);
		if ($getplaces) {
			if ($getplaces->status == 1) {
				foreach ($getplaces->data as $row) {
					$data['id'] = $row->id;
					$data['nama'] = $row->nama;
					$data['places_id'] = $this->places_id;
					$data['data_status'] = 1;
					$data['date_add'] = date('Y-m-d H:i:s');

					$sqlcekgate = "select id,nama from wooz_gate where places_id = '" . $this->places_id . "' and id = $row->id";
					$querycekgate = $this->db->query($sqlcekgate);
					$gatecek = $querycekgate->row();
					if (!$gatecek) {
						$this->db->insert('wooz_gate', $data);
					} else {
						$this->db->where('id', $row->id);
						$this->db->update('wooz_gate', $data);
					}
				}
			}
			$return = 'ok';
		} else {
			$return = 'error';
		}
		echo json_encode($return);
	}

	private function curlget($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		return $response;
	}

	private function getimg($url) {
		$headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/pjpeg';
		$headers[] = 'Connection: Keep-Alive';
		$headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
		$user_agent = 'php';
		$process = curl_init($url);
		curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($process, CURLOPT_HEADER, 0);
		curl_setopt($process, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
		$return = curl_exec($process);
		curl_close($process);
		return $return;
	}
}
