<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regis extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('America/Port_of_Spain');

		$this->db->select('id,places_name,places_backgroud');
		$this->db->from('wooz_places');
		$this->db->where('id', '37');
		$customs = $this->db->get()->row();

		$this->tpl['spot_id'] = 1;
		$this->tpl['custom_model'] = 1;
		$this->tpl['customs'] = 'landing';
		$this->tpl['background'] = $customs->places_backgroud;
		$this->tpl['logo'] = '';
		$this->tpl['main_title'] = 'Registration';
		$this->tpl['css'] = '';
	}

	public function index() {
		header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->tpl['main_title'] = 'Registration';
		$this->tpl['core'] = 'Registration';
		$this->tpl['title'] = "Inevents";

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('department', 'Department', 'trim|required');
		$this->form_validation->set_rules('branch', 'Branch', 'trim|required');
		$this->form_validation->set_rules('team', 'Team', 'trim|required');
		$this->form_validation->set_rules('mealpreference', 'Meal Preference', 'trim|required');
		if ($this->form_validation->run() === TRUE) {
			$db['account_displayname'] = $this->input->post('name');
			$db['department'] = $this->input->post('department');
			$db['branch'] = $this->input->post('branch');
			$db['team'] = $this->input->post('team');
			$db['mealpreference'] = $this->input->post('mealpreference');
			$db['account_joindate'] = date('Y-m-d H:i:s');
			$db['account_status'] = 1;
			$this->db->insert('account', $db);
			$acc_id = $this->db->insert_id();
			// redirect('regis/step2/' . $acc_id);
			redirect('gates/regischeck/rukshun/1/37/0/' . $acc_id);
		}

		$department = $this->list_distinct_data(88);
		$this->tpl['department'] = json_encode($department);
		$branch = $this->list_distinct_data(89);
		$this->tpl['branch'] = json_encode($branch);
		$meal = $this->list_distinct_data(91);
		$this->tpl['meal'] = json_encode($meal);

		$this->tpl['TContent'] = $this->load->view('regis/form', $this->tpl, true);
		$this->load->view('regis/home', $this->tpl);
	}

	function list_distinct_data($id) {
		if ($id == 88) {
			$sql = "SELECT distinct(department) as content FROM `account` ORDER BY `department` ASC";
		} elseif ($id == 89) {
			$sql = "SELECT distinct(branch) as content FROM `account` ORDER BY `branch` ASC";
		} else {
			$sql = "SELECT distinct(mealpreference) as content FROM `account` ORDER BY `mealpreference` ASC";
		}
		$query = $this->db->query($sql);
		$hasil = $query->result();
		$arrayorganization = array();
		if (count($hasil) > 0) {
			foreach ($hasil as $value) {
				$arrayorganization[] = $value->content;
			}
		}
		return $arrayorganization;
	}

	function home() {
		$this->tpl['main_title'] = 'Re-registration';
		$this->tpl['core'] = 'Re-registration';
		$this->tpl['title'] = "Re-registration - Inevents";

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'Name', 'trim|required');
		$this->form_validation->set_rules('name', 'Name', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('telp', 'Telephone', 'trim');

		if ($this->form_validation->run() === TRUE) {
			$id = $this->input->post('id', true);
			$nama = $this->input->post('name', true);
			if (!$nama) {
				$nama = $this->input->post('email', true);
			}
			if (!$nama) {
				$nama = $this->input->post('telp', true);
			}
			redirect('regis/stepcheck/' . $id);
		}

		$this->tpl['TContent'] = $this->load->view('regis/reregis', $this->tpl, true);
		$this->load->view('regis/home', $this->tpl);
	}

	function stepcheck($account_id) {
		header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->tpl['main_title'] = 'Registration';
		$this->tpl['core'] = 'Registration';
		$this->tpl['title'] = "Inevents";

		$this->tpl['info'] = $this->db->get_where('account', array('id' => $account_id))->row();

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_used_en[' . $account_id . ']');
		$this->form_validation->set_rules('telephone', 'Telephone', 'trim|required|callback_telp_used_en[' . $account_id . ']');
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('company-name', 'Company Name', 'trim|required');
		$this->form_validation->set_rules('company-description', 'Company Description', 'trim|required');
		$this->form_validation->set_rules('position', 'Position', 'trim|required');
		$this->form_validation->set_rules('area-of-interest', 'Area of Interest', 'trim|required');
		if ($this->form_validation->run() === TRUE) {
			$db['account_displayname'] = $this->input->post('name');
			$db['email'] = $this->input->post('email');
			$db['category'] = $this->input->post('category');
			$db['address'] = $this->input->post('address');
			$db['country'] = $this->input->post('country');
			$db['company_name'] = $this->input->post('company-name');
			$db['company_description'] = $this->input->post('company-description');
			$db['position'] = $this->input->post('position');
			$db['areaofinterest'] = $this->input->post('area-of-interest');
			$db['telephone'] = $this->input->post('telephone');
			$db['account_lastupdate'] = date('Y-m-d H:i:s');
			$db['account_verify'] = 1;
			$this->db->where('id', $account_id);
			$this->db->update('account', $db);
			redirect('regis/step2/' . $account_id . '/1');
		}

		$this->tpl['TContent'] = $this->load->view('regis/found', $this->tpl, true);
		$this->load->view('regis/home', $this->tpl);
	}

	function step2($account_id, $from = 0) {
		$this->tpl['main_title'] = 'Registration';
		$this->tpl['core'] = 'Registration';
		$this->tpl['title'] = "Step 2 - Inevents";

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('serial', 'RFID Serial', 'trim|callback_rfid_used[' . $account_id . ']|required');
		if ($this->form_validation->run() === TRUE) {
			$db['account_rfid'] = $this->input->post('serial');
			$this->db->where('id', $account_id);
			$this->db->update('account', $db);
			redirect('regis/done/' . $from);
		}
		$this->tpl['info'] = $this->db->get_where('account', array('id' => $account_id))->row();

		$this->tpl['TContent'] = $this->load->view('regis/steprfid', $this->tpl, true);
		$this->load->view('regis/home', $this->tpl);
	}

	function done($from) {
		$this->tpl['page'] = "Done";
		$this->tpl['title'] = "Personal Information";

		$this->tpl['page'] = "thankyou";
		$this->tpl['title'] = "Thankyou!";
		$this->tpl['from'] = $from;
		$this->tpl['TContent'] = $this->load->view('regis/signupdone', $this->tpl, true);
		$this->load->view('regis/home', $this->tpl);
	}

	function logout($from) {
		$this->session->unset_userdata('session_id');
		$this->session->unset_userdata('ip_address');
		$this->session->unset_userdata('user_agent');
		$this->session->unset_userdata('last_activity');
		#$this->_killFacebookCookies();
		$this->session->sess_destroy();
		session_destroy();
		unset($_SESSION);
		$past = time() - 3600;
		foreach ($_COOKIE as $key => $value) {
			setcookie($key, $value, $past, '/');
		}
		if ($from == 1) {
			redirect('regis/home');
		}
		redirect('regis');
	}

	function rfid_used($str, $account_id) {
		if (!isset($str)) {
			$str = $this->input->post('rfid');
		}
		$new = $this->db->get_where('account', array('account_rfid' => $str))->row();
		if (count($new) >= 1) {
			$news = $this->db->get_where('account', array('account_rfid' => $str, 'id' => $account_id))->row();
			if ($news) {
				return true;
			} else {
				$this->form_validation->set_message('rfid_used', 'RFID already registered');
				return false;
			}
		} else {
			return true;
		}
	}

	function email_used_en($str, $account_id) {
		if (!isset($str)) {
			$str = $this->input->post('email');
		}
		$new = $this->db->get_where('account', array('email' => $str))->row();
		if ($new) {
			if ($account_id != 0) {
				$news = $this->db->get_where('account', array('email' => $str, 'id' => $account_id))->row();
				if ($news) {
					return true;
				}
			}
			$this->form_validation->set_message('email_used_en', 'Registered email, please replace Email');
			return false;
		} else {
			return true;
		}
	}

	function telp_used_en($str, $account_id) {
		if (!isset($str)) {
			$str = $this->input->post('telephone');
		}
		/*
			        $valid = preg_match('/^([0-9]{5,14})$/', $str);
			        if (!$valid) {
			            $this->form_validation->set_message('telp_used_en', 'Format Phone Number is not appropriate, please change the Phone Number');
			            return false;
		*/
		$new = $this->db->get_where('account', array('telephone' => $str))->row();
		if ($new) {
			if ($account_id != 0) {
				$news = $this->db->get_where('account', array('telephone' => $str, 'id' => $account_id))->row();
				if ($news) {
					return true;
				}

			}
			$this->form_validation->set_message('telp_used_en', 'Registered Telephone Number, Please replace the Phone Number');
			return false;
		} else {
			return true;
		}
	}

	function checkdata($places) {

		// process posted form data (the requested items like province)
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response

		switch ($places) {
		case 1:
			$sql = "SELECT a.id,a.account_displayname as hasil,a.email as data1,a.telephone as data2 FROM account as a
			WHERE a.account_displayname like '%" . $keyword . "%' and account_verify = 0 and account_status = 2";
			break;
		case 2:
			$sql = "SELECT a.id,a.email as hasil,a.account_displayname as data1,a.telephone as data2 FROM account as a
			WHERE a.email like '%" . $keyword . "%' and account_verify = 0 and account_status = 2";
			break;
		case 3:
			$sql = "SELECT a.id,a.telephone as hasil,a.account_displayname as data1,a.email as data2 FROM account as a
			WHERE a.telephone like '%" . $keyword . "%' and account_verify = 0 and account_status = 2";
			break;

		default:
			$sql = "SELECT a.id,a.account_displayname as hasil,a.email as data1,a.telephone as data2 FROM account as a
			WHERE a.account_displayname like '%" . $keyword . "%' and account_verify = 0 and account_status = 2";
			break;
		}
		$sql .= " and account_status = 2";
		$query = $this->db->query($sql);
		$dataquery = $query->result();

		if (!empty($dataquery)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($dataquery as $row) {
				$data['message'][] = array(
					'value' => $row->hasil,
					'id' => $row->id,
					'data1' => $row->data1,
					'data2' => $row->data2,
				); //Add a row to array
			}
		}
		if ('IS_AJAX') {
			echo json_encode($data); //echo json string if ajax request
		} else {
			echo json_encode($data); //echo json string if ajax request
		}
	}
}
