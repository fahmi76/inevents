<?php

class event extends CI_controller {

	protected $tpl;

	function __construct() {
		parent::__construct();
		$this->tpl['TContent'] = null;
		$this->load->database();
		$this->tpl['spot_id'] = 1;
		$this->tpl['custom_model'] = 1;
		$this->tpl['customs'] = 'landing';
		$this->tpl['background'] = '';
		$this->tpl['logo'] = '';
		$this->tpl['main_title'] = 'Event';
		$this->tpl['css'] = '';
	}

	function index() {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$date = date('Y-m-d');
		$this->db->select('id,places_name,places_duedate');
		$this->db->from('wooz_places');
		$this->db->where('id', '38');
		$customs = $this->db->get()->row();
		$this->tpl['places'] = 'Fashion Focus';
		$this->tpl['places_id'] = '38';
		$this->tpl['TContent'] = $this->load->view('events/main', $this->tpl, true);
		$this->load->view('updatestatus/home', $this->tpl);

	}

}
