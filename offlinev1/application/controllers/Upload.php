<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	protected $main = 'http://inevents.wooz.in/';
	// protected $main = 'http://localhost/inevents/';

	function __construct() {
		parent::__construct();
		date_default_timezone_set('America/Port_of_Spain');
		$this->time = date('H');
		$this->date = date('Y-m-d');
		$this->date = '2016-05-30';
		$this->time = '13';
		$this->places_id = 37;

	}

	public function index() {
		$sqluser = "select id,places_name,places_backgroud,places_logo,places_duedate from wooz_places where id = '21'";
		$queryuser = $this->db->query($sqluser);
		$this->tpl['places'] = $queryuser->row();

		$sqlgate = "select id,nama from wooz_gate where places_id = '21'";
		$querygate = $this->db->query($sqlgate);
		$this->tpl['gate'] = $querygate->result();

		$this->tpl['data'] = 'data';
		$this->tpl['content'] = $this->load->view('dashboard/index', $this->tpl, true);
		$this->load->view('layouts/main', $this->tpl);
	}

	public function user() {
		$this->tpl['data'] = 'data';
		$this->tpl['content'] = $this->load->view('upload/user', $this->tpl, true);
		$this->load->view('upload/body', $this->tpl);

	}

	public function userdata() {
		$json = array();
		$sqlgate = "SELECT id,account_displayname,branch,department,mealpreference,team,account_upload FROM account where account_id = 0";
		$querygate = $this->db->query($sqlgate);
		$data = $querygate->result();
		if ($data) {
			$x = 1;
			foreach ($data as $row) {
				$status = 'not uploaded';
				if ($row->account_upload == 1) {
					$status = 'uploaded';
				} elseif ($row->account_upload == 2) {
					$status = '<button class="btn btn-round btn-danger" onclick="userupload(' . "'" . $row->id . "'" . ')" type="button">Upload manual</button>';
				}
				array_push($json, array('no' => $x, 'name' => $row->account_displayname, 'email' => $row->branch, 'meal' => $row->mealpreference, 'team' => $row->team, 'department' => $row->department, 'status' => $status));
				$x++;
			}
		}
		echo json_encode(array("json" => $json));
	}

	public function userupload() {
		$json = 'kosong';
		if ($this->is_connected()) {
			$sql = "SELECT * FROM account where account_id = 0 and account_upload = 0 and (account_status = 1 or account_verify = 1) order by id asc";
			$query = $this->db->query($sql);
			$result = $query->row();

			if ($result) {
				$cekupload = $this->curlpost($result, $this->main . 'apilocalv1/userpost/' . $this->places_id);
				if ($cekupload != 0) {
					$statusupload['account_id'] = $cekupload;
					$statusupload['account_upload'] = 1;
				} else {
					$statusupload['account_upload'] = 2;
				}
				$this->db->where('id', $result->id);
				$this->db->update('account', $statusupload);
				$json = 'done';
			}
		}
		echo json_encode(array("json" => $json));
	}

	public function userindiv($id) {
		$json = 'kosong';
		if ($this->is_connected()) {
			$sql = "SELECT * FROM account where id = '" . $id . "'";
			$query = $this->db->query($sql);
			$result = $query->row();
			if ($result) {
				$cekupload = $this->curlpost($result, $this->main . 'apilocalv1/userpost/' . $this->places_id);
				if ($cekupload != 0) {
					$statusupload['account_id'] = $cekupload;
					$statusupload['account_upload'] = 1;
				} else {
					$statusupload['account_upload'] = 2;
				}
				$this->db->where('id', $result->id);
				$this->db->update('account', $statusupload);
				$json = 'done';
			}
		} else {
			$json = 'error';
		}
		echo json_encode(array("json" => $json));
	}

	public function checkin() {
		$this->tpl['data'] = 'data';
		$this->tpl['content'] = $this->load->view('upload/checkin', $this->tpl, true);
		$this->load->view('upload/body', $this->tpl);

	}

	public function checkindata() {
		$json = array();
		$sqlgate = "SELECT b.id,account_displayname,branch,department,mealpreference,team,b.log_stamps,b.badge_id,b.log_fb_places,b.log_tw FROM account a inner join log_user_gate b on b.account_id = a.id where a.account_id != 0";
		$querygate = $this->db->query($sqlgate);
		$data = $querygate->result();
		if ($data) {
			$x = 1;
			foreach ($data as $row) {
				$status = 'not uploaded';
				if ($row->log_tw == 1) {
					$status = 'uploaded';
				} elseif ($row->log_tw == 2) {
					$status = '<button class="btn btn-round btn-danger" onclick="userupload(' . "'" . $row->id . "'" . ')" type="button">Upload manual</button>';
				}
				$guest = '';
				if ($row->badge_id != 0) {
					$guest = 'GUEST';
				}
				$cekguest = 'Currently Check-in';
				if ($row->badge_id == 5) {
					$cekguest = 'did not already check in a guest';
				}
				array_push($json, array('no' => $x, 'name' => $row->account_displayname, 'email' => $row->branch, 'department' => $row->department, 'team' => $row->team, 'meal' => $row->mealpreference, 'gate' => $guest, 'time' => $row->log_stamps, 'guest' => $cekguest, 'status' => $status));
				$x++;
			}
		}
		echo json_encode(array("json" => $json));
	}

	public function checkinupload() {
		$json = 'kosong';
		if ($this->is_connected()) {
			$sql = "SELECT b.id,a.account_id,b.places_id,account_displayname,branch,b.log_stamps,b.badge_id,b.log_fb_places,b.log_tw FROM account a inner join log_user_gate b on b.account_id = a.id where b.log_tw = 0 and a.account_id != 0 order by b.id asc";
			$query = $this->db->query($sql);
			$result = $query->row();
			if ($result) {
				$resultupload = array(
					'account_id' => $result->account_id,
					'badge_id' => $result->badge_id,
					'log_fb_places' => $result->log_fb_places,
					'log_stamps' => $result->log_stamps,
				);
				$cekupload = $this->curlpost($resultupload, $this->main . 'apilocalv1/checkinpost/' . $this->places_id);
				if ($cekupload == 1) {
					$statusupload['log_tw'] = 1;
				} else {
					$statusupload['log_tw'] = 2;
				}
				$this->db->where('id', $result->id);
				$this->db->update('log_user_gate', $statusupload);
				$json = 'done';
			}
		} else {
			$json = 'not connected';
		}
		echo json_encode(array("json" => $json));
	}

	function checkinindividu($id) {
		$json = 'kosong';
		if ($this->is_connected()) {
			$sql = "SELECT b.id,a.account_id,b.places_id,account_displayname,branch,b.log_stamps,b.badge_id,b.log_fb_places,b.log_tw FROM account a inner join log_user_gate b on b.account_id = a.id where b.id = '" . $id . "'";
			$query = $this->db->query($sql);
			$result = $query->row();
			if ($result) {
				$resultupload = array(
					'account_id' => $result->account_id,
					'places_id' => $result->places_id,
					'badge_id' => $result->badge_id,
					'log_fb_places' => $result->log_fb_places,
					'log_stamps' => $result->log_stamps,
				);
				$cekupload = $this->curlpost($resultupload, $this->main . 'apilocalv1/checkinpost/' . $this->places_id);
				if ($cekupload == 1) {
					$statusupload['log_tw'] = 1;
				} else {
					$statusupload['log_tw'] = 2;
				}
				$this->db->where('id', $result->id);
				$this->db->update('log_user_gate', $statusupload);
				$json = 'done';
			}
		} else {
			$json = 'error';
		}
		echo json_encode(array("json" => $json));

	}

	function is_connected() {
		$connected = @fsockopen("inevents.wooz.in", 80);
		//website, port  (try 80 or 443)
		if ($connected) {
			$is_conn = true; //action when connected
			fclose($connected);
		} else {
			$is_conn = false; //action in connection failure
		}
		return $is_conn;
	}

	private function curlpost($post, $url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		$response = curl_exec($ch);
		return $response;
	}

	private function curlget($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		return $response;
	}

	function exceluser() {
		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

		$objPHPExcel->setActiveSheetIndex(0);

		$header = array('Id', 'Name', 'Branch', 'Department', 'Team', 'Meal Preference', 'joindate', 'account_verify', 'account_status', 'account_upload', 'lastupdate');

		$sql = "SELECT * FROM account order by id asc";
		$query = $this->db->query($sql);
		$result = $query->result();

		$header1 = array('example-name');

		$col = 0;
		foreach ($header as $field) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			$col++;
		}
		// Fetching the table data
		$col = 0;
		$i = 2;

		foreach ($result as $data) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $data->account_id);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $data->account_displayname);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $data->branch);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $data->department);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, $data->team);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $data->mealpreference);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, $data->account_joindate);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, $data->account_verify);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, $data->account_status);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, $data->account_upload);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, $data->account_lastupdate);
			$i++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		// Sending headers to force the user to download the file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="user-data.xls"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}

	function excelcheckin() {
		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

		$objPHPExcel->setActiveSheetIndex(0);

		$header = array('Id', 'user', 'Name', 'Branch', 'Department', 'Team', 'Meal Preference', 'places', 'Gate', 'timestamp', 'status');

		$sql = "SELECT b.id,a.account_id,a.account_displayname,a.branch,a.department,a.team,a.mealpreference,b.places_id,c.nama,b.log_stamps,b.redeem FROM account a inner join account_log b on b.account_id = a.id inner join wooz_gate c on c.id = b.places_id where c.data_status = 1 order by b.id asc";
		$query = $this->db->query($sql);
		$result = $query->result();

		$header1 = array('example-name');

		$col = 0;
		foreach ($header as $field) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			$col++;
		}
		// Fetching the table data
		$col = 0;
		$i = 2;

		foreach ($result as $data) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $data->id);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $data->account_id);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $data->account_displayname);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $data->branch);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, $data->department);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $data->team);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $i, $data->mealpreference);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $i, $data->places_id);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $i, $data->nama);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $i, $data->log_stamps);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $i, $data->redeem);
			$i++;
		}

		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		// Sending headers to force the user to download the file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="checkin-data.xls"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');

	}
}
