<?php

class Gates extends CI_controller {

	protected $tpl;

	function __construct() {
		parent::__construct();
		date_default_timezone_set('America/Port_of_Spain');

		$this->db->select('id,places_name,places_backgroud');
		$this->db->from('wooz_places');
		$this->db->where('id', '37');
		$customs = $this->db->get()->row();
		$this->custom_id = $customs->id;
		$this->tpl['places_id'] = 37;
		$this->tpl['gate'] = 37;
		$this->tpl['spot_id'] = 1;
		$this->tpl['custom_model'] = 1;
		$this->tpl['customs'] = 'landing';
		$this->tpl['background'] = $customs->places_backgroud;
		$this->tpl['logo'] = '';
		$this->tpl['main_title'] = 'Registration';
		$this->tpl['css'] = '';
		$this->tpl['css'] = '';
	}

	function index() {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$this->db->from('gate');
		$this->tpl['gate'] = $this->db->get()->result();

		$this->tpl['TContent'] = $this->load->view('gates/listplaces', $this->tpl, true);
		$this->load->view('gates/home', $this->tpl);
	}

	function home($nicename, $check, $gate) {
		$this->tpl['check'] = $check;
		$this->tpl['gate'] = $gate;
		$this->tpl['page'] = "homes";
		$this->tpl['body_class'] = '';
		$this->tpl['title'] = "Gate";
		$this->tpl['already'] = 0;
		#$this->tpl['total'] = $this->total_check($check,$gate);

		$this->db->where('places_id', $gate);
		$this->db->from('wooz_gate');
		$places_data = $this->db->get()->row();
		$this->tpl['places_name'] = $places_data->nama;

		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('serial', 'Serial RFID', 'trim');

		if ($this->form_validation->run() === TRUE) {
			$rfid = $this->input->post('serial', true);
			if ($rfid) {
				$sqluser = "select a.account_id FROM account a where account_rfid = '" . $rfid . "' ";
				$queryuser = $this->db->query($sqluser);
				$datauser = $queryuser->row();
				if ($datauser) {
					$data = $this->cek_user_gate($datauser->account_id, $gate, $check);
					$this->tpl['datauser'] = $data['data'];
					$this->tpl['datastatus'] = $data['status'];
					$this->tpl['TContent'] = $this->load->view('gates/found', $this->tpl, true);
				} else {
					$this->tpl['TContent'] = $this->load->view('gates/notfound', $this->tpl, true);
				}
			} else {
				$this->tpl['TContent'] = $this->load->view('gates/notfound', $this->tpl, true);
			}
		} else {
			if ($gate == 21) {
				$this->tpl['TContent'] = $this->load->view('gates/checkname', $this->tpl, true);
			} else {
				$this->tpl['TContent'] = $this->load->view('gates/rfid_1', $this->tpl, true);
			}
		}
		$this->load->view('gates/home', $this->tpl);
	}

	function getname($nicename, $check, $gate) {
		$this->tpl['check'] = $check;
		$this->tpl['gate'] = $gate;
		$this->tpl['page'] = "homes";
		$this->tpl['body_class'] = '';
		$this->tpl['title'] = "Gate";
		$this->tpl['already'] = 0;
		#$this->tpl['total'] = $this->total_check($check,$gate);

		$this->db->where('places_id', $gate);
		$this->db->from('wooz_gate');
		$places_data = $this->db->get()->row();
		$this->tpl['places_name'] = $places_data->nama;

		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'trim');

		if ($this->form_validation->run() === TRUE) {
			$id = $this->input->post('id', true);
			if ($id) {
				$sqluser = "select a.id,a.account_id FROM account a where a.id = '" . $id . "'";
				$queryuser = $this->db->query($sqluser);
				$datauser = $queryuser->row();
				if ($datauser) {
					$data = $this->cek_user_gate($datauser->id, $gate, $check);
					$this->tpl['datauser'] = $data['data'];
					$this->tpl['datastatus'] = $data['status'];
					$this->tpl['TContent'] = $this->load->view('gates/found', $this->tpl, true);
				} else {
					$this->tpl['TContent'] = $this->load->view('gates/notfound', $this->tpl, true);
				}
			} else {
				$this->tpl['TContent'] = $this->load->view('gates/notfound', $this->tpl, true);
			}
		} else {
			$this->tpl['TContent'] = $this->load->view('gates/rfid_1', $this->tpl, true);
		}
		$this->load->view('gates/home', $this->tpl);
	}

	function regischeck($nicename, $check, $gate, $guest = 0, $id) {
		$this->tpl['check'] = $check;
		$this->tpl['gate'] = $gate;
		$this->tpl['page'] = "homes";
		$this->tpl['body_class'] = '';
		$this->tpl['title'] = "Gate";
		$this->tpl['already'] = 0;
		$data = $this->cek_user_gate($id, $gate, $check, $guest);
		// xdebug($data);die;
		$this->tpl['datauser'] = $data['data'];
		$this->tpl['datastatus'] = $data['status'];
		$this->tpl['TContent'] = $this->load->view('gates/found', $this->tpl, true);
		$this->load->view('gates/home', $this->tpl);
	}

	function total_check($check, $gate) {
		$sql = "select count(account_id) as total from wooz_log_user_gate "
			. "where log_gate = '" . $gate . "' and log_check = '" . $check . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		return $data->total;
	}

	function curr_check($nicename, $gate) {
		$sql = "select count(id) as total from log_user_gate "
			. "where log_gate = '" . $gate . "' and log_check in (1) and log_fb_places != '5'";
		/*
			        $sql = "select count(id) as total from wooz_log_user_gate "
			                ."where log_gate = '".$gate."' and log_check in (1) and log_fb_places != '5' and log_check_out = 0";

			        $sql = "select count(distinct(account_id)) as total from wooz_log_user_gate "
		*/
		$query = $this->db->query($sql);
		$data = $query->row();
		$hasil = $data->total;
		/*
			        $sql1 = "select count(distinct(account_id)) as total from wooz_log_user_gate "
			                ."where places_id = '11' and log_check in (5) and log_check_out = 0";
			        $query1 = $this->db->query($sql1);
			        $data1 = $query1->row();
			        $hasil1 = $data1->total ;
		*/
		$json = array();
		array_push($json, array('total' => $hasil));
		echo json_encode(array("json" => $json));
	}

	function listuser() {
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->content['title'] = "| DS - tap";

		$sql = "select distinct(a.account_id),b.account_displayname,b.account_avatar,a.log_time "
			. "from wooz_account_pre b inner join wooz_log_user a on a.account_id = b.id "
			. "group by a.account_id order by a.id desc";
		$query = $this->db->query($sql);
		$this->tpl['data'] = $query->result();
		$this->tpl['TContent'] = $this->load->view('gates/listuser', $this->tpl, true);
		$this->load->view('gates/home', $this->tpl);
	}

	function cek_gate_permission($gate, $check, $acc_id, $form_id) {
		$sql = "select a.account_id,a.content "
			. "from wooz_form_regis_detail a where a.form_regis_id = '" . $form_id . "'  and account_id = '" . $acc_id . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$sqlgate = "select gate_id from wooz_visitor where nama = '" . $data->content . "' and places_id = '" . $this->custom_id . "' ";
			$querygate = $this->db->query($sqlgate);
			$datagate = $querygate->row();
			if ($datagate && in_array($gate, json_decode($datagate->gate_id))) {
				return 1;
			} else {
				return 2;
			}
		} else {
			return 0;
		}
	}
	function ambil_data_user($acc_id, $form_id) {
		$sql = "select a.account_id,a.content "
			. "from wooz_form_regis_detail a where a.form_regis_id = '" . $form_id . "' and account_id = '" . $acc_id . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$hasil = $data->content;
		} else {
			$hasil = '';
		}
		return $hasil;
	}

	function cek_user_gate($acc_id, $gate, $check) {
		$hasil = array(
			'status' => 0,
			'data' => array(),
		);
		$sql = "select a.account_id,a.account_displayname,a.branch,a.department,a.team,a.mealpreference "
			. "from account a where a.id =  '" . $acc_id . "'";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			if ($this->custom_id == 11) {
				$cek_gate = $this->cek_gate_permission($gate, $check, $acc_id, 17);
				if ($check == 2) {
					$cek_gate = 1;
				}
				if ($cek_gate == 1) {
					if ($check == 2) {
						$cek_gate_new = $this->checkingateout($check, $gate, $acc_id);
					} else {
						$cek_gate_new = $this->checkingate($check, $gate, $acc_id);
					}
					if (!$cek_gate_new) {
						$cek_gate = 3;
					} else {
						$cek_gate = $cek_gate_new;
					}
				} else {
					$cek_gate = 4;
				}

				$hasil = array(
					'status' => $cek_gate,
					'data' => array(
						'id' => $data->id,
						'name' => $data->account_displayname,
						'table' => $this->ambil_data_user($acc_id, 13),
						#'photo' => $this->ambil_data_user($acc_id,5)
					),
				);
			} elseif ($this->custom_id == 24) {
				$cek_gate = 1;
				if ($cek_gate == 1) {
					if ($check == 2) {
						$cek_gate_new = $this->checkingateout($check, $gate, $acc_id);
					} else {
						$cek_gate_new = $this->checkingate($check, $gate, $acc_id);
					}
					if (!$cek_gate_new) {
						$cek_gate = 3;
					} else {
						$cek_gate = $cek_gate_new;
					}
				} else {
					$cek_gate = 4;
				}

				$hasil = array(
					'status' => $cek_gate,
					'data' => array(
						'id' => $data->account_id,
						'name' => $data->account_displayname,
						'Department' => $data->branch,
						'Photo' => $data->photo,
						'cek_guest' => $data->rsvp,
						'cek_guest_' => $data->guest,
						'guest' => $this->checkguestreturn($acc_id, $gate),
						'show_guest' => $this->showguestout($acc_id, $gate),
					),
				);

			} elseif ($this->custom_id == 26) {
				$cek_gate_new = $this->checkingate($check, $gate, $acc_id);
				if (!$cek_gate_new) {
					$cek_gate = 3;
				} else {
					$cek_gate = 1;
					if ($cek_gate_new != 1) {
						$cek_gate = 2;
					}
				}
				$hasil = array(
					'status' => $cek_gate,
					'data' => array(
						'id' => $data->account_id,
						'name' => $data->account_displayname,
						'class' => $data->branch,
						'department' => $data->guest,
						'rsvp' => $data->rsvp,
					),
				);

			} elseif ($this->custom_id == 37) {
				$cek_gate_new = $this->checkingate($check, $gate, $acc_id);
				if (!$cek_gate_new) {
					$cek_gate = 3;
				} else {
					$cek_gate = 1;
					if ($cek_gate_new != 1) {
						$cek_gate = 2;
					}
				}
				$hasil = array(
					'status' => $cek_gate,
					'data' => array(
						'id' => $data->account_id,
						'name' => $data->account_displayname,
						'class' => $data->branch,
						'team' => $data->team,
						'meal' => $data->mealpreference,
					),
				);

			} else {
				$cek_gate_new = $this->checkingate($check, $gate, $acc_id);
				if (!$cek_gate_new) {
					$cek_gate = 3;
				} else {
					$cek_gate = 1;
					if ($cek_gate_new != 1) {
						$cek_gate = 2;
					}
				}
				$hasil = array(
					'status' => $cek_gate,
					'data' => array(
						'id' => $data->id,
						'name' => $data->account_displayname,
						'branch' => $this->ambil_data_user($acc_id, 40),
					),
				);
			}
		}
		return $hasil;
	}

	function showguestout($acc_id, $gate) {
		$hasil = 0;
		$sql = "select log_fb_places from log_user_gate "
			. "where log_gate = '" . $gate . "' and badge_id = '" . $acc_id . "' order by id desc";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$hasil = $data->log_fb_places;
		}
		return $hasil;
	}

	function checkguestreturn($acc_id, $gate) {
		$hasil = 0;
		$sql = "select log_fb_places from log_user_gate "
			. "where log_gate = '" . $gate . "' and badge_id = '" . $acc_id . "' order by id desc";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$hasil = 1;
		}
		return $hasil;
	}

	function checkingate($check, $gate, $id) {
		$date = date('Y-m-d');
		$time = date('h:i:s');
		$sql = "select id from log_user_gate "
			. "where log_gate = '" . $gate . "' and log_check = 1 and log_check_out = 0 and account_id = '" . $id . "' order by id desc";
		$query = $this->db->query($sql);
		$data = $query->row();
		if (!$data) {
			$sql1 = "select id,log_status_check from log_user_gate "
				. "where log_gate = '" . $gate . "' and log_check = 1 and log_check_out = 2 and log_status_check = 2 and account_id = '" . $id . "' order by id desc";
			$query1 = $this->db->query($sql1);
			$data1 = $query1->row();
			if ($data1) {
				$bisa = 1;
				if ($data1->log_status_check == 2) {
					$bisa = 2;
				}
			} else {
				$bisa = 1;
			}
			if ($bisa == 1) {
				//$db['log_email'] = $this->mandrill($gate,$id);
				$db['account_id'] = $id;
				$db['places_id'] = $this->custom_id;
				$db['log_type'] = 1;
				$db['log_date'] = $date;
				$db['log_time'] = $time;
				$db['log_gate'] = $gate;
				$db['log_check'] = $check;
				$db['log_hash'] = 'v' . time() . '-' . $id;
				$db['log_status'] = 1;
				$db['log_stamps'] = date('Y-m-d H:i:s');
				$upd = $this->db->insert('log_user_gate', $db);
				return 1;
			} else {
				return 3;
			}
		} else {
			$sql1 = "select id from log_user_gate "
				. "where log_gate = '" . $gate . "' and log_check = 5 and log_check_out = 0 and badge_id = '" . $id . "' order by id desc";
			$query1 = $this->db->query($sql1);
			$data1 = $query1->result();
			if ($data1) {
				/*
					                foreach ($data1 as $row) {
					                    $db_update['log_check'] = 1;
					                    $this->db->where('id', $row->id);
					                    $this->db->update('log_user_gate', $db_update);
				*/
				return 5;
			} else {
				return 2;
			}
		}
	}

	function checkingateout($check, $gate, $id) {
		$date = date('Y-m-d');
		$time = date('h:i:s');
		/*
			        $sql = "select id,log_check_out from wooz_log_user_gate "
		*/
		$sql = "select id,log_check_out from log_user_gate "
			. "where places_id = '24' and log_check = 1 and account_id = '" . $id . "' order by id desc";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			if ($data->log_check_out == 0) {
				$db['log_date'] = $date;
				$db['log_time'] = $time;
				$db['log_check_out'] = $check;
				$this->db->where('id', $data->id);
				$this->db->update('log_user_gate', $db);
				/*
					                $sql1 = "select id from wooz_log_user_gate "
				*/
				$sql1 = "select id from log_user_gate "
					. "where places_id = '11' and log_check = 1 and badge_id = '" . $id . "' order by id desc";
				$query1 = $this->db->query($sql1);
				$data1 = $query1->result();
				if ($data1) {
					foreach ($data1 as $row) {
						$db1['log_date'] = $date;
						$db1['log_time'] = $time;
						$db1['log_check_out'] = $check;
						$this->db->where('id', $row->id);
						$this->db->update('log_user_gate', $db1);
					}
				}
				return 1;
			} else {
				return 2;
			}
		} else {
			return 3;
		}
	}

	function guestnows($nicename, $check, $gate, $account_id) {
		$this->tpl['check'] = $check;
		$this->tpl['gate'] = $gate;
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->tpl['title'] = "Gate";
		$this->tpl['account_id'] = $account_id;
		#$this->tpl['total'] = $this->total_check($check,$gate);

		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('guest', 'choose guest', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
			$check_guest = $this->input->post('guest', true);
			if ($check_guest != 2) {

			}
			redirect('gates/home/' . $nicename . '/' . $check . '/' . $gate);
		} else {
			$this->tpl['TContent'] = $this->load->view('gates/guestnow', $this->tpl, true);
		}
		$this->load->view('gates/home', $this->tpl);

	}

	function guest($nicename, $check, $gate, $account_id) {
		$this->tpl['check'] = $check;
		$this->tpl['gate'] = $gate;
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->tpl['title'] = "Gate";
		$this->tpl['account_id'] = $account_id;
		#$this->tpl['total'] = $this->total_check($check,$gate);

		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('guest', 'choose guest', 'trim|required');
		$this->form_validation->set_rules('quantity', 'choose adult guest', 'trim');
		$this->form_validation->set_rules('quantitychild', 'choose child guest', 'trim');

		if ($this->form_validation->run() === TRUE) {
			$check_guest = $this->input->post('guest', true);
			$name = $this->input->post('quantity', true);
			$namechild = $this->input->post('quantitychild', true);
			if ($check_guest != 2) {
				if ($check_guest == 3) {
					$checkd = 5;
					$status = 2;
					$date = date('Y-m-d');
					$time = date('h:i:s');
					$db['account_id'] = $account_id . '-00';
					$db['badge_id'] = $account_id;
					$db['places_id'] = $this->custom_id;
					$db['log_fb_places'] = $checkd;
					$db['log_type'] = 1;
					$db['log_date'] = $date;
					$db['log_time'] = $time;
					$db['log_gate'] = $gate;
					$db['log_check'] = $checkd;
					$db['log_hash'] = 'v' . time() . '-' . $account_id;
					$db['log_status'] = $status;
					$db['log_stamps'] = date('Y-m-d H:i:s');
					$upd = $this->db->insert('log_user_gate', $db);
				} else {
					$checkd = 1;
					$status = 1;

					for ($a = 1; $a <= $name; $a++) {
						$date = date('Y-m-d');
						$time = date('h:i:s');
						$db['account_id'] = $account_id . '-10' . $a;
						$db['badge_id'] = $account_id;
						$db['places_id'] = $this->custom_id;
						$db['log_fb_places'] = 1;
						$db['log_type'] = 1;
						$db['log_date'] = $date;
						$db['log_time'] = $time;
						$db['log_gate'] = $gate;
						$db['log_check'] = $checkd;
						$db['log_hash'] = 'v' . time() . '-' . $account_id;
						$db['log_status'] = $status;
						$db['log_stamps'] = date('Y-m-d H:i:s');
						$upd = $this->db->insert('log_user_gate', $db);
					}
					for ($a = 1; $a <= $namechild; $a++) {
						$date = date('Y-m-d');
						$time = date('h:i:s');
						$db['account_id'] = $account_id . '-20' . $a;
						$db['badge_id'] = $account_id;
						$db['places_id'] = $this->custom_id;
						$db['log_fb_places'] = 2;
						$db['log_type'] = 1;
						$db['log_date'] = $date;
						$db['log_time'] = $time;
						$db['log_gate'] = $gate;
						$db['log_check'] = $checkd;
						$db['log_hash'] = 'v' . time() . '-' . $account_id;
						$db['log_status'] = $status;
						$db['log_stamps'] = date('Y-m-d H:i:s');
						$upd = $this->db->insert('log_user_gate', $db);
					}
				}
			}
			redirect('gates/home/' . $nicename . '/' . $check . '/' . $gate);
		} else {
			$this->tpl['TContent'] = $this->load->view('gates/guest', $this->tpl, true);
		}
		$this->load->view('gates/home', $this->tpl);
	}

	function checkout($nicename, $check, $gate, $account_id) {
		$this->tpl['check'] = $check;
		$this->tpl['gate'] = $gate;
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->tpl['title'] = "Gate";
		$this->tpl['account_id'] = $account_id;
		#$this->tpl['total'] = $this->total_check($check,$gate);

		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('guest', 'choose guest', 'trim|required');
		$this->form_validation->set_rules('guest', 'choose guest', 'trim|callback_guest_check');

		if ($this->form_validation->run() === TRUE) {
			$check_guest = $this->input->post('guest', true);
			$name = $this->input->post('name', true);
			if ($check_guest != 2) {
				if ($check_guest == 3) {
					$checkd = 5;
				} else {
					$checkd = 1;
				}
				$date = date('Y-m-d');
				$time = date('h:i:s');
				$db['account_id'] = $account_id . '-10';
				$db['badge_id'] = $account_id;
				$db['places_id'] = $this->custom_id;
				$db['log_fb_places'] = $name;
				$db['log_type'] = 1;
				$db['log_date'] = $date;
				$db['log_time'] = $time;
				$db['log_gate'] = $gate;
				$db['log_check'] = $checkd;
				$db['log_hash'] = 'v' . time() . '-' . $account_id;
				$db['log_status'] = 2;
				$db['log_stamps'] = date('Y-m-d H:i:s');
				$upd = $this->db->insert('log_user_gate', $db);
			}
			redirect('gates/home/' . $nicename . '/' . $check . '/' . $gate);
		} else {
			$this->tpl['TContent'] = $this->load->view('gates/checkout', $this->tpl, true);
		}
		$this->load->view('gates/home', $this->tpl);

	}

	function checkoutchoose($nicename, $check, $gate, $account_id, $guest) {
		$sql = "select id from log_user_gate "
			. "where log_gate = '" . $gate . "' and log_check = 1 and log_check_out = 2 and account_id = '" . $account_id . "' order by id desc";
		$query = $this->db->query($sql);
		$data = $query->row();
		if ($data) {
			$db['log_status_check'] = $guest;
			$this->db->where('id', $data->id);
			$this->db->update('log_user_gate', $db);
		}
		redirect('gates/home/' . $nicename . '/' . $check . '/' . $gate);
	}

	function gateschoose($nicename, $check, $gate, $account_id, $guest) {
		if ($guest != 2) {
			if ($guest == 3) {
				$checkd = 5;
				$status = 2;
			} else {
				$checkd = 1;
				$status = 1;
			}
			if ($check == 1) {
				$date = date('Y-m-d');
				$time = date('h:i:s');
				$db['account_id'] = $account_id . '-50';
				$db['badge_id'] = $account_id;
				$db['places_id'] = $this->custom_id;
				$db['log_fb_places'] = $guest;
				$db['log_type'] = 1;
				$db['log_date'] = $date;
				$db['log_time'] = $time;
				$db['log_gate'] = $gate;
				$db['log_check'] = $checkd;
				$db['log_hash'] = 'v' . time() . '-' . $account_id;
				$db['log_status'] = $status;
				$db['log_stamps'] = date('Y-m-d H:i:s');
				//xdebug($db);die;
				$upd = $this->db->insert('log_user_gate', $db);

			} else {
				$date = date('Y-m-d');
				$time = date('h:i:s');
				$sql = "select id,log_check_out from log_user_gate "
					. "where places_id = '24' and log_check = 1 and badge_id = '" . $account_id . "' order by id desc";
				$query = $this->db->query($sql);
				$data = $query->row();
				if ($data) {
					if ($data->log_check_out == 0) {
						$db['log_date'] = $date;
						$db['log_time'] = $time;
						$db['log_check_out'] = $check;
						$this->db->where('id', $data->id);
						$this->db->update('log_user_gate', $db);
					}
				}
			}
		}
		redirect('gates/home/' . $nicename . '/' . $check . '/' . $gate);
	}

	function guest_check($str, $account_id) {
		if (!isset($str)) {
			$str = $this->input->post('quantity');
		}
		$check_guest = $this->input->post('guest', true);
		if ($check_guest == 2) {
			return true;
		} else {
			if ($str) {
				$guest_adult = $this->ambil_data_user($account_id, 27);
				$guest_now = $this->checktotalguest($account_id, 1);
				if ($guest_now != 0) {
					$guest_adult = $guest_adult - $guest_now;
				}
				if ($str > $guest_adult) {
					$this->form_validation->set_message('guest_check', 'adult guest too much');
					return false;
				}
				return true;
			} else {
				$this->form_validation->set_message('guest_check', 'fill guest');
				return false;
			}
		}
	}

	function guest_check_child($str, $account_id) {
		if (!isset($str)) {
			$str = $this->input->post('quantity');
		}
		$check_guest = $this->input->post('guest', true);
		if ($check_guest == 2) {
			return true;
		} else {
			if ($str) {
				$guest_adult = $this->ambil_data_user($account_id, 28);
				$guest_now = $this->checktotalguest($account_id, 2);
				if ($guest_now != 0) {
					$guest_adult = $guest_adult - $guest_now;
				}
				if ($str > $guest_adult) {
					$this->form_validation->set_message('guest_check_child', 'child guest too much');
					return false;
				}
				return true;
			} else {
				$this->form_validation->set_message('guest_check_child', 'fill guest');
				return false;
			}
		}
	}

	function checktotalguest($account_id, $guest) {
		$sql = "SELECT count(id) as total FROM `log_user_gate` WHERE badge_id = '" . $account_id . "' and places_id = '" . $this->custom_id . "' and log_fb_places = '" . $guest . "'";
		$query = $this->db->query($sql);
		$dataquery = $query->row();
		return $dataquery->total;
	}

	function checklater($nicename, $check, $gate) {
		$this->tpl['check'] = $check;
		$this->tpl['gate'] = $gate;
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->tpl['title'] = "Gate";
		$this->tpl['already'] = 0;
		#$this->tpl['total'] = $this->total_check($check,$gate);

		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('id', 'name', 'trim');

		if ($this->form_validation->run() === TRUE) {
			$id = $this->input->post('id', true);
			$name = $this->input->post('name', true);
			$check = $this->input->post('check', true);
			if ($id) {
				//found

				$sql1 = "select id from log_user_gate "
				. "where places_id = '" . $this->custom_id . "' and log_gate = '" . $gate . "' and log_check = 5 and log_check_out = 0 and badge_id = '" . $id . "' order by id desc";
				$query1 = $this->db->query($sql1);
				$data1 = $query1->result();
				if ($data1) {
					//redirect('gates/foundchecklater/'.$nicename.'/'.$check.'/'.$gate.'/'.$id.'/'.$data1->id);

					foreach ($data1 as $row) {
						$date = date('Y-m-d');
						$time = date('h:i:s');
						$db['log_date'] = $date;
						$db['log_time'] = $time;
						$db['log_stamps'] = date('Y-m-d H:i:s');
						$db['log_fb_places'] = 1;
						$db['log_check'] = 1;
						$db['log_status'] = 1;
						$db['log_check'] = 1;
						$this->db->where('id', $row->id);
						$this->db->update('log_user_gate', $db);
					}
				}
				$this->tpl['check'] = $check;
				$this->tpl['name'] = $name;
				$this->tpl['TContent'] = $this->load->view('gates/laterfoundguest', $this->tpl, true);
			} else {
				$this->tpl['TContent'] = $this->load->view('gates/notfoundguest', $this->tpl, true);
			}

		} else {
			$this->tpl['TContent'] = $this->load->view('gates/checklater', $this->tpl, true);
		}
		$this->load->view('gates/home', $this->tpl);
	}

	function foundchecklater($nicename, $check, $gate, $account_id, $logid) {
		$this->tpl['check'] = $check;
		$this->tpl['gate'] = $gate;
		$this->tpl['page'] = "home";
		$this->tpl['body_class'] = '';
		$this->tpl['title'] = "Gate";
		$this->tpl['account_id'] = $account_id;
		#$this->tpl['total'] = $this->total_check($check,$gate);

		$this->load->helper(array('form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('quantity', 'choose adult guest', 'trim');
		$this->form_validation->set_rules('quantitychild', 'choose child guest', 'trim');

		if ($this->form_validation->run() === TRUE) {
			$name = $this->input->post('quantity', true);
			$namechild = $this->input->post('quantitychild', true);

			$checkd = 1;
			$status = 1;

			$date = date('Y-m-d');
			$time = date('h:i:s');
			$db1['log_date'] = $date;
			$db1['log_time'] = $time;
			$db1['log_stamps'] = date('Y-m-d H:i:s');
			$db1['log_check'] = 1;
			$db1['log_status'] = 1;
			$db1['log_check'] = 1;
			$this->db->where('id', $logid);
			$this->db->update('log_user_gate', $db1);

			for ($a = 1; $a <= $name; $a++) {
				$date = date('Y-m-d');
				$time = date('h:i:s');
				$db['account_id'] = $account_id . '-10' . $a;
				$db['badge_id'] = $account_id;
				$db['places_id'] = $this->custom_id;
				$db['log_fb_places'] = 1;
				$db['log_type'] = 1;
				$db['log_date'] = $date;
				$db['log_time'] = $time;
				$db['log_gate'] = $gate;
				$db['log_check'] = $checkd;
				$db['log_hash'] = 'v' . time() . '-' . $account_id;
				$db['log_status'] = $status;
				$db['log_stamps'] = date('Y-m-d H:i:s');
				$upd = $this->db->insert('log_user_gate', $db);
			}
			for ($a = 1; $a <= $namechild; $a++) {
				$date = date('Y-m-d');
				$time = date('h:i:s');
				$db['account_id'] = $account_id . '-20' . $a;
				$db['badge_id'] = $account_id;
				$db['places_id'] = $this->custom_id;
				$db['log_fb_places'] = 2;
				$db['log_type'] = 1;
				$db['log_date'] = $date;
				$db['log_time'] = $time;
				$db['log_gate'] = $gate;
				$db['log_check'] = $checkd;
				$db['log_hash'] = 'v' . time() . '-' . $account_id;
				$db['log_status'] = $status;
				$db['log_stamps'] = date('Y-m-d H:i:s');
				$upd = $this->db->insert('log_user_gate', $db);
			}
			$check = 1;

			redirect('gates/home/' . $nicename . '/' . $check . '/' . $gate);
		} else {
			$this->tpl['TContent'] = $this->load->view('gates/foundguestlater', $this->tpl, true);
		}
		$this->load->view('gates/home', $this->tpl);
	}

	function checkname($nicename, $check, $gate) {

		// process posted form data (the requested items like province)
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response
		$sql = "SELECT distinct(badge_id),b.account_displayname as hasil,a.id,log_check
            FROM log_user_gate as a inner join account b on b.account_id = a.badge_id
            WHERE b.account_displayname like '%" . $keyword . "%' and badge_id != 0
            and places_id = '" . $this->custom_id . "' and log_gate = '" . $gate . "' and log_check = 5 group by badge_id";

		$query = $this->db->query($sql);
		$dataquery = $query->result();

		if (!empty($dataquery)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($dataquery as $row) {
				$data['message'][] = array(
					'value' => $row->hasil,
					'id' => $row->badge_id,
					'check' => $row->log_check,
				); //Add a row to array
			}
		}
		if ('IS_AJAX') {
			echo json_encode($data); //echo json string if ajax request
		} else {
			echo json_encode($data); //echo json string if ajax request
		}
	}

	function mandrill($gate, $userid) {
		$hasil_email = 0;
		$this->load->config('mandrill');
		$this->load->library('mandrill');
		$mandrill_ready = NULL;

		try {
			$this->mandrill->init($this->config->item('mandrill_api_key'));
			$mandrill_ready = TRUE;
		} catch (Mandrill_Exception $e) {
			$mandrill_ready = FALSE;
		}

		if ($mandrill_ready) {
			$copytext = $this->db->get_where('gate_email', array('gate_id' => $gate))->row();
			$email_user = $this->ambil_data_user($userid, 21);
			if ($copytext->attachment) {
				$image = FCPATH . $copytext->attachment;
				$attch = $this->mandrill->getAttachmentStruct($image);
			} else {
				$attch = '';
			}
			//Send us some email!
			$email = array(
				'html' => $copytext->content, //Consider using a view file
				'subject' => $copytext->subject,
				'from_email' => 'donotreply@wooz.in',
				'from_name' => $copytext->nama,
				'to' => array(array('email' => $email_user)), //Check documentation for more details on this one
				"attachments" => array($attch),
			);
			$result = $this->mandrill->messages_send($email);
			$hasil_email = $result[0]['_id'];
		}
		return $hasil_email;
	}

	function cek_content_data($acc_id, $places_id, $content_id) {
		$sql = "select content from wooz_form_regis_detail where account_id = '" . $acc_id . "' and places_id = '" . $places_id . "' and form_regis_id = '" . $content_id . "' ";
		$query = $this->db->query($sql);
		$hasil = $query->row();
		return $hasil->content;
	}

	function checknameawalnew($nicename, $check, $gate) {

		// process posted form data (the requested items like province)
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response
		if (strlen($keyword) >= 3) {
			$sqluser = "select a.id,a.account_displayname as hasil FROM wooz_account a inner join wooz_landing b on b.account_id = a.id
                        where a.account_displayname like '%" . $keyword . "%' and landing_register_form = '" . $this->custom_id . "'";
			$query = $this->db->query($sqluser);
			$dataquery = $query->result();

			if (!empty($dataquery)) {
				$data['response'] = 'true'; //Set response
				$data['message'] = array(); //Create array
				foreach ($dataquery as $row) {
					$data['message'][] = array(
						'value' => $row->hasil,
						'id' => $row->id,
						'department' => $this->ambil_data_user($row->id, 26),
						'employee' => $this->ambil_data_user($row->id, 25),
						'company' => $this->ambil_data_user($row->id, 24),
					); //Add a row to array
				}
			}
			if ('IS_AJAX') {
				echo json_encode($data); //echo json string if ajax request
			} else {
				echo json_encode($data); //echo json string if ajax request
			}
		} else {
			echo json_encode($data);
		}
	}

	function checknameawal($nicename, $check, $gate) {

		// process posted form data (the requested items like province)
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response
		$sqluser = "select a.id,a.account_displayname as hasil FROM account a where a.account_displayname like '%" . $keyword . "%'";
		$query = $this->db->query($sqluser);
		$dataquery = $query->result();

		if (!empty($dataquery)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($dataquery as $row) {
				$data['message'][] = array(
					'value' => $row->hasil,
					'id' => $row->id,
				); //Add a row to array
			}
		}
		if ('IS_AJAX') {
			echo json_encode($data); //echo json string if ajax request
		} else {
			echo json_encode($data); //echo json string if ajax request
		}
	}
}
