<script src="<?php echo base_url(); ?>assets/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />

<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>gates/checknameawal/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id);
                    }
        });
    });
</script>
<?php if ($check == 1): ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#rfid').focus();
    });
</script>
<?php else: ?>
<script type="text/javascript">
    $(document).ready(function () {
        //$('#InputName').focus();
        $('#rfid').focus();
    });
</script>
<?php endif;?>
<?php if ($gate == 26 || $gate == 27): ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#InputName').focus();
    });
</script>
<?php endif;?>
<script type="text/javascript">
    $(function() {
        update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>gates/curr_check/<?php echo $customs; ?>/<?php echo $gate; ?>", function(data) {
            $("p#results").empty();

            $.each(data.json, function() {
                $("p#results").append("\
                        \n\
                    Currently Checked In <b><span>: " + this['total'] + "</span></b>\
                        \n\
                        ");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function later(param,param2,param3){
        //alert(param);
        top.location.href="<?php echo site_url('gates/checklater'); ?>/"+param+"/"+param2+"/"+param3;
    }
</script>
<div class="text-center">
    <?php if ($check == 1): ?>
    <h2 class="tap-wristband"><p class="notify"><b><span>Check-In</span></b></p></h2>
    <h2 class="tap-wristband"><p class="notify" id="results"></p></h2>
    <?php else: ?>
    <h2 class="tap-wristband"><p class="notify"><b><span>Check-Out</span></b></p></h2>
    <h2 class="tap-wristband"><p class="notify" id="results"></p></h2>
    <?php endif;?>
    <p style="font-style:italic;font-size:18px;"><?php echo $places_name; ?></p>
    <table style="margin: 0 auto;">
        <tr>
            <?php if ($check == 1): ?>
            <?php if ($gate != 37): ?>
            <th>
                <h2 class="tap-wristband text-center"><p id="tap-code"><span>Tap Your Card</span></p></h2>
                <form id="input_submit" action="<?php echo current_url(); ?>" method="post">
                    <div class="input-form">
                        <p>
                            <input id="rfid" type="text" autocomplete="off" name="serial" value="" autofocus />
                        </p>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Check RFID</button>
                    </div>
                </form>
            </th>
            <?php endif;?>
            <th>
                <h2 class="tap-wristband text-center"><p id="tap-code"><span>Fill your name</span></p></h2>
                <form id="input_submit" action="<?php echo site_url('gates/getname/' . $customs . '/' . $check . '/' . $gate); ?>" method="post">
                    <div class="input-form">
                        <p>
                            <input id="InputName" type="text" autocomplete="off" name="name" value="" autofocus />
                            <input id="InputId" type="hidden" autocomplete="off" name="id" value="" autofocus />
                        </p>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Check Name</button>
                    </div>
                </form>
            </th>
            <?php else: ?>
            <?php if ($gate != 21): ?>
            <th>
                <h2 class="tap-wristband text-center"><p id="tap-code"><span>Tap Your Card</span></p></h2>
                <form id="input_submit" action="<?php echo current_url(); ?>" method="post">
                    <div class="input-form">
                        <p>
                            <input type="text" autocomplete="off" name="serial" value="" autofocus />
                        </p>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Check RFID</button>
                    </div>
                </form>
            </th>
            <th>
                <h2 class="tap-wristband text-center"><p id="tap-code"><span>Fill your name</span></p></h2>
                <form id="input_submit" action="<?php echo site_url('gates/getname/' . $customs . '/' . $check . '/' . $gate); ?>" method="post">
                    <div class="input-form">
                        <p>
                            <input id="InputName" type="text" autocomplete="off" name="name" value="" autofocus />
                            <input id="InputId" type="hidden" autocomplete="off" name="id" value="" autofocus />
                        </p>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Check Name</button>
                    </div>
                </form>
            </th>
            <?php endif;?>
            <?php endif;?>
        </tr>
    </table>
    <?php if ($gate != 26 && $gate != 37): ?>

    <div id="message_ajax" style="margin-top: 15px;">

            <a href="<?php echo site_url('gates/checklater/' . $customs . '/' . $check . '/' . $gate); ?>">
            <button type="submit" class="btn btn-info">Later</button>
            </a>
    </div>
    <?php endif;?>
    <?php if ($gate == 37): ?>

    <div id="message_ajax" style="margin-top: 15px;">
            <a href="<?php echo site_url('regis'); ?>">
            <button type="submit" class="btn btn-success">Add user</button>
            </a>
    </div>
    <?php endif;?>
</div>