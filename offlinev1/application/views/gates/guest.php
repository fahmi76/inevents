<script type="text/javascript">
    $(document).ready(function () {
        $('#name').focus();
        // This button will increment the value
        $('.add').on('click',function(){
            var $qty=$(this).closest('p').find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
            }
        });
        $('.minus').on('click',function(){
            var $qty=$(this).closest('p').find('.qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
            }
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        //update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>gates/curr_check/<?php echo $customs; ?>/<?php echo $gate; ?>", function(data) {
            $("p#results").empty();

            $.each(data.json, function() {
                $("p#results").append("\
                        \n\
                                Currently Checked In <b><span>: \n\
                                    " + this['total'] + "\
                                \n\
                        </span></b>");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
    function ShowHideDiv(param) {
        var dvPassport = document.getElementById("dvPassport");
        if(param == 'yes'){
            $('#name').focus();
            dvPassport.style.display = "block";
        }else{
            dvPassport.style.display = "none";
        }
    }
</script>
<div class="text-center">
    <h2 class="tap-wristband"><p id="tap-code"><span>GUEST:</span></p></h2>
    
        <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
    <form method="post" role="form" action="<?php echo current_url() ; ?>" id="registration">

        <div class="form-group">
            <br>
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-warning" onclick="ShowHideDiv('yes')">
                    <input type="radio" name="guest" id="chkYes" value="1" > YES
                </label>
                <label class="btn btn-warning" onclick="ShowHideDiv('no')">
                    <input type="radio" name="guest" id="chkNo" value="2"> NO
                </label>
                <label class="btn btn-warning" onclick="ShowHideDiv('yes')">
                    <input type="radio" name="guest" id="chkYes" value="3"> LATER
                </label>
            </div>
        </div> 
        
        <div class="form-group" id="dvPassport" style="display: none">
            <p>
            <label for="InputEmail">Adult Guest</label><br>    
            <input type='button' value='-' id="minus1" class="minus" />
            <input type='text' id="qty1" name='quantity' value='1' class='qty' />
            <input type='button' value='+' id="add1" class="add" /></p>
            <p>
            <label for="InputEmail">Child Guest</label><br>    
            <input type='button' value='-' id="minus2" class="minus" />
            <input type='text' id="qty2" name='quantitychild' value='1' class='qty' />
            <input type='button' value='+' id="add2" class="add"/></p>
        </div>
        <div class="text-center">            
            <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        </div>
    </form>
</div> 