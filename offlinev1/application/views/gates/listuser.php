<h2 class="text-center">List User</h2>
<hr>
<div class="text-center">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $row): ?>
                    <tr>
                        <td>
                            <?php if (isset($row->account_avatar) && $row->account_avatar != ''): ?>
                                <img src="<?php echo base_url(); ?>resizer/thumb.php?img=user/<?php echo $row->account_avatar; ?>" alt="" />
                            <?php else: ?>
                                <img src="<?php echo base_url(); ?>resizer/thumb.php?img=user/default.png" alt="" />
                            <?php endif; ?>
                        </td>
                        <td><?php echo $row->account_displayname; ?></td>
                        <td><?php echo $row->log_time; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>      
</div>