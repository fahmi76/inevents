<style type="text/css">
.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>
<script type="text/javascript">
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            isValid = true;
        if (isValid){
            nextStepWizard.removeAttr('disabled').trigger('click');
        }
    });

    allPrevBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

          prevStepWizard.trigger('click');

            prevStepWizard.removeAttr('enabled').trigger('click');
    });
    $('div.setup-panel div a.btn-primary').trigger('click');
});

    function view(param,param2){
        var token = document.getElementById('token').value;

        $.ajax({
          url: '<?=site_url('quisioner/save/' . $places_id);?>',
          type: 'POST',
          dataType: "json",
          data: 'qid='+param+'&aid='+param2+'&tid='+token,
          success: function(data) {
            //document.getElementById("status_team_"+id_div).innerHTML = data;
            console.log('success');
            //console.log(data);
          },
            error: function(e) {
          }
        });

    }
    function cancel(){
        var product_id = $('#token').val();
        console.log(product_id);
        bootbox.confirm("Do you want to cancel this survey?", function(result) {
            // ajax delete data to database
            if(result){
              $.ajax({
                  type: "DELETE",
                  url: '<?=site_url('quisioner/delete/' . $places_id);?>' + '/' + product_id,
                  success: function (data) {
                      console.log(data);
                      location.reload();
                  },
                  error: function (data) {
                      console.log('Error:', data);
                  }
              });
            }
        });
    }
    function done(){
        //alert(param);
        top.location.href="<?php echo site_url('quisioner/finish/' . $places_id . '/' . $survey); ?>";
    }
</script>

<h2 class="text-center"><b><?php echo $template->question_order; ?></b></h2>
<hr>
<div class="text-center">
    <div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <?php $x = 1;foreach ($data as $row): ?>
        <div class="stepwizard-step">
        <?php if ($x != 1): ?>
            <a href="#step-<?php echo $x; ?>" type="button" class="btn btn-default btn-circle" disabled="disabled"><?php echo $x; ?></a>
        <?php else: ?>
            <a href="#step-<?php echo $x; ?>" type="button" class="btn btn-primary btn-circle"><?php echo $x; ?></a>
        <?php endif;?>
            <p>Step <?php echo $x; ?></p>
        </div>
        <?php $x++;endforeach;?>
        <div class="stepwizard-step">
            <a href="#step-<?php echo $x; ?>" type="button" class="btn btn-default btn-circle" disabled="disabled"><?php echo $x; ?></a>
            <p>Finish</p>
        </div>
    </div>
</div>
    <input type="hidden" id="token" value="<?php echo $token; ?>">
    <?php $x = 1;foreach ($data as $row): ?>
    <div class="row setup-content" id="step-<?php echo $x; ?>">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3 style="font-size: 40px;"> <?php echo $row->nama; ?></h3>
                <?php
$sql = "SELECT id,answer as nama FROM `wooz_quisioner_answer` where question_id = '" . $row->id . "' order by id asc";
$query = $this->db->query($sql);
$answer = $query->result();
?>
                    <div style="text-align: center;">
                <?php foreach ($answer as $rowanswer): ?>
                    <div class="form-group">
                        <button type="button" onclick="view('<?php echo $row->id; ?>','<?php echo $rowanswer->id; ?>');" class="btn nextBtn btn-primary btn-xlg"><?php echo $rowanswer->nama; ?></button>
                    </div>
                <?php endforeach;?>
                    </div>
                <?php if ($x != 1): ?>
                <button class="btn btn-info prevBtn btn-lg pull-left" type="button" >Back</button>
                <button class="btn btn-danger btn-lg pull-right" onclick="cancel();" type="button" >Cancel</button>
                <?php endif;?>
            </div>
        </div>
    </div>
    <?php $x++;endforeach;?>
    <div class="row setup-content" id="step-<?php echo $x; ?>"><br>
        <div class="col-xs-12">
            <div class="col-md-12">
                <h2 class="text-center"><b><?php //echo $template->question_finish; ?>
                <?php //echo $template->question_message; ?>
                Enter Your Details for a Chance to Win</b></h2>
                <div class="col-md-6 col-md-offset-3">
                <form method="post" role="form" action="" id="registration">
                    <div class="form-group has-error">
                        <label for="InputName">Full Name</label>
                        <input type="text" class="form-control input-lg" name="name" id="InputName" autocomplete="off" placeholder="Your Name">
                    </div>
                    <div class="form-group has-error">
                        <label for="InputName">Email</label>
                        <input type="email" class="form-control input-lg" name="email" id="InputEmail" autocomplete="off" placeholder="Your Email">
                    </div>
                    <div class="form-group has-error">
                        <label for="InputName">Contact</label>
                        <input type="text" class="form-control input-lg" name="contact" id="InputContact" autocomplete="off" placeholder="Your Contact">
                    </div>
                    <div class="form-group has-error">
                        <label for="InputName">Location</label>
                        <input type="text" class="form-control input-lg" name="location" id="InputLocation" autocomplete="off" placeholder="Your Location">
                    </div>
                    <div id="result"></div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
                    </div>
                </form>
                </div>
            </div>
            <br>            <br>
            <!--
            <button type="button" onclick="done();" class="btn btn-primary btn-xlg">Done</button>
            -->
        </div>
    </div>
</div>

<script type="text/javascript">

    function cek_error(kode, e) {
        if (!kode.val()) {
            error = 0;
            // Add errors highlight
            kode.closest('.form-group').removeClass('has-success').addClass('has-error');
            // Stop submission of the form
            e.preventDefault();
        } else {
            error = 1;
            // Remove the errors highlight
            kode.closest('.form-group').removeClass('has-error').addClass('has-success');
        }
        return error;
    }
$('#InputName').closest('.form-group').removeClass('has-error');
$('#InputEmail').closest('.form-group').removeClass('has-error');
$('#InputContact').closest('.form-group').removeClass('has-error');
$('#InputLocation').closest('.form-group').removeClass('has-error');

$("#registration").submit(function(e) {
    e.preventDefault();
    var token = document.getElementById('token').value;
    var error1 = 1;
    var error2 = 1;
    var error3 = 1;
    var error4 = 1;
    var my_url = "<?php echo base_url(); ?>quisioner/saveaccount/<?php echo $places_id; ?>/<?php echo $survey; ?>/"+token;

    error1 = cek_error($('#InputName'), e);
    error2 = cek_error($('#InputEmail'), e);
    error3 = cek_error($('#InputContact'), e);
    error4 = cek_error($('#InputLocation'), e);
    if (error1 == 1 && error2 == 1 && error3 == 1 && error4 == 1) {
        var formData = {
            name: $('#InputName').val(),
            email: $('#InputEmail').val(),
            contact: $('#InputContact').val(),
            location: $('#InputLocation').val(),
        }
        console.log(formData);
        $.ajax({
            type: "POST",
            url: my_url,
            data: formData,
            dataType: 'json',
            beforeSend: function() {
                $("#result").html('<div class="col-lg-6 col-lg-offset-2"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
                $("#btn-map").prop('disabled', true); // disable button
            },
            success: function(data) {
                console.log(data);
                done();
            },
            error: function(data) {
                // bootbox.alert({
                //     message: "ada kesalahan silahkan coba lagi",
                //     size: 'small',
                //     callback: function() {
                //         $("#btn-map").prop('disabled', false); // disable button
                //     }
                // });
                console.log('Error:', data);
            }
        });

    }
});
</script>