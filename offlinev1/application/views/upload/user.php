<script type="text/javascript">
    $(function() {
        update_content();
        update_images();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>upload/userdata", function(data) {
            $("tbody#results").empty();

            $.each(data.json, function() {
                $("tbody#results").append("\
                        <tr>\n\
                            <td>"+ this['no'] +"</td><td>"+ this['name'] +"</td><td>"+ this['email'] +"</td><td>"+ this['team'] +"</td>\n\
                        <td>"+ this['department'] +"</td><td>"+ this['meal'] +"</td><td>"+ this['status'] +"</td></tr>");
            });
            setTimeout(function() {
                update_content();
            }, 4000);
        });
    }
    function update_images() {
        $.getJSON("<?php echo base_url(); ?>upload/userupload", function(data) {
            setTimeout(function() {
                update_images();
            }, 10000);
        });
    }
    function userupload(id){
      $.ajax({
          url : "<?php echo base_url(); ?>upload/userindiv/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
            if(data.json == 'kosong'){
                alert('error upload');
            }else if(data.json == 'error'){
                alert('check connection');
            }else{
                alert('success');
                location.reload();
            }

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
    }

    function excel(){
        top.location.href="<?php echo base_url(); ?>upload/exceluser";
    }
</script>

<div class="text-center">
<p class="lead">Upload User Data</p>
<p class="lead"><button class="btn btn-primary" onclick="excel()" type="button">export excel</button></p>
</div>

<div class="row">
    <div class="panel panel-primary filterable">
        <table class="table">
            <thead>
                <tr class="filters">
                    <th>No</th>
                    <th>Name</th>
                    <th>Branch</th>
                    <th>Team</th>
                    <th>Department</th>
                    <th>Meal Preference</th>
                    <th>status</th>
                </tr>
            </thead>
            <tbody id="results">

            </tbody>
        </table>

    </div>
</div>