<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Upload Content</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="<?php echo base_url(); ?>assets/assets/bootstrap.min.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/assets/jquery.min.js"></script>
        <!--[if lt IE 9]>
            <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href="<?php echo base_url(); ?>assets/assets/styles.css" rel="stylesheet">
    </head>
    <body>
<div class="container">
  <?php echo $content; ?>

</div><!-- /.container -->
    <script src="<?php echo base_url(); ?>assets/assets/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/table.js"></script>
    </body>
</html>