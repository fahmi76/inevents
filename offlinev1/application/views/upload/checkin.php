<script type="text/javascript">
    $(function() {
        update_content();
        update_images();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>upload/checkindata", function(data) {
            $("tbody#results").empty();

            $.each(data.json, function() {
                $("tbody#results").append("\
                        <tr>\n\
                            <td>"+ this['no'] +"</td><td>"+ this['name'] +"</td><td>"+ this['email'] +"</td><td>"+ this['team'] +"</td><td>"+ this['department'] +"</td><td>"+ this['meal'] +"</td>\n\
                        <td>"+ this['guest'] +"</td><td>"+ this['time'] +"</td><td>"+ this['status'] +"</td></tr>");
            });
            setTimeout(function() {
                update_content();
            }, 4000);
        });
    }
    function update_images() {
        $.getJSON("<?php echo base_url(); ?>upload/checkinupload", function(data) {
            setTimeout(function() {
                update_images();
            }, 10000);
        });
    }

    function userupload(id){
      $.ajax({
          url : "<?php echo base_url(); ?>upload/checkinindividu/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
            if(data.json == 'kosong'){
                alert('error upload');
            }else if(data.json == 'error'){
                alert('check connection');
            }else{
                alert('success');
                location.reload();
            }

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
    }

    function excel(){
        top.location.href="<?php echo base_url(); ?>upload/excelcheckin";
    }
</script>

<div class="text-center">
<p class="lead">Upload Checkin Data</p>
<p class="lead"><button class="btn btn-primary" onclick="excel()" type="button">export excel</button></p>
</div>

<div class="row">
    <div class="panel panel-primary filterable">
        <table class="table">
            <thead>
                <tr class="filters">
                    <th>No</th>
                    <th>Name</th>
                    <th>Branch</th>
                    <th>Team</th>
                    <th>Department</th>
                    <th>Meal Preference</th>
                    <th>Checkin</th>
                    <th>Time</th>
                    <th>status</th>
                </tr>
            </thead>
            <tbody id="results">

            </tbody>
        </table>

    </div>
</div>