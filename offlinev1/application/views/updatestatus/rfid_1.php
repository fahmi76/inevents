<script type="text/javascript">
    base_url = "<?php echo base_url();?>";
    main_url = "<?php echo site_url();?>";
    nice1 = "<?php echo $nice1;?>";
    nice2 = "<?php echo $nice2;?>";

    $(document).ready(function () {
        $('#rfid').focus();
        $('#loading').hide();
        $('#input_submit').show();

        $('#input_submit').bind('submit', function () {
            $('#loading').show();
            $('#input_submit').show();
            $.ajax({
                url: main_url + "updatestatus/updates/" + nice1 + "/" + nice2,
                type: "POST",
                data: {
                    card: $('#rfid').val(),
                    booth: $('#serial').val()
                },
                dataType: "json",
                success: function (data) {
                    if (data.status == "true") {
                        $('#rfid').focus();
                        $('#message_ajax').show();
                        $('#loading').hide();
                        $('#input_submit').hide();
                        $("#message_ajax").html("<div id='successMessage' class='successMessage'><span><font color='blue' size='15'>" + data.message + "</font></span> Thank You for checkin in <font color='green'>"+data.message2+"</font></div>");
                        
                        setTimeout(function () {
                            $('#message_ajax').hide();
                            $('#input_submit').show();
                            $('#rfid').focus();
                        }, 2000);
                    }
                    else {
                        $('#rfid').focus();
                        $('#message_ajax').show();
                        $('#loading').hide();
                        $('#input_submit').hide();
                        $("#message_ajax").html("<div class='successMessage'><font color='blue' size='15'>" + data.message + "</font></br>Failed due to <font color='red'>" + data.message2 + " </font></div>");
                        setTimeout(function () {
                            $('#message_ajax').hide();
                            $('#input_submit').show();
                            $('#rfid').focus();
                        }, 2000);
                    }

                }
            });
            $('#rfid').val('');
            $('#serial').val();
            return false;
        });
    });
</script>
<script type="text/javascript">
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
</script>
<div class="text-center">
    <h2 class="tap-wristband"><p id="tap-code"><span>Tap Your Card Here</span></p></h2>
    <h2 class="tap-wristband"><p class="notify"><b><span><?php echo $places_name; ?></span></b></p></h2>
    <p style="font-style:italic;font-size:38px;"><?php echo $places_text; ?></p>

    <div id="loading"><img src="<?php echo base_url(); ?>assets/images/ajax-loader.gif" alt="loading..." /></br>Please Wait</div>
    <form id="input_submit" method="post">
        <div class="input-form">
            <p>
                <input id="rfid" type="text" onblur="displayResult(this);" name="serial" value="" autofocus /><br />
                <input id="serial" type="hidden" name="fullname" value="<?php echo $nice2; ?>" />
            </p>
            <input type="submit" name="submit" value="submit" />
        </div>
    </form>

    <div id="message_ajax"></div>
</div> 