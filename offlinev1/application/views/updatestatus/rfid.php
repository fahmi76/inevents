<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>updatestatus/checknameawal/22",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id); 
                    }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#rfid').focus();
    });
</script>
<script type="text/javascript">
    $(function() {
        update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>updatestatus/curr_check/28", function(data) {
            $("p#results").empty();

            $.each(data.json, function() {
                $("p#results").append("\
                        \n\
                    Currently Checked In <b><span>: " + this['total'] + "</span></b>\
                        \n\
                        ");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function later(param,param2,param3){
        //alert(param);
        top.location.href="<?php echo site_url('gates/checklater');?>/"+param+"/"+param2+"/"+param3;
    }
</script>
<div class="text-center">
    <h2 class="tap-wristband"><p class="notify"><b><span>Check-In</span></b></p></h2>
    <h2 class="tap-wristband"><p class="notify" id="results"></p></h2>
    <p style="font-style:italic;font-size:18px;"><?php echo $places_name; ?></p>
    <table style="margin: 0 auto;">
        <tr>    
            <th>
                <h2 class="tap-wristband text-center"><p id="tap-code"><span>Tap Your Card</span></p></h2>
                <form id="input_submit" action="<?php echo site_url('updatestatus/home/28');?>" method="post">
                    <div class="input-form">
                        <p>
                            <input id="rfid" type="text" autocomplete="off" name="serial" value="" autofocus />
                        </p>
                    </div>
                    <div class="text-center">            
                        <button type="submit" class="btn btn-primary">Check RFID</button>
                    </div>
                </form>
            </th>
            <th> 
                <h2 class="tap-wristband text-center"><p id="tap-code"><span>Fill your name</span></p></h2>
                <form id="input_submit" action="<?php echo site_url('updatestatus/getname/28');?>" method="post">
                    <div class="input-form">
                        <p>
                            <input id="InputName" type="text" autocomplete="off" name="name" value="" autofocus />
                            <input id="InputId" type="hidden" autocomplete="off" name="id" value="" autofocus />
                        </p>
                    </div>
                    <div class="text-center">            
                        <button type="submit" class="btn btn-primary">Check Name</button>
                    </div>
                </form>
            </th>
        </tr>
    </table>
</div> 