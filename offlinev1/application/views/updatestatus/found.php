<?php if($datastatus != 0): ?>
		<?php if($datastatus == 1): ?>
		<h2 class="text-center" style="color: green;font-weight: bold;font-size: 250%;">ALLOWED TO ENTER</h2>
		<?php elseif($datastatus == 2): ?>
		<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">ALREADY CHECKED IN</h2>
		<?php else: ?>
		<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">NOT REGISTERED</h2>
		<?php endif; ?>
<?php else: ?>
	<h2 class="text-center" style="color: red;font-weight: bold;font-size: 32px;">NOT REGISTERED</h2>
<?php endif;?>

<hr>
<div class="text-center">
    <h2 style="font-size: 500%;"><b><?php echo $datauser['name']; ?></b></h2>
    <h3 style="font-size: 400%;"><?php echo 'Branch : <br><b>'.$datauser['branch'].'</b>'; ?></h3>    
    <hr>      
</div>

<meta http-equiv="refresh" content="2;URL=<?php echo site_url('updatestatus/home/28'); ?>" />
