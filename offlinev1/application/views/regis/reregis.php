<script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://wooz.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<script type="text/javascript">
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputName").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>regis/checkdata/1",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id); 
                        $("#InputEmail").val(ui.item.data1); 
                        $("#InputTelp").val(ui.item.data2);
                    }
        });
    });

    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputEmail").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>regis/checkdata/2",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id); 
                        $("#InputName").val(ui.item.data1); 
                        $("#InputTelp").val(ui.item.data2);
                    }
        });
    });
    
    $(this).ready(function() {
        jQuery.noConflict();
        $("#InputTelp").autocomplete({
            minLength: 1,
            source:
                    function(req, add) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>regis/checkdata/3",
                            dataType: 'json',
                            type: 'POST',
                            data: req,
                            success:
                                    function(data) {
                                        if (data.response == "true") {
                                            add(data.message);
                                        }
                                    }
                        });
                    },
            select:
                    function(event, ui) {
                        $("#InputId").val(ui.item.id); 
                        $("#InputName").val(ui.item.data1); 
                        $("#InputEmail").val(ui.item.data2);
                    }
        });
    });
    
</script>
<?php if (validation_errors()) : ?>
    <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', ''); ?>
    </p>
<?php endif; ?> 
<br />
<form method="post" action="<?php echo current_url(); ?>" id="registration">
    <div class="form-group">
        <label for="InputName">Fill your name</label>   
        <input type="hidden" class="form-control input-lg" name="id" id="InputId" value=""  placeholder="id">    
        <input type="text" class="form-control input-lg" name="name" id="InputName" value=""  placeholder="name">
    </div>
    <div class="form-group">
        <label for="InputName">or Fill your email</label>       
        <input type="text" class="form-control input-lg" name="email" id="InputEmail" value=""  placeholder="email@email.com">
    </div>
    <div class="form-group">
        <label for="InputName">or Fill your phone</label>       
        <input type="text" class="form-control input-lg" name="telp" id="InputTelp" value=""  placeholder="82323232">
    </div>

    <div class="text-center">            
        <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        <a href="<?php echo site_url('regis')?>">
            <button type="button" class="btn btn-default btn-xlg">New Register</button>
        </a>
    </div>
</form>