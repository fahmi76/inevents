<script type="text/javascript">
    $(document).ready(function() {
        $("#InputName").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://woozin.tgrid.in/assets/customs/loader.gif" /> Please Wait...');
        });
    });
</script>
<div class="panel panel-info">
    <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
        </p>
    <?php endif; ?>

</div>
<form method="post" role="form" action="<?php echo current_url(); ?>" id="registration">
        <div class="form-group">
            <label for="InputName">Full Name</label>        
            <?php
            $name = set_value('name');
            if ($name) {
                $names = $name;
            } elseif($info){
                $names = $info->account_displayname;
            } else {
                $names = '';
            }
            ?>
            <?php echo form_error('name', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="name" id="InputName" autocomplete="off" value="<?php echo $names; ?>" placeholder="your name">
        </div>

        <div class="form-group">
            <label class="InputName">Email</label>      
            <?php
            $email = set_value('email');
            if ($email) {
                $emails = $email;
            } elseif($info){
                $emails = $info->email;
            } else {
                $emails = '';
            }
            ?>
            <?php echo form_error('email', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="email" id="InputName" autocomplete="off"  placeholder="Email" value="<?php echo $emails; ?>">
        </div>  
        <div class="form-group">
            <label class="InputName">Telephone</label>  
            <?php
            $telephone = set_value('telephone');
            if ($telephone) {
                $telephones = $telephone;
            } elseif($info){
                $telephones = $info->telephone;
            } else {
                $telephones = '';
            }
            ?>
            <?php echo form_error('telephone', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="telephone" id="InputName" autocomplete="off"  placeholder="Telephone" value="<?php echo $telephones; ?>">
        </div>  
        <div class="form-group">
            <label class="InputName">Category</label>  
            <?php
            $category = set_value('category');
            if ($category) {
                $categorys = $category;
            } elseif($info){
                $categorys = $info->category;
            } else {
                $categorys = '';
            }
            ?>
            <?php echo form_error('category', '<p class="error">', '</p>'); ?>
            <div class="controls">
                <select class="form-control" name="category">
                    <option value="">-Please Choose-</option>
                    <option value="Artisan" <?php if ($categorys == 'Artisan') : ?>selected="selected"<?php endif; ?>>Artisan</option>
                    <option value="Beautician (Hair/Make Up)" <?php if ($categorys == 'Beautician (Hair/Make Up)') : ?>selected="selected"<?php endif; ?>>Beautician (Hair/Make Up)</option>
                    <option value="Buyer" <?php if ($categorys == 'Buyer') : ?>selected="selected"<?php endif; ?>>Buyer</option>
                    <option value="Consultant" <?php if ($categorys == 'Consultant') : ?>selected="selected"<?php endif; ?>>Consultant</option>
                    <option value="Creative" <?php if ($categorys == 'Creative') : ?>selected="selected"<?php endif; ?>>Creative</option>
                    <option value="Designer" <?php if ($categorys == 'Designer') : ?>selected="selected"<?php endif; ?>>Designer</option>
                    <option value="Educator" <?php if ($categorys == 'Educator') : ?>selected="selected"<?php endif; ?>>Educator</option>
                    <option value="Enthusiast/Consumer" <?php if ($categorys == 'Enthusiast/Consumer') : ?>selected="selected"<?php endif; ?>>Enthusiast/Consumer</option>
                    <option value="Fabric supplier" <?php if ($categorys == 'Fabric supplier') : ?>selected="selected"<?php endif; ?>>Fabric supplier</option>
                    <option value="Financier" <?php if ($categorys == 'Financier') : ?>selected="selected"<?php endif; ?>>Financier</option>
                    <option value="Media" <?php if ($categorys == 'Media') : ?>selected="selected"<?php endif; ?>>Media</option>
                    <option value="Model" <?php if ($categorys == 'Model') : ?>selected="selected"<?php endif; ?>>Model</option>
                    <option value="Photographer" <?php if ($categorys == 'Photographer') : ?>selected="selected"<?php endif; ?>>Photographer</option>
                    <option value="Retailer" <?php if ($categorys == 'Retailer') : ?>selected="selected"<?php endif; ?>>Retailer</option>
                    <option value="Seamstress" <?php if ($categorys == 'Seamstress') : ?>selected="selected"<?php endif; ?>>Seamstress</option>
                    <option value="Sponsor" <?php if ($categorys == 'Sponsor') : ?>selected="selected"<?php endif; ?>>Sponsor</option>
                    <option value="Stitcher" <?php if ($categorys == 'Stitcher') : ?>selected="selected"<?php endif; ?>>Stitcher</option>
                    <option value="Tailor" <?php if ($categorys == 'Tailor') : ?>selected="selected"<?php endif; ?>>Tailor</option>
                </select>
            </div>
        </div> 
        <div class="form-group">
            <label class="InputName">Address</label>  
            <?php
            $address = set_value('address');
            if ($address) {
                $addresss = $address;
            } elseif($info){
                $addresss = $info->address;
            } else {
                $addresss = '';
            }
            ?>
            <?php echo form_error('address', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="address" id="InputName" autocomplete="off"  placeholder="Address" value="<?php echo $addresss; ?>">
        </div>  
        <div class="form-group">
            <label class="InputName">Country</label>  
            <?php
            $country = set_value('country');
            if ($country) {
                $countrys = $country;
            } elseif($info){
                $countrys = $info->country;
            } else {
                $countrys = '';
            }
            ?>
            <?php echo form_error('country', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="country" id="InputName" autocomplete="off"  placeholder="Country" value="<?php echo $countrys; ?>">
        </div>  
        <div class="form-group">
            <label class="InputName">Company Name</label>  
            <?php
            $companyname = set_value('company-name');
            if ($companyname) {
                $companynames = $companyname;
            } elseif($info){
                $companynames = $info->company_name;
            } else {
                $companynames = '';
            }
            ?>
            <?php echo form_error('company-name', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="company-name" id="InputName" autocomplete="off"  placeholder="Company Name" value="<?php echo $companynames; ?>">
        </div>  
        <div class="form-group">
            <label class="InputName">Company Description</label>  
            <?php
            $companydescription = set_value('company-description');
            if ($companydescription) {
                $companydescriptions = $companydescription;
            } elseif($info){
                $companydescriptions = $info->company_description;
            } else {
                $companydescriptions = '';
            }
            ?>
            <?php echo form_error('company-description', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="company-description" id="InputName" autocomplete="off"  placeholder="Company Description" value="<?php echo $companydescriptions; ?>">
        </div>  
        <div class="form-group">
            <label class="InputName">Position</label>  
            <?php
            $position = set_value('position');
            if ($position) {
                $positions = $position;
            } elseif($info){
                $positions = $info->position;
            } else {
                $positions = '';
            }
            ?>
            <?php echo form_error('position', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="position" id="InputName" autocomplete="off"  placeholder="Position" value="<?php echo $positions; ?>">
        </div>  
        <div class="form-group">
            <label class="InputName">Area of Interest</label>  
            <?php
            $areaofinterest = set_value('area-of-interest');
            if ($areaofinterest) {
                $areaofinterests = $areaofinterest;
            } elseif($info){
                $areaofinterests = $info->areaofinterest;
            } else {
                $areaofinterests = '';
            }
            ?>
            <?php echo form_error('area-of-interest', '<p class="error">', '</p>'); ?>
            <input type="text" class="form-control input-lg" name="area-of-interest" id="InputName" autocomplete="off"  placeholder="Area of Interest" value="<?php echo $areaofinterests; ?>">
        </div>       
        <div class="text-center">            
            <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
        </div>
</form>