<link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css') ?>" rel="stylesheet">
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    List All Checkin
                </h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <br />

        <h3>Checkin Data</h3>
        <br />
        <button class="btn btn-danger" onclick="deleteuser()"><i class="glyphicon glyphicon-remove"></i> Delete</button>
        <br />
        <br />
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Branch</th>
                    <th>Department</th>
                    <th>Team</th>
                    <th>Meal Preference</th>
                    <th>Checkin</th>
                    <th style="width:125px;">Time</th>
                </tr>
            </thead>
            <tbody>
            </tbody>

            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Branch</th>
                    <th>Department</th>
                    <th>Team</th>
                    <th>Meal Preference</th>
                    <th>Checkin</th>
                    <th style="width:125px;">Time</th>
                </tr>
            </tfoot>
        </table>
    </div>
    </div>
    </div>
    </div>
    </div>

<script src="<?php echo base_url('assets/jquery/jquery-2.1.4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js') ?>"></script>


<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('checkin/ajax_list') ?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

});

function eventplaces(){
    $.ajax({
      url: '<?=site_url('checkin/sinkron');?>',
      type: 'POST',
      dataType: "json",
      success: function(data) {
        //document.getElementById("status_team_"+id_div).innerHTML = data;
        reload_table();
        //console.log(data);
      },
        error: function(e) {
      }
    });
}

function deleteuser(){
    if(confirm('Are you sure delete all checkin?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('checkin/ajax_delete') ?>",
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}
</script>
