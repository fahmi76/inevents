<script type="text/javascript">
    function report(param){
        //alert(param);
        top.location.href="<?=site_url('admin/reportexcel/report/');?>/"+param;
    }
	function eventplaces(){
		$.ajax({
		  url: '<?=site_url('dashboard/places');?>',
		  type: 'POST',
		  dataType: "json",
		  success: function(data) {
			//document.getElementById("status_team_"+id_div).innerHTML = data;
			location.reload();
			//console.log(data);
		  },
			error: function(e) {
		  }
		});
    }
	function gate(){
		$.ajax({
		  url: '<?=site_url('dashboard/gate');?>',
		  type: 'POST',
		  dataType: "json",
		  success: function(data) {
			//document.getElementById("status_team_"+id_div).innerHTML = data;
			location.reload();
			//console.log(data);
		  },
			error: function(e) {
		  }
		});
	}
</script>
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">
            <div class="title_left">
                <h3>
			        Inevents
			    </h3>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Events</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p><button class="btn btn-info btn-xs" onclick="eventplaces();" type="button"><i class="fa fa-refresh"></i> Synchronize Event</button></p>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Images</th>
                                    <th>Event Name</th>
                                    <th>End Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td><img src="<?php echo base_url() . $places->places_backgroud; ?>" style="width: 100px; height: 100px;"></td>
                                    <td><?php echo $places->places_name; ?></td>
                                    <td><?php echo $places->places_duedate; ?></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Gate Check-in</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <p><button class="btn btn-info btn-xs" onclick="gate();" type="button"><i class="fa fa-refresh"></i> Synchronize Gate</button></p>

                        <table class="table table-striped responsive-utilities jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th class="column-title">No </th>
                                    <th class="column-title">Name </th>
                                    <!--
                                    <th class="column-title">Action </th> -->
                    			</tr>
			                </thead>

			                <tbody>
			                	<?php if ($gate): ?>
			                	<?php $x = 1;foreach ($gate as $row): ?>
			                	<?php if ($x % 2 == 0): ?>
                                <tr class="even pointer">
                            	<?php else: ?>
                                <tr class="odd pointer">
                            	<?php endif;?>
                                    <td class="a-center ">
                                       <?php echo $x; ?>
                                    </td>
                                    <td class=" "> <?php echo $row->nama; ?></td> <!--
                                    <td class=" "> <button class="btn btn-info btn-xs" onclick="report('<?php echo $row->id; ?>');" type="button"><i class="fa fa-refresh"></i> Report Gate</button></td> -->
                                </tr>
                                <?php $x++;endforeach;?>
                            	<?php else: ?>
                            		<tr><td></td><td>Empty Gate</td><td><button class="btn btn-info btn-xs" onclick="gate();" type="button"><i class="fa fa-refresh"></i> Synchronize Gate</button></td></tr>
                            	<?php endif;?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>