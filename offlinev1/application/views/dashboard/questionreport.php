<script type="text/javascript">
    function report(param){
        //alert(param);
        top.location.href="<?=site_url('admin/reportexcel/report/');?>/"+param;
    }
	function eventplaces(){
		$.ajax({
		  url: '<?=site_url('question/upload');?>',
		  type: 'POST',
		  dataType: "json",
		  success: function(data) {
			//document.getElementById("status_team_"+id_div).innerHTML = data;
            if(data.json == 'kosong'){
                bootbox.alert({
                    message: "data already upload",
                    size: 'small',
                    callback: function () {
                      location.reload();
                    }
                });
            }else{
                bootbox.alert({
                    message: "upload done",
                    size: 'small',
                    callback: function () {
                      location.reload();
                    }
                });
            }
            console.log(data);       //
			//location.reload();
			//console.log(data);
		  },
			error: function(e) {
		  }
		});
    }
    function clearquestion(){
        bootbox.confirm("Are you sure delete all quesioner report?", function(result) {
            // ajax delete data to database
            if(result){
              $.ajax({
                url : "<?php echo site_url('question/ajax_delete') ?>",
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    bootbox.alert({
                        message: "delete success",
                        size: 'small',
                        callback: function () {
                          location.reload();
                        }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    bootbox.alert({
                        message: "Error deleting data",
                        size: 'small',
                        callback: function () {
                          location.reload();
                        }
                    });
                }
              });
            }
        });
    }
</script>
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">
            <div class="title_left">
                <h3>
			        Question Report
			    </h3>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Total User : <b><?php echo $data; ?></b> person</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <p><button class="btn btn-info btn-xs" onclick="eventplaces();" type="button"><i class="fa fa-refresh"></i> Upload Question Report</button>
                    <button class="btn btn-danger btn-xs" onclick="clearquestion();" type="button"><i class="fa fa-close"></i> Clear Question Report</button></p>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th></th>
                                    <th></th>
                                    <th>Tittle</th>
                                    <th>Total</th>
                                    <th>Show</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $x = 1;foreach ($question as $row): ?>
                                    <tr>
                                        <td><?php echo $x; ?></td>
                                        <td></td>
                                        <th></th>
                                        <td><?php echo $row['title']; ?>
                                        <td>
                                        </td>
                                        <td><a href="<?php echo site_url('question/reportgroup/1/' . $row['survey_id']) ?>" class="btn-xs btn-primary">Show Correct User</a><br><a href="<?php echo site_url('question/reportgroup/2/' . $row['survey_id']) ?>" class="btn-xs btn-success">Show All User</a></td>
                                    </tr>

                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Name Quisioner</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <?php $y = 1;foreach ($row['question'] as $row1): ?>
                                    <tr>
                                        <td></td>
                                        <td><?php echo $x . '-' . $y; ?></td>
                                        <td></td>
                                        <td><?php echo $row1['question']; ?>
                                        <td><?php echo $row1['total']; ?>
                                        </td>
                                        <td><a href="<?php echo site_url('question/reportuser/1/' . $row1['question_id']) ?>" class="btn-xs btn-warning">Show Data</a></td>
                                    </tr>

                                    <?php
$sqlanswer = "SELECT id,answer,data_status FROM `wooz_quisioner_answer` where question_id = '" . $row1['question_id'] . "' order by id asc";
$queryanswer = $this->db->query($sqlanswer);
$answer = $queryanswer->result();
?>
                                    <?php $z = 1;if ($answer): ?>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>Answer</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <?php foreach ($answer as $rowanswer): ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><?php echo $x . '-' . $y . '-' . $z; ?></td>
                                            <td><?php echo $rowanswer->answer; ?> (<?php if ($rowanswer->data_status == 1) {echo 'Correct Answer';} else {echo 'Wrong Answer';}?>)</td>
                                            <td><?php

$sqlanswer1 = "SELECT count(a.account_id) as total FROM `wooz_quisioner` a inner join wooz_quisioner_account b on b.account_rfid = a.account_id where a.places_id = 38 and a.question_id = '" . $row1['question_id'] . "' and a.answer_id = '" . $rowanswer->id . "'";
$queryanswer1 = $this->db->query($sqlanswer1);
$answer1 = $queryanswer1->row();
echo $answer1->total;
?>

                                            </td>
                                            <td><a href="<?php echo site_url('question/reportuser/2/' . $rowanswer->id) ?>" class="btn-xs btn-info">Show Data</a></td>
                                        </tr>
                                        <?php $z++;endforeach;?>
                                    <?php endif;?>
                                    <?php $y++;endforeach;?>

                                <?php $x++;endforeach;?>
                        </table>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>

</div>