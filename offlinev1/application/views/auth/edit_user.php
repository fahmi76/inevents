<!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo lang('edit_user_heading');?></h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><small><?php echo lang('edit_user_subheading');?></small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
<p></p>
<?php if($message): ?>
  <div id="infoMessage" class="alert alert-danger alert-dismissible fade in"><?php echo $message;?></div>
<?php endif; ?>

<?php 
$attributes = array('class' => 'form-horizontal form-label-lef', 'id' => 'demo-form2');

echo form_open(uri_string(), $attributes);
?>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('edit_user_fname_label', 'first_name');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($first_name);?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('edit_user_lname_label', 'last_name');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($last_name);?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('edit_user_password_label', 'password');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($password);?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($password_confirm);?>
    </div>
</div>

<?php if ($this->ion_auth->is_admin()): ?>
<div class="form-group">
    <label class="col-md-3 col-sm-3 col-xs-12 control-label"><?php echo lang('edit_user_groups_heading');?>
    </label>

    <div class="col-md-9 col-sm-9 col-xs-12">
        <?php foreach ($groups as $group):?>
        <div class="checkbox">
            <label>
            <?php
                $gID=$group['id'];
                $checked = null;
                $item = null;
                foreach($currentGroups as $grp) {
                    if ($gID == $grp->id) {
                        $checked= ' checked="checked"';
                    break;
                    }
                }
            ?>
            <input type="checkbox" class="flat" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
            <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
            </label>
        </div>
        <?php endforeach?>
    </div>
</div>

<?php endif ?>
<?php echo form_hidden('id', $user->id);?>
<?php echo form_hidden($csrf); ?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_submit('submit', lang('edit_user_submit_btn'),'class="btn btn-default submit"');?>
    </div>
</div>

<?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
            <!-- /page content -->