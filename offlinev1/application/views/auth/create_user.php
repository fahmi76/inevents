
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Add Pegawai</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><small><?php echo lang('create_user_subheading');?></small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />

<p></p>

<?php if($message): ?>
  <div id="infoMessage" class="alert alert-danger alert-dismissible fade in"><?php echo $message;?></div>
<?php endif; ?>
<?php 
$attributes = array('class' => 'form-horizontal form-label-lef', 'id' => 'demo-form2');

echo form_open('member', $attributes);
?>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('create_user_fname_label', 'first_name');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($first_name);?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('create_user_lname_label', 'last_name');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($last_name);?>
    </div>
</div>
      
      <?php if($identity_column!=='email') : ?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('create_user_identity_label', 'identity');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
          <?php
          echo form_error('identity');
          echo form_input($identity);
          ?>
    </div>
</div>
      <?php endif; ?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('create_user_email_label', 'email');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($email);?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('create_user_password_label', 'password');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($password);?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($password_confirm);?>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_submit('submit', lang('create_user_submit_btn'),'class="btn btn-default submit"');?>
    </div>
</div>

<?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
            <!-- /page content -->