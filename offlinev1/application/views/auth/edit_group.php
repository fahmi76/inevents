<!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo lang('edit_group_heading');?></h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><small><?php echo lang('edit_group_subheading');?></small></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />

<p></p>
<?php if($message): ?>
  <div id="infoMessage" class="alert alert-danger alert-dismissible fade in"><?php echo $message;?></div>
<?php endif; ?>
<?php 
$attributes = array('class' => 'form-horizontal form-label-lef', 'id' => 'demo-form2');

echo form_open(current_url(), $attributes);
?>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('edit_group_name_label', 'group_name');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($group_name);?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo lang('edit_group_desc_label', 'description');?> <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_input($group_description);?>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_submit('submit', lang('edit_group_submit_btn'),'class="btn btn-default submit"');?>
    </div>
</div>

<?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
            <!-- /page content -->