<!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3><?php echo lang('deactivate_heading');?></h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />

<?php 
$attributes = array('class' => 'form-horizontal form-label-lef', 'id' => 'demo-form2');

echo form_open("auth/deactivate/".$user->id, $attributes);
?>

<div class="form-group">
    <label class="col-md-3 col-sm-3 col-xs-12 control-label"><?php echo sprintf(lang('deactivate_subheading'), $user->username);?>
    </label>

    <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="checkbox">
            <label>
  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
    <input type="radio" name="confirm" value="yes" checked="checked" />
            </label>
        </div>
        <div class="checkbox">
            <label>
    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
    <input type="radio" name="confirm" value="no" />
            </label>
        </div>
    </div>
</div>
  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(array('id'=>$user->id)); ?>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php echo form_submit('submit', lang('deactivate_submit_btn'),'class="btn btn-default submit"');?>
    </div>
</div>

<?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
            <!-- /page content -->