<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-type" value="text/html; charset=UTF-8" />
    <title>Wooz.in</title>

    <!-- Bootstrap core CSS -->

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">



    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="<?php echo base_url(); ?>" class="site_title"><i class="fa fa-won"></i>
                        	<span>Inevents</span>
                        </a>
                    </div>
                    <div class="clearfix"></div>


                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">

                                <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-home"></i>
                                    Home </a>
                                </li>
                                <!--
                                <li><a href="<?php echo base_url(); ?>person"><i class="fa fa-won"></i>
                                	User </a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>report"><i class="fa fa-won"></i>
                                    Team Report </a>
                                </li>
                                -->
                                <li><a href="<?php echo base_url(); ?>question/listquestion"><i class="fa fa-won"></i>
                                    List Question </a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>question/report"><i class="fa fa-won"></i>
                                    List Question Report </a>
                                </li>
                                <!--
                                <li><a href="<?php echo base_url(); ?>checkin"><i class="fa fa-won"></i>
                                    Checkin </a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>checkout"><i class="fa fa-won"></i>
                                    Checkout </a>
                                </li>
                                <li>
                                    <a href="#" onclick="window.open('<?php echo base_url(); ?>upload/checkin');return true;"><i class="fa fa-book"></i>
                                    Upload Checkin</a>
                                </li>
                                <li>
                                    <a href="#" onclick="window.open('<?php echo base_url(); ?>upload/user');return true;"><i class="fa fa-book"></i>
                                    Upload New User</a>
                                </li>
                                -->
                                <?php if ($this->ion_auth->is_admin()): ?>
                                    <li><a href="<?php echo base_url(); ?>auth"><i class="fa fa-users"></i>
                                        Members </a>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                               		<?php echo $this->session->userdata('username'); ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="<?php echo site_url('auth/profile'); ?>">  Profile</a>
                                    <li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <?php echo $content; ?>
            <!-- /page content -->
        </div>


    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
    <?php $this->load->view('layouts/footer.php');?>
</body>

</html>