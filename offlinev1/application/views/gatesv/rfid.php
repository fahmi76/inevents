<script type="text/javascript">
    $(document).ready(function() {
        $("#rfid").focus();
    });
    //document.forms['input_submit'].style.visibility = "hidden";
    function displayResult(obj)
    {
        var inp1 = obj.value;
        var inp2 = inp1.length;
        if (inp2 == 0)
        {
            setTimeout(function() {
                obj.focus()
            }, 10);
        }
    }
    function autosubmit(original) {
        if (original.value.length === 10) {
            document.getElementById('input_submit').submit();
        }
    }
</script>

<div class="text-center">
    <h2>Please tap your M Card</h2>
    <br>
    <img src="<?php echo base_url(); ?>assets/magnum/redeem/images/tap.png" class="img-responsive" style="width: 80%">
</div>
<div id="form">
    <?php
    if (validation_errors()) {
        echo validation_errors('<p class="error">Error! ', '</p>');
        echo '<meta http-equiv="refresh" content="2;URL="' . site_url('magnumcafe') . '">';
    } else {
        ?> 
        <form id="input_submit" method="post" action="<?php echo current_url(); ?>?loc=<?php echo $loc; ?>">
            <div class="input-form">
                <p>
                    <input id="rfid" type="text" name="rfid" style="color: #FFEDA9;background-color:transparent;border:0px solid white;border-color:#FFEDA9;" onblur="displayResult(this);" onKeyUp="autosubmit(this);" value="" autocomplete="off"/><br />
                    <input id="serial" type="hidden" name="places" value="<?php echo $loc; ?>" />
                </p>
                <input type="submit" style="width: 0px; height: 0px; border: none; padding: 0; margin: 0;"/>
            </div>
        </form>
        <?php
    }
    ?>
</div>
<div id="message_ajax"></div>
