<?php if($datastatus != 0): ?>
		<?php if($check == 1): ?>
			<?php if($datastatus == 1 || $datastatus == 5): ?>
			<h2 class="text-center" style="background-color: green;color: white;font-weight: bold;font-size: 250%;">ALLOWED TO ENTER</h2>
			<?php elseif($datastatus == 2): ?>
			<h2 class="text-center"  style="background-color: red;color: white;font-weight: bold;font-size: 250%;">ALREADY CHECKED IN</h2>
			<?php elseif($datastatus == 3): ?>
			<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">NOT ALLOWED TO ENTER</h2>
			<?php elseif($datastatus == 4): ?>
			<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">WRONG GATE</h2>
			<?php else: ?>
			<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">NOT REGISTERED</h2>
			<?php endif; ?>
		<?php else: ?>
			<?php if($datastatus == 1 ): ?>
			<h2 class="text-center" style="color: green;font-weight: bold;font-size: 250%;">ALLOWED TO EXIT</h2>
			<?php elseif($datastatus == 2): ?>
			<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">ALREADY CHECKED OUT</h2>
			<?php elseif($datastatus == 3): ?>
			<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">NOT ALLOWED TO CHECKED OUT</h2>
			<?php elseif($datastatus == 4): ?>
			<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">WRONG GATE</h2>
			<?php else: ?>
			<h2 class="text-center" style="color: red;font-weight: bold;font-size: 250%;">NOT REGISTERED</h2>
			<?php endif; ?>
		<?php endif; ?>
<?php else: ?>
	<h2 class="text-center" style="color: red;font-weight: bold;font-size: 32px;">NOT REGISTERED</h2>
<?php endif;?>

<hr>
<div class="text-center">
    <?php if($places_id == 9): ?>
    <?php if (isset($datauser['photo']) && $datauser['photo'] != ''): ?>
        <img src="<?php echo base_url(); ?>resizer/thumbimage.php?img=user/<?php echo $datauser['photo']; ?>" alt="" />
    <?php else: ?>
        <img src="<?php echo base_url(); ?>resizer/thumbimage.php?img=user/default.png" alt="" />
    <?php endif; ?>
    
    <h3><?php echo 'Group: <b>'.$datauser['group'].'</b>'; ?></h3>  
	<?php elseif($places_id == 25): ?> 
    <h2 style="font-size: 300%;"><b><?php echo $datauser['name']; ?></b></h2>
    <?php if($datauser['Guest'] == 'Yes'):?>
    <h3 style="font-size: 300%;color:green;"><?php echo 'Guest: <b>'.$datauser['Guest'].'</b>'; ?></h3>  
	<?php else:?>
    <h3 style="font-size: 300%;color:red;"><?php echo 'Guest: <b>'.$datauser['Guest'].'</b>'; ?></h3>  
	<?php endif; ?>
    <h3><?php echo 'Department: <b>'.$datauser['Department'].'</b>'; ?></h3>  
	<?php else: ?>
    <h2 style="font-size: 500%;"><b><?php echo $datauser['name']; ?></b></h2>
    <h3 style="font-size: 400%;"><?php echo 'Branch : <br><b>'.$datauser['branch'].'</b>'; ?></h3>       
	<?php endif; ?>
    <hr>      
</div>

<?php if($places_id == 25 && ($datauser['Guest'] == 'Yes')):  ?>
	<?php if($datauser['cek_guest'] == 1): ?>
		<meta http-equiv="refresh" content="3;URL=<?php echo site_url('gates/home/'.$customs.'/'.$check.'/'.$gate); ?>" />
	<?php else: ?>
		<meta http-equiv="refresh" content="3;URL=<?php echo site_url('gates/guestnows/'.$customs.'/'.$check.'/'.$gate.'/'.$datauser['id']); ?>" />
	<?php endif; ?>
<?php elseif($places_id == 25 && ($datauser['Guest'] == 'No')): ?>
	<meta http-equiv="refresh" content="3;URL=<?php echo site_url('gates/home/'.$customs.'/'.$check.'/'.$gate); ?>" />
<?php else: ?>
<?php if($places_id == 20 && $datastatus == 1 && $check == 1): ?>
	<meta http-equiv="refresh" content="3;URL=<?php echo site_url('gates/guest/'.$customs.'/'.$check.'/'.$gate.'/'.$datauser['id']); ?>" />
	<?php elseif($places_id == 20 && $datastatus == 1 && $check == 2): ?>
	<meta http-equiv="refresh" content="3;URL=<?php echo site_url('gates/checkout/'.$customs.'/'.$check.'/'.$gate.'/'.$datauser['id']); ?>" />
	<?php else: ?>
	<meta http-equiv="refresh" content="3;URL=<?php echo site_url('gates/home/'.$customs.'/'.$check.'/'.$gate); ?>" />
	<?php endif; ?>
<?php endif;?>