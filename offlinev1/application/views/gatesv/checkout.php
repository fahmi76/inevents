<script type="text/javascript">
    $(document).ready(function () {
        $('#name').focus();
    });
</script>
<script type="text/javascript">
    $(function() {
        //update_content();
    });

    function update_content() {
        $.getJSON("<?php echo base_url(); ?>gates/curr_check/<?php echo $customs; ?>/<?php echo $gate; ?>", function(data) {
            $("p#results").empty();

            $.each(data.json, function() {
                $("p#results").append("\
                        \n\
                                Currently Checked In <b><span>: \n\
                                    " + this['total'] + "\
                                \n\
                        </span></b>");
            });
            setTimeout(function() {
                update_content();
            }, 5000);
        });
    }
</script>
<div class="text-center">
    <h2 class="tap-wristband"><p id="tap-code"><span>Checkout:</span></p></h2>
<hr>
<div>
    <button type="button" id="yes" class="btn btn-primary btn-block btn-xlg">Returning</button>
    <br >
    <button type="button" id="no" class="btn btn-primary btn-block btn-xlg">Not Returning</button>
    <br >
</div>
<hr>
</div> 

<script type="text/javascript">
    $(function() {
        $('#yes').click(function() {
            window.location = '<?php echo base_url(); ?>gates/checkoutchoose/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>/<?php echo $account_id; ?>/1'
                    });
        $('#no').click(function() {
            window.location = '<?php echo base_url(); ?>gates/checkoutchoose/<?php echo $customs; ?>/<?php echo $check; ?>/<?php echo $gate; ?>/<?php echo $account_id; ?>/2'
                    });
    });
</script>