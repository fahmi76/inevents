<h2 class="text-center">Welcome</h2>
<hr>
<div class="text-center">
    <h2><b><?php echo $datauser->account_displayname; ?></b></h2>
    <?php if (isset($datauser->account_avatar) && $datauser->account_avatar != ''): ?>
        <img src="<?php echo base_url(); ?>resizer/thumbimage.php?img=user/<?php echo $datauser->account_avatar; ?>" alt="" />
    <?php else: ?>
        <img src="<?php echo base_url(); ?>resizer/thumbimage.php?img=user/default.png" alt="" />
    <?php endif; ?>
    <h3><?php echo $datauser->account_avatar_type; ?></h3>
    <br>
    <hr>      
</div>

<meta http-equiv="refresh" content="2;URL=<?php echo site_url('firstcitizens/done/'.$datauser->id.'/'.$places_id); ?>" />