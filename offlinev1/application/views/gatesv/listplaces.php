<h2 class="text-center">List Station Check-in : </h2>
<hr>
<div class="text-center">
    <div class="table-responsive">
    <?php foreach($gate as $row): ?>
    	<h2 class="tap-wristband">
    		<p id="tap-code">
    			<span>
    				<a href="<?php echo base_url(); ?>gate/home/1/<?php echo $row->id?>">
    					<?php echo $row->nama; ?>
    				</a>
    			</span>
    		</p>
    	</h2>
    <?php endforeach; ?>
    </div>      
    <hr>
</div>
<h2 class="text-center">List Station Check-out : </h2>
<hr>
<div class="text-center">
    <div class="table-responsive">
    <?php foreach($gate as $row): ?>
    	<h2 class="tap-wristband">
    		<p id="tap-code">
    			<span>
    				<a href="<?php echo base_url(); ?>gate/home/2/<?php echo $row->id?>">
    					<?php echo $row->nama; ?>
    				</a>
    			</span>
    		</p>
    	</h2>
    <?php endforeach; ?>
    </div>      
</div>