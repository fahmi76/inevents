<div class="text-box">
    <h2 class="center">
        Choose Your Application
    </h2>
        <?php if($registrasi):?>
            <h2 class="center">
                Registration Link :
            </h2>
            <?php foreach($registrasi as $row): ?>
                <p class="center">
                    <a href="<?php echo base_url(); ?>landing/home?url=<?php echo $row['url']; ?>">
                        Registrasi <?php echo $row['name']; ?>
                    </a>
                </p>
            <?php endforeach;?>
			<?php if($private): ?>
				<h2 class="center">
					Pre-Registration Activation Link :
				</h2>
				<?php foreach($registrasi as $row): ?>
					<p class="center">
						<a href="<?php echo base_url(); ?>registrasi?url=<?php echo $row['url']; ?>">
							<?php echo $row['name']; ?>
						</a>
					</p>
				<?php endforeach;?>
			<?php endif; ?>
			<?php if($places_id == 454): ?>
				<h2 class="center">
					Pre-Registration Activation Link :
				</h2>
				<?php foreach($registrasi as $row): ?>
					<p class="center">
						<a href="<?php echo base_url(); ?>registrasi?url=<?php echo $row['url']; ?>">
							<?php echo $row['name']; ?>
						</a>
					</p>
				<?php endforeach;?>
			<?php endif; ?>
        <?php endif; ?>		
        <?php if($private): ?>
            <h2 class="center">
                Pre-Registration Link :
            </h2>
            <?php foreach($private as $row): ?>
                <p class="center">
                    <a href="<?php echo base_url(); ?>landing/home?url=<?php echo $row['url']; ?>">
                        <?php echo $row['name']; ?>
                    </a>
                </p>
            <?php endforeach;?>
        <?php endif; ?>
        <?php if($registrasi):?>
            <h2 class="center">
                Check RFID Link :
            </h2>
            <?php foreach($registrasi as $row): ?>
                <p class="center">
                    <a href="<?php echo base_url(); ?>cekrfid?url=<?php echo $row['url']; ?>">
                        Cek RFID <?php echo $row['name']; ?>
                    </a>
                </p>
            <?php endforeach;?>
        <?php endif; ?>
		<?php if ($places_id == 258 || $places_id == 454): ?>
            <h2 class="center">
                Polling link :
            </h2>
            <?php foreach($registrasi as $row): ?>
                <p class="center">
                    <a href="<?php echo base_url(); ?>fritzpolling?url=<?php echo $row['url']; ?>">
                        Polling
                    </a>
                </p>
            <?php endforeach;?>
            <h2 class="center">
                view polling link :
            </h2>
            <?php foreach($registrasi as $row): ?>
                <p class="center">
                    <a href="<?php echo base_url(); ?>fritzpolling/view?url=<?php echo $row['url']; ?>">
                        View Polling
                    </a>
                </p>
            <?php endforeach;?>
		<?php endif; ?>
		<?php if ($places_id == 429): ?>
            <h2 class="center">
                Link Tambahan Tresemme :
            </h2>
                <p class="center">
                    <a href="<?php echo base_url(); ?>tresemme/tresemme/ceknomer">
                        Cek Nomer Peserta
                    </a>
                </p>
            <h2 class="center">
                Link Print Sertifikat :
            </h2>
                <p class="center">
                    <a href="<?php echo base_url(); ?>tresemme/tresemme">
                        Ambil data peserta
                    </a>
                </p>
		<?php endif; ?>
        <?php if($photo): ?>  
            <h2 class="center">
                Photo Booth Link :
            </h2>
            <?php foreach($photo as $row): ?>
                <p class="center">
					<?php if ($places_id == 420): ?>
						<a href="<?php echo base_url(); ?>magnum/frame?place=<?php echo $row['url']; ?>">
					<?php elseif($places_id == 413): ?>
						<a href="<?php echo base_url(); ?>photoprint?place=<?php echo $row['url']; ?>">
					<?php else: ?>
						<a href="<?php echo base_url(); ?>photobooth?place=<?php echo $row['url']; ?>">
					<?php endif; ?>
                        Photobooth : <?php echo $row['name']; ?>
                    </a>
                </p>      
            <?php endforeach;?>
		    <h2 class="center">
                Photo Booth Setup Link :
            </h2>
            <?php foreach($photo as $row): ?>
                <p class="center">
					<?php if ($places_id == 420): ?>
						<a href="<?php echo base_url(); ?>magnum/frame/setup?place=<?php echo $row['url']; ?>">
					<?php elseif($places_id == 413): ?>
						<a href="<?php echo base_url(); ?>photoprint/setup?place=<?php echo $row['url']; ?>">
					<?php else: ?>
						<a href="<?php echo base_url(); ?>photobooth/setup?place=<?php echo $row['url']; ?>">
					<?php endif; ?>
                       Setup Photobooth <?php echo $row['name']; ?>
                    </a>
                </p>
            <?php endforeach;?>
        <?php endif; ?>
        <?php if($status): ?>
            <h2 class="center">
                Update Status Link :
            </h2>
            <?php foreach($status as $row): ?>
                <p class="center">
                    <a href="<?php echo base_url(); ?>updatestatus?place=<?php echo $row['nice']; ?>">
                        Update <?php echo $row['name']; ?>
                    </a>
                </p>
            <?php endforeach;?>
        <?php endif; ?>
</div>