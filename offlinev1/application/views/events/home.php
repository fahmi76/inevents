<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title>Wooz.in Events</title>
        <meta name="description" content="Wooz.in is a Social Network extension integrate to Radio-Frequency technology. Make the Location Base Social Network experience become more fun and easier. Combine the offline and online activity" />


        <script src="<?php echo $assets_url; ?>/javascripts/jquery-1.7.1.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?php echo $assets_url; ?>/javascripts/jquery-ui-1.8.9.custom.min.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" media="all" href="<?php echo $assets_url; ?>/customs/style.css"/>

        <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/themes/base/jquery.ui.theme.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo $assets_url; ?>/css/themes/base/jquery.ui.all.css" type="text/css" media="all" />        
        <style type="text/css">
<?php
if (isset($css) && $css != '') {
    echo $css;
} else {
    ?>
                body {
                    background: #000;
                    color: #fff;
                }
                a {
                    color:#656565; 
                }
                            
<?php } ?>
        </style>
    </head>

    <body>

        <div id="container">

            <div id="header">
                <div id="section-top">
                </div>
            </div>
            <div id="main-box">
                <?php echo $TContent; ?>
            </div>
            <div id="section-side">
            </div>
            <div id="footer">
                <a href="http://wooz.in" id="smalllogo">
                    <img src="<?php echo $assets_url; ?>/customs/smalllogo.png" alt="wooz.in" style="width: 60px; height: 60px;" />
                </a>
                <a href="<?php echo current_url(); ?>" class="home">Home</a> |
                <a href="http://wooz.in/about">About</a> |
                <a href="http://wooz.in/privacy">Privacy Policy</a> |
                <a href="http://wooz.in/termsofservice">Terms Of Service</a>
                <br />
                Wooz.in is a web based application using RFID.<br />
                &copy; 2012 Wooz.in (Patent Pending). All rights reserved.
            </div>
            <div class="clear"></div>
        </div>
    </body>
</html>
