<div class="text-box">
    <h2 class="center">
        Silahkan Pilih Lokasi Event
    </h2>
    <?php foreach ($data as $row): ?>
        <p class="center">
            <a class="fb-connect" href="<?php echo base_url('event/location/' . $row['id'].'/'. $row['url']); ?>">
                <?php echo $row['name']; ?>
            </a>
        </p>
    <?php endforeach; ?>
</div>