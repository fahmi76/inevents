<?php
	function fnc_extend ($file) {
	$get = explode('.', $file);
	$result = count($get);
	return $get[$result-1];
	}

$maxw = $_REQUEST['w'];
$maxh = $_REQUEST['h'];

$filename = $_REQUEST['img'];
$gambar = strtolower(fnc_extend($filename));

// Get new dimensions
list($w, $h) = getimagesize($filename);

$a = $maxw/$w;
$b = $maxh/$h;

if($a>$b) {
$newwidth = (int) ($w*$a);
$newheight = (int) ($h*$a);

	if($newwidth>$maxw) { $ndx = (int) (($newwidth - $maxw)/2); $dx = -$ndx; } else $dx = 0;
	if($newheight>$maxh) { $ndy = (int) (($newheight - $maxh)/2); $dy = -$ndy; } else $dy = 0;
} else {
$newwidth = (int) ($w*$b);
$newheight = (int) ($h*$b);

	if($newwidth>$maxw) { $ndx = (int) (($newwidth - $maxw)/2); $dx = -$ndx; } else $dx = 0;
	if($newheight>$maxh) { $ndy = (int) (($newheight - $maxh)/2); $dy = -$ndy; } else $dy = 0;
}

if ($gambar=='jpg'||$gambar=='jpeg') {

// Content type
header('Content-type: image/jpeg');

///*
$thumb = imagecreatetruecolor($maxw, $maxh);
$source = imagecreatefromjpeg($filename);

imagefill($thumb, 0, 0, $white);

// Resize
imagecopyresampled($thumb, $source, $dx, $dy, 0, 0, $newwidth, $newheight, $w, $h);

// Output
imagejpeg($thumb);

} else if ($gambar=='gif') {
// Content type
header('Content-type: image/gif');

///*
$thumb = imagecreatetruecolor($maxw, $maxh);
$source = imagecreatefromgif($filename);

imagefill($thumb, 0, 0, $white);

// Resize
imagecopyresampled($thumb, $source, $dx, $dy, 0, 0, $newwidth, $newheight, $w, $h);

// Output
imagegif($thumb);

} else if ($gambar=='png') {
// Content type
header('Content-type: image/png');

///*
$thumb = imagecreatetruecolor($maxw, $maxh);
$source = imagecreatefrompng($filename);

imagefill($thumb, 0, 0, $white);

// Resize
imagecopyresampled($thumb, $source, $dx, $dy, 0, 0, $newwidth, $newheight, $w, $h);

// Output
imagepng($thumb);

}

?> 
