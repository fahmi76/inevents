<?php
function fnc_extend ($file) {
    $get = explode('.', $file);
    $result = count($get);
    return $get[$result-1];
}

$maxw = 300;

$filename = '../'.$_REQUEST['img'];
$gambar = strtolower(fnc_extend($filename));

// Get new dimensions
list($w, $h) = getimagesize($filename);
if($w >= $maxw) {
    $a = $maxw/$w;
    $newwidth = (int) ($w*$a);
    $newheight = (int) ($h*$a);
} else {
    $newwidth = (int) ($w);
    $newheight = (int) ($h);
}

if ($gambar=='jpg'||$gambar=='jpeg') {

// Content type
    header('Content-type: image/jpeg');

///*
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    $source = imagecreatefromjpeg($filename);
    $white = imagecolorallocate($thumb, 255, 255, 255);

    imagefill($thumb, 0, 0, $white);

// Resize
    imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $w, $h);

// Output
    imagejpeg($thumb);

} else if ($gambar=='gif') {
// Content type
    header('Content-type: image/gif');

///*
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    $source = imagecreatefromgif($filename);
    $white = imagecolorallocate($thumb, 255, 255, 255);

    imagefill($thumb, 0, 0, $white);

// Resize
    imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $w, $h);

// Output
    imagegif($thumb);

} else if ($gambar=='png') {
// Content type
    header('Content-type: image/png');

///*
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    $source = imagecreatefrompng($filename);
    $white = imagecolorallocate($thumb, 255, 255, 255);

    imagefill($thumb, 0, 0, $white);
    
// Resize
    imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $w, $h);

// Output
    imagepng($thumb);

}

?> 
