<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->tpl['TContent'] = null;
        $this->tpl['core'] = 'home';
        $this->tpl['main_title'] = 'Wooz.in - ';
        $this->tpl['assets_url'] = $this->config->item('assets_url');
        $this->tpl['uploads_url'] = $this->config->item('uploads_url');

		$this->load->library('curl');
		$this->load->helper('url');
        $this->load->database();
        $this->load->model('landing_m');
        date_default_timezone_set('Asia/Jakarta');
        $custom_page = 'Sollam-Store';
        if ($custom_page != '') {
            $customs = $this->landing_m->getplaceslanding($custom_page);
            if ($customs) {
                $this->custom_id = $customs->id;
                $this->custom_url = $customs->places_landing;
                $this->tpl['spot_id'] = $customs->id;
                $this->custom_model = $customs->places_model;
                $this->tpl['custom_model'] = $this->custom_model;
                $this->tpl['customs'] = $customs->places_landing;
                $this->tpl['background'] = $customs->places_backgroud;
                $this->tpl['logo'] = $customs->places_logo;
                $this->tpl['main_title'] = $customs->places_name;
                $this->tpl['type_registration'] = $customs->places_type_registration;
                $this->tpl['css'] = $customs->places_css;
                //$regis_type = '["1","2","3","4"]';
                $regis_type = '["4"]';
                $this->tpl['regis_type'] = json_decode($regis_type);
                $this->tpl['lang_regis'] = 2;

                if ($customs->places_fbid != '0') {
                	$this->fb_page = $customs->places_fb_page_id;
                    $this->tpl['fb_page'] = $customs->places_fbid;
                }
                if ($customs->places_tw_id != '0') {
                    $this->twitter_name = $customs->places_twitter;	
                    $this->tpl['tw_page'] = $customs->places_twitter;
                    $this->twiiter_id_fan = $customs->places_tw_id;
                }
            } else {
                redirect('?status=custom_page_end_from_showing');
            }
        } else {
            redirect('?status=custom_page_not_found');
        }
	}

	public function index()
	{
        $this->load->library('facebook');
        header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->tpl['page'] = "home";
        $this->tpl['title'] = "Home";
		if ( ! $this->facebook->logged_in()){
			$this->tpl['login'] = $this->facebook->login_url();
		}else{
			$this->tpl['login'] = $this->facebook->logout_url();
		}
        $this->tpl['TContent'] = $this->load->view('landingnews/default', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);
		//$this->load->view('examples/start');
	}

	public function web_login()
	{
        $this->load->library('facebook');
		if ($this->facebook->logged_in())
		{
			$user = $this->facebook->user();
			if ($user['code'] === 200)
			{
	            $data['account_token'] = $this->session->userdata('fb_token');
	            $data['account_fbid'] = $user['data']['id'];
	            $data['account_fb_name'] = $user['data']['name'];
				unset($user['data']['permissions']);
	            $sql = "SELECT id FROM wooz_account_sollam
	                    WHERE account_fbid = '" . $user['data']['id'] . "'";
	            $query = $this->db->query($sql);
	            $dataquery = $query->row();
	            if ($dataquery) {
	                $this->db->where('id', $dataquery->id);
	                $this->db->update('account_sollam', $data);
	                $new = $dataquery->id;
	            } else {
	                $this->db->insert('account_sollam', $data);
	                $new = $this->db->insert_id();
	            }
	            redirect('landing/step2/fb/'.$new, redirect);
			}

		}else{
			redirect($this->facebook->logout_url());
		}
	}

	public function step2($from,$acc_id){
		$this->tpl['from'] = $from;
        $this->tpl['page'] = "step2";
        $this->tpl['title'] = "Personal Information";
        $this->tpl['id'] = $acc_id;
        $this->tpl['likefb'] = 0;
        $this->tpl['followtw'] = 0;
        $this->tpl['logoutfb'] = 0;
        $user = $this->db->get_where('account_sollam', array('id' => $acc_id))->row();

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fullname', 'Name', 'trim|required');
        $this->form_validation->set_rules('age', 'Age', 'trim|required');
        $this->form_validation->set_rules('work', 'Work', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('phone', 'Contact Number', 'trim|required');
        $this->form_validation->set_rules('interested[]', 'Interested', 'trim|required');
        $this->form_validation->set_rules('experience', 'Experience', 'trim|required');
        $this->form_validation->set_rules('recomended', 'Recomended', 'trim|required');
        $this->form_validation->set_rules('comment', 'Comment', 'trim');

        if ($this->form_validation->run() === TRUE) {

            $data['account_displayname'] = $this->input->post('fullname', true);
            $data['account_username'] = $this->landing_m->nicename($this->input->post('fullname', true));
            $data['account_email'] = $this->input->post('email', true);
            $data['account_phone'] = $this->input->post('phone', true);
            $data['account_age'] = $this->input->post('age', true);
            $data['account_work'] = $this->input->post('work', true);
            $data['account_interest'] = json_encode($this->input->post('interested', true));
            $data['account_experience'] = $this->input->post('experience', true);
            $data['account_recomended'] = $this->input->post('recomended', true);
            $data['account_comment'] = $this->input->post('comment', true);
            $data['account_status'] = 1;
            
            if ($acc_id == 0) {
                $this->db->insert('account_sollam', $data);
                $acc_id = $this->db->insert_id();
            } else {
                $this->db->where('id', $acc_id);
                $this->db->update('account_sollam', $data);
            }
            //email
            //$this->landing_m->landingdata($acc_id, $this->custom_id, $data['account_rfid']);
            redirect('landing/step3/' . $from .'/'.$acc_id);
                
            #redirect('landingnew/steprfid?url=' . $this->custom_url . '&places=' . $this->custom_id . '&acc=' . $acc . '&from=' . $from . '&likefollow=' . $likefollow);
        }
        if ($from == 'fb') {
            if(isset($this->fb_page)){
                $this->load->library('facebook');
                $userlike = $this->facebook->pagelike($user->account_fbid,$this->fb_page);
                $this->tpl['likefollow'] = 0;
                if ($userlike['code'] === 200)
                {
                    $this->tpl['likefollow'] = 1;
                }
            }
            //$this->tpl['likefollow'] = $this->facebooks_v4->get_like_fb($user->account_fb_token,$user->account_fb_id,$places->places_fb_page_id);
        }
        // if ($from == 'tw') {
        //     $this->tpl['likefollow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_tw_id);
        // }
        $this->tpl['info'] = $user;
        $this->tpl['TContent'] = $this->load->view('landingnews/signupnext', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);
	}

	public function step3($from,$acc_id){
        $data_user = $this->landing_m->gettable('id', $acc_id, 'account_sollam');
        $this->tpl['page'] = "step3";
        if($from == 'fb'){
            $this->tpl['title'] = "Go With Twitter?";            
        }else{
            $this->tpl['title'] = "Go With Facebook?";
        }
        if($from == 'tw'){
            $this->load->library('facebookv1');
            if ( ! $this->facebookv1->logged_in($acc_id)){
                $this->tpl['login'] = $this->facebookv1->login_url($acc_id);
            }else{
                $this->tpl['login'] = $this->facebookv1->logout_url($acc_id);
            }
        }
        $this->tpl['from'] = $from;

        $this->tpl['id'] = $acc_id;
        $this->tpl['user_name'] = $data_user->account_displayname;
        $this->tpl['TContent'] = $this->load->view('landingnews/signupskip', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);

	}

    function token($id) {
        $appid = $this->config->item('facebook_app_id');
        $secretid = $this->config->item('facebook_app_secret');
        $token = $this->input->post('token');

        $extend_url = "https://graph.facebook.com/oauth/access_token?client_id=".$appid."&client_secret=".$secretid."&grant_type=fb_exchange_token&fb_exchange_token=".$token;
        $resp = file_get_contents($extend_url);
        parse_str($resp,$output);

        $now = strtotime("now");
        $ext = $now + $output['expires'];

        #$data['account_fb_reg'] = $output['expires'];
        $data['account_fb_expired'] = date("Y-m-d", $ext);
        $data['account_token'] = $output['access_token'];
        $data['account_fbid'] = $this->input->post('fbid');
        $data['account_fb_name'] = $this->input->post('name');
        #xdebug($data);die;
        $this->db->where('id', $id);
        $this->db->update('account_sollam', $data);
        echo $id;
    }

    function fblogouts($accid = 0){
        $url = array('url' => base_url() . 'fbtoken/token/'.$accid);
        $this->load->library('facebook_v4/facebooks_v4', $url);
        $url_logout = site_url('landing/step3/tw/'.$accid);
        $sql = "SELECT account_fbid,account_token FROM wooz_account_sollam
            WHERE id = '".$accid."'";
        $query = $this->db->query($sql);
        $dataquery = $query->row();
        if($dataquery->account_fbid != '' && $dataquery->account_token != ''){
            $logouts = $this->facebooks_v4->get_logout_tokens($dataquery->account_token,$url_logout);
        }else{
            $logouts = 'landing/step3/tw/'.$accid;
        }
        redirect($logouts);
    }

	public function step4($from,$acc){
        redirect('landing/step5/'.$from.'/'.$acc);
        $user = $this->landing_m->gettable('id', $acc, 'account_sollam');

        $this->tpl['page'] = "step4";
        $this->tpl['title'] = "Get More info?";
        $this->tpl['id'] = $acc;
        $this->tpl['from'] = $from;
        $this->tpl['followerror'] = $this->input->get('follow', true);
        $this->tpl['user_name'] = $user->account_displayname;
        if ($from == 'fb') {
            $this->load->library('sosmedtw');
            if(isset($this->twitter_name)){
            $this->tpl['likefollow2'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $this->twitter_name);
            }
        } else {
            if(isset($this->fb_page)){
            $url = array('url' => base_url() . 'fbtoken/token/'.$acc);
            $this->load->library('facebook_v4/facebooks_v4', $url);
            $this->tpl['likefollow2'] = $this->facebooks_v4->get_like_fb($user->account_token,$user->account_fbid,$this->fb_page);

            }
        }
        $this->tpl['TContent'] = $this->load->view('landingnews/signupskip2', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);
	}

    public function step5($from,$acc){
        $data_user = $this->landing_m->gettable('id', $acc, 'account_sollam');
        $this->tpl['page'] = "step5";
        $this->tpl['title'] = "Share status?";
        $this->tpl['from'] = $from;
        $this->tpl['places'] = $this->landing_m->gettable('id', $this->custom_id, 'places');
        $this->tpl['id'] = $acc;
        $this->tpl['data'] = $data_user;
        $this->tpl['TContent'] = $this->load->view('landingnews/signupshare', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);
    }

    public function stepshare($from,$acc,$share) {
        $user = $this->db->get_where('account_sollam', array('id' => $acc))->row();
        
        $places = $this->db->get_where('places', array('id' => $this->custom_id))->row();
        
        if ($user->account_fbid && $user->account_token) {

            $url = array('url' => base_url() . 'landing/stepshare/'.$from.'/'.$acc.'/'.$share);
            $this->load->library('facebook_v4/facebooks_v4', $url);
            $landing['landing_fb_like'] = $this->facebooks_v4->get_like_fb($user->account_token, $user->account_fbid, $places->places_fb_page_id);
            $landing['landing_fb_friends'] = $this->facebooks_v4->get_friend_fb($user->account_token, $user->account_fbid);
            if($share == 1){
                $text = $places->places_cstatus_fb;
                $image = FCPATH . "assets/image.jpg";
                $landing['landing_fb_status'] = $this->facebooks_v4->post_photo($places, $image, $user->account_fb_name, $user->account_fbid, $user->account_token, $text);
                
            }
        }
        if ($user->account_tw_token && $user->account_tw_secret) {
            $this->load->library('sosmedtw');
            $landing['landing_tw_follow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_tw_id);
            $landing['landing_tw_friends'] = $this->sosmedtw->friend_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username);
            $landing['landing_tw_follow'] = $this->sosmedtw->follow_tw($user->account_tw_token, $user->account_tw_secret, $user->account_tw_username, $places->places_tw_id);
            if($share == 1){
                $texttw = $places->places_cstatus_tw;
                $image = FCPATH . "assets/image.jpg";
                $landing['landing_tw_status'] = $this->sosmedtw->upload_photo_tw($texttw, $image, $user->account_tw_token, $user->account_tw_secret);

            }
        }
        if($user->account_email != ""){
            $landing['landing_email'] = $this->mandrill($user->account_email,$user->account_displayname,$places->places_subject_email,$places->places_custom_email,"");
        }
        $landing['landing_rfid'] = 0;
        $landing['landing_level'] = $user->account_group;
        $landing['landing_rfid'] = $user->account_rfid;
        $landing['landing_from_regis'] = $from;
        $landing['account_id'] = $acc;
        $landing['landing_register_form'] = $this->custom_id;
        
        $this->db->insert('landing_sollam', $landing);
        redirect(site_url('landing/step6/'.$from.'/'.$acc));
    }

    public function step6($from,$acc){
        $status_form = $this->input->get('status', true);
        $this->tpl['from'] = $from;
        $this->tpl['page'] = "step2";
        $this->tpl['title'] = "Personal Information";
        $this->tpl['id'] = $acc;
        $this->tpl['likefb'] = 0;
        $this->tpl['followtw'] = 0;
        $user = $this->db->get_where('account_sollam', array('id' => $acc))->row();
        $this->tpl['logoutfb'] = 0;
        $this->load->library('facebook');
        if ($this->facebook->logged_in()){
            $this->tpl['logoutfb'] = 1;
            $this->tpl['logout'] = $this->facebook->logout_url();
        }
        $this->tpl['page'] = "thankyou";
        $this->tpl['title'] = "Thankyou!";
        $this->tpl['TContent'] = $this->load->view('landingnews/signupdone', $this->tpl, true);
        $this->load->view('landingnews/home', $this->tpl);

    }

    function logouts() {
        $this->session->unset_userdata('aidi');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('session_id');
        $this->session->unset_userdata('ip_address');
        $this->session->unset_userdata('user_agent');
        $this->session->unset_userdata('last_activity');
        $this->session->unset_userdata('nice');
        $this->session->unset_userdata('grup');
        $this->session->unset_userdata('access');
        $this->session->unset_userdata('kode');

        #$this->facebook->destroySession();
        $this->_killFacebookCookies();
        $this->session->sess_destroy();
        session_destroy();
        unset($_SESSION);
        $past = time() - 3600;
        foreach ($_COOKIE as $key => $value) {
            setcookie($key, $value, $past, '/');
        }
        redirect('landing');
    }

    public function _killFacebookCookies() {
        // get your api key 
        $apiKey = $this->config->item('facebook_app_id');
        // get name of the cookie 

        $cookies = array('user', 'session_key', 'expires', 'ss');
        foreach ($cookies as $name) {
            setcookie($apiKey . '_' . $name, false, time() - 3600);
            unset($_COOKIE[$apiKey . '_' . $name]);
        }

        setcookie($apiKey, false, time() - 3600);
        unset($_COOKIE[$apiKey]);
    }


    function mandrill($email_user,$nama,$subject,$content,$attachment){
        $hasil_email = 0;
        $this->load->config('mandrill');
        $this->load->library('mandrill');
        $mandrill_ready = NULL;
        
        try {
            $this->mandrill->init( $this->config->item('mandrill_api_key') );
            $mandrill_ready = TRUE;
        } catch(Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        
        if( $mandrill_ready ) {
            if($attachment){
                $image = FCPATH.$attachment;
                $attch = $this->mandrill->getAttachmentStruct($image);    
            }else{
                $attch = '';
            }
            $content = "<p>We at Sollam’s Leather Store would like to sincerely thank you for visiting us today and participating in our store’s opening. We truly appreciate your feedback and hope that you enjoyed the experience at Sollam.</p>

 
<p>
Our aim is to provide the finest quality leather products with unique designs to match your individual flair and personality.  Based on your interests, we will keep you up to date with items that we think may excite you, and we do look forward to seeing you back in store with us soon.
</p>
 
<p>
Thanks again from us here at Sollam.
</p>
 
<p>
Warm Regards,</p>

The Sollam Team";
            //Send us some email!
            $email = array(
                'html' => "Dear ".$nama.",<br>".$content, //Consider using a view file
                'subject' => $nama." ".$subject,
                'from_email' => 'donotreply@wooz.in',
                'from_name' => 'Sollam',
                'to' => array(array('email' => $email_user )) , //Check documentation for more details on this one
                "attachments" => array( $attch  )
            );
            $result = $this->mandrill->messages_send($email);
            $hasil_email = $result[0]['_id'];
        }
        return $hasil_email;
    }

	public function js_login()
	{
		$this->load->view('examples/js');
	}

	public function post()
	{

		header('Content-Type: application/json');

		$result = $this->facebook->publish_text($this->input->post('message'));
		echo json_encode($result);

	}

	public function logout()
	{
        $this->load->library('facebook');
		$this->facebook->destroy_session();
		redirect('landing', redirect);
	}

    function email_used($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('email');
        }
        $new = $this->db->get_where('account', array('account_email' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('account', array('account_email' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('email_used', 'Email Telah Terdaftar, Silahkan ganti Email');
                return false;
            }
        } else {
            return true;
        }
    }

    function telp_used($str, $account_id) {
        if (!isset($str)) {
            $str = $this->input->post('telp');
        }
        $valid = preg_match('/^([0-9]{5,14})$/', $str);
        if (!$valid) {
            $this->form_validation->set_message('telp_used', 'Format Nomer Telepon tidak sesuai, Silahkan ganti Nomer Telepon');
            return false;
        }
        $new = $this->db->get_where('account', array('account_phone' => $str))->row();
        if (count($new) >= 1) {
            $news = $this->db->get_where('account', array('account_phone' => $str, 'id' => $account_id))->row();
            if ($news) {
                return true;
            } else {
                $this->form_validation->set_message('telp_used', 'Nomer Telepon Telah Terdaftar, Silahkan ganti Nomer Telepon');
                return false;
            }
        } else {
            return true;
        }
    }
    function year_chk($str) {
        $yearnow = date('Y');
        $syarat = $yearnow - 13;
        $syarat1 = $yearnow - 83;
        if (($str >= $syarat1) && ($str <= $syarat)) {
            return true;
        } else {
            $this->form_validation->set_message('year_chk', 'Tahun kelahiran tidak memenuhi syarat');
            return false;
        }
    }

    function cek_data_dropdown_bulan($str) {
        if ($str != 0) {
            return true;
        } else {
            $this->form_validation->set_message('cek_data_dropdown_bulan', 'Bagian %s Mohon diisi');
            return false;
        }
    }
    function cek_data_tanggal($str) {
        $day = $this->input->post('tgl');
        $month = $this->input->post('bln');
        $year = $this->input->post('thn');

        if (checkdate($month, $day, $year)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('cek_data_tanggal', 'Masukkan tanggal lahir yang benar.');
            return FALSE;
        }
    }


    function twitter(){
        include(APPPATH . 'third_party/tmhOAuth.php');
        include(APPPATH . 'third_party/tmhUtilities.php');
        $this->load->model('home_m');

        $this->tpl['page'] = "home/twitter";
        $this->tpl['title'] = "Twitter Connect";
        //print_r($this->uri);die;
        $goto = $this->uri->segment(3, 0);
        $accid = $this->uri->segment(4, 0);
        $places = $this->uri->segment(5, 0);
        $from = $this->uri->segment(6, 0);
        // print_r($goto);
        // print_r($accid);
        // print_r($places);
        // print_r($from);die;
        $tmhOAuth = new tmhOAuth(array(
            'consumer_key' => $this->config->item('twiiter_consumer_key'),
            'consumer_secret' => $this->config->item('twiiter_consumer_secret'),
        ));

        $twitcancel = $this->input->get('denied', true);
        if ($twitcancel) {
            redirect('landing/'.$goto .'/'.$from.'/'.$accid);
        }

        if (isset($_REQUEST['oauth_verifier'])) {
            /* oauth_verifier */
            $tmhOAuth->config['user_token'] = $_SESSION['oauth']['oauth_token'];
            $tmhOAuth->config['user_secret'] = $_SESSION['oauth']['oauth_token_secret'];

            $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/access_token', ''), array(
                'oauth_verifier' => $_REQUEST['oauth_verifier']
            ));

            if ($code == 200) {
                /* oauth verifier success */
                unset($_SESSION['oauth']);
                $access_token = $tmhOAuth->extract_params($tmhOAuth->response['response']);
                if (is_array($access_token) && count($access_token) != '0') {
                    /* update user token at database */
                    $db['account_tw_token'] = $access_token['oauth_token'];
                    $db['account_tw_secret'] = $access_token['oauth_token_secret'];
                    $db['account_tw_username'] = $access_token['screen_name'];
                    /* get additional user data */
                    $tmhOAuth->config['user_token'] = $access_token['oauth_token'];
                    $tmhOAuth->config['user_secret'] = $access_token['oauth_token_secret'];
                    $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/account/verify_credentials'));

                    if ($code == 200) {
                        /* get data success */
                        $response = json_decode($tmhOAuth->response['response']);
                        $crd['account_tw_username'] = $access_token['screen_name'];
                        $crd['account_twid'] = $access_token['user_id'];
                        $crd['account_tw_token'] = $access_token['oauth_token'];
                        $crd['account_tw_secret'] = $access_token['oauth_token_secret'];

                        $row = $this->home_m->gettwitter($crd['account_twid'], $accid);

                        if ($row) {
                            if ($row->account_displayname == '') {
                                $crd['account_displayname'] = $response->name;
                            }
                        } else {
                            $crd['account_twid'] = $response->id;
                        }

                        $os = array("landing","landingnew","landingnewtw2", "login", "landingtw2", "3", "index");
                        if (in_array($goto, $os)) {
                            if ($row) {
                                $this->home_m->update($row->id, $crd);
                                $accid = $row->id;
                            } else {
                                if ($accid != '0') {
                                    $this->home_m->update($accid, $crd);
                                } else {
                                    $crd['account_displayname'] = $response->name;
                                    $this->db->insert('account_sollam', $crd);
                                    $accid = $this->db->insert_id();
                                }
                            }
                        } else {
                            $this->home_m->update($accid, $crd);
                        }

                            if($goto == 'index'){
                            redirect('landing/step2/'.$from.'/'.$accid);
                            }
                            redirect('landing/step4/'.$from.'/'.$accid);
                    } else {
            			redirect('landing/'.$goto .'/'.$from.'/'.$accid.'?retry=true&status=oauth_verifier_error');
                    }
                }else{
                    redirect('landing/'.$goto .'/'.$from.'/'.$accid.'?retry=true&status=oauth_verifier_error');
                }
            } else {
                /* oauth verifier failed */
                redirect('landing/'.$goto .'/'.$from.'/'.$accid.'?retry=true&status=oauth_verifier_error');
            }
        } else {
            /* twitter connect start */   
            $callback = site_url('landing/twitter/'.$goto .'/'.$accid.'/'.$places.'/'.$from);
          
            $params = array('oauth_callback' => $callback);
            $code = $tmhOAuth->request('POST', $tmhOAuth->url('oauth/request_token', ''), $params);
            if ($code == 200) {
                /* token request available */
                $_SESSION['oauth'] = $tmhOAuth->extract_params($tmhOAuth->response['response']);
                $url = $tmhOAuth->url('oauth/authorize', '') . "?oauth_token={$_SESSION['oauth']['oauth_token']}&force_login=1";
                redirect($url);
            } else {
                /* token request not available */
                redirect('landing/'.$goto .'/'.$from.'/'.$accid.'?retry=true&status=twitter_start_error');
            }
        }
    }
    
    function follow() {
        $this->load->library('sosmedtw');
        $userdata = $this->input->post('id');
        $data_user = $this->landing_m->gettable('id', $userdata, 'account');

        if ($data_user->account_tw_token != '' && $data_user->account_tw_secret != '') {
            $follow = $this->sosmedtw->post_follow_tw($data_user->account_tw_token, $data_user->account_tw_secret, $this->twiiter_id_fan);

            if ($follow == 1) {
                $data1['nama'] = "yes";
            } else {
                $data1['nama'] = "no";
            }
            echo json_encode($data1);
        }
    }
}
