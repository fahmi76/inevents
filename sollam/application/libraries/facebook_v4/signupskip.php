<h2>Hi, <?php echo $user_name; ?></h2>
<hr>
<div class="text-center">
    <?php if ($from == 'fb'): ?>
        <p>Also share your activity through your Twitter account?</p>
        <a class="tw-connect" href="<?php echo base_url(); ?>home/twit/landingnewtw2/<?php echo $customs ?>/<?php echo $id ?>/<?php echo $spot_id; ?>/<?php echo $likefollow; ?>">
            <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
    <?php elseif ($from == 'tw'): ?>
		<a class="fb-connect" href="#" onclick="logout();">
	            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>
		</a>
	<?php else: ?>
        <p>Also share your activity through your Twitter account?</p>
        <a class="tw-connect" href="<?php echo base_url(); ?>home/twit/landingnewtw2/<?php echo $customs ?>/<?php echo $id ?>/<?php echo $spot_id; ?>/<?php echo $likefollow; ?>">
            <button type="button" class="tw-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
		<p>or also share your activity through your Facebook account?</p>	
        <!--<a class="fb-connect" href="<?php echo $login; ?>"> -->
        <a class="fb-connect" href="#" onclick="logout();">
            <button type="button" class="fb-button"><img class="img-responsive" src="<?php echo $assets_url; ?>/landing/images/social-transparent-square.gif"></button>   
        </a>
    <?php endif; ?>   
    <p>or finish right here.</p> 
	
    <a class="reg-done" href="<?php echo site_url('landingnew/step5?url=' . $url . '&places=' . $spot_id . '&from=' . $from . '&acc=' . $id . '&likefollow='.$likefollow); ?>">
        <button type="button" class="btn btn-primary btn-xlg">Done</button>
    </a>
</div>


<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?=$fanspage_id;?>',
            xfbml: true,
            cookie: true,
            version: 'v2.6'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript"> 
    function login(){
        FB.login(function (response) {
            getloginstatus(response);
        }, {scope: 'user_friends,public_profile,email,user_likes,user_photos,publish_actions'}); //'public_profile,user_photos,email,user_birthday,user_hometown,publish_actions,user_likes'
    }
    function getloginstatus(response) {
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                //console.log(response.authResponse.accessToken);
                getfbid(response, response.authResponse.accessToken);
            }
        });
    }

    function getfbid(response, token) {
        FB.api('/me', function (response) {
            //console.log(response);
            postloginfb(response, token);
        });
    }

    function postloginfb(response, token) {
        console.log(response);
        //var postData = {'fbid': response.id, 'name': response.name, 'email': response.email, 'link': response.link, 'hometown': response.hometown.name, 'gender': response:gender};
         var postData = {'fbid': response.id, 'name': response.name, 'email': response.email, 'link': response.link, 'token': token};
         $.post("<?=site_url('landingnew/step5?url='.$event_id.'&acc='.$acc.'&from='.$from.'&places='.$spot_id.'&likefollow=1')?>", postData, function (data) {
            if(data != ""){
                top.location.href = "<?=site_url('landingnew/step5?url='.$event_id.'&acc='.$acc.'&from='.$from.'&places='.$spot_id.'&likefollow=1')?>"; 
                //$('#facebook').css('display', 'block');
                //$('#facebook-name').html(data);
                //$('#next-button').html('<input  type="button" name="next" id="next" value="Next" onclick="next();">');
            }
        });
    }

    function logout(){
        FB.getLoginStatus(function(response) {
            if (response && response.status === 'connected') {
                FB.logout(function(response) {
                    $.ajax({url: "<?=site_url('landingnew/clear_sess?url='.$event_id.'&acc='.$acc.'&from='.$from.'&places='.$spot_id.'&likefollow=1')?>", success: function(result){
                       //document.location.reload();
                       login();
                    }});
                });
            }else{
                login();
            }
        });
    } 
</script>  