<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once( APPPATH . 'libraries/facebook_v4/Facebook/GraphObject.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/GraphSessionInfo.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookSession.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookCurl.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookHttpable.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookCurlHttpClient.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookResponse.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookSDKException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRequestException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookAuthorizationException.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRequest.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookRedirectLoginHelper.php' );
require_once( APPPATH . 'libraries/facebook_v4/Facebook/FacebookServerException.php' );

use Facebook\GraphSessionInfo;
use Facebook\FacebookSession;
use Facebook\FacebookCurl;
use Facebook\FacebookHttpClient;
use Facebook\FacebookCurlHttpClient;
use Facebook\FacebookResponse;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphObject;
use Facebook\FacebookServerException;

class Facebooks_v4 {

    var $ci;
    var $helper;
    var $session;

    public function __construct($url) {
        $this->ci = & get_instance();
        FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook'));
        $this->helper = new FacebookRedirectLoginHelper($url['url']);

        if ($this->ci->session->userdata('fb_token')) {
            $this->session = new FacebookSession($this->ci->session->userdata('fb_token'));

            // Validate the access_token to make sure it's still valid
            try {
                if (!$this->session->validate()) {
                    $this->session = false;
                }
            } catch (Exception $e) {
                // Catch any exceptions
                #echo '1<br />';
                #print_r($e);
                $this->session = false;
            }
        } else {
            try {
                $this->session = $this->helper->getSessionFromRedirect();
            } catch (FacebookRequestException $ex) {
                #echo '2<br />';
                #print_r($ex);
                // When Facebook returns an error
            } catch (\Exception $ex) {
                #echo '3<br />';
                #print_r($ex);
                // When validation fails or other local issues
            }
        }

        if ($this->session) {
            $this->ci->session->set_userdata('fb_token', $this->session->getToken());

            $this->session = new FacebookSession($this->session->getToken());
        }
    }

    public function get_logout_tokens($token, $url) {
        if ($token) {
            return $this->helper->getLogoutUrl_token($token, $url);
        }
        return false;
    }

    public function get_like_fb($token, $fbid, $page_id) {
        $get_like_fb = 0;
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $request = (new FacebookRequest($session, 'GET', '/' . $fbid . '/likes/' . $page_id))->execute();
                $user = $request->getGraphObject()->asArray();
                if ($user) {
                    $get_like_fb = 1;
                }else{
                    $get_like_fb = 0;
                }
            } catch (FacebookRequestException $e) {
                $get_like_fb = 0;
            }
        } else {
            $get_like_fb = 0;
        }
        return $get_like_fb;
    }

    public function get_friend_fb($token, $fbid) {
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $request = (new FacebookRequest($session, 'GET', '/' . $fbid . '/friends'))->execute();
                $user = $request->getGraphObject()->asArray();
                $get_friend_fb = 0;
                if ($user) {
                    if ($user['summary']) {
                        $get_friend_fb = $user['summary']->total_count;
                    }
                }
            } catch (FacebookRequestException $e) {
                $get_friend_fb = 0;
            }
        } else {
            $get_friend_fb = 0;
        }
        return $get_friend_fb;
    }

    public function update_status($param, $fbid, $token) {
        try {
            $session = new FacebookSession($token);
			
			$feed = array(
				'name' => $param['name'],
				'caption' => $param['caption'],
				'link' => $param['link'],
				//'message' => $param['message'],
				'place' => $param['places']			
			);
			
            $response = (new FacebookRequest(
                    $session, 'POST', '/' . $fbid . '/feed', $feed))->execute()->getGraphObject()->asArray();
            $response_id = $response['id'];
        } catch (FacebookRequestException $e) {
            $response_id = 0;
        }
        return $response_id;
    }
	
    public function update_status_test($param, $fbid, $token) {
        try {
            $session = new FacebookSession($token);
            $response = (new FacebookRequest(
                    $session, 'POST', '/' . $fbid . '/feed', array(
                'name' => $param['name'],
                'caption' => $param['caption'],
                'link' => $param['link'],
                'place' => $param['places']     
                //'image' => $param['image'],
                //'message' => $param['message']
                    )
                    ))->execute()->getGraphObject()->asArray();
            $response_id = $response['id'];
        } catch (FacebookRequestException $e) {
            xdebug($e);
            $response_id = 0;
        }
        return $response_id;
    }

	public function user_likes($fbid, $token){
        $get_like_fb = array();
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $request = (new FacebookRequest($session, 'GET', '/' . $fbid . '/likes'))->execute();
                $user = $request->getGraphObject()->asArray();
                if ($user) {
                    $get_like_fb = $user['data'];
                }else{
                    $get_like_fb = array();
                }
            } catch (FacebookRequestException $e) {
                $get_like_fb = array();
            }
        } else {
            $get_like_fb = array();
        }
        return $get_like_fb;	
	}
	
	public function user_interests($fbid, $token){
        $get_like_fb = array();
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $request = (new FacebookRequest($session, 'GET', '/' . $fbid . '/interests'))->execute();
                $user = $request->getGraphObject()->asArray();
                if ($user) {
                    $get_like_fb = $user['data'];
                }else{
                    $get_like_fb = array();
                }
            } catch (FacebookRequestException $e) {
                $get_like_fb = array();
            }
        } else {
            $get_like_fb = array();
        }
        return $get_like_fb;	
	}

    public function destroy() {
        $this->ci->session->set_userdata('fb_token', '');
        $this->session = false;
        $session = new FacebookSession($this->session);
        return $session;
    }

    function get_album($venue, $image, $fb_name, $fbid, $token){        
        $album_id = '';
			#xdebug($token);die;
        #$token = 'CAACGFg9ewNIBABeQgJfhf5GsM0ZCpZAITZCjF8P8ZC93XIivZBTQiyLYIm6RQuyQHNwn9JamVAocOrTVg4oHYuZBsa4B0ZA0wZBHfnj5ZAX2ZBu0juPSao8gWN6zjRKjgcFAoDz07vXd4iq2VWZAxlvo32wzZBYRw6TBvzpiCxeOffwnQ46dQkzt6juMVsLEqHZCnE3yjtcE1n4X8l7ZAoMtr776zW';
        if ($token) {
            try {
                $session = new FacebookSession($token);
                $request = (new FacebookRequest($session, 'GET', '/' . $fbid . '/albums'))->execute();
                $user = $request->getGraphObject()->asArray();
                if ($user) {
                    foreach ($user['data'] as $row) {
                        if($row->name == $venue->places_album_name){
                            $album_id = $row->id;
                            break; 
                        }
                    }
                }else{
                    $album_id = '';
                }
            } catch (FacebookRequestException $e) {
                $album_id = '';
            }
        } else {
            $album_id = '';
        }
        if($album_id == ''){
            try {
                $session = new FacebookSession($token);
                $response = (new FacebookRequest(
                        $session, 'POST', '/' . $fbid . '/albums', array(
                            'name' => $venue->places_album_name,
                            'message' => $venue->places_desc
                            //'message' => 'Nation Building adalah soft skills yang diberikan untuk menguatkan wawasan Beswan Djarum tentang makna hakekat bangsa dan kebangsaan guna mengokohkan keutuhan Indonesia. Kegiatan ini meliputi Talk Show dan Diskusi Kebangsaan, Cultural Visit, serta pergelaran seni'
                        )
                        ))->execute()->getGraphObject()->asArray();
                $album_id = $response['id'];
            } catch (FacebookRequestException $e) {
                $album_id = 0;
            }
        }
        return $album_id;    
    }
    function post_photo_test($venue, $image, $fb_name, $fbid, $token,$album){
        $photo_id = 0;
        #$token = 'CAACGFg9ewNIBABeQgJfhf5GsM0ZCpZAITZCjF8P8ZC93XIivZBTQiyLYIm6RQuyQHNwn9JamVAocOrTVg4oHYuZBsa4B0ZA0wZBHfnj5ZAX2ZBu0juPSao8gWN6zjRKjgcFAoDz07vXd4iq2VWZAxlvo32wzZBYRw6TBvzpiCxeOffwnQ46dQkzt6juMVsLEqHZCnE3yjtcE1n4X8l7ZAoMtr776zW';
        if($album == 0){
            $album = $fbid;
        }
        #xdebug($album);die;
        if ($token) {
            //xdebug($fb_name.' '.$venue->places_fb_photo_caption);die;
            try {
                $session = new FacebookSession($token);
                if($album != 0){
                    $response = (new FacebookRequest(
                            $session, 'POST', '/' . $album . '/photos', array(
                                'caption' => $fb_name.' '.$venue->places_fb_photo_caption, //. " @".$venue->places_address,
                                //'caption' => $fb_name.' '.$arr, //. " @".$venue->places_address,
                                'source' => new CURLFile($image, 'image/jpeg'),
                            )
                            ))->execute()->getGraphObject()->asArray();
                }else{
                    $response = (new FacebookRequest(
                            $session, 'POST', '/me/photos', array(
                                'caption' => $fb_name.' '.$venue->places_fb_photo_caption, //. " @".$venue->places_address,
                                //'caption' => $fb_name.' '.$arr, //. " @".$venue->places_address,
                                'source' => new CURLFile($image, 'image/jpeg'),
                            )
                            ))->execute()->getGraphObject()->asArray();
                }
                $photo_id = $response['id'];
            return $photo_id;
            } catch (FacebookRequestException $e) {
                $photo_id = 2;
                return json_encode($e);
            }
        } else {
            $photo_id = 1;
            return $photo_id;
        }

    }
    function post_photo_hide($venue, $image, $fb_name, $fbid, $token,$album){
        $photo_id = 0;
        #$token = 'CAACGFg9ewNIBABeQgJfhf5GsM0ZCpZAITZCjF8P8ZC93XIivZBTQiyLYIm6RQuyQHNwn9JamVAocOrTVg4oHYuZBsa4B0ZA0wZBHfnj5ZAX2ZBu0juPSao8gWN6zjRKjgcFAoDz07vXd4iq2VWZAxlvo32wzZBYRw6TBvzpiCxeOffwnQ46dQkzt6juMVsLEqHZCnE3yjtcE1n4X8l7ZAoMtr776zW';
        if($album == 0){
            $album = $fbid; 
        }
        #xdebug($album);die;
        if ($token) {
            //xdebug($fb_name.' '.$venue->places_fb_photo_caption);die;
            try {
                $session = new FacebookSession($token);
                if($album != 0){
                    $response = (new FacebookRequest(
                            $session, 'POST', '/' . $album . '/photos', array(
                                'caption' => $fb_name.' '.$venue->places_fb_photo_caption, //. " @".$venue->places_address,
                                //'caption' => $fb_name.' '.$arr, //. " @".$venue->places_address,
                                'no_story' => 1,
                                'source' => new CURLFile($image, 'image/jpeg'),
                            )
                            ))->execute()->getGraphObject()->asArray();
                }else{
                    $response = (new FacebookRequest(
                            $session, 'POST', '/me/photos', array(
                                'caption' => $fb_name.' '.$venue->places_fb_photo_caption, //. " @".$venue->places_address,
                                //'caption' => $fb_name.' '.$arr, //. " @".$venue->places_address,
                                'no_story' => 1,
                                'source' => new CURLFile($image, 'image/jpeg'),
                            )
                            ))->execute()->getGraphObject()->asArray();
                }
                $photo_id = $response['id'];
            return $photo_id;
            } catch (FacebookRequestException $e) {
                $photo_id = 2;
                return json_encode($e);
            }
        } else {
            $photo_id = 1;
            return $photo_id;
        }

    }

    function array_random($arr, $num = 1) {
        shuffle($arr);
       
        $r = array();
        for ($i = 0; $i < $num; $i++) {
            $r[] = $arr[$i];
        }
        return $num == 1 ? $r[0] : $r;
    }    

    function post_photo_manual($venue, $image, $fb_name, $fbid, $token,$album, $nama){
        #$a = array("apple", "banana", "cherry");
        #$arr = $this->array_random($a); 
        #$date = date('d-m-Y H:i:s');
               
        $photo_id = 0;
        #$token = 'CAACGFg9ewNIBABeQgJfhf5GsM0ZCpZAITZCjF8P8ZC93XIivZBTQiyLYIm6RQuyQHNwn9JamVAocOrTVg4oHYuZBsa4B0ZA0wZBHfnj5ZAX2ZBu0juPSao8gWN6zjRKjgcFAoDz07vXd4iq2VWZAxlvo32wzZBYRw6TBvzpiCxeOffwnQ46dQkzt6juMVsLEqHZCnE3yjtcE1n4X8l7ZAoMtr776zW';
        if($album == 0){
            $album = $fbid;
        }
        #xdebug($album);die;

        if ($token) {
            try {
                $session = new FacebookSession($token);
                if($album != 0){
                    $response = (new FacebookRequest(
                            $session, 'POST', '/' . $album . '/photos', array(
                                'caption' => $nama.' '.$venue->places_fb_photo_caption, //. " @".$venue->places_address,
                                //'caption' => $arr.' '.$date,
                                'source' => new CURLFile($image, 'image/jpeg'),
                            )
                            ))->execute()->getGraphObject()->asArray();
                }else{
                    $response = (new FacebookRequest(
                            $session, 'POST', '/me/photos', array(
                                //'caption' => $fb_name.' '.$venue->places_fb_photo_caption,
                                'source' => new CURLFile($image, 'image/jpeg'),
                            )
                            ))->execute()->getGraphObject()->asArray();
                }
                $photo_id = $response['id'];
            } catch (FacebookRequestException $e) {
                //xdebug($e);die;
                $photo_id = 1;
            }
        } else {
            $photo_id = 2;
        }
        //echo $photo_id; die;
        return $photo_id;

    }    

    function post_photo($venue, $image, $fb_name, $fbid, $token,$album){
        $photo_id = 0;
        if($album == 0){
            $album = $fbid;
        }
        if ($token) {
            try {
                $session = new FacebookSession($token);
                if($album != 0){
                    $response = (new FacebookRequest(
                            $session, 'POST', '/' . $album . '/photos', array(
                                'caption' => $venue->places_cstatus_fb, //. " @".$venue->places_address,
                                //'caption' => $arr.' '.$date,
                                //'source' => new CURLFile($image, 'image/jpeg'),
                                'source' => '@'.$image
                            )
                            ))->execute()->getGraphObject()->asArray();
                }else{
                    $response = (new FacebookRequest(
                            $session, 'POST', '/me/photos', array(
                                'caption' => $venue->places_cstatus_fb,
                                //'source' => new CURLFile($image, 'image/jpeg'),
                                'source' => '@'.$image
                            )
                            ))->execute()->getGraphObject()->asArray();
                }
                $photo_id = $response['id'];
            } catch (FacebookRequestException $e) {
                //xdebug($e);die;
                $photo_id = 1;
            }
        } else {
            $photo_id = 2;
        }
        return $photo_id;

    }
    
    public function tag_photo($uid,$x,$y,$photoid,$fbid, $token) {
        try {
            $session = new FacebookSession($token);
            $response = (new FacebookRequest(
                    $session, 'POST', '/' . $photoid . '/tags', array(
                'x' => $x,
                'y' => $y,
                'tag_uid' => $uid
                    )
                    ))->execute()->getGraphObject()->asArray();
            $response_id = $response;
        } catch (FacebookRequestException $e) {
            $response_id = 0;
        }
        return $response_id;
    }

    public function longlivetoken($token) {     
        $url = 'https://graph.facebook.com/oauth/access_token?client_id='.$this->ci->config->item('api_id', 'facebook').'&client_secret='.$this->ci->config->item('app_secret', 'facebook').'&grant_type=fb_exchange_token&fb_exchange_token='.$token;
        $data = file_get_contents($url);
        $exp1 = explode('&', $data);
        $exp2 = explode('=', $exp1[0]);
        $hasil = $exp2[1];
        return $hasil;
    }

}
