<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Facebook App details
| -------------------------------------------------------------------
|
| To get an facebook app details you have to be a registered developer
| at http://developer.facebook.com and create an app for your project.
|
|  facebook_app_id               string  Your facebook app ID.
|  facebook_app_secret           string  Your facebook app secret.
|  facebook_login_type           string  Set login type. (web, js, canvas)
|  facebook_login_redirect_url   string  URL tor redirect back to after login. Do not include domain.
|  facebook_logout_redirect_url  string  URL tor redirect back to after login. Do not include domain.
|  facebook_permissions          array   The permissions you need.
*/

$config['facebook_app_id']              = '147429305270482';
$config['facebook_app_secret']          = '66f546f2742039e20b69e9ce3fc439f6';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'landing/web_login';
$config['facebook_logout_redirect_url'] = 'landing/logout';
$config['facebook_permissions']         = array('user_likes',
	    'publish_actions',
		'user_friends',
		'public_profile',
		'email',
		'user_photos');

$config['facebook']['api_id'] = '147429305270482';
$config['facebook']['app_secret'] = '66f546f2742039e20b69e9ce3fc439f6';
$config['facebook']['redirect_url'] = 'http://wooz.local/newfb/';
$config['facebook']['permissions'] = array(
    'user_likes',
    'publish_actions',
	'user_friends',
	'public_profile',
	'email',
	'user_photos'
);