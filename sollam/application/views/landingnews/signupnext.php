<style type="text/css">
.stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 50%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $("#InputEmail").focus();
        $("#registration").submit(function() {
            $('.action').html('<img src="http://woozin.tgrid.in/assets/customs/loader.gif" /> Please Wait...');
        });
            $('#lstFruits').multiselect({
                includeSelectAllOption: true
            });
    });
</script>
<?php if ($from == 'fb'): ?>
    <script type="text/javascript">
        var windowSizeArray = ["width=800,height=600",
            "width=800,height=400,scrollbars=yes"];

        $(document).ready(function() {
            $('.newWindow').click(function(event) {

                var url = $(this).attr("href");
                var windowName = "popUp";//$(this).attr("name");
                var windowSize = windowSizeArray[$(this).attr("rel")];
                winRef = new Object();
                winRef = window.open(url, windowName, windowSize);

                event.preventDefault();
                //setTimeout('winRef.close()',15000);

            });
        });
    </script>
<?php endif; ?>
<?php if ($from == 'tw'): ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var urlrfidhome = "<?php echo site_url('landing/follow?url=' . $customs); ?>";
            var accountid = "<?php echo $id; ?>";
            $("#summary").hide();
            $("#id101").click(function() {
                $.ajax({
                    url: urlrfidhome,
                    type: "POST",
                    data: {
                        id: accountid,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.nama == "yes")
                        {
                            $("#summary").show();
                            $("#summarys").hide();
                        }
                        else
                        {
                            $("#summarys").show();
                            $("#summary").hide();
                            return false;
                        }
                    }
                });

            });

        });
    </script>
<?php endif; ?>
<div class="panel panel-info">
        <?php if (validation_errors()) : ?>
        <p class="error">Whoops ! Something wrong
        <?php echo validation_errors('<br />&gt; ', '&nbsp;'); ?>
        </p>
    <?php endif; ?>
    <?php if ($from == 'fb'): ?>
        <?php if (isset($fb_page) && $fb_page != 0): ?>
            <div class="panel-heading">"Like" <?php echo $tw_page; ?> Facebook Page</div>
            <div class="panel-body">
                <?php if ($likefollow == 0): ?>
					<!--
						<div class="fb-page" data-href="<?php echo $fb_page; ?>" data-hide-cover="false" data-show-facepile="false" data-show-posts="false">
						<div class="fb-xfbml-parse-ignore">
							<blockquote cite="<?php echo $fb_page; ?>">
								<a href="<?php echo $fb_page; ?>"><?php echo $fb_page; ?></a>
							</blockquote>
						</div>
						</div>
					-->					
					<a href="<?php echo $fb_page; ?>" rel="0" class="newWindow" >Like Our Facebook Page</a>
                <?php else: ?>
                    <a href="#" style="color:green;">You Already Like Our Facebook Page</a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($from == 'tw'): ?>	
        <?php if (isset($tw_page) && $tw_page != 0): ?>
            <div class="panel-heading">"Follow" <?php echo $tw_page; ?> Twitter Page</div>
            <div class="panel-body">
                <?php if ($likefollow == 1): ?>
                    <div id="summaryss" style="text-align: center;" style="width: 400px;"><img src="http://events.wooz.in/assets/customs/Twitter_Follow_BW.png" style="width: 76px;" alt=""/>@<?php echo $tw_page; ?></div>
                <?php else: ?>
                    <div id="summarys" style="text-align: center;" style="width: 400px;"><a id="id101" href="#" >
                            <img src="http://events.wooz.in/assets/customs/Twitter_Follow.png" style="width: 76px;" alt=""/></a>@<?php echo $tw_page; ?></div>
                    <div id="summary" style="text-align: center;" style="width: 400px;"><img src="http://events.wooz.in/assets/customs/Twitter_Follow_BW.png" style="width: 76px;" alt=""/>@<?php echo $tw_page; ?></div>
                <?php endif; ?>	
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
        <p>Step 4</p>
      </div>
    </div>
</div>
  
  <form role="form" action="<?php echo current_url(); ?>" method="post" onsubmit="ShowLoading()">
    <div class="row setup-content" id="step-1">
        <div class="col-md-12">
          <div class="form-group">
                <label for="InputName">Please Tell Us Your Name?</label>        
                <?php
                $name = set_value('fullname');
                if ($name) {
                    $names = $name;
                } elseif (!$info) {
                    $names = '';
                } else {
                    $names = $info->account_displayname;
                }
                ?>
                <input type="text" required="required" class="form-control input-lg" name="fullname" id="InputName" autocomplete="off" value="<?php echo $names; ?>" placeholder="your name">
            </div>
            <?php if ($from == 'fb' || $from == 'tw'): ?>
            <div class="form-group">
                <?php
                $fbname = set_value('sosmed');
                if ($from == 'fb') {
                    ?>
                    <label for="InputName">Facebook Account Name</label> 
                    <?php
                    $sosmednames = $info->account_fb_name;
                } else {
                    ?>
                    <label for="InputName">Twitter Account Name</label>
                    <?php
                    $sosmednames = '@' . $info->account_tw_username;
                }
                ?>
                <input type="text" required="required" class="form-control input-lg" id="sosmed" name="sosmed" value="<?php echo $sosmednames; ?>" disabled />
            </div>
            <?php endif; ?>
            <div class="form-group">
                <label for="InputGender">What's Your Age Group?</label>
                <br>
                <?php
                $age = set_value('age');
                if ($age) {
                    $ages = $age;
                } elseif (!$info) {
                    $ages = '';
                } else {
                    $ages = $info->account_age;
                }
                ?>
                <div class="form-group">
                    <label class="sr-only" for="InputMonth">Month</label>
                    <select class="form-control input-lg" name="age" id="InputMonth">
                        <option value="0">--Choose--</option>
                        <option value="Under 18" <?php if ($ages == "Under 18") : ?>selected="selected"<?php endif; ?>>Under 18</option>
                        <option value="18 - 25" <?php if ($ages == "18 - 25") : ?>selected="selected"<?php endif; ?>>18 - 25</option>
                        <option value="26 - 35" <?php if ($ages == "26 - 35") : ?>selected="selected"<?php endif; ?>>26 - 35</option>
                        <option value="36 - 45" <?php if ($ages == "36 - 45") : ?>selected="selected"<?php endif; ?>>36 - 45</option>
                        <option value="46 - 60" <?php if ($ages == "46 - 60") : ?>selected="selected"<?php endif; ?>>46 - 60</option>
                        <option value="Over 60" <?php if ($ages == "Over 60") : ?>selected="selected"<?php endif; ?>>Over 60</option>
                    </select> 
                </div> 
            </div>
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-md-12">
            <div class="form-group">
                <label for="InputName">What Type Of Work Do You Do?</label>        
                <?php
                $work = set_value('work');
                if ($work) {
                    $works = $work;
                } elseif (!$info) {
                    $works = '';
                } else {
                    $works = $info->account_work;
                }
                ?>
                <input type="text" required="required" class="form-control input-lg" name="work" id="InputName" autocomplete="off" value="<?php echo $works; ?>" placeholder="your work">
            </div>
            <div class="form-group">
                <label for="InputName">What's Your Email Address?</label>        
                <?php
                $email = set_value('email');
                if ($email) {
                    $emails = $email;
                } elseif (!$info) {
                    $emails = '';
                } else {
                    $emails = $info->account_email;
                }
                ?>
                <input type="email" required="required" class="form-control input-lg" name="email" id="InputName" autocomplete="off" value="<?php echo $emails; ?>" placeholder="your email">
            </div>
            <div class="form-group">
                <label for="InputName">What's Your Contact Number?</label>        
                <?php
                $phone = set_value('phone');
                if ($phone) {
                    $phones = $phone;
                } elseif (!$info) {
                    $phones = '';
                } else {
                    $phones = $info->account_phone;
                }
                ?>
                <input type="text" required="required" class="form-control input-lg" name="phone" id="InputName" autocomplete="off" value="<?php echo $phones; ?>" placeholder="Contact number">
            </div>
        
          <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Back</button>
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-md-12">
            <div class="form-group">
                <label for="InputGender">In Which Of Our Items Are You Interested?</label>
                <br>
                <?php
                $interested = set_value('interested');
                if ($interested) {
                    $interesteds = $interested;
                } elseif (!$info) {
                    $interesteds = '';
                } else {
                    $interesteds = $info->account_interest;
                }
                ?>
                <div class="form-group">
                    <label class="sr-only" for="InputMonth">Choose</label>
                    <select class="multiselect" multiple="multiple" id="lstFruits" name="interested[]">
                        <option value="Ladies Bags - Fashion">Ladies Bags - Fashion</option>
                        <option value="Mens Bags - Fashion">Mens Bags - Fashion</option>
                        <option value="Ladies Bags - Professional">Ladies Bags - Professional</option>
                        <option value="Mens Bags - Professional">Mens Bags - Professional</option>
                        <option value="Belts">Belts</option>
                        <option value="Shoes">Shoes</option>
                        <option value="Wallets/Purses">Wallets/Purses</option>
                        <option value="Accessories">Accessories</option>
                    </select> 
                </div> 
            </div>
            <div class="form-group">
                <label for="InputGender">Please Rate Your Overall Experience at our store</label>
                <br>
                <?php
                $experience = set_value('experience');
                if ($experience) {
                    $experiences = $experience;
                } elseif (!$info) {
                    $experiences = '';
                } else {
                    $experiences = $info->account_experience;
                }
                ?>
                <div class="form-group">
                    <label class="sr-only" for="InputMonth">Choose</label>
                    <select class="form-control input-lg" name="experience" id="InputMonth">
                        <option value="0">--Choose--</option>
                        <option value="Poor" <?php if ($experiences == "Poor") : ?>selected="selected"<?php endif; ?>>Poor</option>
                        <option value="Fair" <?php if ($experiences == "Fair") : ?>selected="selected"<?php endif; ?>>Fair</option>
                        <option value="Good" <?php if ($experiences == "Good") : ?>selected="selected"<?php endif; ?>>Good</option>
                        <option value="Great" <?php if ($experiences == "Great") : ?>selected="selected"<?php endif; ?>>Great</option>
                        <option value="Exceptional" <?php if ($experiences == "Exceptional") : ?>selected="selected"<?php endif; ?>>Exceptional</option>
                    </select> 
                </div> 
            </div>
            <div class="form-group">
                <label for="InputGender">Would You Recommend Us to Your Friends and Family</label>
                <br>
                <?php
                $recomended = set_value('recomended');
                if ($recomended) {
                    $recomendeds = $recomended;
                } elseif (!$info) {
                    $recomendeds = '';
                } else {
                    $recomendeds = $info->account_recomended;
                }
                ?>
                <div class="form-group">
                    <label class="sr-only" for="InputMonth">Choose</label>
                    <select class="form-control input-lg" name="recomended" id="InputMonth">
                        <option value="0">--Choose--</option>
                        <option value="Yes" <?php if ($recomendeds == "Yes") : ?>selected="selected"<?php endif; ?>>Yes</option>
                        <option value="No" <?php if ($recomendeds == "No") : ?>selected="selected"<?php endif; ?>>No</option>
                    </select> 
                </div> 
            </div>
          <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Back</button>
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
        </div>
    </div>
    <div class="row setup-content" id="step-4">
        <div class="col-md-12">
          <div class="form-group">
                <label for="InputName">Any Other Comments</label>        
                <?php
                $comment = set_value('comment');
                if ($comment) {
                    $comments = $comment;
                } elseif (!$info) {
                    $comments = '';
                } else {
                    $comments = $info->account_comment;
                }
                ?><br>
                <textarea name="comment"> 
                    <?php echo $comments; ?>
                </textarea>
            </div>

          <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Back</button>
          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
        </div>
    </div>
  </form>

<script type="text/javascript">
    function ShowLoading(e) {
        var div = document.createElement('div');
        var img = document.createElement('img');
        img.src = '<?php echo base_url(); ?>assets/landing/1.gif';
        div.innerHTML = "Loading...<br />";
        div.style.cssText = 'position: fixed; top: 15%; left: 25%; z-index: 5000; height:300px; width: 622px; text-align: center; background: #EDDBB0; border: 1px solid #000';
        div.appendChild(img);
        document.body.appendChild(div);
        return true;
        // These 2 lines cancel form submission, so only use if needed.
        //window.event.cancelBubble = true;
        //e.stopPropagation();
    }
  $(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn'),
          allPrevBtn = $('.prevBtn');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allPrevBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

          prevStepWizard.trigger('click');
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='email']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');
});
  </script>

